package com.qiezzi.choseviewlibrary.bean;

import com.qiezzi.choseviewlibrary.model.IPickerViewData;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/7/29 17:37
 * @描述: ${TODO}
 */

public class AddressBean {
    /**
     * data : [{"areaCode":"010000","areaName":"北京","list":[{"areaCode":"010100","areaName":"北京","list":[{"areaCode":"010101","areaName":"东城区"},{"areaCode":"010102","areaName":"西城区"},{"areaCode":"010103","areaName":"崇文区"}]}]}]
     * logId : L4BjsVzM
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private List<ProvinceList> data;
    private String logId;
    private boolean status;

    public void setData(List<ProvinceList> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<ProvinceList> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class ProvinceList implements IPickerViewData {
        /**
         * areaCode : 010000
         * areaName : 北京
         * list : [{"areaCode":"010100","areaName":"北京","list":[{"areaCode":"010101","areaName":"东城区"},{"areaCode":"010102","areaName":"西城区"},{"areaCode":"010103","areaName":"崇文区"}]}]
         */
        private String areaCode;
        private String areaName;
        private List<CityList> list;

        public void setAreaCode(String areaCode) {
            this.areaCode = areaCode;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public void setCityList(List<CityList> list) {
            this.list = list;
        }

        public String getAreaCode() {
            return areaCode;
        }

        public String getAreaName() {
            return areaName;
        }

        public List<CityList> getCityList() {
            return list;
        }

        @Override
        public String getPickerViewText() {
            return areaName;
        }

        public class CityList implements IPickerViewData{
            /**
             * areaCode : 010100
             * areaName : 北京
             * list : [{"areaCode":"010101","areaName":"东城区"},{"areaCode":"010102","areaName":"西城区"},{"areaCode":"010103","areaName":"崇文区"}]
             */
            private String areaCode;
            private String areaName;
            private List<CountryList> list;

            public void setAreaCode(String areaCode) {
                this.areaCode = areaCode;
            }

            public void setAreaName(String areaName) {
                this.areaName = areaName;
            }

            public void setCountryList(List<CountryList> list) {
                this.list = list;
            }

            public String getAreaCode() {
                return areaCode;
            }

            public String getAreaName() {
                return areaName;
            }

            public List<CountryList> getCountryList() {
                return list;
            }

            @Override
            public String getPickerViewText() {
                return areaName;
            }

            public class CountryList implements IPickerViewData{
                /**
                 * areaCode : 010101
                 * areaName : 东城区
                 */
                private String areaCode;
                private String areaName;

                public void setAreaCode(String areaCode) {
                    this.areaCode = areaCode;
                }

                public void setAreaName(String areaName) {
                    this.areaName = areaName;
                }

                public String getAreaCode() {
                    return areaCode;
                }

                public String getAreaName() {
                    return areaName;
                }

                @Override
                public String getPickerViewText() {
                    return areaName;
                }
            }
        }
    }
}
