package com.hstypay.hstysales.fragment.devicemall.home;


import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.MainActivity;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.bean.DeviceOrderDispatchBean;
import com.hstypay.hstysales.bean.DeviceOrderListBean;
import com.hstypay.hstysales.fragment.devicemall.BaseDeviceMallFragment;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CustomViewFullScreenDialog;
import com.hstypay.hstysales.widget.EditTextDelete;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zeyu.kuang
 * @time 2020/8/23
 * @desc 待收货订单
 */
public class WaitSendOrderDeviceMallFragment extends BaseDeviceMallFragment {

    public static boolean isNeedRefresh = false;

    @Override
    protected String getTradeStatusField() {
        return ""+ Constants.DEVICE_MALL_WAIT_SEND;
    }

    @Override
    protected boolean isHomeFragment() {
        return true;
    }

    @Override
    protected boolean isNeedRefreshWhenVisible() {
        return isNeedRefresh;
    }

    @Override
    protected void setNeedRefreshWhenVisible(boolean needRefresh) {
        isNeedRefresh = needRefresh;
    }

}
