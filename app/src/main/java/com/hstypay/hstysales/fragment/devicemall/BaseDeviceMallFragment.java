package com.hstypay.hstysales.fragment.devicemall;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.DeviceMallOrderActivity;
import com.hstypay.hstysales.activity.MainActivity;
import com.hstypay.hstysales.adapter.DeviceMallOrderRecyclerAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseFragment;
import com.hstypay.hstysales.bean.DeviceOrderDispatchBean;
import com.hstypay.hstysales.bean.DeviceOrderListBean;
import com.hstypay.hstysales.fragment.devicemall.home.AllOrderDeviceMallFragment;
import com.hstypay.hstysales.fragment.devicemall.home.ClosedOrderDeviceMallFragment;
import com.hstypay.hstysales.fragment.devicemall.home.FinishedOrderDeviceMallFragment;
import com.hstypay.hstysales.fragment.devicemall.home.WaitPostOrderDeviceMallFragment;
import com.hstypay.hstysales.fragment.devicemall.home.WaitSendOrderDeviceMallFragment;
import com.hstypay.hstysales.fragment.devicemall.search.DeviceSearchFragment;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.viewholder.DeviceMallOrderViewHolder;
import com.hstypay.hstysales.widget.CustomViewBottomDialog;
import com.hstypay.hstysales.widget.CustomViewFullScreenDialog;
import com.hstypay.hstysales.widget.EditTextDelete;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zeyu.kuang
 * @time 2020/8/25
 * @desc 硬件商城订单页：全部、待发货、待派单、已完成、已关闭、搜索页fragment的base
 */
public abstract class BaseDeviceMallFragment extends BaseFragment implements DeviceMallOrderViewHolder.OnDeviceMallItemBtnClickListener {
    protected final String TAG = this.getClass().getSimpleName();
    protected DeviceMallOrderActivity mContext;
    protected View mRootView;
    protected LinearLayout mLlSelectCalendarAllOrder;//日期选择ll
    protected TextView mTvShowDateAllOrder;//显示选择的日期
    protected TextView mTvCountOrderAll;//总订单笔数
    protected RecyclerView mRecyclerViewOrderItem;//订单条目
    protected RelativeLayout mRlDateCountHeaderMall;
    protected SHSwipeRefreshLayout mSwipeRefreshLayoutMall;
    protected SafeDialog mLoadDialog;
    public static final int pageSize = 10;
    protected int mCurrentPage = 1;
    protected List<DeviceOrderListBean.DataEntity.DataBean> mOrderDataList = new ArrayList<>();
    ;
    protected DeviceMallOrderRecyclerAdapter mAdapter;
    protected View mLlEmptyDeviceMallOrder;//暂无订单
    protected LinearLayout mRlCountSearchTitle;//搜索结果的标题
    protected TextView mTvSearchWordTitle;//tv搜索词
    protected TextView mTvTotalCountSearchTitle;//tv搜索结果总共订单数
    protected View mViewBgSearchResult;//搜索结果的背景
    private CustomViewBottomDialog mCustomViewBottomDialog;
    private CustomViewFullScreenDialog mSureArrivedDialog;
    protected String mSearchParam;//搜索框参数
    private boolean isLoadmore;//加载更多标识
    private boolean isPullRefresh;//下拉刷新标识
    private String mServiceProviderId;//服务商id
    private String mSalesmanId;//城市经理id
    private boolean isFragmentVisible;
    private boolean isNotFirst;

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mRootView = inflater.inflate(R.layout.fragment_mall_order_all, container, false);
        return mRootView;
    }

    protected void initEvent() {
        mSwipeRefreshLayoutMall.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    if (mOnUpDateTransitCountListener != null) {
                        mOnUpDateTransitCountListener.onUpDateTransitCount(false);
                    }
                    mCurrentPage = 1;
                    isPullRefresh = true;
                    getOrderListData(false);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayoutMall.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayoutMall.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    //mCurrentPage++;//要在获取数据成功后才能++，否则失败了又得--回去
                    getOrderListData(false);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayoutMall.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayoutMall.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutMall.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutMall.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayoutMall.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutMall.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutMall.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayoutMall.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });

    }

    protected void initData() {
        mAdapter = new DeviceMallOrderRecyclerAdapter(mContext);
        mAdapter.setOnDeviceMallItemBtnClickListener(this);
        mRecyclerViewOrderItem.setAdapter(mAdapter);
        mAdapter.setOrderList(mOrderDataList);
        isNotFirst = true;
        mCurrentPage = 1;
        getOrderListData(true);
        if (isHomeFragment() && mOnUpDateTransitCountListener != null) {
            //是DeviceHomeFragment，需要加载订单列表数据
            //刷新在途订单数量。订单由小程序产生
            mOnUpDateTransitCountListener.onUpDateTransitCount(true);
        }
    }

    protected void initView() {
        mLoadDialog = mContext.getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
        mRlDateCountHeaderMall = mRootView.findViewById(R.id.rl_date_count_header_mall);
        mLlSelectCalendarAllOrder = mRootView.findViewById(R.id.ll_select_calendar_all_order);
        mTvShowDateAllOrder = mRootView.findViewById(R.id.tv_show_date_all_order);
        mTvCountOrderAll = mRootView.findViewById(R.id.tv_count_order_all);
        mRecyclerViewOrderItem = mRootView.findViewById(R.id.recycler_view_order_all);
        mRecyclerViewOrderItem.setLayoutManager(new LinearLayoutManager(mContext));
        mSwipeRefreshLayoutMall = mRootView.findViewById(R.id.swipeRefreshLayout_mall);
        mLlEmptyDeviceMallOrder = mRootView.findViewById(R.id.ll_empty_device_mall_order);
        mRlCountSearchTitle = mRootView.findViewById(R.id.rl_count_search_title);
        mTvSearchWordTitle = mRootView.findViewById(R.id.tv_search_word_title);
        mTvTotalCountSearchTitle = mRootView.findViewById(R.id.tv_total_count_search_title);
        mViewBgSearchResult = mRootView.findViewById(R.id.view_bg_search_result);
        initSwipeRefreshLayout();
        if (isHomeFragment()) {//是硬件商城订单的首页，无订单时给空白提示view要调整宽度
            LinearLayout ll_empty_root_device = mLlEmptyDeviceMallOrder.findViewById(R.id.ll_empty_root_device);
            LinearLayout.LayoutParams emptyLayoutParam = (LinearLayout.LayoutParams) ll_empty_root_device.getLayoutParams();
            emptyLayoutParam.leftMargin = (int) (getResources().getDimension(R.dimen.device_mall_item_margin_left) + 0.5);
            emptyLayoutParam.rightMargin = (int) (getResources().getDimension(R.dimen.device_mall_item_margin_left) + 0.5);
            ll_empty_root_device.setLayoutParams(emptyLayoutParam);
        }
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayoutMall.setRefreshEnable(true);
        mSwipeRefreshLayoutMall.setLoadmoreEnable(true);
        if (!(this instanceof DeviceSearchFragment)) {
            mSwipeRefreshLayoutMall.setHeaderGuidanceViewTextColor(Color.WHITE);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        if (isFragmentVisible) {
            initData();
        }
        initEvent();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        /*boolean change = isVisibleToUser != getUserVisibleHint();
        super.setUserVisibleHint(isVisibleToUser);
        if (change && isResumed() && isVisible()) {
            if (getUserVisibleHint()) {
                onFragmentVisibleData();
            } else {
                onFragmentInVisible();
            }
        }*/
        super.setUserVisibleHint(isVisibleToUser);
        isFragmentVisible = isVisibleToUser;
        if (mRootView == null) {
            return;
        }

        if (isFragmentVisible) {
            initData();
        }
    }

    /*@Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            onFragmentInVisible();
        } else {
            onFragmentVisible();
        }
    }*/

    /*@Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && !isHidden()) {
            onFragmentVisible();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getUserVisibleHint() && !isHidden()) {
            onFragmentInVisible();
        }
    }*/
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (DeviceMallOrderActivity) getActivity();
    }


    /**
     * 对用户可见时调用,实现懒加载
     */
    protected void onFragmentVisibleData() {
        LogUtil.d(TAG, "onFragmentVisible1");
        if (isHomeFragment()) {
            //是DeviceHomeFragment，需要加载订单列表数据
            if (mOrderDataList == null || mOrderDataList.size() == 0 || isNeedRefreshWhenVisible()) {
                mCurrentPage = 1;
                getOrderListData(true);
                if (mOnUpDateTransitCountListener != null) {
                    //刷新在途订单数量。订单由小程序产生
                    mOnUpDateTransitCountListener.onUpDateTransitCount(true);
                }
            }
        } else {
            //是DeviceSearchFragment，不需要在这里去加载数据，在搜索后才加载数据
        }
    }

    /**
     * 对用户可见时调用,实现懒加载
     */
    public void refreshData() {
        //是DeviceHomeFragment，需要加载订单列表数据
        mCurrentPage = 1;
        getOrderListData(true);
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    /**
     * 获取订单列表，调用时注意设置 mCurrentPage 的值
     *
     * @param needShowLoadDialog 是否需要显示加载进度条
     */
    protected void getOrderListData(boolean needShowLoadDialog) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (needShowLoadDialog) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", mCurrentPage);
            if (!TextUtils.isEmpty(mServiceProviderId))
                map.put("serviceChannelId", mServiceProviderId);
            if (!TextUtils.isEmpty(mSalesmanId))
                map.put("salesmanId", mSalesmanId);
            if ((Constants.DEVICE_MALL_SEARCH + "").equals(getTradeStatusField())) {
                //是搜索 (的下拉刷新和加载更多)
                map.put("tradeStatus", "");//在全部订单中搜索
                map.put("searchParam", mSearchParam);
            } else {
                //不是搜索
                map.put("tradeStatus", getTradeStatusField());
            }
            map.put("sortType", "asc");//排序，asc 就是根据下单时间顺序排
            ServerClient.newInstance(MyApplication.getContext()).getDeviceOrderList(MyApplication.getContext(), Constants.TAG_GET_DEVICE_ORDER_LIST + "_" + getTradeStatusField(), map);//+"_"+getTradeStatusField()避免没显示出来的Fragment也会处理eventBus事件
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOrderListEvent(NoticeEvent event) {
        LogUtil.d("TAG_GET_DEVICE_ORDER_LIST1=" + event.getTag());
        if (event.getTag().equals(Constants.TAG_GET_DEVICE_ORDER_LIST + "_" + getTradeStatusField())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            setSwipeRefreshState(isPullRefresh, isLoadmore, 500);
            DeviceOrderListBean msg = (DeviceOrderListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_DEVICE_ORDER_LIST_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_DEVICE_ORDER_LIST_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mLlEmptyDeviceMallOrder.setVisibility(View.GONE);
                        mSwipeRefreshLayoutMall.setVisibility(View.VISIBLE);
                        if (mCurrentPage == 1) {
                            mOrderDataList.clear();
                        }
                        LogUtil.d("mOrderDataList=" + mOrderDataList.size());
                        mOrderDataList.addAll(msg.getData().getData());
                        LogUtil.d("mOrderDataList2=" + mOrderDataList.size());
                        mAdapter.setOrderList(mOrderDataList);
                        mCurrentPage++;
//                        setNeedRefreshWhenVisible(false);
                    } else {
                        if (isLoadmore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mOrderDataList.clear();//后面会展示空视图
                            mLlEmptyDeviceMallOrder.setVisibility(View.VISIBLE);
                            mSwipeRefreshLayoutMall.setVisibility(View.GONE);
                        }
                    }
                    /*if (mOrderDataList != null && mOrderDataList.size() > 0) {
                        mLlEmptyDeviceMallOrder.setVisibility(View.GONE);
                        mSwipeRefreshLayoutMall.setVisibility(View.VISIBLE);
                    } else {
                        mLlEmptyDeviceMallOrder.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayoutMall.setVisibility(View.GONE);
                    }*/
                    break;
            }
            isLoadmore = false;
            isPullRefresh = false;
        }
    }


    /**
     * 关闭下拉刷新和上拉加载的进度条
     */
    private void setSwipeRefreshState(boolean isPullRefresh, boolean isLoadmore, int delay) {
        if (isPullRefresh) {
            mSwipeRefreshLayoutMall.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayoutMall.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayoutMall.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayoutMall.finishLoadmore();
                }
            }, delay);
        }
    }


    /**
     * 对用户不可见时调用
     */
    protected void onFragmentInVisible() {
        LogUtil.d(TAG, "onFragmentInVisible");
    }


    /**
     * 获取交易状态字段值
     *
     * @return
     */
    protected abstract String getTradeStatusField();

    /**
     * 是不是属于DeviceHomeFragment
     *
     * @return
     */
    protected abstract boolean isHomeFragment();

    /**
     * 页面可见时是否需要刷新页面
     *
     * @return
     */
    protected abstract boolean isNeedRefreshWhenVisible();

    /**
     * 设置页面可见时是否需要刷新页面
     *
     * @param needRefresh
     */
    protected abstract void setNeedRefreshWhenVisible(boolean needRefresh);

    /**
     * 复制订单号
     *
     * @param ordersBean 订单
     */
    @Override
    public void onCopyOrderNum(DeviceOrderListBean.DataEntity.DataBean ordersBean) {
        //复制订单号
        ClipboardManager cm = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData mClipData = ClipData.newPlainText(null, ordersBean.getOrderNo());
        cm.setPrimaryClip(mClipData);
        ToastUtil.showToastShort(getString(R.string.copy_success_mall));
    }


    /**
     * 派单(包括全部、搜索、待发货)
     *
     * @param ordersBean 订单
     */
    @Override
    public void onPostGoods(final DeviceOrderListBean.DataEntity.DataBean ordersBean) {
        //派单
        mCustomViewBottomDialog = new CustomViewBottomDialog(mContext);
        mCustomViewBottomDialog.setView(R.layout.layout_dialog_postgoods_mall);
        final EditTextDelete et_process_man_mall = mCustomViewBottomDialog.findViewById(R.id.et_process_man_mall);//处理人
        final EditTextDelete et_phone_postgoods_mall = mCustomViewBottomDialog.findViewById(R.id.et_phone_postgoods_mall);//电话
        final ImageView iv_edit_tip_mall = mCustomViewBottomDialog.findViewById(R.id.iv_edit_tip_mall);//编辑
        Button btn_sure_post_mall = mCustomViewBottomDialog.findViewById(R.id.btn_sure_post_mall);//确定
        et_process_man_mall.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_REAL_NAME));
        et_phone_postgoods_mall.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_TELEPHONE));
        mCustomViewBottomDialog.show();
        et_phone_postgoods_mall.setOnFocusChangeListener(null);//避免一获取到焦点后delete图片就显示出来，这会与iv_edit_tip_mall重叠
        et_phone_postgoods_mall.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    iv_edit_tip_mall.setVisibility(View.VISIBLE);
                } else {
                    iv_edit_tip_mall.setVisibility(View.GONE);
                }
            }
        });
        btn_sure_post_mall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String postMan = et_process_man_mall.getText().toString();
                String phone = et_phone_postgoods_mall.getText().toString();
                if (TextUtils.isEmpty(phone)) {
                    ToastUtil.showToastShort(getResources().getString(R.string.tip_input_phone_empty_mall));
                    return;
                }
                if (phone.length() < 11) {
                    ToastUtil.showToastShort(getResources().getString(R.string.tip_input_phone_error_mall));
                    return;
                }

                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    Map<String, Object> map = new HashMap<>();
                    map.put("tradeStatus", "2");//需要流转的状态：2 表示 派单（待收货）
                    map.put("orderNo", ordersBean.getOrderNo());//订单号
                    map.put("sendSalesmanName", postMan);//处理人
                    map.put("sendSalesmanMobile", phone);//电话
                    ServerClient.newInstance(MyApplication.getContext()).deviceDispatchOrder(MyApplication.getContext(), Constants.TAG_GET_DEVICE_ORDER_DISPATCH + "_" + getTradeStatusField(), map);//+"_"+getTradeStatusField()避免没显示出来的Fragment也会处理eventBus事件
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDispatchOrderEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_DEVICE_ORDER_DISPATCH + "_" + getTradeStatusField())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            DeviceOrderDispatchBean msg = (DeviceOrderDispatchBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_DEVICE_ORDER_DISPATCH_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_DEVICE_ORDER_DISPATCH_TRUE:
                    mCustomViewBottomDialog.dismiss();
                    ToastUtil.showToastShort(getResources().getString(R.string.post_order_success_mall));
                    AllOrderDeviceMallFragment.isNeedRefresh = true;
                    ClosedOrderDeviceMallFragment.isNeedRefresh = true;
                    FinishedOrderDeviceMallFragment.isNeedRefresh = true;
                    WaitPostOrderDeviceMallFragment.isNeedRefresh = true;
                    WaitSendOrderDeviceMallFragment.isNeedRefresh = true;
                    if (mOnUpDateTransitCountListener != null) {
                        mOnUpDateTransitCountListener.onUpDateTransitCount(true);
                    }
                    mCurrentPage = 1;
                    getOrderListData(true);
                    break;
            }
        }
    }

    /**
     * 确认订单送达(包括全部、搜索、待收货)
     *
     * @param ordersBean 订单
     */
    @Override
    public void onSureArrived(final DeviceOrderListBean.DataEntity.DataBean ordersBean) {
        mSureArrivedDialog = new CustomViewFullScreenDialog(mContext);
        mSureArrivedDialog.setView(R.layout.layout_dialog_sure_arrived_device_mall);
        final EditTextDelete et_get_merchant_receive_code = mSureArrivedDialog.findViewById(R.id.et_get_merchant_receive_code);
        Button btn_cancel_merchant_receiver = mSureArrivedDialog.findViewById(R.id.btn_cancel_merchant_receiver);
        Button btn_sure_merchant_receiver = mSureArrivedDialog.findViewById(R.id.btn_sure_merchant_receiver);
        mSureArrivedDialog.show();
        btn_cancel_merchant_receiver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSureArrivedDialog.dismiss();
            }
        });
        btn_sure_merchant_receiver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String receiveCode = et_get_merchant_receive_code.getText().toString();
                if (TextUtils.isEmpty(receiveCode)) {
                    ToastUtil.showToastShort(getResources().getString(R.string.tip_receive_code_empty));
                    return;
                }
                sureArrived(receiveCode, ordersBean);
            }
        });
    }

    //确认收货请求
    private void sureArrived(String receiveCode, DeviceOrderListBean.DataEntity.DataBean ordersBean) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("tradeStatus", "3");//需要流转的状态：3 已完成
            map.put("orderNo", ordersBean.getOrderNo());//订单号
            map.put("receiveCode", receiveCode);
            ServerClient.newInstance(MyApplication.getContext()).deviceSureOrderArrive(MyApplication.getContext(), Constants.TAG_GET_DEVICE_ORDER_ARRIVE + "_" + getTradeStatusField(), map);//+"_"+getTradeStatusField()避免没显示出来的Fragment也会处理eventBus事件
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSureArriveEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_DEVICE_ORDER_ARRIVE + "_" + getTradeStatusField())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            DeviceOrderDispatchBean msg = (DeviceOrderDispatchBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_DEVICE_ORDER_ARRIVE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_DEVICE_ORDER_ARRIVE_TRUE:
                    mSureArrivedDialog.dismiss();
                    ToastUtil.showToastShort(getResources().getString(R.string.sure_arrived_success_mall));
                    AllOrderDeviceMallFragment.isNeedRefresh = true;
                    ClosedOrderDeviceMallFragment.isNeedRefresh = true;
                    FinishedOrderDeviceMallFragment.isNeedRefresh = true;
                    WaitPostOrderDeviceMallFragment.isNeedRefresh = true;
                    WaitSendOrderDeviceMallFragment.isNeedRefresh = true;
                    if (mOnUpDateTransitCountListener != null) {
                        mOnUpDateTransitCountListener.onUpDateTransitCount(true);
                    }
                    mCurrentPage = 1;
                    getOrderListData(true);
                    break;
            }
        }
    }

    //更新在途订单数量（角标）
    public interface onUpDateTransitCountListener {
        /**
         * 更新在途订单数量
         *
         * @param needLoading 是否需要加载进度条
         */
        void onUpDateTransitCount(boolean needLoading);
    }

    private onUpDateTransitCountListener mOnUpDateTransitCountListener;

    public void setOnUpDateTransitCountListener(onUpDateTransitCountListener onUpDateTransitCountListener) {
        mOnUpDateTransitCountListener = onUpDateTransitCountListener;
    }

    public void setFilter(String serviceProviderId, String salesmanId) {
        mServiceProviderId = serviceProviderId;
        mSalesmanId = salesmanId;
    }
}
