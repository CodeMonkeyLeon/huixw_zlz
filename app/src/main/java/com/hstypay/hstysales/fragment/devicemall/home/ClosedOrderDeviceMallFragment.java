package com.hstypay.hstysales.fragment.devicemall.home;

import com.hstypay.hstysales.adapter.DeviceMallOrderRecyclerAdapter;
import com.hstypay.hstysales.fragment.devicemall.BaseDeviceMallFragment;
import com.hstypay.hstysales.utils.Constants;

/**
 * @author zeyu.kuang
 * @time 2020/8/23
 * @desc 已关闭订单
 */
public class ClosedOrderDeviceMallFragment extends BaseDeviceMallFragment {

    public static boolean isNeedRefresh = false;

    @Override
    protected String getTradeStatusField() {
        return ""+Constants.DEVICE_MALL_WAIT_ClOSE;
    }

    @Override
    protected boolean isHomeFragment() {
        return true;
    }

    @Override
    protected boolean isNeedRefreshWhenVisible() {
        return isNeedRefresh;
    }

    @Override
    protected void setNeedRefreshWhenVisible(boolean needRefresh) {
        isNeedRefresh = needRefresh;
    }
}
