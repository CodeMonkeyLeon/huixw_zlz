package com.hstypay.hstysales.fragment;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.MainActivity;
import com.hstypay.hstysales.adapter.HomeAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseFragment;
import com.hstypay.hstysales.bean.HomeDataBean;
import com.hstypay.hstysales.bean.HomeNoticeBean;
import com.hstypay.hstysales.bean.InfoBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CustomLinearLayoutManager;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/11/21 15:58
 * @描述: ${TODO}
 */

public class HomeFragment extends BaseFragment {
    private View mView;
    private RecyclerView mRecyclerview;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private HomeAdapter mHomeAdapter;
    private SafeDialog mLoadDialog;
    private HomeDataBean mHomeDataBean;
    private boolean mMarquee = true;
    private HomeNoticeBean.DataEntity mNoticeData;

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        initView(mView);
        return mView;
    }

    private void initView(View view) {
        initRecyclerView(view);
        initSwipeRefreshLayout(view);
    }

    private void initRecyclerView(View view) {
        mRecyclerview = (RecyclerView) view.findViewById(R.id.recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mRecyclerview.setLayoutManager(mLinearLayoutManager);
        mRecyclerview.setItemAnimator(new DefaultItemAnimator());
        mLoadDialog = ((MainActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
    }

    private void initSwipeRefreshLayout(View view) {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setLoadmoreEnable(false);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    ServerClient.newInstance(MyApplication.getContext()).homeData(MyApplication.getContext(), Constants.TAG_HOME_DATA, null);
                    ServerClient.newInstance(MyApplication.getContext()).homeNotice(MyApplication.getContext(), Constants.TAG_HOME_NOTICE, null);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {

            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });
    }

    private void initData() {
        mNoticeData = new HomeNoticeBean().new DataEntity();
        mHomeDataBean = new HomeDataBean();
        mHomeAdapter = new HomeAdapter(getActivity(), mHomeDataBean, mMarquee, mNoticeData);
        mRecyclerview.setAdapter(mHomeAdapter);

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).homeData(MyApplication.getContext(), Constants.TAG_HOME_DATA, null);
            ServerClient.newInstance(MyApplication.getContext()).homeNotice(MyApplication.getContext(), Constants.TAG_HOME_NOTICE, null);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMarquee = true;
        initData();
        //mRecyclerview.setAdapter(mHomeAdapter);
    }

    @Override
    protected void onFragmentVisible() {
        super.onFragmentVisible();
        StatusBarUtil.setTranslucentStatusWhiteIcon(getActivity());
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            mMarquee = true;
            mRecyclerview.setAdapter(mHomeAdapter);
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                ServerClient.newInstance(MyApplication.getContext()).homeData(MyApplication.getContext(), Constants.TAG_HOME_DATA, null);
                ServerClient.newInstance(MyApplication.getContext()).homeNotice(MyApplication.getContext(), Constants.TAG_HOME_NOTICE, null);
            } else {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
            }
        } else {
            mMarquee = false;
            mRecyclerview.setAdapter(mHomeAdapter);
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_HOME_DATA)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            HomeDataBean msg = (HomeDataBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    refreshFinish();
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.HOME_DATA_FALSE:
                    refreshFinish();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.HOME_DATA_TRUE:
                    refreshFinish();
                    if (msg != null) {
                        //mHomeDataBean=msg;
                        mHomeDataBean.setData(msg.getData());
                        // mHomeDataBean.setMchTotal(msg.getMchTotal());
                   /*     if (msg.getData().getNotRegisterCount() != 0){
                            SpUtil.putBoolean(MyApplication.getContext(),Constants.SP_HAS_NO_INFO,true);
                        }else{
                            SpUtil.putBoolean(MyApplication.getContext(),Constants.SP_HAS_NO_INFO,false);
                        }*/
                        mHomeAdapter.notifyDataSetChanged();
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_HOME_NOTICE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            HomeNoticeBean msg = (HomeNoticeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    refreshFinish();
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    refreshFinish();
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    refreshFinish();
                    if (msg != null && msg.getData() != null) {
                        ((MainActivity) getActivity()).showRedDot(true);
                        mNoticeData.setTitle(msg.getData().getTitle());
                        mNoticeData.setId(msg.getData().getId());
                        mHomeAdapter.notifyDataSetChanged();
                    } else {
                        //((MainActivity)getActivity()).showRedDot(false);
                        ServerClient.newInstance(MyApplication.getContext()).unReadNotice(MyApplication.getContext(), Constants.TAG_UNREAD_NOTICE, null);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_UNREAD_NOTICE)) {
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg != null && !TextUtils.isEmpty(msg.getData())) {
                        if ("yes".equals(msg.getData())) {
                            ((MainActivity) getActivity()).showRedDot(true);
                        } else {
                            ((MainActivity) getActivity()).showRedDot(false);
                        }
                    } else {
                        ((MainActivity) getActivity()).showRedDot(false);
                    }
                    break;
            }
        }
    }

    private void refreshFinish() {
        mSwipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.finishRefresh();
            }
        }, 500);
    }

    public void setConfig(String config) {
        mHomeAdapter.setConfig(config);
    }
}
