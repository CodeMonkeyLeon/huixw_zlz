package com.hstypay.hstysales.fragment.devicemall.home;

import com.hstypay.hstysales.fragment.devicemall.BaseDeviceMallFragment;

/**
 * @author zeyu.kuang
 * @time 2020/8/23
 * @desc 全部订单
 */
public class AllOrderDeviceMallFragment extends BaseDeviceMallFragment {

    public static boolean isNeedRefresh = false;

    @Override
    protected String getTradeStatusField() {
        return "";
    }

    @Override
    protected boolean isHomeFragment() {
        return true;
    }

    @Override
    protected boolean isNeedRefreshWhenVisible() {
        return isNeedRefresh;
    }

    @Override
    protected void setNeedRefreshWhenVisible(boolean needRefresh) {
        isNeedRefresh = needRefresh;
    }

    @Override
    protected void onFragmentVisible() {
        super.onFragmentVisible();

    }


}
