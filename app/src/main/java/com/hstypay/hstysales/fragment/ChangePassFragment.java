package com.hstypay.hstysales.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.MainActivity;
import com.hstypay.hstysales.adapter.MerchantAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseFragment;
import com.hstypay.hstysales.base.BaseMerchantFragment;
import com.hstypay.hstysales.bean.MerchantListBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CustomLinearLayoutManager;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static android.app.Activity.RESULT_OK;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/11/21 20:29
 * @描述: 变更审核成功
 */

public class ChangePassFragment extends BaseMerchantFragment {
    private View mView;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerview;
    private MerchantAdapter mMerchantAdapter;
    private int type = 8;//页面标题

    private int currentPage = 1;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<MerchantListBean.DataEntity.ItemData> mListData = new ArrayList<>();
    private RelativeLayout mLayoutEmpty;
    private SafeDialog mLoadDialog;
    private boolean isFragmentVisible;
    private boolean isNotFirst;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        LogUtil.d("fragment2_" + isVisibleToUser);
        isFragmentVisible = isVisibleToUser;
        if (mView == null) {
            return;
        }

        MyApplication.type = 10;//查询状态
        if (isFragmentVisible) {
            if (!isNotFirst) {
                onFragmentVisibleChange(false);
            } else {
                onFragmentVisibleChange(true);
            }
        }
    }

    protected void onFragmentVisibleChange(boolean isNotFirst) {
        if (isNotFirst) {
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                currentPage = 1;
                isRefreshed = true;
                DialogUtil.safeShowDialog(mLoadDialog);
                getData(pageSize, currentPage, "");
            } else {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
            }
        } else {
            initData();
        }
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mView = inflater.inflate(R.layout.fragment_list, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView(mView);
        initListener();
        if (isFragmentVisible && !isNotFirst) {
            onFragmentVisibleChange(false);
        }
    }

    private void initView(View view) {
        mLayoutEmpty = (RelativeLayout) view.findViewById(R.id.layout_data_empty);
        mLoadDialog = ((MainActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);

        initRecyclerView(view);
        initSwipeRefreshLayout(view);
    }

    private void initRecyclerView(View view) {
        mRecyclerview = (RecyclerView) view.findViewById(R.id.recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(getActivity());
        mRecyclerview.setLayoutManager(mLinearLayoutManager);
        mRecyclerview.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout(View view) {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    currentPage = 1;
                    isRefreshed = true;
                    getData(pageSize, currentPage, "");
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getData(pageSize, currentPage, "");
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });
    }

    private void initListener() {

    }

    private void initData() {
        mListData.clear();
        mMerchantAdapter = new MerchantAdapter(getActivity(), this, Constants.INTENT_FRAGMENT_TO_MERCHANT, type, mListData);
        mRecyclerview.setAdapter(mMerchantAdapter);

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            isNotFirst = true;
            isRefreshed = true;
            currentPage = 1;
            DialogUtil.safeShowDialog(mLoadDialog);
            getData(pageSize, currentPage, "");
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getData(int pageSize, int currentPage, String search) {
        getMerChantData(pageSize, currentPage, 10, search);
    }

    private void getMerChantData(int pageSize, int currentPage, int activateStatus, String search) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            map.put("mchQueryStatus", activateStatus);
            if (!TextUtils.isEmpty(mServiceProviderId))
                map.put("serviceChannelId", mServiceProviderId);
            if (!TextUtils.isEmpty(mSalesmanId))
                map.put("salesmanId", mSalesmanId);
            if (!TextUtils.isEmpty(search)) {
                map.put("search", search);
            }
            ServerClient.newInstance(MyApplication.getContext()).getMerchantList(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_LIST + type, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtil.d("onResume1");
        Properties prop = new Properties();
        prop.setProperty("status", "审核失败");
        StatService.trackCustomKVEvent(getActivity(), "1028", prop);
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MERCHANT_LIST + type) || event.getTag().equals(Constants.TAG_GET_MERCHANT_LIST_REFRESH + type)) {
            if (event.getTag().equals(Constants.TAG_GET_MERCHANT_LIST_REFRESH + type)) {
                //是推送刷新
                isRefreshed = true;
            }
            DialogUtil.safeCloseDialog(mLoadDialog);
            MerchantListBean msg = (MerchantListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_MERCHANT_LIST_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_MERCHANT_LIST_TRUE://验证码返回成功
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg != null && msg.getData() != null) {
                        if (msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                            mListData.addAll(msg.getData().getData());
                            mMerchantAdapter.notifyDataSetChanged();
                        } else {
                            if (isLoadmore) {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                            }
                        }
                    } else {
                        if (isLoadmore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        }
                    }
                    if (mListData != null && mListData.size() > 0) {
                        mLayoutEmpty.setVisibility(View.GONE);
                    } else {
                        mLayoutEmpty.setVisibility(View.VISIBLE);
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mListData.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_LIST_TO_MERCHANT) {
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                isRefreshed = true;
                currentPage = 1;
                DialogUtil.safeShowDialog(mLoadDialog);
                getData(pageSize, currentPage, "");
            } else {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
            }
        }
    }

    @Override
    public void refreshData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            currentPage = 1;
            isRefreshed = true;
            DialogUtil.safeShowDialog(mLoadDialog);
            getData(pageSize, currentPage, "");
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }
}
