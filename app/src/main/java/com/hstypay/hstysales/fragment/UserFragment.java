package com.hstypay.hstysales.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.BuildConfig;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.AboutUsActivity;
import com.hstypay.hstysales.activity.AppOpenManagerActivity;
import com.hstypay.hstysales.activity.ClipImageActivity;
import com.hstypay.hstysales.activity.DeviceListActivity;
import com.hstypay.hstysales.activity.DeviceMallOrderActivity;
import com.hstypay.hstysales.activity.InvitationCodeActivity;
import com.hstypay.hstysales.activity.MainActivity;
import com.hstypay.hstysales.activity.NoticeActivity;
import com.hstypay.hstysales.activity.RealNameAuthActivity;
import com.hstypay.hstysales.activity.SettingActivity;
import com.hstypay.hstysales.activity.ShareHwcAppActivity;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseFragment;
import com.hstypay.hstysales.bean.InfoBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.SelectPicPopupWindow;
import com.tencent.stat.StatService;
import com.v5kf.client.lib.V5ClientAgent;
import com.v5kf.client.lib.V5ClientAgent.ClientOpenMode;
import com.v5kf.client.lib.V5ClientConfig;
import com.v5kf.client.lib.entity.V5Message;
import com.v5kf.client.ui.ClientChatActivity;
import com.v5kf.client.ui.callback.ChatActivityFuncIconClickListener;
import com.v5kf.client.ui.callback.OnChatActivityListener;
import com.v5kf.client.ui.callback.OnURLClickListener;
import com.v5kf.client.ui.callback.UserWillSendMessageListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/11/21 15:58
 * @描述: ${TODO}
 */

public class UserFragment extends BaseFragment implements View.OnClickListener, OnChatActivityListener {
    private View mView;
    private ImageView mIvUserPhoto, mIvNoticeRed;
    private TextView mTvUserName, mTvUserCompany;
    private RelativeLayout mRlInvitationCode, mRlNotice, mRlSetting, mRlAboutUs, mRlContactUs;

    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    public static final int REQUEST_CROP_PHOTO = 0x1003;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private File tempFile;
    private Uri originalUri;
    private int type = 1;
    private RelativeLayout mRlDeviceMallOrder;//商户硬件订单管理
    private RelativeLayout mRlApplicationOpenManager;//应用开通管理
    private TextView mTvDownloadHwc;//下载汇旺财app
    private RelativeLayout mRlWxAuthMerchant;//微信实名认证
    private RelativeLayout mRlBindDevicesMerchant;//绑定设备

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mView = inflater.inflate(R.layout.fragment_user, container, false);
        initView(mView);
        initListener();
        initV5Chat();
        return mView;
    }

    private void initView(View view) {

        mTvUserName = (TextView) view.findViewById(R.id.tv_user_name);
        mTvUserCompany = (TextView) view.findViewById(R.id.tv_user_company);
        mIvUserPhoto = (ImageView) view.findViewById(R.id.iv_user_photo);

        mRlInvitationCode = (RelativeLayout) view.findViewById(R.id.rl_invitation_code);
        mRlNotice = (RelativeLayout) view.findViewById(R.id.rl_notice);
        mIvNoticeRed = (ImageView) view.findViewById(R.id.iv_notice_red);
        mRlSetting = (RelativeLayout) view.findViewById(R.id.rl_setting);
        mRlAboutUs = (RelativeLayout) view.findViewById(R.id.rl_about_us);
        mRlContactUs = (RelativeLayout) view.findViewById(R.id.rl_contact_us);
        mRlDeviceMallOrder = (RelativeLayout) view.findViewById(R.id.rl_device_mall_order);
        mRlApplicationOpenManager = view.findViewById(R.id.rl_application_open_manager);

        mTvDownloadHwc = view.findViewById(R.id.tv_download_hwc);
        mRlWxAuthMerchant = view.findViewById(R.id.rl_wx_auth_merchant);
        mRlBindDevicesMerchant = view.findViewById(R.id.rl_bind_devices_merchant);

        String appMetaData = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (!(Constants.HXW.equals(appMetaData) || Constants.PLAY.equals(appMetaData))) {
            //oem项目不用机器人客服
            mRlContactUs.setVisibility(View.GONE);
            mTvDownloadHwc.setVisibility(View.GONE);
        }
        if (Constants.XYZF.equals(appMetaData)) {
            mRlDeviceMallOrder.setVisibility(View.GONE);
        }
        if ("SUPERADMIN".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_ROLE_CODE))) {
            mIvUserPhoto.setImageResource(R.mipmap.service_pic_photo);
            mTvUserName.setVisibility(View.GONE);
            if ("SC".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                mTvUserCompany.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_CHANNEL_NAME));
            } else {
                mTvUserCompany.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER_NAME));
            }
        } else {
            mTvUserCompany.setText(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER_NAME));
        }
        String realName = SpUtil.getString(MyApplication.getContext(), Constants.SP_REAL_NAME);
        if (!TextUtils.isEmpty(realName)) {
            mTvUserName.setText(realName);
        }
        initConfig();
    }

    public void initConfig() {
        if (MyApplication.getConfig().contains(Constants.CONFIG_ABOUNT) && mRlAboutUs != null)
            mRlAboutUs.setVisibility(View.VISIBLE);
        if (MyApplication.getConfig().contains(Constants.CONFIG_INVIATION) && mRlInvitationCode != null)
            if ("SALESMAN".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_ROLE_CODE)))
                mRlInvitationCode.setVisibility(View.VISIBLE);
        if (MyApplication.getConfig().contains(Constants.CONFIG_SETTING) && mRlSetting != null)
            mRlSetting.setVisibility(View.VISIBLE);
        if (MyApplication.getConfig().contains(Constants.CONFIG_NOTICE) && mRlNotice != null)
            mRlNotice.setVisibility(View.VISIBLE);
        if (MyApplication.getConfig().contains(Constants.CONFIG_MCH_DEVICE_ORDER) && mRlDeviceMallOrder != null)
            mRlDeviceMallOrder.setVisibility(View.VISIBLE);
        if (MyApplication.getConfig().contains(Constants.CONFIG_APPLI_MANAGER) && mRlApplicationOpenManager != null)
            mRlApplicationOpenManager.setVisibility(View.VISIBLE);
        if (mRlWxAuthMerchant != null)
            mRlWxAuthMerchant.setVisibility(View.VISIBLE);
        if (mRlBindDevicesMerchant != null)
            mRlBindDevicesMerchant.setVisibility(View.VISIBLE);
    }

    private void initListener() {
        //mIvUserPhoto.setOnClickListener(this);
        mRlInvitationCode.setOnClickListener(this);
        mRlNotice.setOnClickListener(this);
        mRlSetting.setOnClickListener(this);
        mRlAboutUs.setOnClickListener(this);
        mRlContactUs.setOnClickListener(this);
        mRlDeviceMallOrder.setOnClickListener(this);
        mRlApplicationOpenManager.setOnClickListener(this);
        mTvDownloadHwc.setOnClickListener(this);
        mRlWxAuthMerchant.setOnClickListener(this);
        mRlBindDevicesMerchant.setOnClickListener(this);
    }

    private void initData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            ServerClient.newInstance(MyApplication.getContext()).unReadNotice(MyApplication.getContext(), Constants.TAG_UNREAD_NOTICE, null);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    @Override
    protected void onFragmentVisible() {
        super.onFragmentVisible();
        StatusBarUtil.setTranslucentStatusWhiteIcon(getActivity());
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            initData();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_user_photo:
                PermissionUtils.checkPermissionArray(getActivity(), permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE1);
                choice();
                break;
            case R.id.rl_invitation_code:
                StatService.trackCustomKVEvent(getActivity(), "1043", null);
                getActivity().startActivity(new Intent(getActivity(), InvitationCodeActivity.class));
                break;
            case R.id.rl_notice:
                StatService.trackCustomKVEvent(getActivity(), "1059", null);
                getActivity().startActivity(new Intent(getActivity(), NoticeActivity.class));
                break;
            case R.id.rl_setting:
                StatService.trackCustomKVEvent(getActivity(), "1052", null);
                getActivity().startActivity(new Intent(getActivity(), SettingActivity.class));
                break;
            case R.id.rl_about_us:
                getActivity().startActivity(new Intent(getActivity(), AboutUsActivity.class));
                break;
            case R.id.rl_contact_us:
//                Intent intent = new Intent(getActivity(), QuestionActivity.class);
//                intent.putExtra(Constants.REGISTER_INTENT, Constants.H5_CONTACT_US_URL);
//                getActivity().startActivity(intent);
                startChatActivity();
                break;
            case R.id.rl_device_mall_order:
                getActivity().startActivity(new Intent(getActivity(), DeviceMallOrderActivity.class));
                break;
            case R.id.rl_application_open_manager:
                getActivity().startActivity(new Intent(getActivity(), AppOpenManagerActivity.class));
                break;
            case R.id.tv_download_hwc:
                //下载app
                getActivity().startActivity(new Intent(getActivity(), ShareHwcAppActivity.class));
                break;
            case R.id.rl_wx_auth_merchant:
                //商家实名认证
                getActivity().startActivity(new Intent(getActivity(), RealNameAuthActivity.class));
                break;
            case R.id.rl_bind_devices_merchant:
                //绑定设备
                getActivity().startActivity(new Intent(getActivity(), DeviceListActivity.class));
                break;
            default:
                break;
        }
    }

    /**
     * 初始化V5KF 界面
     */
    private void initV5Chat() {
        // V5客服系统客户端配置
        V5ClientConfig config = V5ClientConfig.getInstance(getActivity());
        V5ClientConfig.USE_HTTPS = true; // 使用加密连接，默认true
        V5ClientConfig.SOCKET_TIMEOUT = 20000; // 请求超时时间
        config.setHeartBeatEnable(true); // 是否允许发送心跳包保活
        config.setHeartBeatTime(30000); // 心跳包间隔时间ms
        config.setShowLog(true); // 显示日志，默认为true
        config.setLogLevel(V5ClientConfig.LOG_LV_DEBUG); // 显示日志级别，默认为全部显示

        /*if (!TextUtils.isEmpty(MyApplication.getRealName())) {
            config.setNickname(StringUtils.hideFirstName(MyApplication.getRealName()));
        } else if (!TextUtils.isEmpty(MyApplication.getUsername())) {
            config.setNickname(StringUtils.hideLoginName(MyApplication.getUsername()));
        }*/
        config.setNickname(MyApplication.getUsername());
//        config.setNickname("android_sdk_test"); // 设置用户昵称
        config.setGender(0); // 设置用户性别: 0-未知  1-男  2-女
        // 设置用户头像URL
        //config.setAvatar("http://debugimg-10013434.image.myqcloud.com/fe1382d100019cfb572b1934af3d2c04/thumbnail");
        config.setVip(0); // 设置用户VIP等级（0-5）
        /**
         *【建议】设置用户OpenId，以识别不同登录用户，不设置则默认由SDK生成，替代v1.2.0之前的uid,
         *  openId将透传到座席端(建议使用含字母数字和下划线的字符串，尽量不用特殊字符，若含特殊字符系统会进行URL encode处理)
         *	若您是旧版本SDK用户，只是想升级，为兼容旧版，避免客户信息改变可继续使用config.setUid，可不用openId
         */
        config.setOpenId(MyApplication.getUserId());
        //config.setUid(uid); //【弃用】请使用setOpenId替代
        // 设置device_token：集成第三方推送(腾讯信鸽、百度云推)时设置此参数以在离开会话界面时接收推送消息
        //config.setDeviceToken(XGPushConfig.getToken(getApplicationContext())); // 【建议】设置deviceToken

        // 客户信息键值对（JSONObject）
        JSONObject customContent = new JSONObject();
        try {
            customContent.put("APP名称", AppHelper.getAppName(MyApplication.getContext()));
            customContent.put("APP本版号", AppHelper.getVerName(MyApplication.getContext()));
            customContent.put("安卓系统版本", AppHelper.getAndroidSDKVersionName());
            customContent.put("手机型号", Build.BRAND + ";" + Build.MODEL);
            if (!TextUtils.isEmpty(AppHelper.getIPAddress(MyApplication.getContext())))
                customContent.put("IP", AppHelper.getIPAddress(MyApplication.getContext()));
            customContent.put("APP_CHANNEL", AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // 设置客户信息（自定义JSONObjectjian键值对，开启会话前设置）
        config.setUserInfo(customContent);
    }

    private void startChatActivity() {
        /* 开启会话界面 */
        // 可用Bundle传递以下参数
        Bundle bundle = new Bundle();
        bundle.putInt("numOfMessagesOnRefresh", 10);    // 下拉刷新数量，默认为10
        bundle.putInt("numOfMessagesOnOpen", 10);        // 开场显示历史消息数量，默认为10
        bundle.putBoolean("enableVoice", true);            // 是否允许发送语音
        bundle.putBoolean("showAvatar", true);            // 是否显示对话双方的头像
        /*
         * 设置开场白模式，默认为clientOpenModeDefault，可根据客服启动场景设置开场问题
         * clientOpenModeDefault	// 默认开场白方式（无历史消息显示则显示开场白，优先以设置的param字符串为开场白，param为null则使用后台配置的开场白）
         * clientOpenModeQuestion	// 自定义问题开场白，param字符串为问题内容（不为空），设置开场问题获得对应开场白（此模式不可与优先人工客服同用，否则将失效）
         * clientOpenModeNone		// 无开场白方式，仅显示历史消息
         * clientOpenModeAutoHuman  // 开场自动转人工客服
         */
        bundle.putInt("clientOpenMode", ClientOpenMode.clientOpenModeDefault.ordinal());
        //bundle.putString("clientOpenParam", "您好，请问有什么需要帮助的吗？");

        //Context context = getApplicationContext();
        //Intent chatIntent = new Intent(context, ClientChatActivity.class);
        //chatIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //chatIntent.putExtras(bundle);
        //context.startActivity(chatIntent);
        // 进入会话界面(可使用上面的方式或者调用下面方法)，携带bundle(不加bundle参数则全部使用默认配置)
        V5ClientAgent.getInstance().startV5ChatActivityWithBundle(MyApplication.getContext(), bundle);

        /* 添加聊天界面监听器(非必须，有相应需求则添加) */
        // 界面生命周期监听[非必须]
        V5ClientAgent.getInstance().setChatActivityListener(UserFragment.this);
        // 消息发送监听[非必须]，可在此处向坐席透传来自APP客户的相关信息
        V5ClientAgent.getInstance().setUserWillSendMessageListener(new UserWillSendMessageListener() {

            @Override
            public V5Message onUserWillSendMessage(V5Message message) {
                // TODO 可在此处添加消息参数(JSONObject键值对均为字符串)，采集信息透传到坐席端（v1.2.0版本开始不建议使用此方式，除非有实时更新需求的自定义信息）
                // 【注意】v1.2.0以上版本建议使用V5ClientConfig的setUserInfo方法传递客户信息，可不必依附于消息
                //if (flag_userBrowseSomething) {
                //    JSONObject customContent = new JSONObject();
                //    try {
                //        customContent.put("用户级别", "VIP");
                //        customContent.put("用户积分", "300");
                //        customContent.put("来自应用", "ClientDemo");
                //    } catch (JSONException e) {
                //        e.printStackTrace();
                //    }
                //    message.setCustom_content(customContent);
                //
                //    flag_userBrowseSomething = false;
                //}
                return message; // 注：必须将消息对象以返回值返回
            }
        });

        /**
         * 点击链接监听
         * onURLClick返回值：是否消费了此点击事件，返回true则SDK内不再处理此事件，否则默认跳转到指定网页
         */
        V5ClientAgent.getInstance().setURLClickListener(new OnURLClickListener() {

            @Override
            public boolean onURLClick(Context context, V5ClientAgent.ClientLinkType type, String url) {
                // TODO Auto-generated method stub
                switch (type) {
                    case clientLinkTypeArticle: // 点击图文

                        break;
                    case clientLinkTypeURL: // 点击URL链接

                        break;

                    case clientLinkTypeEmail: // 点击电子邮件

                        break;

                    case clientLinkTypePhoneNumber: // 点击电话号码

                        break;
                }
                LogUtil.d("onURLClick:" + url);
                return false; // 是否消费了此点击事件
            }
        });

        /**
         * 点击对话输入框底部功能按钮
         */
        V5ClientAgent.getInstance().setChatActivityFuncIconClickListener(new ChatActivityFuncIconClickListener() {

            /**
             * Activity点击底部功能按钮事件，icon参数值及含义如下：
             * 		v5_icon_ques			//常见问题
             * 		v5_icon_relative_ques	//相关问题
             * 		v5_icon_photo			//图片
             * 		v5_icon_camera			//拍照
             * 		v5_icon_worker			//人工客服
             * 返回值代表是否消费了此事件
             * @param icon 点击的图标名称(对应SDK目录下res/values/v5_arrays中v5_chat_func_icon的值)
             * @return boolean 是否消费事件(返回true则不响应默认点击效果，由此回调处理)
             */
            @Override
            public boolean onChatActivityFuncIconClick(String icon) {
                // TODO Auto-generated method stub
                if (icon.equals("v5_icon_worker")) {
                    // 转到指定客服,参数：(组id, 客服id),参数为0则不指定客服组或者客服,获取组id请咨询客服
                    V5ClientAgent.getInstance().transferHumanService(0, 0);
                    // 返回true来拦截SDK内默认的实现
                    return true;
                }
                return false;
            }
        });
    }

    private void choice() {
        final String status = Environment.getExternalStorageState();
        SelectPicPopupWindow picPopupWindow = new SelectPicPopupWindow(getActivity(), new SelectPicPopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    try {
                        startCamrae();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                try {
                    if (status.equals(Environment.MEDIA_MOUNTED)) {
                        takeImg();
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.tx_sd_pic));
                    }
                } catch (Exception e) {

                }
            }
        });

        picPopupWindow.showAtLocation(mIvUserPhoto, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void takeImg() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }

    String imageUrl;

    private void startCamrae() throws Exception {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String root = AppHelper.getImgCacheDir();
        imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";

        tempFile = new File(imageUrl);
        if (!tempFile.getParentFile().exists()) {
            tempFile.getParentFile().mkdirs();
        }
        if (Build.VERSION.SDK_INT >= 24) {
            //如果是则使用FileProvider
            originalUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileProvider", tempFile);
        } else {
            originalUri = Uri.fromFile(tempFile);
        }
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                //获取相册图片
                case REQUEST_CODE_TAKE_PICTURE:
                    Uri uri = data.getData();
                    gotoClipActivity(uri);
                    break;

                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    gotoClipActivity(Uri.fromFile(tempFile));
                    break;
                case REQUEST_CROP_PHOTO:  //剪切图片返回
                    if (resultCode == RESULT_OK) {
                        final Uri clipUri = data.getData();
                        if (clipUri == null) {
                            return;
                        }
                        String cropImagePath = AppHelper.getPicPath(clipUri);
                        Bitmap bitMap = BitmapFactory.decodeFile(cropImagePath);
                        /*if (type == 1) {
                            mIvUserPhoto.setImageBitmap(bitMap);
                        } else {*/
                        mIvUserPhoto.setImageBitmap(bitMap);
//                        }
                        //此处后面可以将bitMap转为二进制上传后台网络
                        //......

                    }
                    break;
            }
        }
    }

    /**
     * 打开截图界面
     */
    public void gotoClipActivity(Uri uri) {
        if (uri == null) {
            return;
        }
        Intent intent = new Intent();
        intent.setClass(getActivity(), ClipImageActivity.class);
        intent.putExtra(Constants.INTENT_CLIP_TYPE, type);
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_CROP_PHOTO);
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_UNREAD_NOTICE)) {
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg != null && !TextUtils.isEmpty(msg.getData())) {
                        if ("yes".equals(msg.getData())) {
                            mIvNoticeRed.setVisibility(View.VISIBLE);
                            ((MainActivity) getActivity()).showRedDot(true);
                        } else {
                            mIvNoticeRed.setVisibility(View.GONE);
                            ((MainActivity) getActivity()).showRedDot(false);
                        }
                    } else {
                        mIvNoticeRed.setVisibility(View.GONE);
                        ((MainActivity) getActivity()).showRedDot(false);
                    }
                    break;
            }
        }
    }

    /* 会话界面生命周期和连接状态的回调 */
    @Override
    public void onChatActivityCreate(ClientChatActivity activity) {
        // TODO Auto-generated method stub
        LogUtil.d("<onChatActivityCreate>");
        activity.setChatTitle("客服");
    }

    @Override
    public void onChatActivityStart(ClientChatActivity activity) {
        // TODO Auto-generated method stub
        LogUtil.d("<onChatActivityStart>");
    }

    @Override
    public void onChatActivityStop(ClientChatActivity activity) {
        // TODO Auto-generated method stub
        LogUtil.d("<onChatActivityStop>");
    }

    @Override
    public void onChatActivityDestroy(ClientChatActivity activity) {
        // TODO Auto-generated method stub
        LogUtil.d("<onChatActivityDestroy>");
    }

    @Override
    public void onChatActivityConnect(ClientChatActivity activity) {
        // TODO Auto-generated method stub
        LogUtil.d("<onChatActivityConnect>");
        /*
         * 连接建立后才可以调用消息接口发送消息，以下是发送消息示例
         */
        // 【转】指定人工客服（调用时立即转），参数: 客服组id,客服id （以下数字仅作为示例，具体ID请前往V5后台查看客服信息）
        //V5ClientAgent.getInstance().transferHumanService(0, 132916);
        // 【指定人工客服】点击转人工按钮或者问题触发转人工时会转到指定人工，参数"0 132916"中两个数字先后对应需要转的客服组ID和客服ID
        //V5ClientAgent.getInstance().sendMessage(new V5ControlMessage(4, 2, "0 114052"), null);

        // 发送图文消息
//		V5ArticlesMessage articleMsg = new V5ArticlesMessage();
//		V5ArticleBean article = new V5ArticleBean(
//				"V5KF",
//				"http://rs.v5kf.com/upload/10000/14568171024.png",
//				"http://www.v5kf.com/public/weixin/page.html?site_id=10000&id=218833&uid=3657455033351629359",
//				"V5KF是围绕核心技术“V5智能机器人”研发的高品质在线客服系统。可以运用到各种领域，目前的主要产品有：微信智能云平台、网页智能客服系统...");
//		ArrayList<V5ArticleBean> articlesList = new ArrayList<V5ArticleBean>();
//		articlesList.add(article);
//		articleMsg.setArticles(articlesList);
//		V5ClientAgent.getInstance().sendMessage(articleMsg, null);
    }

    @Override
    public void onChatActivityReceiveMessage(ClientChatActivity activity, V5Message message) {
        // TODO Auto-generated method stub
        LogUtil.d("<onChatActivityReceiveMessage> " + message.getDefaultContent(getActivity()));
    }

    @Override
    public void onChatActivityServingStatusChange(ClientChatActivity activity, V5ClientAgent.ClientServingStatus status) {
        // TODO Auto-generated method stub
        switch (status) {
            case clientServingStatusRobot:
            case clientServingStatusQueue:
                activity.setChatTitle("机器人服务中");
                break;
            case clientServingStatusWorker:
                activity.setChatTitle(V5ClientConfig.getInstance(getActivity()).getWorkerName() + "为您服务");
                break;
            case clientServingStatusInTrust:
                activity.setChatTitle("机器人托管中");
                break;
        }
    }
}
