package com.hstypay.hstysales.fragment;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.MerchantSearchActivity;
import com.hstypay.hstysales.adapter.SalesmanAdapter;
import com.hstypay.hstysales.adapter.ServiceProviderAdapter;
import com.hstypay.hstysales.adapter.SimpleFragmentPagerAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.base.BaseFragment;
import com.hstypay.hstysales.base.BaseMerchantFragment;
import com.hstypay.hstysales.bean.SalesmanBean;
import com.hstypay.hstysales.bean.ServiceProviderBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.SafeDialog;
import com.tencent.stat.StatService;
import com.ykk.dropdownmenu.DropDownMenu;
import com.ykk.dropdownmenu.TabBean;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/11/21 15:58
 * @描述: ${TODO}
 */

public class MerchantFragment extends BaseFragment implements View.OnClickListener {
    private View mView;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private TextView mEtSearch;
    private RelativeLayout mRlTop;
    private SimpleFragmentPagerAdapter mFragmentAdapteradapter;
    private List<BaseMerchantFragment> mFragments;
    private DropDownMenu mDropDownMenu;
    private ArrayList<TabBean> headers;
    private SafeDialog mLoadDialog;
    private int mSearchPageSize = 15;
    private int mSearchCurrentPage = 1;
    private boolean isLoadMerchantMore;
    private boolean isLoadOver;
    private int mSelectLabelPosition;
    private ServiceProviderAdapter mServiceProviderAdapter;
    private SalesmanAdapter mSalesmanAdapter;
    private String mCurSelectServiceProviderId;
    private String mCurSelectSalesmanId;
    private List<SalesmanBean.DataEntity.Salesman> mSearchSalesmanList;//搜索到的城市经理列表数据
    private List<ServiceProviderBean.DataEntity.ServiceProvider> mSearchServiceProviderList;//搜索到的市运营列表数据
    private ServiceProviderBean.DataEntity.ServiceProvider mAllServiceProvider;
    private SalesmanBean.DataEntity.Salesman mAllSalesman;
    private int mTabPosition;
    private EditText mEtServiceInput;
    private ImageView mIvServiceClean;
    private View mServiceDataEmpty;
    private EditText mEtSalesmanInput;
    private ImageView mIvSalesmanClean;
    private View mSalesmanDataEmpty;

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mView = inflater.inflate(R.layout.fragment_merchant, container, false);
        initView(mView);
        initData();
        initListener();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onFragmentVisible() {
        super.onFragmentVisible();
        StatusBarUtil.setTranslucentStatus(getActivity());
    }

    private void initView(View view) {
        mLoadDialog = ((BaseActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);

        mRlTop = (RelativeLayout) view.findViewById(R.id.rl_top);
        mEtSearch = (TextView) view.findViewById(R.id.et_search);

        mDropDownMenu = view.findViewById(R.id.dropDownMenu);
    }

    private void initListener() {
        mTabLayout.setOnClickListener(this);
        mViewPager.setOnClickListener(this);
        mRlTop.setOnClickListener(this);
    }

    private void initData() {
        initDropDownMenu();
        initViewPager();
    }

    private void initDropDownMenu() {
        mCurSelectServiceProviderId = "";
        mCurSelectSalesmanId = "";
        mSearchServiceProviderList = new ArrayList<>();
        mSearchSalesmanList = new ArrayList<>();

        mAllServiceProvider = new ServiceProviderBean.DataEntity.ServiceProvider();//"全部商户"
        mAllServiceProvider.setServiceProviderName("全部市运营");
        mAllServiceProvider.setServiceProviderId("");

        mAllSalesman = new SalesmanBean.DataEntity.Salesman();//"全部商户"
        mAllSalesman.setEmpName("全部城市经理");
        mAllSalesman.setEmpId("");

        View contentView = getLayoutInflater().inflate(R.layout.layout_content_fragment_merchant, null);
        mViewPager = (ViewPager) contentView.findViewById(R.id.viewpager_merchant);
        mTabLayout = (TabLayout) contentView.findViewById(R.id.tabs_merchant);

        headers = new ArrayList<>();
        TabBean tabBean2 = new TabBean();
        tabBean2.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean2.setLabel("cityService");
        tabBean2.setTabTitle("全部市运营");
        TabBean tabBean3 = new TabBean();
        tabBean3.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean3.setLabel("cityManager");
        tabBean3.setTabTitle("全部城市经理");
        if ("SUPERADMIN".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_ROLE_CODE))) {
            if ("".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean2);
                headers.add(tabBean3);
            } else if ("SC".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean3);
            }
        }
        mDropDownMenu.setDropDownMenu(headers, initViewData(), contentView);
        mDropDownMenu.setOnTabClickListener(new DropDownMenu.OnTabClickListener() {
            @Override
            public void onTabClick(int position, boolean isOpen) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
                mSelectLabelPosition = position;
                switch (headers.get(position).getLabel()) {
                    case "cityService":
                        if (isOpen)
                            getServiceList(false);
                        break;
                    case "cityManager":
                        if (isOpen)
                            getSalesmanList(false);
                        break;
                }
            }
        });
    }

    /**
     * 设置类型和数据源：
     * DropDownMenu.KEY对应类型（DropDownMenu中的常量，参考上述核心源码）
     * DropDownMenu.VALUE对应数据源：key不是TYPE_CUSTOM则传递string[],key是TYPE_CUSTOM类型则传递对应view
     */
    private List<HashMap<String, Object>> initViewData() {
        List<HashMap<String, Object>> viewDatas = new ArrayList<>();
        HashMap<String, Object> map;
        for (int i = 0; i < headers.size(); i++) {
            map = new HashMap<String, Object>();
            map.put(DropDownMenu.KEY, headers.get(i).getType());
            map.put(DropDownMenu.VALUE, getCustomView(headers.get(i).getLabel()));
            viewDatas.add(map);
        }
        return viewDatas;
    }

    private View getCustomView(String label) {
        View view = getLayoutInflater().inflate(R.layout.layout_dropdown, null);
        switch (label) {
            case "cityService":
                mEtServiceInput = view.findViewById(R.id.et_input);
                mIvServiceClean = view.findViewById(R.id.iv_clean);
                mServiceDataEmpty = view.findViewById(R.id.common_data_empty);
                RecyclerView rvService = view.findViewById(R.id.recycler_view);
                LinearLayoutManager serviceLinearLayoutManager = new LinearLayoutManager(getActivity());
                rvService.setLayoutManager(serviceLinearLayoutManager);
                mServiceProviderAdapter = new ServiceProviderAdapter(getActivity());
                mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                mServiceProviderAdapter.setOnItemClickListener(new ServiceProviderAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mCurSelectServiceProviderId != null && !mCurSelectServiceProviderId.equals(mSearchServiceProviderList.get(position).getServiceProviderId())) {
                            mCurSelectSalesmanId = "";
                            for (int i = 0; i < headers.size(); i++) {
                                if ("cityManager".equals(headers.get(i).getLabel()))
                                    mDropDownMenu.setTabText(i, "全部城市经理");
                            }
                            mCurSelectServiceProviderId = mSearchServiceProviderList.get(position).getServiceProviderId();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mDropDownMenu.setTabText(mSelectLabelPosition, mSearchServiceProviderList.get(position).getServiceProviderName());
                            mFragments.get(mTabPosition).setFilter(mCurSelectServiceProviderId, mCurSelectSalesmanId);
                            mFragments.get(mTabPosition).refreshData();
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvService.setAdapter(mServiceProviderAdapter);
                rvService.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    //这个int用来记录最后一个可见的view
                    int lastVisibleItemPosition;
                    int refreshCurrentPage;//防止重复加载

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mServiceProviderAdapter.getItemCount()) {
                            refreshCurrentPage = mSearchCurrentPage;
                            if (!isLoadOver)
                                getServiceList(true);
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                        lastVisibleItemPosition = serviceLinearLayoutManager.findLastVisibleItemPosition();
                    }
                });
                mEtServiceInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            getServiceList(false);
                            return true;
                        }
                        return false;
                    }
                });
                mEtServiceInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            getServiceList(false);
                        }
                        mIvServiceClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                mIvServiceClean.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEtServiceInput.setText("");
                    }
                });
                break;
            case "cityManager":
                mEtSalesmanInput = view.findViewById(R.id.et_input);
                mIvSalesmanClean = view.findViewById(R.id.iv_clean);
                mSalesmanDataEmpty = view.findViewById(R.id.common_data_empty);
                RecyclerView rvSalesman = view.findViewById(R.id.recycler_view);
                LinearLayoutManager salesmanLinearLayoutManager = new LinearLayoutManager(getActivity());
                rvSalesman.setLayoutManager(salesmanLinearLayoutManager);
                mSalesmanAdapter = new SalesmanAdapter(getActivity());
                mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                mSalesmanAdapter.setOnItemClickListener(new SalesmanAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mCurSelectSalesmanId != null && !mCurSelectSalesmanId.equals(mSearchSalesmanList.get(position).getEmpId())) {
                            mCurSelectSalesmanId = mSearchSalesmanList.get(position).getEmpId();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mDropDownMenu.setTabText(mSelectLabelPosition, mSearchSalesmanList.get(position).getEmpName());
                            mFragments.get(mTabPosition).setFilter(mCurSelectServiceProviderId, mCurSelectSalesmanId);
                            mFragments.get(mTabPosition).refreshData();
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvSalesman.setAdapter(mSalesmanAdapter);
                rvSalesman.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    //这个int用来记录最后一个可见的view
                    int lastVisibleItemPosition;
                    int refreshCurrentPage;//防止重复加载

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mSalesmanAdapter.getItemCount()) {
                            refreshCurrentPage = mSearchCurrentPage;
                            if (!isLoadOver)
                                getSalesmanList(true);
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                        lastVisibleItemPosition = salesmanLinearLayoutManager.findLastVisibleItemPosition();
                    }
                });
                mEtSalesmanInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            getSalesmanList(false);
                            return true;
                        }
                        return false;
                    }
                });
                mEtSalesmanInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            getSalesmanList(false);
                        }
                        mIvSalesmanClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                mIvSalesmanClean.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEtSalesmanInput.setText("");
                    }
                });
                break;
        }
        return view;
    }

    private void initViewPager() {
        List<String> titles = new ArrayList<>();
        titles.add(getString(R.string.merchant_trade_enable));
        titles.add(getResources().getString(R.string.merchant_not_confirm));
        titles.add(getString(R.string.merchant_checking));
        titles.add(getString(R.string.merchant_check_success));
        titles.add(getString(R.string.merchant_check_failed));
        titles.add(getString(R.string.merchant_change_checking));
        titles.add(getString(R.string.merchant_change_check_success));
        titles.add(getString(R.string.merchant_change_check_failed));
        titles.add(getString(R.string.merchant_no_info));
        titles.add(getString(R.string.merchant_freeze));
        titles.add(getString(R.string.merchant_become_invalid));

        for (int i = 0; i < titles.size(); i++) {
            mTabLayout.addTab(mTabLayout.newTab().setText(titles.get(i)));
        }
        mFragments = new ArrayList<>();
        NoInfoFragment noInfoFragment = new NoInfoFragment();
        CheckingFragment checkingFragment = new CheckingFragment();
        mFragments.add(new BusinessFragment());
        mFragments.add(new CheckNotConfirmFragment());//待确认
        mFragments.add(checkingFragment);
        mFragments.add(new SuccessFragment());
        mFragments.add(new FailedFragment());
        mFragments.add(new ChangeFragment());
        mFragments.add(new ChangePassFragment());
        mFragments.add(new ChangeFailFragment());
        mFragments.add(noInfoFragment);
        mFragments.add(new FreezeFragment());
        mFragments.add(new InvalidFragment());
        mFragmentAdapteradapter = new SimpleFragmentPagerAdapter(getActivity(), getChildFragmentManager(), mFragments, titles);
//        FragmentAdapter mFragmentAdapteradapter = new FragmentAdapter(getChildFragmentManager(), fragments, titles);
        //给ViewPager设置适配器
        mViewPager.setAdapter(mFragmentAdapteradapter);
        //将TabLayout和ViewPager关联起来。
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(mFragments.size());
        //给TabLayout设置适配器
        mTabLayout.setTabsFromPagerAdapter(mFragmentAdapteradapter);
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                LogUtil.d("tab======" + tab.getPosition());
                mTabPosition = tab.getPosition();
                mEtSearch.setHint(tab.getPosition() == 8 ? R.string.et_hint_telephone_search : R.string.et_hint_search_merchant);
                mFragments.get(tab.getPosition()).setFilter(mCurSelectServiceProviderId, mCurSelectSalesmanId);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setUpTabBadge();
      /*  checkingFragment.setOnRedStateChangeListener(new CheckingFragment().OnRedStateChangeListener() {
            @Override
            public void redStateChange() {
                setUpTabBadge();
            }
        });*/

        checkingFragment.setOnRedStateChangeListener(new CheckingFragment.OnRedStateChangeListener() {
            @Override
            public void redStateChange() {
                setUpTabBadge();
            }
        });

    }

    /**
     * 设置Tablayout上的标题的角标
     */
    private void setUpTabBadge() {
        // 2. 最实用
        for (int i = 0; i < mFragments.size(); i++) {
            TabLayout.Tab tab = mTabLayout.getTabAt(i);

            // 更新Badge前,先remove原来的customView,否则Badge无法更新
            View customView = tab.getCustomView();
            if (customView != null) {
                ViewParent parent = customView.getParent();
                if (parent != null) {
                    ((ViewGroup) parent).removeView(customView);
                }
            }

            // 更新CustomView
            tab.setCustomView(mFragmentAdapteradapter.getTabItemView(i, MyApplication.hasNoInfo()));
        }
        // 需加上以下代码,不然会出现更新Tab角标后,选中的Tab字体颜色不是选中状态的颜色
        mTabLayout.getTabAt(mTabLayout.getSelectedTabPosition()).getCustomView().setSelected(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_top:
                StatService.trackCustomKVEvent(getActivity(), "1027", null);
                getActivity().startActivity(new Intent(getActivity(), MerchantSearchActivity.class));
                break;
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setUpTabBadge();
        }
    }

    private void getServiceList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                map.put("serviceProviderName", mEtServiceInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getServiceList(MyApplication.getContext(), "TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getSalesmanList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                map.put("empName", mEtSalesmanInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getSalesmanList(MyApplication.getContext(), "TAG_SALESMAN_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals("TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ServiceProviderBean msg = (ServiceProviderBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mServiceDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchServiceProviderList.clear();
                            if (TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                                mSearchServiceProviderList.add(mAllServiceProvider);
                            }
                        }

                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchServiceProviderList.addAll(msg.getData().getData());
                        mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchServiceProviderList.clear();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mServiceDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mServiceDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_SALESMAN_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SalesmanBean msg = (SalesmanBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mSalesmanDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchSalesmanList.clear();
                            if (TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                                mSearchSalesmanList.add(mAllSalesman);
                            }
                        }
                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchSalesmanList.addAll(msg.getData().getData());
                        mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchSalesmanList.clear();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mSalesmanDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mSalesmanDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        }
    }
}
