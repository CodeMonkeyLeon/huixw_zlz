package com.hstypay.hstysales.fragment.devicemall.search;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.hstypay.hstysales.bean.DeviceOrderListBean;
import com.hstypay.hstysales.fragment.devicemall.BaseDeviceMallFragment;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.ToastUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/8/27
 * @desc 搜索结果页
 */
public class DeviceSearchFragment extends BaseDeviceMallFragment {

    public static final String KEY_BUNDLE_ORDERS = "key_bundle_orders";//从宿主页面传过来的订单数据的key
    public static final String KEY_BUNDLE_SEARCH_PARAM = "key_bundle_search_param";//从宿主页面传过来的搜索词的key
    public static final String KEY_BUNDLE_TOTAL_COUNT = "key_bundle_total_count";//从宿主页面传过来的搜索结果订单总条数的key

    @Override
    protected String getTradeStatusField() {
        return ""+ Constants.DEVICE_MALL_SEARCH;
    }

    @Override
    protected boolean isHomeFragment() {
        return false;
    }

    @Override
    protected boolean isNeedRefreshWhenVisible() {
        return false;
    }

    @Override
    protected void setNeedRefreshWhenVisible(boolean needRefresh) {

    }

    @Override
    protected void initView() {
        super.initView();
        mViewBgSearchResult.setVisibility(View.VISIBLE);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) mRecyclerViewOrderItem.getLayoutParams();
        layoutParams.topMargin = DensityUtils.dip2px(mContext,15);
    }

    /*
    * 在主页搜索后，初始化搜索页数据
    * */
    @Override
    protected void initData() {
        super.initData();
        Bundle bundle = getArguments();
        if (bundle!=null){
            mSearchParam = bundle.getString(KEY_BUNDLE_SEARCH_PARAM);
            String totalSearchResult = bundle.getString(KEY_BUNDLE_TOTAL_COUNT);
            ArrayList<DeviceOrderListBean.DataEntity.DataBean> orderDataList = (ArrayList<DeviceOrderListBean.DataEntity.DataBean>) bundle.getSerializable(KEY_BUNDLE_ORDERS);
            setSearchDataView(orderDataList,totalSearchResult);
        }else {
            mRlCountSearchTitle.setVisibility(View.GONE);
            mLlEmptyDeviceMallOrder.setVisibility(View.VISIBLE);
            mSwipeRefreshLayoutMall.setVisibility(View.GONE);
        }

    }

    /*
     * 在搜索页搜索后（其实还是在主页，只是此时主页展示的是DeviceSearchFragment），刷新搜索页数据
     * */
    public void refreshData(ArrayList<DeviceOrderListBean.DataEntity.DataBean> orderDataList, String searchKey,String totalSearchResult) {
        mSearchParam = searchKey;
        setSearchDataView(orderDataList, totalSearchResult);
    }

    private void setSearchDataView(ArrayList<DeviceOrderListBean.DataEntity.DataBean> orderDataList, String totalSearchResult) {
        if (orderDataList!=null && orderDataList.size()>0){
            mRlCountSearchTitle.setVisibility(View.VISIBLE);
            mLlEmptyDeviceMallOrder.setVisibility(View.GONE);
            mSwipeRefreshLayoutMall.setVisibility(View.VISIBLE);
            mTvSearchWordTitle.setText("搜索: "+mSearchParam);
            String htmlTotalCount = "共有<font  color='#4272EE'>"+totalSearchResult+"</font>个订单";
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                mTvTotalCountSearchTitle.setText(Html.fromHtml(htmlTotalCount,Html.FROM_HTML_MODE_LEGACY));
            } else {
                mTvTotalCountSearchTitle.setText(Html.fromHtml(htmlTotalCount));
            }
            mCurrentPage = 1;
            mOrderDataList.clear();
            mOrderDataList.addAll(orderDataList);
            mAdapter.setOrderList(mOrderDataList);
        }else {
            mRlCountSearchTitle.setVisibility(View.GONE);
            mLlEmptyDeviceMallOrder.setVisibility(View.VISIBLE);
            mSwipeRefreshLayoutMall.setVisibility(View.GONE);
        }
    }
}
