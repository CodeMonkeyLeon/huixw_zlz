package com.hstypay.hstysales.fragment;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.MainActivity;
import com.hstypay.hstysales.adapter.RankFragmentAdapter;
import com.hstypay.hstysales.adapter.SalesmanAdapter;
import com.hstypay.hstysales.adapter.ServiceProviderAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.base.BaseFragment;
import com.hstypay.hstysales.bean.RankListBean;
import com.hstypay.hstysales.bean.RankSumaryBean;
import com.hstypay.hstysales.bean.SalesmanBean;
import com.hstypay.hstysales.bean.ServiceProviderBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.TimePickViewHelp;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.viewholder.RankSummaryViewHolder;
import com.hstypay.hstysales.widget.EditTextDelete;
import com.hstypay.hstysales.widget.LinearSpaceItemDecoration;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/11/21 15:58
 * @描述: ${TODO}
 */

public class RankFragment extends BaseFragment implements View.OnClickListener {

    private View mView;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView recyclerview;
    private Context mContex;
    private RankFragmentAdapter rankFragmentAdapter;

    private int mPageSize = 15;
    private int mCurrentPage = 1;
    private boolean isLoadmore;//加载更多标识
    private boolean isPullRefresh;//下拉刷新标识
    private List<RankListBean.SummaryData> mSummaryDataList = new ArrayList<>();

    private SafeDialog mLoadDialog;
    private int mOrderType;//排序类型 1：交易净额，2：交易笔数，3：商户注册时间  默认交易金额
    private int mOrderRule;//排序规则 1：顺序（数量由小到大，时间由远到近） 2：倒序（数量由大到小，时间由近到远） 不传则默认：时间是顺序，数量是倒叙

    private Calendar mCurSelectEndDate;
    private Calendar mCurSelectStartDate;
    private TimePickViewHelp mTimePickViewHelp;
    private String timeFormatType = "yyyy-MM-dd HH:mm:ss";
    private ArrayList<ServiceProviderBean.DataEntity.ServiceProvider> mServiceProviderList;
    private ArrayList<SalesmanBean.DataEntity.Salesman> mSalesmanList;
    private String mServiceProviderId;
    private ServiceProviderBean.DataEntity.ServiceProvider mAllServiceProvider;
    private String mSalesmanId;
    private SalesmanBean.DataEntity.Salesman mAllSalesman;
    private RelativeLayout mRlService;
    private TextView mTvServiceName;
    private ImageView mIvArrowService;
    private ServiceProviderAdapter mServiceProviderAdapter;
    private RelativeLayout mRlSalesman;
    private TextView mTvSalesman;
    private ImageView mIvArrowSalesman;
    private SalesmanAdapter mSalesmanAdapter;
    private LinearLayout mPopLayout;
    private EditTextDelete mEtInput;
    private RecyclerView mRecyclerViewFilter;
    private LinearLayoutManager mLinearLayoutManager;
    private boolean mDateSwitchArrow;
    private boolean mServiceSwitchArrow;
    private boolean mSalesmanSwitchArrow;
    private View mCommonDataEmpty;
    private static final int mMerchantPageSize = 15;
    private int mMerchantCurrentPage = 1;
    private boolean isLoadOver;
    private boolean isLoadMerchantMore;//加载更多标识
    private TextView mTvDataStart;
    private TextView mTvDataEnd;
    private ImageView mIvArrowReport;
    private LinearLayout mLlSelectDate;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContex = context;
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mView = inflater.inflate(R.layout.fragment_rank, container, false);
        initView();
        initData();
        initEvent();
        return mView;
    }

    private void initEvent() {
        mLlSelectDate.setOnClickListener(this);
        mRlService.setOnClickListener(this);
        mRlSalesman.setOnClickListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    mCurrentPage = 1;
                    isPullRefresh = true;
                    getRankSumaryData();
                    getRankListData();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getRankListData();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });

        rankFragmentAdapter.setOnRankHeaderContentClickListener(new RankSummaryViewHolder.OnRankHeaderContentClickListener() {
            @Override
            public void onClickHeaderSelectDate(boolean isHeaderDateExpand, RankSummaryViewHolder.CallBackRankHeaderClick callBackRankHeaderClick) {
                //选择日期
                if (isHeaderDateExpand){
                    mTimePickViewHelp.dismiss();
                    callBackRankHeaderClick.backSelectDate(null);//null 使箭头朝下
                }else {
                    mTimePickViewHelp.show(new TimePickViewHelp.CallBackDataListener() {
                        @Override
                        public void onCancel() {
                            callBackRankHeaderClick.backSelectDate(null);
                        }

                        @Override
                        public void onSubmit(Calendar curSelectStartDate, Calendar curSelectEndDate) {
                            String startDateStr = mTimePickViewHelp.getDateString(curSelectStartDate, "yyyy/MM/dd");
                            String endDateStr = mTimePickViewHelp.getDateString(curSelectEndDate, "yyyy/MM/dd");
                            callBackRankHeaderClick.backSelectDate(startDateStr+"～"+endDateStr);
                            mTvDataStart.setText(startDateStr);
                            mTvDataEnd.setText(startDateStr);
                            mTimePickViewHelp.dismiss();
                            mCurSelectStartDate = curSelectStartDate;
                            mCurSelectEndDate = curSelectEndDate;
                            mCurrentPage = 1;
                            getRankSumaryData();
                            getRankListData();
                        }
                    });
                }
            }

            @Override
            public void onClickDkjQuestion() {
                //单客价，大额笔数提示
                ((BaseActivity)getActivity()).showCommonNoticeDialog(getActivity(),getResources().getString(R.string.title_dialog_amount_count),getResources().getString(R.string.content_dialog_amount_count),null);
            }

            @Override
            public void onClickExpandMerchant() {
                //展开或关闭预览商户数
            }

            @Override
            public void onClickHyshQuestion() {
                //活跃商户
                ((BaseActivity)getActivity()).showCommonNoticeDialog(getActivity(),getResources().getString(R.string.title_dialog_hysh),getResources().getString(R.string.content_dialog_hysh),null);
            }

            @Override
            public void onClickNotHyshQuestion() {
                //非活跃商户
                ((BaseActivity)getActivity()).showCommonNoticeDialog(getActivity(),getResources().getString(R.string.title_dialog_not_hysh),getResources().getString(R.string.content_dialog_not_hysh),null);
            }

            @Override
            public void onClickJyjeRank(RankSummaryViewHolder.ArrowType arrowType) {
                //点击交易净额
                mOrderType = 1;
                if (arrowType == RankSummaryViewHolder.ArrowType.ARROWDOWN){
                    //箭头向下是1
                    mOrderRule = 1;
                }else if (arrowType == RankSummaryViewHolder.ArrowType.ARROWUP){
                    //箭头向上是2
                    mOrderRule = 2;
                }
                mCurrentPage = 1;
                getRankListData();
            }

            @Override
            public void onClickJybsRank(RankSummaryViewHolder.ArrowType arrowType) {
                //点击交易笔数
                mOrderType = 2;
                if (arrowType == RankSummaryViewHolder.ArrowType.ARROWDOWN){
                    mOrderRule = 1;
                }else if (arrowType == RankSummaryViewHolder.ArrowType.ARROWUP){
                    mOrderRule = 2;
                }
                mCurrentPage = 1;
                getRankListData();
            }

            @Override
            public void onClickCjsjRank(RankSummaryViewHolder.ArrowType arrowType) {
                //点击创建时间
                mOrderType = 3;
                if (arrowType == RankSummaryViewHolder.ArrowType.ARROWDOWN){
                    mOrderRule = 1;
                }else if (arrowType == RankSummaryViewHolder.ArrowType.ARROWUP){
                    mOrderRule = 2;
                }
                mCurrentPage = 1;
                getRankListData();
            }

            @Override
            public void onClickTelPhone(String telPhone) {
                //打电话
                AppHelper.call(telPhone,mContex);
            }
        });
    }


    private void initData() {
        mServiceProviderList = new ArrayList<>();
        mSalesmanList = new ArrayList<>();

        mServiceProviderId = "";
        mAllServiceProvider = new ServiceProviderBean.DataEntity.ServiceProvider();//"全部商户"
        mAllServiceProvider.setServiceProviderName("全部市运营");
        mAllServiceProvider.setServiceProviderId(mServiceProviderId);

        mSalesmanId = "";
        mAllSalesman = new SalesmanBean.DataEntity.Salesman();//"全部商户"
        mAllSalesman.setEmpName("全部城市经理");
        mAllSalesman.setEmpId(mSalesmanId);

        mServiceProviderAdapter = new ServiceProviderAdapter(getActivity());
        mServiceProviderAdapter.setOnItemClickListener(new ServiceProviderAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //切换商户
                if (mServiceProviderId != null && !mServiceProviderId.equals(mServiceProviderList.get(position).getServiceProviderId())) {
                    mSalesmanId = "";
                    mTvSalesman.setText("全部城市经理");
                    mServiceProviderId = mServiceProviderList.get(position).getServiceProviderId();
                    mTvServiceName.setText(mServiceProviderList.get(position).getServiceProviderName());
                    mPopLayout.setVisibility(View.GONE);
                    mCurrentPage = 1;
                    getRankSumaryData();
                    getRankListData();
                }
                closeArrow(2);
            }
        });

        mSalesmanAdapter = new SalesmanAdapter(getActivity());
        mSalesmanAdapter.setOnItemClickListener(new SalesmanAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (mSalesmanId != null && !mSalesmanId.equals(mSalesmanList.get(position).getEmpId())) {
                    mSalesmanId = mSalesmanList.get(position).getEmpId();
                    mTvSalesman.setText(mSalesmanList.get(position).getEmpName());
                    mPopLayout.setVisibility(View.GONE);
                    mCurrentPage = 1;
                    getRankSumaryData();
                    getRankListData();
                }
                closeArrow(3);
            }
        });
        mRecyclerViewFilter.addOnScrollListener(new RecyclerView.OnScrollListener() {
            //这个int用来记录最后一个可见的view
            int lastVisibleItemPosition;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int itemCount = -1;
                if (mServiceSwitchArrow) {
                    itemCount = mServiceProviderAdapter.getItemCount();
                } else if (mSalesmanSwitchArrow) {
                    itemCount = mSalesmanAdapter.getItemCount();
                }
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == itemCount) {
                    if (!isLoadOver) {
                        mRecyclerViewFilter.setEnabled(false);
                        if (mServiceSwitchArrow) {
                            getServiceList(true);
                        } else if (mSalesmanSwitchArrow) {
                            getSalesmanList(true);
                        }
                        mRecyclerViewFilter.setEnabled(true);
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                lastVisibleItemPosition = mLinearLayoutManager.findLastVisibleItemPosition();
            }
        });
        if ("SUPERADMIN".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_ROLE_CODE))) {
            if ("".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                mRlService.setVisibility(View.VISIBLE);
                mRlSalesman.setVisibility(View.VISIBLE);
            } else if ("SC".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                mRlSalesman.setVisibility(View.VISIBLE);
            }
        }

        mOrderType = 1;//默认选中交易净额
        mOrderRule = 2;//默认ui箭头向上是2 ，则倒序，由大到小
        recyclerview.setLayoutManager(new LinearLayoutManager(mContex));
        recyclerview.addItemDecoration(new LinearSpaceItemDecoration(DensityUtils.dip2px(mContex,15),false));
        rankFragmentAdapter = new RankFragmentAdapter(mContex);
        recyclerview.setAdapter(rankFragmentAdapter);

        mCurSelectEndDate = Calendar.getInstance();//当前选择的结束日期为今天
        mCurSelectStartDate = Calendar.getInstance();
        mCurSelectStartDate.add(Calendar.MONTH,-1);//当前选择的开始日期为1个月前
        mCurSelectStartDate.set(Calendar.HOUR_OF_DAY,0);
        mCurSelectStartDate.set(Calendar.MINUTE,0);
        mCurSelectStartDate.set(Calendar.SECOND,0);
        mCurSelectEndDate.set(Calendar.HOUR_OF_DAY,23);
        mCurSelectEndDate.set(Calendar.MINUTE,59);
        mCurSelectEndDate.set(Calendar.SECOND,59);
        initPickViewDate();

        /*List<RankListBean.SummaryData> rankListDatas = new ArrayList<>();
        RankListBean.SummaryData data1 = new RankListBean.SummaryData();
        RankListBean.SummaryData data2 = new RankListBean.SummaryData();
        RankListBean.SummaryData data3 = new RankListBean.SummaryData();
        RankListBean.SummaryData data4 = new RankListBean.SummaryData();
        RankListBean.SummaryData data5 = new RankListBean.SummaryData();
        rankListDatas.add(data1);
        rankListDatas.add(data2);
        rankListDatas.add(data3);
        rankListDatas.add(data4);
        rankListDatas.add(data5);
        rankFragmentAdapter.setRankListDatas(rankListDatas);
        RankSumaryBean.RankSumaryData rankSummaryData = new RankSumaryBean.RankSumaryData();
        rankSummaryData.setDate(mTimePickViewHelp.getDateString(mCurSelectStartDate,"yyyy/MM/dd")+"~"+mTimePickViewHelp.getDateString(mCurSelectEndDate,"yyyy/MM/dd"));
        rankFragmentAdapter.setRankSummaryBean(rankSummaryData);*/

        getRankSumaryData();
        getRankListData();
    }

    private void initPickViewDate() {
        Calendar rangeStartDate = mCurSelectStartDate;
        Calendar rangeEndDate = mCurSelectEndDate;
        Calendar curStartDate = mCurSelectStartDate;
        Calendar curEndDate = mCurSelectEndDate;
        mTimePickViewHelp = new TimePickViewHelp((BaseActivity) getActivity(),rangeStartDate,rangeEndDate,curStartDate,curEndDate);
        mTimePickViewHelp.mTv_bottom_tx_pickview.setVisibility(View.GONE);
    }


    private void initView() {
        mLoadDialog = ((MainActivity) getActivity()).getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeRefreshLayout);
        recyclerview = mView.findViewById(R.id.recyclerview);

        mRlService =  mView.findViewById(R.id.rl_service);
        mTvServiceName =  mView.findViewById(R.id.tv_service_name);
        mIvArrowService =  mView.findViewById(R.id.iv_arrow_service);
        mRlSalesman =  mView.findViewById(R.id.rl_salesman);
        mTvSalesman =  mView.findViewById(R.id.tv_salesman);
        mIvArrowSalesman =  mView.findViewById(R.id.iv_arrow_salesman);

        mPopLayout = mView.findViewById(R.id.pop_layout);
        mEtInput = mView.findViewById(R.id.et_input);
        mRecyclerViewFilter = mView.findViewById(R.id.recycler_view);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerViewFilter.setLayoutManager(mLinearLayoutManager);
        mCommonDataEmpty = mView.findViewById(R.id.common_data_empty);

        mTvDataStart = mView.findViewById(R.id.tv_date_start);
        mTvDataEnd = mView.findViewById(R.id.tv_date_end);
        mIvArrowReport = mView.findViewById(R.id.iv_arrow_report);
        mLlSelectDate = mView.findViewById(R.id.ll_select_date);
    }


    @Override
    protected void onFragmentVisible() {
        super.onFragmentVisible();
        StatusBarUtil.setTranslucentStatus(getActivity());
    }


    //获取交易统计数据
    private void getRankSumaryData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("startTime",mTimePickViewHelp.getDateString(mCurSelectStartDate,timeFormatType));
            map.put("endTime",mTimePickViewHelp.getDateString(mCurSelectEndDate,timeFormatType));
            if (!TextUtils.isEmpty(mServiceProviderId))
                map.put("serviceChannelId", mServiceProviderId);
            if (!TextUtils.isEmpty(mSalesmanId))
                map.put("salesmanId", mSalesmanId);
//            map.put("startTime","2020-04-01 00:00:00");
//            map.put("endTime","2020-06-30 23:59:59");
            ServerClient.newInstance(MyApplication.getContext()).getRankSumaryData(MyApplication.getContext(), Constants.TAG_GET_RANK_SUMMARY, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    //获取商户排名列表数据
    private void getRankListData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize",mPageSize);
            map.put("currentPage",mCurrentPage);
            map.put("startTime",mTimePickViewHelp.getDateString(mCurSelectStartDate,timeFormatType));
            map.put("endTime",mTimePickViewHelp.getDateString(mCurSelectEndDate,timeFormatType));
//            map.put("startTime","2020-04-01 00:00:00");
//            map.put("endTime","2020-06-30 23:59:59");
            map.put("orderType",mOrderType);
            map.put("orderRule",mOrderRule);
            if (!TextUtils.isEmpty(mServiceProviderId))
                map.put("serviceChannelId", mServiceProviderId);
            if (!TextUtils.isEmpty(mSalesmanId))
                map.put("salesmanId", mSalesmanId);
            ServerClient.newInstance(MyApplication.getContext()).getRankListData(MyApplication.getContext(), Constants.TAG_RANK_LIST, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getServiceList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mMerchantCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mMerchantPageSize);
            map.put("currentPage", mMerchantCurrentPage);
            if (!TextUtils.isEmpty(mEtInput.getText().toString().trim())) {
                map.put("serviceProviderName", mEtInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getServiceList(MyApplication.getContext(), "TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getSalesmanList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mMerchantCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mMerchantPageSize);
            map.put("currentPage", mMerchantCurrentPage);
            if (!TextUtils.isEmpty(mServiceProviderId))
                map.put("serviceChannelId", mServiceProviderId);
            if (!TextUtils.isEmpty(mEtInput.getText().toString().trim())) {
                map.put("empName", mEtInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getSalesmanList(MyApplication.getContext(), "TAG_SALESMAN_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_RANK_SUMMARY)) {
            //商户排名统计数据返回
            DialogUtil.safeCloseDialog(mLoadDialog);
            RankSumaryBean msg = (RankSumaryBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getResources().getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData()!=null){
                        RankSumaryBean.RankSumaryData rankSumaryData = msg.getData();
                        rankSumaryData.setDate(mTimePickViewHelp.getDateString(mCurSelectStartDate,"yyyy/MM/dd")+"～"+mTimePickViewHelp.getDateString(mCurSelectEndDate,"yyyy/MM/dd"));
                        rankFragmentAdapter.setRankSummaryBean(rankSumaryData);
                        mTvDataStart.setText(mTimePickViewHelp.getDateString(mCurSelectStartDate,"yyyy/MM/dd"));
                        mTvDataEnd.setText(mTimePickViewHelp.getDateString(mCurSelectEndDate,"yyyy/MM/dd"));
                    }
                    break;
            }
        }
        //商户排名列表数据返回
        if (event.getTag().equals(Constants.TAG_RANK_LIST)){
            DialogUtil.safeCloseDialog(mLoadDialog);
            setSwipeRefreshState(isPullRefresh, isLoadmore, 500);
            RankListBean msg = (RankListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).getLoginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg != null && msg.getData() != null) {
                        if (msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                            if (mCurrentPage==1){
                                mSummaryDataList.clear();
                            }
                            mSummaryDataList.addAll(msg.getData().getData());
                            rankFragmentAdapter.setRankListDatas(mSummaryDataList);
                            mCurrentPage++;
                        } else {
                            if (isLoadmore) {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                            }else {
                                mSummaryDataList.clear();//使后面会展示空视图
                                rankFragmentAdapter.setRankListDatas(mSummaryDataList);
                            }
                        }
                    } else {
                        if (isLoadmore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        }else {
                            mSummaryDataList.clear();//使后面会展示空视图
                            rankFragmentAdapter.setRankListDatas(mSummaryDataList);
                        }
                    }
                    //列表之上有概览数据要显示，就不要显示空视图了
                    /*if (mSummaryDataList !=null && mSummaryDataList.size()>0){
                        mTvNull.setVisibility(View.GONE);
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                    }else{
                        mTvNull.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayout.setVisibility(View.GONE);
                    }*/
                    break;
            }
            isLoadmore = false;
            isPullRefresh = false;
        } else if (event.getTag().equals("TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ServiceProviderBean msg = (ServiceProviderBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mCommonDataEmpty.setVisibility(View.GONE);
                        if (mMerchantCurrentPage == 1) {
                            mServiceProviderList.clear();
                            if (TextUtils.isEmpty(mEtInput.getText().toString().trim())) {
                                mServiceProviderList.add(mAllServiceProvider);
                            }
                        }

                        if (msg.getData().getData().size() != mMerchantPageSize)
                            isLoadOver = true;
                        mServiceProviderList.addAll(msg.getData().getData());
                        mServiceProviderAdapter.setData(mServiceProviderList, mServiceProviderId);
                        mMerchantCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mServiceProviderList.clear();
                            mServiceProviderAdapter.setData(mServiceProviderList, mServiceProviderId);
                            mCommonDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mCommonDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_SALESMAN_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SalesmanBean msg = (SalesmanBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mCommonDataEmpty.setVisibility(View.GONE);
                        if (mMerchantCurrentPage == 1) {
                            mSalesmanList.clear();
                            if (TextUtils.isEmpty(mEtInput.getText().toString().trim())) {
                                mSalesmanList.add(mAllSalesman);
                            }
                        }
                        if (msg.getData().getData().size() != mMerchantPageSize)
                            isLoadOver = true;
                        mSalesmanList.addAll(msg.getData().getData());
                        mSalesmanAdapter.setData(mSalesmanList, mSalesmanId);
                        mMerchantCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSalesmanList.clear();
                            mSalesmanAdapter.setData(mSalesmanList, mSalesmanId);
                            mCommonDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mCommonDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        }
    }

    /**
     * 关闭下拉刷新和上拉加载的进度条
     * */
    private void setSwipeRefreshState(boolean isPullRefresh, boolean isLoadmore, int delay) {
        if (isPullRefresh) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void openArrow(int condition) {
        Animation rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 0:
                mDateSwitchArrow = false;
                mIvArrowReport.startAnimation(rotate);
                break;
            case 2://点击市运营
                mEtInput.setText("");
                mServiceSwitchArrow = true;
                mIvArrowService.startAnimation(rotate);
                mRecyclerViewFilter.setAdapter(mServiceProviderAdapter);
                mPopLayout.setVisibility(View.VISIBLE);
                mEtInput.setHint(R.string.hint_search_by_service);
                getServiceList(false);
                break;
            case 3://点击城市经理
                mEtInput.setText("");
                mSalesmanSwitchArrow = true;
                mIvArrowSalesman.startAnimation(rotate);
                mPopLayout.setVisibility(View.VISIBLE);
                mRecyclerViewFilter.setAdapter(mSalesmanAdapter);
                mEtInput.setHint(R.string.hint_search_by_salesman);
                getSalesmanList(false);
                break;
        }
    }

    public void closeArrow(int condition) {
        Animation rotate = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        mPopLayout.setVisibility(View.GONE);
        switch (condition) {
            case 0:
                mDateSwitchArrow = false;
                mIvArrowReport.startAnimation(rotate);
                break;
            case 2:
                mServiceSwitchArrow = false;
                mIvArrowService.startAnimation(rotate);
                break;
            case 3:
                mSalesmanSwitchArrow = false;
                mIvArrowSalesman.startAnimation(rotate);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_select_date:
                //选择日期
                mTimePickViewHelp.show(new TimePickViewHelp.CallBackDataListener() {
                    @Override
                    public void onCancel() {
                        closeArrow(0);
                    }

                    @Override
                    public void onSubmit(Calendar curSelectStartDate, Calendar curSelectEndDate) {
                        String startDateStr = mTimePickViewHelp.getDateString(curSelectStartDate, "yyyy/MM/dd");
                        String endDateStr = mTimePickViewHelp.getDateString(curSelectEndDate, "yyyy/MM/dd");
                        mTvDataStart.setText(startDateStr);
                        mTvDataEnd.setText(endDateStr);
                        mTimePickViewHelp.dismiss();
                        closeArrow(0);
                        mCurSelectStartDate = curSelectStartDate;
                        mCurSelectEndDate = curSelectEndDate;
                        mCurrentPage = 1;
                        getRankSumaryData();
                        getRankListData();
                    }
                });
                if (mDateSwitchArrow) {
                    closeArrow(0);
                } else {
                    openArrow(0);
                }
                if (mPopLayout != null && mPopLayout.getVisibility() == View.VISIBLE) {
                    mPopLayout.setVisibility(View.GONE);
                    if (mSalesmanSwitchArrow) {
                        closeArrow(3);
                    }
                    if (mServiceSwitchArrow) {
                        closeArrow(2);
                    }
                }
                break;
            case R.id.rl_service:
                if (mDateSwitchArrow) {
                    closeArrow(0);
                }
                if (mSalesmanSwitchArrow) {
                    closeArrow(3);
                }
                if (mServiceSwitchArrow) {
                    closeArrow(2);
                } else {
                    openArrow(2);
                }
                break;
            case R.id.rl_salesman:
                if (mDateSwitchArrow) {
                    closeArrow(0);
                }
                if (mServiceSwitchArrow) {
                    closeArrow(2);
                }
                if (mSalesmanSwitchArrow) {
                    closeArrow(3);
                } else {
                    openArrow(3);
                }
                break;
        }
    }
}
