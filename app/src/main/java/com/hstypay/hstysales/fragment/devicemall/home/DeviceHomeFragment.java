package com.hstypay.hstysales.fragment.devicemall.home;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.DeviceMallOrderActivity;
import com.hstypay.hstysales.activity.MainActivity;
import com.hstypay.hstysales.adapter.DeviceMallOrderPagerAdapter;
import com.hstypay.hstysales.adapter.SalesmanAdapter;
import com.hstypay.hstysales.adapter.ServiceProviderAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.base.BaseFragment;
import com.hstypay.hstysales.bean.DeviceMallFragmentTitleBean;
import com.hstypay.hstysales.bean.DeviceOrderTransitCountBean;
import com.hstypay.hstysales.bean.SalesmanBean;
import com.hstypay.hstysales.bean.ServiceProviderBean;
import com.hstypay.hstysales.fragment.devicemall.BaseDeviceMallFragment;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.SafeDialog;
import com.ykk.dropdownmenu.DropDownMenu;
import com.ykk.dropdownmenu.TabBean;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zeyu.kuang
 * @time 2020/8/27
 * @desc 硬件商城home页，包括切换查看各种状态下的订单
 */
public class DeviceHomeFragment extends BaseFragment {

    private View mRootView;
    private DeviceMallOrderActivity mContext;
    private TabLayout mTabsDeviceMallOrder;
    private ViewPager mVpDeviceMallOrder;
    private String[] tabTitles;
    protected SafeDialog mLoadDialog;
    private List<DeviceMallFragmentTitleBean> mFragmentList;
    private final int ALL_DEVICE_MALL_INDEX = 0;//全部订单下标
    private final int WAIT_POST_MALL_INDEX = 1;//待发货下标
    private final int WAIT_SEND_MALL_INDEX = 2;//待收货下标
    private final int FINISHED_MALL_INDEX = 3;//已完成下标
    private final int CLOSED_MALL_INDEX = 4;//已关闭下标
    private String mCurSelectServiceProviderId;
    private String mCurSelectSalesmanId;
    private ArrayList<ServiceProviderBean.DataEntity.ServiceProvider> mSearchServiceProviderList;
    private ArrayList<SalesmanBean.DataEntity.Salesman> mSearchSalesmanList;
    private ArrayList<TabBean> headers;
    private DropDownMenu mDropDownMenu;
    private int mSearchPageSize = 15;
    private int mSearchCurrentPage = 1;
    private boolean isLoadMerchantMore;
    private boolean isLoadOver;
    private int mSelectLabelPosition;
    private ServiceProviderAdapter mServiceProviderAdapter;
    private SalesmanAdapter mSalesmanAdapter;
    private int mTabPosition;
    private EditText mEtServiceInput;
    private ImageView mIvServiceClean;
    private View mServiceDataEmpty;
    private EditText mEtSalesmanInput;
    private ImageView mIvSalesmanClean;
    private View mSalesmanDataEmpty;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (DeviceMallOrderActivity) getActivity();
    }

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mRootView = inflater.inflate(R.layout.fragment_device_home, container, false);
        initView();
        initData();
        initEvent();
        return mRootView;
    }

    private void initView() {
        mLoadDialog = mContext.getLoadDialog(getActivity(), UIUtils.getString(R.string.public_loading), false);
        mTabsDeviceMallOrder = mRootView.findViewById(R.id.tabs_device_mall_order);
        mTabsDeviceMallOrder.setTabIndicatorFullWidth(false);//design:28.0.0下划线宽度适应内容

        mDropDownMenu = mRootView.findViewById(R.id.dropDownMenu);
    }

    private void initEvent() {
        //设置选中的tab字体变大
        mTabsDeviceMallOrder.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getCustomView().findViewById(R.id.tv_tab_view_mall).setSelected(true);
                TextView tv = (TextView) tab.getCustomView().findViewById(R.id.tv_tab_view_mall);
                tv.setTextSize(17);
                TextView tv_tab_mark_mall = tab.getCustomView().findViewById(R.id.tv_tab_mark_mall);
                if (tv_tab_mark_mall.getVisibility() == View.VISIBLE) {
                    mTabsDeviceMallOrder.setSelectedTabIndicator(R.drawable.layer_tab_indicator_havemark);
                } else {
                    mTabsDeviceMallOrder.setSelectedTabIndicator(R.drawable.layer_tab_indicator_nomark);
                }
                mFragmentList.get(tab.getPosition()).getFragment().setFilter(mCurSelectServiceProviderId, mCurSelectSalesmanId);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getCustomView().findViewById(R.id.tv_tab_view_mall).setSelected(false);
                TextView tv = (TextView) tab.getCustomView().findViewById(R.id.tv_tab_view_mall);
                tv.setTextSize(15);
                mTabPosition = tab.getPosition();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        for (DeviceMallFragmentTitleBean deviceMallFragmentTitleBean : mFragmentList) {
            deviceMallFragmentTitleBean.getFragment().setOnUpDateTransitCountListener(new BaseDeviceMallFragment.onUpDateTransitCountListener() {
                @Override
                public void onUpDateTransitCount(boolean needLoading) {
                    getTransitCount(needLoading);
                }
            });
        }
    }

    private void initData() {
        tabTitles = new String[]{getResources().getString(R.string.tab_title_all_mall),
                getResources().getString(R.string.tab_title_wait_post_mall),
                getResources().getString(R.string.tab_title_wait_send_mall),
                getResources().getString(R.string.tab_title_finish_mall),
                getResources().getString(R.string.tab_title_close_mall)};
        initDropDownMenu();
        initFragment();

        //getTransitCount(true);

    }

    /*
     * 获取在途订单数量，包含待发货和待收货的订单数量
     * */
    public void getTransitCount(boolean needLoading) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (needLoading) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            ServerClient.newInstance(MyApplication.getContext()).getTransitCount(MyApplication.getContext(), Constants.TAG_GET_DEVICE_ORDER_TRANSITCOUNT, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTransitCountEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_DEVICE_ORDER_TRANSITCOUNT)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            DeviceOrderTransitCountBean msg = (DeviceOrderTransitCountBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_DEVICE_ORDER_TRANSITCOUNT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((MainActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_DEVICE_ORDER_TRANSITCOUNT_TRUE:
                    if (msg != null && msg.getData() != null) {
                        List<DeviceOrderTransitCountBean.DataBean> dataBeans = msg.getData();
                        int waitPostCount = 0;
                        int waitSendCount = 0;
                        for (DeviceOrderTransitCountBean.DataBean dataBean : dataBeans) {
                            int tradeStatus = dataBean.getTradeStatus();
                            if (tradeStatus == 1) {
                                //待发货订单数量
                                waitPostCount = dataBean.getCount();
                            } else if (tradeStatus == 2) {
                                //待收货订单数量
                                waitSendCount = dataBean.getCount();
                            }
                        }
                        setCustomTabMarkCount(mTabsDeviceMallOrder.getTabAt(WAIT_POST_MALL_INDEX), waitPostCount);//waitPostCount
                        setCustomTabMarkCount(mTabsDeviceMallOrder.getTabAt(WAIT_SEND_MALL_INDEX), waitSendCount);//waitSendCount

                    } else {
                        LogUtil.d(getResources().getString(R.string.no_more_data));
                    }
                    break;
            }
        }
    }

    private void initDropDownMenu() {
        mCurSelectServiceProviderId = "";
        mCurSelectSalesmanId = "";
        mSearchServiceProviderList = new ArrayList<>();
        mSearchSalesmanList = new ArrayList<>();

        View contentView = getLayoutInflater().inflate(R.layout.layout_content_device_home, null);
        mVpDeviceMallOrder = contentView.findViewById(R.id.vp_device_mall_order);

        headers = new ArrayList<>();
        TabBean tabBean2 = new TabBean();
        tabBean2.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean2.setLabel("cityService");
        tabBean2.setTabTitle("全部市运营");
        TabBean tabBean3 = new TabBean();
        tabBean3.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean3.setLabel("cityManager");
        tabBean3.setTabTitle("全部城市经理");
        if ("SUPERADMIN".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_ROLE_CODE))) {
            if ("".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean2);
                headers.add(tabBean3);
            } else if ("SC".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean3);
            }
        }
        mDropDownMenu.setDropDownMenu(headers, initViewData(), contentView);
        mDropDownMenu.setOnTabClickListener(new DropDownMenu.OnTabClickListener() {
            @Override
            public void onTabClick(int position, boolean isOpen) {
                mSelectLabelPosition = position;
                switch (headers.get(position).getLabel()) {
                    case "cityService":
                        if (isOpen)
                            getServiceList(false);
                        break;
                    case "cityManager":
                        if (isOpen)
                            getSalesmanList(false);
                        break;
                }
            }
        });
    }

    /**
     * 设置类型和数据源：
     * DropDownMenu.KEY对应类型（DropDownMenu中的常量，参考上述核心源码）
     * DropDownMenu.VALUE对应数据源：key不是TYPE_CUSTOM则传递string[],key是TYPE_CUSTOM类型则传递对应view
     */
    private List<HashMap<String, Object>> initViewData() {
        List<HashMap<String, Object>> viewDatas = new ArrayList<>();
        HashMap<String, Object> map;
        for (int i = 0; i < headers.size(); i++) {
            map = new HashMap<String, Object>();
            map.put(DropDownMenu.KEY, headers.get(i).getType());
            map.put(DropDownMenu.VALUE, getCustomView(headers.get(i).getLabel()));
            viewDatas.add(map);
        }
        return viewDatas;
    }

    private View getCustomView(String label) {
        View view = getLayoutInflater().inflate(R.layout.layout_dropdown, null);
        switch (label) {
            case "cityService":
                mEtServiceInput = view.findViewById(R.id.et_input);
                mIvServiceClean = view.findViewById(R.id.iv_clean);
                mServiceDataEmpty = view.findViewById(R.id.common_data_empty);
                RecyclerView rvService = view.findViewById(R.id.recycler_view);
                LinearLayoutManager serviceLinearLayoutManager = new LinearLayoutManager(getActivity());
                rvService.setLayoutManager(serviceLinearLayoutManager);
                mServiceProviderAdapter = new ServiceProviderAdapter(getActivity());
                mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                mServiceProviderAdapter.setOnItemClickListener(new ServiceProviderAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mCurSelectServiceProviderId != null && !mCurSelectServiceProviderId.equals(mSearchServiceProviderList.get(position).getServiceProviderId())) {
                            mCurSelectSalesmanId = "";
                            for (int i = 0; i < headers.size(); i++) {
                                if ("cityManager".equals(headers.get(i).getLabel()))
                                    mDropDownMenu.setTabText(i, "全部城市经理");
                            }
                            mCurSelectServiceProviderId = mSearchServiceProviderList.get(position).getServiceProviderId();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mDropDownMenu.setTabText(mSelectLabelPosition, mSearchServiceProviderList.get(position).getServiceProviderName());
                            mFragmentList.get(mTabPosition).getFragment().setFilter(mCurSelectServiceProviderId, mCurSelectSalesmanId);
                            mFragmentList.get(mTabPosition).getFragment().refreshData();
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvService.setAdapter(mServiceProviderAdapter);
                rvService.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    //这个int用来记录最后一个可见的view
                    int lastVisibleItemPosition;
                    int refreshCurrentPage;//防止重复加载

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mServiceProviderAdapter.getItemCount()) {
                            refreshCurrentPage = mSearchCurrentPage;
                            if (!isLoadOver)
                                getServiceList(true);
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                        lastVisibleItemPosition = serviceLinearLayoutManager.findLastVisibleItemPosition();
                    }
                });
                mEtServiceInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            getServiceList(false);
                            return true;
                        }
                        return false;
                    }
                });
                mEtServiceInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            getServiceList(false);
                        }
                        mIvServiceClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                mIvServiceClean.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEtServiceInput.setText("");
                    }
                });
                break;
            case "cityManager":
                mEtSalesmanInput = view.findViewById(R.id.et_input);
                mIvSalesmanClean = view.findViewById(R.id.iv_clean);
                mSalesmanDataEmpty = view.findViewById(R.id.common_data_empty);
                RecyclerView rvSalesman = view.findViewById(R.id.recycler_view);
                LinearLayoutManager salesmanLinearLayoutManager = new LinearLayoutManager(getActivity());
                rvSalesman.setLayoutManager(salesmanLinearLayoutManager);
                mSalesmanAdapter = new SalesmanAdapter(getActivity());
                mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                mSalesmanAdapter.setOnItemClickListener(new SalesmanAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mCurSelectSalesmanId != null && !mCurSelectSalesmanId.equals(mSearchSalesmanList.get(position).getEmpId())) {
                            mCurSelectSalesmanId = mSearchSalesmanList.get(position).getEmpId();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mDropDownMenu.setTabText(mSelectLabelPosition, mSearchSalesmanList.get(position).getEmpName());
                            mFragmentList.get(mTabPosition).getFragment().setFilter(mCurSelectServiceProviderId, mCurSelectSalesmanId);
                            mFragmentList.get(mTabPosition).getFragment().refreshData();
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvSalesman.setAdapter(mSalesmanAdapter);
                rvSalesman.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    //这个int用来记录最后一个可见的view
                    int lastVisibleItemPosition;
                    int refreshCurrentPage;//防止重复加载

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mSalesmanAdapter.getItemCount()) {
                            refreshCurrentPage = mSearchCurrentPage;
                            if (!isLoadOver)
                                getSalesmanList(true);
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                        lastVisibleItemPosition = salesmanLinearLayoutManager.findLastVisibleItemPosition();
                    }
                });
                mEtSalesmanInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            getSalesmanList(false);
                            return true;
                        }
                        return false;
                    }
                });
                mEtSalesmanInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            getSalesmanList(false);
                        }
                        mIvSalesmanClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                mIvSalesmanClean.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEtSalesmanInput.setText("");
                    }
                });
                break;
        }
        return view;
    }

    private void initFragment() {
        for (int i = 0; i < tabTitles.length; i++) {
            mTabsDeviceMallOrder.addTab(mTabsDeviceMallOrder.newTab());
        }
        mTabsDeviceMallOrder.setupWithViewPager(mVpDeviceMallOrder);

        mFragmentList = new ArrayList<>(tabTitles.length);
        mFragmentList.add(new DeviceMallFragmentTitleBean(tabTitles[ALL_DEVICE_MALL_INDEX], (BaseDeviceMallFragment) Fragment.instantiate(mContext, AllOrderDeviceMallFragment.class.getName())));
        mFragmentList.add(new DeviceMallFragmentTitleBean(tabTitles[WAIT_POST_MALL_INDEX], (BaseDeviceMallFragment) Fragment.instantiate(mContext, WaitPostOrderDeviceMallFragment.class.getName())));
        mFragmentList.add(new DeviceMallFragmentTitleBean(tabTitles[WAIT_SEND_MALL_INDEX], (BaseDeviceMallFragment) Fragment.instantiate(mContext, WaitSendOrderDeviceMallFragment.class.getName())));
        mFragmentList.add(new DeviceMallFragmentTitleBean(tabTitles[FINISHED_MALL_INDEX], (BaseDeviceMallFragment) Fragment.instantiate(mContext, FinishedOrderDeviceMallFragment.class.getName())));
        mFragmentList.add(new DeviceMallFragmentTitleBean(tabTitles[CLOSED_MALL_INDEX], (BaseDeviceMallFragment) Fragment.instantiate(mContext, ClosedOrderDeviceMallFragment.class.getName())));
        DeviceMallOrderPagerAdapter pagerAdapter = new DeviceMallOrderPagerAdapter(getChildFragmentManager());
        mVpDeviceMallOrder.setAdapter(pagerAdapter);
        pagerAdapter.setPagers(mFragmentList);

        setCustomTabView();
    }

    /**
     * 设置自定义tabview
     */
    private void setCustomTabView() {
        for (int i = 0; i < tabTitles.length; i++) {
            TabLayout.Tab tab = mTabsDeviceMallOrder.getTabAt(i);//获得每一个tab
            tab.setCustomView(R.layout.tab_item_device_mall);//给每一个tab设置view
            TextView textView = (TextView) tab.getCustomView().findViewById(R.id.tv_tab_view_mall);
            textView.setText(tabTitles[i]);//设置tab上的文字
            if (i == 0) {
                // 设置第一个tab的TextView是被选择的样式
                tab.getCustomView().findViewById(R.id.tv_tab_view_mall).setSelected(true);//第一个tab被选中
                textView.setTextSize(17);
                TextView tv_tab_mark_mall = tab.getCustomView().findViewById(R.id.tv_tab_mark_mall);
                if (tv_tab_mark_mall.getVisibility() == View.VISIBLE) {
                    mTabsDeviceMallOrder.setSelectedTabIndicator(R.drawable.layer_tab_indicator_havemark);
                } else {
                    mTabsDeviceMallOrder.setSelectedTabIndicator(R.drawable.layer_tab_indicator_nomark);
                }
            } else {
                tab.getCustomView().findViewById(R.id.tv_tab_view_mall).setSelected(false);
                textView.setTextSize(15);
            }
        }
    }

    /**
     * 设置角标数量
     */
    private void setCustomTabMarkCount(TabLayout.Tab tab, int count) {
        TextView tv_mark = (TextView) tab.getCustomView().findViewById(R.id.tv_tab_mark_mall);
        String countS = count + "";
        tv_mark.setText(countS);
        if (countS.length() > 2) {
            tv_mark.setText("99+");
            ViewGroup.LayoutParams layoutParams = tv_mark.getLayoutParams();
            layoutParams.width = DensityUtils.dip2px(mContext, 18);//(int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,18,getResources().getDisplayMetrics())+0.5);
            layoutParams.height = DensityUtils.dip2px(mContext, 18);//(int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,18,getResources().getDisplayMetrics())+0.5);
            tv_mark.setLayoutParams(layoutParams);
        }
        if (count > 0) {
            tv_mark.setVisibility(View.VISIBLE);
        } else {
            tv_mark.setVisibility(View.GONE);
        }
    }

    private void getServiceList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                map.put("serviceProviderName", mEtServiceInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getServiceList(MyApplication.getContext(), "TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getSalesmanList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                map.put("empName", mEtSalesmanInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getSalesmanList(MyApplication.getContext(), "TAG_SALESMAN_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals("TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ServiceProviderBean msg = (ServiceProviderBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mServiceDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchServiceProviderList.clear();
                            if (TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                                ServiceProviderBean.DataEntity.ServiceProvider allServiceProvider = new ServiceProviderBean.DataEntity.ServiceProvider();//"全部商户"
                                allServiceProvider.setServiceProviderName("全部市运营");
                                allServiceProvider.setServiceProviderId("");
                                mSearchServiceProviderList.add(allServiceProvider);
                            }
                        }

                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchServiceProviderList.addAll(msg.getData().getData());
                        mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchServiceProviderList.clear();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mServiceDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mServiceDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_SALESMAN_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SalesmanBean msg = (SalesmanBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((BaseActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mSalesmanDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchSalesmanList.clear();
                            if (TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                                SalesmanBean.DataEntity.Salesman allSalesman = new SalesmanBean.DataEntity.Salesman();//"全部商户"
                                allSalesman.setEmpName("全部城市经理");
                                allSalesman.setEmpId("");
                                mSearchSalesmanList.add(allSalesman);
                            }
                        }
                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchSalesmanList.addAll(msg.getData().getData());
                        mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchSalesmanList.clear();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mSalesmanDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mSalesmanDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        }
    }

}
