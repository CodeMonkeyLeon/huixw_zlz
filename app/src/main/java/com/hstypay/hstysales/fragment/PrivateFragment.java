package com.hstypay.hstysales.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.google.gson.Gson;
import com.hstypay.hstysales.BuildConfig;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.BankActivity;
import com.hstypay.hstysales.activity.BankBranchActivity;
import com.hstypay.hstysales.activity.ChangeCardUploadImageActivity;
import com.hstypay.hstysales.activity.ChangeCompanyCardActivity;
import com.hstypay.hstysales.activity.MerchantInfoActivity;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseFragment;
import com.hstypay.hstysales.bean.BankListBean;
import com.hstypay.hstysales.bean.BranchListBean;
import com.hstypay.hstysales.bean.ChoiceBean;
import com.hstypay.hstysales.bean.ImageBean;
import com.hstypay.hstysales.bean.MerchantInfoBean;
import com.hstypay.hstysales.bean.QueryEnabledChangeBean;
import com.hstypay.hstysales.bean.SendMsgBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.ClickUtil;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.ConstantsUtils;
import com.hstypay.hstysales.utils.DateUtil;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.FileUtils;
import com.hstypay.hstysales.utils.ImageFactory;
import com.hstypay.hstysales.utils.ImagePase;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.ChoiceDialog;
import com.hstypay.hstysales.widget.CustomViewFullScreenDialog;
import com.hstypay.hstysales.widget.LegalDialog;
import com.hstypay.hstysales.widget.OpenOrangeDialog;
import com.hstypay.hstysales.widget.SelectPicPopupWindow;
import com.hstypay.hstysales.widget.ShadowLayout;
import com.hstypay.hstysales.widget.TypeDialog;
import com.qiezzi.choseviewlibrary.ChoseCityPicker;
import com.qiezzi.choseviewlibrary.bean.AddressBean;
import com.shehuan.niv.NiceImageView;
import com.squareup.picasso.Picasso;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.fragment
 * @创建者: Jeremy
 * @创建时间: 2017/11/21 20:29
 * @描述: 同名结算卡
 */

public class PrivateFragment extends BaseFragment implements View.OnClickListener {
    private View mView;
    private RelativeLayout ly_phone_six, mRlCardBank, mRlCardAddress, mRlCardBankBranch;
    private LinearLayout ly_sixth;
    private ImageView iv_pic_six, mIvCardIdClean, mIvBank, mIvBankAddress, mIvBankBranch;
    private TextView mTvAcountName, mTvCardId, mTvCardBank, mTvCardAddress, mTvCardBankBranch;
    private EditText mEtAccountNo;
    private ShadowLayout mShadowButton;

    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE6 = 106;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    private String imageUrl;
    private File tempFile;
    private Uri originalUri;
    private String picSixPath;
    private MerchantInfoBean.DataBean mData;
    private String mType;
    private long mBankId = -1L;
    private long mBankBranchId = -1L;
    private String mProvinceCode;
    private String mCityCode;
    private ChoseCityPicker mCityPicker;
    private String mBkCardPhoto;
    private String mContactLine;
    private String mBankName;
    private String mBranchName;
    private RelativeLayout rl_merchant_type;
    private TextView tv_merchant_type;
    private ImageView iv_camera;
    private ChoiceDialog dialogType;
    private int accountTyp = 0;
    private LinearLayout ll_private;
    private RelativeLayout rl_public;
    private TextView tv_account_no_title;
    private TextView tv_submit;
    private ImageView iv_private_type_icon;
    private ImageView iv_bank_address_arrow;
    private ImageView iv_bank_branch_arrow;
    private EditText et_company, et_account_phone;
    private RelativeLayout rl_id_type;
    private TextView tv_private_id_type;
    private ImageView iv_private_id_type_icon;
    private ImageView iv_company_clean;
    private LegalDialog cardDialog;
    private int idCardType;
    private ChoiceDialog dialog;
    private int mchType;
    private RelativeLayout rl_license_valid_type, rl_account_phone;
    private TextView tv_license_valid_type;
    private TextView tv_license_date;
    private TextView tv_license_valid_date;
    private TextView tv_license_to;
    private TextView tv_license_valid_end;
    private TypeDialog typeDialog;
    private int choiceDate = 0;
    private TimePickerView pvTime;
    private ImageView iv_license_valid_icon;
    private ImageView iv_license_date_icon, iv_account_phone_clean;
    private boolean isCardValid;
    private String startTime;
    private String endTime;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE2 = 102;
    private static final int INTENT_REQUEST_CODE_CHANGECARDUPLOADIMAGEACTIVITY = 100;
    private static final int INTENT_REQUEST_CODE_CHANGECARDUPLOADIMAGEACTIVITY_CAMERA = 101;

    public static final int FROM_PRIAVE_FRAGEMENT = 1;
    private LinearLayout mLlBgsqPrivate;
    private NiceImageView mIvPicNewPrivate;
    private String mPicBgsqNet;
    private int mChangeAcountCount;//剩余可修改次数
    private String mMerchantId;
    private RelativeLayout mRlUploadIcon;
    private boolean mIsUploadImgSuccess;//上传变更函成功
    //橙意宝
    private ImageView iv_orange_split;
    private RelativeLayout rl_orange;
    private TextView tv_orange_state;

    @Override
    protected View loadSuccessView(LayoutInflater inflater, ViewGroup container) {
        mView = inflater.inflate(R.layout.fragment_private, container, false);
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView(mView);
        initListener();
        initData();

        Log.i("kkk", "onActivityCreated");
    }

    private void initView(View view) {
        ly_phone_six = (RelativeLayout) view.findViewById(R.id.ly_phone_six);
        ly_sixth = (LinearLayout) view.findViewById(R.id.ly_sixth);
        iv_pic_six = (ImageView) view.findViewById(R.id.iv_pic_six);
        mTvAcountName = (TextView) view.findViewById(R.id.tv_account_name);
        mTvCardId = (TextView) view.findViewById(R.id.tv_card_id);//身份证号
        mEtAccountNo = (EditText) view.findViewById(R.id.et_account_no);//银行卡号
        mIvCardIdClean = (ImageView) view.findViewById(R.id.iv_card_id_clean);
        mRlCardBank = (RelativeLayout) view.findViewById(R.id.rl_card_bank);
        mTvCardBank = (TextView) view.findViewById(R.id.tv_card_bank);
        mRlCardAddress = (RelativeLayout) view.findViewById(R.id.rl_card_address);
        mTvCardAddress = (TextView) view.findViewById(R.id.tv_card_address);
        mRlCardBankBranch = (RelativeLayout) view.findViewById(R.id.rl_card_bank_branch);
        mTvCardBankBranch = (TextView) view.findViewById(R.id.tv_card_bank_branch);
        mIvBank = (ImageView) view.findViewById(R.id.iv_bank_arrow);
        mIvBankBranch = (ImageView) view.findViewById(R.id.iv_bank_branch_arrow);
        mIvBankAddress = (ImageView) view.findViewById(R.id.iv_bank_address_arrow);
        mShadowButton = (ShadowLayout) view.findViewById(R.id.shadow_button);
        rl_merchant_type = view.findViewById(R.id.rl_merchant_type);
        tv_merchant_type = view.findViewById(R.id.tv_merchant_type);
        iv_camera = view.findViewById(R.id.iv_camera);
        ll_private = view.findViewById(R.id.ll_private);
        rl_public = view.findViewById(R.id.rl_public);
        tv_account_no_title = view.findViewById(R.id.tv_account_no_title);
        tv_submit = view.findViewById(R.id.tv_submit);
        iv_private_type_icon = view.findViewById(R.id.iv_private_type_icon);
        iv_bank_address_arrow = view.findViewById(R.id.iv_bank_address_arrow);
        iv_bank_branch_arrow = view.findViewById(R.id.iv_bank_branch_arrow);
        et_company = view.findViewById(R.id.et_company);
        rl_id_type = view.findViewById(R.id.rl_id_type);
        tv_private_id_type = view.findViewById(R.id.tv_private_id_type);
        iv_private_id_type_icon = view.findViewById(R.id.iv_private_id_type_icon);
        iv_company_clean = view.findViewById(R.id.iv_company_clean);
        rl_license_valid_type = view.findViewById(R.id.rl_license_valid_type);//证件有效期类型
        tv_license_valid_type = view.findViewById(R.id.tv_license_valid_type);
        tv_license_date = view.findViewById(R.id.tv_license_date);
        tv_license_valid_date = view.findViewById(R.id.tv_license_valid_date);
        tv_license_to = view.findViewById(R.id.tv_license_to);
        tv_license_valid_end = view.findViewById(R.id.tv_license_valid_end);
        iv_license_valid_icon = view.findViewById(R.id.iv_license_valid_icon);
        iv_license_date_icon = view.findViewById(R.id.iv_license_date_icon);
        iv_account_phone_clean = view.findViewById(R.id.iv_account_phone_clean);
        et_account_phone = view.findViewById(R.id.et_account_phone);
        rl_account_phone = view.findViewById(R.id.rl_account_phone);

        mLlBgsqPrivate = view.findViewById(R.id.ll_bgsq_private);
        mIvPicNewPrivate = view.findViewById(R.id.iv_pic_new_private);
        mRlUploadIcon = view.findViewById(R.id.rl_upload_icon);

        iv_orange_split = view.findViewById(R.id.iv_orange_split);
        rl_orange = view.findViewById(R.id.rl_orange);
        tv_orange_state = view.findViewById(R.id.tv_orange_state);
        iv_orange_split.setVisibility(canShowOrange()? View.VISIBLE : View.GONE);
        rl_orange.setVisibility(canShowOrange() ? View.VISIBLE : View.GONE);
        //设置当前开通状态
        String productMarkStr = mData.getBankAccount().getProductMark();
        //匹配橙意宝文案
        if (canShowOrange() && !StringUtils.isEmptyOrNull(productMarkStr))
            tv_orange_state.setText(getTypeDes(Integer.valueOf(productMarkStr)));
        TextView tv_pic_title = view.findViewById(R.id.tv_pic_title);
        setTextLastCharRed(tv_pic_title);

        initTimePicker();
    }

    //是否可显示橙意宝
    private boolean canShowOrange() { //可交易且商户类型一致
        //当前是否是安徽新零售
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            if (!installChannel.equals(Constants.AHDXSALES)) return false;
        } else {
            return false;
        }
        return ConstantsUtils.isIncome(mType) && ConstantsUtils.isSupportedOrange(mData.getOutMchType());
    }

    //获取橙意宝类型文案
    public String getTypeDes(int type) {
        if (type == OpenOrangeDialog.ITEM_NO_OPEN) {
            return getString(R.string.no_open);
        } else if (type == OpenOrangeDialog.ITEM_ORANGE) {
            return getString(R.string.orange_service);
        } else if (type == OpenOrangeDialog.ITEM_ORANGE_SMOOTH) {
            return getString(R.string.orange_smooth_service);
        } else {
            return "";
        }
    }

    private void setTextLastCharRed(TextView tv){
        String content = tv.getText().toString();
        SpannableString spantwo = new SpannableString(content);
        spantwo.setSpan(new ForegroundColorSpan(UIUtils.getColor(R.color.red)), content.length()-1, content.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tv.setText(spantwo);
    }

    private void initTimePicker() {
        //控制时间范围(如果不设置范围，则使用默认时间1900-2100年，此段代码可注释)
        //因为系统Calendar的月份是从0-11的,所以如果是调用Calendar的set方法来设置时间,月份的范围也要是从0-11
        Calendar selectedDate = Calendar.getInstance();
      /*  Calendar selectedDate = Calendar.getInstance();
        Calendar startDate = Calendar.getInstance();
        startDate.add(startDate.MONTH, -3);  //设置为前3月
        startDate.set(startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
        Calendar endDate = Calendar.getInstance();
        endDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));*/
        //时间选择器
        pvTime = new TimePickerView.Builder(getActivity(), new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                // 这里回调过来的v,就是show()方法里面所添加的 View 参数，如果show的时候没有添加参数，v则为null
                String time = DateUtil.getSelectDate(date);

                if (choiceDate > 0) {

                    switch (choiceDate) {

                        case 1:
                            //营业执照开始日期
                            tv_license_valid_date.setText(time);
                            startTime = time + " 00:00:00";
                            break;
                        case 2:
                            //营业执照结束日期
                            tv_license_valid_end.setText(time);
                            endTime = time + " 00:00:00";
                            break;

                    }
                }

            }
        })
                .setLayoutRes(R.layout.layout_pickerview_time, new CustomListener() {
                    @Override
                    public void customLayout(View v) {
                        Button btnCancel = v.findViewById(R.id.btnCancel);
                        Button btnSubmit = v.findViewById(R.id.btnSubmit);
                        btnCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                pvTime.dismiss();
                            }
                        });
                        btnSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                pvTime.returnData();
                                pvTime.dismiss();
                            }
                        });
                    }
                })
                //年月日时分秒 的显示与否，不设置则默认全部显示
                .setType(new boolean[]{true, true, true, false, false, false})
                .setLabel("", "", "", "", "", "")
                .isCenterLabel(false)
                .setDividerColor(getResources().getColor(R.color.home_line))
                .setContentSize(18)
                .setSubCalSize(18)
                .setDate(selectedDate)
                // .setRangDate(startDate, endDate)
                //.setBackgroundId(0x06000000) //设置外部遮罩颜色
                .setDecorView(null)
                .setLabel("年", "月", "日", null, null, null)
                .setLineSpacingMultiplier(2.5f)
                .build();
    }

    private void initListener() {
        mIvCardIdClean.setOnClickListener(this);
        ly_phone_six.setOnClickListener(this);
        mRlCardBank.setOnClickListener(this);
        mRlCardAddress.setOnClickListener(this);
        mRlCardBankBranch.setOnClickListener(this);
        iv_camera.setOnClickListener(this);
        rl_merchant_type.setOnClickListener(this);
        //   rl_id_type.setOnClickListener(this);
        iv_company_clean.setOnClickListener(this);
        iv_account_phone_clean.setOnClickListener(this);
        tv_submit.setOnClickListener(this);
        rl_license_valid_type.setOnClickListener(this);
        tv_license_valid_date.setOnClickListener(this);
        tv_license_valid_end.setOnClickListener(this);
        mLlBgsqPrivate.setOnClickListener(this);
        rl_orange.setOnClickListener(this);
        setEdittextListener(mEtAccountNo, mIvCardIdClean);
        setEdittextListener(et_company, iv_company_clean);
        setEdittextListener(et_account_phone, iv_account_phone_clean);
    }

    private void initData() {
        if (mData != null) {
            MerchantInfoBean.DataBean.BankAccountBean bankAccount = mData.getBankAccount();

            if (bankAccount != null) {
                if (bankAccount.getAccountType() == 1) {
                    tv_merchant_type.setText("对公账户");
                    accountTyp = 1;
                    //对公
                    rl_account_phone.setVisibility(View.GONE);
                    ll_private.setVisibility(View.GONE);
                    rl_public.setVisibility(View.VISIBLE);
                    tv_account_no_title.setText("结算卡号");
                } else {
                    tv_merchant_type.setText("对私账户");
                    accountTyp = 2;
                    //对私
                    rl_account_phone.setVisibility(View.VISIBLE);
                    tv_account_no_title.setText("银行卡号");
                    ll_private.setVisibility(View.VISIBLE);
                    rl_public.setVisibility(View.GONE);

                }

                // mTvCardId.setText(DateUtil.idString(mData.getIdCard()));


                MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();

                if (mchDetail != null) {

                    if (mchDetail.getPrincipal() != null) {
                        mTvAcountName.setText(mchDetail.getPrincipal());
                    }
                    mTvCardId.setText(mchDetail.getIdCode());

                    switch (mchDetail.getIdCodeType()) {

                        case 1:
                            //身份证
                            tv_private_id_type.setText("身份证");
                            idCardType = 1;
                            break;
                        case 2:
                            //护照
                            tv_private_id_type.setText("护照");
                            idCardType = 2;
                            break;
                        case 3:
                            //港澳居民来往内地通行证
                            tv_private_id_type.setText("港澳居民来往内地通行证");
                            idCardType = 3;
                            break;
                        case 4:
                            //台湾居民来往内地通行证
                            tv_private_id_type.setText("台湾居民来往内地通行证");
                            idCardType = 4;
                            break;
                        default:
                            //其它
                            tv_private_id_type.setText("其它");
                            idCardType = 99;
                            break;
                    }

                }
                boolean idCodeValidFlag = bankAccount.isAccountExpiredFlag();
                if (mData.getMchDetail() != null) {
                    idCodeValidFlag = mData.getMchDetail().isIdCodeValidFlag();
                    startTime = mData.getMchDetail().getIdCodeValidBegin();
                    endTime = mData.getMchDetail().getIdCodeValidEnd();
                } else {
                    startTime = bankAccount.getAccountExpiredBegin();
                    endTime = bankAccount.getAccountExpiredDate();
                }
                if (idCodeValidFlag) {
                    //长期
                    tv_license_valid_type.setText("长期");
                    tv_license_to.setVisibility(View.GONE);
                    tv_license_valid_end.setVisibility(View.GONE);
                    isCardValid = true;
                } else {
                    tv_license_valid_type.setText("非长期");
                    isCardValid = false;
                }

                if (bankAccount.getAccountExpiredBegin() != null) {
                    tv_license_valid_date.setText(bankAccount.getAccountExpiredBegin());
                }

                if (bankAccount.getAccountExpiredDate() != null) {
                    tv_license_valid_end.setText(bankAccount.getAccountExpiredDate());
                }


                et_company.setText(bankAccount.getAccountName());//公司名称
                if (!TextUtils.isEmpty(bankAccount.getTel())) {
                    et_account_phone.setText(bankAccount.getTel());//预留手机号
                }
                mEtAccountNo.setText(bankAccount.getAccountCode());
                mEtAccountNo.setSelection(mEtAccountNo.getText().length());
                mTvCardBank.setText(bankAccount.getBankIdCnt());
                mTvCardAddress.setText(bankAccount.getProvinceCnt() + " " + bankAccount.getCityCnt());

                mBankId = bankAccount.getBankId();
                mProvinceCode = bankAccount.getProvince();
                mCityCode = bankAccount.getCity();
                mBankBranchId = bankAccount.getBankBranchId();
                mContactLine = bankAccount.getContactLine();
                mBranchName = bankAccount.getBankName();
                mTvCardBankBranch.setText(bankAccount.getBankName());
            }

            if ("6".equals(getActivity().getIntent().getStringExtra("type"))) {
                if (mData.getMchQueryStatus() != 0 && mData.getMchQueryStatus() != 2 && mData.getMchQueryStatus() != 100){
                    setExitEable(false);
                }else if (isChangeInfo()){
                    queryEnabledChange();
                }
            } else {
                switch (mData.getMchQueryStatus()) {

                    case -1:
                        //初始商户
                        setExitEable(false);
                        break;
                    /*case 1:
                        //审核通过
                        setExitEable(false);
                        break;*/
                    case 5:
                        //作废商户
                        setExitEable(false);
                        break;
                    /*case 10:
                        //变更审核通过
                        setExitEable(false);
                        break;*/
                    case 101:
                        //冻结商户
                        setExitEable(false);
                        break;
                    default:
                        if (isChangeInfo()){
                            queryEnabledChange();
                        }
                        break;
                }
            }
            MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();
            if (mchDetail!=null){
                mPicBgsqNet = mchDetail.getAccountChangePhoto();
                LogUtil.d("Jeremy","mPicBgsqNet:"+mPicBgsqNet);
                if (!TextUtils.isEmpty(mPicBgsqNet)){
                    mLlBgsqPrivate.setVisibility(View.VISIBLE);
                    String imageUrl = Constants.BASE_URL + mPicBgsqNet;
                    Picasso.get().load(imageUrl).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(mIvPicNewPrivate);
                }
            }
        }

        if ((mChangeAcountCount != MerchantInfoActivity.INITCHANGEAVAILDCOUNT) && mChangeAcountCount<=0){
            setExitEable(false);
        }
//        setButtonView();
    }

    //更新橙意宝状态
    private void updateOrangeState(String merchantId, String typeStr) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", merchantId);
            map.put("productMark", typeStr);
            ServerClient.newInstance(MyApplication.getContext()).updateOrangeState(MyApplication.getContext(), Constants.ORANGE_UPDATE, map, typeStr);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    //是否是变更审核
    private boolean isChangeInfo(){
        if (mData==null){
            return false;
        }
//        if (mData.getMchQueryStatus() == 3 || mData.getMchQueryStatus() == 4 || mData.getMchQueryStatus() == 100|| mData.getMchQueryStatus() == 10){
            //case 3://变更审核中
            //case 4://变更失败
            //case 100://可交易
            //case 10://变更审核通过
        if (mData.getMchQueryStatus() == 1 || mData.getMchQueryStatus() == 100|| mData.getMchQueryStatus() == 10){
            //case 1://审核通过
            return true;
        }
        return false;
    }

    //验证商户资料是否允许修改
    private void queryEnabledChange() {
        if ((mChangeAcountCount != MerchantInfoActivity.INITCHANGEAVAILDCOUNT) && mChangeAcountCount<=0){
            return;
        }
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            DialogUtil.safeShowDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
            map.put("merchantId", mMerchantId);
            ServerClient.newInstance(MyApplication.getContext()).queryEnabledChange(MyApplication.getContext(), Constants.TAG_QUERY_ENABLED_CHANGE+"_"+getClass().getName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void setExitEable(boolean b) {
        if (!b) {
            mIvCardIdClean.setVisibility(View.INVISIBLE);
            mEtAccountNo.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
            mTvCardBank.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
            mTvCardBankBranch.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
            mTvCardAddress.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
            tv_merchant_type.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
            mTvAcountName.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
            mIvBank.setVisibility(View.INVISIBLE);
            mIvBankBranch.setVisibility(View.INVISIBLE);
            mIvBankAddress.setVisibility(View.INVISIBLE);
            mEtAccountNo.setEnabled(false);
            mRlCardBank.setEnabled(false);
            mRlCardBankBranch.setEnabled(false);
            mRlCardAddress.setEnabled(false);
            ly_phone_six.setEnabled(false);
            rl_merchant_type.setEnabled(false);
            iv_private_type_icon.setVisibility(View.GONE);
            iv_camera.setVisibility(View.INVISIBLE);
            iv_bank_address_arrow.setVisibility(View.GONE);
            iv_bank_branch_arrow.setVisibility(View.GONE);
            tv_submit.setVisibility(View.GONE);
            et_company.setEnabled(false);
            iv_company_clean.setVisibility(View.GONE);
            et_account_phone.setEnabled(false);
            et_account_phone.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
            iv_account_phone_clean.setVisibility(View.GONE);

            iv_private_id_type_icon.setVisibility(View.GONE);


            rl_license_valid_type.setEnabled(false);
            tv_license_valid_type.setTextColor(getResources().getColor(R.color.tv_rb_color));
            iv_license_valid_icon.setVisibility(View.GONE);
            tv_license_valid_date.setEnabled(false);
            tv_license_valid_date.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_license_to.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_license_valid_end.setTextColor(getResources().getColor(R.color.tv_rb_color));
            tv_license_valid_end.setEnabled(false);
            iv_license_date_icon.setVisibility(View.GONE);
            rl_id_type.setEnabled(false);
            mLlBgsqPrivate.setEnabled(false);
            mLlBgsqPrivate.setClickable(false);
            mRlUploadIcon.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ly_phone_six:
                PermissionUtils.checkPermissionArray(getActivity(), permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE6);
                choice();
                break;
            case R.id.rl_card_bank:
                Intent intentBank = new Intent(getActivity(), BankActivity.class);
                intentBank.putExtra(Constants.INTENT_BANK_LIST, mBankId);
                startActivityForResult(intentBank, Constants.REQUEST_BANK_LIST);
                break;
            case R.id.rl_card_address:
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
                    ServerClient.newInstance(MyApplication.getContext()).getAddress(MyApplication.getContext(), Constants.TAG_GET_ADDRESS, null);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                }
                break;
            case R.id.rl_card_bank_branch:
                if (mBankId == -1L) {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_bank_empty));
                    return;
                }
                if (TextUtils.isEmpty(mProvinceCode) || TextUtils.isEmpty(mCityCode)) {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_bank_address_empty));
                    return;
                }
                Intent intentBankBranch = new Intent(getActivity(), BankBranchActivity.class);
                intentBankBranch.putExtra(Constants.INTENT_BANK_BRANCH_ID, mBankBranchId);
                intentBankBranch.putExtra(Constants.INTENT_BANK_ID, mBankId);
                intentBankBranch.putExtra(Constants.INTENT_BANK_PROVINCE, mProvinceCode);
                intentBankBranch.putExtra(Constants.INTENT_BANK_CITY, mCityCode);
                startActivityForResult(intentBankBranch, Constants.REQUEST_BANK_BRANCH_LIST);
                break;
//            case R.id.btn_common_enable:
                /*if (mData.getCardType() == 1) {
                    mData.setAccountType(2);
                    mData.setBkCardPhoto(mBkCardPhoto);
                    mData.setAccountCode(mEtAccountNo.getText().toString().trim());
                    mData.setBankId(mBankId);
                    mData.setBankIdCnt(mBankName);
                    mData.setProvince(mProvinceCode);
                    mData.setCity(mCityCode);
                    mData.setBankBranchId(mBankBranchId);
                    mData.setBankBranchIdCnt(mBranchName);
                    mData.setContactLine(mContactLine);
                    Intent intent = new Intent();
                    Bundle mBundle = new Bundle();
                    mBundle.putSerializable(Constants.RESULT_MERCHANT_CARD_INTENT, mData);
                    intent.putExtras(mBundle);
                    getActivity().setResult(Activity.RESULT_OK, intent);
                    getActivity().finish();
                } else {*/

//                submit();
                // }
//                break;
            case R.id.iv_card_id_clean:
                mEtAccountNo.setText("");
                break;
            case R.id.iv_account_phone_clean:
                et_account_phone.setText("");
                break;
            case R.id.iv_camera:
                PermissionUtils.checkPermissionArray(getActivity(), permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE6);
                choice();
                break;
            case R.id.rl_merchant_type:
                //账户类型
                accountType();
                break;
            case R.id.rl_id_type:
                //证件类型
                choiceDialog();

                break;
            case R.id.iv_company_clean:
                iv_company_clean.setVisibility(View.GONE);
                et_company.setText("");
                break;
            case R.id.tv_submit:
                //提交
                if (accountTyp == 1) {
                    //对公
                    rl_account_phone.setVisibility(View.GONE);
                    if (StringUtils.isEmptyOrNull(et_company.getText().toString().trim())) {
                        ToastUtil.showToastShort("请输入公司名称");
                        et_company.setFocusable(true);
                        et_company.setFocusableInTouchMode(true);
                        et_company.requestFocus();
                        return;
                    }
                } else {
                    rl_account_phone.setVisibility(View.VISIBLE);
                    if (StringUtils.isEmptyOrNull(et_account_phone.getText().toString().trim())) {
                        ToastUtil.showToastShort(getString(R.string.hint_card_phone));
                        et_account_phone.setFocusable(true);
                        et_account_phone.setFocusableInTouchMode(true);
                        et_account_phone.requestFocus();
                        return;
                    }
                }


                if (StringUtils.isEmptyOrNull(mEtAccountNo.getText().toString().trim())) {

                    ToastUtil.showToastShort("请输入银行卡号");
                    mEtAccountNo.setFocusable(true);
                    mEtAccountNo.setFocusableInTouchMode(true);
                    mEtAccountNo.requestFocus();
                    return;
                }
                if (StringUtils.isEmptyOrNull(mTvCardBank.getText().toString().trim())) {
                    ToastUtil.showToastShort("请选择开户银行");
                    return;
                }
                if (StringUtils.isEmptyOrNull(mTvCardBankBranch.getText().toString().trim())) {
                    ToastUtil.showToastShort("请选择开户支行");
                    return;
                }

                if (isChangeInfo()){
                    /*if (TextUtils.isEmpty(mPicBgsqNet)){
                        showChangeWarnDialog();
                    }else {
                        //有图片说明已经变更过一次了
                        showOnceWarnDialog();
                    }*/
                    if (mIsUploadImgSuccess){
                        showOnceWarnDialog();
                    }else {
                        showChangeWarnDialog();
                    }
                }else {
                    cardDialog("card");
                }

                break;

            case R.id.rl_license_valid_type:
                //证件有效期类型
                IDType(1);
                break;
            case R.id.tv_license_valid_date:
                //证件开始日期
                choiceDate = 1;
                pvTime.show();
                break;
            case R.id.tv_license_valid_end:
                //证件结束日期
                choiceDate = 2;
                pvTime.show();
                break;
            case R.id.ll_bgsq_private:
                goChangeCardUploadImageActivity(INTENT_REQUEST_CODE_CHANGECARDUPLOADIMAGEACTIVITY_CAMERA);
                break;
            case R.id.rl_orange:
                clickOrange();
                break;
        }
    }

    //点击橙意宝
    private void clickOrange() {
        OpenOrangeDialog orangeDialog = OpenOrangeDialog.newBuilder(getActivity()).build();
        orangeDialog.showDialog();
        orangeDialog.setHandlerListener((type, typeStr, typeDes) -> {
            updateOrangeState(mData.getMerchantId(), typeStr);
        });
    }

    //去上传图片页面
    private void goChangeCardUploadImageActivity(int requestCode) {
        Intent intent = new Intent(getActivity(), ChangeCardUploadImageActivity.class);
        intent.putExtra(Constants.INTENT_NAME_PIC_BGSQ_NET, mPicBgsqNet);
        intent.putExtra(Constants.INTENT_NAME_FROM, FROM_PRIAVE_FRAGEMENT);
        startActivityForResult(intent, requestCode);
    }

    //提示每月仅可更改一次
    private void showOnceWarnDialog() {
        final CustomViewFullScreenDialog dialog = new CustomViewFullScreenDialog(getActivity());
        dialog.setView(R.layout.dialog_change_card_warn);
        TextView tv_content = dialog.findViewById(R.id.tv_content);
        tv_content.setText(getResources().getString(R.string.change_once_bank_card_warn));
        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView btnCancel = dialog.findViewById(R.id.btnCancel);
        TextView btnOk = dialog.findViewById(R.id.btnOk);
        btnCancel.setText("再考虑一下");
        btnOk.setText("是");
        tv_title.setText("温馨提示");
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.btnOk:
                        dialog.dismiss();
                        cardDialog("card");
                        break;
                    case R.id.btnCancel:
                        dialog.dismiss();
                        break;
                }
            }
        };
        btnCancel.setOnClickListener(onClickListener);
        btnOk.setOnClickListener(onClickListener);
        dialog.show();
    }
    //提示上传函
    private void showChangeWarnDialog() {
        final CustomViewFullScreenDialog dialog = new CustomViewFullScreenDialog(getActivity());
        dialog.setView(R.layout.dialog_change_card_warn);
        TextView tv_content = dialog.findViewById(R.id.tv_content);
        tv_content.setText(getResources().getString(R.string.change_bank_card_warn));
        TextView btnCancel = dialog.findViewById(R.id.btnCancel);
        TextView btnOk = dialog.findViewById(R.id.btnOk);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.btnOk:
                        dialog.dismiss();
                        goChangeCardUploadImageActivity(INTENT_REQUEST_CODE_CHANGECARDUPLOADIMAGEACTIVITY);
                        break;
                    case R.id.btnCancel:
                        dialog.dismiss();
                        break;
                }
            }
        };
        btnCancel.setOnClickListener(onClickListener);
        btnOk.setOnClickListener(onClickListener);
        dialog.show();
    }

    private void IDType(final int type) {

        typeDialog = new TypeDialog(getActivity(), new TypeDialog.HandleBtn() {
            @Override
            public void handleOkBtn(String s) {

                switch (type) {

                    case 1:
                        //营业执照有效期
                        if (!StringUtils.isEmptyOrNull(s)) {
                            if (s.equals("1")) {
                                //长期
                                tv_license_valid_type.setText("长期");
                                tv_license_date.setText("证件开始日期");
                                tv_license_to.setVisibility(View.GONE);
                                tv_license_valid_end.setVisibility(View.GONE);
                                isCardValid = true;

                            } else {
                                tv_license_valid_type.setText("非长期");
                                tv_license_date.setText("证件有效期");
                                tv_license_to.setVisibility(View.VISIBLE);
                                tv_license_valid_end.setVisibility(View.VISIBLE);
                                isCardValid = false;

                            }
                        }

                        break;

                }

                typeDialog.dismiss();
            }
        });

        DialogHelper.resizeFull(getActivity(), typeDialog);
        typeDialog.show();

    }


    private void choiceDialog() {

        List<ChoiceBean> mList = new ArrayList<>();
        ChoiceBean bean = new ChoiceBean();
        bean.setId(1);
        bean.setName("身份证");
        ChoiceBean bean1 = new ChoiceBean();
        bean1.setId(2);
        bean1.setName("护照");
        ChoiceBean bean2 = new ChoiceBean();
        bean2.setId(3);
        bean2.setName("港澳居民来往内地通行证");
        ChoiceBean bean3 = new ChoiceBean();
        bean3.setId(4);
        bean3.setName("台湾居民来往内地通行证");
        mList.add(bean);
        mList.add(bean1);
        mList.add(bean2);
        mList.add(bean3);

        dialog = new ChoiceDialog(getActivity(), mList, new ChoiceDialog.HandleBtn() {
            @Override
            public void handleOkBtn(ChoiceBean s) {
                dialog.dismiss();

                if (s != null) {
                    tv_merchant_type.setText(s.getName());

                    switch (s.getId()) {

                        case 1:
                            tv_private_id_type.setText("身份证");
                            idCardType = 1;
                            break;
                        case 2:
                            tv_private_id_type.setText("护照");
                            idCardType = 2;
                            break;
                        case 3:
                            idCardType = 3;
                            tv_private_id_type.setText("港澳居民来往内地通行证");
                            break;
                        case 4:
                            tv_private_id_type.setText("台湾居民来往内地通行证");
                            idCardType = 4;
                            break;
                    }

                }

            }
        });
        DialogHelper.resize(getActivity(), dialog);
        dialog.show();
    }

    private void accountType() {
        List<ChoiceBean> mList = new ArrayList<>();
        ChoiceBean bean = new ChoiceBean();
        ChoiceBean bean1 = new ChoiceBean();
        bean.setName("对公账户");
        bean.setId(1);
        bean1.setName("对私账户");
        bean1.setId(2);

        if (mData != null && mData.getOutMchType() == 3) {
            //个体经营者只有对私账户
            mList.add(bean1);
        } else {
            mList.add(bean1);
            mList.add(bean);
        }

        dialogType = new ChoiceDialog(getActivity(), mList, new ChoiceDialog.HandleBtn() {
            @Override
            public void handleOkBtn(ChoiceBean type) {

                dialogType.dismiss();
                if (type != null) {
                    accountTyp = type.getId();
                    tv_merchant_type.setText(type.getName());
                    if (type.getId() == 1) {
                        //对公
                        rl_account_phone.setVisibility(View.GONE);
                        ll_private.setVisibility(View.GONE);
                        rl_public.setVisibility(View.VISIBLE);
                        tv_account_no_title.setText("结算卡号");
                        accountTyp = 1;

                    } else {
                        //对私
                        rl_account_phone.setVisibility(View.VISIBLE);
                        tv_account_no_title.setText("银行卡号");
                        ll_private.setVisibility(View.VISIBLE);
                        rl_public.setVisibility(View.GONE);
                        accountTyp = 2;

                    }
                }


            }
        });
        DialogHelper.resize(getActivity(), dialogType);
        dialogType.show();


    }

    private void submit() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("accountType", 2);
            map.put("bkCardPhoto", mBkCardPhoto);
            map.put("accountCode", mEtAccountNo.getText().toString().trim());
            map.put("bankId", mBankId);
            map.put("province", mProvinceCode);
            map.put("city", mCityCode);
            map.put("bankBranchId", mBankBranchId);
            map.put("bankName", mBranchName);
            map.put("contactLine", mContactLine);
            //ServerClient.newInstance(MyApplication.getContext()).editBankCard(MyApplication.getContext(),Constants.TAG_EDIT_CARD,map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void choice() {
        if (!ClickUtil.isNotFastClick()){
            return;
        }
        final String status = Environment.getExternalStorageState();
        SelectPicPopupWindow picPopupWindow = new SelectPicPopupWindow(getActivity(), new SelectPicPopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    try {
                        startCamrae();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                try {
                    if (status.equals(Environment.MEDIA_MOUNTED)) {
                        takeImg();
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.tx_sd_pic));
                    }
                } catch (Exception e) {
                }
            }
        });
        picPopupWindow.showAtLocation(iv_pic_six, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void startCamrae() throws Exception {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String root = AppHelper.getImgCacheDir();
        imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";

        tempFile = new File(imageUrl);
        if (!tempFile.getParentFile().exists()){
            tempFile.getParentFile().mkdirs();
        }
        if (Build.VERSION.SDK_INT >= 24) {
            //如果是则使用FileProvider
            originalUri = FileProvider.getUriForFile(getActivity(), BuildConfig.APPLICATION_ID + ".fileProvider", tempFile);
        } else {
            originalUri = Uri.fromFile(tempFile);
        }
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
    }

    private void takeImg() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                //获取相册图片
                case REQUEST_CODE_TAKE_PICTURE:
                    if (data.getData() == null) {
                        return;
                    }
                    String path = AppHelper.getPicPath(data.getData());
                    Bitmap bitmap = null;
                    if (path != null) {
                        /*File file = new File(path);
                        int MAX_POST_SIZE = 8 * 1024 * 1024;
                        if (file.exists() && file.length() > MAX_POST_SIZE) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.tv_pic_than_5MB));
                            return;
                        } else {
                            bitmap = ImagePase.readBitmapFromStream(path);
                        }*/
                        bitmap = ImagePase.readBitmapFromStream(path);
                        if (bitmap != null) {
                            picSixPath = path;
                            setBitmap(ly_sixth, ly_phone_six, bitmap);
                            uploadImage(picSixPath, "bankCardImg");
                        }
                    }
                    break;

                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    String pathPhoto = AppHelper.getPicPath(originalUri);
                    LogUtil.d("path=" + pathPhoto);
                    Bitmap bitmap_pci = null;
                    File file = new File(pathPhoto);
                    if (!file.exists()) {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                        }
                    }
                    bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                    if (bitmap_pci != null) {
                        picSixPath = pathPhoto;
                        setBitmap(ly_sixth, ly_phone_six, bitmap_pci);
                        uploadImage(picSixPath, "bankCardImg");
                    }
                    break;
                default:
                    break;
            }
        }
        if (requestCode == Constants.REQUEST_BANK_LIST && resultCode == Activity.RESULT_OK) {
            BankListBean.DataEntity dataBean = (BankListBean.DataEntity) data.getExtras().getSerializable(Constants.RESULT_BANK_LIST);
            mBankId = dataBean.getBankId();
            mBankName = dataBean.getBankName();
            mTvCardBank.setText(dataBean.getBankName());
            mTvCardBankBranch.setText("");
//            setButtonView();
        }
        if (requestCode == Constants.REQUEST_BANK_BRANCH_LIST && resultCode == Activity.RESULT_OK) {
            BranchListBean.DataEntity dataBean = (BranchListBean.DataEntity) data.getExtras().getSerializable(Constants.RESULT_BANK_BRANCH_LIST);
            mBankBranchId = dataBean.getBankBranchId();
            mContactLine = dataBean.getContactLine();
            mBranchName = dataBean.getBankBranchName();
            mTvCardBankBranch.setText(dataBean.getBankBranchName());
//            setButtonView();
        }

        if ((requestCode == this.INTENT_REQUEST_CODE_CHANGECARDUPLOADIMAGEACTIVITY ||requestCode == INTENT_REQUEST_CODE_CHANGECARDUPLOADIMAGEACTIVITY_CAMERA)&& resultCode == Activity.RESULT_OK){
            //上传变更函返回
            mPicBgsqNet = data.getStringExtra(Constants.INTENT_NAME_PIC_BGSQ_NET);///pic/mch/2020/11/05/3600ac44-fcb4-43d7-857f-3330a4fa7a92.jpg
            LogUtil.d("Jeremy","mPicBgsqNet:"+mPicBgsqNet);
            if (!TextUtils.isEmpty(mPicBgsqNet)){
                mIsUploadImgSuccess = true;
                mLlBgsqPrivate.setVisibility(View.VISIBLE);
                String imageUrl = Constants.BASE_URL + mPicBgsqNet;
                Picasso.get().load(imageUrl).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(mIvPicNewPrivate);
                if (requestCode == INTENT_REQUEST_CODE_CHANGECARDUPLOADIMAGEACTIVITY){
                    cardDialog("card");

                }
            }
        }
    }

    private void uploadImage(String path, final String type) {
        if (!NetworkUtils.isNetworkAvailable(getActivity())) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
            return;
        }
        DialogUtil.safeShowDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);

        String url = Constants.BASE_URL + "/app/merchant/upload/file";
        PostFormBuilder post = OkHttpUtils.post();
        String pathCompress = null;
        if (!TextUtils.isEmpty(path)) {
            //pathCompress = path.substring(0, path.lastIndexOf(".")) + "(1).jpg";
            pathCompress = AppHelper.getImageCacheDir(path);
            ImageFactory.compressPicture(path, pathCompress);
            post = post.addFile(type, type, new File(pathCompress));
        }
        final String finalPathCompress = pathCompress;
        post.url(url).build().
                connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                e.printStackTrace();
                DialogUtil.safeCloseDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
                ToastUtil.showToastShort("上传失败");
                FileUtils.deleteFile(finalPathCompress);
            }

            @Override
            public void onResponse(String response, int id) {
                LogUtil.i("zhouwei", "response" + response);
                FileUtils.deleteFile(finalPathCompress);
                DialogUtil.safeCloseDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
                Gson gson = new Gson();
                ImageBean imageBean = gson.fromJson(response, ImageBean.class);
                if (imageBean != null) {
                    if (imageBean.isStatus()) {
                        ImageBean.DataEntity imageInfo = imageBean.getData();
                        mBkCardPhoto = imageInfo.getBankCardImg();
//                        setButtonView();
                        if (imageInfo.getBankCard() != null) {
                            mEtAccountNo.setText(imageInfo.getBankCard().getNumber());
                            mTvCardBank.setText(imageInfo.getBankCard().getBankname());
                            mBankId = StringUtils.paseStrToLong(imageInfo.getBankCard().getBankno());
//                            setButtonView();
                        }
                    } else {
                        if (imageBean.getError() != null && imageBean.getError().getMessage() != null) {
                            ToastUtil.showToastShort(imageBean.getError().getMessage());
                        }
                    }
                }
            }
        });
    }

    private void setBitmap(LinearLayout ll, RelativeLayout rl, Bitmap bitmap) {
        ll.setVisibility(View.GONE);
        rl.setBackground(new BitmapDrawable(bitmap));
    }

    public void setData(MerchantInfoBean.DataBean data, String type, int changeAcountCount, String merchantId) {
        this.mData = data;
        this.mType = type;
        mChangeAcountCount = changeAcountCount;
        mMerchantId = merchantId;
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_ADDRESS)) {
            DialogUtil.safeCloseDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
            AddressBean msg = (AddressBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.GET_ADDRESS_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((ChangeCompanyCardActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.GET_ADDRESS_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        area(msg);
                    }
                    break;
            }
        }


        //短信验证码
        if (event.getTag().equals(Constants.TAG_SEND_MSG)) {
            DialogUtil.safeCloseDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
            SendMsgBean msg = (SendMsgBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((ChangeCompanyCardActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (cardDialog != null)
                        cardDialog.setTimerStart();
                    break;
            }
        }


        //修改结算卡
        if (event.getTag().equals(Constants.TAG_BANK_ACCOUNT_INFO)) {
            DialogUtil.safeCloseDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
            SendMsgBean msg = (SendMsgBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((ChangeCompanyCardActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    ToastUtil.showToastShort(UIUtils.getString(R.string.submit_success));
                    MyApplication.isRefresh = true;
                    getActivity().finish();
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_QUERY_ENABLED_CHANGE+"_"+getClass().getName())) {
            DialogUtil.safeCloseDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
            QueryEnabledChangeBean msg = (QueryEnabledChangeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.QUERY_ENABLED_CHANGE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    ((ChangeCompanyCardActivity) getActivity()).loginDialog(getActivity(), msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.QUERY_ENABLED_CHANGE_TRUE://请求成功
                    QueryEnabledChangeBean.Data data = msg.getData();
                    if (!data.isEditMchAccount()){
                        setExitEable(false);
                    }
                    break;
            }
        }

        if (event.getTag().equals(Constants.ORANGE_UPDATE)) {
            DialogUtil.safeCloseDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    ToastUtil.showToastShort("申请成功");

                    String stateStr = (String) (event.getMsg());
                    Log.e("1209", "onDataEvent: " + stateStr);
                    int state = -1;
                    if (stateStr != null) {
                        try {
                            state = Integer.valueOf(stateStr);
                            mData.getBankAccount().setProductMark(stateStr); //更新mData
                            MyApplication.isRefresh=true;
                            tv_orange_state.setText(getTypeDes(state));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Constants.ON_EVENT_FALSE://请求失败
                    ToastUtil.showToastShort((String) event.getMsg());
                    break;
            }
        }

    }

    String content;
    String title;

    private void cardDialog(final String type) {

        MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();
        title = "结算信息更改验证";
        content = "您的操作有敏感信息更改，我们会发送短信验证码至" + mchDetail.getTelphone().replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");

        cardDialog = new LegalDialog(getActivity(), title,
                content, "确认", "取消"
                , type, new LegalDialog.HandleBtn() {
            @Override
            public void handleOkBtn(String s) {
                getData(s);//修改结算卡信息
                cardDialog.dismiss();
            }

            @Override
            public void handleCancleBtn() {
                cardDialog.dismiss();
            }
        });
        cardDialog.setOnClickSend(new LegalDialog.OnClickSend() {
            @Override
            public void onClickSend() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
                    Map<String, Object> map = new HashMap<>();
//                    MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();
//                    map.put("mobile", mchDetail.getTelphone());
                    map.put("bizType", "MCH_BANK_ACCOUNT_UPDATE");
                    map.put("bizId", mData.getMerchantId());
                    ServerClient.newInstance(MyApplication.getContext()).send(MyApplication.getContext(), Constants.TAG_SEND_MSG, map);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                }
            }
        });
        DialogHelper.resize(getActivity(), cardDialog);
        cardDialog.show();

    }

    private void getData(String sms) {


        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {

            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", mData.getMerchantId());

            if (accountTyp == 1) {
                //对公

                if (StringUtils.isEmptyOrNull(et_company.getText().toString().trim())) {

                    ToastUtil.showToastShort("请输入公司名称");
                    et_company.setFocusable(true);
                    et_company.setFocusableInTouchMode(true);
                    et_company.requestFocus();
                    return;
                } else {
                    map.put("accountName", et_company.getText().toString().trim());//公司名称
                }

            } else {
                map.put("accountName", mTvAcountName.getText().toString().trim());//开户人姓名
                map.put("tel", et_account_phone.getText().toString().trim());//银行卡预留手机号
            }

            map.put("idCardType", idCardType);//持卡人证件类型
            map.put("idCard", mTvCardId.getText().toString().trim());//持卡人证件号码
            map.put("accountCode", mEtAccountNo.getText().toString().trim());//银行卡号

            Log.i("zhouwei", "银行===" + mEtAccountNo.getText().toString().trim());
            map.put("accountExpiredFlag", isCardValid);//开户人证件是否长期有效
            map.put("accountExpiredDate", endTime);//开户人证件有效期
            map.put("accountExpiredBegin", startTime);//开户人证件开始日期


            map.put("bankId", mBankId);//关联银行
            map.put("province", mProvinceCode);
            map.put("city", mCityCode);
            map.put("accountType", accountTyp);

            map.put("bankBranchId", mBankBranchId);//开户支行id
            map.put("contactLine", mContactLine);
            if (!StringUtils.isEmptyOrNull(mBranchName)) {

                map.put("bankName", mBranchName);//开户支行名称

            }
            // map.put("address", "");//持卡人地址
            map.put("smsCode", sms);//短信验证码
            Map<String, Object> mapInfo = new HashMap<>();
            mapInfo.put("smsCode", sms);
            mapInfo.put("merchantId", mData.getMerchantId());
            mapInfo.put("account", map);
            if (!TextUtils.isEmpty(mPicBgsqNet)){
                Map<String, Object> merchantDetailMap = new HashMap<>();
                merchantDetailMap.put("accountChangePhoto",mPicBgsqNet);
                mapInfo.put("merchantDetail",merchantDetailMap);
            }
            cardDialog.dismiss();
            DialogUtil.safeShowDialog(((ChangeCompanyCardActivity) getActivity()).mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).bankaccountInfo(MyApplication.getContext(), Constants.TAG_BANK_ACCOUNT_INFO, mapInfo);
        } else {

            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (cardDialog != null) {

            cardDialog.dismiss();
        }
    }

    private void area(AddressBean addressBean) {
        mCityPicker = new ChoseCityPicker(getActivity(), addressBean);
        mCityPicker.setOnGetAddress(new ChoseCityPicker.OnGetAddress() {
            @Override
            public void getAddress(String province, String city, String area) {
                //获取省市区地址
                mTvCardAddress.setText(province + " " + city);
                //mData.setProvinceCnt(province);
                //mData.setCityCnt(city);
//                setButtonView();
            }
        });
        mCityPicker.setOnGetAddressCode(new ChoseCityPicker.OnGetAddressCode() {
            @Override
            public void getAddressCode(String province, String city, String area) {
                //获取省市区code
                mProvinceCode = province;
                mCityCode = city;
                //mData.setProvince(province);
                // mData.setCity(city);
                mTvCardBankBranch.setText("");
            }
        });
        mCityPicker.show();
    }

    /*public boolean getButtonState() {
        return mEtAccountNo.getText().toString().trim().length() == 0
                || mTvCardBank.getText().toString().trim().length() == 0 || mTvCardAddress.getText().toString().trim().length() == 0
                || mTvCardBankBranch.getText().toString().trim().length() == 0 || TextUtils.isEmpty(mBkCardPhoto);
    }

    private void setButtonView() {
        if (getButtonState()) {
            //  mShadowButton.setVisibility(View.GONE);
            // mBtnUnable.setVisibility(View.VISIBLE);
        } else {
            // mShadowButton.setVisibility(View.VISIBLE);
            // mBtnUnable.setVisibility(View.GONE);
        }
    }*/

    private void setEdittextListener(final EditText editText, final ImageView imageView) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    imageView.setVisibility(View.VISIBLE);
                } else {
                    imageView.setVisibility(View.INVISIBLE);
                }

            }
        });
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {//获得焦点

                    if (editText.getText().length() > 0) {
                        imageView.setVisibility(View.VISIBLE);
                    } else {
                        imageView.setVisibility(View.INVISIBLE);
                    }
                } else {//失去焦点
                    imageView.setVisibility(View.INVISIBLE);
                }
            }
        });
    }
}
