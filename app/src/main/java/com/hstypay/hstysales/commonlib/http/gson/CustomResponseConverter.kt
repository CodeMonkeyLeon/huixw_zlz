package com.hstypay.hstysales.commonlib.http.gson

import android.util.Log
import com.google.gson.Gson
import com.google.gson.TypeAdapter
import org.json.JSONObject
import java.io.IOException
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Converter

internal class CustomResponseConverter<T>(
        private val gson: Gson,
        private val adapter: TypeAdapter<T>
) : Converter<ResponseBody, T> {

    @Throws(IOException::class)
    override fun convert(value: ResponseBody): T? {
        try {
            var body = value.string()
            val json = JSONObject(body)
            val status = json.optBoolean(STATUS)
            val data = json.opt(DATA)
            if (data == null) {
                json.putOpt(DATA, "{}")
            }
            return adapter.fromJson(json.toString())
        } catch (e: Exception) {
            throw RuntimeException(e.message)
        } finally {
            value.close()
        }
    }

    companion object {
        const val STATUS = "status"
        const val ERROR_MSG = "errorMsg"
        const val MESSAGE = "message"
        const val DATA = "data"
    }
}
