package com.hstypay.hstysales.commonlib.base

import android.app.Activity
import java.util.*

/**
 * Description: Activity管理类
 * @author: zengwendi
 * @date: 2020/04/09
 * Time: 19:05
 */

class AppManager {
    private val activityStack : Stack<Activity> = Stack()

    companion object {
        val instance by lazy {
            AppManager()
        }
    }

    fun addActivity(activity: Activity) {
        activityStack.add(activity)
    }

    fun removeActivity(activity: Activity) {
        activityStack.remove(activity)
    }

    private fun finishAllActivity() {
        for(activity in activityStack) {
            activity.finish()
        }
        activityStack.clear()
    }

}