package com.hstypay.hstysales.commonlib.base


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hstypay.hstysales.base.BaseFragment
import com.hstypay.hstysales.commonlib.http.HttpResponse
import com.hstypay.hstysales.commonlib.utils.ToastUtils
import com.hstypay.hstysales.commonlib.utils.Utils
import com.hstypay.hstysales.commonlib.widget.LoadingDialog

/**
 * @Author dean.zeng
 * @Description BaseActivity
 * @Date 2020-04-01 17:58
 **/
abstract class AppFragment<VM : BaseViewModel> : BaseFragment() {

    protected lateinit var mViewModel: VM


    private var mLoadingDialog: LoadingDialog? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getLayoutId(), null)
    }


    abstract fun getLayoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        mViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application).create(Utils.getClass(this))
        mViewModel = ViewModelProviders.of(this).get(Utils.getClass(this))
        mViewModel.loadState.observe(this, observer)
        initData()
        initView()
    }

    abstract fun initData()

    abstract fun initView()

    /**
     * toast提示
     */
    fun showToast(c: CharSequence) {
        ToastUtils.showShortToast(c)
    }

    /**
     * 弹窗提示
     */
    fun showModal(msg: String, OnClickListener: (() -> Unit?)? = null) {
        (activity as AppActivity<*>).showModal(msg,OnClickListener)
    }

    /**
     * 显示loading
     */
    fun showLoading() {
        if (mLoadingDialog == null) {
            mLoadingDialog = LoadingDialog(context!!).showLoading()
        }
        try {
            mLoadingDialog?.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mLoadingDialog?.dismiss()

    }

    /**
     * 隐藏loading
     */
    fun hideLoading() {
        mLoadingDialog?.dismiss()
    }

    /**
     * 分发应用状态
     */
    private val observer by lazy {
        Observer<HttpResponse> {
            it?.let {
                when (it.state) {
                    HttpResponse.SHOW_LOADING -> showLoading()
                    HttpResponse.HIDE_LOADING -> hideLoading()
                    HttpResponse.SHOW_ERROR -> {
                        hideLoading()
                        showModal(it.msg)
                    }

                }
            }
        }
    }

}