package com.hstypay.hstysales.commonlib.http

import android.os.Build
import com.google.gson.GsonBuilder
import com.hstypay.hstysales.app.MyApplication
import com.hstypay.hstysales.utils.AppHelper
import com.hstypay.hstysales.utils.LogUtil
import com.hstypay.hstysales.utils.MD5
import com.hstypay.hstysales.utils.UrlDecode
import okhttp3.*
import java.io.IOException
import java.util.*

/**
 * @Author dean.zeng
 * @Description FormBody转json
 * @Date 2020/4/26 17:40
 **/
class FormBodyToJsonInterceptor : Interceptor {


    private val gson = GsonBuilder()
            .setPrettyPrinting()
            .disableHtmlEscaping()
            .create()

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val requestBody = request.body()
        val times = System.currentTimeMillis().toString()
        request = request.newBuilder()
                .addHeader("hs-data-random", times)
                .addHeader("api-version", "1.0.0")
                .addHeader("User-Agent", "brand/"+getBrand() +" model/"+getModel()+ AppHelper.getUserAgent(MyApplication.getContext()))
                .build()
        if (requestBody is FormBody) {
            val parameter = TreeMap<String, String>()
            val formBody = requestBody as FormBody?
            for (i in 0 until formBody!!.size()) {
                val key = UrlDecode.decode(formBody.encodedName(i))
                val value = UrlDecode.decode(formBody.encodedValue(i))
                parameter[key] = value
            }
            val newJsonParams = createAppSign(parameter,times)
            request = request.newBuilder()
                    .post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), newJsonParams))
                    .build()
        }
        return chain.proceed(request)
    }


    private fun createAppSign(params: TreeMap<String, String>, random: String): String? {
        var content = ""
        if (params.isNotEmpty()) {
            content = gson.toJson(params)
            LogUtil.i("Jeremy", "json数据==$content")
        }
        val key = MyApplication.getSignKey() + MyApplication.getSkey()
        val mMd5SignKeys = MD5.md5s(key)
        val resMap: MutableMap<String, String> = HashMap()
        val sings = MD5.md5s(mMd5SignKeys + content + random)
        resMap["content"] = content
        resMap["sign"] = sings
        return gson.toJson(resMap)
    }


    private fun getBrand(): String? {
        return Build.BRAND.replace(" ".toRegex(), "-")
    }

    private fun getModel(): String? {
        return Build.MODEL.replace(" ".toRegex(), "-")
    }
}
