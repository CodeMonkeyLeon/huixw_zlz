package com.hstypay.hstysales.commonlib.base

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.hstypay.hstysales.app.MyApplication
import com.hstypay.hstysales.bean.ApiRes
import com.hstypay.hstysales.commonlib.http.HttpResponse
import kotlinx.coroutines.*

/**
 * @Author dean.zeng
 * @Description BaseViewModel
 * @Date 2020-04-09 16:39
 **/
open class BaseViewModel : ViewModel() {


    val loadState by lazy {
        MutableLiveData<HttpResponse>()
    }

    private val viewModelJob = SupervisorJob()


    val uiScope by lazy {
        CoroutineScope(Dispatchers.Main + viewModelJob)
    }


    fun <T> requestSync(
            block: suspend CoroutineScope.() -> ApiRes<T>,
            success: ((t: T?) -> Unit)? = null,
            error: ((Exception) -> Unit)? = null,
            httpResponse: HttpResponse? = null) {
        val exceptionHandler = CoroutineExceptionHandler { _, exception ->
            exception.printStackTrace()
            error?.let {
                it(Exception())
            }
            if (httpResponse!!.isShowError) {//隐藏加载dialog
                httpResponse.state = HttpResponse.SHOW_ERROR
                httpResponse.msg = "网络异常"
                loadState.postValue(httpResponse)
            }
        }

        var httpResponse = httpResponse
        if (httpResponse == null) {
            httpResponse = HttpResponse.build {
                isLoading = true
                isShowError = true
            }
        }
        if (httpResponse.isLoading) {//显示加载dialog
            httpResponse.state = HttpResponse.SHOW_LOADING
            loadState.value=httpResponse
        }
        uiScope.launch(context = Dispatchers.Main + exceptionHandler) {
            try {
                val result = block()
                if (httpResponse.isLoading) {//隐藏加载dialog
                    httpResponse.state = HttpResponse.HIDE_LOADING
//                    loadState.postValue(httpResponse)
                    loadState.value=httpResponse
                }
                if (result.status) {
                    success?.let { it(result.data) }
                } else {
                    if (result.error != null) {
                        if (result.error.code != null && result.error.code == MyApplication.getFreeLogin()) {
                            httpResponse.state = HttpResponse.RE_LOGIN
                        } else {
                            if (httpResponse.isShowError) {//显示错误弹窗
                                httpResponse.state = HttpResponse.SHOW_ERROR
                            }
                        }
                        httpResponse.msg = result.error.message
                        loadState.value=httpResponse
                        error?.let {
                            it(Exception())
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                error?.let {
                    it(e)
                }
                if (httpResponse!!.isShowError) {//隐藏加载dialog
                    httpResponse.state = HttpResponse.SHOW_ERROR
                    httpResponse.msg = "网络异常"
                    loadState.value=httpResponse
                }
            }

        }
    }


    override fun onCleared() {
        viewModelJob.cancel()
        super.onCleared()
    }

}