package com.hstypay.hstysales.commonlib.base


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.hstypay.hstysales.base.BaseActivity
import com.hstypay.hstysales.commonlib.http.HttpResponse
import com.hstypay.hstysales.commonlib.utils.ToastUtils
import com.hstypay.hstysales.commonlib.utils.Utils
import com.hstypay.hstysales.commonlib.widget.LoadingDialog
import com.hstypay.hstysales.utils.StatusBarUtil

/**
 * @Author dean.zeng
 * @Description BaseActivity
 * @Date 2020-04-01 17:58
 **/
abstract class AppActivity<VM : BaseViewModel> : BaseActivity() {

    protected lateinit var mViewModel: VM


    private var mLoadingDialog: LoadingDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        StatusBarUtil.setTranslucentStatus(this)
        AppManager.instance.addActivity(this)
//        mViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application).create(Utils.getClass(this))
        mViewModel= ViewModelProviders.of(this).get(Utils.getClass(this))
        mViewModel.loadState.observe(this, observer)
        initData()
        initView()
    }

    abstract fun initData()

    abstract fun initView()

    /**
     * toast提示
     */
    fun showToast(c: CharSequence) {
        ToastUtils.showShortToast(c)
    }

    /**
     * 弹窗提示
     */
    fun showModal(msg: String, OnClickListener: (() -> Unit?)? = null) {
        showCommonNoticeDialog(this,null,msg) {
            OnClickListener?.let { it() }
        }

    }

    /**
     * 显示loading
     */
    fun showLoading() {
        if (mLoadingDialog == null) {
            mLoadingDialog = LoadingDialog(this).showLoading()
        }
        try {
            mLoadingDialog?.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        AppManager.instance.removeActivity(this)
    }

    /**
     * 隐藏loading
     */
    fun hideLoading() {
        mLoadingDialog?.dismiss()
    }
    abstract fun getLayoutId(): Int
    /**
     * 分发应用状态
     */
    private val observer by lazy {
        Observer<HttpResponse> {
            it?.let {
                when (it.state) {
                    HttpResponse.SHOW_LOADING -> showLoading()
                    HttpResponse.HIDE_LOADING -> hideLoading()
                    HttpResponse.RE_LOGIN -> {
                        hideLoading()
                        getLoginDialog(this,it.msg)
                    }
                    HttpResponse.SHOW_ERROR -> {
                        hideLoading()
                        showModal(it.msg)
                    }

                }
            }
        }
    }

}