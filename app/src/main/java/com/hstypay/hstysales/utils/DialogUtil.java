package com.hstypay.hstysales.utils;

import android.app.Activity;
import android.app.Dialog;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.widget.SelectDialog;
import com.umeng.socialize.utils.Log;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.utils
 * @创建者: Jeremy
 * @创建时间: 2017/12/1 20:18
 * @描述: ${TODO}
 */

public class DialogUtil {
    public static void safeCloseDialog(Dialog dialog) {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
                dialog = null;
            }
        } catch (Exception var2) {
            Log.e("SocializeUtils", "dialog dismiss error", var2);
        }

    }

    public static void safeShowDialog(Dialog dialog) {
        try {
            if (dialog != null && !dialog.isShowing()) {
                dialog.show();
            }
        } catch (Exception var2) {
            Log.e("SocializeUtils", "dialog show error", var2);
        }
    }

    public static void noticeDialog(final Activity activity, final String msg) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SelectDialog info = new SelectDialog(activity, msg, UIUtils.getString(R.string.btn_know), null, new SelectDialog.HandleBtn() {
                    @Override
                    public void handleOkBtn() {
                    }

                    @Override
                    public void handleCancleBtn() {
                    }
                });
                DialogHelper.resize(activity, info);
                info.show();
            }
        });
    }
}
