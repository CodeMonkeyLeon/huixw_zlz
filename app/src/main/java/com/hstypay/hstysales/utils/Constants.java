package com.hstypay.hstysales.utils;

import android.text.TextUtils;

import com.hstypay.hstysales.BuildConfig;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.utils
 * @创建者: Jeremy
 * @创建时间: 2017/6/27 14:02
 * @描述: ${TODO}
 */
public class Constants {
    public static String BASE_URL= BuildConfig.BASE_URL;//正式环境
    public static String BASE_KT_URL = BASE_URL + "/";
//正式环境OEM的配置orgId:
    public static String SERVICE_PROVIDER_ID = "1010000350";//正式登录渠道号
    public static String ORG_ID = ConfigUtil.getReleaseOrgId();//"1010000611";//OEM正式定制的渠道类型
    public static String ORG_TYPE = ConfigUtil.getReleaseOrgType();// "1";//OEM正式定制的渠道类型

    public static String REGISTER_URL = BASE_URL + "/static/merchant-register/register.html#/register";
    public static String INFO_URL = BASE_URL + "/static/merchant-register/register.html#/certification-type?" + AppHelper.getHtmlAndString() + "platform=hxw" + "&userId=";//进件
    //    public static String REGISTER_URL = "http://192.168.160.133:8080/register.html/#/register"+AppHelper.getHtmlString();//注册
//    public static String INFO_URL = "http://192.168.160.133:8080/register.html/#/certification-type?"+AppHelper.getHtmlAndString()+"userId=";//进件
//    public static String REGISTER_URL = BASE_URL + "/static/WebH5/register_salesman/trader.html"+AppHelper.getHtmlString();
    public static String H5_MSG_DETAIL_URL = BASE_URL + "/static/WebH5/news/news.html?" + AppHelper.getHtmlAndString() + "newsId=";
    public static String H5_CONTACT_US_URL = BASE_URL + "/static/WebH5/opinion_feedback/opinion_feedback.html" + AppHelper.getHtmlString();
    public static String URL_COOPERATION_PROTOCOL = BASE_URL + "/static/WebH5/hwc_cooperation_agreement/hwc_cooperation_agreement.html" + AppHelper.getHtmlString();
    public static String HS_APP_MANAGE_PLATFORM = "HS_APP_MANAGE_PLATFORM_";//合作伙伴类型
    public static String HS_APP_SERVICE_PROVIDER = "HS_APP_SERVICE_PROVIDER_";//省、市运营
    public static String HS_APP_MERCHANT = "HS_APP_MERCHANT_";//商户
    public static String APP_CODE = HS_APP_SERVICE_PROVIDER;


    public static void setBaseUrl(String url, String serviceProviderId, String orgId, String orgType, String appCode, String h5Url) {
        BASE_URL = url;
        BASE_KT_URL = BASE_URL + "/";//测试环境
        SERVICE_PROVIDER_ID = serviceProviderId;
        ORG_ID = orgId;
        ORG_TYPE = orgType;
        if ("1".equals(appCode)) {
            APP_CODE = HS_APP_MANAGE_PLATFORM;
        }
        if ("2".equals(appCode)) {
            APP_CODE = HS_APP_SERVICE_PROVIDER;
        }
        if ("3".equals(appCode)) {
            APP_CODE = HS_APP_MERCHANT;
        }

        String h5UrlString;
        if (TextUtils.isEmpty(h5Url)) {
            h5UrlString = BASE_URL;
        } else {
            h5UrlString = h5Url;
        }
        LogUtil.d("hhhhh---" + h5UrlString);
        REGISTER_URL = h5UrlString + "/static/merchant-register/register.html#/register";
        INFO_URL = h5UrlString + "/static/merchant-register/register.html#/certification-type?" + AppHelper.getHtmlAndString() + "platform=hxw" + "&userId=";//进件
        H5_MSG_DETAIL_URL = h5UrlString + "/static/WebH5/news/news.html?" + AppHelper.getHtmlAndString() + "newsId=";
        H5_CONTACT_US_URL = h5UrlString + "/static/WebH5/opinion_feedback/opinion_feedback.html" + AppHelper.getHtmlString();
        URL_COOPERATION_PROTOCOL = h5UrlString + "/static/WebH5/hwc_cooperation_agreement/hwc_cooperation_agreement.html" + AppHelper.getHtmlString();
    }

//    public static final String BASE_URL="https://salesman.hstydev.com";//开发环境
//    public static final String BASE_URL="https://usalesman.hstytest.com";//联合测试环境
//    public static final String BASE_URL="https://gsalesman.hstypay.com";//灰度环境
//    public static final String BASE_URL="http://192.168.6.123:29304";
//    public static final String BASE_URL="https://salesman-old.hstytest.com";//old测试环境
//    public static final String BASE_URL="https://salesman.hstytest.com";//测试环境
//    public static final String BASE_URL="https://salesman.hstypay.com";//正式环境

//    public static final String REGISTER_URL = BASE_URL+"/static/WebH5/register_salesman/trader.html"+AppHelper.getHtmlString();
//    public static final String H5_MSG_DETAIL_URL = BASE_URL+"/static/WebH5/news/news.html?"+AppHelper.getHtmlAndString()+"newsId=";
//    public static final String H5_CONTACT_US_URL = BASE_URL+"/static/WebH5/opinion_feedback/opinion_feedback.html"+AppHelper.getHtmlString();
//    public static final String URL_COOPERATION_PROTOCOL = BASE_URL+"/static/WebH5/hwc_cooperation_agreement/hwc_cooperation_agreement.html"+AppHelper.getHtmlString();
    /**
     * 允许所有日志输出
     */
    public static final String APP_USERNAME = "app_username";
    public static final String APP_MERCHANT_ID = "app_merchant_id";//商户号
    public static final String GETUI_CLIENT_ID = "getui_client_id";//个推设备id
    public static final String PAY_GOODS_NAME = "pay_goods_name";//个推设备id
    public static final String ACTION_MERCHANT_STATUS = "action_merchant_status";
    public static final String SUBMIT_CHECKING = "submit_checking";
    public static final String ACTION_PUSH_STATUS = "intent.action.ACTION_PUSH_STATUS";
    public static final String NEED_CHANGE_PWD = "need_change_pwd";
    public static final String REGISTER_ALLOW_CLOSE = "register_allow_close";
    public static final String APP_META_DATA_KEY = "InstallChannel";
    public static final String REQUEST_CLIENT_APP = "HPAY_AND";
    public static final String PAY_CLIENT_APP = "HPAY_ANDROID";

    public static boolean isDebug = true;
    public static final String SP_SAVE_FILE = "hstysales";
    public static final String SP_SAVE_FILE_STAY = "hstysales_stay";

    public static final String SP_IS_NOT_FIRST_USE = "sp_is_not_first_use";//是否不是第一次使用
    public static final String SP_LOGIN_NAME = "sp_login_name";//登录名
    public static final String SP_USER_NAME = "sp_user_name";//用户名
    public static final String SP_IS_UPDATE = "sp_is_update";//是否需要更新
    public static final String SP_AUTO_STATUS = "sp_auto_status";//免密登录状态

    public static final String SP_LOGIN_TELEPHONE = "sp_login_telephone";//绑定的手机号
    public static final String SP_SIGN_KEY = "sp_sign_key";
    public static final String SP_LOGIN_COOKIE = "sp_login_cookie";
    public static final String SP_REAL_NAME = "sp_real_name";//业务员名字
    public static final String SP_SERVICE_PROVIDER_NAME = "sp_service_provider_name";//省运营名字
    public static final String SP_SERVICE_CHANNEL_NAME = "sp_service_channel_name";//市运营名字
    public static final String SP_ROLE_CODE = "sp_role_code";//登录账户角色
    public static final String SP_SERVICE_PROVIDER = "sp_service_provider";//服务商code
    public static final String SP_HAS_NO_INFO = "sp_has_no_info";//存在未进件商户
    public static final String SP_NOTICE_CLOSE = "sp_notice_close";//首页消息是否关闭
    public static final String SP_ADMIN_CHECK_STORE_ID = "sp_admin_check_store_id";
    public static final String SP_CONFIG = "sp_config";//配置信息
    public static final String SP_CACHE_INFO = "sp_cache_info";//进件缓存信息
    /**
     * 配置信息
     */
    public static final String CONFIG_ABOUNT = "ABOUNT";//关于我们
    public static final String CONFIG_INDEX = "INDEX";//首页
    public static final String CONFIG_INVIATION = "INVIATION";//我的邀请码
    public static final String CONFIG_MERCHANT = "MERCHANT";//商户
    public static final String CONFIG_NOTICE = "NOTICE";//通知
    public static final String CONFIG_RANK = "RANK";//排名
    public static final String CONFIG_REGISTER = "REGISTER";//注册
    public static final String CONFIG_SETTING = "SETTING";//设置
    public static final String CONFIG_MCH_DEVICE_ORDER = "MCH_DEVICE_ORDER";//商户硬件订单
    public static final String CONFIG_APPLI_MANAGER = "APPLI_MANAGER";//商户应用开通管理
    public static final String CONFIG_SHARE_DETAIL = "SHARE_DETAIL";//分润明细
    public static final String CONFIG_SHARE_STATISTICS = "SHARE_STATISTICS";//分润统计

    public static final String CONFIG_ALL = CONFIG_ABOUNT + "," + CONFIG_INDEX + "," + CONFIG_INVIATION
            + "," + CONFIG_MERCHANT + "," + CONFIG_NOTICE + "," + CONFIG_RANK + "," + CONFIG_REGISTER
            + "," + CONFIG_SETTING + "," + CONFIG_MCH_DEVICE_ORDER + "," + CONFIG_APPLI_MANAGER
            + "," + CONFIG_SHARE_DETAIL + "," + CONFIG_SHARE_STATISTICS;


    /**
     * requestCode
     */
    public static final int REQUESTCODE_SETMONEY = 1;
    public static final String REQUESTCODE_SETMONEY_REMARK = "money_remark";
    public static final String REQUESTCODE_SETMONEY_MONEY = "money_set";
    public static final int REQUEST_GET_SHOP = 2;
    public static final String REQUEST_GET_SHOP_NAME = "shop_name";
    public static final String INTENT_CASHIER_SHOP = "intent_cashier_shop";
    public static final String REGISTER_INTENT = "register_intent";
    public static final String REGISTER_INTENT_URL = "register_intent_url";
    public static final String WELCOME_TO_REGISTER = "welcome_to_register";
    public static final String WELCOME_TO_REGISTER_SECOND = "welcome_to_register_second";
    public static final String INTENT_LIST_TO_MERCHANT = "intent_list_to_merchant";//首页商户列表-商户详情
    public static final String INTENT_SEARCH_TO_MERCHANT = "intent_search_to_merchant";//商户搜索列表-商户详情
    public static final String INTENT_FRAGMENT_TO_MERCHANT = "intent_fragment_to_merchant";//商户Fragment列表-商户详情
    public static final String INTENT_OPEN_MEMBER = "intent_open_member";//开通会员卡跳转
    public static final String INTENT_SKIP_MEMBER_DETAIL = "intent_open_member";//跳转到会员卡详情页
    public static final int REQUEST_LIST_TO_MERCHANT = 3;
    public static final String RESULT_SHOP_BEAN_INTENT = "shop_bean_intent";
    public static final int REQUEST_CASHIER_CODE = 4;
    public static final int REQUEST_UPLOAD_IMAGE = 5;
    public static final String RESULT_UPLOAD_IMAGE_INTENT = "result_upload_image_intent";
    public static final String REQUEST_CASHIER_INTENT = "request_cashier_intent";//请求tag
    public static final String RESULT_CASHIER_INTENT = "result_cashier_intent";//返回tag
    public static final int REQUEST_MANAGER_INFO = 6;
    public static String INTENT_NAME_MANAGER_INFO = "intent_name_manager_info";
    public static final int REQUEST_ADD_CASHIER = 7;
    public static final String RESULT_ADD_CASHIER = "result_add_cashier";

    public static final int REQUEST_BILL_DETAIL = 8;
    public static final String RESULT_DETAIL_INTENT = "result_detail_intent";//返回tag

    public static final int REQUEST_BANK_LIST = 9;
    public static final String RESULT_BANK_LIST = "result_bank_list";
    public static final String INTENT_BANK_LIST = "intent_bank_list";
    public static final int REQUEST_BANK_BRANCH_LIST = 10;
    public static final String RESULT_BANK_BRANCH_LIST = "result_bank_branch_list";
    public static final String INTENT_BANK_BRANCH_ID = "intent_bank_branch_id";
    public static final String INTENT_BANK_ID = "intent_bank_id";
    public static final String INTENT_BANK_PROVINCE = "intent_bank_province";
    public static final String INTENT_BANK_CITY = "intent_bank_city";
    public static final int REQUEST_MERCHANT_CARD = 11;
    public static final String INTENT_MERHANT_ID = "intent_merhant_id";
    public static final String RESULT_MERCHANT_CARD_INTENT = "result_merchant_card_intent";
    public static final int REQUEST_INSTALL_PACKAGES = 12;
    public static final int REQUEST_CHANGE_NAME = 13;
    public static final String RESULT_CHANGE_NAME = "result_change_name";
    public static final String INTENT_CHANGE_NAME = "intent_change_name";
    public static final int REQUEST_DEVICE_EMPLOYEE_LIST = 14;
    public static final String RESULT_DEVICE_EMPLOYEE_LIST = "result_device_employee_list";
    public static final String INTENT_DEVICE_EMPLOYEE_LIST = "intent_device_employee_list";
    public static final String INTENT_SHORT_NAME = "intent_short_name";//商户简称/名称
    public static final int REQUEST_SYSTEM_OVERLAY_WINDOW = 15;

    public static final String INTENT_EXAMINE_APPCODE = "appCode";//应用类型的标识
    public static final String INTENT_EXAMINE_BILLNO = "billNo";//应用订购单号
    public static final String INTENT_NAME_MSG_SKIP = "INTENT_NAME_MSG_SKIP";//消息通知

    public static final String TAG_LOGIN = "tag_login";//登录标签tag
    public static final String MSG_LOGIN_TRUE = "login_true";//登录请求成功
    public static final String MSG_LOGIN_FALSE = "login_false";//登录请求失败

    public static final String TAG_LOGOUT = "tag_logout";//退出登录标签tag
    public static final String MSG_LOGOUT_TRUE = "logout_true";//退出登录请求成功
    public static final String MSG_LOGOUT_FALSE = "logout_false";//退出登录请求失败

    public static final String MSG_NET_ERROR = "net_error";//网络请求错误
    public static final String MSG_DATA_ERROR = "data_error";//数据解析错误

    public static final String TAG_SEND_PHONE = "tag_send_phone";//短信验证码tag
    public static final String SEND_PHONE_FALSE = "send_phone_false";//短信验证码失败
    public static final String SEND_PHONE_TRUE = "send_phone_true";//短信验证码成功

    public static final String TAG_GET_VOICE = "tag_get_voice";//获取语音验证码tag
    public static final String GET_VOICE_TRUE = "get_voice_true";//获取语音验证码成功
    public static final String GET_VOICE_FALSE = "get_voice_false";//获取语音验证码失败

    public static final String TAG_CHECK_CODE = "tag_check_code";//短信验证码tag
    public static final String CHECK_CODE_TRUE = "check_code_true";//短信验证码正确
    public static final String CHECK_CODE_FALSE = "check_code_false";//短信验证码错误

    public static final String TAG_RESET_PWD = "tag_reset_pwd";//重置密码成功
    public static final String RESET_PWD_TRUE = "reset_pwd_true";//重置密码成功
    public static final String RESET_PWD_FALSE = "reset_pwd_false";//重置密码失败

    public static final String IS_SUCCESS_DATA = "is_success_data";//是否是商户

    public static final String TAG_AUTO_PWD = "tag_auto_pwd";//免密登录tag
    public static final String AUTO_PWD_FALSE = "auto_pwd_false";//免密登录失败
    public static final String AUTO_PWD_TRUE = "auto_pwd_true";//免密登录成功

    public static final String TAG_GET_BINDING_MSG = "tag_get_binding_msg";//获取绑定短信验证tag
    public static final String GET_BINDING_MSG_FALSE = "get_binding_msg_false";//获取绑定短信验证失败
    public static final String GET_BINDING_MSG_TRUE = "get_binding_msg_true";//获取绑定短信验证成功

    public static final String TAG_GET_BINDING_VOICE = "tag_get_binding_voice";//获取绑定语音验证tag
    public static final String GET_BINDING_VOICE_FALSE = "get_binding_voice_false";//获取绑定语音验证失败
    public static final String GET_BINDING_VOICE_TRUE = "get_binding_voice_true";//获取绑定语音验证成功

    public static final String TAG_GET_BINDING = "tag_get_binding";//绑定手机号tag
    public static final String GET_BINDING_FALSE = "get_binding_false";//绑定手机号失败
    public static final String GET_BINDING_TRUE = "get_binding_true";//绑定手机号成功

    public static final String TAG_HOME_DATA = "tag_home_data";//首页数据tag
    public static final String HOME_DATA_FALSE = "home_data_false";//首页数据获取失败
    public static final String HOME_DATA_TRUE = "home_data_true";//首页数据获取成功

    public static final String TAG_INCOME_DETAIL = "tag_home_data";//收入详情tag
    public static final String INCOME_DETAIL_FALSE = "home_data_false";//收入详情获取失败
    public static final String INCOME_DETAIL_TRUE = "home_data_true";//收入详情获取成功

    public static final String TAG_GET_RANK_SUMMARY = "tag_get_rank_summary";//排名交易统计tag
    public static final String GET_RANK_FALSE = "get_rank_false";//排名获取失败
    public static final String GET_RANK_TRUE = "get_rank_true";//排名获取成功
    public static final String TAG_GET_RANK_TRADE = "tag_get_rank_trade";//排名tag


    public static final String TAG_GET_MERCHANT_LIST = "tag_get_merchant_list";//商户列表tag
    public static final String GET_MERCHANT_LIST_FALSE = "get_merchant_list_false";//商户列表获取失败
    public static final String GET_MERCHANT_LIST_TRUE = "get_merchant_list_true";//商户列表获取成功
    public static final String TAG_GET_MERCHANT_LIST_REFRESH = "tag_get_merchant_list_refresh";//商户列表刷新tag
    public static final String TAG_GET_MERCHANT_LIST_SEARCH = "tag_get_merchant_list_search";//商户列表筛选tag
    public static final String TAG_GET_MERCHANT_LIST_MX_SEARCH = "tag_get_merchant_list_mx_search";//月分润明细页的商户列表筛选tag

    public static final String TAG_GET_PROFIT_SUMMARY_STATISTICS = "tag_get_profit_summary_statistics";//获取城市经理分润的统计概览数据
    public static final String TAG_GET_PROFIT_LIST_STATISTICS = "tag_get_profit_list_statistics";//获取城市经理分润的统计列表数据
    public static final String TAG_GET_MONTH_PROFIT_DETAIL_LIST_STATISTICS = "tag_get_Month_Profit_Detail_List_statistics";//获取城市经理月分润明细列表数据
    public static final String TAG_GET_DAY_PROFIT_DETAIL_LIST_STATISTICS = "tag_get_day_profit_detail_list_statistics";//获取城市经理日分润明细列表数据

    public static final String TAG_QUERY_AGENT_STORE_INFO = "tag_query_agent_store_info";//查询支付宝门店代运营门店信息
    public static final String TAG_CREATE_AGENT_STORE = "tag_create_agent_store";//创建支付宝门店
    public static final String TAG_GEN_AGENT_STORE_INFO = "tag_gen_agent_store_info";//生成授权二维码


    public static final String TAG_MERCHANT_SEARCH_NO_INFO = "tag_merchant_search_no_info";//未进件商户列表tag
    public static final String TAG_MERCHANT_FRAGMENT_NO_INFO = "tag_merchant_fragment_search_no_info";//未进件商户列表tag
    public static final String TAG_MERCHANT_NO_INFO = "tag_merchant_no_info";//未进件商户列表tag
    public static final String MERCHANT_NO_INFO_FALSE = "merchant_no_info_false";//未进件商户列表获取失败
    public static final String MERCHANT_NO_INFO_TRUE = "merchant_no_info_true";//未进件商户列表获取成功

    public static final String TAG_QUEST_LOCATION = "tag_quest_location";//定位查询tag
    public static final String QUEST_LOCATION_TRUE = "quest_location_true";//定位查询成功
    public static final String QUEST_LOCATION_FALSE = "quest_location_false";//定位查询失败

    public static final String TAG_GET_MESSAGE = "tag_get_message";//获取消息tag
    public static final String GET_MESSAGE_FALSE = "get_message_false";//获取消息失败
    public static final String GET_MESSAGE_TRUE = "get_message_true";//获取消息成功

    public static final String TAG_GET_INVITATIONCODE = "tag_get_invitationcode";//获取邀请码tag
    public static final String TAG_HOME_INVITATIONCODE = "tag_home_invitationcode";//判断有无可用邀请码tag
    public static final String GET_INVITATIONCODE_FALSE = "get_invitationcode_false";//获取邀请码失败
    public static final String GET_INVITATIONCODE_TRUE = "get_invitationcode_true";//获取邀请码成功

    public static final String TAG_CODE_DETAIL = "tag_code_detail";//邀请码详情tag
    public static final String CODE_DETAIL_FALSE = "code_detail_false";//邀请码详情失败
    public static final String CODE_DETAIL_TRUE = "code_detail_true";//邀请码详情成功

    public static final String TAG_CODE_URL = "tag_code_url";//生成邀请码链接tag
    public static final String CODE_URL_FALSE = "code_url_false";//生成邀请码链接失败
    public static final String CODE_URL_TRUE = "code_url_true";//生成邀请码链接成功

    public static final String SKEY = "SKEY";
    public static final String SAUTHID = "SAUTHID";
    public static final String MERCHANT_NAME = "merchant_name";//商户名称
    public static final String IS_MERCHANT_FLAG = "is_merchant_flag";//是否是商户isMerchantFlag
    public static final String USER_ID = "user_id";//登录获取的userId
    public static final String SP_REALNAME = "sp_realname";//真实姓名
    public static final String SP_IMEI = "sp_imei";//真实姓名

    public static final String TAG_GET_MERCHANT_INFO = "tag_get_merchant_info";//商户信息tag
    public static final String GET_MERCHANT_INFO_TRUE = "get_merchant_info_true";//商户信息获取成功
    public static final String GET_MERCHANT_INFO_FALSE = "get_merchant_info_false";//商户信息获取失败

    public static final String TAG_GET_MERCHANT_CONFIRM = "tag_get_merchant_confirm";//确认或拒绝商户信息tag
    public static final String GET_MERCHANT_INFO_CONFIRM_TRUE = "get_merchant_info_confirm_true";//确认或拒绝商户信息获取成功
    public static final String GET_MERCHANT_INFO_CONFIRM_FALSE = "get_merchant_info_confirm_false";//确认会拒绝商户信息获取失败

    public static final String TAG_GET_INDUSTRY_TYPE = "tag_get_industry_type";//行业类别tag
    public static final String GET_INDUSTRY_TYPE_TRUE = "get_industry_type_true";//行业类别获取成功
    public static final String GET_INDUSTRY_TYPE_FALSE = "get_industry_type_false";//行业类别获取失败

    public static final String TAG_GET_ADDRESS_PUBLIC = "tag_get_address_public";//地址获取tag
    public static final String TAG_GET_ADDRESS = "tag_get_address";//地址获取tag
    public static final String TAG_GET_MERCHANT_ADDRESS = "tag_get_merchant_address";//地址获取tag
    public static final String GET_ADDRESS_TRUE = "get_address_true";//地址获取成功
    public static final String GET_ADDRESS_FALSE = "get_address_false";//地址获取失败

    public static final String TAG_SUBMIT_MERCHANT_INFO = "tag_submit_merchant_info";//提交信息tag
    public static final String SUBMIT_MERCHANT_INFO_TRUE = "submit_merchant_info_true";//提交信息成功
    public static final String SUBMIT_MERCHANT_INFO_FALSE = "submit_merchant_info_false";//提交信息失败

    public static final String TAG_QUERY_ENABLED_CHANGE = "tag_query_Enabled_Change";//验证商户资料是否允许修改tag
    public static final String QUERY_ENABLED_CHANGE_TRUE = "query_Enabled_Change_true";//验证商户资料是否允许修改成功
    public static final String QUERY_ENABLED_CHANGE_FALSE = "query_Enabled_Change_false";//验证商户资料是否允许修改失败


    public static final String LOGIN_CHANGE_PWD_TAG = "login_change_pwd_tag";//登录后修改密码tag
    public static final String LOGIN_CHANGE_PWD_TRUE = "login_change_pwd_true";//登录后修改密码成功
    public static final String LOGIN_CHANGE_PWD_FALSE = "login_change_pwd_false";//登录后修改密码失败

    public static final String TAG_AUTHENTICATION = "tag_authentication";//身份验证tag
    public static final String AUTHENTICATION_TRUE = "authentication_true";//身份验证成功
    public static final String AUTHENTICATION_FALSE = "authentication_false";//身份验证失败


    public static final String TAG_DOWNLOAD_IMAGE = "tag_download_image";//广告下载tag
    public static final String DOWNLOAD_IMAGE_TRUE = "download_image_true";//广告下载成功
    public static final String DOWNLOAD_IMAGE_FALSE = "download_image_false";//广告下载失败

    public static final String TAG_SAVE_IMAGE = "tag_save_image";//图片下载tag
    public static final String SAVE_IMAGE_TRUE = "save_image_true";//图片下载成功
    public static final String SAVE_IMAGE_FALSE = "save_image_false";//图片下载失败

    public static final String TAG_WELCOME_SKIP = "tag_welcome_skip";
    public static final String WELCOME_SKIP_LOGIN = "welcome_skip_login";
    public static final String WELCOME_SKIP_MAIN = "welcome_skip_main";

    public static final String TAG_VERSION_UPDATE = "tag_version_update";//版本更新
    public static final String TAG_VERSION_UPDATE_ABOUTUS = "TAG_VERSION_UPDATE_ABOUTUS";//关于我们版本更新tag
    public static final String TAG_CHANGE_BINGING = "tag_change_binging";//更换手机号tag
    public static final String TAG_MAIN_QUEST_LOCATION = "tag_main_quest_location";//定位查询tag
    public static final String TAG_COLLECT_INFO = "tag_collect_info";//收集信息tag
    public static final String TAG_CARD_DETAIL = "tag_card_detail";//银行卡详情tag
    public static final String TAG_EDIT_CARD = "tag_edit_card";//编辑银行卡tag
    public static final String TAG_BANK_LIST = "tag_bank_list";//银行列表tag
    public static final String TAG_BANK_BRANCH_LIST = "tag_bank_branch_list";//支行列表tag
    public static final String TAG_MARQUEE = "tag_marquee";//走马灯tag
    public static final String TAG_HOME_NOTICE = "tag_home_notice";//首页消息
    public static final String TAG_UNREAD_NOTICE = "tag_unread_notice";//未读消息
    public static final String TAG_CHOICE_STORE = "tag_choice_store";//选择门店列表
    public static final String TAG_BIND_CODE = "tag_bind_code";//绑定二维码tag
    public static final String TAG_CHECK_QRCODE = "tag_check_qrcode";//查询二维码tag
    public static final String TAG_ADD_VIP_INFO = "tag_add_vip_info";//添加会员卡资料tag
    public static final String TAG_GET_VIP_INFO = "tag_get_vip_info";//查询会员卡详情tag
    public static final String TAG_EDIT_VIP_INFO = "tag_edit_vip_info";//编辑会员卡tag
    public static final String TAG_GET_VIP_URL = "tag_get_vip_url";//获取会员卡领卡链接tag
    public static final String TAG_SEND_MSG = "tag_send_msg";//发送短信tag
    public static final String TAG_SEND_MSG_PUBLIC = "tag_send_msg_public";//发送短信tag
    public static final String TAG_LEGAL_INFO = "tag_legal_info";//修改法人tag
    public static final String TAG_BANK_ACCOUNT_INFO = "tag_bank_account_info";//修改结算卡tag
    public static final String TAG_BANK_ACCOUNT_INFO_PUBLIC = "tag_bank_account_info_public";//修改结算卡tag
    public static final String TAG_PAY_TYPE = "tag_pay_type";//支付类型
    public static final String TAG_NEW_CONFIG = "tag_new_config";//新配置

    public static final String TAG_GET_SETTLE_CHANGE_COUNT = "tag_get_settle_change_count";//查询商户剩余修改结算信息次数tag
    public static final String GET_SETTLE_CHANGE_COUNT_TRUE = "get_settle_change_count_true";//查询商户剩余修改结算信息次数成功
    public static final String GET_SETTLE_CHANGE_COUNT_FALSE = "get_settle_change_count_false";//查询商户剩余修改结算信息次数失败
    public static final String TAG_LINK_EMPLOYEE = "tag_link_employee";//收款设备关联员工


    public static final String TAG_DEVICE_STORE = "tag_device_store";
    public static final String TAG_DEVICE_TYPE = "tag_device_type";
    public static final String TAG_DEVICE_LIST = "tag_device_list";//绑定设备列表tag

    public static final String TAG_UNBIND_DEVICE = "tag_unbind_device";//解绑设备
    public static final String TAG_DETAIL_DEVICE = "tag_detail_device";//设备详情
    public static final String TAG_QUERY_DEVICE_SN = "tag_query_device_sn";//sn查询设备信息
    public static final String TAG_BIND_DEVICE_SN = "tag_bind_device_sn";//绑定设备
    public static final String TAG_WX_SIGN_DETAIL = "tag_wx_sign_detail";//微信签约详情信息
    public static final String TAG_WX_SIGN_CANCEL = "tag_wx_sign_cancel";//撤销微信申请单
    public static final String TAG_WX_SIGN_AGAIN = "tag_wx_sign_again";//微信实名认证重新签约
    public static final String TAG_WX_SIGN_LIST = "tag_wx_sign_list";//微信实名认证商户列表
    public static final String TAG_RANK_LIST = "tag_rank_list";//商户排名列表





    public static final String ON_EVENT_TRUE = "on_event_true";//请求成功
    public static final String ON_EVENT_FALSE = "on_event_false";//请求失败
    public static String MAIN_UPDATE_VERSION_TAG = "main_update_version_tag";//首页版本升级的tag

    /************************APP页面*******************/
    public static final String UPDATE_PROGRESS_TAG = "update_progress_tag";
    public static final String UPDATE_DOWNLOADING = "update_downloading";
    public static final String UPDATE_DOWNLOAD_SUCCESS = "update_download_success";
    public static final String UPDATE_DOWNLOAD_FAILED = "update_download_failed";
//    public static final String INTENT_INSTRUCTION = "instruction";

    public static String INTENT_VIP_INFO = "intent_vip_info";//会员卡详情
    public static String INTENT_REGISTER_VIP = "intent_register_vip";//h5入口会员卡进件
    public static String INTENT_BANK_DETAIL = "intent_bank_detail";//银行卡详情
    public static String INTENT_MERCHANT_ID = "intent_merchant_id";
    public static String INTENT_MERCHANT_PAY_TYPE = "intent_merchant_pay_type";
    public static String INTENT_MERCHANT_NAME = "intent_merchant_name";
    public static String INTENT_VIP_SUCCESS = "intent_vip_success";
    public static String INTENT_MERCHANT_TEL = "intent_merchant_tel";
    public static String INTENT_CODE_URL = "intent_code_url";
    public static String INTENT_CODE_TIMEOUT = "intent_code_timeout";
    public static String INTENT_NAME = "intent_name";
    public static String INTENT_PASSWORD = "intent_password";
    public static String INTENT_CLIP_TYPE = "intent_clip_type";
    public static String INTENT_NAME_MERCHANT_UPLOAD = "intent_name_merchant_upload";//商户-上传图片
    public static String INTENT_BIND_STORE_ID = "intent_bind_store_id";
    public static String INTENT_BIND_STORE_NAME = "intent_bind_store_name";
    public static String INTENT_BIND_CODE = "intent_bind_code";
    public static String INTENT_NAME_DATE = "intent_name_date";//日期Date
    public static String INTENT_NAME_DATE_CN = "intent_name_date_cn";//日期Date
    public static String INTENT_NAME_DEVICE_ID = "intent_name_device_id";
    public static String INTENT_NAME_CATEGORY_CODE = "intent_name_category_code";
    public static String INTENT_NAME_CATEGORY_TYPE = "intent_name_category_type";
    public static String INTENT_NAME_DEVICE_IMG = "intent_name_device_img";
    public static String INTENT_BIND_DEVICE = "intent_bind_device";
    public static String INTENT_WX_SIGN_DATA = "intent_wx_sign_data";


    public static String GETUI_ADD_PUSHER = "getui_add_pusher";//推送用户绑定
    public static String GETUI_PUSH_VOICE_TAG = "getui_push_voice_tag";//语音播报初始化tag
    public static String GETUI_PUSH_VOICE_LIST_TAG = "getui_push_voice_list_tag";//语音播报列表tag
    public static String GETUI_PUSH_VOICE_SET_TAG = "getui_push_voice_set_tag";//语音播报开关设置tag
    public static String INTENT_GET_GOODS_NAME = "intent_get_goods_name";//商品名称
    public static String INTENT_GUIDE_LOGIN = "intent_guide_login";//向导-登录
    public static String INTENT_RESET_PWD_TELPHONE = "intent_reset_pwd_telphone";//重置密码

    public static String INTENT_NAME_FROM = "intent_name_from";
    public static String INTENT_NAME_PIC_BGSQ_NET = "intent_name_pic_bgsq_net";//变更申请函
    public static String INTENT_NAME_PIC_SFSQ_NET = "intent_name_pic_sfsq_net";
    public static String INTENT_CHANGEACOUNTCOUNT = "intent_changeacountcount";
    public static final String INTENT_STORE_DATA_TYPE = "intent_store_data_type";
    public static String INTENT_NAME_BILL_SHOP = "intent_name_bill_shop";//账单-门店
    public static String INTENT_NAME_COLLECT_SHOP = "intent_name_collect_shop";//报表-门店

    public static final String ORDER_DETAIL_REVERSE_TAG = "order_detail_reverse_tag";//订单详情冲正tag
    public static final String ORDER_DETAIL_CHECK_STATE_TAG = "order_detail_check_state_tag";//订单详情查询状态tag

    public static final String AUTO_PWD_FREE_LOGIN_ERROR = "auto_pwd_free_login_error";//免密登录过期
    public static String INTENT_STORE_ID = "intent_store_id";
    public static String INTENT_CASHIER_ID = "intent_cashier_id";
    public static String IS_ALL_STORE = "is_all_store";
    public static String IS_ALL_CASHIER = "is_all_cashier";

    public static String UPDATE_VERSION_TAG = "update_version_tag";//版本升级的tag
    public static String UPDATE_SERVICE_TAG = "update_service_tag";

    public static String UPDATE_INSTALL_TAG = "update_install_tag";
    public static String AUTO_IMAGER_TAG = "auto_image_tag";
    public static String UPDATE_APP_URL = "update_app_url";
    public static String UPDATE_VERSION_NAME = "update_version_name";
    public static String UPDATE_VERSION_CODE = "update_version_code";
    public static String UPDATE_VERSION_CANCEL = "update_version_cancel";//首页版本升级取消
    public static final String MAIN_UPDATE_PROGRESS_TAG = "main_update_progress_tag";
    public static String LOGINOUT_TAG = "app_loginout_tag";//app退出tag
    public static String DELETE_PUSH_VOICE_DEVICEID_TAG = "delete_device_id_tag";//删除设备Id tag
    public static String PUSH_VOICE_DETAIL_TAG = "push_voice_detail_tag";//推送信息跳转到详情页面

    public static final String FILE_CACHE_ROOT = "/hstysales/";
    public static final String FILE_LOG_DIR = "/logs/";
    public static final String DOWNLOADPATH = "/hxw/";
    public static final String APK_FILE_NAME = "hxw.apk";

    /************************商户审核状态*******************/
    public static final String MERCHANT_CHECK_TYPE = "merchant_check_type";//商户审核类型
    public static final int MERCHANT_CHECK_SUCCESS = 1;//商户审核通过
    public static final int MERCHANT_CHECKING = 2;     //商户审核中
    public static final int MERCHANT_CHECK_FAILED = 3; //商户审核失败
    public static final int MERCHANT_NO_INFO = 4;      //商户未进件
    public static final int MERCHANT_FREEZE = 5;       //商户已冻结
    public static final int MERCHANT_TRADABLE = 6;       //可交易
    public static final int MERCHANT_CHANGEING = 7;       //审核变更中
    public static final int MERCHANT_CHANGE_PASS = 8;       //变更审核通过
    public static final int MERCHANT_CHANGE_FAIL = 9;       //变更失败
    public static final int MERCHANT_INVALID = 10;       //作废商户
    public static final int MERCHANT_NOT_CONFIRM = 11;  //待确认


    /*************************硬件商城订单start****************/
    public static final String TAG_GET_DEVICE_ORDER_LIST = "tag_get_device_order_list";//tag硬件商城获取订单列表
    public static final String GET_DEVICE_ORDER_LIST_FALSE = "get_device_order_list_false";//硬件商城获取订单列表失败
    public static final String GET_DEVICE_ORDER_LIST_TRUE = "get_device_order_list_true";//硬件商城获取订单列表成功
    public static final String TAG_GET_DEVICE_ORDER_DISPATCH = "tag_get_device_order_dispatch";//tag硬件商城订单派送
    public static final String GET_DEVICE_ORDER_DISPATCH_FALSE = "get_device_order_dispatch_false";//硬件商城订单派送失败
    public static final String GET_DEVICE_ORDER_DISPATCH_TRUE = "get_device_order_dispatch_true";//硬件商城订单派送成功
    public static final String TAG_GET_DEVICE_ORDER_ARRIVE = "tag_get_device_order_arrive";//tag硬件商城确认订单到达
    public static final String GET_DEVICE_ORDER_ARRIVE_FALSE = "get_device_order_arrive_false";//硬件商城确认订单到达失败
    public static final String GET_DEVICE_ORDER_ARRIVE_TRUE = "get_device_order_arrive_true";//硬件商城确认订单到达成功
    public static final String TAG_GET_DEVICE_ORDER_TRANSITCOUNT = "tag_get_device_order_transitcount";//tag硬件商城获取在途订单数量
    public static final String GET_DEVICE_ORDER_TRANSITCOUNT_FALSE = "get_device_order_transitcount_false";//硬件商城获取在途订单数量失败
    public static final String GET_DEVICE_ORDER_TRANSITCOUNT_TRUE = "get_device_order_transitcount_true";//硬件商城获取在途订单数量成功
    public static final int DEVICE_MALL_WAIT_POST = 1;//待发货
    public static final int DEVICE_MALL_WAIT_SEND = 2;//待派送
    public static final int DEVICE_MALL_WAIT_FINISH = 3;//已完成
    public static final int DEVICE_MALL_WAIT_ClOSE = 5;//已关闭
    public static final int DEVICE_MALL_SEARCH = -10;//搜索
    /*************************硬件商城订单end****************/


    public static final String TAG_GET_APP_EXAMINE_LIST = "tag_get_app_examine_list";//查询应用审核列表
    public static final String GET_APP_EXAMINE_LIST_TRUE = "get_app_examine_list_true";//查询应用审核列表成功
    public static final String GET_APP_EXAMINE_LIST_FALSE = "get_app_examine_list_false";//查询应用审核列表失败
    public static final String TAG_GET_APP_EXAMINE_DETAIL = "tag_get_app_examine_detail";//查询应用审核详情
    public static final String GET_APP_EXAMINE_DETAIL_TRUE = "get_app_examine_detail_true";//应用审核详情成功
    public static final String GET_APP_EXAMINE_DETAIL_FALSE = "get_app_examine_detail_false";//应用审核详情失败
    public static final String TAG_EXAMINE_APP = "tag_examine_app";//应用审核
    public static final String EXAMINE_APP_TRUE = "examine_app_true";//应用审核成功
    public static final String EXAMINE_APP_FALSE = "examine_app_false";//应用审核失败


    /**
     * 渠道名称
     */
    public static final String PLAY = "play";
    public static final String HXW = "hxw";
    public static final String YXF = "yxf";//宁波邮政甬小福
    public static final String XYZF = "xyzf";//凯斯特小云支付
    public static final String JBZF = "jbzf";//吉呗支付
    public static final String KINGDEER = "kingdeer";//金小二
    public static final String WZFHY = "wzfhy";//汇渔收款
    public static final String MISALES = "misales";//米小二
    public static final String AHDXSALES = "ahdxsales";//安徽电信-新零售

    /**
     * h5后缀区分
     */
    public static final String H5_HXW = "";
    public static final String H5_EMS_YXF = "?theme=ny";
    public static final String H5_EMS_YXF_AND = "theme=ny&";
    public static final String H5_EMS_XYZF = "?theme=kts";
    public static final String H5_EMS_XYZF_AND = "theme=kts&";

    /**
     * 商户类型
     */
    public static final int MCH_ENTERPRISE = 1;       //企业商户
    public static final int MCH_INDIVIDUAL = 2;       //个体工商户
    public static final int MCH_MANAGE = 3;       //个体经营者
    public static final int MCH_GOVERNMENT = 4;  //事业单位

    /**
     * 橙意宝
     */
    public static final String ORANGE_UPDATE= "orange_update";

    public static final String ERROR_CODE_CONFIG = "com.base.material.not.null";//配置信息错误/未配置

}
