package com.hstypay.hstysales.utils;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.hstypay.hstysales.R;

//打开和关闭箭头图片的动画工具类
public class ArrowAnimationUtil {

    //箭头打开：箭头由下往上转
    public static void openArrow(ImageView iv_arrow, Context context){
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.rotate_arrow_open_up);
        animation.setInterpolator(new LinearInterpolator());
        animation.setFillAfter(true);
        iv_arrow.startAnimation(animation);
    }

    //箭头关闭：箭头由上往下转
    public static  void closeArrow(ImageView iv_arrow,Context context){
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.rotate_arrow_close_down);
        animation.setInterpolator(new LinearInterpolator());
        animation.setFillAfter(true);
        iv_arrow.startAnimation(animation);
    }
}
