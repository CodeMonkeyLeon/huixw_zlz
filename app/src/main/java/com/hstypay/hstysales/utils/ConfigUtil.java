package com.hstypay.hstysales.utils;

import android.content.Context;

import com.hstypay.hstysales.app.MyApplication;

public class ConfigUtil {

    /**
     * 是否验证登录渠道号
     *
     * @return
     */
    public static boolean verifyServiceId() {
        boolean verifyServiceId = false;
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                /*case Constants.HXW:
                    verifyServiceId = false;
                    break;
                case Constants.YXF:
                    verifyServiceId = false;
                    break;*/
                default:
                    verifyServiceId = false;
                    break;
            }
        }
        return verifyServiceId;
    }

    /**
     * 是否获取配置信息
     *
     * @return true:对应的渠道需要请求配置功能信息和登录检验渠道ID（共用域名的OEM项目要检验orgID）， false:对应的渠道使用默认的配置功能信息
     */
    public static boolean getConfig() {
        boolean getConfigEnable = false;
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case Constants.HXW:
                    getConfigEnable = false;
                    break;
                case Constants.YXF:
                case Constants.XYZF:
                case Constants.JBZF:
                case Constants.KINGDEER:
                case Constants.WZFHY:
                case Constants.MISALES:
                case Constants.AHDXSALES:
                    getConfigEnable = true;
                    break;
                default:
                    getConfigEnable = false;
                    break;
            }
        }
        return getConfigEnable;
    }


    //获取OEM测试定制的渠道ID
    public static String getTestOrgId() {
        String orgId = "";
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case Constants.YXF:
                    orgId = "1050000032";//开发环境：1050000064，测试环境1050000032
                    break;
                case Constants.XYZF:
//                    orgId = "1050000032";//开发环境：1050000064，测试环境1050000032
                    break;
                case Constants.JBZF:
                    break;
                case Constants.KINGDEER:
                    break;
                case Constants.WZFHY:
                    break;
                case Constants.MISALES:
                case Constants.AHDXSALES:
                    break;
                default:
                    break;
            }
        }
        return orgId;
    }

    //获取OEM测试定制的渠道类型
    public static String getTestOrgType() {
        String orgType = "";
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case Constants.YXF:
                    orgType = "5";
                    break;
                case Constants.XYZF:
                    orgType = "5";
                    break;
                case Constants.JBZF:
                    break;
                case Constants.KINGDEER:
                    break;
                case Constants.WZFHY:
                    break;
                case Constants.MISALES:
                case Constants.AHDXSALES:
                    break;
                default:
                    break;
            }
        }
        return orgType;
    }

    //获取OEM正式定制的渠道ID
    public static String getReleaseOrgId() {
        String orgId = "";
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case Constants.YXF:
                    orgId = "1050000031";//灰度1050000030 生产1050000031
                    break;
                case Constants.XYZF:
                    orgId = "1010000948";
                    break;
                case Constants.JBZF:
                    orgId = "1050000039";
                    break;
                case Constants.KINGDEER:
                    orgId = "1050000043";
                    break;
                case Constants.WZFHY:
                    orgId = "1050000045";
                    break;
                case Constants.MISALES:
                    orgId = "1050000048";
                    break;
                case Constants.AHDXSALES:
                    orgId = "1050000049";
                    break;
                default:
                    break;
            }
        }
        return orgId;
    }

    //获取OEM正式定制的渠道类型
    public static String getReleaseOrgType() {
        String orgType = "";
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case Constants.YXF:
                    orgType = "5";
                    break;
                case Constants.XYZF:
                    orgType = "1";
                    break;
                case Constants.JBZF:
                    orgType = "5";
                    break;
                case Constants.KINGDEER:
                    orgType = "5";
                    break;
                case Constants.WZFHY:
                    orgType = "5";
                    break;
                case Constants.MISALES:
                    orgType = "5";
                    break;
                case Constants.AHDXSALES:
                    orgType = "5";
                    break;
                default:
                    break;
            }
        }
        return orgType;
    }

    /**
     * 是否请求升级接口
     *
     * @return
     */
    public static boolean updateVersionEnable() {
        /*boolean updateEnable;
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case Constants.YXF:
                    updateEnable = true;///channel/version接口还不能用于汇小旺所以不能区分出渠道不能升级
                    break;
                case Constants.XYZF:
                    updateEnable = true;
                    break;
                default:
                    updateEnable = true;
            }
        } else {
            updateEnable = false;
        }*/
        return true;
    }

    /**
     * 升级上传的渠道类型
     *
     * @return
     */
    public static String getAppChannel() {
        String appChannel = "";
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case Constants.YXF:
                    appChannel = "EMS";
                    break;
                case Constants.XYZF:
                    appChannel = "KTS";
                    break;
                case Constants.JBZF:
                    appChannel = "JIBEI";
                    break;
                case Constants.KINGDEER:
                    appChannel = "KINGDEE";
                    break;
                case Constants.WZFHY:
                    appChannel = "WZFHY";
                    break;
                case Constants.MISALES:
                    appChannel = "MIBAG";
                    break;
                case Constants.AHDXSALES:
                    appChannel = "AHDX";
                    break;
                default:
                    appChannel = "HS";
                    break;
            }
        }
        return appChannel;
    }

    /**
     * 区分UserAgent
     *
     * @return
     */
    public static String getUserAgent(Context context) {
        String userAgent = "";
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                case Constants.YXF:
                    userAgent = " yxfsales/" + VersionUtils.getVersionCode(context) + " (yxfsales_app_android)";
                    break;
                case Constants.XYZF:
                    userAgent = " xyzfsales/" + VersionUtils.getVersionCode(context) + " (xyzfsales_app_android)";
                    break;
                case Constants.JBZF:
                    userAgent = " jbzfsales/" + VersionUtils.getVersionCode(context) + " (jbzfsales_app_android)";
                    break;
                case Constants.KINGDEER:
                    userAgent = " jxesales/" + VersionUtils.getVersionCode(context) + " (jxesales_app_android)";
                    break;
                case Constants.WZFHY:
                    userAgent = " wzfhySales/" + VersionUtils.getVersionCode(context) + " (wzfhySales_app_android)";
                    break;
                case Constants.MISALES:
                    userAgent = " mibagSales/" + VersionUtils.getVersionCode(context) + " (mibagSales_app_android)";
                    break;
                case Constants.AHDXSALES:
                    userAgent = " AHDXSales/" + VersionUtils.getVersionCode(context) + " (AHDXSales_app_android)";
                    break;
                default:
                    userAgent = " hstysales/" + VersionUtils.getVersionCode(context) + " (hstysales_app_android)";
                    break;
            }
        }
        return userAgent;
    }
}
