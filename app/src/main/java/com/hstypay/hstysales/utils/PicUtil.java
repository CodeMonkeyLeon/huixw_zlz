package com.hstypay.hstysales.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.utils
 * @创建者: Jeremy
 * @创建时间: 2018/5/8 13:58
 * @描述: ${TODO}
 */

public class PicUtil {
    private static final String TAG = "PicUtils";
    private static final String PNG = "png";
    private static final String JPG = "jpg";
    private static final String JPEG = "jpeg";
    private static final String BMP = "bmp";
    private static final String GIF = "gif";
    // private static final String[] imgSuffixes = {PNG, JPG, JPEG, BMP};
    private static final List<String> fileSuffixes = Arrays.asList(PNG, JPG, JPEG, BMP);
    // 缓存文件头信息-文件头信息
    private static final Map<String, String> mFileTypes = new HashMap<>();

    static {
        // images
        mFileTypes.put("FFD8FFE0", JPG);
        mFileTypes.put("89504E47", PNG);
        mFileTypes.put("424D5A52", BMP);
        mFileTypes.put("47494638", GIF);
    }

    /**
     * 指定文件夹中的图片文件转成JPG格式
     *
     * @param dir 图片的所在文件夹路径
     */
    public static void imgToJPG(File dir) {
        File[] files = dir.listFiles();
        String filePath = "";
        for (int i = 0; i < files.length; i++) {
            //先通过后缀名，过滤非图片
            String fileType = files[i].getName().substring(files[i].getName().lastIndexOf('.') + 1);
            if (fileSuffixes.contains(fileType.toLowerCase())) {
                filePath = files[i].getPath();
                String imgType = mFileTypes.get(getFileHeader(filePath)); //获取真正的文件头
                if (!TextUtils.isEmpty(imgType) && !imgType.equals(JPG)) {
                    convertToJpg(filePath, filePath.substring(0, filePath.lastIndexOf('.') + 1) + JPG);
                }
            }
        }
    }

    public static String fileToJPG(String filePath) {
        String jpgFilePath = "";
        String imgType = mFileTypes.get(getFileHeader(filePath)); //获取真正的文件头
        if (!JPG.equals(imgType) && !PNG.equals(imgType)) {
            //jpgFilePath = filePath.substring(0, filePath.lastIndexOf('.'))+"(1)." + JPG;
            jpgFilePath = AppHelper.getImageDir() + "." + JPG;
            convertToJpg(filePath, jpgFilePath);
        } else {
            //jpgFilePath = filePath.substring(0, filePath.lastIndexOf('.'))+"(1)" + filePath.substring(filePath.lastIndexOf("."));
            jpgFilePath = AppHelper.getImageDir() + filePath.substring(filePath.lastIndexOf("."));
            copyFile(filePath, jpgFilePath);
        }
        return jpgFilePath;
    }

    /**
     * 转换成JPG格式图片 并将原照片删除
     *
     * @param pngFilePath png或者bmp照片
     * @param jpgFilePath jpg照片
     */
    private static void convertToJpg(String pngFilePath, String jpgFilePath) {
        Bitmap bitmap = BitmapFactory.decodeFile(pngFilePath);
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(jpgFilePath));
            if (bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)) {
                bos.flush();
            }
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
                bitmap = null;
            }
        }
        //删除非JPG照片
        /*if (!pngFilePath.equals(jpgFilePath)) {
            File oldImg = new File(pngFilePath);
            oldImg.delete();
        }*/
    }

    public static void copyFile(String src, String target) {
        File srcFile = new File(src);
        File targetFile = new File(target);
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(srcFile);
            out = new FileOutputStream(targetFile);
            byte[] bytes = new byte[1024];
            int len = -1;
            while ((len = in.read(bytes)) != -1) {
                out.write(bytes, 0, len);
            }
            in.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 根据文件路径获取文件头信息
     *
     * @param filePath 文件路径
     * @return 文件头信息
     */
    public static String getFileHeader(String filePath) {
        FileInputStream is = null;
        String value = "";
        try {
            is = new FileInputStream(filePath);
            byte[] b = new byte[4];
            is.read(b, 0, b.length);
            value = bytesToHexString(b);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return value;
    }

    public static String getFileType(String filePath){
        String type = ".jpg";
        if (filePath != null) {
            switch (getFileHeader(filePath)) {
                case "FFD8FFE0":
                    type = ".jpg";
                    break;
                case "89504E47":
                    type = ".png";
                    break;
                case "424D5A52":
                    type = ".bmp";
                    break;
                case "47494638":
                    type = ".gif";
                    break;
            }
        }
        return type;
    }


    /**
     * 将要读取文件头信息的文件的byte数组转换成string类型表示
     *
     * @param src 要读取文件头信息的文件的byte数组
     * @return 文件头信息
     */
    private static String bytesToHexString(byte[] src) {
        StringBuilder builder = new StringBuilder();
        if (src == null || src.length <= 0) {
            return "";
        }
        for (int i = 0; i < src.length; i++) {
            // 以十六进制（基数 16）无符号整数形式返回一个整数参数的字符串表示形式，并转换为大写
            String hv = Integer.toHexString(src[i] & 0xFF).toUpperCase();
            if (hv.length() < 2) {
                builder.append(0);
            }
            builder.append(hv);
        }
        return builder.toString();
    }
}
