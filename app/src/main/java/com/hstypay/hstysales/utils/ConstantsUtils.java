package com.hstypay.hstysales.utils;

import com.hstypay.hstysales.R;

public class ConstantsUtils {

    //当前是否是可交易的类型
    public static boolean isTradable(int payType) {
        if (payType == 0) return false; //不可交易
        else if (payType == 10) return true; //可交易
        else if (payType == 20) return true; //部分可交易
        else return false;
    }

    //当前是否已经进件
    public static boolean isIncome(String stateStr) {
        if (!StringUtils.isEmptyOrNull(stateStr)) {
            int state = Integer.valueOf(stateStr);
            return state != Constants.MERCHANT_NO_INFO;
        }
        return false;
    }

    //当前是否已经进件
    public static boolean isIncome(int state) {
        return state != Constants.MERCHANT_NO_INFO;
    }

    //当前是否是可开通橙意宝的企业类型
    public static boolean isSupportedOrange(String mchType) {
        if (String.valueOf(Constants.MCH_ENTERPRISE).equals(mchType) || String.valueOf(Constants.MCH_GOVERNMENT).equals(mchType)) { //企业商户 或 事业单位
            return false;
        }
        return true;
    }

    //当前是否是可开通橙意宝的企业类型
    public static boolean isSupportedOrange(int mchType) {
        if (Constants.MCH_ENTERPRISE == mchType || Constants.MCH_GOVERNMENT == mchType) { //企业商户和事业单位不支持橙意宝
            return false;
        }
        return true;
    }

}
