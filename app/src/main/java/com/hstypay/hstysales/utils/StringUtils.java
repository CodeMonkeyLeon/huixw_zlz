package com.hstypay.hstysales.utils;

import android.text.TextUtils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.R.attr.name;

public class StringUtils {

    public static boolean isEmptyOrNull(String str) {
        if ("".equals(str) || null == str || "null".equals(str)) {
            return true;
        }

        return false;
    }

    public static long paseStrToLong(String str) {
        if (str.equals("0.0")) {
            return 0;
        } else {
            return Long.parseLong(str);
        }

    }

    /**
     * 格式化金额
     *
     * @param s
     * @param len
     * @return
     */
    public static String formatMoney(String s, int len) {
        if (s == null || s.length() < 1) {
            return "";
        }
        NumberFormat formater = null;
        double num = Double.parseDouble(s);
        if (len == 0) {
            formater = new DecimalFormat("###,###");

        } else {
            StringBuffer buff = new StringBuffer();
            buff.append("###,##0.");
            for (int i = 0; i < len; i++) {
                buff.append("0");
            }
            formater = new DecimalFormat(buff.toString());
        }
        String result = formater.format(num);
        if (result.indexOf(".") == -1) {
            //result = "￥" + result + ".00";
            result = result + ".00";
        }
        return result;
    }

    public static boolean isLegalPwd(String str) {
        String regex = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,32}$";
        return str.matches(regex);
    }

    public static boolean isLetterDigit(String str) {
        boolean isDigit = false;//定义一个boolean值，用来表示是否包含数字
        boolean isLetter = false;//定义一个boolean值，用来表示是否包含字母
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {   //用char包装类中的判断数字的方法判断每一个字符
                isDigit = true;
            }
            if (Character.isLetter(str.charAt(i))) {  //用char包装类中的判断字母的方法判断每一个字符
                isLetter = true;
            }
        }
        String regex = "^[a-zA-Z0-9]{6,32}$";
        //boolean isRight = isDigit && isLetter && str.matches(regex);
        boolean isRight = isLetter && str.matches(regex);
        return isRight;
    }

    public static boolean isEmail(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        //String regex = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        //String regex = "^\\w[-\\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\\.)+[A-Za-z]{2,14}$";
        String regex = "^[A-Za-z0-9_][-[A-Za-z0-9_].+]*@([A-Za-z0-9][-A-Za-z0-9]+\\.)+[A-Za-z]{2,14}$";
        return str.matches(regex);
    }

    public static boolean isIdCard(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String regex = "^[1-9]\\d{13,16}[a-zA-Z0-9]{1}$";
        //String regex = "^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$|^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([0-9]|X)$";
        return str.matches(regex);
    }

    public static boolean isName(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        boolean isMatch = str.matches("^([a-zA-Z0-9\\u4e00-\\u9fa5]){1,20}$");
        boolean isNumber = str.matches("[0-9]+");
        return isMatch && !isNumber;
    }

    public static boolean isMerchantName(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        boolean isMatch = str.matches("([a-zA-Z0-9\\u4e00-\\u9fa5]){1,30}$");
        boolean isNumber = str.matches("[0-9]+");
        return isMatch && !isNumber;
    }

    public static boolean isShortName(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        boolean isMatch = str.matches("^([a-zA-Z0-9\\u4e00-\\u9fa5]){1,15}$");
        boolean isNumber = str.matches("[0-9]+");
        return isMatch && !isNumber;
    }

    public static boolean isTelephone(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
//        String regex = "^((\\d{1,4}([-]\\d{1,8}){1,2})|(\\d{7,12}))$";
        String regex = "^1\\d{10}$";
        return str.matches(regex);
    }

    public static void limitLength(String str) {
        if (TextUtils.isEmpty(str))
            return;
        char[] c = str.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if (isChinese(String.valueOf(c[i]))) {
                System.out.println(c[i] + "///");
            }
        }
    }

    public static boolean isChinese(CharSequence charSequence) {
        String regEx = "[\u4e00-\u9fa5]";
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(charSequence);
        return matcher.matches();
    }

    public static String hideName(String name) {
        StringBuilder sb = new StringBuilder();
        if (name.length() > 1) {
            sb.append(name.substring(0, 1));
            for (int i = 0; i < name.length() - 1; i++) {
                sb.append("*");
            }
        } else if (name.length() == 1) {
            sb.append(name);
        }
        return sb.toString();
    }

    public static String hideCardID(String cardID) {
        if (TextUtils.isEmpty(cardID)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        if (cardID.length() > 3) {
            sb.append(cardID.substring(0, 1));
            for (int i = 0; i < cardID.length() - 2; i++) {
                sb.append("*");
            }
            sb.append(cardID.substring(cardID.length() - 1, cardID.length()));
        } else if (cardID.length() > 0) {
            sb.append(name);
        }
        return sb.toString();
    }

    public static String hideTelephone(String telephone) {
        StringBuilder sb = new StringBuilder();
        if (telephone.length() > 7) {
            sb.append(telephone.substring(0, 3));
            for (int i = 0; i < telephone.length() - 7; i++) {
                sb.append("*");
            }
            sb.append(telephone.substring(telephone.length() - 4, telephone.length()));
        } else if (telephone.length() > 0) {
            sb.append(name);
        }
        return sb.toString();
    }

    public static String hideFirstName(String name) {
        StringBuilder sb = new StringBuilder();
        if (name.length() > 1) {
            sb.append("*").append(name.substring(1));
        } else if (name.length() == 1) {
            sb.append(name);
        }
        return sb.toString();
    }

    public static String hideLoginName(String name) {
        StringBuilder sb = new StringBuilder();
        if (name.length() > 4) {
            sb.append("****").append(name.substring(name.length() - 4));
        } else if (name.length() == 1) {
            sb.append(name);
        }
        return sb.toString();
    }

    public static boolean managerName10People(String str) {//10位以下汉字、数字、字母混合
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String regex2 = "^([a-zA-Z0-9\\u4e00-\\u9fa5]){1,10}$";
        return str.matches(regex2);
    }

    public static boolean managerName(String str) {//20位以下汉字、数字、字母混合，不可录入纯数字
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String regex1 = "^[0-9]*$";
        if (str.matches(regex1))
            return false;
        String regex2 = "^([a-zA-Z0-9\\u4e00-\\u9fa5]){1,20}$";
        return str.matches(regex2);
    }

    public static String omitString(String string, int length) {
        if (TextUtils.isEmpty(string)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        if (string.length() > (2 + length * 2)) {
            sb.append(string.substring(0, length));
            sb.append("...");
            sb.append(string.substring(string.length() - length));
        } else {
            sb.append(string);
        }
        return sb.toString();
    }

    /**
     * 对入参保留最多两位小数(舍弃末尾的0)，如:
     * 3.345->3.34
     * 3.40->3.4
     * 3.0->3
     */
    public static String getNoMoreThanTwoDigits(String number) {
        if (TextUtils.isEmpty(number) || !number.contains("."))
            return number;
        if (number.contains(".")) {
            int index = number.indexOf(".");
            int length = number.length();
            for (int i = 0; i < length - index; i++) {
                if (number.charAt(number.length() - 1) == '0' || number.charAt(number.length() - 1) == '.') {
                    number = number.substring(0, number.length() - 1);
                }
            }
        }
        return number;
    }
}
