package com.hstypay.hstysales.utils;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.lib.WheelView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnCustomItemSelectedListener;
import com.bigkoo.pickerview.view.WheelTime;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.base.BaseActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * author: kuangzeyu
 * date: 2021/7/24
 * desc: 选择起始年月日
 */
public class TimePickViewHelp implements OnCustomItemSelectedListener, View.OnClickListener {

    private String timeFormatType = "yyyy-MM-dd HH:mm:ss";
    private TextView mTvDateStartPickProfit;//pickview里开始日期显示
    private TextView mTvDateEndPickProfit;//pickview里日期显示
    private Calendar mCurSelectStartDate;//当前pickview选中的开始日期
    private Calendar mCurSelectEndDate;//当前pickview选中的结束日期
    private Calendar mDateRangeStart;//日期范围的开始日期
    private Calendar mDateRangeEnd;//日期范围的结束日期
    boolean isSelectStartDate = true;//pickview里是否选中开始日期，默认选中
    private TimePickerView mTimePickerView;

    private Context mContext;
    private CallBackDataListener mCallBackDataListener;
    public TextView mTv_bottom_tx_pickview;

    /**
     * @param context
     * @param rangeStart 日期范围的开始日期
     * @param rangeEnd 日期范围的结束日期
     * @param curStartDate 当前显示的开始日期  不能小于rangeStart
     * @param curEndDate 当前显示的结束日期  不能大于curEndDate
     */
    public TimePickViewHelp(BaseActivity context,Calendar rangeStart, Calendar rangeEnd, Calendar curStartDate,Calendar curEndDate){
        mContext = context;
        mDateRangeStart = rangeStart;
        mDateRangeEnd = rangeEnd;
        mCurSelectStartDate = curStartDate;
        mCurSelectEndDate = curEndDate;
        initPickTimeView();
    }

    private void initPickTimeView() {
        TimePickerView.Builder mBuilder = new TimePickerView.Builder(mContext, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                try{
                    LogUtil.d("tagtag",""+new SimpleDateFormat(timeFormatType).format(date));
                    String tv_start = mTvDateStartPickProfit.getText().toString().trim();//2020/10/01
                    Date tv_date_start = new SimpleDateFormat("yyyy/MM/dd").parse(tv_start);
                    mCurSelectStartDate.setTime(tv_date_start);
                    mCurSelectStartDate.set(Calendar.HOUR_OF_DAY,0);
                    mCurSelectStartDate.set(Calendar.MINUTE,0);
                    mCurSelectStartDate.set(Calendar.SECOND,0);

                    String tv_end = mTvDateEndPickProfit.getText().toString().trim();//2020/10/01
                    /*Date today = new Date();
                    String todayStr = new SimpleDateFormat("yyyy/MM/dd").format(today);
                    if (tv_end.equals(todayStr)){
                        //结束日期是今天
                        mCurSelectEndDate.setTime(today);
                    }else {
                        Date tv_date_end = new SimpleDateFormat("yyyy/MM/dd").parse(tv_end);
                        mCurSelectEndDate.setTime(tv_date_end);
                        mCurSelectEndDate.set(Calendar.HOUR_OF_DAY,23);
                        mCurSelectEndDate.set(Calendar.MINUTE,59);
                        mCurSelectEndDate.set(Calendar.SECOND,59);
                    }*/
                    Date tv_date_end = new SimpleDateFormat("yyyy/MM/dd").parse(tv_end);
                    mCurSelectEndDate.setTime(tv_date_end);
                    mCurSelectEndDate.set(Calendar.HOUR_OF_DAY,23);
                    mCurSelectEndDate.set(Calendar.MINUTE,59);
                    mCurSelectEndDate.set(Calendar.SECOND,59);

                    if (mCallBackDataListener !=null){
                        mCallBackDataListener.onSubmit(mCurSelectStartDate,mCurSelectEndDate);
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        mBuilder.setLayoutRes(R.layout.pickview_select_period_profit, new CustomListener() {
            @Override
            public void customLayout(View v) {
                Button btnCancel = v.findViewById(R.id.btnCancel);
                Button btnSubmit = v.findViewById(R.id.btnSubmit);
                mTv_bottom_tx_pickview = v.findViewById(R.id.tv_bottom_tx_pickview);
                WheelView year = v.findViewById(R.id.year);
                WheelView month = v.findViewById(R.id.month);
                WheelView day = v.findViewById(R.id.day);
                year.setOnCustomItemSelectedListener(TimePickViewHelp.this);
                month.setOnCustomItemSelectedListener(TimePickViewHelp.this);
                day.setOnCustomItemSelectedListener(TimePickViewHelp.this);
                btnCancel.setOnClickListener(TimePickViewHelp.this);
                btnSubmit.setOnClickListener(TimePickViewHelp.this);
                mTvDateStartPickProfit = v.findViewById(R.id.tv_date_start_pick_profit);
                mTvDateEndPickProfit = v.findViewById(R.id.tv_date_end_pick_profit);
                setPickViewStartEndDate(mCurSelectStartDate,mCurSelectEndDate);
                mTvDateStartPickProfit.setOnClickListener(TimePickViewHelp.this);
                mTvDateEndPickProfit.setOnClickListener(TimePickViewHelp.this);
            }
        });
        //设置只显示年月
        mBuilder.setType(new boolean[]{true,true,true,false,false,false});
        //设置不显示时间单位
        mBuilder.setLabel("", "", "", "", "", "");
        //起始时间
//        Calendar startDate = Calendar.getInstance();
//        startDate.setTime(mCurSelectStartDate.getTime());
//        startDate.add(Calendar.MONTH,-12);//仅可筛选近1年内的日期
        //结束时间
//        Calendar endDate = Calendar.getInstance();
//        endDate.setTime(mCurSelectEndDate.getTime());
        //设置选择的时间范围
        mBuilder.setRangDate(mDateRangeStart,mDateRangeEnd);

        //设置选中的日期
        if (isSelectStartDate){
            mBuilder.setDate(mCurSelectStartDate);
        }else {
            mBuilder.setDate(mCurSelectEndDate);
        }

        //设置分割线的颜色
        mBuilder.setDividerColor(mContext.getResources().getColor(R.color.home_line));
        //设置间距倍数
        mBuilder.setLineSpacingMultiplier(2.5f);
        mBuilder.setTextColorCenter(mContext.getResources().getColor(R.color.home_blue_text));
        mBuilder.setContentSize(18);
        mBuilder.setOutSideCancelable(false);
        mTimePickerView = mBuilder.build();
    }

    public void show(CallBackDataListener callBackDataListener){
        mCallBackDataListener = callBackDataListener;
        isSelectStartDate = true;
        setPickViewStartEndDate(mCurSelectStartDate,mCurSelectEndDate);
        mTimePickerView.show();
    }

    public void dismiss(){
        mTimePickerView.dismiss();
    }

    /**
     * 设置pickview里tv显示的开始和结束时间以及pickview选中的时间
     * */
    private void setPickViewStartEndDate(Calendar startDate, Calendar endDate) {
        if (mTvDateStartPickProfit!=null){
            mTvDateStartPickProfit.setText(getDateString(startDate,"yyyy/MM/dd"));
            mTvDateEndPickProfit.setText(getDateString(endDate,"yyyy/MM/dd"));
            if (isSelectStartDate){
                mTvDateStartPickProfit.setTextColor(mContext.getResources().getColor(R.color.rb_rank_days_text));
                mTvDateEndPickProfit.setTextColor(mContext.getResources().getColor(R.color.color_ff999999));
                if (mTimePickerView!=null){
                    mTimePickerView.setDate(startDate);//设置pickview选中的开始日期
                }
            }else {
                mTvDateStartPickProfit.setTextColor(mContext.getResources().getColor(R.color.color_ff999999));
                mTvDateEndPickProfit.setTextColor(mContext.getResources().getColor(R.color.rb_rank_days_text));
                if (mTimePickerView!=null){
                    mTimePickerView.setDate(endDate);//设置pickview选中的结束日期
                }
            }
        }
    }

    //选中的Item是第index个
    @Override
    public void onItemSelected(int index) {
        //pickview改变了选择的日期
        try {
            Date date = WheelTime.dateFormat.parse(mTimePickerView.getWheelTime().getTime());
            Calendar tempSelectDate = Calendar.getInstance();
            tempSelectDate.setTime(date);
            //使pickview里的开始时间或结束时间也随着改变
            if (isSelectStartDate){
                String tv_end = mTvDateEndPickProfit.getText().toString().trim();//2020/10/01
                Date tv_date_end = new SimpleDateFormat("yyyy/MM/dd").parse(tv_end);
                Calendar tvEndDate = Calendar.getInstance();
                tvEndDate.setTime(tv_date_end);
                setPickViewStartEndDate(tempSelectDate,tvEndDate);
            }else {
                String tv_start = mTvDateStartPickProfit.getText().toString().trim();//2020/10/01
                Date tv_date_start = new SimpleDateFormat("yyyy/MM/dd").parse(tv_start);
                Calendar tvStartDate = Calendar.getInstance();
                tvStartDate.setTime(tv_date_start);
                setPickViewStartEndDate(tvStartDate, tempSelectDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnCancel:
                mTimePickerView.dismiss();
                if (mCallBackDataListener !=null){
                    mCallBackDataListener.onCancel();
                }
                break;
            case R.id.btnSubmit:
                //日期控件确定按钮
                try {
                    String tv_start = mTvDateStartPickProfit.getText().toString().trim();//2020/10/01
                    Date tv_date_start = new SimpleDateFormat("yyyy/MM/dd").parse(tv_start);
                    Calendar tvStartDate = Calendar.getInstance();
                    tvStartDate.setTime(tv_date_start);
                    String tv_end = mTvDateEndPickProfit.getText().toString().trim();//2020/10/01
                    Date tv_date_end = new SimpleDateFormat("yyyy/MM/dd").parse(tv_end);
                    Calendar tvEndDate = Calendar.getInstance();
                    tvEndDate.setTime(tv_date_end);
                    if (tvStartDate.compareTo(tvEndDate)>0){
                        //开始日期不能大于结束日期
                        ToastUtil.showToastLong(mContext.getResources().getString(R.string.tip_profit_error_period));
                        return;
                    }
                    /*int selectStartYear = tvStartDate.get(Calendar.YEAR);
                    int selectEndYear = tvEndDate.get(Calendar.YEAR);
                    int selectStartMonth = tvStartDate.get(Calendar.MONTH);
                    int selectEndMonth = tvEndDate.get(Calendar.MONTH);
                    if (selectStartYear!=selectEndYear || selectEndMonth!=selectStartMonth){
                        //日期时间段暂不支持跨月筛选
                        ToastUtil.showToastLong(getResources().getString(R.string.tip_profit_unsuport_period));
                        return;
                    }*/
                    mTimePickerView.returnData();
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case R.id.tv_date_start_pick_profit:
                isSelectStartDate = true;
                try{
                    String tv_start = mTvDateStartPickProfit.getText().toString().trim();//2020/10/01
                    Date tv_date_start = new SimpleDateFormat("yyyy/MM/dd").parse(tv_start);
                    Calendar tvStartDate = Calendar.getInstance();
                    tvStartDate.setTime(tv_date_start);
                    String tv_end = mTvDateEndPickProfit.getText().toString().trim();//2020/10/01
                    Date tv_date_end = new SimpleDateFormat("yyyy/MM/dd").parse(tv_end);
                    Calendar tvEndDate = Calendar.getInstance();
                    tvEndDate.setTime(tv_date_end);
                    setPickViewStartEndDate(tvStartDate,tvEndDate);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case R.id.tv_date_end_pick_profit:
                isSelectStartDate = false;
                try{
                    String tv_start = mTvDateStartPickProfit.getText().toString().trim();//2020/10/01
                    Date tv_date_start = new SimpleDateFormat("yyyy/MM/dd").parse(tv_start);
                    Calendar tvStartDate = Calendar.getInstance();
                    tvStartDate.setTime(tv_date_start);
                    String tv_end = mTvDateEndPickProfit.getText().toString().trim();//2020/10/01
                    Date tv_date_end = new SimpleDateFormat("yyyy/MM/dd").parse(tv_end);
                    Calendar tvEndDate = Calendar.getInstance();
                    tvEndDate.setTime(tv_date_end);
                    setPickViewStartEndDate(tvStartDate,tvEndDate);

                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            default:
                break;

        }
    }

    /**
     * 将日历时间转成指定格式的字符串时间
     * @param calendar 日历时间
     * @param format 指定格式
     * @return
     */
    public String getDateString(Calendar calendar,String format){
        Date time = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        String dateString = simpleDateFormat.format(time);
        return dateString;
    }


    public interface CallBackDataListener {
        void onCancel();
        void onSubmit(Calendar curSelectStartDate, Calendar curSelectEndDate);
    }
}
