package com.hstypay.hstysales.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.util.TypedValue;

public class ShapeSelectorUtils {

    private static final String TAG = ShapeSelectorUtils.class.getSimpleName();
    /**
     * Shape is a rectangle, possibly with rounded corners
     */
    public static final int RECTANGLE = 0;

    /**
     * Shape is an ellipse
     */
    public static final int OVAL = 1;

    /**
     * Shape is a line
     */
    public static final int LINE = 2;

    /**
     * Shape is a ring.
     */
    public static final int RING = 3;

    /**
     * Gradient is linear (default.)
     */
    public static final int LINEAR_GRADIENT = 0;

    /**
     * Gradient is circular.
     */
    public static final int RADIAL_GRADIENT = 1;

    /**
     * Gradient is a sweep.
     */
    public static final int SWEEP_GRADIENT = 2;


    private static final int DEFAULT_SHAPE = RECTANGLE;
    private static final int DEFAULT_GRADIENT = LINEAR_GRADIENT;
    private static final int DEFAULT_STROKE_WIDTH = -1;
    private static final int DEFAULT_ROUND_RADIUS = 0;
    private static final String DEFAULT_STROKE_CORLOR = "#4272EE";


    public static StateListDrawable makeSelector(Drawable normal, Drawable pressed, Drawable focused) {
        StateListDrawable bg = new StateListDrawable();
        bg.addState(new int[]{android.R.attr.state_hovered}, pressed);
        bg.addState(new int[]{android.R.attr.state_selected}, pressed);
        bg.addState(new int[]{android.R.attr.state_checked}, pressed);
        bg.addState(new int[]{android.R.attr.state_pressed}, pressed);
        bg.addState(new int[]{android.R.attr.state_focused}, focused);
        bg.addState(new int[]{}, normal);
        return bg;
    }

    public static StateListDrawable makeSelector(Context context, int normalId, int pressedId, int focusedId) {
        StateListDrawable bg = new StateListDrawable();
        Drawable normal = context.getResources().getDrawable(normalId);
        Drawable pressed = context.getResources().getDrawable(pressedId);
        Drawable focused = context.getResources().getDrawable(focusedId);
        bg.addState(new int[]{android.R.attr.state_hovered}, pressed);
        bg.addState(new int[]{android.R.attr.state_selected}, pressed);
        bg.addState(new int[]{android.R.attr.state_checked}, pressed);
        bg.addState(new int[]{android.R.attr.state_pressed,}, pressed);
        bg.addState(new int[]{android.R.attr.state_focused}, focused);
        bg.addState(new int[]{}, normal);
        return bg;
    }


    public static Drawable createDrawableDefault(Context context, String fillColor, int shape) {
        return createShapeDrawableByShape(context, fillColor, DEFAULT_STROKE_CORLOR, DEFAULT_STROKE_WIDTH, DEFAULT_ROUND_RADIUS, shape);
    }

    public static Drawable createDrawableDefaultWithRadius(Context context, String fillColor, int shape, int roundRadius) {
        return createShapeDrawableByShape(context, fillColor, DEFAULT_STROKE_CORLOR, DEFAULT_STROKE_WIDTH, roundRadius, shape);
    }

    public static Drawable createDrawableDefault(Context context, String fillColor, String strokeColor, int strokeWidth, int shape) {
        return createShapeDrawableByShape(context, fillColor, strokeColor, strokeWidth, DEFAULT_ROUND_RADIUS, shape);
    }

    public static Drawable createRectangleDefault(Context context, String fillColor) {

        return createRectangleShape(context, fillColor, DEFAULT_STROKE_CORLOR, DEFAULT_STROKE_WIDTH, DEFAULT_ROUND_RADIUS);

    }

    public static Drawable createRectangleWithRadius(Context context, String fillColor, int roundRadius) {

        return createRectangleShape(context, fillColor, DEFAULT_STROKE_CORLOR, DEFAULT_STROKE_WIDTH, roundRadius);

    }

    public static Drawable createRectangleWithRadii(Context context, String fillColor, float[] radius) {
        return createRectangleWithRadii(context, fillColor, DEFAULT_STROKE_CORLOR, DEFAULT_STROKE_WIDTH, radius,LINEAR_GRADIENT);
    }

    private static Drawable createRectangleWithRadii(Context context, String fillColor, String strokeColor, int strokeWidth,
                                                      float[] radius, int gradient) {
        if (context == null || TextUtils.isEmpty(fillColor)) {
            return null;
        }
        if (!fillColor.startsWith("#") || !strokeColor.startsWith("#")) {
            LogUtil.e(TAG, "Here a String color must be start with '#'");
            return null;
        }

        if (gradient < 0 || gradient > 2) {
            gradient = DEFAULT_GRADIENT;
        }
        if (TextUtils.isEmpty(strokeColor)) {
            strokeColor = DEFAULT_STROKE_CORLOR;
        }
        strokeWidth = dp2px(context, strokeWidth); // dp 边框宽度

        int sColor = Color.parseColor(strokeColor);//边框颜色
        int fColor = Color.parseColor(fillColor);//内部填充颜色

        GradientDrawable drawable = new GradientDrawable();//创建drawable

        drawable.setColor(fColor);
        drawable.setGradientType(gradient);
        drawable.setShape(DEFAULT_SHAPE);
        drawable.setCornerRadii(radius);
        drawable.setStroke(strokeWidth, sColor);

        return drawable;
    }

    public static Drawable createRectangleShape(Context context, String fillColor,
                                                String strokeColor, int strokeWidth,
                                                int roundRadius) {
        return createShapeDrawableByShape(context, fillColor, strokeColor, strokeWidth, roundRadius, RECTANGLE);
    }


    public static Drawable createOvalDefault(Context context, String fillColor) {

        return createOvalShape(context, fillColor, DEFAULT_STROKE_CORLOR, DEFAULT_STROKE_WIDTH, DEFAULT_ROUND_RADIUS);

    }

    public static Drawable createOvalByRadius(Context context, String fillColor, int roundRadius) {

        return createOvalShape(context, fillColor, DEFAULT_STROKE_CORLOR, DEFAULT_STROKE_WIDTH, roundRadius);

    }


    public static Drawable createOvalShape(Context context, String fillColor,
                                           String strokeColor, int strokeWidth,
                                           int roundRadius) {
        return createShapeDrawableByShape(context, fillColor, strokeColor, strokeWidth, roundRadius, OVAL);
    }


    private static Drawable createShapeDrawableByShape(Context context, String fillColor,
                                                       String strokeColor, int strokeWidth,
                                                       int roundRadius, int shape) {
        return createShapeDrawable(context, fillColor, strokeColor, strokeWidth, roundRadius, shape, LINEAR_GRADIENT);
    }

    private static Drawable createShapeDrawable(Context context, String fillColor,
                                                String strokeColor, int strokeWidth,
                                                int roundRadius, int shape, int gradient) {
        if (context == null || TextUtils.isEmpty(fillColor)) {
            return null;
        }
        if (!fillColor.startsWith("#") || !strokeColor.startsWith("#")) {
            LogUtil.e(TAG, "Here a String color must be start with '#'");
            return null;
        }
        if (roundRadius < 0) {
            roundRadius = DEFAULT_STROKE_WIDTH;
        }
        if (shape < 0 || shape > 2) {
            shape = DEFAULT_SHAPE;
        }
        if (gradient < 0 || gradient > 2) {
            gradient = DEFAULT_GRADIENT;
        }
        if (TextUtils.isEmpty(strokeColor)) {
            strokeColor = DEFAULT_STROKE_CORLOR;
        }
        strokeWidth = dp2px(context, strokeWidth); // dp 边框宽度
        roundRadius = dp2px(context, roundRadius); // dp 圆角半径

        int sColor = Color.parseColor(strokeColor);//边框颜色
        int fColor = Color.parseColor(fillColor);//内部填充颜色

        GradientDrawable drawable = new GradientDrawable();//创建drawable

        drawable.setColor(fColor);
        drawable.setGradientType(gradient);
        drawable.setShape(shape);
        drawable.setCornerRadius(roundRadius);
        drawable.setStroke(strokeWidth, sColor);

        return drawable;
    }

    /**
     * dp转px
     *
     * @param context
     * @param dpVal
     * @return
     */
    public static int dp2px(Context context, float dpVal) {

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dpVal, context.getResources().getDisplayMetrics());

    }

    public static String getColorString(Context context, int id) {
        StringBuffer stringBuffer = new StringBuffer();
        int color = context.getResources().getColor(id);
        int red = (color & 0xff0000) >> 16;
        int green = (color & 0x00ff00) >> 8;
        int blue = (color & 0x0000ff);


        stringBuffer.append(Integer.toHexString(red));
        stringBuffer.append(Integer.toHexString(green));
        stringBuffer.append(Integer.toHexString(blue));
        return "#" + stringBuffer.toString();
    }
}
