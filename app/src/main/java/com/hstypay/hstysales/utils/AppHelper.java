package com.hstypay.hstysales.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.List;


@SuppressLint("NewApi")
public class AppHelper {
    private static final String TAG = "AppHelper";

    public static boolean isSdcardExist() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * 如果SDcard存在，则返回SDcard上文件的目录 否则，抛异常
     */
    public static String getCacheDir()
            throws Exception {
        if (!isSdcardExist()) {
            throw new Exception("SD卡不存在");
        }
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        path += Constants.FILE_CACHE_ROOT;
        File dir = new File(path);
        if (!dir.exists()) {
            if (!dir.mkdir()) {
                throw new Exception("创建文件缓存目录失败");
            }
        }
        return path;
    }

    public static String getImageDir() {
        String path;
        try {
            path = getCacheDir() + System.currentTimeMillis();
        } catch (Exception e) {
            e.printStackTrace();
            path = "";
        }
        return path;
    }

    public static String getImageCacheDir(String filePath) {
        String path;
        try {
            path = getCacheDir() + System.currentTimeMillis() + PicUtil.getFileType(filePath);
        } catch (Exception e) {
            e.printStackTrace();
            path = "";
        }
        return path;
    }

    public static String getImgCacheDir()
            throws Exception {
        return FileUtils.getAppCache();
    }

    /**
     * 创建缓存目录
     * 如果SDcard存在，则返回SDcard上文件的目录 否则，抛异常
     */
    public static String getAppCacheDir()
            throws Exception {
        if (!isSdcardExist()) {
            throw new Exception("SD卡不存在");
        }
        File root = new File(FileUtils.getRootPath());
        LogUtil.d("path1===" + root);
        if (!root.exists()) {
            if (!root.mkdirs()) {
                throw new Exception("创建文件缓存目录失败");
            }
        }
        String path = FileUtils.getAppCache();
        LogUtil.d("path===" + path);
        File dir = new File(path);
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                throw new Exception("创建文件缓存目录失败");
            }
        }
        String logPath = FileUtils.getAppLog();
        File logDir = new File(logPath);
        if (!logDir.exists()) {
            if (!logDir.mkdirs()) {
                throw new Exception("创建log文件缓存目录失败");
            }
        }
        String appPath = FileUtils.getAppPath();
        File f = new File(appPath);
        if (!f.exists()) {
            if (!f.mkdirs()) {
                throw new Exception("创建APP文件缓存目录失败");
            }
        }

        return path;
    }


    /**
     * 获取数据库的存放路径，策略为 1.有卡，则主DB存放在应用程序的目录下
     * ，辅DB防止卡的目录下。 2.无卡，则主辅DB都放在应用程序目录下。
     *
     * @param mainAble 是否有卡
     * @return
     */
    public static String getDBPath(boolean mainAble, String dbName) {
        if (isSdcardExist())//false)// //不允许 读取数据库文件
        {
            // 有卡
            if (mainAble) {
                return MyApplication.getContext().getDatabasePath(dbName).getAbsolutePath();
            } else {
                try {
                    String path = getCacheDir() + "databases/";
                    File dir = new File(path);
                    if (!dir.exists()) {
                        if (!dir.mkdirs()) {
                            Log.e(TAG, "--->create databases dir fail!");
                            return null;
                        }
                    }
                    return path + dbName;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        } else {
            // 无卡
            return MyApplication.getContext().getDatabasePath(dbName).getAbsolutePath();
        }
    }

    /**
     * 保存图片到本地
     *
     * @param fileDirect 存放路径
     * @param bitmap     数据
     * @return 图片存储路径
     */
    public static String saveImag(String fileDirect, Bitmap bitmap, CompressFormat format, String fileName) {
        File file = new File(fileDirect);
        String filePath = fileDirect + File.separatorChar + fileName;
        if (!file.exists())
            file.mkdirs();
        BufferedOutputStream dos = null;
        try {
            dos = new BufferedOutputStream(new FileOutputStream(filePath));
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (dos != null)
                    dos.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        bitmap.compress(format, 100, dos);
        return filePath;
    }

    public static String saveImag(String fileDirect, byte[] data, String fileName) {
        File file = new File(fileDirect);
        String filePath = fileDirect + File.separatorChar + fileName;
        if (!file.exists())
            file.mkdirs();
        FileOutputStream dos = null;
        File imgFile = new File(filePath);
        if (!imgFile.exists()) {
            try {
                if (!imgFile.createNewFile()) {
                    LogUtil.d("Jeremy-创建文件失败");
                }
                dos = new FileOutputStream(filePath);
                dos.write(data);
                dos.flush();
            } catch (Exception e) {
            } finally {
                try {
                    if (dos != null)
                        dos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return filePath;
    }

    public static int getResouceIdByName(Context context, String type, String name) {
        return context.getResources().getIdentifier(name, type, context.getPackageName());
    }

    /*	*//** 版本 */
    /*
     * public static String getVersionName(Context mContext) throws Exception {
     * // 获取packagemanager的实例 PackageManager packageManager =
     * mContext.getPackageManager(); // getPackageName()是你当前类的包名，0代表是获取版本信息
     * PackageInfo packInfo =
     * packageManager.getPackageInfo(mContext.getPackageName(),0); String
     * version = packInfo.versionName; return version; }
     */

    /**
     * 版本
     *
     * @param context
     * @return
     */
    public static int getVerCode(Context context) {
        int verCode = -1;
        try {
            verCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
        return verCode;
    }

    public static String getVerName(Context context) {
        String verName = "";
        try {
            verName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
        return verName;
    }

    public static int getAndroidSDKVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static String getAndroidSDKVersionName() {
        return android.os.Build.VERSION.RELEASE;
    }

    /**
     * 程序名称
     *
     * @param context
     * @return
     */
    public static String getAppName(Context context) {
        String verName = context.getResources().getText(R.string.app_name).toString();
        return verName;
    }

    public static String getAppPackageName(Context context) {
        String pName = "";
        try {
            pName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName;
        } catch (NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
        return pName;
    }

    @SuppressLint("MissingPermission")
    public static String getImei(Context context) {
      /*  TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = tm.getDeviceId();
        if (imei == null) {
            imei = "";
        }
        return imei;*/

        try {
            if (StringUtils.isEmptyOrNull(MyApplication.getImei())) {
                String uuid = System.currentTimeMillis() + "";
                MyApplication.setImei(uuid);
                return uuid;
            } else {
                return MyApplication.getImei();
            }
        } catch (Exception e) {
            e.printStackTrace();

            return System.currentTimeMillis() + "";
        }
    }

    @SuppressLint("MissingPermission")
    public static String getImsi(Context context) {
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String imsi = mTelephonyMgr.getSubscriberId();
        if (imsi == null) {
            imsi = "";
        }
        return imsi;
    }

    /**
     * 手机品牌跟
     */
    public static String[] getBrand() {
        String brand = android.os.Build.BRAND;
        String mType = android.os.Build.MODEL;
        return new String[]{brand, mType};

    }

    // ////////////////////////////////////////////////////////////////////
    private static final String[] PHONES_PROJECTION = new String[]{Phone._ID, Phone.DISPLAY_NAME, Phone.NUMBER,
            Phone.CONTACT_ID, Photo.TIMES_CONTACTED, Phone.LAST_TIME_CONTACTED};

    /**
     * 实现文本复制功能
     *
     * @param text
     */
    public static void copy(String text, Context context) {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        cmb.setText(text);
    }

    public static void copy2(String text, Context context){
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData mClipData = ClipData.newPlainText(null, text);
        cm.setPrimaryClip(mClipData);
    }

    /**
     * 实现粘贴功能
     *
     * @param context
     * @return
     */
    @SuppressWarnings("deprecation")
    public static String paste(Context context) {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        return cmb.getText().toString().trim();
    }

    /**
     * 震动
     *
     * @param context
     */
    public static void execVibrator(Context context) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {0, 10, 20, 30}; // 停止 开启 停止 开启
        vibrator.vibrate(pattern, -1); //重复两次上面的pattern 如果只想震动一次，index设为
    }

    @SuppressLint("MissingPermission")
    public static String getIMEI() {
        TelephonyManager tm =
                (TelephonyManager) MyApplication.getContext().getSystemService(Context.TELEPHONY_SERVICE);
        if (Constants.isDebug) {
            return System.currentTimeMillis() + "";
        } else {
            return tm.getDeviceId();
        }
    }

    @SuppressLint("MissingPermission")
    public static String getIMSI() {
        TelephonyManager tm =
                (TelephonyManager) MyApplication.getContext().getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getSubscriberId();
    }

    /**
     * 获取application中指定的meta-data
     *
     * @return 如果没有获取成功(没有对应值 ， 或者异常)，则返回值为空
     */
    public static String getAppMetaData(Context ctx, String key) {
        if (ctx == null || TextUtils.isEmpty(key)) {
            return null;
        }
        String resultData = null;
        try {
            PackageManager packageManager = ctx.getPackageManager();
            if (packageManager != null) {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(ctx.getPackageName(), PackageManager.GET_META_DATA);
                if (applicationInfo != null) {
                    if (applicationInfo.metaData != null) {
                        resultData = applicationInfo.metaData.getString(key);
                    }
                }

            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }

        return resultData;
    }

    public static void call(String phone, Context context) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (ActivityNotFoundException a) {
            a.getMessage();
        }
    }

    /**
     * 返回图片地址
     *
     * @param originalUri
     * @return String
     */
    @SuppressLint("NewApi")
    public static String getPicPath(Uri originalUri) {
        ContentResolver mContentResolver = MyApplication.getContext().getContentResolver();
        String originalPath = null;
        if ((originalUri + "").contains("/sdcard")) {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/sdcard"));
        } else if ((originalUri + "").contains("/data")) {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/data"));
        } else if ((originalUri + "").contains("/storage")) // 小米 手机 适配
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/storage"));
        } else {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT
                    && originalUri.toString().contains("documents")) {
                String wholeID = DocumentsContract.getDocumentId(originalUri);
                String id = wholeID.split(":")[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String sel = MediaStore.Images.Media._ID + " = ?";
                Cursor cursor =
                        mContentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                column,
                                sel,
                                new String[]{id},
                                null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    originalPath = cursor.getString(columnIndex);
                }
                cursor.close();
            } else {

                Cursor cursor = mContentResolver.query(originalUri, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    originalPath = cursor.getString(1);
                    cursor.close();
                }
            }

        }
        return originalPath;
    }

    /**
     * 返回手机运营商名称，在调用支付前调用作判断
     *
     * @param context
     * @return
     */
    public static String getProvidersName(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return "";
        }
        String ProvidersName = "";
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String IMSI = telephonyManager.getSubscriberId();
        Log.i("qweqwes", "运营商代码" + IMSI);
        if (IMSI != null) {
            if (IMSI.startsWith("46000") || IMSI.startsWith("46002") || IMSI.startsWith("46007")) {
                ProvidersName = "中国移动";
            } else if (IMSI.startsWith("46001") || IMSI.startsWith("46006")) {
                ProvidersName = "中国联通";
            } else if (IMSI.startsWith("46003")) {
                ProvidersName = "中国电信";
            }
            Log.i("qweqwes", "运营商==" + ProvidersName);
            return ProvidersName;
        } else {
            return "";
        }
    }

    public static String getOsVersion() {
        return android.os.Build.DISPLAY;
    }

    public static String getNetworkType(Context context) {
        String strNetworkType = "UnKnown";
        final NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.getType() == 1) {
            strNetworkType = "WIFI";
        } else if (activeNetworkInfo != null && activeNetworkInfo.getType() == 0) {
            String subtypeName = activeNetworkInfo.getSubtypeName();
            switch (((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getNetworkType()) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN: //api<8 : replace by 11
                    strNetworkType = "2G";
                    break;
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
                case TelephonyManager.NETWORK_TYPE_EHRPD: //api<11 : replace by 12
                case TelephonyManager.NETWORK_TYPE_HSPAP: //api<13 : replace by 15
                    strNetworkType = "3G";
                    break;
                case TelephonyManager.NETWORK_TYPE_LTE:
                    strNetworkType = "4G";
                    break;
                default:
                    if (subtypeName.equalsIgnoreCase("TD-SCDMA") || subtypeName.equalsIgnoreCase("WCDMA") || subtypeName.equalsIgnoreCase("CDMA2000")) {
                        strNetworkType = "3G";
                        break;
                    }
                    strNetworkType = subtypeName;
                    break;
            }
        }
        return strNetworkType;
    }

    public static String getIPAddress(Context context) {
        NetworkInfo info = ((ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info != null && info.isConnected()) {
            if (info.getType() == ConnectivityManager.TYPE_MOBILE) {//当前使用2G/3G/4G网络
                try {
                    //Enumeration<NetworkInterface> en=NetworkInterface.getNetworkInterfaces();
                    for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                        NetworkInterface intf = en.nextElement();
                        for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                            InetAddress inetAddress = enumIpAddr.nextElement();
                            if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                                return inetAddress.getHostAddress();
                            }
                        }
                    }
                } catch (SocketException e) {
                    e.printStackTrace();
                }

            } else if (info.getType() == ConnectivityManager.TYPE_WIFI) {//当前使用无线网络
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                String ipAddress = intIP2StringIP(wifiInfo.getIpAddress());//得到IPV4地址
                return ipAddress;
            }
        } else {
            //当前无网络连接,请在设置中打开网络
        }
        return null;
    }

    /**
     * 将得到的int类型的IP转换为String类型
     *
     * @param ip
     * @return
     */
    public static String intIP2StringIP(int ip) {
        return (ip & 0xFF) + "." +
                ((ip >> 8) & 0xFF) + "." +
                ((ip >> 16) & 0xFF) + "." +
                (ip >> 24 & 0xFF);
    }


    /**
     * 区分是否是行业APP
     * 给H5链接加上渠道标识
     *
     * @return
     */
    public static String getHtmlString() {
        String htmlString = "";
        if (!StringUtils.isEmptyOrNull(Constants.ORG_ID)){
            htmlString = "?orgId="+Constants.ORG_ID+"&orgType="+Constants.ORG_TYPE;
        }
        return htmlString;
    }

    /**
     * 区分是否是行业APP
     * 给H5链接加上渠道标识
     *
     * @return
     */
    public static String getHtmlAndString() {
        String htmlString = "";
        if (!StringUtils.isEmptyOrNull(Constants.ORG_ID)){
            htmlString = "orgId="+Constants.ORG_ID+"&orgType="+Constants.ORG_TYPE+"&";
        }
        return htmlString;
    }

    public static String getDownloadApkDir() {
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (!StringUtils.isEmptyOrNull(installChannel)){
            return  "/"+installChannel+"/"+installChannel+".apk";
        }else {
            return Constants.DOWNLOADPATH + Constants.APK_FILE_NAME;
        }
    }

    /**
     * 区分是否是行业APP
     *
     * @return
     */
    public static String getUserAgent(Context context) {
        String userAgent = "";
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            switch (installChannel) {
                default:
                    userAgent = " hstypay/" + VersionUtils.getVersionCode(context) + " (hsty_app_android)";
                    break;
            }
        }
        return userAgent;
    }


    /**
     * 判断是否安装了微信
     * */
    public static boolean isWeixinAvilible(Context context) {
        final PackageManager packageManager = context.getPackageManager();// 获取packagemanager
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);// 获取所有已安装程序的包信息
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                if (pn.equals("com.tencent.mm")) {
                    return true;
                }
            }
        }

        return false;
    }
}