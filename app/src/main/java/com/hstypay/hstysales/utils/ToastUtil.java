package com.hstypay.hstysales.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;


public class ToastUtil {

    private static Toast toast;
    private static LayoutInflater mInflater;

    /**
     * 自定义Toast
     *
     * @param message
     */
    public static void showToastShort(CharSequence message) {
        showToast(MyApplication.getContext(),message,Toast.LENGTH_SHORT);
    }

      public static void  showToastLong(CharSequence message) {
          showToast(MyApplication.getContext(),message,Toast.LENGTH_LONG);
    }

    public static void showToast(Context context, CharSequence msg, int length)
    {
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View toastRoot = mInflater.inflate(R.layout.view_toast, null);
        TextView message = (TextView)toastRoot.findViewById(R.id.tv_content);
        message.setText(msg);
        Toast toastStart = new Toast(context);
        toastStart.setGravity(Gravity.CENTER, 0, 0);

        toastStart.setDuration(Toast.LENGTH_SHORT);
        toastStart.setView(toastRoot);
        toastStart.show();
    }

    public static String getString(int resId)
    {
        return MyApplication.getContext().getString(resId);
    }
}
