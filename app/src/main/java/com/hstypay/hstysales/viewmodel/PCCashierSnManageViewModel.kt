package com.hstypay.hstysales.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.hstypay.hstysales.bean.ActivationCode
import com.hstypay.hstysales.bean.StoreList
import com.hstypay.hstysales.commonlib.base.BaseViewModel
import com.hstypay.hstysales.commonlib.http.ApiManager

/**
 * @Author dean.zeng
 * @Description PC收银激活码管理
 * @Date 2020-07-16 16:57
 **/
class PCCashierSnManageViewModel : BaseViewModel() {
    val storeList = MutableLiveData<List<StoreList>>()
    val activationCodeList = MutableLiveData<List<ActivationCode>>()

    /*
    门店列表
     */
    fun findStores(map: Map<String, String>) {
        requestSync({ ApiManager.get().getService().findStores(map/*"10000", "1", "1"*/) },
                {
                    storeList.postValue(it)
                })
    }

    /*
    获取激活码列表
     */
    fun getActivationData(pageSize: String, currentPage: String,
                          activationStatus: String,
                          merchantId: String,
                          storeMerchantId: String) {
        requestSync({
            ApiManager.get().getService().activationCodeList(pageSize, currentPage,
                    activationStatus, merchantId,storeMerchantId)
        }, {
            if (it!=null)
            activationCodeList.postValue(it.data)
        },{
            activationCodeList.postValue(null)
        })
    }


}