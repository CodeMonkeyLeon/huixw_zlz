package com.hstypay.hstysales.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.hstypay.hstysales.commonlib.base.BaseViewModel
import com.hstypay.hstysales.commonlib.http.ApiManager

/**
 * @Author dean.zeng
 * @Description TODO
 * @Date 2020-07-17 15:52
 **/
class CreateActivationCodeViewMode : BaseViewModel() {
    val successLiveData = MutableLiveData<Any>()

    fun addActivationCode(storeMerchantId: String, storeMerchantShortName: String, merchantId: String) {
        requestSync({ ApiManager.get().getService().addActivationCode(storeMerchantId, storeMerchantShortName, merchantId) }, {
            successLiveData.postValue(it)
        })
    }

}