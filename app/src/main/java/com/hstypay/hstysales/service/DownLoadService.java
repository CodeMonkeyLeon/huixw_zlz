package com.hstypay.hstysales.service;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.bean.ProgressBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.SpStayUtil;
import com.hstypay.hstysales.utils.SpUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by admin on 2017/8/12.
 */

public class DownLoadService extends Service {
    /**
     * 安卓系统下载类
     **/
    private boolean isInstall = false;
    private String appurl;
    private String mVersionName;
    private String mIntentName;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //EventBus.getDefault().register(this);
        appurl = intent.getStringExtra("appurl");
        mVersionName = intent.getStringExtra("versionName");
        mIntentName = intent.getStringExtra(Constants.INTENT_NAME);
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + AppHelper.getDownloadApkDir();
        File file = new File(path);
        if (file.exists()) {
            if (!file.delete()) {
                LogUtil.d("Jeremy-原安装文件删除失败！");
            }
        }
        try {
            // 调用下载
            LogUtil.d("版本升级---"+appurl);
            downloadApp(appurl);
            //downloadApp("http://" + appurl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

//=============================================================

    public void downloadApp(final String url){
        new Thread(new Runnable() {
            @Override
            public void run() {
                File file = null;
                InputStream is = null;
                FileOutputStream fos = null;
                try {
                    LogUtil.d("版本升级---"+url);
                    Request request = new Request.Builder()
                            .url(url)
                            .build();
                    Response response = new OkHttpClient().newCall(request).execute();
                    String SDPath = Environment.getExternalStorageDirectory().getAbsolutePath() + AppHelper.getDownloadApkDir();
                    if (response.isSuccessful()) {
                        byte[] buf = new byte[2048];
                        int len = 0;
                        long total = response.body().contentLength();
                        SpUtil.putString(MyApplication.getContext(),"length",String.valueOf(total));
                        file = new File(SDPath);
                        if(!file.getParentFile().exists()){
                            System.out.println("父文件夹不存在，创建之");
                            file.getParentFile().mkdirs();
                        }
                        if (!file.exists()) {
                            if (!file.createNewFile()) {
                                LogUtil.d("Jeremy-安装文件创建失败！");
                            }
                        } else {
                            if (file.length() == total) {
                                EventBus.getDefault().post(new NoticeEvent(mIntentName, Constants.UPDATE_DOWNLOAD_SUCCESS, file));
                                return;
                            } else {
                                if (!file.delete()) {
                                    LogUtil.d("Jeremy-原安装文件删除失败！");
                                }
                                file = new File(SDPath);
                                if (!file.createNewFile()) {
                                    LogUtil.d("Jeremy-安装文件创建失败！");
                                }
                            }
                        }
                        is = response.body().byteStream();
                        fos = new FileOutputStream(file);
                        ProgressBean progressBean = new ProgressBean();
                        progressBean.setTotalSize(total);
                        long sum = 0;
                        while ((len = is.read(buf)) != -1) {
                            fos.write(buf, 0, len);
                            sum += len;
                            progressBean.setDownSize(sum);
                            EventBus.getDefault().post(new NoticeEvent(mIntentName, Constants.UPDATE_DOWNLOADING, progressBean));
                        }
                        fos.flush();
                        is.close();
                        fos.close();
                        Log.d("h_bl", "文件下载成功");
                        SpStayUtil.putString(MyApplication.getContext(), Constants.UPDATE_APP_URL, SDPath);
                        SpStayUtil.putString(MyApplication.getContext(), Constants.UPDATE_VERSION_NAME, mVersionName);
                        EventBus.getDefault().post(new NoticeEvent(mIntentName, Constants.UPDATE_DOWNLOAD_SUCCESS, file));
                        stopSelf();
                    }
                } catch (IOException e) {
                    Log.d("h_bl", "文件下载失败" + e.toString());
                    e.printStackTrace();
                    EventBus.getDefault().post(new NoticeEvent(mIntentName, Constants.UPDATE_DOWNLOAD_FAILED));
                    if (file != null) {
                        if (!file.delete()) {
                            LogUtil.d("Jeremy-原安装文件删除失败！");
                        }
                    }
                } finally {
                    if (fos != null) {
                        try {
                            fos.flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

}
