
package com.hstypay.hstysales.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.hstypay.hstysales.BuildConfig;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.MerchantListActivity;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.bean.PushModel;
import com.hstypay.hstysales.bean.ReceiveMerchantBean;
import com.hstypay.hstysales.broadcast.MerchantAppOpenReceiver;
import com.hstypay.hstysales.broadcast.MerchantStatusReceiver;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.igexin.sdk.GTIntentService;
import com.igexin.sdk.message.GTCmdMessage;
import com.igexin.sdk.message.GTNotificationMessage;
import com.igexin.sdk.message.GTTransmitMessage;

import java.util.HashMap;
import java.util.Map;


public class GetuiReciverService extends GTIntentService {


    @Override
    public void onReceiveServicePid(Context context, int i) {

    }

    // 接收 cid
    @Override
    public void onReceiveClientId(Context context, String s) {

    }

    // 处理透传消息
    @Override
    public void onReceiveMessageData(Context context, GTTransmitMessage gtTransmitMessage) {
        try {
            byte[] payload = gtTransmitMessage.getPayload();

            Log.e("zhouwei", "接收状1====" + gtTransmitMessage.getMessageId());
            Log.e("zhouwei", "接收状2====" + gtTransmitMessage.toString());
            Log.e("zhouwei", "接收状3====" + gtTransmitMessage.getPkgName());
            Log.e("zhouwei", "接收状4====" + gtTransmitMessage.getAppid());
            Log.e("zhouwei", "接收状5====" + new String(gtTransmitMessage.getPayload()));

            String data = new String(payload);
            LogUtil.d("data====" + data);
            Gson gson = new Gson();
            PushModel pushModel = gson.fromJson(data, PushModel.class);
            showNotify(context, pushModel);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showNotify(Context context, PushModel data) {
        int id = (int) (System.currentTimeMillis() / 1000);
        PendingIntent pendingIntent = null;
        Intent clickIntent = null;
        int messageKind = data.getMessageKind();//消息类型
        if (messageKind == 80003) {  //商户进件信息变更
            LogUtil.d("GetuiReciverService", "商户进件信息变更>>>>>");
            String data_merchant = data.getData();
            Gson gson = new Gson();
            ReceiveMerchantBean merchantBean = gson.fromJson(data_merchant, ReceiveMerchantBean.class);
            int mchQueryStatus = merchantBean.getMchQueryStatus();
            upDataMerChantData(MerchantListActivity.pageSize, 1, mchQueryStatus);

            clickIntent = new Intent(context, MerchantStatusReceiver.class);//点击通知之后要发送的广播
            clickIntent.putExtra(MerchantStatusReceiver.INTENT_EXTRA_KEY_DATA, data_merchant);
        } else if (messageKind == 80005) {  //商户应用申请
            String data_merchant = data.getData();
            if (Build.VERSION.SDK_INT >= 26 && MyApplication.getContext().getApplicationInfo().targetSdkVersion > 22) {
                if (Settings.canDrawOverlays(MyApplication.getContext())) {
                    sendExamineBroadcast(data, BuildConfig.APPLICATION_ID + ".AppOpenManagerActivity");
                }
            } else {
                sendExamineBroadcast(data, BuildConfig.APPLICATION_ID + ".AppOpenManagerActivity");
            }
            clickIntent = new Intent(context, MerchantAppOpenReceiver.class);//点击通知之后要发送的广播
            clickIntent.putExtra(MerchantStatusReceiver.INTENT_EXTRA_KEY_DATA, data_merchant);
        }
        pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), id, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = null;
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ONE_ID = getPackageName();
            Uri mUri = Settings.System.DEFAULT_NOTIFICATION_URI;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ONE_ID, "driver", NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription("description");
            manager.createNotificationChannel(mChannel);
            mChannel.setSound(mUri, Notification.AUDIO_ATTRIBUTES_DEFAULT);
            notification = new Notification.Builder(this, CHANNEL_ONE_ID)
                    .setChannelId(CHANNEL_ONE_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(data.getTitle())// 设置通知栏标题
                    .setContentText(data.getContent())
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .setAutoCancel(true)
                    .build();
        } else {
            // 提升应用权限
            notification = new Notification.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(data.getTitle())// 设置通知栏标题
                    .setContentText(data.getContent())
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .setAutoCancel(true)

                    .build();
        }
        if (data.getMessageKind() == null) {
            manager.notify(0, notification);
        } else {
            manager.notify(data.getMessageKind(), notification);
        }
        openMusic(context);
        //设置震动
        Vibrator vibrator = (Vibrator) context.getSystemService(context.VIBRATOR_SERVICE);
        vibrator.vibrate(200);//震动时长 ms
    }

    //打开音乐的方法
    public void openMusic(Context context) {
        RingtoneManager rm = new RingtoneManager(context);//初始化 系统声音
        Uri uri = rm.getDefaultUri(rm.TYPE_NOTIFICATION);//获取系统声音路径
        Ringtone mRingtone = rm.getRingtone(context, uri);//通过Uri 来获取提示音的实例对象
        mRingtone.play();//播放:
    }

    private void upDataMerChantData(int pageSize, int currentPage, int activateStatus) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            map.put("mchQueryStatus", activateStatus);
            ServerClient.newInstance(MyApplication.getContext()).getMerchantList(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_LIST_REFRESH, map);
            ServerClient.newInstance(MyApplication.getContext()).homeData(MyApplication.getContext(), Constants.TAG_HOME_DATA, null);
            ServerClient.newInstance(MyApplication.getContext()).getMerchantList(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_LIST_REFRESH + MerchantStatusReceiver.getPagerType(activateStatus), map);

        }
    }

    private void sendExamineBroadcast(PushModel data, String activityName) {
        Intent intent = new Intent(Constants.ACTION_PUSH_STATUS);
        intent.putExtra("title", data.getTitle());
        intent.putExtra("content", data.getContent());
        intent.putExtra("data", data.getData());
        intent.putExtra("activityName", activityName);
        intent.setComponent(new ComponentName(AppHelper.getAppPackageName(MyApplication.getContext()),
                "com.hstypay.hstysales.broadcast.PushReceiver"));
        sendBroadcast(intent);
    }

    // cid 离线上线通知
    @Override
    public void onReceiveOnlineState(Context context, boolean b) {


        LogUtil.i("zhouwei", "推送状态" + b);
    }

    // 各种事件处理回执
    @Override
    public void onReceiveCommandResult(Context context, GTCmdMessage gtCmdMessage) {

    }


    // 通知到达，只有个推通道下发的通知会回调此方法
    @Override
    public void onNotificationMessageArrived(Context context, GTNotificationMessage gtNotificationMessage) {

    }

    // 通知点击，只有个推通道下发的通知会回调此方法
    @Override
    public void onNotificationMessageClicked(Context context, GTNotificationMessage gtNotificationMessage) {

    }


}

