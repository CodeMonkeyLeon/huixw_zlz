package com.hstypay.hstysales.service.live;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.Zxing.HandlerManager;
import com.hstypay.hstysales.utils.LogUtil;


public class PlayerMusicService extends Service {
    private final static String TAG = "hehui";
    private MediaPlayer mMediaPlayer;
    private boolean isStop =false;

    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == HandlerManager.FOREGROUNDLIVE) {
                isStop = true;
            }
        };
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtil.i(TAG, TAG + "---->onCreate,start mc service");
        mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.silent);
        mMediaPlayer.setLooping(true);
        HandlerManager.registerHandler(HandlerManager.FOREGROUNDLIVE,handler);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtil.i(TAG, TAG + "---->onStartCommand");
        new Thread(new Runnable() {
            @Override
            public void run() {
                startPlayMusic();
            }

        }).start();
        return START_STICKY;
    }

    private void startPlayMusic() {
        if (mMediaPlayer != null) {
            LogUtil.i(TAG, "mMediaPlayer is playing " + mMediaPlayer.getDuration());
        }
        if (mMediaPlayer != null && !mMediaPlayer.isPlaying()) {
            LogUtil.i(TAG, "start play music ");
            mMediaPlayer.start();
        }
    }

    private void stopPlayMusic() {
        if (mMediaPlayer != null) {
            LogUtil.i(TAG, "stop music ");
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopPlayMusic();
        LogUtil.i(TAG, TAG + "---->onDestroy,stop service");
        if(!isStop){
            Intent intent = new Intent(getApplicationContext(), PlayerMusicService.class);
            startService(intent);
        }
    }
}
