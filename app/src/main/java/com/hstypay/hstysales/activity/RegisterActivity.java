package com.hstypay.hstysales.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.ConfigUtil;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.ImageUtil;
import com.hstypay.hstysales.utils.LocationUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.SpStayUtil;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.utils.VersionUtils;
import com.hstypay.hstysales.widget.AndroidBug5497Workaround;
import com.hstypay.hstysales.widget.CommonNoticeDialog;
import com.hstypay.hstysales.widget.SelectCommonDialog;
import com.hstypay.hstysales.widget.SelectDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/27 10:33
 * @描述: ${TODO}
 */

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private WebView mWvResgister;
    private ImageView mIvBack, mIvClose;
    private TextView mTvTitlte;
    private Button mBtnClose;

    private ValueCallback<Uri> mUploadMessage;// 表单的数据信息
    private ValueCallback<Uri[]> mUploadCallbackAboveL;
    private final static int FILECHOOSER_RESULTCODE = 1;// 表单的结果回调</span>
    private Uri imageUri;
    private String url;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private String[] locationPermissionArray = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE3 = 1003;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE5 = 1005;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE6 = 1006;
    private String mPhoto;

    private ProgressBar mPg;
    private String fromUrl;
    private boolean isAllowClose;
    private SelectDialog mDialog;
    private LinearLayout mLlWebview;
    private RelativeLayout mRlNetError;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        StatusBarUtil.setTranslucentStatus(this);
        AndroidBug5497Workaround.assistActivity(this);
        fromUrl = getIntent().getStringExtra(Constants.REGISTER_INTENT_URL);
        isAllowClose = getIntent().getBooleanExtra(Constants.REGISTER_ALLOW_CLOSE, false);
        if (savedInstanceState != null) {
            url = savedInstanceState.getString("url");
        } else {
            url = getIntent().getStringExtra(Constants.REGISTER_INTENT);
        }
        //if (Constants.URL_REGISTER_FIRST.equals(url)) {
        SpUtil.removeKey(Constants.MERCHANT_NAME);
        //}
        LogUtil.d("RegisterActivity oncreate" + url);
        if (url == null) {
            return;
        }
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvClose = (ImageView) findViewById(R.id.iv_close);
        mBtnClose = (Button) findViewById(R.id.btn_close);
        mTvTitlte = (TextView) findViewById(R.id.tv_title);
        mWvResgister = (WebView) findViewById(R.id.wv_register);
        mLlWebview = (LinearLayout) findViewById(R.id.ll_webview);
        mRlNetError = (RelativeLayout) findViewById(R.id.layout_net_error);
        mPg = (ProgressBar) findViewById(R.id.progressBar);
        mIvBack.setOnClickListener(this);
        mIvClose.setOnClickListener(this);
        mBtnClose.setOnClickListener(this);
        WebSettings webSettings = mWvResgister.getSettings();
        setZoom(webSettings);
        //定位权限
        webSettings.setDatabaseEnabled(true);
        //设置定位的数据库路径
        String dir = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath();
        webSettings.setGeolocationDatabasePath(dir);
        webSettings.setGeolocationEnabled(true);
        //webSettings.setDomStorageEnabled(true);

        webSettings.setAllowFileAccess(true);
        String ua = webSettings.getUserAgentString();
        LogUtil.d("ua====" + ua);
//        webSettings.setUserAgentString(ua + " hstysales/" + VersionUtils.getVersionCode(this) + " (hstysales_app_android)");
        webSettings.setUserAgentString(ua + ConfigUtil.getUserAgent(this));
        //设置为可调用js方法
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);

        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDefaultTextEncodingName("utf-8");

        webSettings.setJavaScriptEnabled(true);
        mWvResgister.requestFocus();
        mWvResgister.addJavascriptInterface(new JsInteration(), "android");

        setWebChromeClient(mWvResgister);

        String[] cookies = SpUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_COOKIE, "").split("<<<->>>");
        if (cookies != null && cookies.length > 0) {
            for (int i = 0; i < cookies.length; i++) {
                setCookie(url, cookies[i]);
            }
        }
        mWvResgister.loadUrl(url);

        mWvResgister.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //mWvResgister.loadUrl(url);
                mIvClose.setVisibility(isAllowClose ? View.VISIBLE : View.INVISIBLE);
                LogUtil.d("RegisterActivity url=" + url);
                RegisterActivity.this.url = url;
                if (url.contains("WebH5/register_salesman/success.html")) {
                    mIvBack.setVisibility(View.INVISIBLE);
                    mBtnClose.setVisibility(View.VISIBLE);
                } else {
                    mIvBack.setVisibility(View.VISIBLE);
                    mBtnClose.setVisibility(View.INVISIBLE);
                }
                return false;
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                mLlWebview.setVisibility(View.GONE);
                mRlNetError.setVisibility(View.VISIBLE);
            }

            //华为mate9保时捷版打不开
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                // super.onReceivedSslError(view, handler, error);
                handler.proceed();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                LogUtil.d("title===", view.getTitle());
                mTvTitlte.setText(view.getTitle());
            }
        });
    }

    public void setWebChromeClient(WebView wvResgister) {
        wvResgister.setWebChromeClient(new WebChromeClient() {


            @Override
            public void onProgressChanged(WebView view, int newProgress) {

                if (newProgress == 100) {
                    mPg.setVisibility(View.GONE);//加载完网页进度条消失
                } else {
                    mPg.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                    mPg.setProgress(newProgress);//设置进度值
                }

            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                LogUtil.d("title===", title);
                mTvTitlte.setText(title);
            }

            @Override
            public boolean onShowFileChooser(WebView webView,
                                             ValueCallback<Uri[]> filePathCallback,
                                             FileChooserParams fileChooserParams) {
                mUploadCallbackAboveL = filePathCallback;
                take();
                return true;
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                mUploadMessage = uploadMsg;
                take();
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
                mUploadMessage = uploadMsg;
                take();
            }

            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                mUploadMessage = uploadMsg;
                take();
            }

            @Override
            public void onReceivedIcon(WebView view, Bitmap icon) {
                super.onReceivedIcon(view, icon);

            }

            @Override
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
                super.onGeolocationPermissionsShowPrompt(origin, callback);
            }

        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE3:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ImageUtil.saveAssetsImage(MyApplication.getContext(), mPhoto+".jpg");;
                } else {
                    showDialog(getString(R.string.permission_set_content_storage));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE5:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    //
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE6:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    //
                } else {
                    showDialog(getString(R.string.permission_set_content_location));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    /*private void showDialog() {
        if (mDialog == null) {
            mDialog = new SelectDialog(this, UIUtils.getString(R.string.dialog_notice_camera)
                    , UIUtils.getString(R.string.btn_setting), UIUtils.getString(R.string.btn_cancel_text),
                    new SelectDialog.HandleBtn() {
                        @Override
                        public void handleOkBtn() {
                            Intent intent = getAppDetailSettingIntent(RegisterActivity.this);
                            startActivity(intent);
                        }

                        @Override
                        public void handleCancleBtn() {
                        }
                    });
        }
        DialogHelper.resize(this, mDialog);
        mDialog.show();
    }*/

    public void setCookie(String url, String stringCookie) {
        CookieSyncManager.createInstance(MyApplication.getContext());
        CookieManager cookieManager = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.removeSessionCookies(null);
            cookieManager.setAcceptThirdPartyCookies(mWvResgister, true);
            cookieManager.flush();
        } else {
            cookieManager.removeSessionCookie();
            cookieManager.setAcceptCookie(true);
            CookieSyncManager.getInstance().sync();
        }
        cookieManager.setAcceptCookie(true);
        cookieManager.setCookie(url, stringCookie);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                WebBackForwardList webBackForwardList = mWvResgister.copyBackForwardList();
                LogUtil.d("RegisterActivity--" + webBackForwardList.getSize());
                if (mWvResgister.canGoBack()) {
                    LogUtil.d("RegisterActivity--" + true);
                    mWvResgister.goBack();
                } else {
                    LogUtil.d("RegisterActivity--" + false);
                    if (Constants.WELCOME_TO_REGISTER.equals(fromUrl)) {
                        int autoStatus = SpUtil.getInt(MyApplication.getContext(), Constants.SP_AUTO_STATUS, 0);
                        if (autoStatus == 2) {
                            startActivity(new Intent(this, MainActivity.class));
                            finish();
                        } else {
                            finish();
                        }
                    } else if (Constants.WELCOME_TO_REGISTER_SECOND.equals(fromUrl)) {
                        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        finish();
                    } else {
                        finish();
                    }
                }
                break;
            case R.id.iv_close:
                finish();
                break;
            case R.id.btn_close:
                finish();
                break;
            default:
                break;
        }
    }

    public class JsInteration {
        @JavascriptInterface
        public void completeRegister() {
            RegisterActivity.this.finish();
        }

        @JavascriptInterface
        public void completeSubmit() {
            RegisterActivity.this.finish();
        }

        /*@JavascriptInterface
        public void requestPermission() {//已废弃
            PermissionUtils.checkPermissionArray(RegisterActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE5);
        }*/

        @JavascriptInterface
        public boolean checkCameraPermission() {
            boolean cameraPermission = PermissionUtils.checkPermissionArray(RegisterActivity.this, permissionArray);
            LogUtil.d("checkCameraPermission===" + cameraPermission);
            return cameraPermission;
        }

        @JavascriptInterface
        public void requestCameraPermission() {
            LogUtil.d("checkCameraPermission===" + 11111);
            boolean cameraPermission = PermissionUtils.checkPermissionArray(RegisterActivity.this, permissionArray);
            if (!cameraPermission) {
                LogUtil.d("checkCameraPermission===" + 222222);
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE5, permissionArray, getString(R.string.permission_content_photo));
            }
        }

        @JavascriptInterface
        public void requestLocation() {//已废弃
            LocationUtil locationUtil = new LocationUtil();
            locationUtil.setOnLocationListener(new LocationUtil.OnLocationListener() {
                @Override
                public void location(double lat, double lng) {

                }
            });
        }

        @JavascriptInterface
        public boolean checkLocationPermission() {
            boolean locationPermission = PermissionUtils.checkPermissionArray(RegisterActivity.this, locationPermissionArray);
            return locationPermission;
        }

        @JavascriptInterface
        public void requestLocationPermission() {
            boolean cameraPermission = PermissionUtils.checkPermissionArray(RegisterActivity.this, locationPermissionArray);
            if (!cameraPermission) {
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE6, locationPermissionArray, getString(R.string.permission_content_location));
            }
        }

        @JavascriptInterface
        public void openVip(String merchantId) {
            Intent intentOpenVip = new Intent();
            intentOpenVip.setClass(RegisterActivity.this, VipGuideActivity.class);
            intentOpenVip.putExtra(Constants.INTENT_MERCHANT_ID, merchantId);
            intentOpenVip.putExtra(Constants.INTENT_NAME, Constants.INTENT_REGISTER_VIP);
            startActivity(intentOpenVip);
        }

        @JavascriptInterface
        public void setCacheInfo(String param) {
            String merchantId = "";
            String data = "";
            try {
                JSONObject jsonObject = new JSONObject(param);
                merchantId = jsonObject.optString("key");
                data = jsonObject.optString("data");
                SpStayUtil.putString(MyApplication.getContext(), Constants.SP_CACHE_INFO + merchantId, data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public String getCacheInfo(String param) {
            String merchantId = "";
            try {
                JSONObject jsonObject = new JSONObject(param);
                merchantId = jsonObject.optString("key");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return SpStayUtil.getString(MyApplication.getContext(), Constants.SP_CACHE_INFO + merchantId, "");
        }

        @JavascriptInterface
        public void cleanCacheInfo(String param) {
            String merchantId = "";
            try {
                JSONObject jsonObject = new JSONObject(param);
                merchantId = jsonObject.optString("key");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            SpStayUtil.removeKey(Constants.SP_CACHE_INFO + merchantId);
        }

        @JavascriptInterface
        public void downloadPhoto(String photo) {
            mPhoto = photo;
            try {
                boolean results = PermissionUtils.checkPermissionArray(RegisterActivity.this, permissionArray);
                if (results) {
                    ImageUtil.saveAssetsImage(MyApplication.getContext(), photo+".jpg");
                } else {
                    showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE3, permissionArray, getString(R.string.permission_content_storage));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mWvResgister.canGoBack()) {
            if (url.contains("WebH5/register_salesman/success.html")) {
                finish();
            } else {
                mWvResgister.goBack();
            }
        } else {
            if (Constants.WELCOME_TO_REGISTER.equals(fromUrl)) {
                int autoStatus = SpUtil.getInt(MyApplication.getContext(), Constants.SP_AUTO_STATUS, 0);
                if (autoStatus == 2) {
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                } else {
                    finish();
                }
            } else if (Constants.WELCOME_TO_REGISTER_SECOND.equals(fromUrl)) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage && null == mUploadCallbackAboveL) return;
            Uri result = data == null || resultCode != RESULT_OK ? null : data.getData();
            if (mUploadCallbackAboveL != null) {
                onActivityResultAboveL(requestCode, resultCode, data);
            } else if (mUploadMessage != null) {

                if (result != null) {
                    String path = getPath(getApplicationContext(),
                            result);
                    Uri uri = Uri.fromFile(new File(path));
                    mUploadMessage
                            .onReceiveValue(uri);
                } else {
                    mUploadMessage.onReceiveValue(imageUri);
                }
                mUploadMessage = null;

            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        LogUtil.i("RegisterActivity", "ThirdActivity onSaveInstanceState");
        outState.putString("url", url);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        LogUtil.i("RegisterActivity", "ThirdActivity onRestoreInstanceState");
        if (savedInstanceState != null) {
            String url = (String) savedInstanceState.getString("url");
        }
    }

    @SuppressWarnings("null")
    @TargetApi(Build.VERSION_CODES.BASE)
    private void onActivityResultAboveL(int requestCode, int resultCode, Intent data) {
        if (requestCode != FILECHOOSER_RESULTCODE
                || mUploadCallbackAboveL == null) {
            return;
        }
        Uri[] results = null;
        if (resultCode == Activity.RESULT_OK) {
            if (data == null) {
                results = new Uri[]{imageUri};
            } else {
                String dataString = data.getDataString();
                ClipData clipData = data.getClipData();

                if (clipData != null) {
                    results = new Uri[clipData.getItemCount()];
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        results[i] = item.getUri();
                    }
                }
                if (dataString != null)
                    results = new Uri[]{Uri.parse(dataString)};
            }
        }
        if (results != null) {
            mUploadCallbackAboveL.onReceiveValue(results);
            mUploadCallbackAboveL = null;
        } else {
            results = new Uri[]{imageUri};
            mUploadCallbackAboveL.onReceiveValue(results);
            mUploadCallbackAboveL = null;
        }

        return;
    }


    private void take() {
        File imageStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "MyApp");
        // Create the storage directory if it does not exist
        if (!imageStorageDir.exists()) {
            imageStorageDir.mkdirs();
        }
        File file = new File(imageStorageDir + File.separator + "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
        imageUri = Uri.fromFile(file);

        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent i = new Intent(captureIntent);
            i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            i.setPackage(packageName);
            i.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            cameraIntents.add(i);
        }
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("image/*");
        Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
        RegisterActivity.this.startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public void setZoom(WebSettings settings) {
        if (AppHelper.getAndroidSDKVersion() == 17) {
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        //各种分辨率适应
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int mDensity = metrics.densityDpi;
        if (mDensity == 120) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        } else if (mDensity == 160) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        } else if (mDensity == 240) {
            settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }
    }
}
