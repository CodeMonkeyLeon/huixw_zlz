package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.InfoBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpStayUtil;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.EditTextWatcher;
import com.hstypay.hstysales.widget.NoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.ShadowLayout;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class ChangeTelActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mTvNotice, mTvGetCode, mTvVoiceButton;
    private ImageView mIvClean;
    private EditText mEtTelephone, mEtCode;
    private ShadowLayout mShadowButton;
    private Button mBtnEnable, mBtnUnable;
    private SafeDialog mLoadDialog;
    private NoticeDialog mNoticeDialog;
    private TimeCount time;

    private static final int GET_MESSAGE_CODE = 1;
    private static final int GET_VOICE_CODE = 2;

    private final View.OnFocusChangeListener listener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case R.id.et_id:
                    if (hasFocus) {
                        if (mEtTelephone.getText().toString().length() > 0) {
                            mIvClean.setVisibility(View.VISIBLE);
                        } else {
                            mIvClean.setVisibility(View.GONE);
                        }
                    } else {
                        mIvClean.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);

        initView();
        initListener();
        initData();
    }

    private void initView() {
        setContentView(R.layout.activity_change_tel);
        StatusBarUtil.setTranslucentStatus(this);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        //mTvTitle.setText(getString(R.string.title_binding_tel));

        mTvNotice = (TextView) findViewById(R.id.tv_change_tel_notice);
        mIvClean = (ImageView) findViewById(R.id.iv_clean_input);
        mEtTelephone = (EditText) findViewById(R.id.et_id);//手机号
        mEtCode = (EditText) findViewById(R.id.et_code);
        mTvGetCode = (TextView) findViewById(R.id.tv_get_code);
        mTvVoiceButton = (TextView) findViewById(R.id.tv_phone_verify);
        mShadowButton = (ShadowLayout) findViewById(R.id.shadow_button);
        mBtnEnable = (Button) findViewById(R.id.btn_common_enable);
        mBtnUnable = (Button) findViewById(R.id.btn_common_unable);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mIvClean.setOnClickListener(this);
        mBtnEnable.setOnClickListener(this);
        mTvGetCode.setOnClickListener(this);
        mTvVoiceButton.setOnClickListener(this);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (mEtTelephone.isFocused()) {
                    if (mEtTelephone.getText().toString().length() > 0) {
                        mIvClean.setVisibility(View.VISIBLE);
                    } else {
                        mIvClean.setVisibility(View.GONE);
                    }
                }

                if (!StringUtils.isEmptyOrNull(mEtTelephone.getText().toString())
                        && !StringUtils.isEmptyOrNull(mEtCode.getText().toString())) {
                    mShadowButton.setVisibility(View.VISIBLE);
                    mBtnUnable.setVisibility(View.GONE);
                } else {
                    mShadowButton.setVisibility(View.GONE);
                    mBtnUnable.setVisibility(View.VISIBLE);
                }

            }
        });
        mEtCode.addTextChangedListener(editTextWatcher);
        mEtTelephone.addTextChangedListener(editTextWatcher);
        mEtCode.setOnFocusChangeListener(listener);
        mEtTelephone.setOnFocusChangeListener(listener);
    }

    private void initData(){
        String loginTelephone = SpUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_TELEPHONE);
        if (TextUtils.isEmpty(loginTelephone)){
            mTvNotice.setText(UIUtils.getString(R.string.binding_tel_notice));
            mTvTitle.setText(UIUtils.getString(R.string.setting_binding_tel));
        }else{
            mTvNotice.setText(UIUtils.getString(R.string.change_tel_notice)+loginTelephone);
            mTvTitle.setText(UIUtils.getString(R.string.setting_change_tel));
        }
    }

    //Eventbus接收数据,验证手机号
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_BINDING_MSG)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_BINDING_MSG_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_BINDING_MSG_TRUE://验证码返回成功
                    loadGetCode();
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GET_BINDING_VOICE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_BINDING_VOICE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_BINDING_VOICE_TRUE:
                    showDialog();
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CHANGE_BINGING)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_LOGIN_NAME, mEtTelephone.getText().toString().trim());
                    NoticeDialog bindingSuccess = new NoticeDialog(ChangeTelActivity.this, null, null, R.layout.notice_dialog_binding);
                    bindingSuccess.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                        @Override
                        public void clickOk() {
                            StatService.trackCustomKVEvent(ChangeTelActivity.this, "1058", null);
                            SpStayUtil.putString(MyApplication.getContext(),Constants.SP_LOGIN_TELEPHONE,mEtTelephone.getText().toString().trim());
                            startActivity(new Intent(ChangeTelActivity.this,LoginActivity.class));
                            finish();
                        }
                    });
                    bindingSuccess.show();
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GET_BINDING)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_BINDING_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_BINDING_TRUE:
                    SpUtil.putString(MyApplication.getContext(), Constants.SP_LOGIN_TELEPHONE, mEtTelephone.getText().toString().trim());
                    NoticeDialog bindingSuccess = new NoticeDialog(ChangeTelActivity.this, null, null, R.layout.notice_dialog_binding);
                    bindingSuccess.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                        @Override
                        public void clickOk() {
                            StatService.trackCustomKVEvent(ChangeTelActivity.this, "1058", null);
                            finish();
                        }
                    });
                    bindingSuccess.show();
                    break;
            }
        }
    }

    private void showDialog() {
        if (mNoticeDialog == null) {
            mNoticeDialog = new NoticeDialog(ChangeTelActivity.this, null, null, R.layout.notice_dialog_voice);
            mNoticeDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    mNoticeDialog.dismiss();
                }
            });
        }
        mNoticeDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mNoticeDialog != null) {
            mNoticeDialog.dismiss();
            mNoticeDialog = null;
        }
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_clean_input:
                mEtTelephone.setText("");
                mIvClean.setVisibility(View.GONE);
                break;
            case R.id.tv_get_code:
                //获取验证码
                getCode(GET_MESSAGE_CODE);
                break;
            //下一步
            case R.id.btn_common_enable:
                StatService.trackCustomKVEvent(ChangeTelActivity.this, "1057", null);
                bindingTel();
                break;
            case R.id.tv_phone_verify:
                getCode(GET_VOICE_CODE);
                break;
        }
    }

    private void bindingTel() {
        if (!StringUtils.isEmptyOrNull(mEtTelephone.getText().toString())) {
            if (mEtTelephone.getText().toString().length() != 11) {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_tel_length));
                return;
            }
        }
        if (!StringUtils.isEmptyOrNull(mEtCode.getText().toString())) {
            if (mEtCode.getText().toString().length() != 6) {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_code_length));
                return;
            }
        }

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("phone", mEtTelephone.getText().toString());
            map.put("code", mEtCode.getText().toString().trim());
            String loginPhone = SpUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_TELEPHONE);
            if (TextUtils.isEmpty(loginPhone)) {
                ServerClient.newInstance(MyApplication.getContext()).bindingTel(MyApplication.getContext(), Constants.TAG_GET_BINDING, map);
            }else {
                String loginPwd = getIntent().getStringExtra(Constants.INTENT_PASSWORD);
                map.put("password", loginPwd);
                ServerClient.newInstance(MyApplication.getContext()).changeBindingTel(MyApplication.getContext(), Constants.TAG_CHANGE_BINGING, map);
            }
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }


    }

    private void getCode(int type) {
        if (!StringUtils.isEmptyOrNull(mEtTelephone.getText().toString())) {
            if (mEtTelephone.getText().toString().length() != 11) {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_tel_length));
                return;
            }
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_tel_empty));
            return;
        }

        if (type == GET_MESSAGE_CODE) {
            getMessageCode();
        } else if (type == GET_VOICE_CODE) {
            getVoiceCode();
        }

    }

    private void getMessageCode() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("phone", mEtTelephone.getText().toString().trim());
            ServerClient.newInstance(MyApplication.getContext()).getBindingMsg(MyApplication.getContext(), Constants.TAG_GET_BINDING_MSG, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getVoiceCode() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("phone", mEtTelephone.getText().toString().trim());
            ServerClient.newInstance(MyApplication.getContext()).getBindingVoice(MyApplication.getContext(), Constants.TAG_GET_BINDING_VOICE, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void loadGetCode() {
        Countdown();
    }

    /**
     * 验证码倒计时60秒
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void Countdown() {
        // 总共60 没间隔1秒
        time = new TimeCount(60000, 1000);
        time.start();

    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发
            mTvGetCode.setEnabled(true);
            mTvGetCode.setClickable(true);
            mTvGetCode.setText(R.string.reacquire_verify_code);
            mTvGetCode.setTextColor(getResources().getColor(R.color.rb_rank_days_text));
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程显示
            mTvGetCode.setEnabled(false);
            mTvGetCode.setClickable(false);
            mTvGetCode.setTextColor(getResources().getColor(R.color.user_edit_color));
            mTvGetCode.setText(getString(R.string.verify_code_count_down) + millisUntilFinished / 1000 + "s");
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }
}
