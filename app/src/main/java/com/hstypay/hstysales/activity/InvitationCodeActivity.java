package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.adapter.InvitationCodeAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.CodeDetailBean;
import com.hstypay.hstysales.bean.CodeUrlBean;
import com.hstypay.hstysales.bean.InvitationCodeBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CustomLinearLayoutManager;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class InvitationCodeActivity extends BaseActivity implements View.OnClickListener {
    private RecyclerView mRecyclerview;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private InvitationCodeAdapter mInvitationCodeAdapter;
    private ImageView mIvBack, mIvClean;
    private TextView mTvTitle;
    private SafeDialog mLoadDialog;

    private int pageSize = 15;
    private int currentPage = 1;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<InvitationCodeBean.DataEntity.ItemData> mListData = new ArrayList<>();
    private RelativeLayout mLayoutEmpty;
    private EditText mEtSearch;
    private int clickPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        setContentView(R.layout.activity_invitation_code);
        StatusBarUtil.setTranslucentStatus(this);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvClean = (ImageView) findViewById(R.id.iv_clean);
        mEtSearch = (EditText) findViewById(R.id.et_search);
        mTvTitle.setText(getString(R.string.title_invitation_code));
        mLayoutEmpty = (RelativeLayout) findViewById(R.id.layout_data_empty);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);

        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initRecyclerView() {
        mRecyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerview.setLayoutManager(mLinearLayoutManager);
        mRecyclerview.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setRefreshEnable(false);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    currentPage = 1;
                    getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mIvClean.setOnClickListener(this);

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mIvClean.setVisibility(View.VISIBLE);
                } else {
                    mIvClean.setVisibility(View.INVISIBLE);
                }
            }
        });
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    StatService.trackCustomKVEvent(InvitationCodeActivity.this, "1044", null);
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        currentPage = 1;
                        isRefreshed = true;
                        DialogUtil.safeShowDialog(mLoadDialog);
                        getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                        mSwipeRefreshLayout.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mSwipeRefreshLayout.finishRefresh();
                            }
                        }, 500);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void initData() {
        mInvitationCodeAdapter = new InvitationCodeAdapter(InvitationCodeActivity.this, mListData);
        mInvitationCodeAdapter.setOnShareClickListener(new InvitationCodeAdapter.OnShareClickListener() {
            @Override
            public void skipToshare(String invitationCode) {
                StatService.trackCustomKVEvent(InvitationCodeActivity.this, "1046", null);
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    Map<String, Object> map = new HashMap<>();
                    map.put("invitationCode", invitationCode);
                    ServerClient.newInstance(MyApplication.getContext()).getCodeUrl(MyApplication.getContext(), Constants.TAG_CODE_URL, map);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                }
            }
        });
        mInvitationCodeAdapter.setOnDetailGetListener(new InvitationCodeAdapter.OnDetailGetListener() {
            @Override
            public void showDetail(int position) {
                //clickPosition = position;
                if (mListData.get(position).isOpen()) {
                    mListData.get(position).setOpen(false);
                    mInvitationCodeAdapter.notifyDataSetChanged();
                } else {
                    StatService.trackCustomKVEvent(InvitationCodeActivity.this, "1045", null);
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        if (clickPosition!=-1){
                            mListData.get(clickPosition).setOpen(false);
                        }
                        clickPosition = position;
                        mListData.get(position).setOpen(true);
                        DialogUtil.safeShowDialog(mLoadDialog);
                        Map<String, Object> map = new HashMap<>();
                        map.put("invitationCode", mListData.get(position).getInvitationCode());
                        ServerClient.newInstance(MyApplication.getContext()).getCodeDetail(MyApplication.getContext(), Constants.TAG_CODE_DETAIL, map);
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    }
                }
            }
        });
        mRecyclerview.setAdapter(mInvitationCodeAdapter);

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            currentPage = 1;
            isRefreshed = true;
            getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getData(int pageSize, int currentPage, String search) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            if (!TextUtils.isEmpty(search)) {
                map.put("search", search);
            }
            ServerClient.newInstance(MyApplication.getContext()).getInvitationcode(MyApplication.getContext(), Constants.TAG_GET_INVITATIONCODE, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_clean:
                mEtSearch.setText("");
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshed = true;
                    currentPage = 1;
                    DialogUtil.safeShowDialog(mLoadDialog);
                    getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
                break;
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_INVITATIONCODE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InvitationCodeBean msg = (InvitationCodeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_INVITATIONCODE_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(InvitationCodeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_INVITATIONCODE_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg != null && msg.getData() != null) {
                        if (msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                            mLayoutEmpty.setVisibility(View.GONE);
                            mRecyclerview.setVisibility(View.VISIBLE);
                            mListData.addAll(msg.getData().getData());
                            mInvitationCodeAdapter.notifyDataSetChanged();
                        } else {
                            if (isLoadmore) {
                                mLayoutEmpty.setVisibility(View.GONE);
                                mRecyclerview.setVisibility(View.VISIBLE);
                                ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                            } else {
                                mLayoutEmpty.setVisibility(View.VISIBLE);
                                mRecyclerview.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        if (isLoadmore) {
                            mLayoutEmpty.setVisibility(View.GONE);
                            mRecyclerview.setVisibility(View.VISIBLE);
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mLayoutEmpty.setVisibility(View.VISIBLE);
                            mRecyclerview.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        }
        if (event.getTag().equals(Constants.TAG_CODE_DETAIL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CodeDetailBean msg = (CodeDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.CODE_DETAIL_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.CODE_DETAIL_TRUE:
                    if (msg != null && msg.getData() != null) {
                        //mOnDetailGetListener.showDetail(msg.getData());
                        mListData.get(clickPosition).setMerchantCount(msg.getData().getMerchantCount() + "");
                        mInvitationCodeAdapter.notifyDataSetChanged();
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CODE_URL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CodeUrlBean msg = (CodeUrlBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.CODE_URL_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.CODE_URL_TRUE:
                    if (msg != null && msg.getData() != null) {
                        Intent intent = new Intent(InvitationCodeActivity.this, ShareActivity.class);
                        intent.putExtra(Constants.INTENT_CODE_URL, msg.getData().getUrl());
                        intent.putExtra(Constants.INTENT_CODE_TIMEOUT, msg.getData().getTimeout());
                        startActivity(intent);
                    }
                    break;
            }
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mListData.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }
}
