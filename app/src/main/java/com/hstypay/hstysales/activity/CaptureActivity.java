package com.hstypay.hstysales.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.Zxing.Camera.CameraManager;
import com.hstypay.hstysales.Zxing.Decoding.CaptureActivityHandler;
import com.hstypay.hstysales.Zxing.Decoding.InactivityTimer;
import com.hstypay.hstysales.Zxing.view.ViewfinderView;
import com.hstypay.hstysales.adapter.MerchantRecyclerAdapter;
import com.hstypay.hstysales.adapter.StoreRecyclerAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.BindCodeBean;
import com.hstypay.hstysales.bean.DeviceInfoSn;
import com.hstypay.hstysales.bean.Info;
import com.hstypay.hstysales.bean.MerchantListBean;
import com.hstypay.hstysales.bean.QrcodeBean;
import com.hstypay.hstysales.bean.StoreListBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.BindDialog;
import com.hstypay.hstysales.widget.CommonNoticeDialog;
import com.hstypay.hstysales.widget.CustomLinearLayoutManager;
import com.hstypay.hstysales.widget.CustomViewFullScreenDialog;
import com.hstypay.hstysales.widget.EditTextDelete;
import com.hstypay.hstysales.widget.NoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.SelectDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;


public class CaptureActivity extends BaseActivity implements Callback, View.OnClickListener {
    private CaptureActivityHandler handler;

    private ViewfinderView viewfinderView;
    private boolean hasSurface;
    private Vector<BarcodeFormat> decodeFormats;
    private String characterSet;
    private boolean mNeedFlashLightOpen = true;
    private InactivityTimer inactivityTimer;
    private Context mContext;
    private LinearLayout ly_back;
    private TextView tv_input_code;
    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;
    private LinearLayout ly_open_light;
    private ImageView iv_light;
    private TextView tv_money, tv_title;
    private String money;
    private double doubleMoney;
    private String mStore_id;
    private String intentName;
    private TextView mTvScanNotice;
    private String storeName;

    private MediaPlayer mediaPlayer;
    private static final float BEEP_VOLUME = 0.10f;
    private boolean playBeep;
    private boolean vibrate;
    private SelectDialog mDialog;
    private NoticeDialog mNoticeDialog;
    private SafeDialog mBindingDialog, mLoadDialog;
    private TextView mTvInputSn;

    private RelativeLayout mRlSelectMerchant;
    private RelativeLayout mRlSelectStore;
    private RelativeLayout mRlMerchantInput;
    private EditTextDelete mEtMerchantInput;
    private SHSwipeRefreshLayout mSwipeRefreshLayoutMerchant;
    private RecyclerView mRecyclerViewMerchant;
    private TextView mTvMerchantNull;
    private List<MerchantListBean.DataEntity.ItemData> mMerchantList;//商户列表数据
    private String mCurSelectMerchantId = "";//当前选中的商户ID
    private String mCurSelectMerchantName;//当前选中的商户名称
    private MerchantRecyclerAdapter mMerchantRecyclerAdapter;//商户列表的adapter
    private PopupWindow mMerchantsPopupWindow;
    private TextView mEtShowMerchant;
    private TextView mEtShowStore;


    private PopupWindow mStoresPopupWindow;
    private RelativeLayout mRlStoreInput;
    private EditTextDelete mEtStoreInput;
    private SHSwipeRefreshLayout mSwipeRefreshLayoutStore;
    private RecyclerView mStoreRecyclerView;
    private TextView mTvShopNull;
    private List<StoreListBean.DataEntity> mStoreList;//门店列表数据
    private StoreRecyclerAdapter mShopRecyclerAdapter;
    private String mCurSelectStoreId = "";//当前选中的门店ID
    private String mCurSelectStoreName;

    private String mCurSelectMerchantIdIntent;
    private String mCurSelectMerchantNameIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        mContext = this;
        doubleMoney = getIntent().getDoubleExtra("moneys", 0);//金额
        LogUtil.d("money--", doubleMoney * 100 + "");
        money = String.valueOf(BigDecimal.valueOf(doubleMoney).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP));
        LogUtil.d("money", money);
        //门店Id
        mStore_id = getIntent().getStringExtra("store_id");
        CameraManager.init(this);
        viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
        hasSurface = false;
        inactivityTimer = new InactivityTimer(this);
        intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        storeName = getIntent().getStringExtra(Constants.INTENT_BIND_STORE_NAME);
        mBindingDialog = getLoadDialog(this, UIUtils.getString(R.string.bind_loading), false);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        initView();
        initData();
        setLister();

        if (!isCameraUseable()) {
            showDialog();
        }
    }

    private void initData() {
        mCurSelectMerchantIdIntent = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        mCurSelectMerchantNameIntent = getIntent().getStringExtra(Constants.INTENT_MERCHANT_NAME);
        if (!TextUtils.isEmpty(mCurSelectMerchantIdIntent)) {
            mCurSelectMerchantId = mCurSelectMerchantIdIntent;
        }
        if (!TextUtils.isEmpty(mCurSelectMerchantNameIntent)) {
            mCurSelectMerchantName = mCurSelectMerchantNameIntent;
        }

    }

    private void showDialog() {
        if (mDialog == null) {
            mDialog = new SelectDialog(this, UIUtils.getString(R.string.dialog_notice_camera)
                    , UIUtils.getString(R.string.btn_setting), UIUtils.getString(R.string.btn_cancel_text),
                    new SelectDialog.HandleBtn() {
                        @Override
                        public void handleOkBtn() {
                            Intent intent = getAppDetailSettingIntent(CaptureActivity.this);
                            startActivity(intent);
                        }

                        @Override
                        public void handleCancleBtn() {
                        }
                    });
        }
        DialogHelper.resize(this, mDialog);
        mDialog.show();
    }


    public static boolean isCameraUseable() {
        boolean canUse = true;
        Camera mCamera = null;
        try {
            mCamera = Camera.open();
            // setParameters 是针对魅族MX5。MX5通过Camera.open()拿到的Camera对象不为null
            Camera.Parameters mParameters = mCamera.getParameters();
            mCamera.setParameters(mParameters);
        } catch (Exception e) {
            canUse = false;
        }
        if (mCamera != null) {
            mCamera.release();
        }
        return canUse;
    }


    private void setLister() {
        ly_back.setOnClickListener(this);
        ly_open_light.setOnClickListener(this);
        mTvInputSn.setOnClickListener(this);
    }

    private void initView() {
        ly_back = (LinearLayout) findViewById(R.id.ly_back);
        ly_open_light = (LinearLayout) findViewById(R.id.ly_open_light);
        iv_light = (ImageView) findViewById(R.id.iv_light);
        tv_money = (TextView) findViewById(R.id.tv_money);
        tv_title = (TextView) findViewById(R.id.tv_title);

        mTvScanNotice = (TextView) findViewById(R.id.tv_code_info);
        mTvInputSn = findViewById(R.id.tv_input_sn);
        if (Constants.INTENT_BIND_CODE.equals(intentName)) {
            tv_title.setText("绑定收款码");
            tv_money.setVisibility(View.INVISIBLE);
            mTvScanNotice.setText("扫码可对" + storeName + "进行绑定");
        } else if (Constants.INTENT_BIND_DEVICE.equals(intentName)) {
            tv_title.setText("扫一扫");
            tv_money.setVisibility(View.INVISIBLE);
            mTvScanNotice.setText("扫码设备SN码绑定设备");
            mTvInputSn.setVisibility(View.VISIBLE);
        }
    }


    @SuppressWarnings("unchecked")
    @Override
    protected void onResume() {
        super.onResume();
        surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        surfaceHolder = surfaceView.getHolder();

        if (hasSurface) {
            initCamera(surfaceHolder, false);
        } else {
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
        decodeFormats = null;
        characterSet = null;

        playBeep = true;
        AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
        if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
            playBeep = false;
        }
        initBeepSound();
        vibrate = true;

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        CameraManager.get().closeDriver();
        if (!hasSurface) {
            SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
        }
    }

    void closeCamera() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
    }

    @Override
    protected void onDestroy() {
        inactivityTimer.shutdown();
        super.onDestroy();
    }

    void restartCamera() {
        closeCamera();
        viewfinderView.setVisibility(View.VISIBLE);
        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        initCamera(surfaceHolder, false);
        // 恢复活动监控器
        //        inactivityTimer.onResume();
    }

    private void initCamera(SurfaceHolder surfaceHolder, boolean isFirst) {
        try {
            CameraManager.get().openDriver(surfaceHolder, surfaceView);
        } catch (IOException ioe) {
            return;
        } catch (RuntimeException e) {
            return;
        }
        if (handler == null) {
            handler = new CaptureActivityHandler(this, decodeFormats, characterSet);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder, false);
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        hasSurface = false;

    }

    public ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return handler;
    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }


    private void initBeepSound() {
        if (playBeep && mediaPlayer == null) {
            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnCompletionListener(beepListener);

            AssetFileDescriptor file = getResources().openRawResourceFd(R.raw.beep);
            try {
                mediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                file.close();
                mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
                mediaPlayer.prepare();
            } catch (IOException e) {
                mediaPlayer = null;
            } finally {
                if (file != null) {
                    try {
                        file.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    private final MediaPlayer.OnCompletionListener beepListener = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    };


    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    public void submitData(final String code, boolean vibration) {
        if (Constants.INTENT_BIND_CODE.equals(intentName)) {
            String[] split = code.split("&");
            String qrcodeId = "";
            if (split != null && split.length > 0) {
                for (int i = 0; i < split.length; i++) {
                    if (split[i].contains("qrcode=")) {
                        String[] mchIds = split[i].split("qrcode=");
                        for (int i1 = 0; i1 < mchIds.length; i1++) {
                            LogUtil.d("qrcode", mchIds[i1].toString() + i + "--" + i1);
                            qrcodeId = mchIds[1];
                        }
                    }
                }
            }

            if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                return;
            } else {
                Map<String, Object> map = new HashMap();
//                    map.put("qrcode", qrcodeId);
                if (TextUtils.isEmpty(qrcodeId)) {
                    map.put("qrurl", code);
                } else {
                    map.put("qrcode", qrcodeId);
                }
                ServerClient.newInstance(CaptureActivity.this).checkQrcode(CaptureActivity.this, Constants.TAG_CHECK_QRCODE, map);
            }

        } else if (Constants.INTENT_BIND_DEVICE.equals(intentName)) {
            //绑定设备
            queryDeviceInfoBySn(code);
        }
    }

    //Eventbus接收数据，绑定二维码
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBindCode(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_BIND_CODE)) {
            DialogUtil.safeCloseDialog(mBindingDialog);
            BindCodeBean msg = (BindCodeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    getDialog(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    //绑定成功
                    ToastUtil.showToastShort(UIUtils.getString(R.string.dialog_bind_code_success));
                    startActivity(new Intent(this, MerchantInfoActivity.class));
                    finish();
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CHECK_QRCODE)) {
            QrcodeBean msg = (QrcodeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    getDialog(getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        switch (msg.getData().getUseStatus()) {
                            case 0://未绑定
                                showBindDialog(this, storeName, msg.getData().getQrcode(), msg.getData().getId());
                                break;
                            case 1://已绑定
                                getDialog("绑定失败,该二维码已被绑定过");
                                break;
                            case 2://已失效
                                getDialog("绑定失败,该二维码已失效");
                                break;
                            case 3://禁用
                                getDialog("绑定失败,该二维码已被禁用");
                                break;

                        }
                    }
                    break;
            }
        }
        //根据sn查询设备信息
        if (event.getTag().equals(Constants.TAG_QUERY_DEVICE_SN)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            DeviceInfoSn msg = (DeviceInfoSn) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    getDialog(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    DeviceInfoSn.DeviceInfoData deviceInfoData = msg.getData();
                    if (deviceInfoData != null) {
                        String status = deviceInfoData.getStatus();
                        if ("1".equals(status)) {
                            getDialog("此设备已绑定，请勿重复操作");
                        } else {
                            String deviceId = deviceInfoData.getDeviceId();
                            if (!TextUtils.isEmpty(deviceId)) {
                                //弹窗选商户和门店后绑定设备
                                showSelectMerchantStoreDialog(deviceInfoData);
                            } else {
                                getDialog("找不到此设备");
                            }
                        }
                    } else {
                        getDialog("找不到此设备");
                    }
                    break;
            }
        }
        //绑定设备
        if (event.getTag().equals(Constants.TAG_BIND_DEVICE_SN)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    getDialog(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    getDialog(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    showCommonNoticeDialog(CaptureActivity.this, "", "绑定设备成功", new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            setResult(RESULT_OK);
                            finish();
                        }
                    });
                    break;
            }
        }
    }

    /**
     * 选商户和门店后绑定设备的弹窗
     * 1.从商户详情进来的不展示选商户，其它必须选商户后再视情况看是否需要选门店
     * 2. bindSet是2 必须选门店，其它可不选门店
     * bindSet 1 : 绑定到商户/门店 2: 必须绑定到门店级
     *
     * @param deviceInfoData
     */
    private void showSelectMerchantStoreDialog(DeviceInfoSn.DeviceInfoData deviceInfoData) {
        CustomViewFullScreenDialog selectStoreDialog = new CustomViewFullScreenDialog(this);
        selectStoreDialog.setView(R.layout.dialog_select_merchant_store_bind);
        selectStoreDialog.show();
        closeCamera();
        mRlSelectMerchant = selectStoreDialog.findViewById(R.id.rl_select_merchant);
        mRlSelectStore = selectStoreDialog.findViewById(R.id.rl_select_store);
        mEtShowMerchant = selectStoreDialog.findViewById(R.id.et_show_merchant);
        mEtShowStore = selectStoreDialog.findViewById(R.id.et_show_store);
        TextView tv_cancel_dialog = selectStoreDialog.findViewById(R.id.tv_cancel_dialog);
        TextView tv_sure_dialog = selectStoreDialog.findViewById(R.id.tv_sure_dialog);
        tv_cancel_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectStoreDialog.dismiss();
                restartCamera();
            }
        });
        tv_sure_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(mCurSelectMerchantId)) {
                    ToastUtil.showToastShort(getResources().getString(R.string.select_merchant));
                    return;
                }
                if (deviceInfoData != null && "2".equals(deviceInfoData.getBindSet()) && TextUtils.isEmpty(mCurSelectStoreId)) {
                    ToastUtil.showToastShort(getResources().getString(R.string.select_store));
                    return;
                }
                bindDeviceSn(deviceInfoData);
                selectStoreDialog.dismiss();
                restartCamera();
            }
        });
        mRlSelectMerchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //先获取商户列表，再展示列表弹窗
                initMerchantPopupWindow();
                mMerchantCurrentPage = 1;
                getMerchantList();
            }
        });
        mRlSelectStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(mCurSelectMerchantId)) {
                    ToastUtil.showToastShort(getResources().getString(R.string.select_merchant));
                    return;
                }
                //先获取门店列表，再展示列表弹窗选择门店
                initStorePopupWindow();
                mStoreCurrentPage = 1;
                getStoreList();
            }
        });

        if (!TextUtils.isEmpty(mCurSelectMerchantIdIntent)) {
            //是从商户详情跳过来的，则不用选商户
            mRlSelectMerchant.setVisibility(View.GONE);
        }
    }

    //绑定设备
    private void bindDeviceSn(DeviceInfoSn.DeviceInfoData deviceInfoData) {
        if (deviceInfoData == null) {
            return;
        }
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", mCurSelectMerchantId);
            map.put("storeMerchantId", mCurSelectStoreId);
            map.put("sn", deviceInfoData.getSn());
            map.put("deviceId", deviceInfoData.getDeviceId());
            ServerClient.newInstance(MyApplication.getContext()).bindDevice(MyApplication.getContext(), Constants.TAG_BIND_DEVICE_SN, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    //初始化门店列表弹窗
    private void initStorePopupWindow() {
        View view_store_list = LayoutInflater.from(this).inflate(R.layout.layout_store_list, null, false);
        mStoresPopupWindow = new PopupWindow(view_store_list, mRlSelectStore.getWidth(), DensityUtils.dip2px(this, 160));
        mStoresPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mStoresPopupWindow.setOutsideTouchable(true);
        mRlStoreInput = view_store_list.findViewById(R.id.rl_store_input);
        mEtStoreInput = view_store_list.findViewById(R.id.et_store_input);
        mSwipeRefreshLayoutStore = view_store_list.findViewById(R.id.swipeRefreshLayout_store);
        mStoreRecyclerView = view_store_list.findViewById(R.id.recyclerView_store);
        mTvShopNull = view_store_list.findViewById(R.id.tv_shop_null);
        mEtStoreInput.setText("");
        mStoreRecyclerView.setLayoutManager(new CustomLinearLayoutManager(this));
        mSwipeRefreshLayoutStore.setRefreshEnable(false);
        mSwipeRefreshLayoutStore.setLoadmoreEnable(true);
        mSwipeRefreshLayoutStore.setRefreshEnable(false);
        mSwipeRefreshLayoutStore.setLoadmoreEnable(true);
        mSwipeRefreshLayoutStore.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadStoreMore = true;
                    getStoreList();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayoutStore.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayoutStore.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutStore.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutStore.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayoutStore.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutStore.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutStore.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayoutStore.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });

        mEtStoreInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //从门店列表里搜索门店
                    String searchKey = mEtStoreInput.getText().toString().trim();
                    mStoreCurrentPage = 1;
                    getStoreList();
                    return true;
                }
                return false;
            }
        });
        mEtStoreInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (isClear) {//mEtStoreInput.setText("")会触发onEditChanged
                        mStoreCurrentPage = 1;
                        getStoreList();
                    }
                }
            }
        });

        mStoreList = new ArrayList<>();
        mShopRecyclerAdapter = new StoreRecyclerAdapter(this);
        mStoreRecyclerView.setAdapter(mShopRecyclerAdapter);
        mShopRecyclerAdapter.setOnItemClickListener(new StoreRecyclerAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mCurSelectStoreId = mStoreList.get(position).getStoreId();
                mCurSelectStoreName = mStoreList.get(position).getStoreName().trim();
                mEtShowStore.setText(mCurSelectStoreName);
                mStoresPopupWindow.dismiss();
            }
        });
    }

    //获取门店列表
    private static final int mStorePageSize = 15;
    private int mStoreCurrentPage = 1;
    private boolean isLoadStoreMore;//加载更多标识

    private void getStoreList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!isLoadStoreMore) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mStorePageSize);
            map.put("currentPage", mStoreCurrentPage);
            if (!TextUtils.isEmpty(mCurSelectMerchantId)) {
                map.put("merchantId", mCurSelectMerchantId);
            }
            if (!TextUtils.isEmpty(mEtStoreInput.getText().toString().trim())) {
                map.put("storeName", mEtStoreInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_DEVICE_STORE + getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(getResources().getString(R.string.net_error));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetStoreList(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_DEVICE_STORE + getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            setSwipeRefreshState(mSwipeRefreshLayoutStore, isLoadStoreMore, 500);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getResources().getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        if (mStoreCurrentPage == 1) {
                            mStoreList.clear();
                        }
                        mStoreList.addAll(msg.getData());
                        mShopRecyclerAdapter.setData(mStoreList,mCurSelectStoreId);
                        mStoreCurrentPage++;
                        mRlStoreInput.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayoutStore.setVisibility(View.VISIBLE);
                        mTvShopNull.setVisibility(View.GONE);

                    } else {
                        if (isLoadStoreMore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mStoreList.clear();
                            mShopRecyclerAdapter.setData(mStoreList,mCurSelectStoreId);
                            mRlStoreInput.setVisibility(View.GONE);
                            mSwipeRefreshLayoutStore.setVisibility(View.GONE);
                            mTvShopNull.setVisibility(View.VISIBLE);
                        }
                    }
                    if (mStoresPopupWindow != null && !mStoresPopupWindow.isShowing()) {
                        mStoresPopupWindow.showAsDropDown(mRlSelectStore);
                    }
                    break;
            }
            isLoadStoreMore = false;
        }
    }


    //初始化商户列表弹窗
    private void initMerchantPopupWindow() {
        View view_merchant_list = LayoutInflater.from(this).inflate(R.layout.layout_merchant_list, null, false);
        mMerchantsPopupWindow = new PopupWindow(view_merchant_list, mRlSelectMerchant.getWidth(), DensityUtils.dip2px(this, 200));
        mMerchantsPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mMerchantsPopupWindow.setOutsideTouchable(true);
        mRlMerchantInput = view_merchant_list.findViewById(R.id.rl_merchant_input);
        mEtMerchantInput = view_merchant_list.findViewById(R.id.et_merchant_input);
        mSwipeRefreshLayoutMerchant = view_merchant_list.findViewById(R.id.swipeRefreshLayout_merchant);
        mRecyclerViewMerchant = view_merchant_list.findViewById(R.id.recyclerView_merchant);
        mTvMerchantNull = view_merchant_list.findViewById(R.id.tv_merchant_null);

        mEtMerchantInput.setText("");
        mRecyclerViewMerchant.setLayoutManager(new CustomLinearLayoutManager(this));
        mSwipeRefreshLayoutMerchant.setRefreshEnable(false);
        mSwipeRefreshLayoutMerchant.setLoadmoreEnable(true);
        mSwipeRefreshLayoutMerchant.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadMerchantMore = true;
                    getMerchantList();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayoutMerchant.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayoutMerchant.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutMerchant.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutMerchant.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayoutMerchant.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutMerchant.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutMerchant.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayoutMerchant.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });
        mEtMerchantInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //从商户列表里搜索商户
                    mMerchantCurrentPage = 1;
                    getMerchantList();
                    return true;
                }
                return false;
            }
        });
        mEtMerchantInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {//mEtMerchantInput.setText("")会触发onEditChanged
                    mMerchantCurrentPage = 1;
                    getMerchantList();
                }
            }
        });

        mMerchantList = new ArrayList<>();
        mMerchantRecyclerAdapter = new MerchantRecyclerAdapter(this);
        mRecyclerViewMerchant.setAdapter(mMerchantRecyclerAdapter);
        mMerchantRecyclerAdapter.setOnItemClickListener(new MerchantRecyclerAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                MerchantListBean.DataEntity.ItemData selectMerchant = mMerchantList.get(position);//选中的商户
                if (!TextUtils.isEmpty(mCurSelectMerchantId) && !mCurSelectMerchantId.equals(selectMerchant.getMerchantId())) {
                    //需重新选门店
                    mEtShowStore.setText("");
                    mCurSelectStoreId = "";
                }
                mCurSelectMerchantId = selectMerchant.getMerchantId();
                mCurSelectMerchantName = selectMerchant.getMerchantName().trim();
                mEtShowMerchant.setText(mCurSelectMerchantName);
                mMerchantsPopupWindow.dismiss();
            }
        });
    }

    //获取商户列表
    private static final int mMerchantPageSize = 15;
    private int mMerchantCurrentPage = 1;
    private boolean isLoadMerchantMore;//加载更多标识

    private void getMerchantList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!isLoadMerchantMore) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mMerchantPageSize);
            map.put("currentPage", mMerchantCurrentPage);
            map.put("mchQueryStatus", "100");
            if (!TextUtils.isEmpty(mEtMerchantInput.getText().toString().trim())) {
                map.put("search", mEtMerchantInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getMerchantList2(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_LIST_MX_SEARCH + "" + getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetMerchantList(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MERCHANT_LIST_MX_SEARCH + "" + getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            setSwipeRefreshState(mSwipeRefreshLayoutMerchant, isLoadMerchantMore, 500);
            MerchantListBean msg = (MerchantListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_MERCHANT_LIST_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(CaptureActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_MERCHANT_LIST_TRUE:
                    if (msg != null && msg.getData() != null) {
                        if (msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                            if (mMerchantCurrentPage == 1) {
                                mMerchantList.clear();
                            }
                            mMerchantList.addAll(msg.getData().getData());
                            mMerchantRecyclerAdapter.setData(mMerchantList, mCurSelectMerchantId);
                            mMerchantCurrentPage++;
                            mRlMerchantInput.setVisibility(View.VISIBLE);
                            mSwipeRefreshLayoutMerchant.setVisibility(View.VISIBLE);
                            mTvMerchantNull.setVisibility(View.GONE);

                        } else {
                            if (isLoadMerchantMore) {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                            } else {
                                mMerchantList.clear();
                                mMerchantRecyclerAdapter.setData(mMerchantList, mCurSelectMerchantId);
                                mRlMerchantInput.setVisibility(View.GONE);
                                mSwipeRefreshLayoutMerchant.setVisibility(View.GONE);
                                mTvMerchantNull.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        if (isLoadMerchantMore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mMerchantList.clear();
                            mMerchantRecyclerAdapter.setData(mMerchantList, mCurSelectMerchantId);
                            mRlMerchantInput.setVisibility(View.GONE);
                            mSwipeRefreshLayoutMerchant.setVisibility(View.GONE);
                            mTvMerchantNull.setVisibility(View.VISIBLE);
                        }
                    }

                    if (mMerchantsPopupWindow != null && !mMerchantsPopupWindow.isShowing()) {
                        mMerchantsPopupWindow.showAsDropDown(mRlSelectMerchant);
                    }
                    break;
            }
            isLoadMerchantMore = false;
        }
    }

    //关闭下拉加载的进度条
    private void setSwipeRefreshState(SHSwipeRefreshLayout shSwipeRefreshLayout, boolean isLoadMore, int delay) {
        if (isLoadMore) {
            shSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    shSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ly_back:
                finish();
                break;
            case R.id.ly_open_light:
                if (mNeedFlashLightOpen) {
                    turnOn();
                } else {
                    turnFlashLightOff();
                }
                break;
            case R.id.tv_input_sn:
                showInputSnDialog();
                break;
        }
    }

    public void showInputSnDialog() {
        CustomViewFullScreenDialog inputSnDialog = new CustomViewFullScreenDialog(this);
        inputSnDialog.setView(R.layout.dialog_input_sn);
        EditText et_input_sn_dialog = inputSnDialog.findViewById(R.id.et_input_sn_dialog);
        TextView tv_cancel_dialog = inputSnDialog.findViewById(R.id.tv_cancel_dialog);
        TextView tv_sure_dialog = inputSnDialog.findViewById(R.id.tv_sure_dialog);
        inputSnDialog.show();
        closeCamera();
        tv_cancel_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restartCamera();
                inputSnDialog.dismiss();
            }
        });
        tv_sure_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputSn = et_input_sn_dialog.getText().toString();
                if (TextUtils.isEmpty(inputSn)) {
                    ToastUtil.showToastShort(getResources().getString(R.string.tv_input_sn_tip));
                    return;
                }
                restartCamera();
                inputSnDialog.dismiss();
                queryDeviceInfoBySn(inputSn);
            }
        });
    }


    //根据sn查询设备信息
    private void queryDeviceInfoBySn(String sn) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("sn", sn);
            ServerClient.newInstance(MyApplication.getContext()).queryDeviceInfoBySn(MyApplication.getContext(), Constants.TAG_QUERY_DEVICE_SN, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }


    //开灯
    private void turnOn() {
        mNeedFlashLightOpen = false;
        iv_light.setImageResource(R.mipmap.icon_general_scan_light_off);
        CameraManager.get().setFlashLight(true);
    }


    /**
     * 关灯
     * <功能详细描述>
     * [类、类#方法、类#成员]
     */
    private void turnFlashLightOff() {
        mNeedFlashLightOpen = true;
        iv_light.setImageResource(R.mipmap.icon_general_scan_light_on);
        CameraManager.get().setFlashLight(false);
    }

    public void showBindDialog(Activity context, String storeName, final String qrcodeId, String id) {
        BindDialog bindDialog = new BindDialog(context, storeName, id, new BindDialog.HandleBtn() {
            @Override
            public void handleOkBtn() {
                StatService.trackCustomKVEvent(CaptureActivity.this, "1083", null);
                String storeId = getIntent().getStringExtra(Constants.INTENT_BIND_STORE_ID);
                if (!StringUtils.isEmptyOrNull(qrcodeId)) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("storeMerchantId", storeId);
                    map.put("qrcode", qrcodeId);
                    if (!NetworkUtils.isNetworkAvailable(CaptureActivity.this)) {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                        return;
                    } else {
                        DialogUtil.safeShowDialog(mBindingDialog);
                        ServerClient.newInstance(CaptureActivity.this).bindCode(CaptureActivity.this, Constants.TAG_BIND_CODE, map);
                    }
                }
            }

            @Override
            public void handleCancelBtn() {
                restartCamera();
            }
        });
        bindDialog.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void getDialog(String content) {
        if (mNoticeDialog == null) {
            mNoticeDialog = new NoticeDialog(CaptureActivity.this, content, getString(R.string.btn_know), R.layout.notice_dialog_common);
            mNoticeDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    restartCamera();
                }
            });
        }
        mNoticeDialog.show();
    }
}
