package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.StatusBarUtil;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2018/4/18 14:07
 * @描述: ${TODO}
 */

public class VipGuideActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private TextView mTvTitle;
    private Button mBtnEnsure;
    private String mMerchantId,mIntentName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        setContentView(R.layout.activity_vip_guide);
        StatusBarUtil.setTranslucentStatus(this);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(getString(R.string.title_vip_guide));
        mBtnEnsure = (Button) findViewById(R.id.btn_common_enable);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnEnsure.setOnClickListener(this);
    }

    private void initData() {
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if (TextUtils.isEmpty(mMerchantId)) {
            mMerchantId = "";
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_common_enable:
                Intent intent = new Intent(VipGuideActivity.this, VipInfoActivity.class);
                intent.putExtra(Constants.INTENT_SKIP_MEMBER_DETAIL, false);//重新提交，是否显示详情
                if (!TextUtils.isEmpty(mMerchantId)) {
                    intent.putExtra(Constants.INTENT_MERCHANT_ID, mMerchantId);
                }
                if (!TextUtils.isEmpty(mIntentName)) {
                    intent.putExtra(Constants.INTENT_NAME, mIntentName);
                }
                startActivity(intent);
                break;
        }
    }
}
