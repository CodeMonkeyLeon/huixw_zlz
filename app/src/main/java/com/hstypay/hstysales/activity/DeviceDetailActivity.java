package com.hstypay.hstysales.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.lib.WheelView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.adapter.DeviceManageTypeAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.DeviceDetailBean;
import com.hstypay.hstysales.bean.DeviceType;
import com.hstypay.hstysales.bean.DeviceTypeBean;
import com.hstypay.hstysales.bean.Info;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.ArrowAnimationUtil;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DateUtil;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.TimePickViewHelp;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.SafeDialog;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

//设备信息详情页
public class DeviceDetailActivity extends BaseActivity implements View.OnClickListener {

    private TextView mTvTitle;
    private ImageView mIvBack;
    private SafeDialog mLoadDialog;
    private String mDeviceId;
    private String mCategoryCode;
    private String mCategoryName;
    private String mLogoImgUrl;
    private ImageView mIvDeviceLogoDetail;//设备图片
    private TextView mTvMerchantNameDeviceDetail;//所属商户名称
    private TextView mTvStoreNameDeviceDetail;//门店名称
    private TextView mTvDeviceTypeDeviceDetail;//设备类型
    private TextView mTvDeviceSnDeviceDetail;//设备sn码
    private TextView mTvSelectDateDeviceDetail;//日期显示
    private ImageView mIvArrowDateDeviceDetail;//日期箭头
    private TextView mTvJyjeDeviceDetail;//交易金额
    private TextView mTvYhjeDeviceDetail;//优惠金额
    private TextView mTvYhbsDeviceDetail;//优惠笔数
    private TextView mTvSyjeDeviceDetail;//实收金额
    private TextView mTvTkjeDeviceDetail;//退款金额
    private TextView mTvTkbsDeviceDetail;//退款笔数
    private TextView mTvJyzeDeviceDetail;//交易净额

    private String timeFormatType = "yyyy-MM-dd HH:mm:ss";
    private Calendar mCurSelectEndDate;
    private Calendar mCurSelectStartDate;
    private TimePickViewHelp mTimePickViewHelp;
    private boolean isDateOpen;//头部日期是否展开

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_detail);
        StatusBarUtil.setTranslucentStatusWhiteIcon(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initEvent() {
        mIvBack.setOnClickListener(this);
        mTvSelectDateDeviceDetail.setOnClickListener(this);
    }

    private void initData() {
        mTvTitle.setText(getString(R.string.device_detail));
        mDeviceId = getIntent().getStringExtra(Constants.INTENT_NAME_DEVICE_ID);
        mCategoryCode = getIntent().getStringExtra(Constants.INTENT_NAME_CATEGORY_CODE);
        mCategoryName = getIntent().getStringExtra(Constants.INTENT_NAME_CATEGORY_TYPE);
        mLogoImgUrl = getIntent().getStringExtra(Constants.INTENT_NAME_DEVICE_IMG);

        mCurSelectEndDate = Calendar.getInstance();//当前选择的结束日期为今天
        mCurSelectStartDate = Calendar.getInstance();
        mCurSelectStartDate.add(Calendar.MONTH,-1);//当前选择的开始日期为1个月前
        mCurSelectStartDate.set(Calendar.HOUR_OF_DAY,0);
        mCurSelectStartDate.set(Calendar.MINUTE,0);
        mCurSelectStartDate.set(Calendar.SECOND,0);
        mCurSelectEndDate.set(Calendar.HOUR_OF_DAY,23);
        mCurSelectEndDate.set(Calendar.MINUTE,59);
        mCurSelectEndDate.set(Calendar.SECOND,59);
        initPickViewDate();

        getDeviceDetailData();

        switch (mCategoryCode) {
            case "HAPY_POS"://扫码pos
            case "HPAY_POS"://智能pos
                Picasso.get().load(R.mipmap.icon_receive_device).into(mIvDeviceLogoDetail);
                break;
            case "HPAY_FACEPAY"://刷脸终端
                Picasso.get().load(R.mipmap.icon_item_face_device).into(mIvDeviceLogoDetail);
                break;
            case "CLOUD_BOX"://云音箱
                Picasso.get().load(R.mipmap.icon_item_cloud).into(mIvDeviceLogoDetail);
                break;
            case "CLOUD_PRINT"://云打印
                Picasso.get().load(R.mipmap.icon_item_printer_device).into(mIvDeviceLogoDetail);
                break;
            case "HPAY_RAM"://动态台卡
                Picasso.get().load(R.mipmap.icon_item_dynamic).into(mIvDeviceLogoDetail);
                break;
            default:
                Picasso.get().load(Constants.BASE_URL + mLogoImgUrl).error(R.mipmap.icon_item_cloud).placeholder(R.mipmap.icon_item_cloud).into(mIvDeviceLogoDetail);
                break;
        }
        mTvDeviceTypeDeviceDetail.setText("设备类型："+(mCategoryName==null?"":mCategoryName));

        mTvSelectDateDeviceDetail.setText(mTimePickViewHelp.getDateString(mCurSelectStartDate,"yyyy/MM/dd")+"～"+mTimePickViewHelp.getDateString(mCurSelectEndDate,"yyyy/MM/dd"));

    }

    private void initPickViewDate() {
        Calendar rangeStartDate = mCurSelectStartDate;
        Calendar rangeEndDate = mCurSelectEndDate;
        Calendar curStartDate = mCurSelectStartDate;
        Calendar curEndDate = mCurSelectEndDate;
        mTimePickViewHelp = new TimePickViewHelp(this,rangeStartDate,rangeEndDate,curStartDate,curEndDate);
        mTimePickViewHelp.mTv_bottom_tx_pickview.setVisibility(View.GONE);
    }


    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        View rl_title_device_detail = findViewById(R.id.rl_title_device_detail);
        rl_title_device_detail.setBackgroundColor(getResources().getColor(R.color.transparent));
        mTvTitle = findViewById(R.id.tv_title);
        mIvBack = findViewById(R.id.iv_back);

        mIvDeviceLogoDetail = findViewById(R.id.iv_device_logo_detail);
        mTvMerchantNameDeviceDetail = findViewById(R.id.tv_merchant_name_device_detail);
        mTvStoreNameDeviceDetail = findViewById(R.id.tv_store_name_device_detail);
        mTvDeviceTypeDeviceDetail = findViewById(R.id.tv_device_type_device_detail);
        mTvDeviceSnDeviceDetail = findViewById(R.id.tv_device_sn_device_detail);
        mTvSelectDateDeviceDetail = findViewById(R.id.tv_select_date_device_detail);
        mIvArrowDateDeviceDetail = findViewById(R.id.iv_arrow_date_device_detail);
        mTvJyjeDeviceDetail = findViewById(R.id.tv_jyje_device_detail);
        mTvYhjeDeviceDetail = findViewById(R.id.tv_yhje_device_detail);
        mTvYhbsDeviceDetail = findViewById(R.id.tv_yhbs_device_detail);
        mTvSyjeDeviceDetail = findViewById(R.id.tv_syje_device_detail);
        mTvTkjeDeviceDetail = findViewById(R.id.tv_tkje_device_detail);
        mTvTkbsDeviceDetail = findViewById(R.id.tv_tkbs_device_detail);
        mTvJyzeDeviceDetail = findViewById(R.id.tv_jyze_device_detail);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back://返回
                onBackPressed();
                break;
            case R.id.tv_select_date_device_detail:
                //日期选择
                if (isDateOpen){
                    ArrowAnimationUtil.closeArrow(mIvArrowDateDeviceDetail,this);
                    isDateOpen = false;
                    mTimePickViewHelp.dismiss();
                }else {
                    ArrowAnimationUtil.openArrow(mIvArrowDateDeviceDetail,this);
                    isDateOpen = true;
                    mTimePickViewHelp.show(new TimePickViewHelp.CallBackDataListener() {
                        @Override
                        public void onCancel() {
                            ArrowAnimationUtil.closeArrow(mIvArrowDateDeviceDetail,DeviceDetailActivity.this);
                            isDateOpen = false;
                        }

                        @Override
                        public void onSubmit(Calendar curSelectStartDate, Calendar curSelectEndDate) {
                            String startDateStr = mTimePickViewHelp.getDateString(curSelectStartDate, "yyyy/MM/dd");
                            String endDateStr = mTimePickViewHelp.getDateString(curSelectEndDate, "yyyy/MM/dd");
                            mTvSelectDateDeviceDetail.setText(startDateStr+"～"+endDateStr);
                            ArrowAnimationUtil.closeArrow(mIvArrowDateDeviceDetail,DeviceDetailActivity.this);
                            isDateOpen = false;
                            mTimePickViewHelp.dismiss();
                            mCurSelectStartDate = curSelectStartDate;
                            mCurSelectEndDate = curSelectEndDate;
                            getDeviceDetailData();
                        }
                    });
                }

                break;
            default:
                break;
        }
    }


    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void getDeviceDetailData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("id", mDeviceId);
            map.put("startTime",mTimePickViewHelp.getDateString(mCurSelectStartDate,timeFormatType));
            map.put("endTime",mTimePickViewHelp.getDateString(mCurSelectEndDate,timeFormatType));
//            map.put("startTime","2021-07-01 00:00:00");
//            map.put("endTime","2021-07-22 23:59:59");
            ServerClient.newInstance(MyApplication.getContext()).getDeviceDeatil(MyApplication.getContext(), Constants.TAG_DETAIL_DEVICE, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        //解绑设备
        if (event.getTag().equals(Constants.TAG_DETAIL_DEVICE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            DeviceDetailBean msg = (DeviceDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getResources().getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(DeviceDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData()!=null){
                        DeviceDetailBean.DeviceDetailData deviceDetailData = msg.getData();
                        mTvMerchantNameDeviceDetail.setText(deviceDetailData.getMerchantIdCnt());
                        mTvStoreNameDeviceDetail.setText("门店名称："+deviceDetailData.getStoreMerchantIdCnt());
                        mTvDeviceSnDeviceDetail.setText("设备SN码："+deviceDetailData.getSn());
                        DeviceDetailBean.TradeResult tradeResult = deviceDetailData.getTradeResult();
                        if (tradeResult!=null){
                            mTvJyjeDeviceDetail.setText(DateUtil.formatMoneyUtil((tradeResult.getSaleTotalAmount()) / 100d));
                            mTvYhjeDeviceDetail.setText(DateUtil.formatMoneyUtil((tradeResult.getMchDiscountsTotalAmount()) / 100d));
                            mTvYhbsDeviceDetail.setText(tradeResult.getMchDiscountsTotalCount()+"");
                            mTvSyjeDeviceDetail.setText(DateUtil.formatMoneyUtil((tradeResult.getPayAmount()) / 100d));
                            mTvTkjeDeviceDetail.setText(DateUtil.formatMoneyUtil((tradeResult.getRefundTotalAmount()) / 100d));
                            mTvTkbsDeviceDetail.setText(tradeResult.getRefundTotalCount()+"");
                            mTvJyzeDeviceDetail.setText(DateUtil.formatMoneyUtil((tradeResult.getPayNetAmount()) / 100d));
                        }
                    }
                    break;
            }
        }
    }

}