package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.BuildConfig;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.LoginBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.ConfigUtil;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.ShapeSelectorUtils;
import com.hstypay.hstysales.utils.SpStayUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.ExchangeDialog;
import com.hstypay.hstysales.widget.SafeDialog;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvClean, mIvEyePwd, mIvLogin;
    private EditText mEtUserName, mEtUserPwd;
    private Button mBtnForgetPwd, mBtnLogin;
    private boolean isOpenPwd;
    private SafeDialog mLoadDialog;
    private int srollHeight = 0;
    private boolean isFristIn = true;
    private RelativeLayout mRlLogin;
    private final static int COUNTS = 5;//点击次数
    private final static long DURATION = 3000L;//规定有效时间
    private long[] mHits = new long[COUNTS];
    private ExchangeDialog mDialog;
    private ImageButton mIbLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        setContentView(R.layout.activity_login);
        StatusBarUtil.setTranslucentStatus(this);
        mRlLogin = (RelativeLayout) findViewById(R.id.login_top);
        mIvLogin = (ImageView) findViewById(R.id.iv_login);
        mIvClean = (ImageView) findViewById(R.id.iv_clean);
        mIvEyePwd = (ImageView) findViewById(R.id.iv_eye_pwd);
        mEtUserName = (EditText) findViewById(R.id.user_name);
        mEtUserPwd = (EditText) findViewById(R.id.user_pwd);
        mBtnForgetPwd = (Button) findViewById(R.id.btn_forget_pwd);
        mBtnLogin = (Button) findViewById(R.id.btn_login);
        mIbLogin = findViewById(R.id.ib_login);

        mLoadDialog = getLoadDialog(this, getString(R.string.login_loading), false);

        mEtUserName.setText(SpStayUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_NAME));
        mEtUserName.setSelection(mEtUserName.getText().toString().trim().length());
        if (!TextUtils.isEmpty(mEtUserName.getText().toString().trim())) {
            mEtUserPwd.setFocusable(true);
            mEtUserPwd.setFocusableInTouchMode(true);
            mEtUserPwd.requestFocus();
        } else {
            mEtUserName.setFocusable(true);
            mEtUserName.setFocusableInTouchMode(true);
            mEtUserName.requestFocus();
        }
        setButtonState();
        controlKeyboardLayout(mRlLogin, mBtnLogin.getVisibility() == View.VISIBLE ? mBtnLogin : mIbLogin);
    }

    private void initListener() {
        mIvClean.setOnClickListener(this);
        mIvEyePwd.setOnClickListener(this);
        mBtnForgetPwd.setOnClickListener(this);
        mBtnLogin.setOnClickListener(this);
        mIbLogin.setOnClickListener(this);

        if (!BuildConfig.IS_OFFICIAL) {
            mIvLogin.setOnClickListener(this);
        }

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                setButtonState();
            }
        };
        mEtUserName.addTextChangedListener(textWatcher);
        mEtUserPwd.addTextChangedListener(textWatcher);
        mEtUserName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    mIvClean.setVisibility(mEtUserName.getText().toString().length() == 0 ? View.INVISIBLE : View.VISIBLE);
                } else {
                    mIvClean.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void initData() {

    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_LOGIN)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            LoginBean msg = (LoginBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.MSG_LOGIN_FALSE:
                    if (msg.getError().getMessage() != null) {
                        ToastUtil.showToastShort(msg.getError().getMessage());
                    }
                    break;
                case Constants.MSG_LOGIN_TRUE:
                    SpStayUtil.putString(MyApplication.getContext(), Constants.SP_LOGIN_NAME, mEtUserName.getText().toString().trim());
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mEtUserPwd.setText("");
        if (!TextUtils.isEmpty(mEtUserName.getText().toString().trim())) {
            mEtUserPwd.setFocusable(true);
            mEtUserPwd.setFocusableInTouchMode(true);
            mEtUserPwd.requestFocus();
        } else {
            mEtUserName.setFocusable(true);
            mEtUserName.setFocusableInTouchMode(true);
            mEtUserName.requestFocus();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_clean:
                mEtUserName.setText("");
                mEtUserName.setFocusable(true);
                mEtUserName.setFocusableInTouchMode(true);
                mEtUserName.requestFocus();
                break;
            case R.id.iv_eye_pwd:
                isOpenPwd = setEye(isOpenPwd);
                setPwdVisible(isOpenPwd, mIvEyePwd, mEtUserPwd);
                break;
            case R.id.btn_forget_pwd:
                Intent intent = new Intent(LoginActivity.this, FindPwdActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_login:
            case R.id.ib_login:
                login();
                break;
            case R.id.iv_login:
                System.arraycopy(mHits, 1, mHits, 0, mHits.length - 1);
                //实现左移，然后最后一个位置更新距离开机的时间，如果最后一个时间和最开始时间小于DURATION，即连续5次点击
                mHits[mHits.length - 1] = SystemClock.uptimeMillis();
                if (mHits[0] >= (SystemClock.uptimeMillis() - DURATION)) {
                    mHits = new long[COUNTS];
                    showExchangeDialog();
                }
                break;
        }
    }

    private void showExchangeDialog() {
        if (mDialog == null) {
            mDialog = new ExchangeDialog(LoginActivity.this);
            DialogHelper.resize(LoginActivity.this, mDialog);
        }
        mDialog.show();
    }

    private void login() {
        String mUserName = mEtUserName.getText().toString().trim();
        String userPwd = mEtUserPwd.getText().toString().trim();
        if (StringUtils.isEmptyOrNull(mUserName)) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_username));
            mEtUserName.setFocusable(true);
            mEtUserName.setFocusableInTouchMode(true);
            mEtUserName.requestFocus();
            return;
        }


        if (StringUtils.isEmptyOrNull(userPwd)) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_userpwd));
            mEtUserPwd.setFocusable(true);
            mEtUserPwd.setFocusableInTouchMode(true);
            mEtUserPwd.requestFocus();
            return;
        }

        /*if(userPwd.length()<6){
            ToastHelper.showInfo(LoginActivity.this,ToastHelper.toStr(R.string.pwd_prompting));//6-32位字母、数字
            return;
        }*/

        if (NetworkUtils.isNetWorkValid(LoginActivity.this)) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(LoginActivity.this).login(LoginActivity.this, Constants.TAG_LOGIN, mUserName, userPwd);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
            return;
        }
    }

    private void setPwdVisible(boolean isOpen, ImageView ivEye, EditText etPwd) {
        if (isOpen) {
            ivEye.setImageResource(R.mipmap.login_password_open);
            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ivEye.setImageResource(R.mipmap.login_password_close);
            etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        etPwd.setSelection(etPwd.getText().toString().trim().length());
    }

    private boolean setEye(boolean isOpen) {
        return isOpen ? false : true;
    }

    private void setButtonState() {
        if (!StringUtils.isEmptyOrNull(mEtUserName.getText().toString().trim())
                && !StringUtils.isEmptyOrNull(mEtUserPwd.getText().toString().trim())) {
            mIbLogin.setEnabled(true);
            Drawable ovalDefault = ShapeSelectorUtils.createOvalDefault(MyApplication.getContext(), MyApplication.themeColor());
            Drawable ovalPressed = ShapeSelectorUtils.createOvalDefault(MyApplication.getContext(), MyApplication.themeColor());
            ovalPressed.setAlpha(204);
            StateListDrawable stateListDrawable = ShapeSelectorUtils.makeSelector(ovalDefault, ovalPressed, ovalPressed);
            mIbLogin.setBackground(stateListDrawable);
            setButtonBg(mBtnLogin, true);
        } else {
            mIbLogin.setEnabled(false);
            Drawable ovalDefault = ShapeSelectorUtils.createOvalDefault(MyApplication.getContext(), MyApplication.themeColor());
            ovalDefault.setAlpha(102);
            mIbLogin.setBackground(ovalDefault);
            setButtonBg(mBtnLogin, false);
        }

        if (mEtUserName.hasFocus()) {
            mIvClean.setVisibility(mEtUserName.getText().toString().length() == 0 ? View.INVISIBLE : View.VISIBLE);
        }
    }

    public void setButtonBg(Button button, boolean enable) {
        if (enable) {
            button.setEnabled(true);
            button.setBackgroundResource(R.drawable.selector_btn_login_bg);
            button.setTextColor(UIUtils.getColor(R.color.login_enable_text));
        } else {
            button.setEnabled(false);
            button.setBackgroundResource(R.drawable.shape_login_button);
            button.setTextColor(UIUtils.getColor(R.color.login_unable_text));
        }
    }

    private void controlKeyboardLayout(final View root, final View scrollToView) {
        root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                //获取root在窗体的可视区域
                root.getWindowVisibleDisplayFrame(rect);
                //获取root在窗体的不可视区域高度(被其他View遮挡的区域高度)
                int rootInvisibleHeight = root.getRootView().getHeight() - rect.bottom;
                //System.out.println("height======="+rootInvisibleHeight);
                //若不可视区域高度大于100，则键盘显示
                if (rootInvisibleHeight > 300) {
                    int[] location = new int[2];
                    //获取scrollToView在窗体的坐标
                    scrollToView.getLocationInWindow(location);
                    //计算root滚动高度，使scrollToView在可见区域
                    if (isFristIn) {
                        srollHeight = (location[1] + scrollToView.getHeight()) - rect.bottom;
                        isFristIn = false;
                    }
                    mIvLogin.setVisibility(View.INVISIBLE);
                    root.scrollTo(0, srollHeight);
                } else {
                    //键盘隐藏
                    mIvLogin.setVisibility(View.VISIBLE);
                    root.scrollTo(0, 0);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }
}
