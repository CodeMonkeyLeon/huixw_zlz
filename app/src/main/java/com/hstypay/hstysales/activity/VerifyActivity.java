package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.InfoBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.ShadowLayout;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class VerifyActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle,mTvUsername;
    private EditText mEtPwd;
    private ShadowLayout mShadowButton;
    private Button mBtnEnsure,mBtnUnsure;
    private SafeDialog mLoadDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        setContentView(R.layout.activity_verify);
        StatusBarUtil.setTranslucentStatus(this);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mShadowButton = (ShadowLayout) findViewById(R.id.shadow_button);
        mBtnEnsure = (Button) findViewById(R.id.btn_common_enable);
        mBtnUnsure = (Button) findViewById(R.id.btn_common_unable);
        mTvUsername = (TextView) findViewById(R.id.tv_login_username);
        mEtPwd = (EditText) findViewById(R.id.et_login_pwd);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mTvTitle.setText(getString(R.string.title_verify));
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnEnsure.setOnClickListener(this);

        mEtPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length()>0){
                    mShadowButton.setVisibility(View.VISIBLE);
                    mBtnUnsure.setVisibility(View.GONE);
                }else{
                    mShadowButton.setVisibility(View.GONE);
                    mBtnUnsure.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void initData() {
        mTvUsername.setText(SpUtil.getString(MyApplication.getContext(),Constants.SP_USER_NAME));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_common_enable:
                StatService.trackCustomKVEvent(VerifyActivity.this, "1056", null);
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    Map<String,Object> map = new HashMap<>();
                    map.put("password",mEtPwd.getText().toString().trim());
                    ServerClient.newInstance(MyApplication.getContext()).authentication(MyApplication.getContext(),Constants.TAG_AUTHENTICATION,map);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                }
                break;
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_AUTHENTICATION)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.AUTHENTICATION_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(VerifyActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.AUTHENTICATION_TRUE:
                    Intent intent = new Intent(this, ChangeTelActivity.class);
                    intent.putExtra(Constants.INTENT_PASSWORD,mEtPwd.getText().toString().trim());
                    startActivity(intent);
                    finish();
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }
}
