package com.hstypay.hstysales.activity;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.tinycashier.PCCashierSnManageActivity;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.BaseBean;
import com.hstypay.hstysales.bean.CardDetailBean;
import com.hstypay.hstysales.bean.ConfirmMerchantBean;
import com.hstypay.hstysales.bean.MerchantInfoBean;
import com.hstypay.hstysales.bean.PayTypeStatusBean;
import com.hstypay.hstysales.bean.SettleChangeCountBean;
import com.hstypay.hstysales.bean.WxSignDetailBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CustomViewCenterDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.SelectDialog;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class MerchantInfoActivity extends BaseActivity implements View.OnClickListener {

    private ImageView iv_back;
    private ImageView iv_title_button;
    private String propValue = "审核失败";
    private String mMerchantTel, mMerchantId, mMerchantName;
    private TextView tv_title, mTvPayTypeStatus,mTvNotice;
    private SafeDialog mLoadDialog, mLoadPayTypeDialog;
    private MerchantInfoBean.DataBean mData;
    private TextView tv_merchant_name;
    private TextView tv_merchand_num;
    private RelativeLayout rl_basic_info;
    private RelativeLayout rl_person_info;
    private RelativeLayout rl_settle_info;
    private RelativeLayout rl_credentials_info;
    private RelativeLayout rl_bind_code_info, rl_pay_type_info,mRlPcSnManage;
    private CardDetailBean.DataEntity mCardData;
    private ImageView iv_state;
    private TextView tv_state;
    private TextView tv_fail_info;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };

    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private String type;//页面标识
    private LinearLayout mLlNotConfirmMerchantInfo;//待确认的商户
    private Button mBtnConfirmSureMarchatInfo;//确认提交
    private Button mBtnRefuseSureMarchatInfo;//拒绝
    private String mConfirmStatus;
    private TextView mTvCountChangeSettle;
    public static final int INITCHANGEAVAILDCOUNT = 100;
    private int mChangeAcountCount= INITCHANGEAVAILDCOUNT;//剩余可修改次数,初始化为100表示可修改否不受改字段控制
    private String mMerchantShortName;
    private RelativeLayout mRlZfbSqMerchant;
    private RelativeLayout mRlWxRealNameMerchant;//微信实名认证
    private TextView mTvStateWxAuthMerchant;//微信实名认证状态
    private RelativeLayout mRlAliRealNameMerchant;//支付宝实名认证
    private TextView mTvStateAliAuthMerchant;//支付宝实名认证状态
    private RelativeLayout mRlDeviceMerchant;
    private TextView mTvCopyNameMerchantInfo;
    private TextView mTvCopyIdMerchantInfo;
    private WxSignDetailBean.SignDetailData mSignDetailData;
    private static final int REQUEST_CODE_WX_SIGN_DETAIL = 105;//微信签约详情
    //新增第三方商户id 字段 thirdMchId
    private LinearLayout ll_thirdMchId;
    private TextView tv_thirdMchId, tv_copy_thirdMchId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_info);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    private void initData() {
        mCardData = new CardDetailBean().new DataEntity();
        mMerchantTel = getIntent().getStringExtra(Constants.INTENT_MERCHANT_TEL);
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        mMerchantName = getIntent().getStringExtra(Constants.INTENT_MERCHANT_NAME);
        type = getIntent().getStringExtra("type");
        getData(mMerchantId);

        //getWxSignDetail();
    }

    //获取微信签约详情信息
    private void getWxSignDetail() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            //DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", mMerchantId);
            ServerClient.newInstance(MyApplication.getContext()).queryWxSignDetail(MyApplication.getContext(), Constants.TAG_WX_SIGN_DETAIL, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }


    private void getData(String merchantId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", merchantId);
            ServerClient.newInstance(MyApplication.getContext()).getMerchantInfo(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_INFO, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void initEvent() {
        iv_back.setOnClickListener(this);
        iv_title_button.setOnClickListener(this);
        rl_basic_info.setOnClickListener(this);
        rl_person_info.setOnClickListener(this);
        rl_settle_info.setOnClickListener(this);
        rl_credentials_info.setOnClickListener(this);
        rl_bind_code_info.setOnClickListener(this);
        rl_pay_type_info.setOnClickListener(this);
        mBtnConfirmSureMarchatInfo.setOnClickListener(this);
        mBtnRefuseSureMarchatInfo.setOnClickListener(this);
        mRlPcSnManage.setOnClickListener(this);
        mRlZfbSqMerchant.setOnClickListener(this);
        mRlDeviceMerchant.setOnClickListener(this);
        mTvCopyNameMerchantInfo.setOnClickListener(this);
        mTvCopyIdMerchantInfo.setOnClickListener(this);
        //thirdMchId
        tv_copy_thirdMchId.setOnClickListener(this);
    }

    private void initView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_title_button = (ImageView) findViewById(R.id.iv_title_button);
        iv_title_button.setVisibility(View.VISIBLE);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText(UIUtils.getString(R.string.title_merchant_info));
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mLoadPayTypeDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        tv_merchant_name = findViewById(R.id.tv_merchant_name);
        tv_merchand_num = findViewById(R.id.tv_merchand_num);
        mTvCopyNameMerchantInfo = findViewById(R.id.tv_copy_name_merchant_info);
        mTvCopyIdMerchantInfo = findViewById(R.id.tv_copy_id_merchant_info);
        mTvNotice = findViewById(R.id.tv_change_bank_card_notice);
        rl_basic_info = findViewById(R.id.rl_basic_info);//基础信息
        rl_person_info = findViewById(R.id.rl_person_info);//法人信息
        rl_settle_info = findViewById(R.id.rl_settle_info);//结算信息
        mTvCountChangeSettle = findViewById(R.id.tv_count_change_settle); //商户剩余修改结算信息次数
        rl_credentials_info = findViewById(R.id.rl_credentials_info);//证件信息
        rl_bind_code_info = findViewById(R.id.rl_bind_code_info);//绑定二维码
        rl_pay_type_info = findViewById(R.id.rl_pay_type_info);//支付类型
        mTvPayTypeStatus = findViewById(R.id.tv_pay_type_status);//支付类型
        iv_state = findViewById(R.id.iv_state);
        tv_state = findViewById(R.id.tv_state);
        tv_fail_info = findViewById(R.id.tv_fail_info);
        mLlNotConfirmMerchantInfo = findViewById(R.id.ll_not_confirm_merchant_info);
        mBtnConfirmSureMarchatInfo = findViewById(R.id.btn_confirm_sure_marchat_info);
        mBtnRefuseSureMarchatInfo = findViewById(R.id.btn_refuse_sure_marchat_info);
        mRlPcSnManage = findViewById(R.id.rl_pc_sn_manage);
        mRlZfbSqMerchant = findViewById(R.id.rl_zfb_sq_merchant);
        mRlDeviceMerchant = findViewById(R.id.rl_device_merchant);
        mRlWxRealNameMerchant = findViewById(R.id.rl_wx_real_name_merchant);
        mTvStateWxAuthMerchant = findViewById(R.id.tv_state_wx_auth_merchant);
        mRlAliRealNameMerchant = findViewById(R.id.rl_ali_real_name_merchant);
        mTvStateAliAuthMerchant = findViewById(R.id.tv_state_ali_auth_merchant);
        //thirdMchId
        ll_thirdMchId = findViewById(R.id.ll_thirdMchId);
        tv_thirdMchId = findViewById(R.id.tv_thirdMchId);
        tv_copy_thirdMchId = findViewById(R.id.tv_copy_thirdMchId);
    }

    //是否显示thirdMchId
    public void showThirdMchId(boolean isShow) {
        //当前是否是安徽新零售
        String installChannel = AppHelper.getAppMetaData(MyApplication.getContext(), Constants.APP_META_DATA_KEY);
        if (installChannel != null) {
            if (!installChannel.equals(Constants.AHDXSALES)) return;
        } else {
            return;
        }
        ll_thirdMchId.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_title_button:
                mtaProperties(MerchantInfoActivity.this, "1038", "status", propValue);
                call(mMerchantTel);
                break;
            case R.id.rl_basic_info:
                //基本信息
                Intent intent = new Intent(MerchantInfoActivity.this, MerchantBasicActivity.class);
                intent.putExtra("data", mData);
                intent.putExtra("type", type);
                intent.putExtra(Constants.INTENT_MERCHANT_ID,mMerchantId);
                startActivity(intent);
                break;
            case R.id.rl_person_info:
                //法人信息
                Intent legalIntent = new Intent(MerchantInfoActivity.this, LegalPersonActivity.class);
                legalIntent.putExtra("data", mData);
                legalIntent.putExtra("type", type);
                legalIntent.putExtra(Constants.INTENT_MERCHANT_ID,mMerchantId);
                startActivity(legalIntent);
                break;
            case R.id.rl_settle_info:
                //结算信息
                Intent intentCard = new Intent();
                intentCard.putExtra(Constants.INTENT_BANK_DETAIL, mData);
                intentCard.putExtra("type", type);
                intentCard.putExtra(Constants.INTENT_CHANGEACOUNTCOUNT,mChangeAcountCount);
                intentCard.putExtra(Constants.INTENT_MERCHANT_ID,mMerchantId);
                intentCard.setClass(MerchantInfoActivity.this, ChangeCompanyCardActivity.class);
                startActivityForResult(intentCard,Constants.REQUEST_MERCHANT_CARD);
                break;
            case R.id.rl_credentials_info:
                //证件信息
                //上传证件照
                boolean b = PermissionUtils.checkPermissionArray(MerchantInfoActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE1);
                Intent UPintent = new Intent(this, LicenseInfoActivity.class);
                UPintent.putExtra(Constants.INTENT_MERCHANT_ID,mMerchantId);
                if (mData != null) {
                    UPintent.putExtra(Constants.INTENT_NAME_MERCHANT_UPLOAD, mData);
                }
                UPintent.putExtra("type", type);
                if (b) {
                    startActivityForResult(UPintent, Constants.REQUEST_UPLOAD_IMAGE);
                }
                break;
            case R.id.rl_bind_code_info:
                //绑定二维码
                Intent intentBind = new Intent(MerchantInfoActivity.this, BindingCodeActivity.class);
                intentBind.putExtra(Constants.INTENT_NAME, Constants.INTENT_BIND_CODE);
                intentBind.putExtra(Constants.INTENT_MERHANT_ID, mMerchantId);
                startActivity(intentBind);
                break;
            case R.id.rl_pay_type_info:
                //支付类型
                Intent intentPayType = new Intent(this, PayTypeActivity.class);
                intentPayType.putExtra(Constants.INTENT_MERCHANT_TEL, mMerchantTel);
                intentPayType.putExtra(Constants.INTENT_MERCHANT_ID, mMerchantId);
                startActivity(intentPayType);
                break;
            case R.id.btn_confirm_sure_marchat_info:
                //确认提交
                if (mData!=null){
                    confirmSureMarchat("1");
                }
                break;
            case R.id.btn_refuse_sure_marchat_info:
                //拒绝
                showRefuseMarchatDialog();
                break;
            case R.id.rl_pc_sn_manage:
                //PC收银激活码管理
                startActivity(new Intent(this, PCCashierSnManageActivity.class)
                        .putExtra(Constants.INTENT_MERHANT_ID, mMerchantId)
                        .putExtra(Constants.INTENT_SHORT_NAME, mMerchantShortName));
                break;
            case R.id.rl_zfb_sq_merchant:
                Intent intentZfb = new Intent(this,ZfbStoreAuthorActivity.class);
                intentZfb.putExtra(Constants.INTENT_MERCHANT_ID, mMerchantId);
                startActivity(intentZfb);
                break;
            case R.id.rl_wx_real_name_merchant:
                //微信实名认证详情
                Intent intentWx = new Intent(this,WxRealNameAuthDetailActivity.class);
                intentWx.putExtra(Constants.INTENT_MERCHANT_ID, mMerchantId);
                intentWx.putExtra(Constants.INTENT_NAME,"INTENT_WX_AUTH");
                if (mSignDetailData!=null){
                    intentWx.putExtra(Constants.INTENT_WX_SIGN_DATA,mSignDetailData);
                }
                startActivityForResult(intentWx,REQUEST_CODE_WX_SIGN_DETAIL);
                break;
            case R.id.rl_ali_real_name_merchant:
                //支付宝实名认证详情
                Intent intentAli = new Intent(this,WxRealNameAuthDetailActivity.class);
                intentAli.putExtra(Constants.INTENT_MERCHANT_ID, mMerchantId);
                intentAli.putExtra(Constants.INTENT_NAME,"INTENT_ALI_AUTH");
                if (mSignDetailData!=null){
                    intentAli.putExtra(Constants.INTENT_WX_SIGN_DATA,mSignDetailData);
                }
                startActivityForResult(intentAli,REQUEST_CODE_WX_SIGN_DETAIL);
                break;
            case R.id.rl_device_merchant:
                //设备管理
                Intent intentDeviceList = new Intent(this,DeviceListActivity.class);//DeviceListActivity,DeviceDetailActivity
                intentDeviceList.putExtra(Constants.INTENT_MERCHANT_ID, mMerchantId);
                intentDeviceList.putExtra(Constants.INTENT_MERCHANT_NAME, mMerchantName);
                startActivity(intentDeviceList);
                break;
            case R.id.tv_copy_name_merchant_info:
                //复制商户名称
                if (!TextUtils.isEmpty(mMerchantName)){
                    AppHelper.copy2(mMerchantName,MerchantInfoActivity.this);
                    ToastUtil.showToastShort(getString(R.string.copy_success_mall));
                }
                break;
            case R.id.tv_copy_id_merchant_info:
                //复制商户id
                if (!TextUtils.isEmpty(mMerchantId)){
                    AppHelper.copy2(mMerchantId,MerchantInfoActivity.this);
                    ToastUtil.showToastShort(getString(R.string.copy_success_mall));
                }
                break;
            case R.id.tv_copy_thirdMchId:
                //复制第三方商户id
                String thirdMchIdStr = tv_thirdMchId.getText().toString().trim();
                if (!TextUtils.isEmpty(thirdMchIdStr)) {
                    AppHelper.copy2(thirdMchIdStr, MerchantInfoActivity.this);
                    ToastUtil.showToastShort(getString(R.string.copy_success_mall));
                }
                break;
            default:
                break;
        }
    }

    private void showRefuseMarchatDialog() {
        final CustomViewCenterDialog customViewCenterDialog = new CustomViewCenterDialog(this);
        customViewCenterDialog.setView(R.layout.layout_dialog_merchant_info_refuse);
        customViewCenterDialog.findViewById(R.id.tv_cancel_merchant_info_refuse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //取消
                customViewCenterDialog.dismiss();
            }
        });
        customViewCenterDialog.findViewById(R.id.tv_sure_merchant_info_refuse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //确认拒绝
                customViewCenterDialog.dismiss();
                if (mData!=null){
                    confirmSureMarchat("2");
                }
            }
        });
        customViewCenterDialog.show();
    }

    /**
     *确认商户进件资料
     * @param confirmStatus   1 确认通过，2 拒绝
     */
    private void confirmSureMarchat(String confirmStatus) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mConfirmStatus = confirmStatus;
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", mMerchantId);
            map.put("confirmStatus",confirmStatus);
            map.put("updateVersion",mData.getUpdateVersion());
            ServerClient.newInstance(MyApplication.getContext()).confirmMerchant(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_CONFIRM, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    //查询商户剩余修改结算信息次数
    private void getSettleChangeCount() {
        if (isChangeInfo()){
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                DialogUtil.safeShowDialog(mLoadPayTypeDialog);
                Map<String, Object> map = new HashMap<>();
                map.put("merchantId", mMerchantId);
                ServerClient.newInstance(MyApplication.getContext()).getSettleChangeCount(MyApplication.getContext(), Constants.TAG_GET_SETTLE_CHANGE_COUNT, map);
            } else {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
            }
        }
    }
    //是否是变更审核
    private boolean isChangeInfo(){
        if (mData==null){
            return false;
        }
//        if (mData.getMchQueryStatus() == 3 || mData.getMchQueryStatus() == 4 || mData.getMchQueryStatus() == 100|| mData.getMchQueryStatus() == 10){
        if (mData.getMchQueryStatus() == 1 || mData.getMchQueryStatus() == 100|| mData.getMchQueryStatus() == 10){
            //case 3://变更审核中
            //case 4://变更失败
            //case 100://可交易
            //case 10://变更审核通过
            //case 1://审核通过
            return true;
        }
        return false;
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MERCHANT_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            MerchantInfoBean msg = (MerchantInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.GET_MERCHANT_INFO_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_MERCHANT_INFO_TRUE://请求成功
                    if (msg.getData() != null) {
                        mData = msg.getData();
                        if ("6".equals(type)
                                && (mData.getMchQueryStatus() == 3 || mData.getMchQueryStatus() == 4 || mData.getMchQueryStatus() == 10)
                                && !TextUtils.isEmpty(mData.getDataBeforeJson())) {
                            //TODO 赋值修改前
                        }
                        mMerchantShortName = mData.getMchDetail().getMerchantShortName();
                        setView(mData);
                        getSettleChangeCount();
                    }
                    break;
            }
            dismissLoading();
        }else if (event.getTag().equals(Constants.TAG_GET_MERCHANT_CONFIRM)) {
            //确认商户资料或拒绝
            DialogUtil.safeCloseDialog(mLoadDialog);
            ConfirmMerchantBean msg = (ConfirmMerchantBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.GET_MERCHANT_INFO_CONFIRM_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_MERCHANT_INFO_CONFIRM_TRUE://请求成功
                    ToastUtil.showToastShort("操作成功");
                    finish();
                    break;
            }
            dismissLoading();
        }else if (event.getTag().equals(Constants.TAG_GET_SETTLE_CHANGE_COUNT)){
            DialogUtil.safeCloseDialog(mLoadPayTypeDialog);
            SettleChangeCountBean msg = (SettleChangeCountBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.net_error));
                    break;
                case Constants.GET_SETTLE_CHANGE_COUNT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_SETTLE_CHANGE_COUNT_TRUE://请求成功
                    if (msg.getData() != null) {
                        mChangeAcountCount = msg.getData();
                        if (mChangeAcountCount<0 ||mChangeAcountCount==0){
                            mTvCountChangeSettle.setText("本月修改机会已用完");
                        }else {
                            mTvCountChangeSettle.setText("本月可修改1次");

                        }
                    }
                    break;
            }
        }else if (event.getTag().equals(Constants.TAG_WX_SIGN_DETAIL)) {
            //微信签约详情
            DialogUtil.safeCloseDialog(mLoadDialog);
            WxSignDetailBean msg = (WxSignDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    //ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.GET_MERCHANT_INFO_FALSE:
                    /*if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }*/
                    break;
                case Constants.GET_MERCHANT_INFO_TRUE://请求成功
                    mSignDetailData = msg.getData();
                    if (mSignDetailData !=null){
                        String statusCnt = mSignDetailData.getStatusCnt();
                        mTvStateWxAuthMerchant.setText(statusCnt==null?"":statusCnt);
                    }
                    break;
            }
        }
    }

    private void setView(MerchantInfoBean.DataBean mData) {
        if (mData != null) {
            MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();
            if (mchDetail != null && mchDetail.getPrincipal() != null) {
                tv_merchant_name.setText(mchDetail.getPrincipal() + "-" + mData.getMerchantName());
                mMerchantName =  mData.getMerchantName();
            }
            if (mData.getMerchantId() != null) {
                tv_merchand_num.setText("商户编号： " + mData.getMerchantId());
                mMerchantId = mData.getMerchantId();
            }
            if (mData.getWxRealNameStatus()==null){
                //未进行微信实名认证
                mRlWxRealNameMerchant.setVisibility(View.GONE);
            }else {
                mRlWxRealNameMerchant.setVisibility(View.VISIBLE);
                mRlWxRealNameMerchant.setOnClickListener(this);
                mTvStateWxAuthMerchant.setText(mData.getWxRealNameStatusCnt());
            }
            if (mData.getAliRealNameStatus()==null){
                //未进行支付宝实名认证
                mRlAliRealNameMerchant.setVisibility(View.GONE);
            }else {
                mRlAliRealNameMerchant.setVisibility(View.VISIBLE);
                mRlAliRealNameMerchant.setOnClickListener(this);
                mTvStateAliAuthMerchant.setText(mData.getAliRealNameStatusCnt());
            }
            //第三方商户号thirdMchId
            if (!StringUtils.isEmptyOrNull(mData.getThirdMchId())) {
                tv_thirdMchId.setText(mData.getThirdMchId());
                showThirdMchId(true);
            } else {
                showThirdMchId(false);
            }

            switch (mData.getTradeStatus()) {
                case 0:
                    mTvPayTypeStatus.setText(R.string.merchant_trade_unable);
                    break;
                case 10:
                    mTvPayTypeStatus.setText(R.string.merchant_trade_enable);
                    break;
                case 20:
                    mTvPayTypeStatus.setText(R.string.merchant_trade_enable_part);
                    break;
            }
            if (type != null && type.equals("6")) {
                //可交易
                iv_state.setImageResource(R.mipmap.icon_check_successful);
                tv_state.setText(R.string.notice_merchant_trade_enable);
                mRlPcSnManage.setVisibility(View.VISIBLE);
            }else {
                switch (mData.getMchQueryStatus()) {

                    case -1:
                        //初始商户
                        mRlPcSnManage.setVisibility(View.GONE);
                        iv_state.setImageResource(R.mipmap.checking_icon);
                        tv_state.setText(R.string.notice_merchant_no_info);
                        break;
                    case 0:
                        //审核中
                        mRlPcSnManage.setVisibility(View.GONE);
                        iv_state.setImageResource(R.mipmap.checking_icon);
                        tv_state.setText(R.string.notice_merchant_checking);
                        break;
                    case 1:
                        //审核通过
                        mRlPcSnManage.setVisibility(View.GONE);
                        if (StringUtils.isEmptyOrNull(mData.getExamineRemark())) {
                            iv_state.setImageResource(R.mipmap.check_pass_icon);
                            tv_state.setText(R.string.notice_merchant_platform_check_success);
                        } else {
                            iv_state.setImageResource(R.mipmap.check_fail_icon);
                            tv_state.setText(R.string.notice_merchant_check_failed);
                            tv_fail_info.setVisibility(View.VISIBLE);
                            tv_fail_info.setText(mData.getExamineRemark());
                        }
                        break;
                    case 2:
                        //审核不通过
                        mRlPcSnManage.setVisibility(View.GONE);
                        iv_state.setImageResource(R.mipmap.check_fail_icon);
                        tv_state.setText(R.string.notice_merchant_check_failed);
                        if (!StringUtils.isEmptyOrNull(mData.getExamineRemark())) {
                            tv_fail_info.setVisibility(View.VISIBLE);
                            tv_fail_info.setText(mData.getExamineRemark());
                        }
                        break;
                    case 3:
                        //变更审核中
                        mRlPcSnManage.setVisibility(View.VISIBLE);
                        iv_state.setImageResource(R.mipmap.checking_icon);
                        tv_state.setText(R.string.notice_merchant_change_checking);
                        break;
                    case 4:
                        //修改审核不通过
                        mRlPcSnManage.setVisibility(View.VISIBLE);
                        iv_state.setImageResource(R.mipmap.check_fail_icon);
                        tv_state.setText(R.string.notice_merchant_check_failed);
                        if (!StringUtils.isEmptyOrNull(mData.getExamineRemark())) {
                            tv_fail_info.setVisibility(View.VISIBLE);
                            tv_fail_info.setText(mData.getExamineRemark());
                        }
                        break;
                    case 5:
                        //作废商户
                        mRlPcSnManage.setVisibility(View.GONE);
                        iv_state.setImageResource(R.mipmap.icon_invalid);
                        tv_state.setText(R.string.notice_merchant_freeze);
                        if (!StringUtils.isEmptyOrNull(mData.getExamineRemark())) {
                            tv_fail_info.setVisibility(View.VISIBLE);
                            tv_fail_info.setText(mData.getExamineRemark());
                        }
                        break;
                    case 6:
                        //待确认
                        mRlPcSnManage.setVisibility(View.GONE);
                        iv_state.setImageResource(R.mipmap.checking_icon);
                        tv_state.setText(R.string.notice_merchant_not_confirm);
                        mLlNotConfirmMerchantInfo.setVisibility(View.VISIBLE);
                        rl_bind_code_info.setVisibility(View.GONE);
                        mRlZfbSqMerchant.setVisibility(View.GONE);
                        mRlWxRealNameMerchant.setVisibility(View.GONE);
                        mRlAliRealNameMerchant.setVisibility(View.GONE);
                        break;
                    case 10:
                        //变更审核通过
                        mRlPcSnManage.setVisibility(View.VISIBLE);
                        if (StringUtils.isEmptyOrNull(mData.getExamineRemark())) {
                            iv_state.setImageResource(R.mipmap.check_pass_icon);
                            tv_state.setText(R.string.notice_merchant_platform_check_success);
                        } else {
                            iv_state.setImageResource(R.mipmap.check_fail_icon);
                            tv_state.setText(R.string.notice_merchant_check_failed);
                            tv_fail_info.setVisibility(View.VISIBLE);
                            tv_fail_info.setText(mData.getExamineRemark());
                        }
                        break;
                    case 100:
                        //可交易
                        mRlPcSnManage.setVisibility(View.VISIBLE);
                        iv_state.setImageResource(R.mipmap.icon_check_successful);
                        tv_state.setText(R.string.notice_merchant_trade_enable);
                        break;
                    case 101:
                        //冻结商户
                        mRlPcSnManage.setVisibility(View.GONE);
                        iv_state.setImageResource(R.mipmap.freeze_icon);
                        if (!StringUtils.isEmptyOrNull(mData.getExamineRemark())) {
                            String s = getString(R.string.merchant_freeze_notice) + mData.getExamineRemark();
                            SpannableString span = new SpannableString(s);
                            span.setSpan(new ForegroundColorSpan(UIUtils.getColor(R.color.color_fd414b)), 30, s.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                            tv_state.setText(span);
                        } else {
                            tv_state.setText(R.string.merchant_froze_notice);
                        }
                        break;

                }

            }
            if (Constants.KINGDEER.equals(AppHelper.getAppMetaData(MyApplication.getContext(),Constants.APP_META_DATA_KEY))) {
                mRlPcSnManage.setVisibility(View.GONE);
            }
            if (mData.getMchQueryStatus() ==3) {
                mTvNotice.setVisibility(View.VISIBLE);
            } else {
                mTvNotice.setVisibility(View.GONE);
            }
        }

    }

    public void mtaProperties(Activity activity, String number, String key, String value) {
        Properties prop = new Properties();
        prop.setProperty(key, value);
        StatService.trackCustomKVEvent(activity, number, prop);
    }

    private void call(String phone) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (ActivityNotFoundException a) {
            a.getMessage();
        }
    }

   /* private void setCardEntity(CardDetailBean.DataEntity cardEntity, MerchantInfoBean.DataBean.BankAccountBean data) {
        cardEntity.setAccountType(data.getAccountType());
        cardEntity.setAccountCode(data.getAccountCode());
        cardEntity.setAccountName(data.getAccountName());
        cardEntity.setBankBranchId(data.getBankBranchId());
        cardEntity.setBankBranchIdCnt(data.getBankBranchIdCnt());
        cardEntity.setBankId(data.getBankId());
        cardEntity.setBankIdCnt(data.getBankIdCnt());
        cardEntity.setBkCardPhoto(data.getBkCardPhoto());
        cardEntity.setBkLicensePhoto(data.getBkLicensePhoto());
        cardEntity.setCity(data.getBcity());
        cardEntity.setCityCnt(data.getBcityCnt());
        cardEntity.setProvince(data.getBprovince());
        cardEntity.setProvinceCnt(data.getBprovinceCnt());
        cardEntity.setContactLine(data.getContactLine());
        cardEntity.setIdCard(data.getIdCard());
        cardEntity.setMerchantClass(data.getMerchantClass());
        cardEntity.setCardType(1);
        cardEntity.setIdCode(data.getIdCode());
        cardEntity.setMerchantExamineStatus(data.getExamineStatus());
        cardEntity.setActivateStatus(data.getActivateStatus());
        cardEntity.setTelphone(data.getTelphone());
        cardEntity.setMerchantId(data.getMerchantId());
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_MERCHANT_CARD){
            getSettleChangeCount();
        }else if (requestCode == REQUEST_CODE_WX_SIGN_DETAIL && resultCode == RESULT_OK){
            //微信签约详情返回
            getData(mMerchantId);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE1:
                LogUtil.d("PermissionRequest" + PermissionUtils.verifyPermissions(grantResults));
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    // Permission Granted
                } else {
                    showDialog();
                }
                break;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private SelectDialog mDialog;

    private void showDialog() {
        if (mDialog == null) {
            mDialog = new SelectDialog(this, UIUtils.getString(R.string.dialog_notice_cameras)
                    , UIUtils.getString(R.string.btn_setting), UIUtils.getString(R.string.btn_cancel_text),
                    new SelectDialog.HandleBtn() {
                        @Override
                        public void handleOkBtn() {
                            Intent intent = getAppDetailSettingIntent(MerchantInfoActivity.this);
                            startActivity(intent);
                        }

                        @Override
                        public void handleCancleBtn() {
                        }
                    });
        }
        DialogHelper.resize(this, mDialog);
        mDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MyApplication.isRefresh) {
            MyApplication.isRefresh = false;
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                Map<String, Object> map = new HashMap<>();
                map.put("merchantId", mMerchantId);
                ServerClient.newInstance(MyApplication.getContext()).getMerchantInfo(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_INFO, map);
            }
        }
    }


    public <T> T toBean(String arg0, Class clazz) {
        return (T) new Gson().fromJson(arg0, clazz);
    }
}
