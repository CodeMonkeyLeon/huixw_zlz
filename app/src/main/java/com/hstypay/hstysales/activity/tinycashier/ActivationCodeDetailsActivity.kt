package com.hstypay.hstysales.activity.tinycashier

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.text.TextUtils
import android.view.View

import com.hstypay.hstysales.R
import com.hstypay.hstysales.activity.ChangeNameActivity
import com.hstypay.hstysales.bean.ActivationCode
import com.hstypay.hstysales.bean.CashierItem
import com.hstypay.hstysales.commonlib.base.AppActivity
import com.hstypay.hstysales.commonlib.widget.ActivationCodeHelpDialog
import com.hstypay.hstysales.utils.Constants
import com.hstypay.hstysales.viewmodel.ActivationCodeDetailsViewModel
import kotlinx.android.synthetic.main.activity_activation_code_details.*
import kotlinx.android.synthetic.main.title_arrow.*

/**
 * @Author dean.zeng
 * @Description PC收银激活码详情
 * @Date 2020-07-17 19:13
 **/
class ActivationCodeDetailsActivity : AppActivity<ActivationCodeDetailsViewModel>() {


    companion object {
        const val KEY_ACTIVATION_CODE = "key_activation_code"
        const val KEY_UPDATE_SHORT_NAME = "key_update_short_name"
        const val REQUEST_CASHIER_BEAN_CODE = 0x11//收银员
        const val REQUEST_STORE_BEAN_CODE = 0x12//门店简称
    }


    private lateinit var activationCode: ActivationCode

    override fun initData() {
        iv_back.setOnClickListener { finish() }
        tv_title.text = "PC收银激活码详情"
        iv_title_button.setImageResource(R.mipmap.question_circle)
        iv_title_button.imageTintList = ColorStateList.valueOf(Color.parseColor("#FFFFFF"))
        iv_title_button.visibility = View.VISIBLE
        iv_title_button.setOnClickListener {
            ActivationCodeHelpDialog(this).show()
        }

        llMerchantShortName.setOnClickListener {//收银员
            startActivityForResult(Intent(this, ChangeNameActivity::class.java)
                    .putExtra(KEY_UPDATE_SHORT_NAME, true)
                    .putExtra(Constants.INTENT_CHANGE_NAME, tvStoreMerchantShortName.text.toString())
                    , REQUEST_STORE_BEAN_CODE)
        }
        llUserName.setOnClickListener {//门店
            val storeId = activationCode.storeMerchantId
            mViewModel.getCashierList(storeId)
        }
        activationCode = intent.getSerializableExtra(KEY_ACTIVATION_CODE) as ActivationCode
        mViewModel.getActivationCodeDetails(activationCode.id)
    }

    override fun initView() {
        mViewModel.activationCode.observe(this, Observer {
            it?.let {
                activationCode = it
                tvActivationCode.text = it.activationCode
                tvActivationStatus.text = if (it.activationStatus == 0) "未激活" else "已激活"
                tvMerchantShortName.text = it.merchantShortName
                tvStoreMerchantName.text = it.storeMerchantName
                tvStoreMerchantShortName.text = it.storeMerchantShortName
                tvSn.text = it.sn
                tvUserName.text = if (TextUtils.isEmpty(it.userName)) "" else it.userName
            }
        })
        mViewModel.cashierList.observe(this, Observer {
            if (!it.isNullOrEmpty()) {
                val intent = Intent(this, CashierChoiceActivity::class.java)
                intent.putExtra(Constants.INTENT_DEVICE_EMPLOYEE_LIST, it)
                intent.putExtra(KEY_ACTIVATION_CODE, activationCode)
                startActivityForResult(intent, REQUEST_CASHIER_BEAN_CODE)
            } else {
                showModal("无数据")
            }
        })
        mViewModel.upData.observe(this, Observer {
            showModal("修改成功")
            tvStoreMerchantShortName.text = it
        })
    }

    override fun getLayoutId(): Int = R.layout.activity_activation_code_details


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_STORE_BEAN_CODE && data != null) {//门店简称
            val extras = data.extras
            if (extras != null) {
                val name = extras.getString(Constants.RESULT_CHANGE_NAME)

                mViewModel.activationCodeUpdate(activationCode.id, name)
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CASHIER_BEAN_CODE && data != null) {
            val dataBean = data.getSerializableExtra(Constants.RESULT_DEVICE_EMPLOYEE_LIST) as CashierItem
            activationCode.userId = dataBean.userId
            activationCode.userName = dataBean.name
            tvUserName.text = dataBean.name
        }
    }


}