package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.adapter.AppOpenFilterAdapter;
import com.hstypay.hstysales.adapter.AppOpenManagerAdapter;
import com.hstypay.hstysales.adapter.MerchantRecyclerAdapter;
import com.hstypay.hstysales.adapter.SalesmanAdapter;
import com.hstypay.hstysales.adapter.ServiceProviderAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.AppOpenFilterBean;
import com.hstypay.hstysales.bean.ExamineListBean;
import com.hstypay.hstysales.bean.SalesmanBean;
import com.hstypay.hstysales.bean.ServiceProviderBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CustomMatchViewPopupWindow;
import com.hstypay.hstysales.widget.LinearSpaceItemDecoration;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;
import com.ykk.dropdownmenu.DropDownMenu;
import com.ykk.dropdownmenu.TabBean;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Desc: 商户应用开通管理页面
 *
 * @author kuangzeyu
 * @date 2020/12/10
 **/
public class AppOpenManagerActivity extends BaseActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_GO_DETAIL = 100;
    private static final int REQUEST_CODE_GO_SEARCH = 101;
    private ImageView mIvTitleButton;//搜索
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRvAppOpen;//应用开通列表
    private RelativeLayout mLayoutDataEmpty;//空视图
    private AppOpenManagerAdapter mAppOpenManagerAdapter;//应用开通列表adapter
    public static final String ID_TYPE_ALL = "";//所有类型
    public static final String ID_TYPE_HB = "ALIPAY-ISTALLMENT";//花呗分期类型
    public static final String ID_TYPE_YJ = "PREAUTH-PAY";//预授权押金类型
    public static final String ID_STATUS_ALL = "";//所有状态
    public static final String ID_STATUS_PASS = "30";//审核通过状态
    public static final String ID_STATUS_FAILED = "50";//审核不通过状态
    public static final String ID_STATUS_WAIT = "20";//待审核状态
    private List<AppOpenFilterBean> allTypes = new ArrayList<>();//类型筛选集合数据
    private List<AppOpenFilterBean> allStatus = new ArrayList<>();//状态筛选集合数据
    private AppOpenFilterAdapter mTypeFilterAdapter;//类型筛选的adapter
    private AppOpenFilterAdapter mStatusFilterAdapter;//状态筛选的adapter
    private CustomMatchViewPopupWindow mFilterTypePop;//类型筛选弹窗
    private CustomMatchViewPopupWindow mFilterStatusPop;//状态筛选弹窗
    private SafeDialog mLoadDialog;
    private String mAppCode;//当前选择的应用类型code
    private String mOrderStatus;//当前选择的审核状态
    private int mPageSize = 15;
    private int mCurrentPage = 1;
    private int mSearchPageSize = 15;
    private int mSearchCurrentPage = 1;
    private boolean isLoadmore;//加载更多标识
    private boolean isPullRefresh;//下拉刷新标识
    private List<ExamineListBean.ExamineBean> mExamineBeans = new ArrayList<>();
    private String mCurSelectServiceProviderId;
    private String mCurSelectSalesmanId;
    private List<ServiceProviderBean.DataEntity.ServiceProvider> mSearchServiceProviderList;
    private List<SalesmanBean.DataEntity.Salesman> mSearchSalesmanList;
    private ArrayList<TabBean> headers;
    private DropDownMenu mDropDownMenu;
    private boolean isLoadMerchantMore;
    private boolean isLoadOver;
    private int mSelectLabelPosition;
    private ServiceProviderAdapter mServiceProviderAdapter;
    private SalesmanAdapter mSalesmanAdapter;
    private EditText mEtServiceInput;
    private ImageView mIvServiceClean;
    private View mServiceDataEmpty;
    private EditText mEtSalesmanInput;
    private ImageView mIvSalesmanClean;
    private View mSalesmanDataEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_open_manager);
        StatusBarUtil.setTranslucentStatus(this);
        initView();
        initData();
        initEvent();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        TextView tv_title = findViewById(R.id.tv_title);
        tv_title.setText(getResources().getString(R.string.user_app_open_manager));
        mIvTitleButton = findViewById(R.id.iv_title_button);
        mIvTitleButton.setVisibility(View.VISIBLE);
        mIvTitleButton.setImageResource(R.mipmap.icon_general_top_search);

        mLayoutDataEmpty = findViewById(R.id.ll_empty_no_data);
        mDropDownMenu = findViewById(R.id.dropDownMenu);
    }

    private void initEvent() {
        findViewById(R.id.iv_back).setOnClickListener(this);
        mIvTitleButton.setOnClickListener(this);
    }

    private void initData() {
        getAllTypes();
        getAllStatus();

        initDropDownMenu();
        getAppExamineList();

        if (Constants.INTENT_NAME_MSG_SKIP.equals(getIntent().getStringExtra(Constants.INTENT_NAME))) {
            Intent intent = new Intent(AppOpenManagerActivity.this, AppOpenDetailActivity.class);
            intent.putExtra(Constants.INTENT_EXAMINE_BILLNO, getIntent().getStringExtra(Constants.INTENT_EXAMINE_BILLNO));
            startActivityForResult(intent, REQUEST_CODE_GO_DETAIL);
        }
    }

    private void initDropDownMenu() {
        mCurSelectServiceProviderId = "";
        mCurSelectSalesmanId = "";
        mAppCode = ID_TYPE_ALL;
        mOrderStatus = ID_STATUS_ALL;
        mSearchServiceProviderList = new ArrayList<>();
        mSearchSalesmanList = new ArrayList<>();

        View contentView = getLayoutInflater().inflate(R.layout.layout_content_app_open_manager, null);
        mSwipeRefreshLayout = contentView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    mCurrentPage = 1;
                    isPullRefresh = true;
                    getAppExamineList();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    //mCurrentPage++;//要在获取数据成功后才能++，否则失败了又得--回去
                    getAppExamineList();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });
        mRvAppOpen = contentView.findViewById(R.id.rv_app_open);
        mRvAppOpen.setLayoutManager(new LinearLayoutManager(this));
        mRvAppOpen.addItemDecoration(new LinearSpaceItemDecoration(DensityUtils.dip2px(this, 15), true));
        mAppOpenManagerAdapter = new AppOpenManagerAdapter(this);
        mAppOpenManagerAdapter.setOnItemClickListener(new AppOpenManagerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ExamineListBean.ExamineBean examineItemBean) {
                Intent intent = new Intent(AppOpenManagerActivity.this, AppOpenDetailActivity.class);
                intent.putExtra(Constants.INTENT_EXAMINE_APPCODE, examineItemBean.getAppCode());
                intent.putExtra(Constants.INTENT_EXAMINE_BILLNO, examineItemBean.getBillNo());
                startActivityForResult(intent, REQUEST_CODE_GO_DETAIL);
            }

        });
        mRvAppOpen.setAdapter(mAppOpenManagerAdapter);

        mLayoutDataEmpty = contentView.findViewById(R.id.layout_data_empty);
        mSwipeRefreshLayout.setVisibility(View.GONE);

        headers = new ArrayList<>();
        TabBean tabBean1 = new TabBean();
        tabBean1.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean1.setLabel("type");
        tabBean1.setTabTitle("全部类型");
        TabBean tabBean2 = new TabBean();
        tabBean2.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean2.setLabel("cityService");
        tabBean2.setTabTitle("全部市运营");
        TabBean tabBean3 = new TabBean();
        tabBean3.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean3.setLabel("cityManager");
        tabBean3.setTabTitle("全部城市经理");
        TabBean tabBean4 = new TabBean();
        tabBean4.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean4.setLabel("status");
        tabBean4.setTabTitle("全部状态");
        headers.add(tabBean1);
        headers.add(tabBean4);
        if ("SUPERADMIN".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_ROLE_CODE))) {
            if ("".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean2);
                headers.add(tabBean3);
            } else if ("SC".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean3);
            }
        }
        mDropDownMenu.setDropDownMenu(headers, initViewData(), contentView);
        mDropDownMenu.setOnTabClickListener(new DropDownMenu.OnTabClickListener() {
            @Override
            public void onTabClick(int position, boolean isOpen) {
                mSelectLabelPosition = position;
                switch (headers.get(position).getLabel()) {
                    case "cityService":
                        if (isOpen)
                            getServiceList(false);
                        break;
                    case "cityManager":
                        if (isOpen)
                            getSalesmanList(false);
                        break;
                }
            }
        });
    }

    /**
     * 设置类型和数据源：
     * DropDownMenu.KEY对应类型（DropDownMenu中的常量，参考上述核心源码）
     * DropDownMenu.VALUE对应数据源：key不是TYPE_CUSTOM则传递string[],key是TYPE_CUSTOM类型则传递对应view
     */
    private List<HashMap<String, Object>> initViewData() {
        List<HashMap<String, Object>> viewDatas = new ArrayList<>();
        HashMap<String, Object> map;
        for (int i = 0; i < headers.size(); i++) {
            map = new HashMap<String, Object>();
            map.put(DropDownMenu.KEY, headers.get(i).getType());
            map.put(DropDownMenu.VALUE, getCustomView(headers.get(i).getLabel()));
            viewDatas.add(map);
        }
        return viewDatas;
    }

    private View getCustomView(String label) {
        View view;

        view = getLayoutInflater().inflate(R.layout.layout_dropdown, null);
        FrameLayout flSearch = view.findViewById(R.id.fl_search);
        switch (label) {
            case "cityService":
                mEtServiceInput = view.findViewById(R.id.et_input);
                mIvServiceClean = view.findViewById(R.id.iv_clean);
                mServiceDataEmpty = view.findViewById(R.id.common_data_empty);
                RecyclerView rvService = view.findViewById(R.id.recycler_view);
                LinearLayoutManager serviceLinearLayoutManager = new LinearLayoutManager(this);
                rvService.setLayoutManager(serviceLinearLayoutManager);
                mServiceProviderAdapter = new ServiceProviderAdapter(AppOpenManagerActivity.this);
                mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                mServiceProviderAdapter.setOnItemClickListener(new ServiceProviderAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mCurSelectServiceProviderId != null && !mCurSelectServiceProviderId.equals(mSearchServiceProviderList.get(position).getServiceProviderId())) {
                            mCurSelectSalesmanId = "";
                            for (int i = 0; i < headers.size(); i++) {
                                if ("cityManager".equals(headers.get(i).getLabel()))
                                    mDropDownMenu.setTabText(i, "全部城市经理");
                            }
                            mCurSelectServiceProviderId = mSearchServiceProviderList.get(position).getServiceProviderId();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mDropDownMenu.setTabText(mSelectLabelPosition, mSearchServiceProviderList.get(position).getServiceProviderName());
                            mCurrentPage = 1;
                            getAppExamineList();
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvService.setAdapter(mServiceProviderAdapter);
                rvService.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    //这个int用来记录最后一个可见的view
                    int lastVisibleItemPosition;
                    int refreshCurrentPage;//防止重复加载

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mServiceProviderAdapter.getItemCount()) {
                            refreshCurrentPage = mSearchCurrentPage;
                            if (!isLoadOver)
                                getServiceList(true);
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                        lastVisibleItemPosition = serviceLinearLayoutManager.findLastVisibleItemPosition();
                    }
                });
                mEtServiceInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            getServiceList(false);
                            return true;
                        }
                        return false;
                    }
                });
                mEtServiceInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            getServiceList(false);
                        }
                        mIvServiceClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                mIvServiceClean.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEtServiceInput.setText("");
                    }
                });
                break;
            case "cityManager":
                mEtSalesmanInput = view.findViewById(R.id.et_input);
                mIvSalesmanClean = view.findViewById(R.id.iv_clean);
                mSalesmanDataEmpty = view.findViewById(R.id.common_data_empty);
                RecyclerView rvSalesman = view.findViewById(R.id.recycler_view);
                LinearLayoutManager salesmanLinearLayoutManager = new LinearLayoutManager(this);
                rvSalesman.setLayoutManager(salesmanLinearLayoutManager);
                mSalesmanAdapter = new SalesmanAdapter(AppOpenManagerActivity.this);
                mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                mSalesmanAdapter.setOnItemClickListener(new SalesmanAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mCurSelectSalesmanId != null && !mCurSelectSalesmanId.equals(mSearchSalesmanList.get(position).getEmpId())) {
                            mCurSelectSalesmanId = mSearchSalesmanList.get(position).getEmpId();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mDropDownMenu.setTabText(mSelectLabelPosition, mSearchSalesmanList.get(position).getEmpName());
                            mCurrentPage = 1;
                            getAppExamineList();
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvSalesman.setAdapter(mSalesmanAdapter);
                rvSalesman.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    //这个int用来记录最后一个可见的view
                    int lastVisibleItemPosition;
                    int refreshCurrentPage;//防止重复加载

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mSalesmanAdapter.getItemCount()) {
                            LogUtil.d("refreshCurrentPage="+refreshCurrentPage+"---mSearchCurrentPage="+mSearchCurrentPage);
                            refreshCurrentPage = mSearchCurrentPage;
                            if (!isLoadOver)
                                getSalesmanList(true);
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                        lastVisibleItemPosition = salesmanLinearLayoutManager.findLastVisibleItemPosition();
                    }
                });
                mEtSalesmanInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            getSalesmanList(false);
                            return true;
                        }
                        return false;
                    }
                });
                mEtSalesmanInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            getSalesmanList(false);
                        }
                        mIvSalesmanClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                mIvSalesmanClean.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEtSalesmanInput.setText("");
                    }
                });
                break;
            case "type":
                flSearch.setVisibility(View.GONE);
                RecyclerView rvType = view.findViewById(R.id.recycler_view);
                LinearLayoutManager typeLinearLayoutManager = new LinearLayoutManager(this);
                rvType.setLayoutManager(typeLinearLayoutManager);
                mTypeFilterAdapter = new AppOpenFilterAdapter(this);
                mTypeFilterAdapter.setData(allTypes, mAppCode);
                mTypeFilterAdapter.setOnFilterItemClickListener(new AppOpenFilterAdapter.OnFilterItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mAppCode != null && !mAppCode.equals(allTypes.get(position).getId())) {
                            mAppCode = allTypes.get(position).getId();
                            mTypeFilterAdapter.setData(allTypes, mAppCode);
                            mDropDownMenu.setTabText(mSelectLabelPosition, allTypes.get(position).getName());
                            mCurrentPage = 1;
                            getAppExamineList();
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvType.setAdapter(mTypeFilterAdapter);
                break;
            case "status":
                flSearch.setVisibility(View.GONE);
                RecyclerView rvStatus = view.findViewById(R.id.recycler_view);
                LinearLayoutManager rvStatusLinearLayoutManager = new LinearLayoutManager(this);
                rvStatus.setLayoutManager(rvStatusLinearLayoutManager);
                mStatusFilterAdapter = new AppOpenFilterAdapter(this);
                mStatusFilterAdapter.setData(allStatus, mOrderStatus);
                mStatusFilterAdapter.setOnFilterItemClickListener(new AppOpenFilterAdapter.OnFilterItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mOrderStatus != null && !mOrderStatus.equals(allStatus.get(position).getId())) {
                            mOrderStatus = allStatus.get(position).getId();
                            mStatusFilterAdapter.setData(allStatus, mOrderStatus);
                            mDropDownMenu.setTabText(mSelectLabelPosition, allStatus.get(position).getName());
                            mCurrentPage = 1;
                            getAppExamineList();
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvStatus.setAdapter(mStatusFilterAdapter);
                break;
        }
        return view;
    }

    /**
     * 查询应用审核列表
     */
    private void getAppExamineList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!(isPullRefresh || isLoadmore)) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            if (!TextUtils.isEmpty(mAppCode)) {
                map.put("appCode", mAppCode);
            }
            if (!TextUtils.isEmpty(mOrderStatus)) {
                map.put("orderStatus", mOrderStatus);
            }
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mCurSelectSalesmanId))
                map.put("salesmanId", mCurSelectSalesmanId);
            map.put("pageSize", mPageSize);
            map.put("currentPage", mCurrentPage);
            ServerClient.newInstance(MyApplication.getContext()).getAppExamineList(MyApplication.getContext(), Constants.TAG_GET_APP_EXAMINE_LIST, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getServiceList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!isLoadMore) {
                DialogUtil.safeShowDialog(mLoadDialog);
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                map.put("serviceProviderName", mEtServiceInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getServiceList(MyApplication.getContext(), "TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getSalesmanList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!isLoadMore) {
                DialogUtil.safeShowDialog(mLoadDialog);
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                map.put("empName", mEtSalesmanInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getSalesmanList(MyApplication.getContext(), "TAG_SALESMAN_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetAppExamineList(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_APP_EXAMINE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            setSwipeRefreshState(isPullRefresh, isLoadmore, 500);
            ExamineListBean msg = (ExamineListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_APP_EXAMINE_LIST_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(AppOpenManagerActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_APP_EXAMINE_LIST_TRUE:
                    if (msg != null && msg.getData() != null) {
                        List<ExamineListBean.ExamineBean> examineBeans = msg.getData().getData();
                        if (examineBeans != null && examineBeans.size() > 0) {
                            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                            mLayoutDataEmpty.setVisibility(View.GONE);
                            if (mCurrentPage == 1) {
                                mExamineBeans.clear();
                            }
                            mExamineBeans.addAll(examineBeans);
                            mAppOpenManagerAdapter.setData(mExamineBeans);
                            mCurrentPage++;
                        } else {
                            if (isLoadmore) {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                            } else {
                                mExamineBeans.clear();
                                mAppOpenManagerAdapter.setData(mExamineBeans);
                                mSwipeRefreshLayout.setVisibility(View.GONE);
                                mLayoutDataEmpty.setVisibility(View.VISIBLE);
                            }
                        }

                    } else {
                        if (isLoadmore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mExamineBeans.clear();
                            mAppOpenManagerAdapter.setData(mExamineBeans);
                            mSwipeRefreshLayout.setVisibility(View.GONE);
                            mLayoutDataEmpty.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isPullRefresh = false;
        } else if (event.getTag().equals("TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ServiceProviderBean msg = (ServiceProviderBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(AppOpenManagerActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mServiceDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchServiceProviderList.clear();
                            if (TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                                ServiceProviderBean.DataEntity.ServiceProvider allServiceProvider = new ServiceProviderBean.DataEntity.ServiceProvider();//"全部商户"
                                allServiceProvider.setServiceProviderName("全部市运营");
                                allServiceProvider.setServiceProviderId("");
                                mSearchServiceProviderList.add(allServiceProvider);
                            }
                        }

                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchServiceProviderList.addAll(msg.getData().getData());
                        mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchServiceProviderList.clear();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mServiceDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mServiceDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_SALESMAN_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SalesmanBean msg = (SalesmanBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(AppOpenManagerActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mSalesmanDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchSalesmanList.clear();
                            if (TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                                SalesmanBean.DataEntity.Salesman allSalesman = new SalesmanBean.DataEntity.Salesman();//"全部商户"
                                allSalesman.setEmpName("全部城市经理");
                                allSalesman.setEmpId("");
                                mSearchSalesmanList.add(allSalesman);
                            }
                        }
                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchSalesmanList.addAll(msg.getData().getData());
                        mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchSalesmanList.clear();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mSalesmanDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mSalesmanDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        }
    }

    /**
     * 关闭下拉刷新和上拉加载的进度条
     */
    private void setSwipeRefreshState(boolean isPullRefresh, boolean isLoadmore, int delay) {
        if (isPullRefresh) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    /**
     * 获取所有可筛选的审核状态
     */
    private void getAllStatus() {
        AppOpenFilterBean checkAllStatusBean = new AppOpenFilterBean(ID_STATUS_ALL, getResources().getString(R.string.filter_all_status));
        AppOpenFilterBean checkPassStatusBean = new AppOpenFilterBean(ID_STATUS_PASS, getResources().getString(R.string.filter_check_pass));
        AppOpenFilterBean checkUnPassStatusBean = new AppOpenFilterBean(ID_STATUS_FAILED, getResources().getString(R.string.filter_check_unpass));
        AppOpenFilterBean checkWaitStatusBean = new AppOpenFilterBean(ID_STATUS_WAIT, getResources().getString(R.string.filter_check_wait));
        allStatus.add(checkAllStatusBean);
        allStatus.add(checkPassStatusBean);
        allStatus.add(checkUnPassStatusBean);
        allStatus.add(checkWaitStatusBean);
    }

    /**
     * 获取所有可筛选的应用类型
     */
    private void getAllTypes() {
        AppOpenFilterBean allTypeBean = new AppOpenFilterBean(ID_TYPE_ALL, getResources().getString(R.string.filter_all_type));
        AppOpenFilterBean huaBeiTypeBean = new AppOpenFilterBean(ID_TYPE_HB, getResources().getString(R.string.filter_hb));
        AppOpenFilterBean YajinTypeBean = new AppOpenFilterBean(ID_TYPE_YJ, getResources().getString(R.string.filter_yj));
        allTypes.add(allTypeBean);
        allTypes.add(huaBeiTypeBean);
        allTypes.add(YajinTypeBean);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_title_button:
                //搜索
                startActivityForResult(new Intent(AppOpenManagerActivity.this, AppOpenSearchActivity.class), REQUEST_CODE_GO_SEARCH);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GO_DETAIL || requestCode == REQUEST_CODE_GO_SEARCH) {
            if (resultCode == RESULT_OK) {
                mCurrentPage = 1;
                getAppExamineList();
            }
        }
    }
}