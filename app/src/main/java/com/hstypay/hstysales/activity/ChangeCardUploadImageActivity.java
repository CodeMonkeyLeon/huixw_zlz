package com.hstypay.hstysales.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hstypay.hstysales.BuildConfig;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.PictureBean;
import com.hstypay.hstysales.bean.UploadImageBean;
import com.hstypay.hstysales.fragment.PrivateFragment;
import com.hstypay.hstysales.fragment.PublicFragment;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.ClickUtil;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.FileUtils;
import com.hstypay.hstysales.utils.ImageFactory;
import com.hstypay.hstysales.utils.ImagePase;
import com.hstypay.hstysales.utils.ImageUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CommonNoticeDialog;
import com.hstypay.hstysales.widget.CustomViewBottomDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.SelectDialog;
import com.shehuan.niv.NiceImageView;
import com.squareup.picasso.Picasso;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import okhttp3.Call;

/**
 * @author zeyu.kuang
 * @time 2020/11/2
 * @desc 变更结算卡信息上传申请函和授权函图片
 */
public class ChangeCardUploadImageActivity extends BaseActivity implements View.OnClickListener {

    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;//从相册选图片code
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;//调用拍照功能code
    private LinearLayout mRlUploadBgsqCard;
    private NiceImageView mNivPicBgsqCard;//变更申请函
    private ImageView mIvIconCameraBgsqCard;
    private LinearLayout mRlUploadSfsqCard;
    private NiceImageView mNivPicSfsqCard;//第三方授权函
    private ImageView mIvIconCameraSfsqCard;
    private Button mBtnSubmit;
    private TextView mTvTitle;
    private ImageView mIvBack;
    public SafeDialog mLoadDialog;
    private PictureBean mPic8;//变更申请函
    private PictureBean mPic9; //第三方授权函
    private PictureBean mSelectPictureBean;


    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE2 = 102;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE3 = 103;

    private String path_bgsq;//变更申请函图片本地路径
    private String path_sfsq;//第三方授权函图片本地路径
    private String mLicense_bgsq;//接口返回的图片url
    private String mLicense_sfsq;//接口返回的图片url
    private int mIntentFrom;
    private boolean mIsToUploadSfsq;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_card_upload_image);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initEvent() {
        mIvBack.setOnClickListener(this);
        mRlUploadBgsqCard.setOnClickListener(this);
        mRlUploadSfsqCard.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);

    }

    private void initData() {
        //变更申请函
        mPic8 = new PictureBean();
        mPic8.setPicId(8);
        mPic8.setPicName("变更申请函");
        //第三方授权函
        mPic9 = new PictureBean();
        mPic9.setPicId(9);
        mPic9.setPicName("第三方授权函");

        Intent intent = getIntent();
        mIntentFrom = intent.getIntExtra(Constants.INTENT_NAME_FROM,0);
        if (PrivateFragment.FROM_PRIAVE_FRAGEMENT == mIntentFrom){
            //是同名结算卡变更
            mRlUploadBgsqCard.setVisibility(View.VISIBLE);
            mRlUploadSfsqCard.setVisibility(View.GONE);
            mLicense_bgsq = intent.getStringExtra(Constants.INTENT_NAME_PIC_BGSQ_NET);
            if (!TextUtils.isEmpty(mLicense_bgsq)){
                String imageUrl = Constants.BASE_URL + mLicense_bgsq;
//                Picasso.get().load(imageUrl).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(mNivPicBgsqCard);
            }
        }else if (PublicFragment.FROM_PUBLIC_FRAGEMENT == mIntentFrom){
            mRlUploadBgsqCard.setVisibility(View.VISIBLE);
            mRlUploadSfsqCard.setVisibility(View.VISIBLE);
            mLicense_bgsq = intent.getStringExtra(Constants.INTENT_NAME_PIC_BGSQ_NET);
            mLicense_sfsq = intent.getStringExtra(Constants.INTENT_NAME_PIC_SFSQ_NET);
            if (!TextUtils.isEmpty(mLicense_bgsq) && !TextUtils.isEmpty(mLicense_sfsq)){
                String imageUrl_bgsq = Constants.BASE_URL + mLicense_bgsq;
                String imageUrl_sfsq = Constants.BASE_URL + mLicense_sfsq;
//                Picasso.get().load(imageUrl_bgsq).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(mNivPicBgsqCard);
//                Picasso.get().load(imageUrl_sfsq).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(mNivPicSfsqCard);
            }
        }

    }

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mRlUploadBgsqCard = findViewById(R.id.ll_upload_bgsq_card);
        mNivPicBgsqCard = findViewById(R.id.niv_pic_bgsq_card);
        mIvIconCameraBgsqCard = findViewById(R.id.iv_icon_camera_bgsq_card);
        mRlUploadSfsqCard = findViewById(R.id.ll_upload_sfsq_card);
        mNivPicSfsqCard = findViewById(R.id.niv_pic_sfsq_card);
        mIvIconCameraSfsqCard = findViewById(R.id.iv_icon_camera_sfsq_card);
        mBtnSubmit = findViewById(R.id.btn_submit);
//        setButtonEnable(mBtnSubmit,true);

        mTvTitle.setText(getResources().getString(R.string.title_upload_pic));
        TextView tv_pic_title = findViewById(R.id.tv_pic_title);
        TextView tv_pic_title_auto = findViewById(R.id.tv_pic_title_auto);
        setTextLastCharRed(tv_pic_title);
        setTextLastCharRed(tv_pic_title_auto);

    }
    private void setTextLastCharRed(TextView tv){
        String content = tv.getText().toString();
        SpannableString spantwo = new SpannableString(content);
        spantwo.setSpan(new ForegroundColorSpan(UIUtils.getColor(R.color.red)), content.length()-1, content.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tv.setText(spantwo);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_upload_bgsq_card:
                if (ClickUtil.isNotFastClick()){
                    choicePhoto(mPic8);
                }
                break;
            case R.id.ll_upload_sfsq_card:
                if (ClickUtil.isNotFastClick()){
                    choicePhoto(mPic9);
                }
                break;
            case R.id.btn_submit:
                if (ClickUtil.isNotFastClick()){
                    mIsToUploadSfsq = false;
                    submit(path_bgsq);//上传变更申请函
                }
                break;
            default:
                break;

        }
    }

    private void submit(final String path) {
        /*if (TextUtils.isEmpty(path)){
            if (PrivateFragment.FROM_PRIAVE_FRAGEMENT == mIntentFrom ){
                if (!TextUtils.isEmpty(mLicense_bgsq)){
                    //有原先的图片在展示
                    Intent intent = new Intent();
                    intent.putExtra(Constants.INTENT_NAME_PIC_BGSQ_NET,mLicense_bgsq);
                    setResult(RESULT_OK,intent);
                    finish();
                    return;
                }
            }else if (PublicFragment.FROM_PUBLIC_FRAGEMENT == mIntentFrom ){
                if (!TextUtils.isEmpty(mLicense_bgsq) && !TextUtils.isEmpty(mLicense_sfsq)){
                    //有原先的图片在展示
                    Intent intent = new Intent();
                    intent.putExtra(Constants.INTENT_NAME_PIC_BGSQ_NET,mLicense_bgsq);
                    intent.putExtra(Constants.INTENT_NAME_PIC_SFSQ_NET,mLicense_sfsq);
                    setResult(RESULT_OK,intent);
                    finish();
                    return;
                }
            }

            if (TextUtils.isEmpty(path_bgsq)){
                ToastUtil.showToastShort("请上传变更申请函");
            }else if (TextUtils.isEmpty(path_sfsq)){
                ToastUtil.showToastShort("请上传第三方授权函");
            }
            return;
        }*/
        if (PrivateFragment.FROM_PRIAVE_FRAGEMENT == mIntentFrom ){
            if (TextUtils.isEmpty(path_bgsq)){
                ToastUtil.showToastShort("请上传变更申请函");
                return;
            }
        }else if (PublicFragment.FROM_PUBLIC_FRAGEMENT == mIntentFrom ){
            if (TextUtils.isEmpty(path_bgsq)||TextUtils.isEmpty(path_sfsq)){
                if (TextUtils.isEmpty(path_bgsq)){
                    ToastUtil.showToastShort("请上传变更申请函");
                }else if (TextUtils.isEmpty(path_sfsq)){
                    ToastUtil.showToastShort("请上传第三方授权函");
                }
                return;
            }
        }

        if (!NetworkUtils.isNetworkAvailable(this)) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
            return;
        }
        DialogUtil.safeShowDialog(mLoadDialog);

        String url = Constants.BASE_URL + "/app/merchant/file/upload";
        String picPath = null;
        PostFormBuilder post = OkHttpUtils.post();
        if (!TextUtils.isEmpty(path)) {
            picPath = AppHelper.getImageCacheDir(path);
            LogUtil.d("pic====" + picPath + ",,," + path);
            ImageFactory.compressPicture(path, picPath);
            post = post.addFile("license.jpg", "license", new File(picPath));
        }


        final String finalPicPath = picPath;
        post.url(url).build().connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                e.printStackTrace();
                LogUtil.i("zhouwei", "onErroreee//..>>>" + e);
                DialogUtil.safeCloseDialog(mLoadDialog);
                ToastUtil.showToastShort("上传失败");
                FileUtils.deleteFile(finalPicPath);

            }

            @Override
            public void onResponse(String response, int id) {
                LogUtil.i("zhouwei", "response" + response);
                FileUtils.deleteFile(finalPicPath);
                Gson gson = new Gson();
                UploadImageBean uploadImageBean = gson.fromJson(response, UploadImageBean.class);
                if (uploadImageBean != null) {
                    if (uploadImageBean.isStatus()) {
                        if (!TextUtils.isEmpty(path_sfsq) && path.equals(path_bgsq) && !mIsToUploadSfsq){
                            UploadImageBean.DataEntity data = uploadImageBean.getData();
                            if (data!=null){
                                mLicense_bgsq = data.getLicense();
                            }
                            submit(path_sfsq);//上传第三方授权函
                            mIsToUploadSfsq = true;//为了防止path_sfsq==path_bgsq出现死循环
                        }else {
                            UploadImageBean.DataEntity data = uploadImageBean.getData();
                            if (path.equals(path_bgsq) && !mIsToUploadSfsq){
                                mLicense_bgsq = data.getLicense();///pic/mch/2020/11/05/2ca6e395-0c87-46fd-892a-36d2f02189d1.jpg
                            }else if (path.equals(path_sfsq)){
                                mLicense_sfsq = data.getLicense();
                            }
                            ToastUtil.showToastShort("上传成功");
                            DialogUtil.safeCloseDialog(mLoadDialog);
                            Intent intent = new Intent();
                            intent.putExtra(Constants.INTENT_NAME_PIC_BGSQ_NET,mLicense_bgsq);
                            intent.putExtra(Constants.INTENT_NAME_PIC_SFSQ_NET,mLicense_sfsq);
                            setResult(RESULT_OK,intent);
                            finish();
                        }
                    } else {
                        if (uploadImageBean.getError() != null && uploadImageBean.getError().getMessage() != null) {
                            ToastUtil.showToastShort(uploadImageBean.getError().getMessage());
                        }
                        DialogUtil.safeCloseDialog(mLoadDialog);
                    }
                }else {
                    ToastUtil.showToastShort("上传失败");
                    DialogUtil.safeCloseDialog(mLoadDialog);
                }
            }
        });
    }

    private void choicePhoto(final PictureBean bean) {
        mSelectPictureBean = bean;
        final String status = Environment.getExternalStorageState();
        final CustomViewBottomDialog dialog = new CustomViewBottomDialog(this);
        dialog.setView(R.layout.dialog_change_bank_card_select_picture);
        TextView mTv_btn_take_photo = dialog.findViewById(R.id.tv_btn_take_photo);//拍照
        TextView tv_btn_select_photo = dialog.findViewById(R.id.tv_btn_select_photo);//上传
        TextView tv_btn_down_mb = dialog.findViewById(R.id.tv_btn_down_mb);//下载模板
        TextView tv_btn_cancel = dialog.findViewById(R.id.tv_btn_cancel);//取消
        View.OnClickListener l = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.tv_btn_take_photo:
                        //打开相机拍照
                        dialog.dismiss();
                        if (status.equals(Environment.MEDIA_MOUNTED)) {
                            try {
                                boolean results = PermissionUtils.checkPermissionArray(ChangeCardUploadImageActivity.this, permissionArray);
                                if (results) {
                                    startCamera();
                                } else {
                                    showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE1, permissionArray, getString(R.string.permission_content_photo));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    case R.id.tv_btn_select_photo:
                        //选择相册
                        dialog.dismiss();
                        try {
                            if (status.equals(Environment.MEDIA_MOUNTED)) {
                                boolean results = PermissionUtils.checkPermissionArray(ChangeCardUploadImageActivity.this, permissionArray);
                                if (results) {
                                    takeImg();
                                } else {
                                    showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE2, permissionArray, getString(R.string.permission_content_photo));
                                }
                            } else {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.tx_sd_pic));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case R.id.tv_btn_down_mb:
                        //下载模板
                        dialog.dismiss();
                        try {
                            boolean results = PermissionUtils.checkPermissionArray(ChangeCardUploadImageActivity.this, permissionArray);
                            if (results) {
                                if (bean.getPicId() == 8) {
                                    //变更申请函
                                    ImageUtil.saveAssetsImage(MyApplication.getContext(), "accountChangePhoto.jpg");
                                } else if (bean.getPicId() == 9) {
                                    //第三方申请函
                                    ImageUtil.saveAssetsImage(MyApplication.getContext(), "thirdAuthPhoto.jpg");
                                }
                            } else {
                                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE3, permissionArray, getString(R.string.permission_content_storage));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case R.id.tv_btn_cancel:
                        dialog.dismiss();
                        break;
                }
            }
        };
        mTv_btn_take_photo.setOnClickListener(l);
        tv_btn_select_photo.setOnClickListener(l);
        tv_btn_down_mb.setOnClickListener(l);
        tv_btn_cancel.setOnClickListener(l);
        dialog.show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE1:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    startCamera();
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE2:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    takeImg();
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE3:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    if (mSelectPictureBean != null && mSelectPictureBean.getPicId() == 8) {
                        //变更申请函
                        ImageUtil.saveAssetsImage(MyApplication.getContext(), "accountChangePhoto.jpg");
                    } else if (mSelectPictureBean != null && mSelectPictureBean.getPicId() == 9) {
                        //第三方申请函
                        ImageUtil.saveAssetsImage(MyApplication.getContext(), "thirdAuthPhoto.jpg");
                    }
                } else {
                    showDialog(getString(R.string.permission_set_content_camera));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private Uri originalUri = null;
    private String imageUrl;
    //调用相机拍照
    private void startCamera(){
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String root = AppHelper.getImgCacheDir();
            imageUrl = root + new Date().getTime() + ".jpg";

            File tempFile = new File(imageUrl);
            if (!tempFile.getParentFile().exists()){
                tempFile.getParentFile().mkdirs();
            }

            if (Build.VERSION.SDK_INT >= 24) {
                //如果是则使用FileProvider
                originalUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".fileProvider", tempFile);
            } else {
                originalUri = Uri.fromFile(tempFile);
            }
            intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
            startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //从相册选择图片
    private void takeImg(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                //获取相册图片
                case REQUEST_CODE_TAKE_PICTURE:
                    if (data.getData() == null) {
                        return;
                    }
                    String path = getPicPath(data.getData());
                    Bitmap bitmap = null;
                    if (path != null) {
                        /*File file = new File(path);
                        int MAX_POST_SIZE = 8 * 1024 * 1024;
                        if (file.exists() && file.length() > MAX_POST_SIZE) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.tv_pic_than_5MB));
                            return;
                        } else {
                            bitmap = ImagePase.readBitmapFromStream(path);
                        }*/
                        bitmap = ImagePase.readBitmapFromStream(path);
                        if (bitmap != null) {
                            switch (mSelectPictureBean.getPicId()){
                                case 8:
                                    path_bgsq = path;
                                    setBitmapView(mNivPicBgsqCard, bitmap);
                                    break;
                                case 9:
                                    path_sfsq = path;
                                    setBitmapView(mNivPicSfsqCard, bitmap);
                                    break;
                            }
                        }
                    }
                    break;
                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    String pathPhoto = getPicPath(originalUri);
                    LogUtil.d("path=" + pathPhoto);
                    /*Bitmap bitmap_pci = null;
                    File file = new File(pathPhoto);
                    if (file.exists() && file.length() / 1024 > 100) {
                        bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                    } else {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                            bitmap_pci = ImagePase.createBitmap(imageUrl, ImagePase.bitmapSize_Image);
                        }
                    }*/
                    Bitmap bitmap_pci = null;
                    File file = new File(pathPhoto);
                    if (!file.exists()) {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                        }
                    }
                    bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);

                    if (bitmap_pci != null) {
                        Bitmap bitmap2 = ImagePase.readBitmapFromStream(pathPhoto);
                        switch (mSelectPictureBean.getPicId()){
                            case 8:
                                path_bgsq = pathPhoto;
                                setBitmapView(mNivPicBgsqCard, bitmap2);
                                break;
                            case 9:
                                path_sfsq = pathPhoto;
                                setBitmapView(mNivPicSfsqCard, bitmap2);
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 返回图片地址
     *
     * @param originalUri
     * @return String
     */
    @SuppressLint("NewApi")
    public String getPicPath(Uri originalUri) {
        ContentResolver mContentResolver = this.getContentResolver();
        String originalPath = null;
        if ((originalUri + "").contains("/sdcard")) {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/sdcard"));
        } else if ((originalUri + "").contains("/data")) {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/data"));
        } else if ((originalUri + "").contains("/storage")) // 小米 手机 适配
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/storage"));
        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
                    && originalUri.toString().contains("documents")) {
                String wholeID = DocumentsContract.getDocumentId(originalUri);
                String id = wholeID.split(":")[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String sel = MediaStore.Images.Media._ID + " = ?";
                Cursor cursor =
                        mContentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                column,
                                sel,
                                new String[]{id},
                                null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    originalPath = cursor.getString(columnIndex);
                }
                cursor.close();
            } else {

                Cursor cursor = mContentResolver.query(originalUri, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    originalPath = cursor.getString(1);
                    cursor.close();
                }
            }

        }
        return originalPath;
    }

    private void setBitmapView(final NiceImageView iv, final Bitmap bitmap) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (bitmap != null) {
                    iv.setImageBitmap(bitmap);
                    iv.setVisibility(View.VISIBLE);
                    iv.postInvalidate();
                }
            }
        });

    }
}
