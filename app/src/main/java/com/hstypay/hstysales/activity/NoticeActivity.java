package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.adapter.NoticeAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.NoticeBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CustomLinearLayoutManager;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class NoticeActivity extends BaseActivity implements View.OnClickListener, NoticeAdapter.OnRecyclerViewItemClickListener {
    private RecyclerView mRecyclerview;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private NoticeAdapter mNoticeAdapter;
    private ImageView mIvBack;
    private TextView mTvTitle;

    private int pageSize = 15;
    private int currentPage = 1;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<NoticeBean.DataEntity.ItemData> mListData = new ArrayList<>();
    private RelativeLayout mLayoutEmpty;
    private SafeDialog mLoadDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        setContentView(R.layout.activity_notice);
        StatusBarUtil.setTranslucentStatus(this);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(getString(R.string.title_notice));
        mLayoutEmpty = (RelativeLayout) findViewById(R.id.layout_data_empty);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);

        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initRecyclerView() {
        mRecyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerview.setLayoutManager(mLinearLayoutManager);
        mRecyclerview.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    currentPage = 1;
                    isRefreshed = true;
                    getData(pageSize, currentPage);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getData(pageSize, currentPage);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
    }

    private void initData() {
        mNoticeAdapter = new NoticeAdapter(MyApplication.getContext(),mListData);
        mNoticeAdapter.setOnItemClickListener(this);
        mRecyclerview.setAdapter(mNoticeAdapter);

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            currentPage = 1;
            isRefreshed = true;
            getData(pageSize,currentPage);
        }
    }

    private void getData(int pageSize, int currentPage) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String,Object> map = new HashMap<>();
            map.put("pageSize",pageSize);
            map.put("currentPage",currentPage);
            ServerClient.newInstance(MyApplication.getContext()).getMessage(MyApplication.getContext(), Constants.TAG_GET_MESSAGE, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MESSAGE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            NoticeBean msg = (NoticeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_MESSAGE_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(NoticeActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_MESSAGE_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg != null && msg.getData() != null) {
                        if (msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                            mLayoutEmpty.setVisibility(View.GONE);
                            mRecyclerview.setVisibility(View.VISIBLE);
                            mListData.addAll(msg.getData().getData());
                            mNoticeAdapter.notifyDataSetChanged();
                        } else {
                            if (isLoadmore) {
                                mLayoutEmpty.setVisibility(View.GONE);
                                mRecyclerview.setVisibility(View.VISIBLE);
                                ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                            } else {
                                mLayoutEmpty.setVisibility(View.VISIBLE);
                                mRecyclerview.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        if (isLoadmore) {
                            mLayoutEmpty.setVisibility(View.GONE);
                            mRecyclerview.setVisibility(View.VISIBLE);
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mLayoutEmpty.setVisibility(View.VISIBLE);
                            mRecyclerview.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mListData.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    @Override
    public void onItemClick(int position) {
        //TODO 替换URL
        mListData.get(position).setIsRead(1);
        Intent intent = new Intent(NoticeActivity.this, RegisterActivity.class);
        intent.putExtra(Constants.REGISTER_INTENT, Constants.H5_MSG_DETAIL_URL + mListData.get(position).getId());
        startActivity(intent);
    }
}
