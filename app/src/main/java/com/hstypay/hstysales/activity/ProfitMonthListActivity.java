package com.hstypay.hstysales.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.bigkoo.pickerview.listener.CustomListener;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.adapter.MerchantRecyclerAdapter;
import com.hstypay.hstysales.adapter.ProfitListAdapter;
import com.hstypay.hstysales.adapter.SalesmanAdapter;
import com.hstypay.hstysales.adapter.ServiceProviderAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.MerchantListBean;
import com.hstypay.hstysales.bean.ProfitListBean;
import com.hstypay.hstysales.bean.ProfitMonthListBean;
import com.hstypay.hstysales.bean.SalesmanBean;
import com.hstypay.hstysales.bean.ServiceProviderBean;
import com.hstypay.hstysales.utils.DateUtil;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.widget.LinearSpaceItemDecoration;
import com.ykk.dropdownmenu.TabBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;
import com.ykk.dropdownmenu.DropDownMenu;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Desc: 一个月的分润明细页面
 *
 * @author kuangzeyu
 * @date 2020/10/23
 **/
public class ProfitMonthListActivity extends BaseActivity implements View.OnClickListener {

    private TextView mTvTitle;
    private ImageView mIvBack;
    private SHSwipeRefreshLayout mSwipeRefreshLayoutProfit;
    private RecyclerView mRvProfitListMonth;
    private View mLayoutDataEmpty;//空视图
    private ProfitListAdapter mProfitListAdapter;//分润明细数据的adapter
    private TextView mTvDateMonthProfit;//tv日期：xx年xx月
    private ImageView mIvArrowMonthProfit;//iv日期箭头

    private TimePickerView mTimePickerView;

    private SafeDialog mLoadDialog;

    private String timeFormatType = "yyyy-MM-dd HH:mm:ss";

    private Calendar mCurSelectDate;//当前选中的日期
    //    private Calendar mLastSelectDate;//记录上一次选择的日期
    private String mCurSelectServiceProviderId;//当前选中的市运营ID
    private String mCurSelectSalesmanId;//当前选中的城市经理ID
    private String mCurSelectMerchantId;//当前选中的商户ID
    private String mCurSelectMerchantName;

    private boolean isLoadmore;//加载更多标识
    private boolean isPullRefresh;//下拉刷新标识
    //    private List<ProfitListBean.SummaryData> mSummaryDataList = new ArrayList<>();//分润列表数据
    private SHSwipeRefreshLayout mSwipeRefreshLayoutStore;
    private ServiceProviderBean.DataEntity.ServiceProvider mAllServiceProvider;//"全部市运营"
    private SalesmanBean.DataEntity.Salesman mAllSalesman;//"全部城市经理"
    private MerchantListBean.DataEntity.ItemData mAllMerchant;//"全部商户"
    private boolean isLoadMerchantMore;
    private DropDownMenu mDropDownMenu;
    private List<TabBean> headers;
    private MerchantRecyclerAdapter mMerchantRecyclerAdapter;
    private SalesmanAdapter mSalesmanAdapter;
    private ServiceProviderAdapter mServiceProviderAdapter;
    private boolean isLoadOver;
    private int mSearchPageSize = 15;
    private int mSearchCurrentPage = 1;
    private List<MerchantListBean.DataEntity.ItemData> mSearchMerchantList;//搜索到的商户列表数据
    private List<SalesmanBean.DataEntity.Salesman> mSearchSalesmanList;//搜索到的城市经理列表数据
    private List<ServiceProviderBean.DataEntity.ServiceProvider> mSearchServiceProviderList;//搜索到的市运营列表数据
    private int mSelectLabelPosition;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private EditText mEtServiceInput;
    private ImageView mIvServiceClean;
    private View mServiceDataEmpty;
    private EditText mEtSalesmanInput;
    private ImageView mIvSalesmanClean;
    private View mSalesmanDataEmpty;
    private EditText mEtMerchantInput;
    private ImageView mIvMerchantClean;
    private View mMerchantDataEmpty;
    private LinearLayout mLlDate;
    private TextView mTvDate,mTvCityService, mTvSalesman, mTvMerchant;;
    private ImageView mIvDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profit_month_list);
        StatusBarUtil.setTranslucentStatus(this);
        initView();
        initData();
        initEvent();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle.setText(UIUtils.getString(R.string.title_profit_list));

        mDropDownMenu = findViewById(R.id.dropDownMenu);
    }

    private void initEvent() {
        mIvBack.setOnClickListener(this);
        mLlDate.setOnClickListener(this);
    }

    private void initData() {
        mCurSelectDate = Calendar.getInstance();
        mCurSelectDate.setTime(new Date());

        initPickTimeView();

        initDropDownMenu();

        getMonthProfitDetailList();
    }

    private void initDropDownMenu() {
        mCurSelectServiceProviderId = "";
        mCurSelectSalesmanId = "";
        mCurSelectMerchantId = "";
        mSearchServiceProviderList = new ArrayList<>();
        mSearchSalesmanList = new ArrayList<>();
        mSearchMerchantList = new ArrayList<>();

        mAllServiceProvider = new ServiceProviderBean.DataEntity.ServiceProvider();
        mAllServiceProvider.setServiceProviderName("全部市运营");
        mAllServiceProvider.setServiceProviderId("");

        mAllSalesman = new SalesmanBean.DataEntity.Salesman();
        mAllSalesman.setEmpName("全部城市经理");
        mAllSalesman.setEmpId("");

        mAllMerchant = new MerchantListBean.DataEntity.ItemData();
        mAllMerchant.setMerchantName("全部商户");
        mAllMerchant.setMerchantId("");

        View contentView = getLayoutInflater().inflate(R.layout.layout_content_view, null);
        mLlDate = contentView.findViewById(R.id.ll_date);
        mTvDate = contentView.findViewById(R.id.tv_date);
        mIvDate = contentView.findViewById(R.id.iv_date);
        mTvCityService = contentView.findViewById(R.id.tv_city_service);
        mTvSalesman = contentView.findViewById(R.id.tv_salesman);
        mTvMerchant = contentView.findViewById(R.id.tv_merchant);
        mSwipeRefreshLayoutProfit = contentView.findViewById(R.id.swipeRefreshLayout_profit);
        mSwipeRefreshLayoutProfit.setRefreshEnable(false);
        mSwipeRefreshLayoutProfit.setLoadmoreEnable(false);
        mRvProfitListMonth = contentView.findViewById(R.id.rv_profit_list_month);
        mLayoutDataEmpty = contentView.findViewById(R.id.layout_data_empty);
        mSwipeRefreshLayoutProfit.setVisibility(View.GONE);
        mRvProfitListMonth.setLayoutManager(new LinearLayoutManager(this));
        mRvProfitListMonth.addItemDecoration(new LinearSpaceItemDecoration(DensityUtils.dip2px(this, 15), true));
        mProfitListAdapter = new ProfitListAdapter(this, ProfitListAdapter.ProfitAdapterType.MONTH_PROFIT);
        mRvProfitListMonth.setAdapter(mProfitListAdapter);
        mTvDate.setText(DateUtil.getDateString(mCurSelectDate,"yyyy年MM月"));

        headers = new ArrayList<>();
        /*TabBean tabBean1 = new TabBean();
        tabBean1.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean1.setLabel("date");
        tabBean1.setTabTitle(DateUtil.getDateString(Calendar.getInstance(), "yyyy年MM月"));*/
        TabBean tabBean2 = new TabBean();
        tabBean2.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean2.setLabel("cityService");
        tabBean2.setTabTitle("市运营");
        TabBean tabBean3 = new TabBean();
        tabBean3.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean3.setLabel("cityManager");
        tabBean3.setTabTitle("城市经理");
        TabBean tabBean4 = new TabBean();
        tabBean4.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean4.setLabel("merchant");
        tabBean4.setTabTitle("商户");
//        headers.add(tabBean1);
        if ("SUPERADMIN".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_ROLE_CODE))) {
            if ("".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean2);
                headers.add(tabBean3);
                mTvCityService.setVisibility(View.VISIBLE);
                mTvSalesman.setVisibility(View.VISIBLE);
            } else if ("SC".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean3);
                mTvSalesman.setVisibility(View.VISIBLE);
            }
        }
        headers.add(tabBean4);
        mDropDownMenu.setDropDownMenu(headers, initViewData(), contentView);
        mDropDownMenu.setOnTabClickListener(new DropDownMenu.OnTabClickListener() {
            @Override
            public void onTabClick(int position, boolean isOpen) {
//                mSearchCurrentPage = 1;
//                isLoadMerchantMore = false;
//                isLoadOver = false;
                mSelectLabelPosition = position;
                switch (headers.get(position).getLabel()) {
//                    case "date":
//                        mTimePickerView.show();
//                        break;
                    case "cityService":
                        if (isOpen)
                            getServiceList(false);
                        break;
                    case "cityManager":
                        if (isOpen)
                            getSalesmanList(false);
                        break;
                    case "merchant":
                        if (isOpen)
                            getMerchantsByKey(false);
                        break;
                }
            }
        });
    }

    /**
     * 设置类型和数据源：
     * DropDownMenu.KEY对应类型（DropDownMenu中的常量，参考上述核心源码）
     * DropDownMenu.VALUE对应数据源：key不是TYPE_CUSTOM则传递string[],key是TYPE_CUSTOM类型则传递对应view
     */
    private List<HashMap<String, Object>> initViewData() {
        List<HashMap<String, Object>> viewDatas = new ArrayList<>();
        HashMap<String, Object> map;
        for (int i = 0; i < headers.size(); i++) {
            map = new HashMap<String, Object>();
            map.put(DropDownMenu.KEY, headers.get(i).getType());
            map.put(DropDownMenu.VALUE, getCustomView(headers.get(i).getLabel()));
            viewDatas.add(map);
        }
        return viewDatas;
    }

    private View getCustomView(String label) {
        View view;
        if ("date".equals(label)) {
            view = null;
        } else {
            view = getLayoutInflater().inflate(R.layout.layout_dropdown, null);
            switch (label) {
                case "cityService":
                    mEtServiceInput = view.findViewById(R.id.et_input);
                    mIvServiceClean = view.findViewById(R.id.iv_clean);
                    mServiceDataEmpty = view.findViewById(R.id.common_data_empty);
                    RecyclerView rvService = view.findViewById(R.id.recycler_view);
                    LinearLayoutManager serviceLinearLayoutManager = new LinearLayoutManager(this);
                    rvService.setLayoutManager(serviceLinearLayoutManager);
                    mServiceProviderAdapter = new ServiceProviderAdapter(ProfitMonthListActivity.this);
                    mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                    mServiceProviderAdapter.setOnItemClickListener(new ServiceProviderAdapter.OnRecyclerViewItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            if (mCurSelectServiceProviderId != null && !mCurSelectServiceProviderId.equals(mSearchServiceProviderList.get(position).getServiceProviderId())) {
                                mCurSelectSalesmanId = "";
                                mCurSelectMerchantId = "";
                                for (int i = 0; i < headers.size(); i++) {
                                    if ("cityManager".equals(headers.get(i).getLabel())) {
                                        mTvSalesman.setText("全部城市经理");
                                    }
                                    if ("merchant".equals(headers.get(i).getLabel())) {
                                        mTvMerchant.setText("全部商户");
                                    }
                                }
                                mCurSelectServiceProviderId = mSearchServiceProviderList.get(position).getServiceProviderId();
                                mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
//                                mDropDownMenu.setTabText(mSelectLabelPosition, mSearchServiceProviderList.get(position).getServiceProviderName());
                                mTvCityService.setText(mSearchServiceProviderList.get(position).getServiceProviderName());
                                getMonthProfitDetailList();
                            }
                            mDropDownMenu.closeMenu();
                        }
                    });
                    rvService.setAdapter(mServiceProviderAdapter);
                    rvService.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        //这个int用来记录最后一个可见的view
                        int lastVisibleItemPosition;
                        int refreshCurrentPage;//防止重复加载

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mServiceProviderAdapter.getItemCount()) {
                                refreshCurrentPage = mSearchCurrentPage;
                                if (!isLoadOver)
                                    getServiceList(true);
                            }
                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                            lastVisibleItemPosition = serviceLinearLayoutManager.findLastVisibleItemPosition();
                        }
                    });
                    mEtServiceInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                getServiceList(false);
                                return true;
                            }
                            return false;
                        }
                    });
                    mEtServiceInput.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() == 0) {
                                getServiceList(false);
                            }
                            mIvServiceClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    mIvServiceClean.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mEtServiceInput.setText("");
                        }
                    });
                    break;
                case "cityManager":
                    mEtSalesmanInput = view.findViewById(R.id.et_input);
                    mIvSalesmanClean = view.findViewById(R.id.iv_clean);
                    mSalesmanDataEmpty = view.findViewById(R.id.common_data_empty);
                    RecyclerView rvSalesman = view.findViewById(R.id.recycler_view);
                    LinearLayoutManager salesmanLinearLayoutManager = new LinearLayoutManager(this);
                    rvSalesman.setLayoutManager(salesmanLinearLayoutManager);
                    mSalesmanAdapter = new SalesmanAdapter(ProfitMonthListActivity.this);
                    mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                    mSalesmanAdapter.setOnItemClickListener(new SalesmanAdapter.OnRecyclerViewItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            if (mCurSelectSalesmanId != null && !mCurSelectSalesmanId.equals(mSearchSalesmanList.get(position).getEmpId())) {
                                mCurSelectMerchantId = "";
                                for (int i = 0; i < headers.size(); i++) {
                                    if ("merchant".equals(headers.get(i).getLabel()))
                                        mTvMerchant.setText("全部商户");
                                }
                                mCurSelectSalesmanId = mSearchSalesmanList.get(position).getEmpId();
                                mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
//                                mDropDownMenu.setTabText(mSelectLabelPosition, mSearchSalesmanList.get(position).getEmpName());
                                mTvSalesman.setText(mSearchSalesmanList.get(position).getEmpName());
                                getMonthProfitDetailList();
                            }
                            mDropDownMenu.closeMenu();
                        }
                    });
                    rvSalesman.setAdapter(mSalesmanAdapter);
                    rvSalesman.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        //这个int用来记录最后一个可见的view
                        int lastVisibleItemPosition;
                        int refreshCurrentPage;//防止重复加载

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mSalesmanAdapter.getItemCount()) {
                                LogUtil.d("refreshCurrentPage="+refreshCurrentPage+"---mSearchCurrentPage="+mSearchCurrentPage);
                                refreshCurrentPage = mSearchCurrentPage;
                                if (!isLoadOver)
                                    getSalesmanList(true);
                            }
                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                            lastVisibleItemPosition = salesmanLinearLayoutManager.findLastVisibleItemPosition();
                        }
                    });
                    mEtSalesmanInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                getSalesmanList(false);
                                return true;
                            }
                            return false;
                        }
                    });
                    mEtSalesmanInput.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() == 0) {
                                getSalesmanList(false);
                            }
                            mIvSalesmanClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    mIvSalesmanClean.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mEtSalesmanInput.setText("");
                        }
                    });
                    break;
                case "merchant":
                    mEtMerchantInput = view.findViewById(R.id.et_input);
                    mIvMerchantClean = view.findViewById(R.id.iv_clean);
                    mMerchantDataEmpty = view.findViewById(R.id.common_data_empty);
                    RecyclerView rvMerchant = view.findViewById(R.id.recycler_view);
                    LinearLayoutManager merchantLinearLayoutManager = new LinearLayoutManager(this);
                    rvMerchant.setLayoutManager(merchantLinearLayoutManager);
                    mMerchantRecyclerAdapter = new MerchantRecyclerAdapter(ProfitMonthListActivity.this);
                    mMerchantRecyclerAdapter.setData(mSearchMerchantList, mCurSelectMerchantId);
                    mMerchantRecyclerAdapter.setOnItemClickListener(new MerchantRecyclerAdapter.OnRecyclerViewItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            if (mCurSelectMerchantId != null && !mCurSelectMerchantId.equals(mSearchMerchantList.get(position).getMerchantId())) {
                                mCurSelectMerchantId = mSearchMerchantList.get(position).getMerchantId();
                                mMerchantRecyclerAdapter.setData(mSearchMerchantList, mCurSelectMerchantId);
//                                mDropDownMenu.setTabText(mSelectLabelPosition, mSearchMerchantList.get(position).getMerchantName());
                                mTvMerchant.setText(mSearchMerchantList.get(position).getMerchantName());
                                getMonthProfitDetailList();
                            }
                            mDropDownMenu.closeMenu();
                        }
                    });
                    rvMerchant.setAdapter(mMerchantRecyclerAdapter);
                    rvMerchant.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        //这个int用来记录最后一个可见的view
                        int lastVisibleItemPosition;
                        int refreshCurrentPage;//防止重复加载

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mMerchantRecyclerAdapter.getItemCount()) {
                                refreshCurrentPage = mSearchCurrentPage;
                                if (!isLoadOver)
                                    getMerchantsByKey(true);
                            }
                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                            lastVisibleItemPosition = merchantLinearLayoutManager.findLastVisibleItemPosition();
                        }
                    });
                    mEtMerchantInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                getMerchantsByKey(false);
                                return true;
                            }
                            return false;
                        }
                    });
                    mEtMerchantInput.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() == 0) {
                                getMerchantsByKey(false);
                            }
                            mIvMerchantClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    mIvMerchantClean.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mEtMerchantInput.setText("");
                        }
                    });
                    break;
            }
        }
        return view;
    }

    /**
     * 获取月分润明细列表数据
     */
    private void getMonthProfitDetailList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!(isPullRefresh || isLoadmore)) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mCurSelectSalesmanId))
                map.put("salesmanId", mCurSelectSalesmanId);
            if (!TextUtils.isEmpty(mCurSelectMerchantId))
                map.put("merchantId", mCurSelectMerchantId);

            Calendar startTime = Calendar.getInstance();
            startTime.setTime(mCurSelectDate.getTime());
            startTime.set(Calendar.DAY_OF_MONTH, 1);//开始日期为当月的1号
            startTime.set(Calendar.HOUR_OF_DAY, 0);
            startTime.set(Calendar.MINUTE, 0);
            startTime.set(Calendar.SECOND, 0);

            Calendar endTime = Calendar.getInstance();
            endTime.setTime(mCurSelectDate.getTime());
            endTime.set(Calendar.HOUR_OF_DAY, 23);
            endTime.set(Calendar.MINUTE, 59);
            endTime.set(Calendar.SECOND, 59);

            map.put("startTime", DateUtil.getDateString(startTime, timeFormatType));
            map.put("endTime", DateUtil.getDateString(endTime, timeFormatType));
            map.put("realTimeFlag", "0");//0表示非实时查询 1表示实时查询
            ServerClient.newInstance(MyApplication.getContext()).getMonthProfitDetailList(MyApplication.getContext(), Constants.TAG_GET_MONTH_PROFIT_DETAIL_LIST_STATISTICS, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void initPickTimeView() {
        TimePickerView.Builder builder = new TimePickerView.Builder(this, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                //切换日期
                mCurSelectDate.setTime(date);
                Calendar nowDate = Calendar.getInstance();
                nowDate.setTime(new Date());
                if (mCurSelectDate.get(Calendar.MONTH) != nowDate.get(Calendar.MONTH)) {
                    //选择的日期不是本月
                    mCurSelectDate.set(Calendar.DAY_OF_MONTH, mCurSelectDate.getActualMaximum(Calendar.DAY_OF_MONTH));
                    mCurSelectDate.set(Calendar.HOUR_OF_DAY, 23);
                    mCurSelectDate.set(Calendar.MINUTE, 59);
                    mCurSelectDate.set(Calendar.SECOND, 59);
                } else {
                    mCurSelectDate.setTime(nowDate.getTime());
                }
                getMonthProfitDetailList();
            }
        });
        builder.setLayoutRes(R.layout.pickview_year_month_profit, new CustomListener() {
            @Override
            public void customLayout(View v) {
                Button btnCancel = v.findViewById(R.id.btnCancel);
                Button btnSubmit = v.findViewById(R.id.btnSubmit);
                btnCancel.setOnClickListener(ProfitMonthListActivity.this);
                btnSubmit.setOnClickListener(ProfitMonthListActivity.this);
            }
        });
        //设置只显示年月
        builder.setType(new boolean[]{true, true, false, false, false, false});
        //设置不显示时间单位
        builder.setLabel("", "", "", "", "", "");
        //起始时间
        Calendar startDate = Calendar.getInstance();
        //tartDate.setTimeInMillis(0);
        startDate.setTime(mCurSelectDate.getTime());
        startDate.add(Calendar.MONTH, -12);//12个月前
        //结束时间
        Calendar endDate = Calendar.getInstance();
        endDate.setTime(mCurSelectDate.getTime());
        //设置选择的时间范围
        builder.setRangDate(startDate, endDate);
        //设置选择的日期
        builder.setDate(mCurSelectDate);
        //设置分割线的颜色
        builder.setDividerColor(getResources().getColor(R.color.home_line));
        //设置间距倍数
        builder.setLineSpacingMultiplier(2.5f);
        builder.setTextColorCenter(getResources().getColor(R.color.home_blue_text));
        builder.setContentSize(18);
        builder.setOutSideCancelable(false);
        mTimePickerView = builder.build();
    }

    private void getMerchantsByKey(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mEtMerchantInput.getText().toString().trim())) {
                map.put("search", mEtMerchantInput.getText().toString().trim());
            }
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mCurSelectSalesmanId))
                map.put("salesmanId", mCurSelectSalesmanId);
            map.put("mchQueryStatus", "100");
            ServerClient.newInstance(MyApplication.getContext()).getMerchantList2(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_LIST_MX_SEARCH + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getServiceList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                map.put("serviceProviderName", mEtServiceInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getServiceList(MyApplication.getContext(), "TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getSalesmanList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                map.put("empName", mEtSalesmanInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getSalesmanList(MyApplication.getContext(), "TAG_SALESMAN_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btnCancel:
                //复原当前选择的时间为上一次的时间
                //mCurSelectDate.setTime(mLastSelectDate.getTime());
                mTimePickerView.dismiss();
                closeArrow(mIvDate);
                break;
            case R.id.btnSubmit:
                mTimePickerView.returnData();
                mTimePickerView.dismiss();
                mTvDate.setText(DateUtil.getDateString(mCurSelectDate,"yyyy年MM月"));
                closeArrow(mIvDate);
                break;
            case R.id.ll_date:
                mTimePickerView.show();
                openArrow(mIvDate);
                break;
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MONTH_PROFIT_DETAIL_LIST_STATISTICS)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ProfitMonthListBean msg = (ProfitMonthListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    //复原当前选择的时间为上一次的时间
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    //复原当前选择的时间为上一次的时间
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ProfitMonthListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    List<ProfitListBean.SummaryData> summaryDataList = msg.getData();
                    if (summaryDataList != null && summaryDataList.size() > 0) {
                        mProfitListAdapter.setProfitListData(summaryDataList);
                        mLayoutDataEmpty.setVisibility(View.GONE);
                        mSwipeRefreshLayoutProfit.setVisibility(View.VISIBLE);
                    } else {
                        mProfitListAdapter.setProfitListData(summaryDataList);
                        mLayoutDataEmpty.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayoutProfit.setVisibility(View.GONE);
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ServiceProviderBean msg = (ServiceProviderBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ProfitMonthListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mServiceDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchServiceProviderList.clear();
                            if (TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                                mSearchServiceProviderList.add(mAllServiceProvider);
                            }
                        }

                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchServiceProviderList.addAll(msg.getData().getData());
                        mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchServiceProviderList.clear();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mServiceDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mServiceDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_SALESMAN_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SalesmanBean msg = (SalesmanBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ProfitMonthListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mSalesmanDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchSalesmanList.clear();
                            if (TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                                mSearchSalesmanList.add(mAllSalesman);
                            }
                        }
                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchSalesmanList.addAll(msg.getData().getData());
                        mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchSalesmanList.clear();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mSalesmanDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mSalesmanDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals(Constants.TAG_GET_MERCHANT_LIST_MX_SEARCH + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            MerchantListBean msg = (MerchantListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_MERCHANT_LIST_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ProfitMonthListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_MERCHANT_LIST_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mMerchantDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchMerchantList.clear();
                            if (TextUtils.isEmpty(mEtMerchantInput.getText().toString().trim())) {
                                mSearchMerchantList.add(mAllMerchant);
                            }
                        }

                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchMerchantList.addAll(msg.getData().getData());
                        mMerchantRecyclerAdapter.setData(mSearchMerchantList, mCurSelectMerchantId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchMerchantList.clear();
                            mMerchantRecyclerAdapter.setData(mSearchMerchantList, mCurSelectMerchantId);
                            mMerchantDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mMerchantDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        }
    }

    private void openArrow(View view) {
        Animation rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        view.startAnimation(rotate);
    }

    public void closeArrow(View view) {
        Animation rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        view.startAnimation(rotate);
    }
}