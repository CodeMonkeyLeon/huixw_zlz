package com.hstypay.hstysales.activity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.base.BaseActivity;

/**
 * @author zeyu.kuang
 * @time 2020/12/11
 * @desc 花呗分期介绍宣传页
 */
public class HuaBeiIntroduceActivity extends BaseActivity {

    private ImageView mIvIntroduceHbCase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hua_bei_introduce);
        TextView tv_title = findViewById(R.id.tv_title);
        tv_title.setText(getResources().getString(R.string.title_introduce_hb));
        FrameLayout fl_introduce_hb_ability = findViewById(R.id.fl_introduce_hb_ability);
        FrameLayout fl_introduce_hb_mode_jifei = findViewById(R.id.fl_introduce_hb_mode_jifei);
        FrameLayout fl_introduce_hb_method_open = findViewById(R.id.fl_introduce_hb_method_open);
        FrameLayout fl_introduce_hb_case = findViewById(R.id.fl_introduce_hb_case);
        View.inflate(this,R.layout.layout_introduce_hb_ability,fl_introduce_hb_ability);
        View.inflate(this,R.layout.layout_introduce_hb_mode_jifei,fl_introduce_hb_mode_jifei);
        View.inflate(this,R.layout.layout_introduce_hb_method_open,fl_introduce_hb_method_open);
        View.inflate(this,R.layout.layout_introduce_hb_case,fl_introduce_hb_case);
        mIvIntroduceHbCase = findViewById(R.id.iv_introduce_hb_case);
        mIvIntroduceHbCase.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mIvIntroduceHbCase.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ViewGroup.LayoutParams layoutParams = mIvIntroduceHbCase.getLayoutParams();
                int width = mIvIntroduceHbCase.getWidth();
                layoutParams.height = width * 1021/617; //图片实际宽高 617  1021
                mIvIntroduceHbCase.setLayoutParams(layoutParams);
            }
        });

        findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
