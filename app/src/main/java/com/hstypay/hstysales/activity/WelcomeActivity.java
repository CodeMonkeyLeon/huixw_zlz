package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.LoginBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpStayUtil;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class WelcomeActivity extends BaseActivity {

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1://第一次使用
                    startActivity(new Intent(WelcomeActivity.this, GuideActivity.class));
                    finish();
                    break;
                case 2:
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        ServerClient.newInstance(WelcomeActivity.this).autoPwd(WelcomeActivity.this, Constants.TAG_AUTO_PWD);
                    } else {
                        startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
                        WelcomeActivity.this.finish();
                    }
                    break;
                case 3:
                    startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
                    WelcomeActivity.this.finish();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        setContentView(R.layout.activity_welcome);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.type=100;
    }

    private void initListener() {

    }

    private void initData() {
        boolean isNotFirstUse = SpStayUtil.getBoolean(MyApplication.getContext(), Constants.SP_IS_NOT_FIRST_USE);
        if (isNotFirstUse) {
            if (!TextUtils.isEmpty(MyApplication.getCookies())) {
                mHandler.sendEmptyMessageDelayed(2,500);
            } else {
                mHandler.sendEmptyMessageDelayed(3, 1000);
            }
        } else {
            mHandler.sendEmptyMessageDelayed(1, 1000);
        }
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //免密登录返回的数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onWelcomeData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_AUTO_PWD)) {
            LoginBean msg = (LoginBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    SpUtil.removeKey(Constants.SKEY);
                    SpUtil.removeAll();
                    startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
                    WelcomeActivity.this.finish();
                    break;
                case Constants.AUTO_PWD_FALSE:
                    SpUtil.removeKey(Constants.SKEY);
                    SpUtil.removeAll();
                    startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
                    WelcomeActivity.this.finish();
                    break;
                case Constants.AUTO_PWD_TRUE:
                    startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
                    finish();
                    break;
            }
        }
    }
}