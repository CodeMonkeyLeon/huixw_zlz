package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.Zxing.MaxCardManager;
import com.hstypay.hstysales.adapter.SalesmanAdapter;
import com.hstypay.hstysales.adapter.ServiceProviderAdapter;
import com.hstypay.hstysales.adapter.WxRealMerchantAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.Info;
import com.hstypay.hstysales.bean.SalesmanBean;
import com.hstypay.hstysales.bean.ServiceProviderBean;
import com.hstypay.hstysales.bean.SignMerchantListBean;
import com.hstypay.hstysales.bean.WxSignDetailBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.ImageUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CommonNoticeDialog;
import com.hstypay.hstysales.widget.CustomViewFullScreenDialog;
import com.hstypay.hstysales.widget.EditTextDelete;
import com.hstypay.hstysales.widget.LinearSpaceItemDecoration;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;
import com.ykk.dropdownmenu.DropDownMenu;
import com.ykk.dropdownmenu.TabBean;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//微信实名认证的所有商户列表
public class WxRealAuthorMerchantListActivity extends BaseActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_WX_SIGN_DETAIL = 100;
    private TextView mTvTitle;
    private ImageView mIvBack;
    private RadioGroup mRgStateSignWx;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;

    public static final int mPageSize = 15;
    protected int mCurrentPage = 1;
    private boolean isLoadmore;//加载更多标识
    private boolean isPullRefresh;//下拉刷新标识
    private List<WxSignDetailBean.SignDetailData> mSignDetailDataList = new ArrayList<>();
    private SafeDialog mLoadDialog;

    private int mStatus = -1;//签约状态, 0-初始,10-签约成功,20-签约失败, 30-待签约 , -1全部
    private EditTextDelete mEtInput;
    private WxRealMerchantAdapter mWxRealMerchantAdapter;
    private TextView mTvNull;
    private RelativeLayout mLlInput;
    private int mLastRequestStatus;
    private DropDownMenu mDropDownMenu;
    private String mCurSelectServiceProviderId;
    private String mCurSelectSalesmanId;
    private ArrayList<ServiceProviderBean.DataEntity.ServiceProvider> mSearchServiceProviderList;
    private ArrayList<SalesmanBean.DataEntity.Salesman> mSearchSalesmanList;
    private ArrayList<TabBean> headers;
    private int mSearchCurrentPage = 1;
    private int mSearchPageSize = 15;
    private boolean isLoadMerchantMore;
    private boolean isLoadOver;
    private int mSelectLabelPosition;
    private EditText mEtServiceInput;
    private ImageView mIvServiceClean;
    private View mServiceDataEmpty;
    private ServiceProviderAdapter mServiceProviderAdapter;
    private EditText mEtSalesmanInput;
    private ImageView mIvSalesmanClean;
    private View mSalesmanDataEmpty;
    private SalesmanAdapter mSalesmanAdapter;
    private ServiceProviderBean.DataEntity.ServiceProvider mAllServiceProvider;
    private SalesmanBean.DataEntity.Salesman mAllSalesman;
    private String mIntentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wx_real_author_merchant_list);
        StatusBarUtil.setTranslucentStatusWhiteIcon(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mTvTitle = findViewById(R.id.tv_title);
        mIvBack = findViewById(R.id.iv_back);

        mDropDownMenu = findViewById(R.id.dropDownMenu);
    }

    private void initData() {
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mTvTitle.setText("INTENT_ALI_AUTH".equals(mIntentName) ? R.string.ali_real_name_auth : R.string.wx_real_name_auth);
        initDropDownMenu();
        getWxSignMerchantList(false);
    }

    private void initEvent() {
        mIvBack.setOnClickListener(this);
        mRgStateSignWx.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_all_merchant:
                        //全部
                        mStatus = -1;
                        break;
                    case R.id.rb_init_merchant:
                        //初始
                        mStatus = 0;
                        break;
                    case R.id.rb_wait_sign_merchant:
                        //待签约
                        mStatus = 30;
                        break;
                    case R.id.rb_sign_success_merchant:
                        //签约成功
                        mStatus = 10;
                        break;
                    case R.id.rb_sign_fail_merchant:
                        //签约失败
                        mStatus = 20;
                        break;
                    default:
                        break;
                }
                if (mLastRequestStatus != mStatus) {
                    mCurrentPage = 1;
                    getWxSignMerchantList(false);
                }
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    mCurrentPage = 1;
                    isPullRefresh = true;
                    getWxSignMerchantList(false);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getWxSignMerchantList(false);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });
        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //从门店列表里搜索门店
                    mCurrentPage = 1;
                    getWxSignMerchantList(false);
                    return true;
                }
                return false;
            }
        });
        mEtInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (isClear) {
                        mCurrentPage = 1;
                        getWxSignMerchantList(false);
                    }
                }
            }
        });


        mWxRealMerchantAdapter.setOnWxRealMerchantFunctionClickListener(new WxRealMerchantAdapter.OnWxRealMerchantFunctionClickListener() {
            @Override
            public void itemClick(int position) {
                //微信实名认证详情
                Intent intentWx = new Intent(WxRealAuthorMerchantListActivity.this, WxRealNameAuthDetailActivity.class);
                intentWx.putExtra(Constants.INTENT_MERCHANT_ID, mSignDetailDataList.get(position).getMerchantId());
                intentWx.putExtra(Constants.INTENT_NAME, mIntentName);
                WxSignDetailBean.SignDetailData signDetailData = mSignDetailDataList.get(position);
                if (signDetailData != null) {
                    intentWx.putExtra(Constants.INTENT_WX_SIGN_DATA, signDetailData);
                }
                startActivityForResult(intentWx, REQUEST_CODE_WX_SIGN_DETAIL);
            }

            @Override
            public void cancelSignWx(int position) {
                //撤销申请单
                showRemindCancelReplyDialog(position);
            }

            @Override
            public void signHelpWx(int position) {
                //人工签约
                if ("INTENT_ALI_AUTH".equals(mIntentName)) {
                    if (mSignDetailDataList.get(position) != null) {
                        Intent intent = new Intent(WxRealAuthorMerchantListActivity.this, RealNameAuthCodeActivity.class);
                        intent.putExtra("INTENT_SIGN_DETAIL", mSignDetailDataList.get(position));
                        startActivity(intent);
                    } else {
                        ToastUtil.showToastLong(getString(R.string.error_data));
                    }
                } else {
                    showWxSignDialog(position);
                }
            }

            @Override
            public void reSignWx(int position) {
                //重新签约
                againSignAuth(position);
            }
        });
    }

    private void initDropDownMenu() {
        mCurSelectServiceProviderId = "";
        mCurSelectSalesmanId = "";
        mSearchServiceProviderList = new ArrayList<>();
        mSearchSalesmanList = new ArrayList<>();
        mAllServiceProvider = new ServiceProviderBean.DataEntity.ServiceProvider();//"全部商户"
        mAllServiceProvider.setServiceProviderName("全部市运营");
        mAllServiceProvider.setServiceProviderId("");

        mAllSalesman = new SalesmanBean.DataEntity.Salesman();//"全部商户"
        mAllSalesman.setEmpName("全部城市经理");
        mAllSalesman.setEmpId("");

        View contentView = getLayoutInflater().inflate(R.layout.layout_content_wx_real_author, null);

        mRgStateSignWx = contentView.findViewById(R.id.rg_state_sign_wx);
        mTvNull = contentView.findViewById(R.id.tv_null);
        mLlInput = contentView.findViewById(R.id.ll_input);
        mEtInput = contentView.findViewById(R.id.et_input);
        mSwipeRefreshLayout = contentView.findViewById(R.id.swipeRefreshLayout);
        mRecyclerView = contentView.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new LinearSpaceItemDecoration(DensityUtils.dip2px(this, 12), false));
        mWxRealMerchantAdapter = new WxRealMerchantAdapter(this);
        mRecyclerView.setAdapter(mWxRealMerchantAdapter);

        headers = new ArrayList<>();
        TabBean tabBean2 = new TabBean();
        tabBean2.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean2.setLabel("cityService");
        tabBean2.setTabTitle("全部市运营");
        TabBean tabBean3 = new TabBean();
        tabBean3.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean3.setLabel("cityManager");
        tabBean3.setTabTitle("全部城市经理");
        if ("SUPERADMIN".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_ROLE_CODE))) {
            if ("".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean2);
                headers.add(tabBean3);
            } else if ("SC".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean3);
            }
        }
        mDropDownMenu.setDropDownMenu(headers, initViewData(), contentView);
        mDropDownMenu.setOnTabClickListener(new DropDownMenu.OnTabClickListener() {
            @Override
            public void onTabClick(int position, boolean isOpen) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
                mSelectLabelPosition = position;
                switch (headers.get(position).getLabel()) {
                    case "cityService":
                        if (isOpen)
                            getServiceList(false);
                        break;
                    case "cityManager":
                        if (isOpen)
                            getSalesmanList(false);
                        break;
                }
            }
        });
    }

    /**
     * 设置类型和数据源：
     * DropDownMenu.KEY对应类型（DropDownMenu中的常量，参考上述核心源码）
     * DropDownMenu.VALUE对应数据源：key不是TYPE_CUSTOM则传递string[],key是TYPE_CUSTOM类型则传递对应view
     */
    private List<HashMap<String, Object>> initViewData() {
        List<HashMap<String, Object>> viewDatas = new ArrayList<>();
        HashMap<String, Object> map;
        for (int i = 0; i < headers.size(); i++) {
            map = new HashMap<String, Object>();
            map.put(DropDownMenu.KEY, headers.get(i).getType());
            map.put(DropDownMenu.VALUE, getCustomView(headers.get(i).getLabel()));
            viewDatas.add(map);
        }
        return viewDatas;
    }

    private View getCustomView(String label) {
        View view = getLayoutInflater().inflate(R.layout.layout_dropdown, null);
        switch (label) {
            case "cityService":
                mEtServiceInput = view.findViewById(R.id.et_input);
                mIvServiceClean = view.findViewById(R.id.iv_clean);
                mServiceDataEmpty = view.findViewById(R.id.common_data_empty);
                RecyclerView rvService = view.findViewById(R.id.recycler_view);
                LinearLayoutManager serviceLinearLayoutManager = new LinearLayoutManager(this);
                rvService.setLayoutManager(serviceLinearLayoutManager);
                mServiceProviderAdapter = new ServiceProviderAdapter(WxRealAuthorMerchantListActivity.this);
                mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                mServiceProviderAdapter.setOnItemClickListener(new ServiceProviderAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mCurSelectServiceProviderId != null && !mCurSelectServiceProviderId.equals(mSearchServiceProviderList.get(position).getServiceProviderId())) {
                            mCurSelectSalesmanId = "";
                            for (int i = 0; i < headers.size(); i++) {
                                if ("cityManager".equals(headers.get(i).getLabel()))
                                    mDropDownMenu.setTabText(i, "全部城市经理");
                            }
                            mCurSelectServiceProviderId = mSearchServiceProviderList.get(position).getServiceProviderId();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mDropDownMenu.setTabText(mSelectLabelPosition, mSearchServiceProviderList.get(position).getServiceProviderName());
                            mCurrentPage = 1;
                            getWxSignMerchantList(true);
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvService.setAdapter(mServiceProviderAdapter);
                rvService.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    //这个int用来记录最后一个可见的view
                    int lastVisibleItemPosition;
                    int refreshCurrentPage;//防止重复加载

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mServiceProviderAdapter.getItemCount()) {
                            refreshCurrentPage = mSearchCurrentPage;
                            if (!isLoadOver)
                                getServiceList(true);
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                        lastVisibleItemPosition = serviceLinearLayoutManager.findLastVisibleItemPosition();
                    }
                });
                mEtServiceInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            getServiceList(false);
                            return true;
                        }
                        return false;
                    }
                });
                mEtServiceInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            getServiceList(false);
                        }
                        mIvServiceClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                mIvServiceClean.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEtServiceInput.setText("");
                    }
                });
                break;
            case "cityManager":
                mEtSalesmanInput = view.findViewById(R.id.et_input);
                mIvSalesmanClean = view.findViewById(R.id.iv_clean);
                mSalesmanDataEmpty = view.findViewById(R.id.common_data_empty);
                RecyclerView rvSalesman = view.findViewById(R.id.recycler_view);
                LinearLayoutManager salesmanLinearLayoutManager = new LinearLayoutManager(this);
                rvSalesman.setLayoutManager(salesmanLinearLayoutManager);
                mSalesmanAdapter = new SalesmanAdapter(WxRealAuthorMerchantListActivity.this);
                mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                mSalesmanAdapter.setOnItemClickListener(new SalesmanAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mCurSelectSalesmanId != null && !mCurSelectSalesmanId.equals(mSearchSalesmanList.get(position).getEmpId())) {
                            mCurSelectSalesmanId = mSearchSalesmanList.get(position).getEmpId();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mDropDownMenu.setTabText(mSelectLabelPosition, mSearchSalesmanList.get(position).getEmpName());
                            mCurrentPage = 1;
                            getWxSignMerchantList(true);
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvSalesman.setAdapter(mSalesmanAdapter);
                rvSalesman.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    //这个int用来记录最后一个可见的view
                    int lastVisibleItemPosition;
                    int refreshCurrentPage;//防止重复加载

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mSalesmanAdapter.getItemCount()) {
                            refreshCurrentPage = mSearchCurrentPage;
                            if (!isLoadOver)
                                getSalesmanList(true);
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                        lastVisibleItemPosition = salesmanLinearLayoutManager.findLastVisibleItemPosition();
                    }
                });
                mEtSalesmanInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            getSalesmanList(false);
                            return true;
                        }
                        return false;
                    }
                });
                mEtSalesmanInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            getSalesmanList(false);
                        }
                        mIvSalesmanClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                mIvSalesmanClean.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEtSalesmanInput.setText("");
                    }
                });
                break;
        }
        return view;
    }

    //人工签约
    private void showWxSignDialog(int position) {
        CustomViewFullScreenDialog wxSignDialog = new CustomViewFullScreenDialog(this);
        wxSignDialog.setView(R.layout.dialog_wx_real_sign);
        ImageView iv_qrcode_wx_sign_dialog = wxSignDialog.findViewById(R.id.iv_qrcode_wx_sign_dialog);
        TextView tv_wx_sign_detail = wxSignDialog.findViewById(R.id.tv_wx_sign_detail);
        Button btn_know_dialog_wx = wxSignDialog.findViewById(R.id.btn_know_dialog_wx);
        wxSignDialog.show();

        btn_know_dialog_wx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wxSignDialog.dismiss();
            }
        });
        tv_wx_sign_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String jumpUrl = "https://pay.weixin.qq.com/static/help_guide/register_guide.shtml";
                /*String jumpUrl = "https://adm.hstypay.com/html/index.html#/wxrealname-contract-info";
                Intent intent = new Intent(getActivity(), RegisterActivity.class);
                intent.putExtra(Constants.REGISTER_INTENT, jumpUrl);
                intent.putExtra(Constants.REGISTER_ALWAYS_ALLOW_CLOSE, true);
                intent.putExtra(Constants.REGISTER_ALLOW_CACHE, true);
                startActivity(intent);*/
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri content_url = Uri.parse(jumpUrl);
                intent.setData(content_url);
                startActivity(intent);
            }
        });

        try {
            String wechatRealQrcodeUrl = mSignDetailDataList.get(position).getWechatRealQrcodeUrl();
            Bitmap bitmap = ImageUtil.base64ToBitmap(wechatRealQrcodeUrl);
            if (bitmap != null) {
                iv_qrcode_wx_sign_dialog.setImageBitmap(bitmap);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back://返回
                onBackPressed();
                break;
            default:
                break;
        }
    }

    private void getServiceList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                map.put("serviceProviderName", mEtServiceInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getServiceList(MyApplication.getContext(), "TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getSalesmanList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                map.put("empName", mEtSalesmanInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getSalesmanList(MyApplication.getContext(), "TAG_SALESMAN_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    //获取签约管理列表（目前仅微信实名认证） isOptionRefresh 是否是操作后刷新状态，是则不用转圈了，避免接着转圈
    private void getWxSignMerchantList(boolean isOptionRefreshForShowLoad) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!(isPullRefresh || isLoadmore || isOptionRefreshForShowLoad)) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            if (mStatus != -1) {
                map.put("status", mStatus);
            }
            if (!TextUtils.isEmpty(mEtInput.getText().toString().trim())) {
                map.put("q", mEtInput.getText().toString().trim());
            }
            map.put("pageSize", mPageSize);
            map.put("currentPage", mCurrentPage);
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mCurSelectSalesmanId))
                map.put("salesmanId", mCurSelectSalesmanId);
            map.put("contractType", "INTENT_ALI_AUTH".equals(mIntentName) ? "14" : "5");
            ServerClient.newInstance(MyApplication.getContext()).wxSignMerchantList(MyApplication.getContext(), Constants.TAG_WX_SIGN_LIST, map);
            mLastRequestStatus = mStatus;
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetSignListData(NoticeEvent event) {
        //签约商户列表数据返回
        if (event.getTag().equals(Constants.TAG_WX_SIGN_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            setSwipeRefreshState(isPullRefresh, isLoadmore, 500);
            SignMerchantListBean msg = (SignMerchantListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(WxRealAuthorMerchantListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg != null && msg.getData() != null) {
                        if (msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                            if (mCurrentPage == 1) {
                                mSignDetailDataList.clear();
                            }
                            mSignDetailDataList.addAll(msg.getData().getData());
                            mWxRealMerchantAdapter.setSignListDatas(mSignDetailDataList);
                            mCurrentPage++;
                        } else {
                            if (isLoadmore) {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                            } else {
                                mSignDetailDataList.clear();//使后面会展示空视图
                                mWxRealMerchantAdapter.setSignListDatas(mSignDetailDataList);
                            }
                        }
                    } else {
                        if (isLoadmore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mSignDetailDataList.clear();//使后面会展示空视图
                            mWxRealMerchantAdapter.setSignListDatas(mSignDetailDataList);
                        }
                    }

                    if (mSignDetailDataList != null && mSignDetailDataList.size() > 0) {
                        mTvNull.setVisibility(View.GONE);
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                    } else {
                        mTvNull.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayout.setVisibility(View.GONE);
                    }
                    break;
            }
            isLoadmore = false;
            isPullRefresh = false;
        } else if (event.getTag().equals("TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ServiceProviderBean msg = (ServiceProviderBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(WxRealAuthorMerchantListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mServiceDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchServiceProviderList.clear();
                            if (TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {

                                mSearchServiceProviderList.add(mAllServiceProvider);
                            }
                        }

                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchServiceProviderList.addAll(msg.getData().getData());
                        mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchServiceProviderList.clear();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mServiceDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mServiceDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_SALESMAN_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SalesmanBean msg = (SalesmanBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(WxRealAuthorMerchantListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mSalesmanDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchSalesmanList.clear();
                            if (TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {

                                mSearchSalesmanList.add(mAllSalesman);
                            }
                        }
                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchSalesmanList.addAll(msg.getData().getData());
                        mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchSalesmanList.clear();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mSalesmanDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mSalesmanDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        }
    }

    /**
     * 关闭下拉刷新和上拉加载的进度条
     */
    private void setSwipeRefreshState(boolean isPullRefresh, boolean isLoadmore, int delay) {
        if (isPullRefresh) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }


    //撤销申请单提示
    public void showRemindCancelReplyDialog(int position) {
        CustomViewFullScreenDialog unBindDialog = new CustomViewFullScreenDialog(this);
        unBindDialog.setView(R.layout.dialog_device_unbind_tip);
        Button btn_not_unbind = unBindDialog.findViewById(R.id.btn_not_unbind);
        Button btn_to_unbind = unBindDialog.findViewById(R.id.btn_to_unbind);
        TextView tv_title_unbind_dialog = unBindDialog.findViewById(R.id.tv_title_unbind_dialog);
        TextView tv_content_unbind_dialog = unBindDialog.findViewById(R.id.tv_content_unbind_dialog);
        tv_title_unbind_dialog.setText(getResources().getString(R.string.title_cancel_reply_tip));
        tv_content_unbind_dialog.setText(getResources().getString(R.string.content_cancel_reply_tip));
        unBindDialog.show();
        btn_not_unbind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unBindDialog.dismiss();
            }
        });
        btn_to_unbind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unBindDialog.dismiss();
                cancelReply(position);
            }
        });
    }


    //重新签约
    private void againSignAuth(int position) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("id", mSignDetailDataList.get(position).getId());
            ServerClient.newInstance(MyApplication.getContext()).wxAuthReSign(MyApplication.getContext(), Constants.TAG_WX_SIGN_AGAIN + getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    //撤销申请单
    private void cancelReply(int position) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("id", mSignDetailDataList.get(position).getId());
            ServerClient.newInstance(MyApplication.getContext()).cancelWxSignReply(MyApplication.getContext(), Constants.TAG_WX_SIGN_CANCEL + getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_WX_SIGN_CANCEL + getClass().getSimpleName())) {
            //撤销申请单
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(WxRealAuthorMerchantListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(WxRealAuthorMerchantListActivity.this, "提示", msg.getError().getMessage(), null);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    //撤销成功，刷新状态
                    ToastUtil.showToastShort(getResources().getString(R.string.opt_success));
                    mCurrentPage = 1;
                    getWxSignMerchantList(true);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_WX_SIGN_AGAIN + getClass().getSimpleName())) {
            //重新签约
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(WxRealAuthorMerchantListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(WxRealAuthorMerchantListActivity.this, "提示", msg.getError().getMessage(), null);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    // 重新签约，刷新状态
                    ToastUtil.showToastShort(getResources().getString(R.string.opt_success));
                    mCurrentPage = 1;
                    getWxSignMerchantList(true);
                    break;
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_WX_SIGN_DETAIL) {
            if (resultCode == RESULT_OK) {
                mCurrentPage = 1;
                getWxSignMerchantList(false);
            }
        }
    }
}