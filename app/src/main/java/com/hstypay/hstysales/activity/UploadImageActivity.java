package com.hstypay.hstysales.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hstypay.hstysales.BuildConfig;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.MerChantModifyBean;
import com.hstypay.hstysales.bean.MerchantInfoBean;
import com.hstypay.hstysales.bean.PictureBean;
import com.hstypay.hstysales.bean.UploadImageBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.FileUtils;
import com.hstypay.hstysales.utils.ImageFactory;
import com.hstypay.hstysales.utils.ImagePase;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CheckPictureDialog;
import com.hstypay.hstysales.widget.MyGridView;
import com.hstypay.hstysales.widget.NoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.SelectDialog;
import com.hstypay.hstysales.widget.SelectImagePopupWindow;
import com.hstypay.hstysales.widget.SelectPicPopupWindow;
import com.hstypay.hstysales.widget.ShadowLayout;
import com.shehuan.niv.NiceImageView;
import com.squareup.picasso.Picasso;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;

/**
 * Created by admin on 2017/7/11.
 */

public class UploadImageActivity extends BaseActivity implements View.OnClickListener {

    private ImageView iv_back;
    private TextView tv_title;
    private LinearLayout ly_phone_two, ly_phone_three, ly_phone_six, ly_phone_seven, ly_phone_one, ly_phone_four, ly_phone_five;
    private TextView mBtnEnsure;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    /**
     * 拍照保存照片的uri
     */
    private Uri originalUri = null;
    private File tempFile;
    private String ispicture = "1";
    private String picOnePath, picTowPath, picThreePath, picFourPath, picFivePath, picSixPath, picSevenPath, picEightPath, picNinePath, picTenPath, picElevenPath, picTwelvePath, picThirteenPath,

    picFourteenPath,picFifteenPath,picSixteenPath,picSeventeenPath,picEighteenPath,picNineteenPath,picTwentyPath,pictwentyOnePath,picTwentyTwoPath,picTwentyThreePath;
    private String picOnePath1, picTowPath1, picThreePath1, picFourPath1, picFivePath1, picSixPath1, picSevenPath1;

    /**
     * 1.营业执照 2.身份证正面 3.身份证背面 4.门头照 5.开户许可证 6.银行卡正面照 7.手持身份证照
     */
    private LinearLayout ly_first, ly_three, ly_four, ly_fifth, ly_sixth, ly_seventh;
    private ImageView iv_pic_one, iv_pic_three, iv_pic_four, iv_pic_five, iv_pic_six, iv_pic_seven;
    private TextView tv_check_example;
    private NiceImageView iv_pic_two;

    boolean takepic = false;

    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE1 = 101;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE2 = 102;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE3 = 103;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE4 = 104;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE5 = 105;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE6 = 106;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE7 = 107;

    private static final int MERCHANT_PERSON = 108;
    private static final int MERCHANT_COMPANY_PUBLIC = 109;
    private static final int MERCHANT_COMPANY_PRIVATE = 110;
    private int mType = -1;

    private SelectDialog mDialog;
    private MerchantInfoBean.DataBean mData;
    private SafeDialog mLoadDialog;
    // private ShadowLayout mShadowButton;
    private TextView tv_pic_info;
    private TextView tv_two, tv_three, tv_seven, tv_six, tv_four, tv_one, tv_five;
    private MyGridView gv_activy;
    private NiceImageView imageView;
    private NiceImageView imageNew;
    private boolean isClick = true;
    private String basePath;
    private RelativeLayout rl_upload;
    private boolean del1,del2,del3,del4,del5,del6,del7,del8,del9,del10,del11,del12,del13,del14,del15,del16,del17,del18,del19,del20,del21,del22,del23;


    private String path1,path2,path3,path4,path5,path6,path7,path8,path9,path10,path11,path12,path13,path14,path15,path16,path17,path18,path19,path20,path21,path22,path23;

    private String changePic,authorizatioPic;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE1:
                LogUtil.d("PermissionRequest" + PermissionUtils.verifyPermissions(grantResults));
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    // Permission Granted
                } else {
                    showDialog();
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE2:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    // Permission Granted
                } else {
                    showDialog();
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE3:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    // Permission Granted
                } else {
                    showDialog();
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE4:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    // Permission Granted
                } else {
                    showDialog();
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE5:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    // Permission Granted
                } else {
                    showDialog();
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE6:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    // Permission Granted
                } else {
                    showDialog();
                }
                break;
            case MY_PERMISSIONS_REQUEST_CALL_PHONE7:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    // Permission Granted
                } else {
                    showDialog();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void showDialog() {
        if (mDialog == null) {
            mDialog = new SelectDialog(this, UIUtils.getString(R.string.dialog_notice_camera)
                    , UIUtils.getString(R.string.btn_setting), UIUtils.getString(R.string.btn_cancel_text),
                    new SelectDialog.HandleBtn() {
                        @Override
                        public void handleOkBtn() {
                            Intent intent = getAppDetailSettingIntent(UploadImageActivity.this);
                            startActivity(intent);
                        }

                        @Override
                        public void handleCancleBtn() {
                        }
                    });
        }
        DialogHelper.resize(this, mDialog);
        mDialog.show();
    }

    private void initListener() {

        iv_back.setOnClickListener(this);
        ly_phone_one.setOnClickListener(this);
        ly_phone_two.setOnClickListener(this);
        ly_phone_three.setOnClickListener(this);
        ly_phone_four.setOnClickListener(this);
        ly_phone_five.setOnClickListener(this);
        ly_phone_six.setOnClickListener(this);
        ly_phone_seven.setOnClickListener(this);
        mBtnEnsure.setOnClickListener(this);
        tv_check_example.setOnClickListener(this);
    }

    private void initView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_pic_info = findViewById(R.id.tv_pic_info);
        SpannableString spannableString = new SpannableString(getString(R.string.tv_pic_info));
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#FD414B")), 0, 3, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tv_pic_info.setText(spannableString);
        tv_title.setText(UIUtils.getString(R.string.title_merchant_photos));
        ly_phone_one = (LinearLayout) findViewById(R.id.ly_phone_one);
        ly_phone_two = (LinearLayout) findViewById(R.id.ly_phone_two);
        ly_phone_three = (LinearLayout) findViewById(R.id.ly_phone_three);
        ly_phone_four = (LinearLayout) findViewById(R.id.ly_phone_four);
        ly_phone_five = (LinearLayout) findViewById(R.id.ly_phone_five);
        ly_phone_six = (LinearLayout) findViewById(R.id.ly_phone_six);
        ly_phone_seven = (LinearLayout) findViewById(R.id.ly_phone_seven);
        //  mShadowButton = (ShadowLayout) findViewById(R.id.shadow_button);
        mBtnEnsure = (TextView) findViewById(R.id.btn_common_enable);
        // mBtnUnsure = (TextView) findViewById(R.id.btn_common_unable);
        iv_pic_one = (ImageView) findViewById(R.id.iv_pic_one);
        ly_first = (LinearLayout) findViewById(R.id.ly_first);
        ly_three = (LinearLayout) findViewById(R.id.ly_three);
        ly_four = (LinearLayout) findViewById(R.id.ly_four);
        ly_fifth = (LinearLayout) findViewById(R.id.ly_fifth);
        ly_sixth = (LinearLayout) findViewById(R.id.ly_sixth);
        ly_seventh = (LinearLayout) findViewById(R.id.ly_seventh);
        iv_pic_two = findViewById(R.id.iv_pic_two);
        iv_pic_three = (ImageView) findViewById(R.id.iv_pic_three);
        iv_pic_four = (ImageView) findViewById(R.id.iv_pic_four);
        iv_pic_five = (ImageView) findViewById(R.id.iv_pic_five);
        iv_pic_six = (ImageView) findViewById(R.id.iv_pic_six);
        iv_pic_seven = (ImageView) findViewById(R.id.iv_pic_seven);
        tv_check_example = (TextView) findViewById(R.id.tv_cheak_example);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        tv_two = findViewById(R.id.tv_two);
        tv_three = findViewById(R.id.tv_three);
        tv_seven = findViewById(R.id.tv_seven);
        tv_six = findViewById(R.id.tv_six);
        tv_four = findViewById(R.id.tv_four);
        tv_one = findViewById(R.id.tv_one);
        tv_five = findViewById(R.id.tv_five);

        gv_activy = findViewById(R.id.gv_activy);
        spannabText(tv_two, "* 身份证正面照");
        spannabText(tv_three, "* 身份证反面照");
        spannabText(tv_six, "* 银行卡照 (带卡号)");
        spannabText(tv_seven, "* 手持身份证照");
        spannabText(tv_five, "* 开户许可证");
        spannabText(tv_four, "* 门头照");
        spannabText(tv_one, "* 营业执照");

    }

    private void spannabText(TextView view, String s) {
        SpannableString spantwo = new SpannableString(s);
        spantwo.setSpan(new ForegroundColorSpan(Color.parseColor("#FD414B")), 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        view.setText(spantwo);
    }

    private void initData() {

        List<PictureBean> mList = new ArrayList<>();
        PictureBean idCard = new PictureBean();
        PictureBean idFord = new PictureBean();
        PictureBean handId = new PictureBean();
        PictureBean bankCard = new PictureBean();
        PictureBean company = new PictureBean();
        PictureBean license = new PictureBean();
        PictureBean bkLicense = new PictureBean();
        PictureBean pic8 = new PictureBean();
        PictureBean pic9 = new PictureBean();
        PictureBean pic10 = new PictureBean();
        PictureBean pic11 = new PictureBean();
        PictureBean pic12 = new PictureBean();
        PictureBean pic13 = new PictureBean();
        PictureBean pic14 = new PictureBean();
        PictureBean pic15 = new PictureBean();
        PictureBean pic16 = new PictureBean();
        PictureBean pic17 = new PictureBean();
        PictureBean pic18 = new PictureBean();
        PictureBean pic19 = new PictureBean();
        PictureBean pic20 = new PictureBean();
        PictureBean pic21 = new PictureBean();
        PictureBean pic22 = new PictureBean();
        PictureBean pic23 = new PictureBean();
        mData = (MerchantInfoBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_NAME_MERCHANT_UPLOAD);


        switch (mData.getMchQueryStatus()) {

            case -1:
                //初始商户
                setExitEable(false);
                break;
            case 1:
                //审核通过
                setExitEable(false);
                break;
            case 5:
                //作废商户
                setExitEable(false);
                break;
            case 10:
                //变更审核通过
                setExitEable(false);
                break;
            case 101:
                //冻结商户
                setExitEable(false);
                break;
        }

        MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();
        if (!StringUtils.isEmptyOrNull(mchDetail.getIdCardPhotos())) {

                String[] split = mchDetail.getIdCardPhotos().split(",");
                if (split != null && split.length > 0) {
                    if (split.length == 1) {
                        String card = split[0];
                        if(!StringUtils.isEmptyOrNull(card.trim())) {
                            idCard.setUrl(Constants.BASE_URL + split[0]);
                            idCard.setMust(true);
                            idCard.setPicId(2);
                            idCard.setPicName(getString(R.string.tv_identification_photo_front));
                        }else {
                            idCard.setMust(true);
                            idCard.setPicId(2);
                            idCard.setPicName(getString(R.string.tv_identification_photo_front));
                        }

                        idFord.setMust(true);
                        idFord.setPicId(3);
                        idFord.setPicName(getString(R.string.tv_identification_photo_back));

                    } else if (split.length == 2) {
                        String card = split[0];
                        if(!StringUtils.isEmptyOrNull(card.trim())) {
                            idCard.setUrl(Constants.BASE_URL + split[0]);
                            idCard.setMust(true);
                            idCard.setPicId(2);
                            idCard.setPicName(getString(R.string.tv_identification_photo_front));
                        }else {

                            idCard.setMust(true);
                            idCard.setPicId(2);
                            idCard.setPicName(getString(R.string.tv_identification_photo_front));
                        }


                        String cardBack = split[1];
                        if(!StringUtils.isEmptyOrNull(cardBack.trim())) {

                            idFord.setUrl(Constants.BASE_URL + split[1]);
                            idFord.setMust(true);
                            idFord.setPicId(3);
                            idFord.setPicName(getString(R.string.tv_identification_photo_back));
                        }else {
                            idFord.setMust(true);
                            idFord.setPicId(3);
                            idFord.setPicName(getString(R.string.tv_identification_photo_back));

                        }
                    }
                }

            } else {
                //idCard.setUrl(Constants.BASE_URL);
                idCard.setMust(true);
                idCard.setPicId(2);
                idCard.setPicName(getString(R.string.tv_identification_photo_front));

                // idFord.setUrl(Constants.BASE_URL);
                idFord.setMust(true);
                idFord.setPicId(3);
                idFord.setPicName(getString(R.string.tv_identification_photo_back));
            }

            if (!StringUtils.isEmptyOrNull(mchDetail.getHandIdcardPhoto())) {
                //手持身份证照片
                handId.setUrl(Constants.BASE_URL + mchDetail.getHandIdcardPhoto());
                handId.setMust(false);
                handId.setPicId(7);
                handId.setPicName(getString(R.string.tv_identification_photo_hand));
            } else {
                handId.setMust(false);
                handId.setPicId(7);
                handId.setPicName(getString(R.string.tv_identification_photo_hand));
            }

            if (!StringUtils.isEmptyOrNull(mchDetail.getBkCardPhoto())) {
                //银行卡
                bankCard.setUrl(Constants.BASE_URL + mchDetail.getBkCardPhoto());
                bankCard.setMust(false);
                bankCard.setPicId(6);
                bankCard.setPicName("银行卡正面照");
            } else {
                bankCard.setMust(false);
                bankCard.setPicId(6);
                bankCard.setPicName("银行卡正面照");
            }


            if (!StringUtils.isEmptyOrNull(mchDetail.getCompanyPhoto())) {
                //门头照
                company.setUrl(Constants.BASE_URL + mchDetail.getCompanyPhoto());
                company.setMust(false);
                company.setPicId(4);
                company.setPicName("店铺门头照");
            } else {

                company.setMust(false);
                company.setPicId(4);
                company.setPicName("店铺门头照");
            }


            if (!StringUtils.isEmptyOrNull(mchDetail.getLicensePhoto())) {
                //营业执照
                license.setUrl(Constants.BASE_URL + mchDetail.getLicensePhoto());
                license.setMust(false);
                license.setPicId(1);
                license.setPicName("营业执照照片");
            } else {

                license.setUrl(Constants.BASE_URL);
                license.setMust(false);
                license.setPicId(1);
                license.setPicName("营业执照照片");
            }

            if (!StringUtils.isEmptyOrNull(mchDetail.getBkLicensePhoto())) {
                //开户许可证
                bkLicense.setUrl(Constants.BASE_URL + mchDetail.getBkLicensePhoto());
                bkLicense.setMust(true);
                bkLicense.setPicId(5);
                bkLicense.setPicName("开户许可证");
            } else {
                //  bkLicense.setUrl(Constants.BASE_URL);
                bkLicense.setMust(true);
                bkLicense.setPicId(5);
                bkLicense.setPicName("开户许可证");
            }


            if(!StringUtils.isEmptyOrNull(mchDetail.getAccountChangePhoto())){
                pic8.setPicId(8);
                pic8.setMust(false);
                pic8.setPicName("变更申请函");
                pic8.setUrl(Constants.BASE_URL + mchDetail.getAccountChangePhoto());

            }else {
                pic8.setPicId(8);
                pic8.setMust(false);
                pic8.setPicName("变更申请函");
            }
        if(!StringUtils.isEmptyOrNull(mchDetail.getThirdAuthPhoto())){
            pic9.setUrl(Constants.BASE_URL +mchDetail.getThirdAuthPhoto());
            pic9.setPicId(9);
            pic9.setMust(false);
            pic9.setPicName("第三方授权函");
        }else {
            pic9.setPicId(9);
            pic9.setMust(false);
            pic9.setPicName("第三方授权函");
        }


        if(!StringUtils.isEmptyOrNull(mchDetail.getOrgPhoto())){
            pic10.setUrl(Constants.BASE_URL +mchDetail.getOrgPhoto());
            pic10.setPicId(10);
            pic10.setMust(false);
            pic10.setPicName("工信网证明照 ");

        }else {
            pic10.setPicId(10);
            pic10.setMust(false);
            pic10.setPicName("工信网证明照 ");
        }

        if(!StringUtils.isEmptyOrNull(mchDetail.getBussinessPlacePhoto())) {
            pic11.setUrl(Constants.BASE_URL +mchDetail.getBussinessPlacePhoto());
            pic11.setPicId(11);
            pic11.setMust(false);
            pic11.setPicName("经营场所内景照一");
        }else {

            pic11.setPicId(11);
            pic11.setMust(false);
            pic11.setPicName("经营场所内景照一");
        }

        if(!StringUtils.isEmptyOrNull(mchDetail.getBussinessPlacePhoto2())) {
            pic11.setUrl(Constants.BASE_URL + mchDetail.getBussinessPlacePhoto2());
            pic12.setPicId(12);
            pic12.setMust(false);
            pic12.setPicName("经营场所内景照二");
        }else {
            pic12.setPicId(12);
            pic12.setMust(false);
            pic12.setPicName("经营场所内景照二");

        }


        if(!StringUtils.isEmptyOrNull(mchDetail.getSupplementPhotos())){

            String[] split = mchDetail.getSupplementPhotos().split(",");
            if (split != null && split.length > 0) {
                if (split.length == 1) {

                    String s = split[0];
                    if(!StringUtils.isEmptyOrNull(s.trim())) {
                        pic13.setUrl(Constants.BASE_URL + split[0]);
                        pic13.setPicId(13);
                        pic13.setMust(false);
                        pic13.setPicName("商户协议照");


                        //商户补充照片 逗号隔开，第一个是商户协议照，第二个是收银台照（商户协议照，在事业单位中为必填的单位证明函）
                        pic22.setUrl(Constants.BASE_URL + split[0]);
                        pic22.setPicId(22);
                        pic22.setMust(true);
                        pic22.setPicName("单位证明函");


                        pic14.setPicId(14);
                        pic14.setMust(false);
                        pic14.setPicName("收银台照");
                    }else {
                        pic13.setPicId(13);
                        pic13.setMust(false);
                        pic13.setPicName("商户协议照");


                        pic22.setPicId(22);
                        pic22.setMust(true);
                        pic22.setPicName("单位证明函");

                        pic14.setPicId(14);
                        pic14.setMust(false);
                        pic14.setPicName("收银台照");
                    }

                } else if (split.length == 2) {

                    String s = split[0];
                    if(!StringUtils.isEmptyOrNull(s.trim())) {
                        pic13.setUrl(Constants.BASE_URL + split[0]);
                        pic13.setPicId(13);
                        pic13.setMust(false);
                        pic13.setPicName("商户协议照");

                        //商户补充照片 逗号隔开，第一个是商户协议照，第二个是收银台照（商户协议照，在事业单位中为必填的单位证明函）
                        pic22.setUrl(Constants.BASE_URL + split[0]);
                        pic22.setPicId(22);
                        pic22.setMust(true);
                        pic22.setPicName("单位证明函");
                    }else {

                        pic13.setPicId(13);
                        pic13.setMust(false);
                        pic13.setPicName("商户协议照");


                        pic22.setPicId(22);
                        pic22.setMust(true);
                        pic22.setPicName("单位证明函");
                    }

                    String s1 = split[1];
                    if(!StringUtils.isEmptyOrNull(s1.trim())) {
                        pic14.setUrl(Constants.BASE_URL + split[1]);
                        pic14.setPicId(14);
                        pic14.setMust(false);
                        pic14.setPicName("收银台照");
                    }else {
                        pic14.setPicId(14);
                        pic14.setMust(false);
                        pic14.setPicName("收银台照");

                    }
                }
            }

        }else {
            pic13.setPicId(13);
            pic13.setMust(false);
            pic13.setPicName("商户协议照");

            pic14.setPicId(14);
            pic14.setMust(false);
            pic14.setPicName("收银台照");


            pic22.setPicId(22);
            pic22.setMust(true);
            pic22.setPicName("单位证明函");
        }


        if(!StringUtils.isEmptyOrNull(mchDetail.getContactCertPhoto())){

            String[] split = mchDetail.getContactCertPhoto().split(",");
            if (split != null && split.length > 0) {
                if (split.length == 1) {
                    String s = split[0];
                    if(!StringUtils.isEmptyOrNull(s.trim())) {
                        pic15.setUrl(Constants.BASE_URL + split[0]);
                        pic15.setPicId(15);
                        pic15.setMust(false);
                        pic15.setPicName("联系人证件正面照");
                    }else {
                        pic15.setPicId(15);
                        pic15.setMust(false);
                        pic15.setPicName("联系人证件正面照");

                    }

                    pic16.setPicId(16);
                    pic16.setMust(false);
                    pic16.setPicName("联系人证件反面照");

                } else if (split.length == 2) {

                    String s = split[0];
                    if(!StringUtils.isEmptyOrNull(s.trim())) {
                        pic15.setUrl(Constants.BASE_URL + split[0]);
                        pic15.setPicId(15);
                        pic15.setMust(false);
                        pic15.setPicName("联系人证件正面照");
                    }else {

                        pic15.setPicId(15);
                        pic15.setMust(false);
                        pic15.setPicName("联系人证件正面照");
                    }

                    String s1 = split[1];
                    if(!StringUtils.isEmptyOrNull(s1.trim())) {
                        pic16.setUrl(Constants.BASE_URL + split[1]);
                        pic16.setPicId(16);
                        pic16.setMust(false);
                        pic16.setPicName("联系人证件反面照");
                    }else {

                        pic16.setPicId(16);
                        pic16.setMust(false);
                        pic16.setPicName("联系人证件反面照");
                    }
                }
            }

        }else {

            pic15.setPicId(15);
            pic15.setMust(false);
            pic15.setPicName("联系人证件正面照");

            pic16.setPicId(16);
            pic16.setMust(false);
            pic16.setPicName("联系人证件反面照");
        }


            if(!StringUtils.isEmptyOrNull(mchDetail.getContactHandCertPhoto())){
                pic17.setUrl(Constants.BASE_URL +mchDetail.getContactHandCertPhoto() );
                pic17.setPicId(17);
                pic17.setMust(false);
                pic17.setPicName("联系人手执证件照");

            }else {
                pic17.setPicId(17);
                pic17.setMust(false);
                pic17.setPicName("联系人手执证件照");
            }


            if(!StringUtils.isEmptyOrNull(mchDetail.getOtherPhoto())){

                String[] split = mchDetail.getOtherPhoto().split(",");
                if (split != null && split.length > 0) {
                    if (split.length == 1) {

                        String s = split[0];
                        if(!StringUtils.isEmptyOrNull(s.trim())) {
                            pic18.setUrl(Constants.BASE_URL + split[0]);
                            pic18.setPicId(18);
                            pic18.setMust(false);
                            pic18.setPicName("其他图片一");
                        }else {
                            pic18.setPicId(18);
                            pic18.setMust(false);
                            pic18.setPicName("其他图片一");

                        }


                        pic19.setPicId(19);
                        pic19.setMust(false);
                        pic19.setPicName("其他图片二");


                        pic20.setPicId(20);
                        pic20.setMust(false);
                        pic20.setPicName("其他图片三");


                        pic21.setPicId(21);
                        pic21.setMust(false);
                        pic21.setPicName("其他图四");

                    } else if (split.length == 2) {


                        String s = split[0];
                        if(!StringUtils.isEmptyOrNull(s.trim())) {
                            pic18.setUrl(Constants.BASE_URL + split[0]);
                            pic18.setPicId(18);
                            pic18.setMust(false);
                            pic18.setPicName("其他图片一");
                        }else {
                            pic18.setPicId(18);
                            pic18.setMust(false);
                            pic18.setPicName("其他图片一");

                        }

                        String s1 = split[1];
                        if(!StringUtils.isEmptyOrNull(s1.trim())) {
                            pic19.setUrl(Constants.BASE_URL + split[1]);
                            pic19.setPicId(19);
                            pic19.setMust(false);
                            pic19.setPicName("其他图片二");
                        }else {
                            pic19.setPicId(19);
                            pic19.setMust(false);
                            pic19.setPicName("其他图片二");

                        }


                        pic20.setPicId(20);
                        pic20.setMust(false);
                        pic20.setPicName("其他图片三");


                        pic21.setPicId(21);
                        pic21.setMust(false);
                        pic21.setPicName("其他图四");
                    }else if (split.length == 3) {
                        String s = split[0];
                        if(!StringUtils.isEmptyOrNull(s.trim())) {
                            pic18.setUrl(Constants.BASE_URL + split[0]);
                            pic18.setPicId(18);
                            pic18.setMust(false);
                            pic18.setPicName("其他图片一");
                        }else {
                            pic18.setPicId(18);
                            pic18.setMust(false);
                            pic18.setPicName("其他图片一");

                        }

                        String s1 = split[1];
                        if(!StringUtils.isEmptyOrNull(s1.trim())) {
                            pic19.setUrl(Constants.BASE_URL + split[1]);
                            pic19.setPicId(19);
                            pic19.setMust(false);
                            pic19.setPicName("其他图片二");
                        }else {
                            pic19.setPicId(19);
                            pic19.setMust(false);
                            pic19.setPicName("其他图片二");

                        }


                        String s2 = split[2];
                        if(!StringUtils.isEmptyOrNull(s2.trim())) {
                            pic20.setUrl(Constants.BASE_URL + split[2]);
                            pic20.setPicId(20);
                            pic20.setMust(false);
                            pic20.setPicName("其他图片三");

                        }else {
                            pic20.setPicId(20);
                            pic20.setMust(false);
                            pic20.setPicName("其他图片三");

                        }

                        pic21.setPicId(21);
                        pic21.setMust(false);
                        pic21.setPicName("其他图四");
                    }else if (split.length == 4) {
                        String s = split[0];
                        if(!StringUtils.isEmptyOrNull(s.trim())) {
                            pic18.setUrl(Constants.BASE_URL + split[0]);
                            pic18.setPicId(18);
                            pic18.setMust(false);
                            pic18.setPicName("其他图片一");
                        }else {
                            pic18.setPicId(18);
                            pic18.setMust(false);
                            pic18.setPicName("其他图片一");

                        }

                        String s1 = split[1];
                        if(!StringUtils.isEmptyOrNull(s1.trim())) {
                            pic19.setUrl(Constants.BASE_URL + split[1]);
                            pic19.setPicId(19);
                            pic19.setMust(false);
                            pic19.setPicName("其他图片二");
                        }else {
                            pic19.setPicId(19);
                            pic19.setMust(false);
                            pic19.setPicName("其他图片二");

                        }


                        String s2 = split[2];
                        if(!StringUtils.isEmptyOrNull(s2.trim())) {
                            pic20.setUrl(Constants.BASE_URL + split[2]);
                            pic20.setPicId(20);
                            pic20.setMust(false);
                            pic20.setPicName("其他图片三");

                        }else {
                            pic20.setPicId(20);
                            pic20.setMust(false);
                            pic20.setPicName("其他图片三");

                        }


                        String s3 = split[3];
                        if(!StringUtils.isEmptyOrNull(s3.trim())) {
                            pic21.setUrl(Constants.BASE_URL + split[3]);
                            pic21.setPicId(21);
                            pic21.setMust(false);
                            pic21.setPicName("其他图四");
                        }else {

                            pic21.setPicId(21);
                            pic21.setMust(false);
                            pic21.setPicName("其他图四");
                        }
                    }
                }

            }else {
                pic18.setPicId(18);
                pic18.setMust(false);
                pic18.setPicName("其他图片一");

                pic19.setPicId(19);
                pic19.setMust(false);
                pic19.setPicName("其他图片二");

                pic20.setPicId(20);
                pic20.setMust(false);
                pic20.setPicName("其他图片三");

                pic21.setPicId(21);
                pic21.setMust(false);
                pic21.setPicName("其他图四");
            }

            if(!StringUtils.isEmptyOrNull(mchDetail.getBkCardPhotoBack())){

                pic23.setUrl(mchDetail.getBkCardPhotoBack());
                pic23.setPicId(23);
                pic23.setMust(false);
                pic23.setPicName("银行卡反面照");
            }else {

                pic23.setPicId(23);
                pic23.setMust(false);
                pic23.setPicName("银行卡反面照");
            }



        MerchantInfoBean.DataBean.BankAccountBean bankAccount = mData.getBankAccount();

            switch (mData.getOutMchType()){
                case 1:
                    //企业商户
                    if(bankAccount.getAccountType()==1){
                        //企业
                        mList.add(pic8);
                        mList.add(pic9);
                        idCard.setMust(true);
                        idFord.setMust(true);
                        license.setMust(true);
                        company.setMust(true);
                        bkLicense.setMust(true);
                        mList.add(idCard);
                        mList.add(idFord);
                        mList.add(license);
                        mList.add(company);
                        mList.add(bkLicense);
                        mList.add(pic10);
                        mList.add(handId);
                        mList.add(pic11);
                        mList.add(pic12);
                        mList.add(pic13);
                        mList.add(pic14);
                        mList.add(pic15);
                        mList.add(pic16);
                        mList.add(pic17);
                        mList.add(pic18);
                        mList.add(pic19);
                        mList.add(pic20);
                        mList.add(pic21);

                    }else {
                        //个人
                        mList.add(pic8);
                        mList.add(pic9);
                        idCard.setMust(true);
                        idFord.setMust(true);
                        license.setMust(true);
                        bankCard.setMust(true);
                        company.setMust(true);
                        mList.add(idCard);
                        mList.add(idFord);
                        mList.add(license);
                        mList.add(bankCard);
                        mList.add(company);
                        mList.add(pic10);
                        mList.add(handId);
                        mList.add(pic11);
                        mList.add(pic12);
                        mList.add(pic13);
                        mList.add(pic14);
                        mList.add(pic23);
                        mList.add(pic15);
                        mList.add(pic16);
                        mList.add(pic17);
                        mList.add(pic18);
                        mList.add(pic19);
                        mList.add(pic20);
                        mList.add(pic21);

                    }

                    break;
                case 2:
                    //个体工商户
                    mList.add(pic8);
                    mList.add(pic9);
                    idCard.setMust(true);
                    idFord.setMust(true);
                    license.setMust(true);
                    bankCard.setMust(true);
                    company.setMust(true);
                    mList.add(idCard);
                    mList.add(idFord);
                    mList.add(license);
                    mList.add(bankCard);
                    mList.add(company);
                    mList.add(pic10);
                    mList.add(handId);
                    mList.add(pic11);
                    mList.add(pic12);
                    mList.add(pic13);
                    mList.add(pic14);
                    mList.add(pic23);
                    mList.add(pic15);
                    mList.add(pic16);
                    mList.add(pic17);
                    mList.add(pic18);
                    mList.add(pic19);
                    mList.add(pic20);
                    mList.add(pic21);

                    break;
                case 3:
                    //个体经营者
                    mList.add(pic8);
                    mList.add(pic9);
                    handId.setMust(true);
                    idCard.setMust(true);
                    idFord.setMust(true);
                    bankCard.setMust(true);
                    company.setMust(true);
                    mList.add(idCard);
                    mList.add(idFord);
                    mList.add(bankCard);
                    mList.add(company);
                    mList.add(handId);
                    mList.add(pic13);
                    mList.add(pic11);
                    mList.add(pic12);
                    mList.add(pic14);
                    mList.add(pic23);
                    mList.add(pic15);
                    mList.add(pic16);
                    mList.add(pic17);
                    mList.add(pic18);
                    mList.add(pic19);
                    mList.add(pic20);
                    mList.add(pic21);

                    break;
                case 4:
                    //事业单位

                    if(bankAccount.getAccountType()==1) {
                        //企业

                        mList.add(pic8);
                        mList.add(pic9);

                        idCard.setMust(true);
                        idFord.setMust(true);
                        license.setMust(true);
                        company.setMust(true);
                        pic22.setMust(true);
                        bkLicense.setMust(true);
                        mList.add(idCard);
                        mList.add(idFord);
                        mList.add(license);
                        mList.add(company);
                        mList.add(pic22);
                        mList.add(bkLicense);
                        mList.add(handId);
                        mList.add(pic11);
                        mList.add(pic12);
                        mList.add(pic14);
                        mList.add(pic15);
                        mList.add(pic16);
                        mList.add(pic17);
                        mList.add(pic18);
                        mList.add(pic19);
                        mList.add(pic20);
                        mList.add(pic21);
                    }else {
                        mList.add(pic8);
                        mList.add(pic9);
                        idCard.setMust(true);
                        idFord.setMust(true);
                        license.setMust(true);
                        bankCard.setMust(true);
                        company.setMust(true);
                        pic22.setMust(true);
                        mList.add(idCard);
                        mList.add(idFord);
                        mList.add(license);
                        mList.add(bankCard);
                        mList.add(company);
                        mList.add(pic22);
                        mList.add(handId);
                        mList.add(pic11);
                        mList.add(pic12);
                        mList.add(pic14);
                        mList.add(pic23);


                        mList.add(pic15);
                        mList.add(pic16);
                        mList.add(pic17);

                        mList.add(pic18);
                        mList.add(pic19);
                        mList.add(pic20);
                        mList.add(pic21);


                    }

                    break;
            }

            MyAdape adape = new MyAdape(UploadImageActivity.this, mList);
            gv_activy.setAdapter(adape);



    }

    private void setExitEable(boolean b) {
        mBtnEnsure.setVisibility(View.GONE);
        isClick = false;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            //营业执照
            case R.id.ly_phone_one:
                PermissionUtils.checkPermissionArray(UploadImageActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE1);
                ispicture = "1";
                choice();
                break;
            //正面照
            case R.id.ly_phone_two:
                PermissionUtils.checkPermissionArray(UploadImageActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE2);
                ispicture = "2";
                choice();
                break;
            //反面照
            case R.id.ly_phone_three:
                PermissionUtils.checkPermissionArray(UploadImageActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE3);
                ispicture = "3";
                choice();
                break;
            //门头照
            case R.id.ly_phone_four:
                PermissionUtils.checkPermissionArray(UploadImageActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE4);
                ispicture = "4";
                choice();
                break;
            //开户许可照
            case R.id.ly_phone_five:
                PermissionUtils.checkPermissionArray(UploadImageActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE5);
                ispicture = "5";
                choice();
                break;
            //银行卡正面照
            case R.id.ly_phone_six:
                PermissionUtils.checkPermissionArray(UploadImageActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE6);
                ispicture = "6";
                choice();
                break;
            //手持身份证照
            case R.id.ly_phone_seven:
                PermissionUtils.checkPermissionArray(UploadImageActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE7);
                ispicture = "7";
                choice();
                break;
            //确认上传
            case R.id.btn_common_enable:

                submitData();
                break;
            //查看范例
            case R.id.tv_cheak_example:
                checkExample();
                break;
        }
    }

    private void submitData() {

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {

            Map<String, Object> map = new HashMap<>();
            Map<String, String> mchMap = new HashMap<>();
            map.put("merchantId",mData.getMerchantId());
            map.put("merchantName",mData.getMerchantName());

            if(!StringUtils.isEmptyOrNull(picOnePath)){
                mchMap.put("licensePhoto",picOnePath);
            }

            if(!StringUtils.isEmptyOrNull(picTowPath) && !StringUtils.isEmptyOrNull(picThreePath)){

                mchMap.put("idCardPhotos",picTowPath+","+picThreePath);
            }else {

                if(!StringUtils.isEmptyOrNull(picTowPath)){

                    mchMap.put("idCardPhotos",picTowPath);
                }else if(!StringUtils.isEmptyOrNull(picThreePath)){

                    mchMap.put("idCardPhotos",","+picThreePath);
                }

            }

            if(!StringUtils.isEmptyOrNull(picFourPath)){

                mchMap.put("companyPhoto",picFourPath);
            }

            if(!StringUtils.isEmptyOrNull(picFivePath)){

                mchMap.put("bkLicensePhoto",picFivePath);
            }

            if(!StringUtils.isEmptyOrNull(picSixPath)){

                mchMap.put("bkCardPhoto",picSixPath);
            }

            if(!StringUtils.isEmptyOrNull(picSevenPath)){

                mchMap.put("handIdcardPhoto",picSevenPath);
            }


            if(!StringUtils.isEmptyOrNull(picEightPath)){

                mchMap.put("accountChangePhoto",picEightPath);
            }

            if(!StringUtils.isEmptyOrNull(picNinePath)){

                mchMap.put("thirdAuthPhoto",picNinePath);
            }

            if(!StringUtils.isEmptyOrNull(picTenPath)){

                mchMap.put("orgPhoto",picTenPath);
            }


            if(!StringUtils.isEmptyOrNull(picElevenPath)){

                mchMap.put("bussinessPlacePhoto",picElevenPath);
            }


            if(!StringUtils.isEmptyOrNull(picTwelvePath)){

                mchMap.put("bussinessPlacePhoto2",picTwelvePath);
            }


            if(!StringUtils.isEmptyOrNull(picThirteenPath) && !StringUtils.isEmptyOrNull(picFourteenPath)){

                mchMap.put("supplementPhotos",picThirteenPath+","+picFourteenPath);
            }else {
                if(!StringUtils.isEmptyOrNull(picThirteenPath)){

                    mchMap.put("supplementPhotos",picThirteenPath);
                }else if(!StringUtils.isEmptyOrNull(picFourteenPath)){

                    mchMap.put("supplementPhotos",","+picFourteenPath);
                }
            }


            if(!StringUtils.isEmptyOrNull(picFifteenPath) && !StringUtils.isEmptyOrNull(picSixteenPath)){

                mchMap.put("contactCertPhoto",picTwelvePath+","+picSixteenPath);
            }else {

                if(!StringUtils.isEmptyOrNull(picFifteenPath)){

                    mchMap.put("contactCertPhoto",picFifteenPath);
                }else if(!StringUtils.isEmptyOrNull(picSixteenPath)){

                    mchMap.put("contactCertPhoto",","+picSixteenPath);
                }

            }



            if(!StringUtils.isEmptyOrNull(picSeventeenPath)){

                mchMap.put("contactHandCertPhoto",picSeventeenPath);
            }


            if(!StringUtils.isEmptyOrNull(picEighteenPath) && !StringUtils.isEmptyOrNull(picNineteenPath)

            && !StringUtils.isEmptyOrNull(picTwentyPath)  && !StringUtils.isEmptyOrNull(pictwentyOnePath)){

                mchMap.put("otherPhoto",picEighteenPath+","+picNineteenPath+","+picTwentyPath+","+pictwentyOnePath);
            }else {

                if(!StringUtils.isEmptyOrNull(picEighteenPath)){
                    mchMap.put("otherPhoto",picEighteenPath);
                }else if(!StringUtils.isEmptyOrNull(picEighteenPath) && !StringUtils.isEmptyOrNull(picNineteenPath)){

                    mchMap.put("otherPhoto",picEighteenPath+","+picNineteenPath);
                }else if(!StringUtils.isEmptyOrNull(picEighteenPath) && !StringUtils.isEmptyOrNull(picNineteenPath)
                        && !StringUtils.isEmptyOrNull(picTwentyPath)){

                    mchMap.put("otherPhoto",picEighteenPath+","+picNineteenPath+","+picTwentyPath);
                }else if(!StringUtils.isEmptyOrNull(picNineteenPath)){

                    mchMap.put("otherPhoto",","+picNineteenPath);
                }else if(!StringUtils.isEmptyOrNull(picNineteenPath) && !StringUtils.isEmptyOrNull(picTwentyPath)){

                    mchMap.put("otherPhoto",","+picNineteenPath+","+picTwentyPath);
                }else if (!StringUtils.isEmptyOrNull(picNineteenPath) && !StringUtils.isEmptyOrNull(picTwentyPath)
                &&  !StringUtils.isEmptyOrNull(pictwentyOnePath)){

                    mchMap.put("otherPhoto",","+picNineteenPath+","+picTwentyPath+","+pictwentyOnePath);
                }else if(!StringUtils.isEmptyOrNull(picTwentyPath)){

                    mchMap.put("otherPhoto",","+","+picTwentyPath);
                }else if(!StringUtils.isEmptyOrNull(picTwentyPath)
                        &&  !StringUtils.isEmptyOrNull(pictwentyOnePath)){

                    mchMap.put("otherPhoto",","+","+picTwentyPath+","+pictwentyOnePath);

                }else if(!StringUtils.isEmptyOrNull(pictwentyOnePath)){

                    mchMap.put("otherPhoto",","+","+","+pictwentyOnePath);
                }
            }

            if(!StringUtils.isEmptyOrNull(picTwentyTwoPath)){

                mchMap.put("supplementPhotos",picTwentyTwoPath);
            }
            if(!StringUtils.isEmptyOrNull(picTwentyThreePath)){

                mchMap.put("bkCardPhotoBack",picTwentyThreePath);
            }


            map.put("detail",mchMap);
            UploadImageActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtil.safeShowDialog(mLoadDialog);
                }
            });

            ServerClient.newInstance(MyApplication.getContext()).submitMerchantInfo(MyApplication.getContext(), Constants.TAG_SUBMIT_MERCHANT_INFO, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }

    }

    private void checkExample() {
        CheckPictureDialog dialog = new CheckPictureDialog(UploadImageActivity.this);
        DialogHelper.resize(UploadImageActivity.this, dialog);
        dialog.show();
    }

    private void UploadImage() {

        if (!NetworkUtils.isNetworkAvailable(UploadImageActivity.this)) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
            return;
        }
        DialogUtil.safeShowDialog(mLoadDialog);

        //compressImage();

        String url = Constants.BASE_URL + "/app/merchant/file/upload";
        PostFormBuilder post = OkHttpUtils.post();
        if (!TextUtils.isEmpty(picOnePath)) {
            //picOnePath1 = picOnePath.substring(0, picOnePath.lastIndexOf(".")) + "(1).jpg";
            picOnePath1 = AppHelper.getImageCacheDir(picOnePath);
            LogUtil.d("pic====" + picOnePath + ",,," + picOnePath1);
            ImageFactory.compressPicture(picOnePath, picOnePath1);
            post = post.addFile("license.jpg", "license", new File(picOnePath1));
        }
        if (!TextUtils.isEmpty(picTowPath)) {
            //picTowPath1 = picTowPath.substring(0, picTowPath.lastIndexOf(".")) + "(1).jpg";
            picTowPath1 = AppHelper.getImageCacheDir(picTowPath);
            LogUtil.d("pic====" + picTowPath + ",,," + picTowPath1);
            ImageFactory.compressPicture(picTowPath, picTowPath1);
            post = post.addFile("picfilezm.jpg", "picfilezm", new File(picTowPath1));
        }
        if (!TextUtils.isEmpty(picThreePath)) {
            //picThreePath1 = picThreePath.substring(0, picThreePath.lastIndexOf(".")) + "(1).jpg";
            picThreePath1 = AppHelper.getImageCacheDir(picThreePath);
            LogUtil.d("pic====" + picThreePath + ",,," + picThreePath1);
            ImageFactory.compressPicture(picThreePath, picThreePath1);
            post = post.addFile("picfilefm.jpg", "picfilefm", new File(picThreePath1));
        }
        if (!TextUtils.isEmpty(picFourPath)) {
            //picFourPath1 = picFourPath.substring(0, picFourPath.lastIndexOf(".")) + "(1).jpg";
            picFourPath1 = AppHelper.getImageCacheDir(picFourPath);
            LogUtil.d("pic====" + picFourPath + ",,," + picFourPath1);
            ImageFactory.compressPicture(picFourPath, picFourPath1);
            post = post.addFile("store.jpg", "store", new File(picFourPath1));
        }
        if (!TextUtils.isEmpty(picFivePath)) {
            //picFivePath1 = picFivePath.substring(0, picFivePath.lastIndexOf(".")) + "(1).jpg";
            picFivePath1 = AppHelper.getImageCacheDir(picFivePath);
            LogUtil.d("pic====" + picFivePath + ",,," + picFivePath1);
            ImageFactory.compressPicture(picFivePath, picFivePath1);
            post = post.addFile("bkLicensePhoto.jpg", "bkLicensePhoto", new File(picFivePath1));
        }
        if (!TextUtils.isEmpty(picSixPath)) {
            //picSixPath1 = picSixPath.substring(0, picSixPath.lastIndexOf(".")) + "(1).jpg";
            picSixPath1 = AppHelper.getImageCacheDir(picSixPath);
            LogUtil.d("pic====" + picSixPath + ",,," + picSixPath1);
            ImageFactory.compressPicture(picSixPath, picSixPath1);
            post = post.addFile("bkCardPhoto.jpg", "bkCardPhoto", new File(picSixPath1));
        }
        if (!TextUtils.isEmpty(picSevenPath)) {
            //picSevenPath1 = picSevenPath.substring(0, picSevenPath.lastIndexOf(".")) + "(1).jpg";
            picSevenPath1 = AppHelper.getImageCacheDir(picSevenPath);
            LogUtil.d("pic====" + picSevenPath + ",,," + picSevenPath1);
            ImageFactory.compressPicture(picSevenPath, picSevenPath1);
            post = post.addFile("handIdcardPhoto.jpg", "handIdcardPhoto", new File(picSevenPath1));
        }
        post.url(url).build().
                connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                e.printStackTrace();
                LogUtil.i("zhouwei", "onErroreee//..>>>" + e);
                dismissLoading();
                ToastUtil.showToastShort("上传失败");
                FileUtils.deleteFile(picOnePath1);
                FileUtils.deleteFile(picTowPath1);
                FileUtils.deleteFile(picThreePath1);
                FileUtils.deleteFile(picFourPath1);
                FileUtils.deleteFile(picFivePath1);
                FileUtils.deleteFile(picSixPath1);
                FileUtils.deleteFile(picSevenPath1);
            }

            @Override
            public void onResponse(String response, int id) {
                LogUtil.i("zhouwei", "response" + response);
                FileUtils.deleteFile(picOnePath1);
                FileUtils.deleteFile(picTowPath1);
                FileUtils.deleteFile(picThreePath1);
                FileUtils.deleteFile(picFourPath1);
                FileUtils.deleteFile(picFivePath1);
                FileUtils.deleteFile(picSixPath1);
                FileUtils.deleteFile(picSevenPath1);
                dismissLoading();
                Gson gson = new Gson();
                UploadImageBean uploadImageBean = gson.fromJson(response, UploadImageBean.class);
                if (uploadImageBean != null) {
                    if (uploadImageBean.isStatus()) {
                        UploadImageBean.DataEntity data = uploadImageBean.getData();
                        if (data != null) {
                            Intent intent = new Intent();
                            Bundle mBundle = new Bundle();
                            mBundle.putSerializable(Constants.RESULT_UPLOAD_IMAGE_INTENT, data);   //传递一个user对象列表
                            intent.putExtras(mBundle);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    } else {
                        if (uploadImageBean.getError() != null && uploadImageBean.getError().getMessage() != null) {
                            ToastUtil.showToastShort(uploadImageBean.getError().getMessage());
                        }
                    }
                }
            }
        });
    }

    private void choice() {
        final String status = Environment.getExternalStorageState();
        SelectPicPopupWindow picPopupWindow = new SelectPicPopupWindow(UploadImageActivity.this, new SelectPicPopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    try {
                        startCamrae();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                try {
                    if (status.equals(Environment.MEDIA_MOUNTED)) {
                        takeImg();
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.tx_sd_pic));
                    }
                } catch (Exception e) {

                }
            }
        });

        picPopupWindow.showAtLocation(iv_pic_one, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }


    private void choicePic(PictureBean bean) {
        final String status = Environment.getExternalStorageState();
        SelectImagePopupWindow popupWindow = new SelectImagePopupWindow(UploadImageActivity.this, bean, basePath,new SelectImagePopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    try {
                        startCamrae();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                try {
                    if (status.equals(Environment.MEDIA_MOUNTED)) {
                        takeImg();
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.tx_sd_pic));
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void delete(PictureBean be) {

                //删除图片
                initDelete(be);

            }

            @Override
            public void download(PictureBean be) {

            }
        });

        popupWindow.showAtLocation(iv_pic_one, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void initDelete(PictureBean bean) {

        if(bean!=null){

            switch (bean.getPicId()){

                case 1:
                    del1=true;
                    path1="";
                    picOnePath="";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf1));
                        imageNew.postInvalidate();
                    }

                    break;
                case 2:
                    del2=true;
                    path2="";
                    picTowPath="";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf2));
                        imageNew.postInvalidate();
                    }
                    break;
                case 3:
                    del3=true;
                    path3="";
                    picThreePath="";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf3));
                        imageNew.postInvalidate();
                    }
                    break;
                case 4:
                    del4=true;
                    path4="";
                    picFourPath="";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf4));
                        imageNew.postInvalidate();
                    }
                    break;
                case 5:
                    del5=true;
                    path5="";
                    picFivePath="";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf5));
                        imageNew.postInvalidate();
                    }
                    break;
                case 6:
                    del6=true;
                    path6="";
                    picSixPath="";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf6));
                        imageNew.postInvalidate();
                    }
                    break;
                case 7:
                    del7=true;
                    path7="";
                    picSevenPath="";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf7));
                        imageNew.postInvalidate();
                    }
                    break;
                case 8:
                    del8=true;
                    path8="";
                    picEightPath="";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 9:
                    del9=true;
                    path9="";
                    picNinePath="";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 10:
                    del10=true;
                    path10="";
                    picTenPath="";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf10));
                        imageNew.postInvalidate();
                    }
                    break;
                case 11:
                    del11=true;
                    path11="";
                    picElevenPath="";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf11));
                        imageNew.postInvalidate();
                    }


                    break;
                case 12:
                    del12=true;
                    path12="";
                    picTwelvePath="";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf11));
                        imageNew.postInvalidate();
                    }
                    break;
                case 13:
                    del13=true;
                    path13="";
                    picThirteenPath="";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf13));
                        imageNew.postInvalidate();
                    }
                    break;
                case 14:
                    del14=true;
                    path14="";
                    picFourteenPath="";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 15:
                    del15=true;
                    path15="";
                    picFifteenPath="";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf2));
                        imageNew.postInvalidate();
                    }
                    break;
                case 16:
                    del16=true;
                    path16="";
                    picSixteenPath="";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf3));
                        imageNew.postInvalidate();
                    }
                    break;
                case 17:
                    del17=true;
                    path17="";
                    picSeventeenPath="";

                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                        imageNew.setImageDrawable(UploadImageActivity.this.getResources().getDrawable(R.mipmap.upload_sf7));
                        imageNew.postInvalidate();
                    }
                    break;
                case 18:
                    del18=true;
                    path18="";
                    picEighteenPath="";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 19:
                    del19=true;
                    path19="";
                    picNineteenPath="";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 20:
                    del20=true;
                    path20="";
                    picTwentyPath="";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 21:
                    del21=true;
                    path21="";
                    pictwentyOnePath="";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 22:
                    del22=true;
                    path22="";
                    picTwentyTwoPath="";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
                case 23:
                    del23=true;
                    path23="";
                    picTwentyThreePath="";
                    imageView.setVisibility(View.GONE);
                    imageView.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.VISIBLE);
                        rl_upload.postInvalidate();
                    }
                    break;
            }
        }

    }


    private void takeImg() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);

    }

    String imageUrl;

    private void startCamrae() throws Exception {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String root = AppHelper.getImgCacheDir();
        imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";

        tempFile = new File(imageUrl);
        if (!tempFile.getParentFile().exists()){
            tempFile.getParentFile().mkdirs();
        }
        if (Build.VERSION.SDK_INT >= 24) {
            //如果是则使用FileProvider
            originalUri = FileProvider.getUriForFile(UploadImageActivity.this, BuildConfig.APPLICATION_ID + ".fileProvider", tempFile);
        } else {
            originalUri = Uri.fromFile(tempFile);
        }
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                //获取相册图片
                case REQUEST_CODE_TAKE_PICTURE:
                    if (data.getData() == null) {
                        return;
                    }
                    String path = getPicPath(data.getData());

                    Bitmap bitmap = null;
                    if (path != null) {
                        takepic = false;
                        /*File file = new File(path);
                        int MAX_POST_SIZE = 8 * 1024 * 1024;
                        if (file.exists() && file.length() > MAX_POST_SIZE) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.tv_pic_than_5MB));
                            return;
                        } else {
                            bitmap = ImagePase.readBitmapFromStream(path);
                        }*/
                        bitmap = ImagePase.readBitmapFromStream(path);
                        if (bitmap != null) {
                            if (ispicture.equals("1")) {
                                //picOnePath = path;
                                // setImageBitmap(iv_pic_one,bitmap);
                               /*
                                setBitmapView(imageView, bitmap);*/
                               del1=false;
                               path1=path;
                                uploadimg(path);
                            } else if (ispicture.equals("2")) {
                               // picTowPath = path;
                                //setImageBitmap(iv_pic_two,bitmap);
                                //setBitmapView(imageView, bitmap);
                                del2=false;
                                path2=path;
                                uploadimg(path);
                            } else if (ispicture.equals("3")) {
                                //picThreePath = path;
                                //setImageBitmap(iv_pic_three,bitmap);
                                //setBitmapView(imageView, bitmap);
                                del3=false;
                                path3=path;
                                uploadimg(path);
                            } else if (ispicture.equals("4")) {
                                //picFourPath = path;
                                //setImageBitmap(iv_pic_four,bitmap);
                                //setBitmapView(imageView, bitmap);
                                uploadimg(path);
                                path4=path;
                                del4=false;
                            } else if (ispicture.equals("5")) {
                               // picFivePath = path;
                                //setImageBitmap(iv_pic_five,bitmap);
                               // setBitmapView(imageView, bitmap);
                                del5=false;
                                path5=path;
                                uploadimg(path);
                            } else if (ispicture.equals("6")) {
                              //  picSixPath = path;
                                // setImageBitmap(iv_pic_six,bitmap);
                               // setBitmapView(imageView, bitmap);
                                del6=false;
                                path6=path;
                                uploadimg(path);
                            } else if (ispicture.equals("7")) {
                               // picSevenPath = path;
                                //setImageBitmap(iv_pic_seven,bitmap);
                                //setBitmapView(imageView, bitmap);
                                del7=false;
                                path7=path;
                                uploadimg(path);
                            } else if (ispicture.equals("8")) {
                                /*picEightPath = path;
                                setBitmapView(imageView, bitmap);*/
                                del8=false;
                                path8=path;
                                uploadimg(path);
                            } else if (ispicture.equals("9")) {
                               /* picNinePath = path;
                                setBitmapView(imageView, bitmap);*/
                                del9=false;
                                path9=path;
                                uploadimg(path);
                            } else if (ispicture.equals("10")) {
                               /* picTenPath = path;
                                setBitmapView(imageView, bitmap);*/
                                del10=false;
                                path10=path;
                                uploadimg(path);
                            } else if (ispicture.equals("11")) {
                               /* picElevenPath = path;
                                setBitmapView(imageView, bitmap);*/
                                del11=false;
                                path11=path;
                                uploadimg(path);
                            } else if (ispicture.equals("12")) {
                              /*  picTwelvePath = path;
                                setBitmapView(imageView, bitmap);*/
                                del12=false;
                                path12=path;
                                uploadimg(path);
                            } else if (ispicture.equals("13")) {
                                /*picThirteenPath = path;
                                setBitmapView(imageView, bitmap);*/
                                del13=false;
                                path13=path;
                                uploadimg(path);
                            }else if (ispicture.equals("14")) {
                                del14=false;
                                path14=path;
                                uploadimg(path);
                            }else if (ispicture.equals("15")) {
                                del15=false;
                                path15=path;
                                uploadimg(path);
                            }else if (ispicture.equals("16")) {
                                del16=false;
                                path16=path;
                                uploadimg(path);
                            }else if (ispicture.equals("17")) {

                                del17=false;
                                path17=path;
                                uploadimg(path);
                            }else if (ispicture.equals("18")) {

                                del18=false;
                                path18=path;
                                uploadimg(path);
                            }else if (ispicture.equals("19")) {
                                del19=false;
                                path19=path;
                                uploadimg(path);
                            }else if (ispicture.equals("20")) {

                                del20=false;
                                path20=path;
                                uploadimg(path);
                            }else if (ispicture.equals("21")) {

                                del21=false;
                                path21=path;
                                uploadimg(path);
                            }else if (ispicture.equals("22")) {

                                del22=false;
                                path22=path;
                                uploadimg(path);
                            }else if (ispicture.equals("23")) {

                                del23=false;
                                path23=path;
                                uploadimg(path);
                            }
                        }
                    }
                    break;

                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    String pathPhoto = getPicPath(originalUri);
                    LogUtil.d("path=" + pathPhoto);
                    takepic = true;
                    /*Bitmap bitmap_pci = null;
                    File file = new File(pathPhoto);
                    if (file.exists() && file.length() / 1024 > 100) {
                        bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                    } else {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                            bitmap_pci = ImagePase.createBitmap(imageUrl, ImagePase.bitmapSize_Image);
                        }
                    }*/
                    Bitmap bitmap_pci = null;
                    File file = new File(pathPhoto);
                    if (!file.exists()) {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                        }
                    }
                    bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                    if (bitmap_pci != null) {
                        if (ispicture.equals("1")) {
                            del1=false;
                            path1=pathPhoto;
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("2")) {
                           // picTowPath = pathPhoto;
                            //setImageBitmap(iv_pic_two,bitmap_pci);
                            //setBitmapView(imageView, bitmap_pci);
                            uploadimg(pathPhoto);

                            del2=false;
                            path2=pathPhoto;
                        } else if (ispicture.equals("3")) {
                           // picThreePath = pathPhoto;
                            // setImageBitmap(iv_pic_three,bitmap_pci);
                            //setBitmapView(imageView, bitmap_pci);
                            del3=false;
                            path3=pathPhoto;
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("4")) {
                           /* picFourPath = pathPhoto;
                            setBitmapView(imageView, bitmap_pci);*/
                            //setImageBitmap(iv_pic_four,bitmap_pci);

                            del4=false;
                            path4=pathPhoto;
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("5")) {
                            /*picFivePath = pathPhoto;
                            setBitmapView(imageView, bitmap_pci);*/
                            //setImageBitmap(iv_pic_five,bitmap_pci);
                            del5=false;
                            path5=pathPhoto;
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("6")) {
                           /* picSixPath = pathPhoto;
                            setBitmapView(imageView, bitmap_pci);*/
                            //setImageBitmap(iv_pic_six,bitmap_pci);
                            del6=false;
                            path6=pathPhoto;
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("7")) {
                           // picSevenPath = pathPhoto;
                            //setImageBitmap(iv_pic_seven,bitmap_pci);
                            //setBitmapView(imageView, bitmap_pci);
                            del7=false;
                            path7=pathPhoto;
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("8")) {
                           /* picEightPath = pathPhoto;
                            setBitmapView(imageView, bitmap_pci);*/
                            del8=false;
                            path8=pathPhoto;
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("9")) {
                           /* picNinePath = pathPhoto;
                            setBitmapView(imageView, bitmap_pci);*/
                            del9=false;
                            path9=pathPhoto;
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("10")) {
                           /* picTenPath = pathPhoto;
                            setBitmapView(imageView, bitmap_pci);*/
                            del10=false;
                            path10=pathPhoto;
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("11")) {
                           /* picElevenPath = pathPhoto;
                            setBitmapView(imageView, bitmap_pci);*/
                            del11=false;
                            path11=pathPhoto;
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("12")) {
                            /*picTwelvePath = pathPhoto;
                            setBitmapView(imageView, bitmap_pci);*/
                            del12=false;
                            path12=pathPhoto;
                            uploadimg(pathPhoto);
                        } else if (ispicture.equals("13")) {
                            /*picThirteenPath = pathPhoto;
                            setBitmapView(imageView, bitmap_pci);*/
                            del13=false;
                            path13=pathPhoto;
                            uploadimg(pathPhoto);
                        }else if (ispicture.equals("14")) {

                            del4=false;
                            path14=pathPhoto;
                            uploadimg(pathPhoto);
                        }
                        else if (ispicture.equals("15")) {

                            del15=false;
                            path15=pathPhoto;
                            uploadimg(pathPhoto);
                        }
                        else if (ispicture.equals("16")) {

                            del16=false;
                            path16=pathPhoto;
                            uploadimg(pathPhoto);
                        }
                        else if (ispicture.equals("17")) {

                            del17=false;
                            path17=pathPhoto;
                            uploadimg(pathPhoto);
                        }
                        else if (ispicture.equals("18")) {

                            del18=false;
                            path18=pathPhoto;
                            uploadimg(pathPhoto);
                        }
                        else if (ispicture.equals("19")) {

                            del19=false;
                            path19=pathPhoto;
                            uploadimg(pathPhoto);
                        }
                        else if (ispicture.equals("20")) {

                            del20=false;
                            path20=pathPhoto;
                            uploadimg(pathPhoto);
                        }
                        else if (ispicture.equals("21")) {

                            del21=false;
                            path21=pathPhoto;
                            uploadimg(pathPhoto);
                        }else if (ispicture.equals("22")) {

                            del22=false;
                            path22=pathPhoto;
                            uploadimg(pathPhoto);
                        }
                        else if (ispicture.equals("23")) {

                            del23=false;
                            path23=pathPhoto;
                            uploadimg(pathPhoto);
                        }

                    }
                    break;
                default:
                    break;
            }
        }
    }

    private String picPath;

    private void uploadimg(final String path) {

        if (!NetworkUtils.isNetworkAvailable(UploadImageActivity.this)) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
            return;
        }
        DialogUtil.safeShowDialog(mLoadDialog);

        String url = Constants.BASE_URL + "/app/merchant/file/upload";
        PostFormBuilder post = OkHttpUtils.post();
        if (!TextUtils.isEmpty(path)) {
            //picOnePath1 = picOnePath.substring(0, picOnePath.lastIndexOf(".")) + "(1).jpg";
            picPath = AppHelper.getImageCacheDir(path);
            LogUtil.d("pic====" + path + ",,," + path);
            ImageFactory.compressPicture(path, picPath);
            post = post.addFile("license.jpg", "license", new File(picPath));

            // newPost(post, picPath);
        }


        post.url(url).build().
                connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                e.printStackTrace();
                LogUtil.i("zhouwei", "onErroreee//..>>>" + e);
                DialogUtil.safeCloseDialog(mLoadDialog);
                ToastUtil.showToastShort("上传失败");
                FileUtils.deleteFile(picPath);

            }

            @Override
            public void onResponse(String response, int id) {
                LogUtil.i("zhouwei", "response" + response);
                FileUtils.deleteFile(picPath);
                DialogUtil.safeCloseDialog(mLoadDialog);
                Gson gson = new Gson();
                UploadImageBean uploadImageBean = gson.fromJson(response, UploadImageBean.class);
                if (uploadImageBean != null) {
                    if (uploadImageBean.isStatus()) {
                        UploadImageBean.DataEntity data = uploadImageBean.getData();
                        if (data != null) {
                           /* Intent intent = new Intent();
                            Bundle mBundle = new Bundle();
                            mBundle.putSerializable(Constants.RESULT_UPLOAD_IMAGE_INTENT, data);   //传递一个user对象列表
                            intent.putExtras(mBundle);
                            setResult(RESULT_OK, intent);
                            finish();*/

                           imagePath(data);
                            Bitmap bitmap = ImagePase.readBitmapFromStream(path);
                            setBitmapView(imageView, bitmap);
                        }
                    } else {
                        if (uploadImageBean.getError() != null && uploadImageBean.getError().getMessage() != null) {
                            ToastUtil.showToastShort(uploadImageBean.getError().getMessage());
                        }
                    }
                }
            }
        });
    }

    private void imagePath(UploadImageBean.DataEntity data) {

        int p = Integer.parseInt(ispicture);

        switch (p) {
            case 1:
                picOnePath = data.getLicense();
                break;
            case 2:
                picTowPath = data.getLicense();
                break;
            case 3:
                picThreePath = data.getLicense();
                break;
            case 4:
                picFourPath = data.getLicense();
                break;
            case 5:
                picFivePath = data.getLicense();
                break;
            case 6:
                picSixPath = data.getLicense();
                break;
            case 7:
                picSevenPath = data.getLicense();

                break;
            case 8:
                picEightPath = data.getLicense();
                break;
            case 9:
                picNinePath = data.getLicense();
                break;
            case 10:
                picTenPath = data.getLicense();
                break;
            case 11:
                picElevenPath = data.getLicense();
                break;
            case 12:
                picTwelvePath = data.getLicense();
                break;
            case 13:
                picThirteenPath = data.getLicense();
                break;
            case 14:
                picFourteenPath = data.getLicense();
                break;
            case 15:
                picFifteenPath = data.getLicense();
                break;
            case 16:
                picSixteenPath = data.getLicense();
                break;
            case 17:
                picSeventeenPath = data.getLicense();
                break;
            case 18:
                picEighteenPath = data.getLicense();
                break;
            case 19:
                picNineteenPath = data.getLicense();
                break;
            case 20:
                picTwentyPath = data.getLicense();
                break;
            case 21:
                pictwentyOnePath = data.getLicense();
                break;
            case 22:
                picTwentyTwoPath = data.getLicense();
                break;
            case 23:
                picTwentyThreePath = data.getLicense();
                break;
        }

    }


    /**
     * 返回图片地址
     *
     * @param originalUri
     * @return String
     */
    @SuppressLint("NewApi")
    public String getPicPath(Uri originalUri) {
        ContentResolver mContentResolver = UploadImageActivity.this.getContentResolver();
        String originalPath = null;
        if ((originalUri + "").contains("/sdcard")) {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/sdcard"));
        } else if ((originalUri + "").contains("/data")) {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/data"));
        } else if ((originalUri + "").contains("/storage")) // 小米 手机 适配
        {
            originalPath = originalUri.toString().substring(originalUri.toString().indexOf("/storage"));
        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
                    && originalUri.toString().contains("documents")) {
                String wholeID = DocumentsContract.getDocumentId(originalUri);
                String id = wholeID.split(":")[1];
                String[] column = {MediaStore.Images.Media.DATA};
                String sel = MediaStore.Images.Media._ID + " = ?";
                Cursor cursor =
                        mContentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                column,
                                sel,
                                new String[]{id},
                                null);
                int columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    originalPath = cursor.getString(columnIndex);
                }
                cursor.close();
            } else {

                Cursor cursor = mContentResolver.query(originalUri, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    originalPath = cursor.getString(1);
                    cursor.close();
                }
            }

        }
        return originalPath;
    }

    private void setButton(int type) {
        switch (type) {
            case MERCHANT_PERSON:
                if (StringUtils.isEmptyOrNull(picTowPath) && StringUtils.isEmptyOrNull(picThreePath) &&
                        StringUtils.isEmptyOrNull(picFourPath) && StringUtils.isEmptyOrNull(picSixPath) && StringUtils.isEmptyOrNull(picSevenPath)) {
                    // mBtnUnsure.setVisibility(View.VISIBLE);
                    //  mShadowButton.setVisibility(View.GONE);
                    mBtnEnsure.setVisibility(View.GONE);
                } else {
                    // mBtnUnsure.setVisibility(View.GONE);
                    // mShadowButton.setVisibility(View.GONE);
                    mBtnEnsure.setVisibility(View.VISIBLE);
                }
                break;
            case MERCHANT_COMPANY_PRIVATE:
                if (StringUtils.isEmptyOrNull(picOnePath) && StringUtils.isEmptyOrNull(picTowPath) && StringUtils.isEmptyOrNull(picThreePath) &&
                        StringUtils.isEmptyOrNull(picFourPath) && StringUtils.isEmptyOrNull(picSixPath)) {
                    //mBtnUnsure.setVisibility(View.VISIBLE);
                    // mShadowButton.setVisibility(View.GONE);
                    mBtnEnsure.setVisibility(View.GONE);
                } else {
                    // mBtnUnsure.setVisibility(View.GONE);
                    // mShadowButton.setVisibility(View.GONE);
                    mBtnEnsure.setVisibility(View.VISIBLE);
                }
                break;
            case MERCHANT_COMPANY_PUBLIC:
                if (StringUtils.isEmptyOrNull(picOnePath) && StringUtils.isEmptyOrNull(picTowPath) && StringUtils.isEmptyOrNull(picThreePath) &&
                        StringUtils.isEmptyOrNull(picFourPath) && StringUtils.isEmptyOrNull(picFivePath)) {
                    // mBtnUnsure.setVisibility(View.VISIBLE);
                    // mShadowButton.setVisibility(View.GONE);
                    mBtnEnsure.setVisibility(View.GONE);
                } else {
                    //  mBtnUnsure.setVisibility(View.GONE);
                    // mShadowButton.setVisibility(View.GONE);
                    mBtnEnsure.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private void setBitmap(LinearLayout ll, RelativeLayout rl, Bitmap bitmap) {
        ll.setVisibility(View.GONE);
        rl.setBackground(new BitmapDrawable(bitmap));
        setButton(mType);
    }

    private void setImageBitmap(ImageView iv, Bitmap bitmap) {
        // ll.setVisibility(View.GONE);
        // rl.setBackground(new BitmapDrawable(bitmap));
        if (bitmap != null) {
            iv.setImageBitmap(bitmap);
        }

        setButton(mType);
    }

    private void setBitmapView(final NiceImageView iv, final Bitmap bitmap) {


        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (bitmap != null) {
                    iv.setImageBitmap(bitmap);
                    iv.setVisibility(View.VISIBLE);
                    iv.postInvalidate();
                    if (rl_upload != null) {
                        rl_upload.setVisibility(View.GONE);
                    }
                }

                setButton(mType);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }


    class MyAdape extends BaseAdapter {

        private Context mContext;
        private List<PictureBean> mList;

        public MyAdape(Context context, List<PictureBean> list) {

            this.mContext = context;
            this.mList = list;
        }

        @Override
        public int getCount() {

            return mList.size();
        }

        @Override
        public Object getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {


            if (convertView == null) {
                convertView = View.inflate(mContext, R.layout.image_gridview_item, null);
                holder = new ViewPayTypeHolder();
                holder.iv_pic = convertView.findViewById(R.id.iv_pic);
                holder.tv_pic_title = convertView.findViewById(R.id.tv_pic_title);
                holder.rl_upload = convertView.findViewById(R.id.rl_upload);
                holder.iv_upload_icon = convertView.findViewById(R.id.iv_upload_icon);
                holder.iv_pic_new = convertView.findViewById(R.id.iv_pic_new);
                convertView.setTag(holder);
            } else {
                holder = (ViewPayTypeHolder) convertView.getTag();
            }


            PictureBean bean = mList.get(position);

            if (bean != null) {

                if (bean.isMust()) {
                    SpannableString spantwo = new SpannableString("* " + bean.getPicName());
                    spantwo.setSpan(new ForegroundColorSpan(UIUtils.getColor(R.color.red)), 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                    holder.tv_pic_title.setText(spantwo);
                }else {
                    if (bean.getPicName() != null) {
                        holder.tv_pic_title.setText(bean.getPicName());
                    }
                }


                switch (bean.getPicId()) {
                    case 1:
                        //营业执照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf1).error(R.mipmap.upload_sf1).into(holder.iv_pic);
                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf1));
                        }
                        break;
                    case 2:
                        //身份证正面照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf2).error(R.mipmap.upload_sf2).into(holder.iv_pic);
                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf2));
                        }
                        break;
                    case 3:
                        //身份证反面照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf3).error(R.mipmap.upload_sf3).into(holder.iv_pic);

                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf3));
                        }
                        break;
                    case 4:
                        //门头照

                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf4).error(R.mipmap.upload_sf4).into(holder.iv_pic);

                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf4));
                        }
                        break;
                    case 5:
                        //开户许可证
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf5).error(R.mipmap.upload_sf5).into(holder.iv_pic);
                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf5));
                        }
                        break;
                    case 6:
                        //银行卡
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf6).error(R.mipmap.upload_sf6).into(holder.iv_pic);
                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf6));
                        }
                        break;
                    case 7:
                        //手持身份证
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf7).error(R.mipmap.upload_sf7).into(holder.iv_pic);
                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf7));
                        }
                        break;

                    case 8:
                        //变更申请函
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }

                    case 9:
                        //第三方授权函
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 10:
                        //工信网证明照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf10).error(R.mipmap.upload_sf10).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf10));
                        }
                        break;
                    case 11:

                        //经营场所内景照一
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf11).error(R.mipmap.upload_sf11).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf11));
                        }

                        break;
                    case 12:

                        //经营场所内景照二
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf11).error(R.mipmap.upload_sf11).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf11));
                        }

                        break;
                    case 13:

                        //商户协议照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf13).error(R.mipmap.upload_sf13).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf13));
                        }

                        break;

                    case 14:

                        //收银台
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }

                        break;

                    case 15:

                        //联系人证件正面照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf2).error(R.mipmap.upload_sf2).into(holder.iv_pic);
                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf2));
                        }

                        break;

                    case 16:

                        //联系人证件反面照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf3).error(R.mipmap.upload_sf3).into(holder.iv_pic);

                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf3));
                        }

                        break;

                    case 17:

                        //联系人手执身份证照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.upload_sf7).error(R.mipmap.upload_sf7).into(holder.iv_pic);
                        } else {

                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                            holder.iv_pic_new.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.upload_sf7));
                        }

                        break;

                    case 18:

                        //其他图片一
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }


                        break;

                    case 19:

                        //其他图片二
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }


                        break;

                    case 20:

                        //其他图片三
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 21:
                        //其他图片四
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 22:
                        //单位证明函
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 23:
                        //银行卡反面照
                        if (!StringUtils.isEmptyOrNull(bean.getUrl())) {
                            Picasso.get().load(bean.getUrl()).placeholder(R.mipmap.icon_general_noloading).error(R.mipmap.icon_general_noloading).into(holder.iv_pic);
                        } else {
                            holder.iv_pic.setVisibility(View.GONE);
                            holder.rl_upload.setVisibility(View.VISIBLE);
                        }
                        break;
                }

                if (!isClick) {

                    holder.iv_pic.setEnabled(false);
                    holder.rl_upload.setEnabled(false);
                }

                setListener(bean, convertView);
            }

            return convertView;
        }

        private void setListener(final PictureBean bean, final View v) {

            holder.rl_upload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageView = v.findViewById(R.id.iv_pic);
                    rl_upload = v.findViewById(R.id.rl_upload);
                    imageNew = v.findViewById(R.id.iv_pic_new);
                    basePath = "";
                    choiceImage(bean);
                }
            });

            holder.iv_pic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageView = view.findViewById(R.id.iv_pic);
                    rl_upload = v.findViewById(R.id.rl_upload);
                    imageNew = v.findViewById(R.id.iv_pic_new);
                    basePath = "";
                    choiceImage(bean);

                }
            });

        }

        private ViewPayTypeHolder holder;

        private void choiceImage(PictureBean bean) {

            switch (bean.getPicId()) {

                case 1:
                    //营业执照
                    ispicture = "1";
                    baseDel=del1;
                    basePath=path1;
                    //basePath = picOnePath;
                    break;
                case 2:
                    //身份证正面照
                    ispicture = "2";
                    baseDel=del2;
                    basePath=path2;
                    break;
                case 3:
                    //身份证反面照
                    ispicture = "3";
                    baseDel=del3;
                    basePath=path3;
                    break;
                case 4:
                    //门头照
                    ispicture = "4";
                    baseDel=del4;
                    basePath=path4;
                    break;
                case 5:
                    //开户许可证
                    ispicture = "5";
                    baseDel=del5;
                    basePath=path5;
                    break;
                case 6:
                    //银行卡
                    ispicture = "6";
                    baseDel=del6;
                    basePath=path6;
                    break;
                case 7:
                    //手持身份证
                    ispicture = "7";
                    baseDel=del7;
                    basePath=path7;
                    break;
                case 8:
                    //变更申请函
                    ispicture = "8";
                    baseDel=del8;
                    basePath=path8;
                    break;
                case 9:
                    //第三方授权函
                    ispicture = "9";
                    baseDel=del9;
                    basePath=path9;
                    break;
                case 10:
                    //工信网证明照
                    ispicture = "10";
                    baseDel=del10;
                    basePath=path10;
                    break;
                case 11:
                    //经营场所内景照一
                    ispicture = "11";
                    baseDel=del11;
                    basePath=path11;
                    break;
                case 12:
                    //经营场所内景照二
                    ispicture = "12";
                    baseDel=del12;
                    basePath=path12;
                    break;
                case 13:
                    //商户协议照
                    ispicture = "13";
                    baseDel=del13;
                    basePath=path13;
                    break;
                case 14:
                    //收银台
                    ispicture = "14";
                    baseDel=del14;
                    basePath=path14;
                    break;
                case 15:
                    //商户协议照
                    ispicture = "15";
                    baseDel=del15;
                    basePath=path15;
                    break;
                case 16:
                    //商户协议照
                    ispicture = "16";
                    baseDel=del16;
                    basePath=path16;
                    break;
                case 17:
                    //商户协议照
                    ispicture = "17";
                    baseDel=del17;
                    basePath=path17;
                    break;
                case 18:
                    //商户协议照
                    ispicture = "18";
                    baseDel=del18;
                    baseDel=del18;
                    basePath=path18;
                    break;
                case 19:
                    //商户协议照
                    ispicture = "19";
                    baseDel=del19;
                    basePath=path19;
                    break;
                case 20:
                    //商户协议照
                    ispicture = "20";
                    baseDel=del20;
                    basePath=path20;
                    break;
                case 21:
                    //商户协议照
                    ispicture = "21";
                    baseDel=del21;
                    basePath=path21;
                    break;
                case 22:
                    //商户协议照
                    ispicture = "22";
                    baseDel=del22;
                    basePath=path22;
                    break;
                case 23:
                    //商户协议照
                    ispicture = "23";
                    baseDel=del23;
                    basePath=path23;
                    break;
            }

            PermissionUtils.checkPermissionArray(UploadImageActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE2);
            choicePic(bean);
        }

        class ViewPayTypeHolder {
            NiceImageView iv_pic, iv_pic_new;
            ImageView iv_upload_icon;
            RelativeLayout rl_upload;
            TextView tv_pic_title;

        }
    }

    private boolean baseDel;

    @Override
    public boolean isNeedEventBus() {
        return true;
    }



    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {

        if (event.getTag().equals(Constants.TAG_SUBMIT_MERCHANT_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            MerChantModifyBean msg = (MerChantModifyBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.SUBMIT_MERCHANT_INFO_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(UploadImageActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.SUBMIT_MERCHANT_INFO_TRUE://请求成功
                    if (msg.isStatus()) {
                        NoticeDialog noticeDialog = new NoticeDialog(UploadImageActivity.this, null, null, R.layout.notice_dialog_submit);
                        noticeDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                            @Override
                            public void clickOk() {

                                MyApplication.isRefresh=true;
                                finish();
                            }
                        });
                        DialogHelper.resize(UploadImageActivity.this, noticeDialog);
                        noticeDialog.show();
                    }
                    break;
            }
        }


    }


}