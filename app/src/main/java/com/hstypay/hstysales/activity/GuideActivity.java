package com.hstypay.hstysales.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.SpStayUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class GuideActivity extends BaseActivity {

    private final static int[] PICS = new int[]{R.mipmap.start_image_01,
            R.mipmap.start_image_02,
            R.mipmap.start_image_03};
    private final static int[] SLOGANS = new int[]{R.string.guide_slogan1,
            R.string.guide_slogan2,
            R.string.guide_slogan3};
    private static final String TAG = "GuideUI";
    private ViewPager mPager;
    private Button mBtnStart;
    private LinearLayout mPointContainer;
    private View mSelectedPoint;
    private List<View> mViews;
    private int mSpace;
    private RelativeLayout mRlPointContainer;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
            ,Manifest.permission.READ_PHONE_STATE,Manifest.permission.ACCESS_WIFI_STATE,Manifest.permission.ACCESS_NETWORK_STATE
            ,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //去掉标题
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        //初始化view
        initView();
        //数据加载
        initData();
        PermissionUtils.checkPermissionArray(this, permissionArray, PermissionUtils.PERMISSION_REQUEST_CODE);
    }


    private void initView() {
        mPager = (ViewPager) findViewById(R.id.guide_pager);
        mPointContainer = (LinearLayout) findViewById(R.id.guide_point_container);
        mSelectedPoint = findViewById(R.id.guide_selected_point);
        mBtnStart = (Button) findViewById(R.id.guide_btn_start);
        mRlPointContainer = (RelativeLayout) findViewById(R.id.guide_point);
    }


    private void initData() {

        //初始化图片数据
        mViews = new ArrayList<>();
        for (int i = 0; i < PICS.length; i++) {
            ImageView iv = new ImageView(this);
            //设置图片
            View view = LayoutInflater.from(this).inflate(R.layout.item_guide, null);
            view.setBackgroundResource(R.color.white);
            ((ImageView) view.findViewById(R.id.guide_image)).setImageResource(PICS[i]);
            String slogan = getString(SLOGANS[i]);
            if (slogan.length() >= 4) {
                SpannableStringBuilder style = new SpannableStringBuilder(slogan);
                style.setSpan(new ForegroundColorSpan(UIUtils.getColor(R.color.theme_color)), slogan.length() - 4, slogan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                style.setSpan(new AbsoluteSizeSpan(27,true),slogan.length() - 4, slogan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
                ((TextView) view.findViewById(R.id.guide_text)).setText(style);
            } else {
                ((TextView) view.findViewById(R.id.guide_text)).setText(SLOGANS[i]);
            }
            mViews.add(view);
            //给装点的容器动态加载点
            ImageView point = new ImageView(this);
            point.setBackgroundResource(R.drawable.shape_guide_dot_normal);
            point.setScaleType(ImageView.ScaleType.CENTER);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(DensityUtils.dp2px(this, 8),
                    DensityUtils.dp2px(this, 8));
            params.leftMargin = DensityUtils.dp2px(this, 8);
            params.rightMargin = DensityUtils.dp2px(this, 8);

            mPointContainer.addView(point, params);
        }


        //监听页面布局改变
        mPointContainer.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mSpace = mPointContainer.getChildAt(1).getLeft() - mPointContainer.getChildAt(0).getLeft();
                        LogUtil.d(TAG, "space : " + mSpace);

                        mPointContainer.getViewTreeObserver()
                                .removeGlobalOnLayoutListener(this);
                    }
                });

        //给viewpager加载数据
        mPager.setAdapter(new GuideAdapter());//adapter--->list
        //设置viewpager滑动监听
        mPager.addOnPageChangeListener(new PagerListener());
        //设置点击
        mBtnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //保存已经不是第一次使用了
                SpStayUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_NOT_FIRST_USE, true);
                //跳转到主页
                Intent intent = new Intent(GuideActivity.this, LoginActivity.class);
                intent.putExtra(Constants.INTENT_NAME,Constants.INTENT_GUIDE_LOGIN);
                startActivity(intent);
                finish();
            }
        });

    }

    private class PagerListener
            implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            //滑动时的回调
            //     mSelectedPoint 设置他的marginLeft

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mSelectedPoint.getLayoutParams();
            //计算两点间的距离
            //            int space = DensityUtils.dp2px(GuideUI.this, 20);
            params.leftMargin = (int) (mSpace * position + mSpace * positionOffset + 0.5f);
            mSelectedPoint.setLayoutParams(params);
        }

        @Override
        public void onPageSelected(int position) {
            //页面选中时的回调

            //如果选中的是最后页面就显示button
            mBtnStart.setVisibility(position == (PICS.length - 1) ? View.VISIBLE : View.GONE);
            mRlPointContainer.setVisibility(position == (PICS.length - 1) ? View.GONE : View.VISIBLE);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            //滑动状态改变时的回调
        }
    }

    private class GuideAdapter
            extends PagerAdapter {

        @Override
        public int getCount() {
            if (mViews != null) {
                return mViews.size();
            }
            return 0;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = mViews.get(position);
            //添加到容器中
            container.addView(view);
            //返回标记
            return view;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
