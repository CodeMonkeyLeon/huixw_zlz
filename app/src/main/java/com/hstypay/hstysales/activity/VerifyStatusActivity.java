package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.widget.ShadowLayout;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class VerifyStatusActivity extends BaseActivity implements View.OnClickListener {


    private TextView mTvTitle,mTvVerifyNotice,mTvVerifyReason,mTvVerifyStatus;
    private ImageView mIvBack,mIvVerfyIcon;
    private Button mBtnEnsure;
    private ShadowLayout mSlButton;
    private static final int CHECKING = 3;
    private static final int CHECK_FAILED = 6;
    private int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        setContentView(R.layout.activity_verify_status);
        StatusBarUtil.setTranslucentStatus(this);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);

        mTvTitle.setText(getString(R.string.title_verify_status));
        //mIvBack.setVisibility(View.GONE);

        mIvVerfyIcon = (ImageView) findViewById(R.id.iv_verify_icon);
        mTvVerifyStatus = (TextView) findViewById(R.id.tv_verify_status);
        mTvVerifyNotice = (TextView) findViewById(R.id.tv_verify_notice);
        mTvVerifyReason = (TextView) findViewById(R.id.tv_verify_reason);
        mSlButton = (ShadowLayout) findViewById(R.id.sl_button);
        mBtnEnsure = (Button) findViewById(R.id.btn_common_enable);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnEnsure.setOnClickListener(this);
    }

    private void initData(){
        status = getIntent().getIntExtra(Constants.INTENT_OPEN_MEMBER,-1);
        switch (status){
            case CHECKING:
                mIvVerfyIcon.setImageResource(R.mipmap.icon_verify_ing);
                mTvVerifyStatus.setText(getString(R.string.tx_checking));
                mTvVerifyNotice.setText(getString(R.string.submit_success_introduction));
                mTvVerifyReason.setVisibility(View.GONE);
                mSlButton.setVisibility(View.GONE);
                break;
            case CHECK_FAILED:
                mIvVerfyIcon.setImageResource(R.mipmap.icon_verify_failed);
                mTvVerifyStatus.setText(getString(R.string.merchant_check_failed));
                mTvVerifyNotice.setText(getString(R.string.verify_failed_notice));
                //mTvVerifyReason.setText("");
                mTvVerifyReason.setVisibility(View.GONE);
                mSlButton.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_common_enable:
                Intent intent = new Intent(VerifyStatusActivity.this,VipInfoActivity.class);
                intent.putExtra(Constants.INTENT_SKIP_MEMBER_DETAIL,false);//重新提交，是否显示详情
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_VIP_SUCCESS);
                startActivity(intent);
                break;
        }
    }
}