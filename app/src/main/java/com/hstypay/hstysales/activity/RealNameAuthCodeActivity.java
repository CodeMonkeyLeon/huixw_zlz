package com.hstypay.hstysales.activity;

import android.Manifest;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.WxSignDetailBean;
import com.hstypay.hstysales.listener.OnSingleClickListener;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.ScreenUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.squareup.picasso.Picasso;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 验证身份
 */

public class RealNameAuthCodeActivity extends BaseActivity {
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;
    private TextView mTvTitle, mTvMerchantName;
    private Button mBtnConfirm;
    private ImageView mIvQrCode;
    private RelativeLayout mRlQrCode;
    private ImageView mIvBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realname_auth_code);
        StatusBarUtil.setTranslucentStatusWhiteIcon(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initView() {
        mIvBack = findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);
        mBtnConfirm = findViewById(R.id.btn_confirm);
        mIvQrCode = findViewById(R.id.iv_qrcode);
        mTvMerchantName = findViewById(R.id.tv_merchant_name);
        mRlQrCode = findViewById(R.id.rl_qrcode);

        mTvTitle.setText(getString(R.string.ali_real_name_auth));

        setButtonEnable(mBtnConfirm, true);
    }

    private void initEvent() {
        OnSingleClickListener onSingleClickListener = new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                switch (view.getId()) {
                    case R.id.iv_back:
                        finish();
                        break;
                    case R.id.btn_confirm:
                        boolean results = PermissionUtils.checkPermissionArray(RealNameAuthCodeActivity.this, permissionArray);
                        if (results) {
                            ScreenUtils.saveImageToGallery(RealNameAuthCodeActivity.this, ScreenUtils.createViewBitmap(mRlQrCode));
                            showCommonNoticeDialog(RealNameAuthCodeActivity.this, getString(R.string.dialog_copy_picture));
                        } else {
                            showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_storage));
                        }
                        break;
                }

            }
        };
        mBtnConfirm.setOnClickListener(onSingleClickListener);
        mIvBack.setOnClickListener(onSingleClickListener);
    }

    private void initData() {
        WxSignDetailBean.SignDetailData signDetailData = (WxSignDetailBean.SignDetailData) getIntent().getSerializableExtra("INTENT_SIGN_DETAIL");
        mTvMerchantName.setText(signDetailData.getMerchantName());
        Picasso.get().load(signDetailData.getWechatRealQrcodeUrl()).error(R.mipmap.icon_general_noloading).into(mIvQrCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    ScreenUtils.saveImageToGallery(this, ScreenUtils.createViewBitmap(mRlQrCode));
                    showCommonNoticeDialog(RealNameAuthCodeActivity.this, getString(R.string.dialog_copy_picture));
                } else {
                    showDialog(getString(R.string.permission_set_content_storage));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }
}
