package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.InfoBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.EditTextWatcher;
import com.hstypay.hstysales.widget.NoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.SelectDialog;
import com.hstypay.hstysales.widget.ShadowLayout;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class ChangePwdActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack, mIvEyeOld, mIvEyeNew, mIvEyeEnsure;
    private TextView mTvTitle;
    private EditText mEtOldPwd, mEtNewPwd1, mEtNewPwd2;
    private Button mBtnSubmit, mBtnUnable;
    private boolean isOpenOld, isOpenNew, isOpenEnsure;
    private ShadowLayout mBtnShadow;
    private SelectDialog mSelectDialog;
    private SafeDialog mLoadDialog;
    private NoticeDialog mNoticeDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pwd);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvEyeOld = (ImageView) findViewById(R.id.iv_eye_old);
        mIvEyeNew = (ImageView) findViewById(R.id.iv_eye_new);
        mIvEyeEnsure = (ImageView) findViewById(R.id.iv_eye_ensure);
        mTvTitle = (TextView) findViewById(R.id.tv_title);

        mTvTitle.setText(getString(R.string.title_change_pwd));

        mEtOldPwd = (EditText) findViewById(R.id.et_old_pwd);
        mEtNewPwd1 = (EditText) findViewById(R.id.et_new_pwd1);
        mEtNewPwd2 = (EditText) findViewById(R.id.et_new_pwd2);
        mBtnSubmit = (Button) findViewById(R.id.btn_common_enable);
        mBtnUnable = (Button) findViewById(R.id.btn_common_unable);
        mBtnShadow = (ShadowLayout) findViewById(R.id.sl_home_merchant_freeze);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);

        if (!StringUtils.isEmptyOrNull(mEtOldPwd.getText().toString().trim())
                && !StringUtils.isEmptyOrNull(mEtNewPwd1.getText().toString().trim())
                && !StringUtils.isEmptyOrNull(mEtNewPwd2.getText().toString().trim())) {
            mBtnShadow.setVisibility(View.VISIBLE);
            mBtnUnable.setVisibility(View.GONE);
        } else {
            mBtnShadow.setVisibility(View.GONE);
            mBtnUnable.setVisibility(View.VISIBLE);
        }
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvEyeOld.setOnClickListener(this);
        mIvEyeNew.setOnClickListener(this);
        mIvEyeEnsure.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (!StringUtils.isEmptyOrNull(mEtOldPwd.getText().toString().trim())
                        && !StringUtils.isEmptyOrNull(mEtNewPwd1.getText().toString().trim())
                        && !StringUtils.isEmptyOrNull(mEtNewPwd2.getText().toString().trim())) {

                    mBtnShadow.setVisibility(View.VISIBLE);
                    mBtnUnable.setVisibility(View.GONE);
                } else {
                    mBtnShadow.setVisibility(View.GONE);
                    mBtnUnable.setVisibility(View.VISIBLE);
                }
            }
        });
        mEtOldPwd.addTextChangedListener(editTextWatcher);
        mEtNewPwd1.addTextChangedListener(editTextWatcher);
        mEtNewPwd2.addTextChangedListener(editTextWatcher);
    }

    public void initData() {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChangePwd(NoticeEvent event) {
        if (event.getTag().equals(Constants.LOGIN_CHANGE_PWD_TAG)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network));
                    break;
                case Constants.LOGIN_CHANGE_PWD_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ChangePwdActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.LOGIN_CHANGE_PWD_TRUE:
                    mNoticeDialog = new NoticeDialog(ChangePwdActivity.this
                            , UIUtils.getString(R.string.dialog_notice_change_success)
                            , UIUtils.getString(R.string.btn_know), R.layout.notice_dialog_text);
                    mNoticeDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                        @Override
                        public void clickOk() {
                            SpUtil.removeKey(Constants.SKEY);
                            SpUtil.removeAll();
                            startActivity(new Intent(ChangePwdActivity.this, LoginActivity.class));
                            ChangePwdActivity.this.finish();
                        }
                    });
                    mNoticeDialog.show();
                    break;
            }
        }
    }


    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            //提交
            case R.id.btn_common_enable:
                StatService.trackCustomKVEvent(ChangePwdActivity.this, "1054", null);
                submit();
                break;
            case R.id.iv_eye_old:
                isOpenOld = setEye(isOpenOld);
                setPwdVisible(isOpenOld, mIvEyeOld, mEtOldPwd);
                break;
            case R.id.iv_eye_new:
                isOpenNew = setEye(isOpenNew);
                setPwdVisible(isOpenNew, mIvEyeNew, mEtNewPwd1);
                break;
            case R.id.iv_eye_ensure:
                isOpenEnsure = setEye(isOpenEnsure);
                setPwdVisible(isOpenEnsure, mIvEyeEnsure, mEtNewPwd2);
                break;
            default:
                break;
        }
    }

    private void submit() {
        /*if (mEtOldPwd.getText().toString().trim().length() < 6) {
            mEtOldPwd.setFocusable(true);
            mEtOldPwd.setFocusableInTouchMode(true);
            mEtOldPwd.requestFocus();
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_password_legal));
            return;
        }*/
        if(StringUtils.isEmptyOrNull(mEtOldPwd.getText().toString().trim()) || StringUtils.isEmptyOrNull(mEtNewPwd1.getText().toString().trim()) ||StringUtils.isEmptyOrNull(mEtNewPwd2.getText().toString().trim())){
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_userpwd));
            return;
        }

        if (!mEtNewPwd1.getText().toString().trim().equals(mEtNewPwd2.getText().toString().trim())) {
            mEtNewPwd2.setFocusable(true);
            mEtNewPwd2.setFocusableInTouchMode(true);
            mEtNewPwd2.requestFocus();
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_pwd_different));
            return;
        }

        if (!StringUtils.isLegalPwd(mEtNewPwd1.getText().toString().trim())){
            mEtNewPwd1.setFocusable(true);
            mEtNewPwd1.setFocusableInTouchMode(true);
            mEtNewPwd1.requestFocus();
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_password_legal));
            return;
        }

        if (!StringUtils.isLegalPwd(mEtNewPwd2.getText().toString().trim())){
            mEtNewPwd2.setFocusable(true);
            mEtNewPwd2.setFocusableInTouchMode(true);
            mEtNewPwd2.requestFocus();
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_password_legal));
            return;
        }


        if (mEtOldPwd.getText().toString().trim().equals(mEtNewPwd1.getText().toString().trim())) {
            mEtNewPwd1.setFocusable(true);
            mEtNewPwd1.setFocusableInTouchMode(true);
            mEtNewPwd1.requestFocus();
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_pwd_same));
            return;
        }

        showEnsureDialog();
    }

    public void showEnsureDialog() {

        mSelectDialog = new SelectDialog(ChangePwdActivity.this, UIUtils.getString(R.string.dialog_notice_change_pwd)
                , UIUtils.getString(R.string.btn_ensure_text), UIUtils.getString(R.string.btn_cancel_text), new SelectDialog.HandleBtn() {
            @Override
            public void handleOkBtn() {
                Map<String, Object> map = new HashMap<>();
                map.put("oldPassword", mEtOldPwd.getText().toString().trim());
                map.put("newPassword", mEtNewPwd2.getText().toString().trim());
                DialogUtil.safeShowDialog(mLoadDialog);
                ServerClient.newInstance(ChangePwdActivity.this).changePwd(ChangePwdActivity.this, Constants.LOGIN_CHANGE_PWD_TAG, map);
            }

            @Override
            public void handleCancleBtn() {
                mSelectDialog.dismiss();
            }
        });
        DialogHelper.resize(ChangePwdActivity.this, mSelectDialog);
        mSelectDialog.show();
    }

    private void setPwdVisible(boolean isOpen, ImageView ivEye, EditText etPwd) {
        if (isOpen) {
            ivEye.setImageResource(R.mipmap.change_password_eyes);
            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ivEye.setImageResource(R.mipmap.change_password_close);
            etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        etPwd.setSelection(etPwd.getText().toString().trim().length());
    }

    private boolean setEye(boolean isOpen) {
        return isOpen ? false : true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSelectDialog != null) {
            mSelectDialog.dismiss();
        }
        if (mNoticeDialog != null) {
            mNoticeDialog.dismiss();
        }
        DialogUtil.safeCloseDialog(mLoadDialog);
    }
}
