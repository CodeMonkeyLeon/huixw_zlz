package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.BaseBean;
import com.hstypay.hstysales.bean.ExamineDetailBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.viewholder.AppOpenManagerViewHolder;
import com.hstypay.hstysales.widget.CustomViewFullScreenDialog;
import com.hstypay.hstysales.widget.SafeDialog;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zeyu.kuang
 * @time 2020/12/11
 * @desc 管理应用开通详情页
 */
public class AppOpenDetailActivity extends BaseActivity implements View.OnClickListener {

    private Button mBtnCheckPassApp;//审核通过按钮
    private Button mBtnCheckUnpassApp;//审核不通过按钮
    private LinearLayout mLlNoticeOpenDetail;//添加花呗分期免息的通知
    private ImageView mIvDeleteNotice;//删除
    private ImageView mIvIconAppOpenDetail;//应用类型标识图标
    private TextView mTvNameAppOpenDetail;//应用名称
    private TextView mTvAppMerchantNameValue;//商户名称
    private TextView mTvAppMerchantId;//商户编号
    private TextView mTvAppApplyTime;//申请时间
    private TextView mTvAppContactMobile;//联系电话
    private TextView mTvAppContactAddressValue;//联系地址
    private RelativeLayout mRlAppCheckMan;//审核人rl
    private TextView mTvAppCheckMan;//审核人
    private RelativeLayout mRlAppCheckTime;//审核时间rl
    private TextView mTvAppCheckTime;//审核时间
    private LinearLayout mLlAppUnpassReason;//审核未通过原因ll
    private TextView mTvAppUnpassReason;//审核未通过原因tv
    private ImageView mIvIconPassStatusApp;//审核状态图标
    private TextView mTvUnderstandHb;//了解花呗分期免息
    private String mAppCode;//应用类型标识码
    private SafeDialog mLoadDialog;
    private String mBillNo;//应用订购单号
    private TextView mTvNoticeOpenDetail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_open_detail);
        initView();
        initData();
        initEvent();
    }

    private void initView() {
        TextView tv_title = findViewById(R.id.tv_title);
        tv_title.setText(getResources().getString(R.string.user_app_open_manager));
        mBtnCheckPassApp = findViewById(R.id.btn_check_pass_app);
        mBtnCheckUnpassApp = findViewById(R.id.btn_check_unpass_app);
        mLlNoticeOpenDetail = findViewById(R.id.ll_notice_open_detail);
        mTvNoticeOpenDetail = findViewById(R.id.tv_notice_open_detail);
        mIvDeleteNotice = findViewById(R.id.iv_delete_notice);
        mIvIconAppOpenDetail = findViewById(R.id.iv_icon_app_open_detail);
        mTvNameAppOpenDetail = findViewById(R.id.tv_name_app_open_detail);
        mTvAppMerchantNameValue = findViewById(R.id.tv_app_merchant_name_value);
        mTvAppMerchantId = findViewById(R.id.tv_app_merchant_id);
        mTvAppApplyTime = findViewById(R.id.tv_app_apply_time);
        mTvAppContactMobile = findViewById(R.id.tv_app_contact_mobile);
        mTvAppContactAddressValue = findViewById(R.id.tv_app_contact_address_value);
        mRlAppCheckMan = findViewById(R.id.rl_app_check_man);
        mTvAppCheckMan = findViewById(R.id.tv_app_check_man);
        mRlAppCheckTime = findViewById(R.id.rl_app_check_time);
        mTvAppCheckTime = findViewById(R.id.tv_app_check_time);
        mLlAppUnpassReason = findViewById(R.id.ll_app_unpass_reason);
        mTvAppUnpassReason = findViewById(R.id.tv_app_unpass_reason);
        mIvIconPassStatusApp = findViewById(R.id.iv_icon_pass_status_app);
        mTvUnderstandHb = findViewById(R.id.tv_understand_hb);
    }

    private void initEvent() {
        findViewById(R.id.iv_back).setOnClickListener(this);
        mTvUnderstandHb.setOnClickListener(this);
        mBtnCheckPassApp.setOnClickListener(this);
        mBtnCheckUnpassApp.setOnClickListener(this);
        mIvDeleteNotice.setOnClickListener(this);
    }

    private void initData() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        Intent intent = getIntent();
        mAppCode = intent.getStringExtra(Constants.INTENT_EXAMINE_APPCODE);
        mBillNo = intent.getStringExtra(Constants.INTENT_EXAMINE_BILLNO);
        getExamineDetail(mBillNo);
    }

    /**
     * @param examineDetailData 应用详细信息
     * @param appCode           应用类型的标识
     */
    private void setViewValue(ExamineDetailBean.DataBean examineDetailData, String appCode) {
        if (AppOpenManagerViewHolder.TYPE_HBFQ_CODE.equals(appCode)) {
            //花呗分期免息
            mIvIconAppOpenDetail.setImageResource(R.mipmap.icon_app_huabeibig);
        } else if (AppOpenManagerViewHolder.TYPE_PLEDGE_CODE.equals(appCode)) {
            //押金收款
            mIvIconAppOpenDetail.setImageResource(R.mipmap.icon_app_yajin);
        } else {
            mIvIconAppOpenDetail.setVisibility(View.GONE);
        }
        mTvNameAppOpenDetail.setText(examineDetailData.getAppIdCnt());
        mTvAppMerchantNameValue.setText(examineDetailData.getAdminIdCnt());
        mTvAppMerchantId.setText(examineDetailData.getAdminId());
        mTvAppApplyTime.setText(examineDetailData.getCreateTime());
        mTvAppContactMobile.setText(examineDetailData.getTel());
        mTvAppContactAddressValue.setText(examineDetailData.getAddress());
        String examineUserNo = examineDetailData.getExamineUserNo();
        if (TextUtils.isEmpty(examineUserNo)) {
            mTvAppCheckMan.setText(examineDetailData.getExamineUserCnt());
        } else {
            mTvAppCheckMan.setText(examineDetailData.getExamineUserCnt() + "(" + examineUserNo + ")");
        }
        mTvAppCheckTime.setText(examineDetailData.getExamineTime());
        mTvAppUnpassReason.setText(examineDetailData.getExamineRemark());
        String orderStatus = examineDetailData.getOrderStatus();
        if (AppOpenManagerViewHolder.STATUS_NEED_EXAMINE.equals(orderStatus)) {
            mIvIconPassStatusApp.setImageResource(R.mipmap.icon_app_daishenhe);
        } else if (AppOpenManagerViewHolder.STATUS_OK.equals(orderStatus)) {
            mIvIconPassStatusApp.setImageResource(R.mipmap.icon_app_yitongguo);
        } else if (AppOpenManagerViewHolder.STATUS_FAIL.equals(orderStatus)) {
            mIvIconPassStatusApp.setImageResource(R.mipmap.icon_app_weitongguo);
        } else {
            mIvIconPassStatusApp.setVisibility(View.GONE);
        }
    }

    /**
     * 查询应用审核详情
     *
     * @param billNo 订购单号
     */
    private void getExamineDetail(String billNo) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("billNo", billNo);
            ServerClient.newInstance(MyApplication.getContext()).getExamineDetail(MyApplication.getContext(), Constants.TAG_GET_APP_EXAMINE_DETAIL, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_understand_hb:
                //了解花呗分期
                startActivity(new Intent(AppOpenDetailActivity.this, HuaBeiIntroduceActivity.class));
                break;
            case R.id.btn_check_pass_app:
                //审核通过
                showOpenHbFqFreeTip();
                break;
            case R.id.btn_check_unpass_app:
                //审核不通过
                showCheckNotPassTip();
                break;
            case R.id.iv_delete_notice:
                mLlNoticeOpenDetail.setVisibility(View.GONE);
                break;
        }
    }

    /**
     * 未开通花呗分期免息支付类型弹窗和错误提示弹窗
     */
    private void showNotOpenFqPayTip(String message) {
        final CustomViewFullScreenDialog notOpenFqPayDialog = new CustomViewFullScreenDialog(this);
        notOpenFqPayDialog.setView(R.layout.dialog_title_context_confirm);
        notOpenFqPayDialog.setClickOutDismiss();
        TextView tv_content_dialog = notOpenFqPayDialog.findViewById(R.id.tv_content_dialog);
        tv_content_dialog.setText(TextUtils.isEmpty(message) ? "" : message);
        Button btn_confirm_dialog = notOpenFqPayDialog.findViewById(R.id.btn_confirm_dialog);
        notOpenFqPayDialog.show();
        btn_confirm_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notOpenFqPayDialog.dismiss();
            }
        });

    }

    /**
     * 二次确认给商户开通应用-花呗分期免息
     */
    private void showOpenHbFqFreeTip() {
        final CustomViewFullScreenDialog openFqPayDialog = new CustomViewFullScreenDialog(this);
        openFqPayDialog.setView(R.layout.dialog_title_context_cancel_confirm);
        openFqPayDialog.setClickOutDismiss();
        TextView tv_content_dialog = openFqPayDialog.findViewById(R.id.tv_content_dialog);
        if (AppOpenManagerViewHolder.TYPE_HBFQ_CODE.equals(mAppCode)) {
            //花呗分期免息
            tv_content_dialog.setText(getResources().getString(R.string.content_dialog_open_hb));
        } else if (AppOpenManagerViewHolder.TYPE_PLEDGE_CODE.equals(mAppCode)) {
            //押金收款
            tv_content_dialog.setText(getResources().getString(R.string.content_dialog_open_pledge));
        } else {
            //其它应用
            tv_content_dialog.setText(getResources().getString(R.string.content_dialog_open_other));
        }
        openFqPayDialog.show();

        openFqPayDialog.findViewById(R.id.btn_cancel_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFqPayDialog.dismiss();
            }
        });
        openFqPayDialog.findViewById(R.id.btn_confirm_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFqPayDialog.dismiss();
                checkApp(true, null);
            }
        });

    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    /**
     * @param isCheckPass   审核通过true  审核不通过false
     * @param examineRemark 审核不通过填的原因
     */
    private void checkApp(boolean isCheckPass, String examineRemark) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("billNo", mBillNo);
            map.put("orderStatus", isCheckPass ? AppOpenManagerViewHolder.STATUS_OK : AppOpenManagerViewHolder.STATUS_FAIL);
            if (!isCheckPass) {
                map.put("examineRemark", examineRemark);
            }
            ServerClient.newInstance(MyApplication.getContext()).examineApp(MyApplication.getContext(), Constants.TAG_EXAMINE_APP, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCheckApp(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_EXAMINE_APP)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            BaseBean msg = (BaseBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.EXAMINE_APP_FALSE:
                    if (msg.getError() != null) {
                        if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                            if (msg.getError().getMessage() != null) {
                                loginDialog(AppOpenDetailActivity.this, msg.getError().getMessage());
                            }
                        } else {
                            if (msg.getError().getMessage() != null) {
                                showNotOpenFqPayTip(msg.getError().getMessage());
                            }
                        }
                    }
                    break;
                case Constants.EXAMINE_APP_TRUE:
                    setResult(RESULT_OK);
                    finish();
                    break;
            }
        } else {
            if (event.getTag().equals(Constants.TAG_GET_APP_EXAMINE_DETAIL)) {
                DialogUtil.safeCloseDialog(mLoadDialog);
                ExamineDetailBean msg = (ExamineDetailBean) event.getMsg();
                switch (event.getCls()) {
                    case Constants.MSG_NET_ERROR:
                        ToastUtil.showToastShort(getString(R.string.error_network_request));
                        break;
                    case Constants.GET_APP_EXAMINE_DETAIL_FALSE:
                        if (msg.getError() != null) {
                            if (msg.getError().getCode() != null) {
                                if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                    if (msg.getError().getMessage() != null) {
                                        loginDialog(AppOpenDetailActivity.this, msg.getError().getMessage());
                                    }
                                } else {
                                    if (msg.getError().getMessage() != null) {
                                        ToastUtil.showToastShort(msg.getError().getMessage());
                                    }
                                }
                            }
                        }
                        break;
                    case Constants.GET_APP_EXAMINE_DETAIL_TRUE:
                        if (msg != null && msg.getData() != null) {
                            ExamineDetailBean.DataBean examineDetailData = msg.getData();
                            if (examineDetailData != null) {
                                if (!TextUtils.isEmpty(examineDetailData.getAppCode())) {
                                    mAppCode = examineDetailData.getAppCode();
                                }
                                if (!TextUtils.isEmpty(examineDetailData.getBillNo())) {
                                    mBillNo = examineDetailData.getBillNo();
                                }
                                String orderStatus = examineDetailData.getOrderStatus();
                                if (AppOpenManagerViewHolder.STATUS_NEED_EXAMINE.equals(orderStatus)) {
                                    //待审核
                                    if (AppOpenManagerViewHolder.TYPE_HBFQ_CODE.equals(mAppCode)) {
                                        //花呗分期免息
                                        mLlNoticeOpenDetail.setVisibility(View.VISIBLE);
                                        mTvNoticeOpenDetail.setText(getResources().getString(R.string.tv_app_open_detail_notice));
                                    } else if (AppOpenManagerViewHolder.TYPE_PLEDGE_CODE.equals(mAppCode)) {
                                        //押金收款
                                        mLlNoticeOpenDetail.setVisibility(View.VISIBLE);
                                        mTvNoticeOpenDetail.setText(getResources().getString(R.string.tv_app_open_detail_notice_ysq));
                                    }
                                    mBtnCheckPassApp.setVisibility(View.VISIBLE);
                                    mBtnCheckUnpassApp.setVisibility(View.VISIBLE);
                                } else if (AppOpenManagerViewHolder.STATUS_OK.equals(orderStatus)) {
                                    //审核通过
                                    mRlAppCheckMan.setVisibility(View.VISIBLE);
                                    mRlAppCheckTime.setVisibility(View.VISIBLE);
                                } else if (AppOpenManagerViewHolder.STATUS_FAIL.equals(orderStatus)) {
                                    //审核不通过
                                    mRlAppCheckMan.setVisibility(View.VISIBLE);
                                    mRlAppCheckTime.setVisibility(View.VISIBLE);

                                    mLlAppUnpassReason.setVisibility(View.VISIBLE);

                                    if (AppOpenManagerViewHolder.TYPE_HBFQ_CODE.equals(mAppCode)) {
                                        //花呗分期免息
                                        mLlNoticeOpenDetail.setVisibility(View.VISIBLE);
                                        mTvNoticeOpenDetail.setText(getResources().getString(R.string.tv_app_open_detail_notice));
                                    } else if (AppOpenManagerViewHolder.TYPE_PLEDGE_CODE.equals(mAppCode)) {
                                        //押金收款
                                        mLlNoticeOpenDetail.setVisibility(View.VISIBLE);
                                        mTvNoticeOpenDetail.setText(getResources().getString(R.string.tv_app_open_detail_notice_ysq));
                                    }
                                } else {
                                    //其它
                                }

                                if (AppOpenManagerViewHolder.TYPE_HBFQ_CODE.equals(mAppCode)) {
                                    //花呗分期免息
                                    mTvUnderstandHb.setVisibility(View.VISIBLE);
                                }

                                setViewValue(examineDetailData, mAppCode);
                            }
                        } else {
                            ToastUtil.showToastShort(getResources().getString(R.string.error_data));
                        }
                        break;
                }
            }
        }
    }


    /**
     * 审核不通过原因
     */
    private void showCheckNotPassTip() {
        final CustomViewFullScreenDialog dialog = new CustomViewFullScreenDialog(this);
        dialog.setView(R.layout.dialog_edit_cancel_confirm_open);
        dialog.setClickOutDismiss();
        final EditText et_reason_refund_dialog = dialog.findViewById(R.id.et_reason_refuse_dialog);
        Button btn_cancel_dialog = dialog.findViewById(R.id.btn_cancel_dialog);
        Button btn_confirm_dialog = dialog.findViewById(R.id.btn_confirm_dialog);
        final TextView tv_counter_reason_refuse = dialog.findViewById(R.id.tv_counter_reason_refuse);//计数
        et_reason_refund_dialog.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = s.length();
                tv_counter_reason_refuse.setText("" + length + "/30");
            }
        });
        btn_cancel_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btn_confirm_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reason = et_reason_refund_dialog.getText().toString().trim();
                if (reason == null || reason.length() < 1) {
                    Toast.makeText(AppOpenDetailActivity.this, getResources().getString(R.string.hint_input_refuse_reason), Toast.LENGTH_SHORT).show();
                    return;
                }
                dialog.dismiss();
                checkApp(false, reason);
            }
        });
        dialog.show();

    }

    /*@Override
    public void finish() {
        if (Constants.INTENT_NAME_MSG_SKIP.equals(getIntent().getStringExtra(Constants.INTENT_NAME)))
            startActivity(new Intent(this, AppOpenManagerActivity.class));
        super.finish();
    }*/
}
