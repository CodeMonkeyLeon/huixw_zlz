package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.EditTextDelete;

import static com.hstypay.hstysales.activity.tinycashier.ActivationCodeDetailsActivity.KEY_UPDATE_SHORT_NAME;


/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/7/01 10:09
 * @描述: 门店单选列表
 */
public class ChangeNameActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mTvNull, mButton;
    private EditTextDelete mEtName;
    private boolean isShortName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_name);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
    }

    public void initView() {
        String name = getIntent().getStringExtra(Constants.INTENT_CHANGE_NAME);
        isShortName = getIntent().getBooleanExtra(KEY_UPDATE_SHORT_NAME, false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = findViewById(R.id.button_title);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mEtName = findViewById(R.id.et_name);
        if (isShortName) {
            mTvTitle.setText("修改门店简称");
            mEtName.setHint("请填写门店简称");
        } else {
            mTvTitle.setText(R.string.title_change_name);
        }
        mButton.setVisibility(View.VISIBLE);
        mButton.setText(R.string.btn_complete);
        mButton.setEnabled(false);
        mButton.setTextColor(UIUtils.getColor(R.color.white_fifty));
        mEtName.setClearImage(R.mipmap.ic_search_clear);
        mEtName.setText(name);
        if (name.length() > 0) {
            mButton.setEnabled(true);
            mButton.setTextColor(UIUtils.getColor(R.color.white));
        } else {
            mButton.setEnabled(false);
            mButton.setTextColor(UIUtils.getColor(R.color.white_fifty));
        }
    }


    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);

        mEtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mButton.setEnabled(true);
                    mButton.setTextColor(UIUtils.getColor(R.color.white));
                } else {
                    mButton.setEnabled(false);
                    mButton.setTextColor(UIUtils.getColor(R.color.white_fifty));
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                if (isShortName) {
                    if (StringUtils.managerName10People(mEtName.getText().toString().trim())) {
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.RESULT_CHANGE_NAME, mEtName.getText().toString().trim());   //传递一个user对象列表
                        intent.putExtras(bundle);
                        setResult(RESULT_OK, intent);
                        finish();
                    } else {
                        showCommonNoticeDialog(ChangeNameActivity.this, getString(R.string.regex_manager_name_10_People));
                    }
                    return;
                }
                if (StringUtils.managerName(mEtName.getText().toString().trim())) {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.RESULT_CHANGE_NAME, mEtName.getText().toString().trim());   //传递一个user对象列表
                    intent.putExtras(bundle);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    showCommonNoticeDialog(ChangeNameActivity.this, getString(R.string.regex_manager_name));
                }
                break;
        }
    }
}
