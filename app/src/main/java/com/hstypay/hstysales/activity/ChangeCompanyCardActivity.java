package com.hstypay.hstysales.activity;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.CardDetailBean;
import com.hstypay.hstysales.bean.InfoBean;
import com.hstypay.hstysales.bean.MerchantInfoBean;
import com.hstypay.hstysales.fragment.PrivateFragment;
import com.hstypay.hstysales.fragment.PublicFragment;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.utils.ClickUtil;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.LegalDialog;
import com.hstypay.hstysales.widget.NoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 修改银行卡
 */

public class ChangeCompanyCardActivity extends BaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private TextView mTvTitle, mTvNull, mTvReason;
    private ImageView mIvBack;
    public SafeDialog mLoadDialog;
    private LinearLayout mLlExamineStatus;

    private ScrollView mSvContent;
    private RadioButton mRbPrivate, mRbPublic;
    private PrivateFragment mPrivateFragment;
    private PublicFragment mPublicFragment;
    private Fragment[] mFragments;
    private RadioGroup mRgType;
    private int mIndex = 0;
    private MerchantInfoBean.DataBean mData;
    private boolean isPrivate = false;
    private boolean isPublic = false;
    private LegalDialog dialog;
    private String isTag;
    private LinearLayout ll_company_type;
    private int mChangeAcountCount;//剩余可修改次数
    private String mMerchantId;
    private ImageView mIvTypePrivate;
    private ImageView mIvTypePublic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        setContentView(R.layout.activity_change_company_card);
        StatusBarUtil.setTranslucentStatus(this);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);

        mLlExamineStatus = (LinearLayout) findViewById(R.id.ll_examine_status);
        ll_company_type = findViewById(R.id.ll_company_type);
        mTvReason = (TextView) findViewById(R.id.tv_merchant_reason);
        mRgType = (RadioGroup) findViewById(R.id.rg_type_company);
        mRbPrivate = (RadioButton) findViewById(R.id.rb_type_private);
        mRbPublic = (RadioButton) findViewById(R.id.rb_type_public);
        mIvTypePrivate = findViewById(R.id.iv_type_private);
        mIvTypePublic = findViewById(R.id.iv_type_public);

        mSvContent = (ScrollView) findViewById(R.id.sv_content);
        mTvNull = (TextView) findViewById(R.id.tv_not_data);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mRgType.setOnCheckedChangeListener(this);
        mIvTypePrivate.setOnClickListener(this);
        mIvTypePublic.setOnClickListener(this);
    }

    private void initData() {
        mData = (MerchantInfoBean.DataBean) getIntent().getSerializableExtra(Constants.INTENT_BANK_DETAIL);
        mChangeAcountCount = getIntent().getIntExtra(Constants.INTENT_CHANGEACOUNTCOUNT,1);
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        initFragment(mData);
        if (mData != null) {
            mSvContent.setVisibility(View.VISIBLE);
          /*  if (mData.getExamineStatus()==2){
                mLlExamineStatus.setVisibility(View.VISIBLE);
                mTvReason.setText(mData.getExamineRemark());
            }*/

            mTvTitle.setText("结算信息");

            MerchantInfoBean.DataBean.MchDetailBean mchDetail = mData.getMchDetail();

            MerchantInfoBean.DataBean.BankAccountBean bankAccount = mData.getBankAccount();
            if (mData.getMchUnincorporatedSettlement() == 0) {
                ll_company_type.setVisibility(View.GONE);
                //同名结算卡
                mRbPrivate.setChecked(true);
                setIndexSelected(0);
            } else {
                if (mchDetail != null && mchDetail.getIdCode() != null && bankAccount != null && bankAccount.getIdCard() != null) {
                    if (mchDetail.getIdCode().equals(bankAccount.getIdCard())) {
                        //同名结算卡
                        mRbPrivate.setChecked(true);
                        setIndexSelected(0);
                    } else {
                        mRbPublic.setChecked(true);
                        setIndexSelected(1);
                    }
                } else {
                    mRbPublic.setChecked(true);
                    setIndexSelected(1);
                }
            }
        } else {
            mTvNull.setVisibility(View.VISIBLE);
        }
    }

    private void initFragment(MerchantInfoBean.DataBean data) {
        mPrivateFragment = new PrivateFragment();
        mPrivateFragment.setData(data,getIntent().getStringExtra("type"),mChangeAcountCount,mMerchantId);
        mPublicFragment = new PublicFragment();
        mPublicFragment.setData(data,getIntent().getStringExtra("type"),mChangeAcountCount,mMerchantId);
        //添加到数组
        mFragments = new Fragment[]{mPrivateFragment, mPublicFragment};
        //开启事务
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        //添加首页
        ft.add(R.id.content, mPrivateFragment).commit();
        //默认设置为第0个
        setIndexSelected(0);
    }

    private void setIndexSelected(int index) {
        if (mIndex == index) {
            return;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        //隐藏
        ft.hide(mFragments[mIndex]);
        //判断是否添加
        if (!mFragments[index].isAdded()) {
            ft.add(R.id.content, mFragments[index]).show(mFragments[index]);
        } else {
            ft.show(mFragments[index]);
        }
        ft.commit();
        //再次赋值
        mIndex = index;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        //底部导航按钮选中状态切换的时候就切换ViewPager显示的页面
        switch (checkedId) {
            case R.id.rb_type_private:
                //mTvTitle.setText("更改结算银行卡");
                setIndexSelected(0);
                isPrivate = true;

                break;
            case R.id.rb_type_public:
                // mTvTitle.setText("更改开户许可证");
                setIndexSelected(1);
                isPublic = true;
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_type_private:
                dialog("同名结算卡号", "更改为法人身份证号下的结算银行卡发生变更操作");
                break;

            case R.id.iv_type_public:
                dialog("非同名结算卡号", "更改为非法人身份证下的结算银行卡发生变更操作");
                break;
        }
    }

    private void dialog(String title, String content) {
        if (ClickUtil.isNotFastClick()){
            if (dialog!=null && dialog.isShowing()){
                dialog.dismiss();
            }
            dialog = new LegalDialog(ChangeCompanyCardActivity.this, title, content,
                    "我知道了", null, "change_card", new LegalDialog.HandleBtn() {
                @Override
                public void handleOkBtn(String s) {

                    dialog.dismiss();
                }

                @Override
                public void handleCancleBtn() {
                    dialog.dismiss();
                }
            });

            DialogHelper.resize(this, dialog);
            dialog.show();
        }

    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_EDIT_CARD)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    getDialog();
                    break;
            }
        }
    }

    public void getDialog() {
        NoticeDialog dialogInfo = new NoticeDialog(ChangeCompanyCardActivity.this, null, null, R.layout.notice_dialog_verify);
        dialogInfo.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                if (mData != null && mData.getExamineStatus() == 2) {
                    StatService.trackCustomKVEvent(ChangeCompanyCardActivity.this, "1125", null);
                } else {
                    StatService.trackCustomKVEvent(ChangeCompanyCardActivity.this, "1120", null);
                }
                ChangeCompanyCardActivity.this.finish();
            }
        });
        dialogInfo.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }
}
