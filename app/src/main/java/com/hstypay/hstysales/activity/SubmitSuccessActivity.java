package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.StatusBarUtil;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class SubmitSuccessActivity extends BaseActivity {


    private TextView mTvTitle;
    private ImageView mIvBack;
    private Button mButton;
    private String mIntentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        setContentView(R.layout.activity_submit_success);
        StatusBarUtil.setTranslucentStatus(this);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = (Button) findViewById(R.id.btn_common_enable);
        mTvTitle.setText(getString(R.string.title_submit_success));
        mIvBack.setVisibility(View.GONE);
    }

    private void initListener() {
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mIntentName) && Constants.INTENT_VIP_SUCCESS.equals(mIntentName)) {
                    Intent intent = new Intent(SubmitSuccessActivity.this, MerchantInfoActivity.class);
                    intent.putExtra(Constants.INTENT_NAME,Constants.SUBMIT_CHECKING);
                    startActivity(intent);
                    finish();
                }else if (!TextUtils.isEmpty(mIntentName) && Constants.INTENT_REGISTER_VIP.equals(mIntentName)){
                    Intent intent = new Intent(SubmitSuccessActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    finish();
                }
            }
        });
    }

    private void initData(){
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
    }
}