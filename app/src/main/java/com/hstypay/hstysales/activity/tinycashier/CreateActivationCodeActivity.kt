package com.hstypay.hstysales.activity.tinycashier

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.view.View
import com.hstypay.hstysales.R
import com.hstypay.hstysales.activity.ShopActivity
import com.hstypay.hstysales.bean.StoreListBean
import com.hstypay.hstysales.commonlib.base.AppActivity
import com.hstypay.hstysales.commonlib.widget.ActivationCodeHelpDialog
import com.hstypay.hstysales.utils.Constants
import com.hstypay.hstysales.utils.StringUtils
import com.hstypay.hstysales.utils.TextInputHelper
import com.hstypay.hstysales.viewmodel.CreateActivationCodeViewMode
import kotlinx.android.synthetic.main.activity_create_activation_code.*
import kotlinx.android.synthetic.main.title_arrow.*

/**
 * @Author dean.zeng
 * @Description  微收银激活码创建
 * @Date 2020-07-17 15:50
 **/
class CreateActivationCodeActivity : AppActivity<CreateActivationCodeViewMode>() {


    private lateinit var storeMerchantId: String

    companion object {
        const val REQUEST_SHOP_BEAN_CODE = 0x92
    }

    private lateinit var textInputHelper: TextInputHelper
    private lateinit var mMerchantId: String

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun initData() {
        mMerchantId = intent.getStringExtra(Constants.INTENT_MERHANT_ID);
        iv_back.setOnClickListener { finish() }
        tv_title.text = "创建激活码"
        iv_title_button.setImageResource(R.mipmap.question_circle)
        iv_title_button.imageTintList = ColorStateList.valueOf(Color.parseColor("#FFFFFF"))
        iv_title_button.visibility = View.VISIBLE
        iv_title_button.setOnClickListener {
            ActivationCodeHelpDialog(this).show()
        }
        tvMerchantName.text = intent.getStringExtra(Constants.INTENT_SHORT_NAME)
        llStoreName.setOnClickListener {
            storeMerchantId = if (!this::storeMerchantId.isInitialized) {
                ""
            } else {
                storeMerchantId
            }
            startActivityForResult(Intent(this, ShopActivity::class.java)
                    .putExtra(Constants.INTENT_STORE_ID, storeMerchantId)
                    .putExtra(Constants.INTENT_MERCHANT_ID, mMerchantId), REQUEST_SHOP_BEAN_CODE)
        }
        textInputHelper = TextInputHelper(btnSubmit)
        textInputHelper.addViews(tvStoreMerchantName, tvStoreMerchantShortName)
        btnSubmit.setOnClickListener {
            if (StringUtils.managerName10People(tvStoreMerchantShortName.text.toString())) {
                mViewModel.addActivationCode(storeMerchantId, tvStoreMerchantShortName.text.toString(), mMerchantId)
            } else {
                showCommonNoticeDialog(this, getString(R.string.regex_manager_name_10_People))
            }
        }


    }


    override fun onDestroy() {
        super.onDestroy()
        textInputHelper.removeViews()
    }

    override fun initView() {
        mViewModel.successLiveData.observe(this, Observer {//创建成功
            showModal("创建成功") {
                val intent = Intent()
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        })
    }

    override fun getLayoutId(): Int = R.layout.activity_create_activation_code


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_SHOP_BEAN_CODE && data != null) {
            val extras = data.extras
            val shopBean = extras.getSerializable(Constants.RESULT_SHOP_BEAN_INTENT) as StoreListBean.DataEntity
            tvStoreMerchantName.text = shopBean.storeName
            storeMerchantId = shopBean.storeId
        }
    }

}