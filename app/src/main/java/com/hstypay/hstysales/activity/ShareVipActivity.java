package com.hstypay.hstysales.activity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.Zxing.MaxCardManager;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.CodeUrlBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.ScreenUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.NoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.SelectDialog;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

public class ShareVipActivity extends BaseActivity implements View.OnClickListener {
    private String mCodeUrl;
    private Bitmap mQRCode;
    private SafeDialog mLoadDialog;
    private ImageView mIvBack, mIvShareCode;
    private Button mButton;
    private TextView mTvMerchantName;
    private LinearLayout mLlShareWecaht, mLlShareFrients, mLlShareWeibo, mLlShareQQ, mLlShareQQSpace;
    private RelativeLayout mRlShareCode,mRlShareIcon;
    private SafeDialog dialog;
    private UMWeb web;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;
    private Bitmap mViewBitmap;
    private NoticeDialog mDialogInfo;
    private String mMerchantId, mMerchantName;

    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
            DialogUtil.safeShowDialog(dialog);
        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(dialog);
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            DialogUtil.safeCloseDialog(dialog);
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(dialog);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_vip);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mButton = (Button) findViewById(R.id.button_title);
        mIvShareCode = (ImageView) findViewById(R.id.iv_share_qrcode);
        mTvMerchantName = (TextView) findViewById(R.id.tv_code_merchant_name);
        mLlShareWecaht = (LinearLayout) findViewById(R.id.ll_share_wechat);
        mLlShareFrients = (LinearLayout) findViewById(R.id.ll_share_friends);
        mLlShareWeibo = (LinearLayout) findViewById(R.id.ll_share_weibo);
        mLlShareQQ = (LinearLayout) findViewById(R.id.ll_share_qq);
        mLlShareQQSpace = (LinearLayout) findViewById(R.id.ll_share_qq_space);
        mRlShareCode = (RelativeLayout) findViewById(R.id.rl_share_code);
        mRlShareIcon = (RelativeLayout) findViewById(R.id.rl_share_icon);
        dialog = getLoadDialog(this, getString(R.string.share_loading), true, 0.9f);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mButton.setOnClickListener(this);
        mLlShareWecaht.setOnClickListener(this);
        mLlShareFrients.setOnClickListener(this);
        mLlShareWeibo.setOnClickListener(this);
        mLlShareQQ.setOnClickListener(this);
        mLlShareQQSpace.setOnClickListener(this);
    }

    public void initData() {
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        mMerchantName = getIntent().getStringExtra(Constants.INTENT_MERCHANT_NAME);
        mTvMerchantName.setText(mMerchantName);
        getMemberQRCode(mMerchantId);
    }

    private void getMemberQRCode(String merchantId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", merchantId);
            ServerClient.newInstance(MyApplication.getContext())
                    .queryMchMemberQRCode(MyApplication.getContext(), Constants.TAG_GET_VIP_URL, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }


    @Override
    public void onClick(View view) {
        PermissionUtils.checkPermissionArray(ShareVipActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE);
        if (!TextUtils.isEmpty(mCodeUrl)) {
            mViewBitmap = ScreenUtils.createViewBitmap(mRlShareCode);
        }
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.button_title:
                ScreenUtils.saveImageToGallery(this, mViewBitmap);
                getDialogSuccess(getString(R.string.dialog_copy_picture));
                break;
            case R.id.ll_share_wechat:
                //StatService.trackCustomKVEvent(ShareVipActivity.this, "1142", null);
                ShareImage(SHARE_MEDIA.WEIXIN, mViewBitmap, mViewBitmap);
                break;
            case R.id.ll_share_friends:
                //StatService.trackCustomKVEvent(ShareVipActivity.this, "1146", null);
                ShareImage(SHARE_MEDIA.WEIXIN_CIRCLE, mViewBitmap, mViewBitmap);
                break;
            case R.id.ll_share_weibo:
                //StatService.trackCustomKVEvent(ShareVipActivity.this, "1144", null);
                break;
            case R.id.ll_share_qq:
                //StatService.trackCustomKVEvent(ShareVipActivity.this, "1143", null);
                ShareImage(SHARE_MEDIA.QQ, mViewBitmap, mViewBitmap);
                break;
            case R.id.ll_share_qq_space:
                //StatService.trackCustomKVEvent(ShareVipActivity.this, "1145", null);
                ShareImage(SHARE_MEDIA.QZONE, mViewBitmap, mViewBitmap);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    // Permission Granted
                } else {
                    showDialog();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void showDialog() {
        SelectDialog mDialog = new SelectDialog(this, "需要文件读写等权限后才能使用", "去设置", "取消", new SelectDialog.HandleBtn() {
            @Override
            public void handleOkBtn() {
                Intent intent = getAppDetailSettingIntent(ShareVipActivity.this);
                startActivity(intent);
            }

            @Override
            public void handleCancleBtn() {

            }
        });
        DialogHelper.resize(this, mDialog);
        mDialog.show();
    }

    private void ShareImage(SHARE_MEDIA platform, Bitmap image, Bitmap thumb) {
        UMImage pic = new UMImage(ShareVipActivity.this, image);
        pic.setThumb(new UMImage(ShareVipActivity.this, thumb));
        new ShareAction(ShareVipActivity.this).withMedia(pic).setPlatform(platform).setCallback(shareListener).withText("识别二维码领取会员卡").share();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DialogUtil.safeCloseDialog(dialog);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(dialog);
        UMShareAPI.get(this).release();
    }

    public void getDialogSuccess(String title) {
        if (mDialogInfo == null) {
            mDialogInfo = new NoticeDialog(ShareVipActivity.this, title, getString(R.string.dialog_notice_button), R.layout.notice_dialog_common);
            DialogHelper.resize(ShareVipActivity.this, mDialogInfo);
            mDialogInfo.show();

        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_VIP_URL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CodeUrlBean msg = (CodeUrlBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ShareVipActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null) {
                        mCodeUrl = msg.getData().getMemerUrl();
                        if (msg.getData().getRetCode()) {
                            if (!TextUtils.isEmpty(mCodeUrl)) {
                                mRlShareIcon.setVisibility(View.VISIBLE);
                                mButton.setVisibility(View.VISIBLE);
                                try {
                                    mQRCode = MaxCardManager.getInstance().create2DCode(mCodeUrl,
                                            DensityUtils.dip2px(ShareVipActivity.this, 190),
                                            DensityUtils.dip2px(ShareVipActivity.this, 190));
                                } catch (WriterException e) {
                                    e.printStackTrace();
                                }
                                mIvShareCode.setImageBitmap(mQRCode);
                            } else {
                                mRlShareIcon.setVisibility(View.INVISIBLE);
                                mButton.setVisibility(View.INVISIBLE);
                                ToastUtil.showToastShort(getString(R.string.error_not_open_member));
                            }
                        } else {
                            mRlShareIcon.setVisibility(View.INVISIBLE);
                            mButton.setVisibility(View.INVISIBLE);
                            if (!TextUtils.isEmpty(msg.getData().getMessage())) {
                                ToastUtil.showToastShort(msg.getData().getMessage());
                            }
                        }
                    }
                    break;
            }
        }
    }
}
