package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.adapter.AppOpenManagerAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.ExamineDetailBean;
import com.hstypay.hstysales.bean.ExamineListBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.EditTextDelete;
import com.hstypay.hstysales.widget.LinearSpaceItemDecoration;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Desc: 商户应用开通管理搜索页面
 * @author kuangzeyu
 * @date 2020/12/11
 **/
public class AppOpenSearchActivity extends BaseActivity implements View.OnClickListener {

    private EditTextDelete mEtInputSearch;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRvAppOpenSearch;
    private LinearLayout mLlEmptyNoData;//空视图
    private AppOpenManagerAdapter mAppOpenManagerAdapter;
    private SafeDialog mLoadDialog;
    private String mClickItemAppCode;//应用类型的标识
    private String mClickItemBillNo;//应用订购单号
    private static final int REQUEST_CODE_GO_DETAIL = 200;
    private String mSearchKey;
    private int mPageSize = 15;
    private int mCurrentPage = 1;
    private boolean isLoadmore;//加载更多标识
    private boolean isPullRefresh;//下拉刷新标识
    List<ExamineListBean.ExamineBean> mExamineBeans = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_open_search);
        initView();
        initData();
        initEvent();
    }

    private void initEvent() {
        findViewById(R.id.iv_back).setOnClickListener(this);
        findViewById(R.id.tv_cancel_open_app_search).setOnClickListener(this);

        mEtInputSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //搜索
                    String searchKey = mEtInputSearch.getText().toString().trim();
                    if (!TextUtils.isEmpty(searchKey)){
                        mSearchKey = searchKey;
                        mCurrentPage=1;
                        getAppExamineList();
                    }
                    return true;
                }

                return false;
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (TextUtils.isEmpty(mSearchKey)){
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                    return;
                }
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    mCurrentPage = 1;
                    isPullRefresh = true;
                    getAppExamineList();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    if (TextUtils.isEmpty(mSearchKey)){
                        mSwipeRefreshLayout.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mSwipeRefreshLayout.finishLoadmore();
                            }
                        }, 500);
                        return;
                    }
                    isLoadmore = true;
                    //mCurrentPage++;//要在获取数据成功后才能++，否则失败了又得--回去
                    getAppExamineList();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });

        mAppOpenManagerAdapter.setOnItemClickListener(new AppOpenManagerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ExamineListBean.ExamineBean examineItemBean) {
                Intent intent = new Intent(AppOpenSearchActivity.this, AppOpenDetailActivity.class);
                intent.putExtra(Constants.INTENT_EXAMINE_APPCODE, examineItemBean.getAppCode());
                intent.putExtra(Constants.INTENT_EXAMINE_BILLNO, examineItemBean.getBillNo());
                startActivityForResult(intent, REQUEST_CODE_GO_DETAIL);
            }
        });
    }

    private void initData() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);

        mAppOpenManagerAdapter = new AppOpenManagerAdapter(this);
        mRvAppOpenSearch.setAdapter(mAppOpenManagerAdapter);
    }

    private void initView() {
        TextView tv_title = findViewById(R.id.tv_title);
        tv_title.setText(getResources().getString(R.string.user_app_open_manager));
        mEtInputSearch = findViewById(R.id.et_input_search);
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        mLlEmptyNoData = findViewById(R.id.ll_empty_no_data);
        mRvAppOpenSearch = findViewById(R.id.rv_app_open_search);
        mRvAppOpenSearch.setLayoutManager(new LinearLayoutManager(this));
        mRvAppOpenSearch.addItemDecoration(new LinearSpaceItemDecoration(DensityUtils.dip2px(this,15),false));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
            case R.id.tv_cancel_open_app_search:
                finish();
                break;
        }
    }


    @Override
    public boolean isNeedEventBus() {
        return true;
    }
    /**
     * 查询应用审核列表
     * @param
     */
    private void getAppExamineList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!(isPullRefresh || isLoadmore)){
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("merchantIdOrName",mSearchKey);
            map.put("pageSize", mPageSize);
            map.put("currentPage", mCurrentPage);
            ServerClient.newInstance(MyApplication.getContext()).getAppExamineList(MyApplication.getContext(), Constants.TAG_GET_APP_EXAMINE_LIST+"_"+getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetAppExamineList(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_APP_EXAMINE_LIST+"_"+getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            setSwipeRefreshState(isPullRefresh, isLoadmore, 500);
            ExamineListBean msg = (ExamineListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_APP_EXAMINE_LIST_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(AppOpenSearchActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_APP_EXAMINE_LIST_TRUE:
                    if (msg != null && msg.getData() != null) {
                        List<ExamineListBean.ExamineBean> examineBeans = msg.getData().getData();
                        if (examineBeans!=null && examineBeans.size()>0){
                            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                            mLlEmptyNoData.setVisibility(View.GONE);
                            if (mCurrentPage==1){
                                mExamineBeans.clear();
                            }
                            mExamineBeans.addAll(examineBeans);
                            mAppOpenManagerAdapter.setData(mExamineBeans);
                            mCurrentPage++;
                        }else {
                            if (isLoadmore) {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                            }else {
                                mExamineBeans.clear();
                                mAppOpenManagerAdapter.setData(mExamineBeans);
                                mSwipeRefreshLayout.setVisibility(View.GONE);
                                mLlEmptyNoData.setVisibility(View.VISIBLE);
                            }
                        }

                    }else {
                        if (isLoadmore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        }else {
                            mExamineBeans.clear();
                            mAppOpenManagerAdapter.setData(mExamineBeans);
                            mSwipeRefreshLayout.setVisibility(View.GONE);
                            mLlEmptyNoData.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isPullRefresh = false;
        }
    }
    /**
     * 关闭下拉刷新和上拉加载的进度条
     * */
    private void setSwipeRefreshState(boolean isPullRefresh, boolean isLoadmore, int delay) {
        if (isPullRefresh) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GO_DETAIL){
            if (resultCode == RESULT_OK){
                mCurrentPage=1;
                getAppExamineList();
                setResult(RESULT_OK);
            }
        }
    }
}