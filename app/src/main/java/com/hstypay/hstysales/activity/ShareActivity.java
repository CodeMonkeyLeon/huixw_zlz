package com.hstypay.hstysales.activity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.Zxing.MaxCardManager;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.ScreenUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CommonNoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.tencent.stat.StatService;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

public class ShareActivity extends BaseActivity implements View.OnClickListener {
    private String mCodeUrl;
    private long mCodeTimeout;
    private Bitmap mQRCode;
    private ImageView mIvBack, mIvShareCode;
    private TextView mTvUserTitle, mTvCodeTimeout;
    private LinearLayout mLlShareWecaht, mLlShareFrients, mLlShareWeibo, mLlShareQQ, mLlShareQQSpace;
    private SafeDialog dialog;
    private UMWeb web;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;

    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
            DialogUtil.safeShowDialog(dialog);
        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(dialog);
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            DialogUtil.safeCloseDialog(dialog);
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(dialog);
        }
    };
    private Button mBtnConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
        PermissionUtils.checkPermissionArray(this, permissionArray, PermissionUtils.PERMISSION_REQUEST_CODE);
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvShareCode = (ImageView) findViewById(R.id.iv_share_qrcode);
        mTvUserTitle = (TextView) findViewById(R.id.tv_user_share_title);
        mTvCodeTimeout = (TextView) findViewById(R.id.tv_code_notice_timeout);
        mLlShareWecaht = (LinearLayout) findViewById(R.id.ll_share_wechat);
        mLlShareFrients = (LinearLayout) findViewById(R.id.ll_share_friends);
        mLlShareWeibo = (LinearLayout) findViewById(R.id.ll_share_weibo);
        mLlShareQQ = (LinearLayout) findViewById(R.id.ll_share_qq);
        mLlShareQQSpace = (LinearLayout) findViewById(R.id.ll_share_qq_space);
        mBtnConfirm = findViewById(R.id.btn_confirm);
        dialog = getLoadDialog(this, getString(R.string.share_loading), true, 0.9f);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mLlShareWecaht.setOnClickListener(this);
        mLlShareFrients.setOnClickListener(this);
        mLlShareWeibo.setOnClickListener(this);
        mLlShareQQ.setOnClickListener(this);
        mLlShareQQSpace.setOnClickListener(this);
        mBtnConfirm.setOnClickListener(this);
    }

    public void initData() {
        mCodeUrl = getIntent().getStringExtra(Constants.INTENT_CODE_URL);
        mCodeTimeout = getIntent().getLongExtra(Constants.INTENT_CODE_TIMEOUT, 0);
        initMedia();
        if (!TextUtils.isEmpty(mCodeUrl)) {
            try {
                mQRCode = MaxCardManager.getInstance().create2DCode(mCodeUrl,
                        DensityUtils.dip2px(ShareActivity.this, 165),
                        DensityUtils.dip2px(ShareActivity.this, 165));
            } catch (WriterException e) {
                e.printStackTrace();
            }
            mIvShareCode.setImageBitmap(mQRCode);
        }
        String realName = SpUtil.getString(MyApplication.getContext(), Constants.SP_REAL_NAME);
        if (TextUtils.isEmpty(realName)) {
            mTvUserTitle.setVisibility(View.GONE);
        } else {
            mTvUserTitle.setText(UIUtils.getString(R.string.share_title_name_before) + realName + UIUtils.getString(R.string.share_title_name_after));
        }
        String timeout = formatTime(mCodeTimeout);
        if (TextUtils.isEmpty(timeout)) {
            mTvCodeTimeout.setVisibility(View.GONE);
        } else {
            mTvCodeTimeout.setText(UIUtils.getString(R.string.share_timeout_before) + timeout + UIUtils.getString(R.string.share_timeout_after));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_share_wechat:
                StatService.trackCustomKVEvent(ShareActivity.this, "1047", null);
                if (TextUtils.isEmpty(mCodeUrl)){
                    ToastUtil.showToastShort(getString(R.string.error_qrcode));
                }else {
                    ShareWeb(SHARE_MEDIA.WEIXIN, R.mipmap.share_icon);
                }
                break;
            case R.id.ll_share_friends:
                StatService.trackCustomKVEvent(ShareActivity.this, "1051", null);
                if (TextUtils.isEmpty(mCodeUrl)){
                    ToastUtil.showToastShort(getString(R.string.error_qrcode));
                }else {
                    ShareWeb(SHARE_MEDIA.WEIXIN_CIRCLE, R.mipmap.friends_logo);
                }
                break;
            case R.id.ll_share_weibo:
                StatService.trackCustomKVEvent(ShareActivity.this, "1049", null);

                break;
            case R.id.ll_share_qq:
                StatService.trackCustomKVEvent(ShareActivity.this, "1048", null);
                if (TextUtils.isEmpty(mCodeUrl)){
                    ToastUtil.showToastShort(getString(R.string.error_qrcode));
                }else {
                    ShareWeb(SHARE_MEDIA.QQ, R.mipmap.share_icon);
                }
                break;
            case R.id.ll_share_qq_space:
                StatService.trackCustomKVEvent(ShareActivity.this, "1050", null);
                if (TextUtils.isEmpty(mCodeUrl)){
                    ToastUtil.showToastShort(getString(R.string.error_qrcode));
                }else {
                    ShareWeb(SHARE_MEDIA.QZONE, R.mipmap.space_logo);
                }
                break;

            case R.id.btn_confirm:
                //分享到相册
                boolean results = PermissionUtils.checkPermissionArray(ShareActivity.this, permissionArray);
                if (results) {
                    ScreenUtils.saveImageToGallery(this, ScreenUtils.createViewBitmap(findViewById(R.id.ll_save_to_photo)));
                    CommonNoticeDialog dialog = new CommonNoticeDialog(ShareActivity.this,  getString(R.string.dialog_copy_picture), "");
                    DialogHelper.resize(ShareActivity.this, dialog);
                    dialog.show();
                } else {
                    showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_storage));
                }
                break;
        }
    }

    private void initMedia() {
        web = new UMWeb(mCodeUrl);
        LogUtil.d("mCodeUrl====" + mCodeUrl);
        web.setDescription("我叫" + SpUtil.getString(MyApplication.getContext(), Constants.SP_REAL_NAME) + "，我为汇旺财代言。");
        web.setTitle("汇旺财，会发财");
    }

    private void ShareWeb(SHARE_MEDIA platform, int thumb_img) {
        UMImage thumb = new UMImage(ShareActivity.this, thumb_img);
        web.setThumb(thumb);
        new ShareAction(ShareActivity.this)
                .withMedia(web)
                .setPlatform(platform)
                .setCallback(shareListener).share();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DialogUtil.safeCloseDialog(dialog);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(dialog);
        UMShareAPI.get(this).release();
    }

    private String formatTime(long timeout) {
        if (timeout >= 3600) {
            return timeout / 60 / 60 + "小时";
        } else if (timeout >= 60) {
            return timeout / 60 + "分钟";
        } else if (timeout > 0) {
            return timeout + "秒";
        } else {
            return "";
        }
    }
}
