package com.hstypay.hstysales.activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.DeviceOrderListBean;
import com.hstypay.hstysales.fragment.devicemall.BaseDeviceMallFragment;
import com.hstypay.hstysales.fragment.devicemall.home.DeviceHomeFragment;
import com.hstypay.hstysales.fragment.devicemall.search.DeviceSearchFragment;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CustomViewFullScreenDialog;
import com.hstypay.hstysales.widget.EditTextDelete;
import com.hstypay.hstysales.widget.SafeDialog;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zeyu.kuang
 * @time 2020/8/23
 * @desc 硬件商城订单管理页
 */
public class DeviceMallOrderActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvTitleSearch;
    private ImageView mIvTitleBack;
    private DeviceHomeFragment mDeviceHomeFragment;
    private DeviceSearchFragment mDeviceSearchFragment;
    private CustomViewFullScreenDialog mSearchDialog;
    private FragmentManager mFragmentManager;
    private SafeDialog mLoadDialog;
    private String mSearchKey;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_mall_order);
        StatusBarUtil.setTranslucentStatusWhiteIcon(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initEvent() {
        mIvTitleSearch.setOnClickListener(this);
        mIvTitleBack.setOnClickListener(this);
        mDeviceSearchFragment.setOnUpDateTransitCountListener(new BaseDeviceMallFragment.onUpDateTransitCountListener() {
            @Override
            public void onUpDateTransitCount(boolean needLoading) {
                mDeviceHomeFragment.getTransitCount(needLoading);
            }
        });
    }

    private void initData() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mDeviceHomeFragment = new DeviceHomeFragment();
        mDeviceSearchFragment = new DeviceSearchFragment();
        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(R.id.fl_content_device_mall, mDeviceHomeFragment,DeviceHomeFragment.class.getSimpleName());
        transaction.commit();
    }

    private void initView() {
        setTitleView();
    }

    private void setTitleView() {
        View rl_title_mall = findViewById(R.id.rl_title_mall);
        rl_title_mall.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView tv_title = findViewById(R.id.tv_title);
        tv_title.setText(getString(R.string.title_device_mall));
        mIvTitleSearch = findViewById(R.id.iv_title_button);
        mIvTitleSearch.setVisibility(View.VISIBLE);
        mIvTitleSearch.setImageResource(R.mipmap.icon_general_top_search);
        mIvTitleBack = findViewById(R.id.iv_back);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_title_button://搜索
                showSearchDialog();
                break;
            case R.id.iv_back://返回
                onBackPressed();
                break;
            default:
                break;
        }
    }
    //展示搜索弹框
    private void showSearchDialog() {
        mSearchDialog = new CustomViewFullScreenDialog(this);
        mSearchDialog.setView(R.layout.layout_diialog_search_order_mall);
        final EditTextDelete et_search_order_dialog_mall = mSearchDialog.findViewById(R.id.et_search_order_dialog_mall);
        final TextView tv_hint_search_order = mSearchDialog.findViewById(R.id.tv_hint_search_order);
        Button btn_cancel_search_order_mall = mSearchDialog.findViewById(R.id.btn_cancel_search_order_mall);
        Button btn_sure_search_order_mall = mSearchDialog.findViewById(R.id.btn_sure_search_order_mall);
        mSearchDialog.show();
        et_search_order_dialog_mall.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear){
                    tv_hint_search_order.setVisibility(View.VISIBLE);
                }else {
                    tv_hint_search_order.setVisibility(View.GONE);
                }
            }
        });
        btn_cancel_search_order_mall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchDialog.dismiss();
            }
        });
        btn_sure_search_order_mall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchKey = et_search_order_dialog_mall.getText().toString();
                if (TextUtils.isEmpty(searchKey)){
                    ToastUtil.showToastShort(getResources().getString(R.string.input_search_key_mall));
                    return;
                }
                startSearch(searchKey);
            }
        });
    }
    //开始搜索订单
    private void startSearch(String searchKey) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mSearchKey = searchKey;
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("tradeStatus","");//在全部订单中搜索
            map.put("pageSize", BaseDeviceMallFragment.pageSize);
            map.put("currentPage", 1);
            map.put("searchParam",searchKey);
            map.put("sortType","asc");//排序，asc 就是根据下单时间顺序排
            ServerClient.newInstance(MyApplication.getContext()).getDeviceOrderList(MyApplication.getContext(), Constants.TAG_GET_DEVICE_ORDER_LIST+"_"+this.getClass().getSimpleName(), map);//避免其它Fragment也会处理eventBus事件
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onStartSearchEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_DEVICE_ORDER_LIST+"_"+this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            DeviceOrderListBean msg = (DeviceOrderListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_DEVICE_ORDER_LIST_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(DeviceMallOrderActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_DEVICE_ORDER_LIST_TRUE:
                    if (msg != null && msg.getData() != null) {
                        if (msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                            mSearchDialog.dismiss();
                            ArrayList<DeviceOrderListBean.DataEntity.DataBean> dataBeans = msg.getData().getData();
                            if (mDeviceSearchFragment.isVisible()){
                                //当前展示的是DeviceSearchFragment
                                mDeviceSearchFragment.refreshData(dataBeans,mSearchKey,""+msg.getData().getTotalRows());
                            }else {
                                //当前展示的是DeviceHomeFragment
                                Bundle bundle = new Bundle();
                                bundle.putString(DeviceSearchFragment.KEY_BUNDLE_SEARCH_PARAM,mSearchKey);
                                bundle.putString(DeviceSearchFragment.KEY_BUNDLE_TOTAL_COUNT,""+msg.getData().getTotalRows());
                                bundle.putSerializable(DeviceSearchFragment.KEY_BUNDLE_ORDERS,  dataBeans);
                                mDeviceSearchFragment.setArguments(bundle);
                                FragmentTransaction transaction = mFragmentManager.beginTransaction();
                                transaction.addToBackStack(null);
                                transaction.add(R.id.fl_content_device_mall,mDeviceSearchFragment,DeviceSearchFragment.class.getSimpleName());
                                transaction.commit();
                            }
                        } else {
                            ToastUtil.showToastShort(getResources().getString(R.string.search_no_data));
                        }
                    } else {
                        ToastUtil.showToastShort(getResources().getString(R.string.search_no_data));
                    }
                    break;
            }
        }
    }

}