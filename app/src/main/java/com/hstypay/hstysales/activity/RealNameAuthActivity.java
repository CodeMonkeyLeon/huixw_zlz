package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.listener.OnSingleClickListener;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.StatusBarUtil;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: 验证身份
 */

public class RealNameAuthActivity extends BaseActivity {
    private TextView mTvTitle;
    private ImageView mIvBack;
    private ConstraintLayout mClWx,mClAli;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realname_auth);
        StatusBarUtil.setTranslucentStatusWhiteIcon(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
    }

    private void initView() {
        mIvBack = findViewById(R.id.iv_back);
        mClWx = findViewById(R.id.cl_wx);
        mClAli = findViewById(R.id.cl_ali);
        mTvTitle = findViewById(R.id.tv_title);

        mTvTitle.setText(R.string.real_name_auth);
    }

    private void initEvent() {
        OnSingleClickListener onSingleClickListener = new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                switch (view.getId()) {
                    case R.id.iv_back:
                        finish();
                        break;
                    case R.id.cl_wx:
                        startActivity(new Intent(RealNameAuthActivity.this, WxRealAuthorMerchantListActivity.class)
                                .putExtra(Constants.INTENT_NAME,"INTENT_WX_AUTH"));
                        break;
                    case R.id.cl_ali:
                        startActivity(new Intent(RealNameAuthActivity.this, WxRealAuthorMerchantListActivity.class)
                                .putExtra(Constants.INTENT_NAME,"INTENT_ALI_AUTH"));
                        break;
                }
            }
        };
        mClWx.setOnClickListener(onSingleClickListener);
        mClAli.setOnClickListener(onSingleClickListener);
        mIvBack.setOnClickListener(onSingleClickListener);
    }
}
