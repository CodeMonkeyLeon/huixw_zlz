package com.hstypay.hstysales.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hstypay.hstysales.BuildConfig;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.ImageBean;
import com.hstypay.hstysales.bean.InfoBean;
import com.hstypay.hstysales.bean.VipInfoBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.FileUtils;
import com.hstypay.hstysales.utils.ImagePase;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.PicUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.LogoNoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.SelectPicPopupWindow;
import com.hstypay.hstysales.widget.ShadowLayout;
import com.hstypay.hstysales.widget.clipImage.CircleImageView;
import com.squareup.picasso.Picasso;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class VipInfoActivity extends BaseActivity implements View.OnClickListener {


    private TextView mTvTitle, mTvLogo, mTvNotice, mTvReason, mTvInfoNotice, mTvAgreeProtocol;
    private ImageView mIvBack, mIvCheckStatus, mIvBrandClean, mIvRateClean, mIvLogo, mIvLogoBg, mIvAgreeProtocol;
    private ScrollView mSclMemberDetail;
    private Button mBtnEnsure, mBtnUnable;
    private ShadowLayout mShadowButton;
    private LinearLayout mLlExamineStatus, mLlAgreeProtocol;
    private RelativeLayout mRlButton, mRlLogo;
    private CircleImageView mCvLogo;
    private View mViewLogoWhite;
    private EditText mEtVipBrand, mEtVipRate;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE6 = 106;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x1001;
    public static final int REQUEST_CODE_CAPTURE_PICTURE = 0x1002;
    private String imageUrl;
    private File tempFile;
    private Uri originalUri;
    private String picPath;
    private SafeDialog mLoadDialog;
    private String mBrandLogoPhoto;
    private boolean mAgreeProtocol = true;
    private String mMerchantId;
    private VipInfoBean.DataEntity mVipInfo;
    private String mIntentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        setContentView(R.layout.activity_vip_info);
        StatusBarUtil.setTranslucentStatus(this);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle.setText(getString(R.string.title_vip_info));

        mSclMemberDetail = (ScrollView) findViewById(R.id.sl_member_detail);
        mLlExamineStatus = (LinearLayout) findViewById(R.id.ll_examine_status);
        mIvCheckStatus = (ImageView) findViewById(R.id.iv_check_status);
        mTvNotice = (TextView) findViewById(R.id.tv_vip_verify_notice);
        mTvReason = (TextView) findViewById(R.id.tv_vip_verify_reason);

        mIvLogoBg = (ImageView) findViewById(R.id.iv_vip_logo_bg);
        mViewLogoWhite = findViewById(R.id.view_vip_logo_white);
        mRlLogo = (RelativeLayout) findViewById(R.id.rl_vip_logo);
        mCvLogo = (CircleImageView) findViewById(R.id.cv_vip_logo);
        mIvLogo = (ImageView) findViewById(R.id.iv_vip_logo);
        mTvLogo = (TextView) findViewById(R.id.tv_vip_logo);

        mTvInfoNotice = (TextView) findViewById(R.id.tv_vip_info_notice);

        mEtVipBrand = (EditText) findViewById(R.id.et_vip_brand);
        mIvBrandClean = (ImageView) findViewById(R.id.iv_vip_brand_clean);
        mEtVipRate = (EditText) findViewById(R.id.et_vip_rate);
        mIvRateClean = (ImageView) findViewById(R.id.iv_vip_rate_clean);

        mLlAgreeProtocol = (LinearLayout) findViewById(R.id.ll_agree_protocol);
        mIvAgreeProtocol = (ImageView) findViewById(R.id.iv_agree_protocol);
        mTvAgreeProtocol = (TextView) findViewById(R.id.tv_agree_protocol);

        mRlButton = (RelativeLayout) findViewById(R.id.rl_button);
        mShadowButton = (ShadowLayout) findViewById(R.id.sl_button);
        mBtnEnsure = (Button) findViewById(R.id.btn_common_enable);
        mBtnUnable = (Button) findViewById(R.id.btn_common_unable);

        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
    }

    private void initListener() {
        mBtnEnsure.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mRlLogo.setOnClickListener(this);
        mIvAgreeProtocol.setOnClickListener(this);
        mTvAgreeProtocol.setOnClickListener(this);
        mIvBrandClean.setOnClickListener(this);
        mIvRateClean.setOnClickListener(this);

        mEtVipBrand.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mIvBrandClean.setVisibility(s.length() != 0 ? View.VISIBLE : View.GONE);
                setButtonView();
            }
        });
        mEtVipRate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mIvRateClean.setVisibility(s.length() != 0 ? View.VISIBLE : View.GONE);
                setButtonView();
            }
        });
    }

    private void initData() {
        //初始化状态
        boolean showDetail = getIntent().getBooleanExtra(Constants.INTENT_SKIP_MEMBER_DETAIL, false);
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        if (showDetail) {
            mIvLogoBg.getBackground().setAlpha(255);
            mIvLogo.setVisibility(View.GONE);
            mTvInfoNotice.setVisibility(View.GONE);
            mTvLogo.setText(getString(R.string.vip_logo_checking));
            mVipInfo = (VipInfoBean.DataEntity) getIntent().getSerializableExtra(Constants.INTENT_VIP_INFO);
            setView(mVipInfo);
        } else {
            mTvLogo.setText(getString(R.string.vip_logo_first));
            mTvInfoNotice.setVisibility(View.VISIBLE);
            mIvLogoBg.getBackground().setAlpha(100);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_agree_protocol:
                if (mAgreeProtocol) {
                    mAgreeProtocol = false;
                    mIvAgreeProtocol.setImageResource(R.mipmap.checkbox_unselected);
                } else {
                    mAgreeProtocol = true;
                    mIvAgreeProtocol.setImageResource(R.mipmap.checkbox_selected);
                }
                setButtonView();
                break;
            case R.id.tv_agree_protocol:
                Intent intent = new Intent(VipInfoActivity.this, RegisterActivity.class);
                intent.putExtra(Constants.REGISTER_INTENT, Constants.URL_COOPERATION_PROTOCOL);
                startActivity(intent);
                break;
            case R.id.rl_vip_logo:
                showDialog();
                /*PermissionUtils.checkPermissionArray(this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE6);
                choice();*/
                break;
            case R.id.iv_vip_brand_clean:
                mEtVipBrand.setText("");
                break;
            case R.id.iv_vip_rate_clean:
                mEtVipRate.setText("");
                break;
            case R.id.btn_common_enable:
                submitInfo();
                break;
        }
    }

    private void showDialog() {
        LogoNoticeDialog dialog = new LogoNoticeDialog(VipInfoActivity.this);
        dialog.setOnClickOkListener(new LogoNoticeDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                PermissionUtils.checkPermissionArray(VipInfoActivity.this, permissionArray, MY_PERMISSIONS_REQUEST_CALL_PHONE6);
                //choice();
                final String status = Environment.getExternalStorageState();
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    takeImg();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.tx_sd_pic));
                }
            }
        });
        //DialogHelper.resize(VipInfoActivity.this, dialog);
        dialog.show();
    }

    private void submitInfo() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", mMerchantId);
            map.put("brandLogo", mBrandLogoPhoto);
            map.put("brandName", mEtVipBrand.getText().toString().trim());
            try {
                BigDecimal bigStr = new BigDecimal(mEtVipRate.getText().toString().trim().toString()).multiply(new BigDecimal(10000)).setScale(0, BigDecimal.ROUND_DOWN);
                map.put("costRate", bigStr.toString());
            } catch (Exception e) {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_rate_number));
                return;//异常 说明包含非数字。
            }
            DialogUtil.safeShowDialog(mLoadDialog);
            if (mVipInfo != null) {
                ServerClient.newInstance(MyApplication.getContext()).editMchMember(MyApplication.getContext(), Constants.TAG_EDIT_VIP_INFO, map);
            } else {
                ServerClient.newInstance(MyApplication.getContext()).addVipInfo(MyApplication.getContext(), Constants.TAG_ADD_VIP_INFO, map);
            }
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_ADD_VIP_INFO) || event.getTag().equals(Constants.TAG_EDIT_VIP_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(VipInfoActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    Intent intent = new Intent(VipInfoActivity.this, SubmitSuccessActivity.class);
                    //intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_VIP_SUCCESS);
                    if (!TextUtils.isEmpty(mIntentName)) {
                        intent.putExtra(Constants.INTENT_NAME, mIntentName);
                    }
                    startActivity(intent);
                    break;
            }
        }
    }

    private void choice() {
        final String status = Environment.getExternalStorageState();
        SelectPicPopupWindow picPopupWindow = new SelectPicPopupWindow(VipInfoActivity.this, new SelectPicPopupWindow.HandleTv() {
            @Override
            public void takePic() {
                if (status.equals(Environment.MEDIA_MOUNTED)) {
                    //打开相机
                    try {
                        startCamrae();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void choicePic() {
                //从相册选择图片
                try {
                    if (status.equals(Environment.MEDIA_MOUNTED)) {
                        takeImg();
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.tx_sd_pic));
                    }
                } catch (Exception e) {

                }
            }
        });
        picPopupWindow.showAtLocation(mRlLogo, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    private void startCamrae() throws Exception {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String root = AppHelper.getImgCacheDir();
        imageUrl = root + String.valueOf(new Date().getTime()) + ".jpg";
        tempFile = new File(imageUrl);
        if (!tempFile.getParentFile().exists()){
            tempFile.getParentFile().mkdirs();
        }
        if (Build.VERSION.SDK_INT >= 24) {
            //如果是则使用FileProvider
            originalUri = FileProvider.getUriForFile(VipInfoActivity.this, BuildConfig.APPLICATION_ID + ".fileProvider", tempFile);
        } else {
            originalUri = Uri.fromFile(tempFile);
        }
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, originalUri);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_PICTURE);
    }

    private void takeImg() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setDataAndType(MediaStore.Images.Media.INTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                //获取相册图片
                case REQUEST_CODE_TAKE_PICTURE:
                    if (data.getData() == null) {
                        return;
                    }
                    String path = AppHelper.getPicPath(data.getData());
                    Bitmap bitmap = null;
                    if (path != null) {
                        /*File file = new File(path);
                        int MAX_POST_SIZE = 2 * 1024 * 1024;
                        if (file.exists() && file.length() > MAX_POST_SIZE) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.tv_pic_than_2MB));
                            return;
                        } else {
                            bitmap = ImagePase.readBitmapFromStream(path);
                        }*/
                        bitmap = ImagePase.readBitmapFromStream(path);
                        if (bitmap != null) {
                            picPath = path;
                            mIvLogoBg.getBackground().setAlpha(255);
                            mCvLogo.setImageBitmap(bitmap);
                            mIvLogo.setVisibility(View.GONE);
                            uploadImage(picPath, "accessory");
                        }
                    }
                    break;

                // 拍照
                case REQUEST_CODE_CAPTURE_PICTURE:
                    String pathPhoto = AppHelper.getPicPath(originalUri);
                    LogUtil.d("path=" + pathPhoto);
                    /*Bitmap bitmap_pci = null;
                    File file = new File(pathPhoto);
                    if (file.exists() && file.length() / 1024 > 100) {
                        bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                    } else {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                            bitmap_pci = ImagePase.createBitmap(imageUrl, ImagePase.bitmapSize_Image);
                        }
                    }*/
                    Bitmap bitmap_pci = null;
                    File file = new File(pathPhoto);
                    if (!file.exists()) {
                        File f = new File(imageUrl);
                        if (f.exists()) {
                            pathPhoto = imageUrl;
                        }
                    }
                    bitmap_pci = ImagePase.createBitmap(pathPhoto, ImagePase.bitmapSize_Image);
                    if (bitmap_pci != null) {
                        picPath = pathPhoto;
                        mIvLogoBg.getBackground().setAlpha(255);
                        mCvLogo.setImageBitmap(bitmap_pci);
                        mIvLogo.setVisibility(View.GONE);
                        uploadImage(picPath, "accessory");
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void uploadImage(String path, final String type) {
        if (!NetworkUtils.isNetworkAvailable(VipInfoActivity.this)) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
            mBrandLogoPhoto = "";
            mViewLogoWhite.setVisibility(View.GONE);
            mIvLogo.setVisibility(View.VISIBLE);
            mCvLogo.setImageResource(R.drawable.shape_vip_logo_bg);
            mIvLogoBg.getBackground().setAlpha(100);
            setButtonView();
            return;
        }
        LogUtil.d("path----+==+" + PicUtil.getFileHeader(path));
        DialogUtil.safeShowDialog(mLoadDialog);
        String url = Constants.BASE_URL + "/app/merchant/member/img";
        PostFormBuilder post = OkHttpUtils.post();
        String pathCompress = null;
        if (!TextUtils.isEmpty(path) && path.contains(".")) {
            pathCompress = PicUtil.fileToJPG(path);
            String postfix = pathCompress.substring(pathCompress.lastIndexOf("."));
             /*pathCompress = path.substring(0, path.lastIndexOf(".")) + "(1)"+postfix;*/
            LogUtil.d("path=========1-" + pathCompress);
//            ImageFactory.compressPicture(path, pathCompress);
            post = post.addFile(type, type + postfix, new File(pathCompress));
        } else {
            ToastUtil.showToastShort("文件格式有误！");
        }
        final String finalPathCompress = pathCompress;
        post.url(url).build().
                connTimeOut(30000).writeTimeOut(30000).readTimeOut(30000).execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                e.printStackTrace();
                DialogUtil.safeCloseDialog(mLoadDialog);
                mBrandLogoPhoto = "";
                mViewLogoWhite.setVisibility(View.GONE);
                mIvLogo.setVisibility(View.VISIBLE);
                mCvLogo.setImageResource(R.drawable.shape_vip_logo_bg);
                mIvLogoBg.getBackground().setAlpha(100);
                ToastUtil.showToastShort("上传失败");
                FileUtils.deleteFile(finalPathCompress);
                setButtonView();
            }

            @Override
            public void onResponse(String response, int id) {
                LogUtil.i("Jeremy", "response" + response);
                DialogUtil.safeCloseDialog(mLoadDialog);
                FileUtils.deleteFile(finalPathCompress);
                Gson gson = new Gson();
                ImageBean imageBean = gson.fromJson(response, ImageBean.class);
                if (imageBean != null && imageBean.getData() != null) {
                    mBrandLogoPhoto = imageBean.getData().getLogoPath();
                    if (TextUtils.isEmpty(mBrandLogoPhoto)){
                        mBrandLogoPhoto = "";
                        mViewLogoWhite.setVisibility(View.GONE);
                        mIvLogo.setVisibility(View.VISIBLE);
                        mCvLogo.setImageResource(R.drawable.shape_vip_logo_bg);
                        mIvLogoBg.getBackground().setAlpha(100);
                        ToastUtil.showToastShort("上传失败");
                    }else {
                        mViewLogoWhite.setVisibility(View.VISIBLE);
                        ToastUtil.showToastShort("上传成功");
                    }
                } else {
                    mBrandLogoPhoto = "";
                    mViewLogoWhite.setVisibility(View.GONE);
                    mIvLogo.setVisibility(View.VISIBLE);
                    mCvLogo.setImageResource(R.drawable.shape_vip_logo_bg);
                    mIvLogoBg.getBackground().setAlpha(100);
                    ToastUtil.showToastShort("上传失败");
                }
                setButtonView();
            }
        });
    }

    public boolean getButtonState() {
        return mEtVipBrand.getText().toString().trim().length() == 0
                || mEtVipRate.getText().toString().trim().length() == 0
                || TextUtils.isEmpty(mBrandLogoPhoto) || mAgreeProtocol == false;
    }

    private void setButtonView() {
        if (getButtonState()) {
            mShadowButton.setVisibility(View.GONE);
            mBtnUnable.setVisibility(View.VISIBLE);
        } else {
            mShadowButton.setVisibility(View.VISIBLE);
            mBtnUnable.setVisibility(View.GONE);
        }
    }

    private void setView(VipInfoBean.DataEntity vipInfo) {
        if (vipInfo != null) {
            mLlAgreeProtocol.setVisibility(View.GONE);
            mEtVipBrand.setText(vipInfo.getBrandName());
            mEtVipBrand.setSelection(mEtVipBrand.getText().length());
            BigDecimal divide = new BigDecimal(vipInfo.getCostRate()).divide(new BigDecimal("10000"));
            mEtVipRate.setText(divide.toString());
            mBrandLogoPhoto = vipInfo.getBrandLogo();
            if (!StringUtils.isEmptyOrNull(vipInfo.getBrandLogo())) {
                Picasso.get()
                        .load(Constants.BASE_URL + vipInfo.getBrandLogo())
                        .placeholder(R.mipmap.icon_general_noloading)
                        .error(R.mipmap.icon_general_noloading)
                        .into(mCvLogo);
            }
            mViewLogoWhite.setVisibility(View.VISIBLE);
            if (vipInfo.getExamineStatus() == 2) {
                mLlExamineStatus.setVisibility(View.VISIBLE);
                mIvCheckStatus.setImageResource(R.mipmap.icon_examine);
                mTvReason.setVisibility(View.GONE);
                mTvNotice.setText(getString(R.string.tx_checking));
                mRlButton.setVisibility(View.GONE);
                mTvReason.setVisibility(View.GONE);
                mIvBrandClean.setVisibility(View.INVISIBLE);
                mIvRateClean.setVisibility(View.INVISIBLE);
                mEtVipBrand.setEnabled(false);
                mEtVipRate.setEnabled(false);
                mRlLogo.setEnabled(false);
                mEtVipBrand.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
                mEtVipRate.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
            } else if (vipInfo.getExamineStatus() == 4) {
                mLlExamineStatus.setVisibility(View.VISIBLE);
                mIvCheckStatus.setImageResource(R.mipmap.icon_examine_fail);
                mTvReason.setVisibility(View.VISIBLE);
                mTvNotice.setText(getString(R.string.vip_info_check_failed));
                mRlButton.setVisibility(View.VISIBLE);
                mTvReason.setVisibility(View.VISIBLE);
                mTvReason.setText(vipInfo.getExamineRemark());
                mBtnEnsure.setText(getString(R.string.btn_info_resubmit));
                mBtnUnable.setText(getString(R.string.btn_info_resubmit));
                mEtVipBrand.setEnabled(true);
                mEtVipRate.setEnabled(true);
                mEtVipBrand.setTextColor(UIUtils.getColor(R.color.setting_tx_color));
                mEtVipRate.setTextColor(UIUtils.getColor(R.color.setting_tx_color));
            }
            setButtonView();
        }
    }
}