package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.adapter.BindingCodeAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.StoreListBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CustomLinearLayoutManager;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/7/7.
 */

public class BindingCodeActivity extends BaseActivity implements View.OnClickListener, BindingCodeAdapter.OnRecyclerViewItemClickListener {

    private ImageView mIvBack;
    private TextView mTvTitle;
    private RecyclerView mRvShop;
    private CustomLinearLayoutManager mLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private int currentPage = 1;
    private int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<StoreListBean.DataEntity> mList = new ArrayList<>();
    private BindingCodeAdapter mAdapter;
    private String mIntentName,mMerchantId;
    private SafeDialog mLoadDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binding_code);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText(getString(R.string.bind_code));

        initRecyclerView();
        initSwipeRefreshLayout();
    }

    private void initRecyclerView() {
        mRvShop = (RecyclerView) findViewById(R.id.shop_recyclerview);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRvShop.setLayoutManager(mLinearLayoutManager);
        mRvShop.setItemAnimator(new DefaultItemAnimator());
    }

    private void initSwipeRefreshLayout() {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setRefreshEnable(false);
        //mSwipeRefreshLayout.setFooterView(R.layout.refresh_view);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    currentPage = 1;
                    isRefreshed = true;
                    getData(mMerchantId,pageSize, currentPage);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getData(mMerchantId,pageSize, currentPage);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载...");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });
    }

    public void initData() {
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERHANT_ID);
        mList = new ArrayList<>();
        mAdapter = new BindingCodeAdapter(BindingCodeActivity.this, mList);
        mRvShop.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            isRefreshed = true;
            currentPage = 1;
            DialogUtil.safeShowDialog(mLoadDialog);
            getData(mMerchantId,pageSize, currentPage);
        }else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getData(String merchantId,int pageSize, int currentPage) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", merchantId);
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_CHOICE_STORE, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }


    //选择门店接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void EmpManage(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_CHOICE_STORE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        mList.addAll(msg.getData());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mList.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                //startActivity(new Intent(this,StoreCodeActivity.class));
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(this, CaptureActivity.class);
        intent.putExtra(Constants.INTENT_BIND_STORE_ID, mList.get(position).getStoreId());
        intent.putExtra(Constants.INTENT_BIND_STORE_NAME, mList.get(position).getStoreName());
        intent.putExtra(Constants.INTENT_NAME, mIntentName);
        startActivity(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
