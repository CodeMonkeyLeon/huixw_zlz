package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.InfoBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.EditTextWatcher;
import com.hstypay.hstysales.widget.NoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.ShadowLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by admin on 2017/7/5.
 */

public class ResetPwdActivity extends BaseActivity implements View.OnClickListener {

    private ImageView iv_back;
    private EditText mEtPwd1;
    private ImageView mIvEyeNew, mIvEyeEnsure;
    private EditText mEtPwd2;
    private Button mBtnEnable, mBtnUnable;
    private TextView mTvTitle;
    private String mTelphone;
    private boolean isOpenNew, isOpenEnsure;
    private ShadowLayout mShadowButton;
    private NoticeDialog mNoticeDialog;
    private SafeDialog mLoadDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pwd);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
    }

    private void initListener() {
        iv_back.setOnClickListener(this);
        mBtnEnable.setOnClickListener(this);
        mIvEyeNew.setOnClickListener(this);
        mIvEyeEnsure.setOnClickListener(this);
    }

    private void initView() {
        mTelphone = getIntent().getStringExtra(Constants.INTENT_RESET_PWD_TELPHONE);
        iv_back = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTitle.setText("重置密码");

        mEtPwd1 = (EditText) findViewById(R.id.et_new_pwd1);
        mEtPwd2 = (EditText) findViewById(R.id.et_new_pwd2);
        mIvEyeNew = (ImageView) findViewById(R.id.iv_eye_new);
        mIvEyeEnsure = (ImageView) findViewById(R.id.iv_eye_ensure);
        mShadowButton = (ShadowLayout) findViewById(R.id.shadow_button);
        mBtnEnable = (Button) findViewById(R.id.btn_common_enable);
        mBtnUnable = (Button) findViewById(R.id.btn_common_unable);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (!StringUtils.isEmptyOrNull(mEtPwd1.getText().toString().trim())
                        && !StringUtils.isEmptyOrNull(mEtPwd2.getText().toString().trim())) {
                    mShadowButton.setVisibility(View.VISIBLE);
                    mBtnUnable.setVisibility(View.GONE);
                } else {
                    mShadowButton.setVisibility(View.GONE);
                    mBtnUnable.setVisibility(View.VISIBLE);
                }
            }
        });
        mEtPwd1.addTextChangedListener(editTextWatcher);
        mEtPwd2.addTextChangedListener(editTextWatcher);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            //确定
            case R.id.btn_common_enable:
                nextStep();
                break;
            case R.id.iv_eye_new:
                isOpenNew = setEye(isOpenNew);
                setPwdVisible(isOpenNew, mIvEyeNew, mEtPwd1);
                break;
            case R.id.iv_eye_ensure:
                isOpenEnsure = setEye(isOpenEnsure);
                setPwdVisible(isOpenEnsure, mIvEyeEnsure, mEtPwd2);
                break;
        }
    }

    private void nextStep() {
        if (StringUtils.isEmptyOrNull(mEtPwd1.getText().toString().trim()) || StringUtils.isEmptyOrNull(mEtPwd2.getText().toString().trim())) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_userpwd));
            return;
        }

        if (!mEtPwd1.getText().toString().trim().equals(mEtPwd2.getText().toString().trim())) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_pwd_different));
            mEtPwd2.setFocusable(true);
            mEtPwd2.setFocusableInTouchMode(true);
            mEtPwd2.requestFocus();
            return;
        }

        if (!StringUtils.isLegalPwd(mEtPwd1.getText().toString().trim())){
            mEtPwd1.setFocusable(true);
            mEtPwd1.setFocusableInTouchMode(true);
            mEtPwd1.requestFocus();
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_password_legal));
            return;
        }

        if (!StringUtils.isLegalPwd(mEtPwd2.getText().toString().trim())){
            mEtPwd2.setFocusable(true);
            mEtPwd2.setFocusableInTouchMode(true);
            mEtPwd2.requestFocus();
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_password_legal));
            return;
        }
        /*if (mEtPwd1.getText().toString().trim().length() < 6 || mEtPwd2.getText().toString().trim().length() < 6) {
            ToastUtil.showToastShort("");
            return;
        }*/

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).resetPwd(MyApplication.getContext(), Constants.TAG_RESET_PWD, mEtPwd2.getText().toString().trim(), mTelphone);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }

        //密码修改成功跳转到登录界面
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据,验证手机号
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResetPassword(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_RESET_PWD)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.RESET_PWD_FALSE:
                    if (msg != null && msg.getError() != null) {
                        if (!TextUtils.isEmpty(msg.getError().getMessage()))
                            ToastUtil.showToastShort(msg.getError().getMessage());
                    }
                    break;
                case Constants.RESET_PWD_TRUE://验证码返回成功
                    showDialog();
                    break;
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    private void showDialog() {
        if (mNoticeDialog == null) {
            mNoticeDialog = new NoticeDialog(ResetPwdActivity.this, UIUtils.getString(R.string.dialog_notice_reset_pwd)
                    , UIUtils.getString(R.string.dialog_notice_button), R.layout.notice_dialog_text);
            mNoticeDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    mNoticeDialog.dismiss();
                    startActivity(new Intent(ResetPwdActivity.this, LoginActivity.class));
                    finish();
                }
            });
        }
        mNoticeDialog.show();
    }

    private void setPwdVisible(boolean isOpen, ImageView ivEye, EditText etPwd) {
        if (isOpen) {
            ivEye.setImageResource(R.mipmap.change_password_eyes);
            etPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ivEye.setImageResource(R.mipmap.change_password_close);
            etPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
        etPwd.setSelection(etPwd.getText().toString().trim().length());
    }

    private boolean setEye(boolean isOpen) {
        return isOpen ? false : true;
    }

}
