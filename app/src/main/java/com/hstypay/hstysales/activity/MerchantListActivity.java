package com.hstypay.hstysales.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.adapter.MerchantAdapter;
import com.hstypay.hstysales.adapter.SalesmanAdapter;
import com.hstypay.hstysales.adapter.ServiceProviderAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.MerchantListBean;
import com.hstypay.hstysales.bean.SalesmanBean;
import com.hstypay.hstysales.bean.ServiceProviderBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CustomLinearLayoutManager;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;
import com.tencent.stat.StatService;
import com.ykk.dropdownmenu.DropDownMenu;
import com.ykk.dropdownmenu.TabBean;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class MerchantListActivity extends BaseActivity implements View.OnClickListener {
    private RecyclerView mMerchantRecyclerview;
    private CustomLinearLayoutManager mCustomLinearLayoutManager;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private ImageView mIvBack, mIvDataClean;
    private TextView mTvTitle;
    private MerchantAdapter mMerchantAdapter;
    private int type;//页面标识
    private String propValue = "";
    private SafeDialog mLoadDialog;
    private EditText mEtSearch;

    private int currentPage = 2;
    public static final int pageSize = 15;
    private boolean isRefreshed;//刷新过
    private boolean isLoadmore;//加载更多
    private List<MerchantListBean.DataEntity.ItemData> mListData = new ArrayList<>();
    private RelativeLayout mLayoutEmpty;
    private int mSearchPageSize = 15;
    private int mSearchCurrentPage = 1;
    private boolean isLoadMerchantMore;
    private boolean isLoadOver;
    private int mSelectLabelPosition;
    private ServiceProviderAdapter mServiceProviderAdapter;
    private SalesmanAdapter mSalesmanAdapter;
    private String mCurSelectServiceProviderId;
    private String mCurSelectSalesmanId;
    private List<SalesmanBean.DataEntity.Salesman> mSearchSalesmanList;//搜索到的城市经理列表数据
    private List<ServiceProviderBean.DataEntity.ServiceProvider> mSearchServiceProviderList;//搜索到的市运营列表数据
    private ServiceProviderBean.DataEntity.ServiceProvider mAllServiceProvider;
    private SalesmanBean.DataEntity.Salesman mAllSalesman;
    private DropDownMenu mDropDownMenu;
    private ArrayList<TabBean> headers;
    private EditText mEtServiceInput;
    private ImageView mIvServiceClean;
    private View mServiceDataEmpty;
    private EditText mEtSalesmanInput;
    private ImageView mIvSalesmanClean;
    private View mSalesmanDataEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        setContentView(R.layout.activity_merchant_list);
        StatusBarUtil.setTranslucentStatus(this);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);

        mDropDownMenu = findViewById(R.id.dropDownMenu);
    }

    private void initTitle() {
        type = getIntent().getIntExtra(Constants.MERCHANT_CHECK_TYPE, 0);
        switch (type) {
            case Constants.MERCHANT_CHECK_FAILED:
                mTvTitle.setText(getString(R.string.merchant_check_failed));
                propValue = getString(R.string.merchant_check_failed);
                break;
            case Constants.MERCHANT_CHECKING:
                mTvTitle.setText(getString(R.string.merchant_checking));
                propValue = getString(R.string.merchant_checking);
                break;
            case Constants.MERCHANT_CHECK_SUCCESS:
                mTvTitle.setText(getString(R.string.merchant_check_success));
                propValue = getString(R.string.merchant_check_success);
                break;
            case Constants.MERCHANT_NO_INFO:
                propValue = getString(R.string.merchant_no_info);
                break;
            case Constants.MERCHANT_FREEZE:
                propValue = getString(R.string.merchant_freeze);
                break;
            case Constants.MERCHANT_TRADABLE:
                propValue = getString(R.string.merchant_trade_enable);
                break;
            case Constants.MERCHANT_CHANGEING:
                propValue = getString(R.string.merchant_change_checking);
                break;
            case Constants.MERCHANT_CHANGE_PASS:
                propValue = getString(R.string.merchant_change_check_success);
                break;
            case Constants.MERCHANT_CHANGE_FAIL:
                propValue = getString(R.string.merchant_change_check_failed);
                break;
            case Constants.MERCHANT_INVALID:
                propValue = getString(R.string.merchant_become_invalid);
                break;
            case Constants.MERCHANT_NOT_CONFIRM://待确认
                propValue = getString(R.string.merchant_not_confirm);
                break;
        }
        mTvTitle.setText(propValue);
    }

    private void initRecyclerView(View view) {
        mMerchantRecyclerview = (RecyclerView) view.findViewById(R.id.recyclerview);
        mCustomLinearLayoutManager = new CustomLinearLayoutManager(this);
        mMerchantRecyclerview.setLayoutManager(mCustomLinearLayoutManager);
        mMerchantRecyclerview.setItemAnimator(new DefaultItemAnimator());
        mMerchantAdapter = new MerchantAdapter(MerchantListActivity.this, null, Constants.INTENT_LIST_TO_MERCHANT, type, mListData);
        mMerchantRecyclerview.setAdapter(mMerchantAdapter);
    }

    private void initSwipeRefreshLayout(View view) {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setRefreshEnable(false);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    currentPage = 1;
                    isRefreshed = true;
                    getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mIvDataClean.setOnClickListener(this);
        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    mIvDataClean.setVisibility(View.VISIBLE);
                } else {
                    mIvDataClean.setVisibility(View.INVISIBLE);
                }
            }
        });
        mEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    mtaProperties(MerchantListActivity.this, "1004", "status", propValue);
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        isRefreshed = true;
                        currentPage = 1;
                        DialogUtil.safeShowDialog(mLoadDialog);
                        getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                        mSwipeRefreshLayout.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mSwipeRefreshLayout.finishRefresh();
                            }
                        }, 500);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void initData() {
        initTitle();
        initDropDownMenu();
    }

    private void initDropDownMenu() {
        mCurSelectServiceProviderId = "";
        mCurSelectSalesmanId = "";
        mSearchServiceProviderList = new ArrayList<>();
        mSearchSalesmanList = new ArrayList<>();

        mAllServiceProvider = new ServiceProviderBean.DataEntity.ServiceProvider();//"全部商户"
        mAllServiceProvider.setServiceProviderName("全部市运营");
        mAllServiceProvider.setServiceProviderId("");

        mAllSalesman = new SalesmanBean.DataEntity.Salesman();//"全部商户"
        mAllSalesman.setEmpName("全部城市经理");
        mAllSalesman.setEmpId("");

        View contentView = getLayoutInflater().inflate(R.layout.layout_content_merchant, null);

        initRecyclerView(contentView);
        initSwipeRefreshLayout(contentView);

        mIvDataClean = (ImageView) contentView.findViewById(R.id.iv_data_clean);
        mEtSearch = (EditText) contentView.findViewById(R.id.et_search);
        mLayoutEmpty = (RelativeLayout) contentView.findViewById(R.id.layout_data_empty);

        if (type == Constants.MERCHANT_NO_INFO)
            mEtSearch.setHint(R.string.et_hint_telephone_search);

        headers = new ArrayList<>();
        TabBean tabBean2 = new TabBean();
        tabBean2.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean2.setLabel("cityService");
        tabBean2.setTabTitle("全部市运营");
        TabBean tabBean3 = new TabBean();
        tabBean3.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean3.setLabel("cityManager");
        tabBean3.setTabTitle("全部城市经理");
        if ("SUPERADMIN".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_ROLE_CODE))) {
            if ("".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean2);
                headers.add(tabBean3);
            } else if ("SC".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean3);
            }
        }
        mDropDownMenu.setDropDownMenu(headers, initViewData(), contentView);
        mDropDownMenu.setOnTabClickListener(new DropDownMenu.OnTabClickListener() {
            @Override
            public void onTabClick(int position, boolean isOpen) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
                mSelectLabelPosition = position;
                switch (headers.get(position).getLabel()) {
                    case "cityService":
                        if (isOpen)
                            getServiceList(false);
                        break;
                    case "cityManager":
                        if (isOpen)
                            getSalesmanList(false);
                        break;
                }
            }
        });
    }

    /**
     * 设置类型和数据源：
     * DropDownMenu.KEY对应类型（DropDownMenu中的常量，参考上述核心源码）
     * DropDownMenu.VALUE对应数据源：key不是TYPE_CUSTOM则传递string[],key是TYPE_CUSTOM类型则传递对应view
     */
    private List<HashMap<String, Object>> initViewData() {
        List<HashMap<String, Object>> viewDatas = new ArrayList<>();
        HashMap<String, Object> map;
        for (int i = 0; i < headers.size(); i++) {
            map = new HashMap<String, Object>();
            map.put(DropDownMenu.KEY, headers.get(i).getType());
            map.put(DropDownMenu.VALUE, getCustomView(headers.get(i).getLabel()));
            viewDatas.add(map);
        }
        return viewDatas;
    }

    private View getCustomView(String label) {
        View view = getLayoutInflater().inflate(R.layout.layout_dropdown, null);
        switch (label) {
            case "cityService":
                mEtServiceInput = view.findViewById(R.id.et_input);
                mIvServiceClean = view.findViewById(R.id.iv_clean);
                mServiceDataEmpty = view.findViewById(R.id.common_data_empty);
                RecyclerView rvService = view.findViewById(R.id.recycler_view);
                LinearLayoutManager serviceLinearLayoutManager = new LinearLayoutManager(this);
                rvService.setLayoutManager(serviceLinearLayoutManager);
                mServiceProviderAdapter = new ServiceProviderAdapter(MerchantListActivity.this);
                mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                mServiceProviderAdapter.setOnItemClickListener(new ServiceProviderAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mCurSelectServiceProviderId != null && !mCurSelectServiceProviderId.equals(mSearchServiceProviderList.get(position).getServiceProviderId())) {
                            mCurSelectSalesmanId = "";
                            for (int i = 0; i < headers.size(); i++) {
                                if ("cityManager".equals(headers.get(i).getLabel()))
                                    mDropDownMenu.setTabText(i, "全部城市经理");
                            }
                            mCurSelectServiceProviderId = mSearchServiceProviderList.get(position).getServiceProviderId();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mDropDownMenu.setTabText(mSelectLabelPosition, mSearchServiceProviderList.get(position).getServiceProviderName());
                            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                                isRefreshed = true;
                                currentPage = 1;
                                mListData.clear();
                                DialogUtil.safeShowDialog(mLoadDialog);
                                getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
                            } else {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                            }
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvService.setAdapter(mServiceProviderAdapter);
                rvService.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    //这个int用来记录最后一个可见的view
                    int lastVisibleItemPosition;
                    int refreshCurrentPage;//防止重复加载

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mServiceProviderAdapter.getItemCount()) {
                            refreshCurrentPage = mSearchCurrentPage;
                            if (!isLoadOver)
                                getServiceList(true);
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                        lastVisibleItemPosition = serviceLinearLayoutManager.findLastVisibleItemPosition();
                    }
                });
                mEtServiceInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            getServiceList(false);
                            return true;
                        }
                        return false;
                    }
                });
                mEtServiceInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            getServiceList(false);
                        }
                        mIvServiceClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                mIvServiceClean.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEtServiceInput.setText("");
                    }
                });
                break;
            case "cityManager":
                mEtSalesmanInput = view.findViewById(R.id.et_input);
                mIvSalesmanClean = view.findViewById(R.id.iv_clean);
                mSalesmanDataEmpty = view.findViewById(R.id.common_data_empty);
                RecyclerView rvSalesman = view.findViewById(R.id.recycler_view);
                LinearLayoutManager salesmanLinearLayoutManager = new LinearLayoutManager(this);
                rvSalesman.setLayoutManager(salesmanLinearLayoutManager);
                mSalesmanAdapter = new SalesmanAdapter(MerchantListActivity.this);
                mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                mSalesmanAdapter.setOnItemClickListener(new SalesmanAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mCurSelectSalesmanId != null && !mCurSelectSalesmanId.equals(mSearchSalesmanList.get(position).getEmpId())) {
                            mCurSelectSalesmanId = mSearchSalesmanList.get(position).getEmpId();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mDropDownMenu.setTabText(mSelectLabelPosition, mSearchSalesmanList.get(position).getEmpName());
                            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                                isRefreshed = true;
                                currentPage = 1;
                                mListData.clear();
                                DialogUtil.safeShowDialog(mLoadDialog);
                                getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
                            } else {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                            }
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvSalesman.setAdapter(mSalesmanAdapter);
                rvSalesman.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    //这个int用来记录最后一个可见的view
                    int lastVisibleItemPosition;
                    int refreshCurrentPage;//防止重复加载

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mSalesmanAdapter.getItemCount()) {
                            refreshCurrentPage = mSearchCurrentPage;
                            if (!isLoadOver)
                                getSalesmanList(true);
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                        lastVisibleItemPosition = salesmanLinearLayoutManager.findLastVisibleItemPosition();
                    }
                });
                mEtSalesmanInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            getSalesmanList(false);
                            return true;
                        }
                        return false;
                    }
                });
                mEtSalesmanInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            getSalesmanList(false);
                        }
                        mIvSalesmanClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                mIvSalesmanClean.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEtSalesmanInput.setText("");
                    }
                });
                break;
        }
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_data_clean:
                mEtSearch.setText("");
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    currentPage = 1;
                    isRefreshed = true;
                    getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
                break;
        }
    }

    private void getData(int pageSize, int currentPage, String search) {
        int checkType = getIntent().getIntExtra(Constants.MERCHANT_CHECK_TYPE, 0);
        switch (checkType) {
            case Constants.MERCHANT_CHECK_FAILED:
                getMerChantData(pageSize, currentPage, 2, search);
                break;
            case Constants.MERCHANT_CHECKING:
                getMerChantData(pageSize, currentPage, 0, search);
                break;
            case Constants.MERCHANT_CHECK_SUCCESS:
                getMerChantData(pageSize, currentPage, 1, search);
                break;
            case Constants.MERCHANT_NO_INFO:
                getNoInfoData(pageSize, currentPage, search);
                //getMerChantData(pageSize,currentPage,-1,search);
                break;
            case Constants.MERCHANT_FREEZE:
                getMerChantData(pageSize, currentPage, 101, search);
                break;
            case Constants.MERCHANT_TRADABLE:
                getMerChantData(pageSize, currentPage, 100, search);
                break;
            case Constants.MERCHANT_CHANGEING:
                getMerChantData(pageSize, currentPage, 3, search);
                break;
            case Constants.MERCHANT_CHANGE_PASS:
                getMerChantData(pageSize, currentPage, 10, search);
                break;
            case Constants.MERCHANT_CHANGE_FAIL:
                getMerChantData(pageSize, currentPage, 4, search);
                break;
            case Constants.MERCHANT_INVALID:
                getMerChantData(pageSize, currentPage, 5, search);
                break;
            case Constants.MERCHANT_NOT_CONFIRM://待确认
                getMerChantData(pageSize, currentPage, 6, search);
                break;
        }
    }

    private void getMerChantData(int pageSize, int currentPage, int activateStatus, String search) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            //if (examineStatus!=-1) {
            //map.put("examineStatus", examineStatus);
            //  }
            // if (activateStatus!=-1) {
            map.put("mchQueryStatus", activateStatus);
            // }
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mCurSelectSalesmanId))
                map.put("salesmanId", mCurSelectSalesmanId);
            if (!TextUtils.isEmpty(search)) {
                map.put("search", search);
            }
            ServerClient.newInstance(MyApplication.getContext()).getMerchantList(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_LIST, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getNoInfoData(int pageSize, int currentPage, String search) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSize);
            map.put("currentPage", currentPage);
            if (!TextUtils.isEmpty(search)) {
                map.put("search", search);
            }
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mCurSelectSalesmanId))
                map.put("salesmanId", mCurSelectSalesmanId);
            ServerClient.newInstance(MyApplication.getContext()).getMerchantNoInfo(MyApplication.getContext(), Constants.TAG_MERCHANT_NO_INFO, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getServiceList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                map.put("serviceProviderName", mEtServiceInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getServiceList(MyApplication.getContext(), "TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getSalesmanList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                map.put("empName", mEtSalesmanInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getSalesmanList(MyApplication.getContext(), "TAG_SALESMAN_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MERCHANT_LIST) || event.getTag().equals(Constants.TAG_GET_MERCHANT_LIST_REFRESH)) {
            if (event.getTag().equals(Constants.TAG_GET_MERCHANT_LIST_REFRESH)) {
                //是推送刷新
                isRefreshed = true;
            }
            DialogUtil.safeCloseDialog(mLoadDialog);
            MerchantListBean msg = (MerchantListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_MERCHANT_LIST_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_MERCHANT_LIST_TRUE://验证码返回成功
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mLayoutEmpty.setVisibility(View.GONE);
                        mMerchantRecyclerview.setVisibility(View.VISIBLE);
                        mListData.addAll(msg.getData().getData());
                        mMerchantAdapter.notifyDataSetChanged();
                    } else {
                        if (isLoadmore) {
                            mLayoutEmpty.setVisibility(View.GONE);
                            mMerchantRecyclerview.setVisibility(View.VISIBLE);
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mLayoutEmpty.setVisibility(View.VISIBLE);
                            mMerchantRecyclerview.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        } else if (event.getTag().equals(Constants.TAG_MERCHANT_NO_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            MerchantListBean msg = (MerchantListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.MERCHANT_NO_INFO_FALSE:
                    setErrorState(isRefreshed, isLoadmore, 500);
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.MERCHANT_NO_INFO_TRUE://验证码返回成功
                    setSuccessState(isRefreshed, isLoadmore, 500);
                    if (msg != null && msg.getData() != null) {
                        if (msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                            mLayoutEmpty.setVisibility(View.GONE);
                            mMerchantRecyclerview.setVisibility(View.VISIBLE);
                            mListData.addAll(msg.getData().getData());
                            mMerchantAdapter.notifyDataSetChanged();
                        } else {
                            if (isLoadmore) {
                                mLayoutEmpty.setVisibility(View.GONE);
                                mMerchantRecyclerview.setVisibility(View.VISIBLE);
                                ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                            } else {
                                mLayoutEmpty.setVisibility(View.VISIBLE);
                                mMerchantRecyclerview.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        if (isLoadmore) {
                            mLayoutEmpty.setVisibility(View.GONE);
                            mMerchantRecyclerview.setVisibility(View.VISIBLE);
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mLayoutEmpty.setVisibility(View.VISIBLE);
                            mMerchantRecyclerview.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
            isLoadmore = false;
            isRefreshed = false;
        } else if (event.getTag().equals("TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ServiceProviderBean msg = (ServiceProviderBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mServiceDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchServiceProviderList.clear();
                            if (TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                                mSearchServiceProviderList.add(mAllServiceProvider);
                            }
                        }

                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchServiceProviderList.addAll(msg.getData().getData());
                        mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchServiceProviderList.clear();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mServiceDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mServiceDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_SALESMAN_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SalesmanBean msg = (SalesmanBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mSalesmanDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchSalesmanList.clear();
                            if (TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                                mSearchSalesmanList.add(mAllSalesman);
                            }
                        }
                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchSalesmanList.addAll(msg.getData().getData());
                        mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchSalesmanList.clear();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mSalesmanDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mSalesmanDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        }
    }

    private void setErrorState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    private void setSuccessState(boolean isRefreshed, boolean isLoadmore, long delay) {
        if (isRefreshed) {
            mListData.clear();
            currentPage = 2;
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    currentPage++;
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            isRefreshed = true;
            currentPage = 1;
            mListData.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_LIST_TO_MERCHANT) {
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                isRefreshed = true;
                currentPage = 1;
                DialogUtil.safeShowDialog(mLoadDialog);
                getData(pageSize, currentPage, mEtSearch.getText().toString().trim());
            } else {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    public void mtaProperties(Activity activity, String number, String key, String value) {
        Properties prop = new Properties();
        prop.setProperty(key, value);
        StatService.trackCustomKVEvent(activity, number, prop);
    }
}
