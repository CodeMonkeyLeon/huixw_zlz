package com.hstypay.hstysales.activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.adapter.MerchantRecyclerAdapter;
import com.hstypay.hstysales.adapter.ProfitListAdapter;
import com.hstypay.hstysales.adapter.ProfitStatisticsAdapter;
import com.hstypay.hstysales.adapter.SalesmanAdapter;
import com.hstypay.hstysales.adapter.ServiceProviderAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.MerchantListBean;
import com.hstypay.hstysales.bean.ProfitsummaryBean;
import com.hstypay.hstysales.bean.ProfitListBean;
import com.hstypay.hstysales.bean.SalesmanBean;
import com.hstypay.hstysales.bean.ServiceProviderBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.TimePickViewHelp;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.EditTextDelete;
import com.hstypay.hstysales.widget.LinearSpaceItemDecoration;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;
import com.ykk.dropdownmenu.DropDownMenu;
import com.ykk.dropdownmenu.TabBean;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Desc: 分润统计页面
 *
 * @author kuangzeyu
 * @date 2020/10/23
 **/
public class ProfitStatisticsActivity extends BaseActivity implements View.OnClickListener {
    private TextView mTvTitle;
    private ImageView mIvBack;
    private SHSwipeRefreshLayout mSwipeRefreshLayoutProfit;
    private RecyclerView mRvProfitListStatistics;
    private ProfitStatisticsAdapter mProfitStatisticsAdapter;//分润统计数据的adapter
    private SafeDialog mLoadDialog;//统计金额那一部分的loading、获取商户列表的loading
    private SafeDialog mLoadDialogList;//列表金额那一部分的loading
    private MerchantRecyclerAdapter mMerchantRecyclerAdapter;//商户列表的adapter
    private List<MerchantListBean.DataEntity.ItemData> mMerchantList;//商户列表数据
    private String timeFormatType = "yyyy-MM-dd HH:mm:ss";

    private Calendar mCurSelectStartDate;//当前pickview选中的开始日期
    private Calendar mCurSelectEndDate;//当前pickview选中的结束日期
    private String mCurSelectMerchantId;//当前选中的商户ID

    public static final int mPageSize = 15;
    protected int mCurrentPage = 1;
    private boolean isLoadmore;//加载更多标识
    private boolean isPullRefresh;//下拉刷新标识
    private List<ProfitListBean.SummaryData> mSummaryDataList = new ArrayList<>();//分润列表数据
    private MerchantListBean.DataEntity.ItemData mAllMerchant;//"全部商户"
    private TextView mTvProfitStatisticDateTip;
    private TimePickViewHelp mTimePickViewHelp;
    private SalesmanAdapter mSalesmanAdapter;

    private ServiceProviderAdapter mServiceProviderAdapter;
    private ServiceProviderBean.DataEntity.ServiceProvider mAllServiceProvider;
    private SalesmanBean.DataEntity.Salesman mAllSalesman;
    private boolean isLoadOver;
    private String mCurSelectServiceProviderId;
    private String mCurSelectSalesmanId;
    private List<MerchantListBean.DataEntity.ItemData> mSearchMerchantList;//搜索到的商户列表数据
    private List<SalesmanBean.DataEntity.Salesman> mSearchSalesmanList;//搜索到的城市经理列表数据
    private List<ServiceProviderBean.DataEntity.ServiceProvider> mSearchServiceProviderList;//搜索到的市运营列表数据
    private EditText mEtServiceInput;
    private ImageView mIvServiceClean;
    private View mServiceDataEmpty;
    private EditText mEtSalesmanInput;
    private ImageView mIvSalesmanClean;
    private View mSalesmanDataEmpty;
    private EditText mEtMerchantInput;
    private ImageView mIvMerchantClean;
    private View mMerchantDataEmpty;
    private LinearLayout mLlDate;
    private TextView mTvDate,mTvCityService, mTvSalesman, mTvMerchant;;
    private ImageView mIvDate;
    private DropDownMenu mDropDownMenu;
    private ArrayList<TabBean> headers;
    private int mSearchPageSize = 15;
    private int mSearchCurrentPage = 1;
    private boolean isLoadMerchantMore;//加载更多标识

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profit_statistics);
        StatusBarUtil.setTranslucentStatusWhiteIcon(this);
        initView();
        initData();
        initEvent();
    }

    private void initView() {
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle.setText(UIUtils.getString(R.string.title_profit_statistics));
        mDropDownMenu = findViewById(R.id.dropDownMenu);
    }

    private void initData() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mLoadDialogList = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);


        initDropDownMenu();
        getSummaryStatisticData();
        getListStatisticData();
    }

    private void initEvent() {
        mIvBack.setOnClickListener(this);
        mLlDate.setOnClickListener(this);

        mSwipeRefreshLayoutProfit.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    mCurrentPage = 1;
                    isPullRefresh = true;
                    getSummaryStatisticData();
                    getListStatisticData();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayoutProfit.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayoutProfit.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmore = true;
                    //mCurrentPage++;//要在获取数据成功后才能++，否则失败了又得--回去
                    getListStatisticData();
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayoutProfit.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayoutProfit.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutProfit.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutProfit.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayoutProfit.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutProfit.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutProfit.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayoutProfit.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });

    }

    private void initDropDownMenu() {
        mCurSelectServiceProviderId = "";
        mCurSelectSalesmanId = "";
        mCurSelectMerchantId = "";
        mSearchServiceProviderList = new ArrayList<>();
        mSearchSalesmanList = new ArrayList<>();
        mSearchMerchantList = new ArrayList<>();

        mAllServiceProvider = new ServiceProviderBean.DataEntity.ServiceProvider();
        mAllServiceProvider.setServiceProviderName("全部市运营");
        mAllServiceProvider.setServiceProviderId("");

        mAllSalesman = new SalesmanBean.DataEntity.Salesman();
        mAllSalesman.setEmpName("全部城市经理");
        mAllSalesman.setEmpId("");

        mAllMerchant = new MerchantListBean.DataEntity.ItemData();
        mAllMerchant.setMerchantName("全部商户");
        mAllMerchant.setMerchantId("");

        View contentView = getLayoutInflater().inflate(R.layout.layout_content_profit_statistics, null);
        mLlDate = contentView.findViewById(R.id.ll_date);
        mTvDate = contentView.findViewById(R.id.tv_date);
        mIvDate = contentView.findViewById(R.id.iv_date);
        mTvCityService = contentView.findViewById(R.id.tv_city_service);
        mTvSalesman = contentView.findViewById(R.id.tv_salesman);
        mTvMerchant = contentView.findViewById(R.id.tv_merchant);
        mSwipeRefreshLayoutProfit = contentView.findViewById(R.id.swipeRefreshLayout_profit);
        mSwipeRefreshLayoutProfit.setRefreshEnable(false);
        mSwipeRefreshLayoutProfit.setLoadmoreEnable(false);

        mLlDate = contentView.findViewById(R.id.ll_date);
        mTvProfitStatisticDateTip = contentView.findViewById(R.id.tv_profit_statistic_date_tip);

        mSwipeRefreshLayoutProfit = contentView.findViewById(R.id.swipeRefreshLayout_profit);
        mRvProfitListStatistics = contentView.findViewById(R.id.rv_profit_list_statistics);
        mSwipeRefreshLayoutProfit.setHeaderGuidanceViewTextColor(getResources().getColor(R.color.white));
        mRvProfitListStatistics.setLayoutManager(new LinearLayoutManager(this));
        mRvProfitListStatistics.addItemDecoration(new LinearSpaceItemDecoration(DensityUtils.dip2px(this, 15), false));
        mProfitStatisticsAdapter = new ProfitStatisticsAdapter(this);
        mRvProfitListStatistics.setAdapter(mProfitStatisticsAdapter);

        mCurSelectStartDate = Calendar.getInstance();
        mCurSelectEndDate = Calendar.getInstance();

        mCurSelectStartDate.setTime(new Date());
        //int day_of_month = mCurSelectStartDate.get(Calendar.DAY_OF_MONTH);
        mCurSelectStartDate.set(Calendar.DAY_OF_MONTH, 1);//本月的1号作为选择的开始时间
        mCurSelectEndDate.setTime(new Date());//当前时间作为选择的结束时间
        mCurSelectEndDate.add(Calendar.DAY_OF_MONTH, -2);//不含今日和昨日
        if (mCurSelectStartDate.compareTo(mCurSelectEndDate) > 0) {
            mCurSelectStartDate.set(Calendar.YEAR, mCurSelectEndDate.get(Calendar.YEAR));
            mCurSelectStartDate.set(Calendar.MONTH, mCurSelectEndDate.get(Calendar.MONTH));
            mCurSelectStartDate.set(Calendar.DAY_OF_MONTH, 1);
        }

        Date profitStatisticDateTipDate = mCurSelectEndDate.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String tipDate = simpleDateFormat.format(profitStatisticDateTipDate);
        mTvProfitStatisticDateTip.setText(getResources().getString(R.string.profit_statistic_date_tip) + tipDate);
        initPickTimeView();

        setTitleDateView();

        headers = new ArrayList<>();
        TabBean tabBean2 = new TabBean();
        tabBean2.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean2.setLabel("cityService");
        tabBean2.setTabTitle("市运营");
        TabBean tabBean3 = new TabBean();
        tabBean3.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean3.setLabel("cityManager");
        tabBean3.setTabTitle("城市经理");
        TabBean tabBean4 = new TabBean();
        tabBean4.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean4.setLabel("merchant");
        tabBean4.setTabTitle("商户");
        if ("SUPERADMIN".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_ROLE_CODE))) {
            if ("".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean2);
                headers.add(tabBean3);
                mTvCityService.setVisibility(View.VISIBLE);
                mTvSalesman.setVisibility(View.VISIBLE);
            } else if ("SC".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                headers.add(tabBean3);
                mTvSalesman.setVisibility(View.VISIBLE);
            }
        }
        headers.add(tabBean4);
        mDropDownMenu.setDropDownMenu(headers, initViewData(), contentView);
        mDropDownMenu.setOnTabClickListener(new DropDownMenu.OnTabClickListener() {
            @Override
            public void onTabClick(int position, boolean isOpen) {
                switch (headers.get(position).getLabel()) {
                    case "cityService":
                        if (isOpen)
                            getServiceList(false);
                        break;
                    case "cityManager":
                        if (isOpen)
                            getSalesmanList(false);
                        break;
                    case "merchant":
                        if (isOpen)
                            getMerchants(false);
                        break;
                }
            }
        });
    }

    /**
     * 设置类型和数据源：
     * DropDownMenu.KEY对应类型（DropDownMenu中的常量，参考上述核心源码）
     * DropDownMenu.VALUE对应数据源：key不是TYPE_CUSTOM则传递string[],key是TYPE_CUSTOM类型则传递对应view
     */
    private List<HashMap<String, Object>> initViewData() {
        List<HashMap<String, Object>> viewDatas = new ArrayList<>();
        HashMap<String, Object> map;
        for (int i = 0; i < headers.size(); i++) {
            map = new HashMap<String, Object>();
            map.put(DropDownMenu.KEY, headers.get(i).getType());
            map.put(DropDownMenu.VALUE, getCustomView(headers.get(i).getLabel()));
            viewDatas.add(map);
        }
        return viewDatas;
    }

    private View getCustomView(String label) {
        View view;
        if ("date".equals(label)) {
            view = null;
        } else {
            view = getLayoutInflater().inflate(R.layout.layout_dropdown, null);
            switch (label) {
                case "cityService":
                    mEtServiceInput = view.findViewById(R.id.et_input);
                    mIvServiceClean = view.findViewById(R.id.iv_clean);
                    mServiceDataEmpty = view.findViewById(R.id.common_data_empty);
                    RecyclerView rvService = view.findViewById(R.id.recycler_view);
                    LinearLayoutManager serviceLinearLayoutManager = new LinearLayoutManager(this);
                    rvService.setLayoutManager(serviceLinearLayoutManager);
                    mServiceProviderAdapter = new ServiceProviderAdapter(ProfitStatisticsActivity.this);
                    mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                    mServiceProviderAdapter.setOnItemClickListener(new ServiceProviderAdapter.OnRecyclerViewItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            if (mCurSelectServiceProviderId != null && !mCurSelectServiceProviderId.equals(mSearchServiceProviderList.get(position).getServiceProviderId())) {
                                mCurSelectSalesmanId = "";
                                mCurSelectMerchantId = "";
                                for (int i = 0; i < headers.size(); i++) {
                                    if ("cityManager".equals(headers.get(i).getLabel())) {
                                        mTvSalesman.setText("全部城市经理");
                                    }
                                    if ("merchant".equals(headers.get(i).getLabel())) {
                                        mTvMerchant.setText("全部商户");
                                    }
                                }
                                mCurSelectServiceProviderId = mSearchServiceProviderList.get(position).getServiceProviderId();
                                mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
//                                mDropDownMenu.setTabText(mSelectLabelPosition, mSearchServiceProviderList.get(position).getServiceProviderName());
                                mTvCityService.setText(mSearchServiceProviderList.get(position).getServiceProviderName());
                                getSummaryStatisticData();
                                mCurrentPage = 1;
                                getListStatisticData();
                            }
                            mDropDownMenu.closeMenu();
                        }
                    });
                    rvService.setAdapter(mServiceProviderAdapter);
                    rvService.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        //这个int用来记录最后一个可见的view
                        int lastVisibleItemPosition;
                        int refreshCurrentPage;//防止重复加载

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mServiceProviderAdapter.getItemCount()) {
                                refreshCurrentPage = mSearchCurrentPage;
                                if (!isLoadOver)
                                    getServiceList(true);
                            }
                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                            lastVisibleItemPosition = serviceLinearLayoutManager.findLastVisibleItemPosition();
                        }
                    });
                    mEtServiceInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                getServiceList(false);
                                return true;
                            }
                            return false;
                        }
                    });
                    mEtServiceInput.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() == 0) {
                                getServiceList(false);
                            }
                            mIvServiceClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    mIvServiceClean.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mEtServiceInput.setText("");
                        }
                    });
                    break;
                case "cityManager":
                    mEtSalesmanInput = view.findViewById(R.id.et_input);
                    mIvSalesmanClean = view.findViewById(R.id.iv_clean);
                    mSalesmanDataEmpty = view.findViewById(R.id.common_data_empty);
                    RecyclerView rvSalesman = view.findViewById(R.id.recycler_view);
                    LinearLayoutManager salesmanLinearLayoutManager = new LinearLayoutManager(this);
                    rvSalesman.setLayoutManager(salesmanLinearLayoutManager);
                    mSalesmanAdapter = new SalesmanAdapter(ProfitStatisticsActivity.this);
                    mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                    mSalesmanAdapter.setOnItemClickListener(new SalesmanAdapter.OnRecyclerViewItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            if (mCurSelectSalesmanId != null && !mCurSelectSalesmanId.equals(mSearchSalesmanList.get(position).getEmpId())) {
                                mCurSelectMerchantId = "";
                                for (int i = 0; i < headers.size(); i++) {
                                    if ("merchant".equals(headers.get(i).getLabel()))
                                        mTvMerchant.setText("全部商户");
                                }
                                mCurSelectSalesmanId = mSearchSalesmanList.get(position).getEmpId();
                                mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
//                                mDropDownMenu.setTabText(mSelectLabelPosition, mSearchSalesmanList.get(position).getEmpName());
                                mTvSalesman.setText(mSearchSalesmanList.get(position).getEmpName());
                                getSummaryStatisticData();
                                mCurrentPage = 1;
                                getListStatisticData();
                            }
                            mDropDownMenu.closeMenu();
                        }
                    });
                    rvSalesman.setAdapter(mSalesmanAdapter);
                    rvSalesman.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        //这个int用来记录最后一个可见的view
                        int lastVisibleItemPosition;
                        int refreshCurrentPage;//防止重复加载

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mSalesmanAdapter.getItemCount()) {
                                LogUtil.d("refreshCurrentPage="+refreshCurrentPage+"---mSearchCurrentPage="+mSearchCurrentPage);
                                refreshCurrentPage = mSearchCurrentPage;
                                if (!isLoadOver)
                                    getSalesmanList(true);
                            }
                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                            lastVisibleItemPosition = salesmanLinearLayoutManager.findLastVisibleItemPosition();
                        }
                    });
                    mEtSalesmanInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                getSalesmanList(false);
                                return true;
                            }
                            return false;
                        }
                    });
                    mEtSalesmanInput.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() == 0) {
                                getSalesmanList(false);
                            }
                            mIvSalesmanClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    mIvSalesmanClean.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mEtSalesmanInput.setText("");
                        }
                    });
                    break;
                case "merchant":
                    mEtMerchantInput = view.findViewById(R.id.et_input);
                    mIvMerchantClean = view.findViewById(R.id.iv_clean);
                    mMerchantDataEmpty = view.findViewById(R.id.common_data_empty);
                    RecyclerView rvMerchant = view.findViewById(R.id.recycler_view);
                    LinearLayoutManager merchantLinearLayoutManager = new LinearLayoutManager(this);
                    rvMerchant.setLayoutManager(merchantLinearLayoutManager);
                    mMerchantRecyclerAdapter = new MerchantRecyclerAdapter(ProfitStatisticsActivity.this);
                    mMerchantRecyclerAdapter.setData(mSearchMerchantList, mCurSelectMerchantId);
                    mMerchantRecyclerAdapter.setOnItemClickListener(new MerchantRecyclerAdapter.OnRecyclerViewItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            if (mCurSelectMerchantId != null && !mCurSelectMerchantId.equals(mSearchMerchantList.get(position).getMerchantId())) {
                                mCurSelectMerchantId = mSearchMerchantList.get(position).getMerchantId();
                                mMerchantRecyclerAdapter.setData(mSearchMerchantList, mCurSelectMerchantId);
//                                mDropDownMenu.setTabText(mSelectLabelPosition, mSearchMerchantList.get(position).getMerchantName());
                                mTvMerchant.setText(mSearchMerchantList.get(position).getMerchantName());
                                getSummaryStatisticData();
                                mCurrentPage = 1;
                                getListStatisticData();
                            }
                            mDropDownMenu.closeMenu();
                        }
                    });
                    rvMerchant.setAdapter(mMerchantRecyclerAdapter);
                    rvMerchant.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        //这个int用来记录最后一个可见的view
                        int lastVisibleItemPosition;
                        int refreshCurrentPage;//防止重复加载

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mMerchantRecyclerAdapter.getItemCount()) {
                                refreshCurrentPage = mSearchCurrentPage;
                                if (!isLoadOver)
                                    getMerchants(true);
                            }
                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                            //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                            lastVisibleItemPosition = merchantLinearLayoutManager.findLastVisibleItemPosition();
                        }
                    });
                    mEtMerchantInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                getMerchants(false);
                                return true;
                            }
                            return false;
                        }
                    });
                    mEtMerchantInput.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (s.length() == 0) {
                                getMerchants(false);
                            }
                            mIvMerchantClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    mIvMerchantClean.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mEtMerchantInput.setText("");
                        }
                    });
                    break;
            }
        }
        return view;
    }

    /**
     * 获取分润统计列表数据
     */
    private void getListStatisticData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!(isPullRefresh || isLoadmore)) {
                DialogUtil.safeShowDialog(mLoadDialogList);
            }

            Map<String, Object> map = new HashMap<>();
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mCurSelectSalesmanId))
                map.put("salesmanId", mCurSelectSalesmanId);
            if (!TextUtils.isEmpty(mCurSelectMerchantId))
                map.put("merchantId", mCurSelectMerchantId);
            mCurSelectStartDate.set(Calendar.HOUR_OF_DAY, 0);//获取数据的开始日期要从0时0分0秒算起
            mCurSelectStartDate.set(Calendar.MINUTE, 0);
            mCurSelectStartDate.set(Calendar.SECOND, 0);
            mCurSelectEndDate.set(Calendar.HOUR_OF_DAY, 23);
            mCurSelectEndDate.set(Calendar.MINUTE, 59);
            mCurSelectEndDate.set(Calendar.SECOND, 59);
            map.put("startTime", mTimePickViewHelp.getDateString(mCurSelectStartDate, timeFormatType));
            map.put("endTime", mTimePickViewHelp.getDateString(mCurSelectEndDate, timeFormatType));
            map.put("realTimeFlag", "0");//0表示非实时查询 1表示实时查询
            map.put("pageSize", mPageSize);
            map.put("currentPage", mCurrentPage);
            ServerClient.newInstance(MyApplication.getContext()).getListStatisticData(MyApplication.getContext(), Constants.TAG_GET_PROFIT_LIST_STATISTICS, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetListStatisticData(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_PROFIT_LIST_STATISTICS)) {
            DialogUtil.safeCloseDialog(mLoadDialogList);
            setSwipeRefreshState(isPullRefresh, isLoadmore, 500);
            ProfitListBean msg = (ProfitListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ProfitStatisticsActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    /*ProfitListBean.Data data = msg.getData();
                    if (data!=null){
                        List<ProfitListBean.SummaryData> summaryDataList = data.getData();
                        mProfitStatisticsAdapter.setProfitListData(summaryDataList);
                    }*/
                    if (msg != null && msg.getData() != null) {
                        if (msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                            if (mCurrentPage == 1) {
                                mSummaryDataList.clear();
                            }
                            mSummaryDataList.addAll(msg.getData().getData());
                            mProfitStatisticsAdapter.setProfitListData(mSummaryDataList);
                            mCurrentPage++;
                        } else {
                            if (isLoadmore) {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                            } else {
                                mSummaryDataList.clear();//使后面会展示空视图
                                mProfitStatisticsAdapter.setProfitListData(mSummaryDataList);
                            }
                        }
                    } else {
                        if (isLoadmore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mSummaryDataList.clear();//使后面会展示空视图
                            mProfitStatisticsAdapter.setProfitListData(mSummaryDataList);
                        }
                    }
                    //列表之上有概览数据要显示，就不要显示空视图了
                    /*if (mSummaryDataList !=null && mSummaryDataList.size()>0){
                        mTvNoDataView.setVisibility(View.GONE);
                        mSwipeRefreshLayoutProfit.setVisibility(View.VISIBLE);
                    }else{
                        mTvNoDataView.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayoutProfit.setVisibility(View.GONE);
                    }*/
                    break;
            }
            isLoadmore = false;
            isPullRefresh = false;
        }
    }

    /**
     * 关闭下拉刷新和上拉加载的进度条
     */
    private void setSwipeRefreshState(boolean isPullRefresh, boolean isLoadmore, int delay) {
        if (isPullRefresh) {
            mSwipeRefreshLayoutProfit.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayoutProfit.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayoutProfit.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayoutProfit.finishLoadmore();
                }
            }, delay);
        }
    }

    /**
     * 获取分润统计概览数据
     */
    private void getSummaryStatisticData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!(isPullRefresh)) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mCurSelectSalesmanId))
                map.put("salesmanId", mCurSelectSalesmanId);
            if (!TextUtils.isEmpty(mCurSelectMerchantId))
                map.put("merchantId", mCurSelectMerchantId);
            mCurSelectStartDate.set(Calendar.HOUR_OF_DAY, 0);
            mCurSelectStartDate.set(Calendar.MINUTE, 0);
            mCurSelectStartDate.set(Calendar.SECOND, 0);
            mCurSelectEndDate.set(Calendar.HOUR_OF_DAY, 23);
            mCurSelectEndDate.set(Calendar.MINUTE, 59);
            mCurSelectEndDate.set(Calendar.SECOND, 59);
            map.put("startTime", mTimePickViewHelp.getDateString(mCurSelectStartDate, timeFormatType));
            map.put("endTime", mTimePickViewHelp.getDateString(mCurSelectEndDate, timeFormatType));
            map.put("realTimeFlag", "0");//0表示非实时查询 1表示实时查询
            ServerClient.newInstance(MyApplication.getContext()).getSummaryStatistic(MyApplication.getContext(), Constants.TAG_GET_PROFIT_SUMMARY_STATISTICS, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetSummaryStatistic(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_PROFIT_SUMMARY_STATISTICS)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ProfitsummaryBean msg = (ProfitsummaryBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ProfitStatisticsActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    ProfitsummaryBean.ProfitSummaryData profitSummaryData = msg.getData();
                    if (profitSummaryData != null) {
                        mProfitStatisticsAdapter.setProfitSummaryData(profitSummaryData);
                    }
                    break;
            }
        }

    }

    /**
     * 设置头部选择的日期值
     */
    private void setTitleDateView() {
        mTvDate.setText(mCurSelectStartDate.get(Calendar.YEAR) + "."+mTimePickViewHelp.getDateString(mCurSelectStartDate, "MM.dd") + "～" + mTimePickViewHelp.getDateString(mCurSelectEndDate, "MM.dd"));
        mTvProfitStatisticDateTip.setText(getText(R.string.profit_statistic_date_tip)+mTimePickViewHelp.getDateString(mCurSelectEndDate, "MM.dd"));
//        mTvMonthDayStatistics.setText(mTimePickViewHelp.getDateString(mCurSelectStartDate, "MM.dd") + "～" + mTimePickViewHelp.getDateString(mCurSelectEndDate, "MM.dd"));
    }


    private void initPickTimeView() {
        //起始时间
        Calendar startDate = Calendar.getInstance();
        startDate.setTime(mCurSelectEndDate.getTime());
        startDate.add(Calendar.MONTH, -12);//仅可筛选近1年内的日期
        //结束时间
        Calendar endDate = Calendar.getInstance();
        endDate.setTime(mCurSelectEndDate.getTime());

        mTimePickViewHelp = new TimePickViewHelp(this, startDate, endDate, mCurSelectStartDate, mCurSelectEndDate);

    }

    private void getMerchants(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mEtMerchantInput.getText().toString().trim())) {
                map.put("search", mEtMerchantInput.getText().toString().trim());
            }
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mCurSelectSalesmanId))
                map.put("salesmanId", mCurSelectSalesmanId);
            map.put("mchQueryStatus", "100");
            ServerClient.newInstance(MyApplication.getContext()).getMerchantList2(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_LIST_MX_SEARCH + "_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getServiceList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                map.put("serviceProviderName", mEtServiceInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getServiceList(MyApplication.getContext(), "TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getSalesmanList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                map.put("empName", mEtSalesmanInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getSalesmanList(MyApplication.getContext(), "TAG_SALESMAN_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MERCHANT_LIST_MX_SEARCH + "_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            MerchantListBean msg = (MerchantListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_MERCHANT_LIST_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ProfitStatisticsActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_MERCHANT_LIST_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mMerchantDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchMerchantList.clear();
                            if (TextUtils.isEmpty(mEtMerchantInput.getText().toString().trim())) {
                                mSearchMerchantList.add(mAllMerchant);
                            }
                        }

                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchMerchantList.addAll(msg.getData().getData());
                        mMerchantRecyclerAdapter.setData(mSearchMerchantList, mCurSelectMerchantId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchMerchantList.clear();
                            mMerchantRecyclerAdapter.setData(mSearchMerchantList, mCurSelectMerchantId);
                            mMerchantDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mMerchantDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        }else if (event.getTag().equals("TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ServiceProviderBean msg = (ServiceProviderBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ProfitStatisticsActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mServiceDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchServiceProviderList.clear();
                            if (TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                                mSearchServiceProviderList.add(mAllServiceProvider);
                            }
                        }

                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchServiceProviderList.addAll(msg.getData().getData());
                        mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchServiceProviderList.clear();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mServiceDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mServiceDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_SALESMAN_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SalesmanBean msg = (SalesmanBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ProfitStatisticsActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mSalesmanDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchSalesmanList.clear();
                            if (TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                                mSearchSalesmanList.add(mAllSalesman);
                            }
                        }
                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchSalesmanList.addAll(msg.getData().getData());
                        mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchSalesmanList.clear();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mSalesmanDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mSalesmanDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_date:
                //日期选择
                openArrow(mIvDate);
                mTimePickViewHelp.show(new TimePickViewHelp.CallBackDataListener() {
                    @Override
                    public void onCancel() {
                        closeArrow(mIvDate);
                    }

                    @Override
                    public void onSubmit(Calendar curSelectStartDate, Calendar curSelectEndDate) {
                        int selectStartYear = curSelectStartDate.get(Calendar.YEAR);
                        int selectEndYear = curSelectEndDate.get(Calendar.YEAR);
                        int selectStartMonth = curSelectStartDate.get(Calendar.MONTH);
                        int selectEndMonth = curSelectEndDate.get(Calendar.MONTH);
                        if (selectStartYear != selectEndYear || selectEndMonth != selectStartMonth) {
                            //日期时间段暂不支持跨月筛选
                            ToastUtil.showToastLong(getResources().getString(R.string.tip_profit_unsuport_period));
                            return;
                        }
                        mTimePickViewHelp.dismiss();
                        closeArrow(mIvDate);

                        mCurSelectStartDate = curSelectStartDate;
                        mCurSelectEndDate = curSelectEndDate;
                        setTitleDateView();
                        getSummaryStatisticData();
                        mCurrentPage = 1;
                        getListStatisticData();
                    }
                });
                break;
            default:
                break;
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    private void openArrow(View view) {
        Animation rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        view.startAnimation(rotate);
    }

    public void closeArrow(View view) {
        Animation rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        view.startAnimation(rotate);
    }
}
