package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.InfoBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.NoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.SelectCommonDialog;
import com.hstypay.hstysales.widget.SelectDialog;
import com.igexin.sdk.PushManager;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/11/22 9:34
 * @描述: ${TODO}
 */

public class SettingActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;
    private TextView mTvTitle, mTvTel, mTvTelTitle;
    private Button mBtnExit;
    private RelativeLayout mRlChangePwd, mRlChangeTel;
    private NoticeDialog mNoticeDialog;
    private SafeDialog mLoadDialog;
    private SelectCommonDialog mSelectDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getInstance().addActivity(this);

        initView();
        initListener();
        initData();
    }

    private void initView() {
        setContentView(R.layout.activity_setting);
        StatusBarUtil.setTranslucentStatus(this);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvTel = (TextView) findViewById(R.id.tv_setting_tel);
        mTvTelTitle = (TextView) findViewById(R.id.tv_setting_tel_title);
        mBtnExit = (Button) findViewById(R.id.btn_exit);

        mRlChangePwd = (RelativeLayout) findViewById(R.id.rl_change_pwd);
        mRlChangeTel = (RelativeLayout) findViewById(R.id.rl_change_tel);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.logout_loading), true);
        mTvTitle.setText(getString(R.string.title_setting));
    }


    private void initListener() {
        mIvBack.setOnClickListener(this);
        mBtnExit.setOnClickListener(this);
        mRlChangePwd.setOnClickListener(this);
        mRlChangeTel.setOnClickListener(this);
    }

    private void initData() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        String loginTelephone = SpUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_TELEPHONE);
        if (TextUtils.isEmpty(loginTelephone)) {
            mTvTelTitle.setText(UIUtils.getString(R.string.setting_binding_tel));
            mTvTel.setText("");
        } else {
            mTvTelTitle.setText(UIUtils.getString(R.string.setting_change_tel));
            mTvTel.setText(loginTelephone);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_exit:
                showExitDialog();
                break;
            case R.id.rl_change_pwd:
                StatService.trackCustomKVEvent(SettingActivity.this, "1053", null);
                startActivity(new Intent(this, ChangePwdActivity.class));
                break;
            case R.id.rl_change_tel:
                StatService.trackCustomKVEvent(SettingActivity.this, "1055", null);
                startActivity(new Intent(this, VerifyActivity.class));
                break;
        }
    }

    public void showExitDialog() {
        mSelectDialog = new SelectCommonDialog(SettingActivity.this
                , UIUtils.getString(R.string.logout_content)
                , UIUtils.getString(R.string.btn_cancel_text)
                , UIUtils.getString(R.string.btn_ensure_text)
                , R.layout.select_common_dialog);
        mSelectDialog.setOnClickOkListener(() -> {
            if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                DialogUtil.safeShowDialog(mLoadDialog);
                ServerClient.newInstance(SettingActivity.this).logout(SettingActivity.this, Constants.TAG_LOGOUT, null);
            } else {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
            }
        });
        DialogHelper.resize(SettingActivity.this, mSelectDialog);
        mSelectDialog.show();
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_LOGOUT)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.MSG_LOGOUT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.MSG_LOGOUT_TRUE:
                    SpUtil.removeKey(Constants.SKEY);
                    SpUtil.removeAll();
                    MyApplication.getInstance().finishAllActivity();
                    PushManager.getInstance().turnOffPush(MyApplication.getContext());
                    startActivity(new Intent(SettingActivity.this, LoginActivity.class));
                    break;
            }
        }
    }
}
