package com.hstypay.hstysales.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hstypay.hstysales.BuildConfig;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.ProgressBean;
import com.hstypay.hstysales.bean.UpdateVersionBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.service.DownLoadService;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.ConfigUtil;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.SpStayUtil;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.utils.VersionUtils;
import com.hstypay.hstysales.widget.CommonConfirmDialog;
import com.hstypay.hstysales.widget.CommonNoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.SelectCommonDialog;
import com.hstypay.hstysales.widget.SelectDialog;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class AboutUsActivity extends BaseActivity implements View.OnClickListener {
    private ImageView mIvBack;//test master
    private TextView mTvTitle;
    private TextView mTvVersionName, mTvVersionTitle;
    private TextView tv_ver;
    private RelativeLayout rl_version_check;
    private String mVersionName;
    private int mVersionCode;
    private Intent mService;
    private UpdateVersionBean.DataBean mData;
    private Uri apkurl;
    private CommonConfirmDialog mCommonConfirmDialog;
    private boolean isWifiType;
    private ImageView mIvVersionCheck;
    private SafeDialog mLoadDialog;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mLoadDialog = getLoadDialog(AboutUsActivity.this, UIUtils.getString(R.string.update_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvVersionCheck = (ImageView) findViewById(R.id.iv_version_check);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvVersionTitle = (TextView) findViewById(R.id.tv_version_title);
        mTvVersionName = (TextView) findViewById(R.id.tv_version_name);
        tv_ver = (TextView) findViewById(R.id.tv_ver);
        rl_version_check = (RelativeLayout) findViewById(R.id.rl_version_check);
        mTvTitle.setText(UIUtils.getString(R.string.title_about_us));
        mTvVersionTitle.setText(SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE) ?
                UIUtils.getString(R.string.tv_version_old) : UIUtils.getString(R.string.tv_version_new));
        mIvVersionCheck.setVisibility(SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE) ? View.VISIBLE : View.GONE);
    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        if (ConfigUtil.updateVersionEnable()) {
            rl_version_check.setOnClickListener(this);
        }
    }

    public void initData() {
        mVersionName = VersionUtils.getVersionName(MyApplication.getContext());
        mTvVersionName.setText(mVersionName);
        tv_ver.setText(MyApplication.getContext().getString(R.string.app_name) + " " + mVersionName);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.rl_version_check:
                checkVersion();
                break;
            default:
                break;
        }
    }

    private void checkVersion() {
        // 进行网络判断
        if (!NetworkUtils.isNetworkAvailable(this)) {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        } else {
            //发起网络请求
            mVersionCode = VersionUtils.getVersionCode(AboutUsActivity.this);
            Map<String, Object> map = new HashMap<>();
            map.put("client", Constants.REQUEST_CLIENT_APP);
            map.put("versionCode", mVersionCode);
            map.put("appChannel", ConfigUtil.getAppChannel());
            map.put("appType", "YWY");
            map.put("currentVersion", VersionUtils.getVersionName(MyApplication.getContext()));
            ServerClient.newInstance(AboutUsActivity.this).updateVersion2(AboutUsActivity.this, Constants.UPDATE_VERSION_TAG, map);
        }
    }

    //Eventbus接收数据,版本更新
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateVersion(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_VERSION_UPDATE_ABOUTUS)) {
            UpdateVersionBean msg = (UpdateVersionBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(AboutUsActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mData = msg.getData();
                    if (mData != null) {
                        updateVersion(mData);
                    }

                    break;
            }
        }
        if (event.getTag().equals(Constants.UPDATE_VERSION_TAG)) {
            UpdateVersionBean msg = (UpdateVersionBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                            if (msg.getError().getMessage() != null) {
                                loginDialog(AboutUsActivity.this, msg.getError().getMessage());
                            }
                        } else {
                            if (msg.getError().getMessage() != null) {
                                ToastUtil.showToastShort(msg.getError().getMessage());
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mData = msg.getData();
                    if (mData != null) {
                        updateVersion(mData);
                    }
                    break;
            }
            dismissLoading();
        }
    }

    private void updateVersion(final UpdateVersionBean.DataBean data) {
        if (data != null) {
            if (VersionUtils.getVersionName(MyApplication.getContext()).equals(data.getVersionName())) {
                SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE, false);
                ToastUtil.showToastShort("已是最新版本");
                return;
            }
            if (data.getForceUpgrade() != null && (data.getForceUpgrade().intValue() == 0 || data.getForceUpgrade().intValue() == 1)) {
                update(data);
            }
        }
    }

    private void update(UpdateVersionBean.DataBean data) {
        //进行版本升级
        mTvVersionTitle.setText(UIUtils.getString(R.string.tv_version_old));
        String versionName = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_VERSION_NAME);
        if (!versionName.equals(data.getVersionName())) {
            //如何服务器版本大于保存的版本，直接下载最新的版本
            //启动服务
            showUpgradeInfoDialog(data, new ComDialogListener(data), true);
            //}
        } else {
            String string = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_APP_URL);
            if (!StringUtils.isEmptyOrNull(string)) {//已经下载完成直接安装
                Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), BuildConfig.APPLICATION_ID + ".fileProvider", new File(string));
                apkurl = contentUri;
                showUpgradeInfoDialog(data, new ComDialogListener(data), true);
            }
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    public void showUpgradeInfoDialog(UpdateVersionBean.DataBean result, CommonConfirmDialog.ConfirmListener listener, boolean isWifi) {
        mCommonConfirmDialog = new CommonConfirmDialog(AboutUsActivity.this, listener, result, isWifi);
        if (!mCommonConfirmDialog.isShowing()) {
            mCommonConfirmDialog.show();
        }
    }

    /**
     * 升级点击事件
     */
    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpdateVersionBean.DataBean result;

        public ComDialogListener(UpdateVersionBean.DataBean result) {
            this.result = result;
        }

        @Override
        public void ok() {
            boolean results = PermissionUtils.checkPermissionArray(AboutUsActivity.this, permissionArray);
            if (results) {
                if (apkurl != null) {
                    openAPKFile();
                } else {
                    installDownloadApk(result);
                }
            } else {
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArray, getString(R.string.permission_content_update));
            }
        }

        @Override
        public void cancel() {
            if (mService != null) {
                stopService(mService);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    installDownloadApk(mData);
                } else {
                    showDialog(getString(R.string.permission_set_content_update));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void installDownloadApk(final UpdateVersionBean.DataBean data) {
        if (data != null) {
            if (data.getForceUpgrade() != null && (data.getForceUpgrade().intValue() == 0 || data.getForceUpgrade().intValue() == 1)) {
                //进行版本升级
                SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE, true);
                String versionName = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_VERSION_NAME);
                mService = new Intent(AboutUsActivity.this, DownLoadService.class);
                mService.putExtra("appurl", data.getAppUrl());
                mService.putExtra("versionName", data.getVersionName());
                mService.putExtra(Constants.INTENT_NAME, Constants.UPDATE_PROGRESS_TAG);
                if (!versionName.equals(data.getVersionName())) {
                    //如何服务器版本大于保存的版本，直接下载最新的版本
                    //启动服务
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            startService(mService);
                        }
                    }).start();
                    if (mCommonConfirmDialog != null) {
                        mCommonConfirmDialog.setView(false);
                    }
                } else {
                    String string = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_APP_URL);
                    if (!StringUtils.isEmptyOrNull(string)) {//已经下载完成直接安装
                        Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), BuildConfig.APPLICATION_ID + ".fileProvider", new File(string));
                        apkurl = contentUri;
                        showUpgradeInfoDialog(data, new ComDialogListener(data), true);
                    } else {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                startService(mService);
                            }
                        }).start();
                        if (mCommonConfirmDialog != null) {
                            mCommonConfirmDialog.setView(false);
                        }
                    }
                }

            }
        } else {
            LogUtil.d("版本升级---11111");
        }
    }

    //Eventbus接收数据,服务返回
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateService(NoticeEvent event) {
        if (event.getTag().equals(Constants.UPDATE_PROGRESS_TAG)) {
            switch (event.getCls()) {
                case Constants.UPDATE_DOWNLOAD_SUCCESS:
                    File file = (File) event.getMsg();
                    if (file != null && file.isFile()) {
                        Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), BuildConfig.APPLICATION_ID + ".fileProvider", file);
                        apkurl = contentUri;
                        openAPKFile();
                        if (mCommonConfirmDialog != null) {
                            mCommonConfirmDialog.setView(true);
                        }
                    }
                    break;
                case Constants.UPDATE_DOWNLOAD_FAILED:
                    if (mCommonConfirmDialog != null) {
                        mCommonConfirmDialog.setView(true);
                        ToastUtil.showToastShort("下载失败，请重新下载！");
                    }
                    break;
                case Constants.UPDATE_DOWNLOADING:
                    ProgressBean progressBean = (ProgressBean) event.getMsg();
                    if (mCommonConfirmDialog != null) {
                        mCommonConfirmDialog.setProgress(progressBean);
                    }
                    break;
            }
        }
    }

    private void installAPK(Uri apk, Context context) {
        if (Build.VERSION.SDK_INT < 17) {
            Intent intents = new Intent();
            intents.setAction("android.intent.action.VIEW");
            intents.addCategory("android.intent.category.DEFAULT");
            intents.setType("application/vnd.android.package-archive");
            intents.setData(apk);
            intents.setDataAndType(apk, "application/vnd.android.package-archive");
            intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intents);
            LogUtil.i("zhouwei", "准备下载23以下");
        } else {

            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Constants.DOWNLOADPATH + Constants.APK_FILE_NAME);
            LogUtil.i("zhouwei", "app安装路径23以上===" + file);
            if (file.exists()) {
//                openFile(file, AboutUsActivity.this, apk);
                openAPKFile();
            } else {
                LogUtil.i("zhouwei", "下载失败===");
            }
        }
    }


    /**
     * 打开安装包
     */
    private void openAPKFile() {
        String filePath = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_APP_URL);
        String mimeDefault = "application/vnd.android.package-archive";
        File apkFile = null;
        if (!TextUtils.isEmpty(filePath) && !TextUtils.isEmpty(Uri.parse(filePath).getPath())) {
            apkFile = new File(filePath);
        }
        if (apkFile == null) {
            return;
        }
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //兼容7.0
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                //这里牵涉到7.0系统中URI读取的变更
                Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), BuildConfig.APPLICATION_ID + ".fileProvider", new File(filePath));
                intent.setDataAndType(contentUri, mimeDefault);
                //兼容8.0
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        startInstallPermissionSettingActivity();
                        return;
                    }
                }
            } else {
                intent.setDataAndType(Uri.fromFile(apkFile), mimeDefault);
            }
            if (getPackageManager().queryIntentActivities(intent, 0).size() > 0) {
                //如果APK安装界面存在，携带请求码跳转。使用forResult是为了处理用户 取消 安装的事件。外面这层判断理论上来说可以不要，但是由于国内的定制，这个加上还是比较保险的
                startActivity(intent);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * 跳转到设置-允许安装未知来源-页面
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startInstallPermissionSettingActivity() {
        //后面跟上包名，可以直接跳转到对应APP的未知来源权限设置界面。使用startActivityForResult 是为了在关闭设置界面之后，获取用户的操作结果，然后根据结果做其他处理
        Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, Uri.parse("package:" + getPackageName()));
        startActivityForResult(intent, Constants.REQUEST_INSTALL_PACKAGES);
    }

    /**
     * 功用：未知来源权限弹窗
     * 说明：8.0系统中升级APK时，如果跳转到了 未知来源权限设置界面，并且用户没用允许该权限，会弹出此窗口
     */
    private void showUnKnowResourceDialog() {
        SelectDialog dialog = new SelectDialog(AboutUsActivity.this, "未知来源权限设置界面",
                UIUtils.getString(R.string.btn_cancel_text), UIUtils.getString(R.string.btn_ensure_text)
                , new SelectDialog.HandleBtn() {
            @Override
            public void handleOkBtn() {
                //兼容8.0
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        startInstallPermissionSettingActivity();
                    }
                }
            }

            @Override
            public void handleCancleBtn() {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_INSTALL_PACKAGES) {
            if (resultCode == RESULT_OK) {
                openAPKFile();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        showUnKnowResourceDialog();
                    }
                }
            }
        }
    }
}
