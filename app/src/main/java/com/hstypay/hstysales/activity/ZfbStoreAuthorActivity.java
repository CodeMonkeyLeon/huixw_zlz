package com.hstypay.hstysales.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.Zxing.MaxCardManager;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.Info;
import com.hstypay.hstysales.bean.QueryAgentStoreInfoBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.ClickUtil;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.ScreenUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CommonNoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author kuangzeyu
 * @time 2021/3/23
 * @desc 支付宝门店授权管理
 */
public class ZfbStoreAuthorActivity extends BaseActivity implements View.OnClickListener {

    private TextView tv_title;
    private ImageView iv_back;
    private ImageView mIvIconAuthStatusZfb;//授权状态图片
    private TextView mTvAuthStatusZfb;//授权状态文字
    private FrameLayout mRlContentBottomFunction;
    private TextView mTvGenReviewCode;//生成或查看授权二维码
    private ImageView mIvTwoDimenCodeZfb;//授权二维码
    private TextView mTvShareWeixinZfb;//微信分享二维码
    private TextView mTvSaveFileZfb;//保存二维码
    private TextView mTvCreateZfbStore;//创建支付宝门店
    private LinearLayout mLlShowZfbStoreId;//ll支付宝门店id
    private TextView mTvIdZfbStore;//tv支付宝门店ID
    private SafeDialog shareDialog;
    private SafeDialog mLoadDialog;
    private String qrCodeUrl;//授权二维码
    private LinearLayout llContentZfbAuth;
    private LinearLayout llFailTipZfbStore;//创建门店失败提示

    private UMShareListener mShareListener  = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
            DialogUtil.safeShowDialog(shareDialog);
        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(shareDialog);
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            DialogUtil.safeCloseDialog(shareDialog);
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(shareDialog);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zfb_store_auth);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }


    private void initEvent() {
        iv_back.setOnClickListener(this);
    }

    private void initData() {
        String merchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", merchantId);
            //查询支付宝门店代运营门店信息，包括获取代运营门店id
            ServerClient.newInstance(MyApplication.getContext()).queryZfbAgentStoreInfo(MyApplication.getContext(), Constants.TAG_QUERY_AGENT_STORE_INFO, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void initView() {
        shareDialog = getLoadDialog(this, UIUtils.getString(R.string.share_loading), true, 0.9f);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText(UIUtils.getString(R.string.zfb_auth_opt));
        iv_back = (ImageView) findViewById(R.id.iv_back);
        mIvIconAuthStatusZfb = findViewById(R.id.iv_icon_auth_status_zfb);
        mTvAuthStatusZfb = findViewById(R.id.tv_auth_status_zfb);
        mRlContentBottomFunction = findViewById(R.id.fl_content_bottom_function);
        llContentZfbAuth = findViewById(R.id.ll_content_zfb_auth);
        llContentZfbAuth.setVisibility(View.GONE);
        llFailTipZfbStore = findViewById(R.id.ll_fail_tip_zfb_store);
        llFailTipZfbStore.setVisibility(View.GONE);
    }

    private void test(){
        //生成或查看授权二维码
        mRlContentBottomFunction.removeAllViews();
        View view_gen_code = View.inflate(this, R.layout.layout_zfb_gen_code, mRlContentBottomFunction);
        mTvGenReviewCode = view_gen_code.findViewById(R.id.tv_gen_review_code);
        mTvGenReviewCode.setOnClickListener(this);

        //创建支付宝门店 或 展示支付宝门店号
        mRlContentBottomFunction.removeAllViews();
        View view_store_id = View.inflate(this, R.layout.layout_zfb_store_id, mRlContentBottomFunction);
        mTvCreateZfbStore = view_store_id.findViewById(R.id.tv_create_zfb_store);
        mTvCreateZfbStore.setOnClickListener(this);
        mLlShowZfbStoreId = view_store_id.findViewById(R.id.ll_show_zfb_store_id);
        mTvIdZfbStore = view_store_id.findViewById(R.id.tv_id_zfb_store);

        //展示授权二维码
        mRlContentBottomFunction.removeAllViews();
        View view_review_code = View.inflate(this, R.layout.layout_zfb_review_code, mRlContentBottomFunction);
        mIvTwoDimenCodeZfb = view_review_code.findViewById(R.id.iv_two_dimen_code_zfb);
        LinearLayout ll_share_code_zfb = findViewById(R.id.ll_share_code_zfb);
        ll_share_code_zfb.setVisibility(View.VISIBLE);
        mTvShareWeixinZfb = findViewById(R.id.tv_share_weixin_zfb);
        mTvSaveFileZfb = findViewById(R.id.tv_save_file_zfb);
        mTvShareWeixinZfb.setOnClickListener(this);
        mTvSaveFileZfb.setOnClickListener(this);
        try {
            Bitmap dCode = MaxCardManager.getInstance().create2DCode("http://www.baidu.com",
                    DensityUtils.dip2px(this, 165),
                    DensityUtils.dip2px(this, 165));
            mIvTwoDimenCodeZfb.setImageBitmap(dCode);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_QUERY_AGENT_STORE_INFO) || event.getTag().equals(Constants.TAG_GEN_AGENT_STORE_INFO)) {
            //查询支付宝门店代运营门店信息返回、未授权时生成授权二维码返回
            DialogUtil.safeCloseDialog(mLoadDialog);
            QueryAgentStoreInfoBean msg = (QueryAgentStoreInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    if (event.getTag().equals(Constants.TAG_GEN_AGENT_STORE_INFO)){
                        showGenCodeBtnView();
                    }

                    showCommonNoticeDialog(ZfbStoreAuthorActivity.this,UIUtils.getString(R.string.error_network_request), msg.getError().getMessage(), new CommonNoticeDialog.OnClickOkListener() {
                        @Override
                        public void onClickOk() {
                            if( event.getTag().equals(Constants.TAG_QUERY_AGENT_STORE_INFO)){
                                finish();
                            }
                        }
                    });
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ZfbStoreAuthorActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    //ToastUtil.showToastShort(msg.getError().getMessage());
                                    showCommonNoticeDialog(ZfbStoreAuthorActivity.this, getResources().getString(R.string.title_wxts), msg.getError().getMessage(), new CommonNoticeDialog.OnClickOkListener() {
                                        @Override
                                        public void onClickOk() {
                                            if( event.getTag().equals(Constants.TAG_QUERY_AGENT_STORE_INFO)){
                                                finish();
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }
                    if (event.getTag().equals(Constants.TAG_GEN_AGENT_STORE_INFO)){
                        showGenCodeBtnView();
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg!=null){
                        llContentZfbAuth.setVisibility(View.VISIBLE);
                        llFailTipZfbStore.setVisibility(View.GONE);
                        QueryAgentStoreInfoBean.AgentStoreInfo agentStoreInfo = msg.getData();
                        if (agentStoreInfo==null){
                            //默认为未授权
                            mIvIconAuthStatusZfb.setImageResource(R.mipmap.icon_zfb_wei_auth);
                            mTvAuthStatusZfb.setText(getResources().getString(R.string.zfb_agent_store_wei_auth));

                            showGenCodeBtnView();
                        }else {
                            String authorizeStatus = agentStoreInfo.getAuthorizeStatus();
                            if ("2".equals(authorizeStatus)){
                                //成功-已授权
                                mIvIconAuthStatusZfb.setImageResource(R.mipmap.icon_zfb_yi_auth);
                                mTvAuthStatusZfb.setText(getResources().getString(R.string.zfb_agent_store_yi_auth));
                                //是否已创建支付宝门店
                                String shopStatus = agentStoreInfo.getShopStatus();
                                if ("3".equals(shopStatus)){
                                    //已成功创建，显示支付宝门店号
                                    String shopId = agentStoreInfo.getShopId();
                                    mRlContentBottomFunction.removeAllViews();
                                    View view_store_id = View.inflate(this, R.layout.layout_zfb_store_id, mRlContentBottomFunction);
                                    mTvCreateZfbStore = view_store_id.findViewById(R.id.tv_create_zfb_store);
                                    mTvCreateZfbStore.setOnClickListener(this);
                                    mLlShowZfbStoreId = view_store_id.findViewById(R.id.ll_show_zfb_store_id);
                                    mTvIdZfbStore = view_store_id.findViewById(R.id.tv_id_zfb_store);
                                    mTvCreateZfbStore.setVisibility(View.GONE);
                                    mLlShowZfbStoreId.setVisibility(View.VISIBLE);
                                    mTvIdZfbStore.setText(shopId);
                                }else {
                                    if ("1".equals(shopStatus)||"4".equals(shopStatus)){
                                        //1-初始：显示创建支付宝门店； 4-创建失败： 显示创建支付宝门店,并提示创建失败；
                                        mRlContentBottomFunction.removeAllViews();
                                        View view_store_id = View.inflate(this, R.layout.layout_zfb_store_id, mRlContentBottomFunction);
                                        mTvCreateZfbStore = view_store_id.findViewById(R.id.tv_create_zfb_store);
                                        mTvCreateZfbStore.setOnClickListener(this);
                                        mLlShowZfbStoreId = view_store_id.findViewById(R.id.ll_show_zfb_store_id);
                                        mTvCreateZfbStore.setVisibility(View.VISIBLE);
                                        mLlShowZfbStoreId.setVisibility(View.GONE);
                                        mTvCreateZfbStore.setText(getResources().getString(R.string.zfb_create_store));
                                        mTvCreateZfbStore.setTextColor(getResources().getColor(R.color.home_blue_text));
                                        if ("4".equals(shopStatus)){
                                            //4-创建失败：
                                            llFailTipZfbStore.setVisibility(View.VISIBLE);
                                            String shopResultDesc = agentStoreInfo.getShopResultDesc();
                                            TextView tv_fail_tip_zfb_store = findViewById(R.id.tv_fail_tip_zfb_store);
                                            tv_fail_tip_zfb_store.setText(getResources().getString(R.string.zfb_store_create_fail)+shopResultDesc);
                                            findViewById(R.id.iv_fail_tip_zfb_store).setOnClickListener(ZfbStoreAuthorActivity.this);
                                        }
                                    }else if ("2".equals(shopStatus)){
                                        //2-受理中： 显示受理中；
                                        mRlContentBottomFunction.removeAllViews();
                                        View view_store_id = View.inflate(this, R.layout.layout_zfb_store_id, mRlContentBottomFunction);
                                        mTvCreateZfbStore = view_store_id.findViewById(R.id.tv_create_zfb_store);
                                        mTvCreateZfbStore.setOnClickListener(this);
                                        mLlShowZfbStoreId = view_store_id.findViewById(R.id.ll_show_zfb_store_id);
                                        mTvCreateZfbStore.setVisibility(View.VISIBLE);
                                        mLlShowZfbStoreId.setVisibility(View.GONE);
                                        mTvCreateZfbStore.setText(getResources().getString(R.string.zfb_store_checking));
                                        mTvCreateZfbStore.setTextColor(getResources().getColor(R.color.tv_color_9295a3));
                                    }

                                }
                            }else {
                                //未授权代运营门店
                                mIvIconAuthStatusZfb.setImageResource(R.mipmap.icon_zfb_wei_auth);
                                mTvAuthStatusZfb.setText(getResources().getString(R.string.zfb_agent_store_wei_auth));
                            /*//是否已生成授权二维码
                            qrCodeUrl = agentStoreInfo.getQrCodeUrl();
                            if (TextUtils.isEmpty(qrCodeUrl)){
                                //未生成授权二维码，显示生成二维码按钮，未授权时该接口一定会返回一个qrCodeUrl，所以不存在此条件的情况
                                mRlContentBottomFunction.removeAllViews();
                                View view_gen_code = View.inflate(this, R.layout.layout_zfb_gen_code, mRlContentBottomFunction);
                                mTvGenReviewCode = view_gen_code.findViewById(R.id.tv_gen_review_code);
                                mTvGenReviewCode.setOnClickListener(this);
                                mTvGenReviewCode.setText(getResources().getString(R.string.zfb_gen_auth_code));
                            }else {
                                //生成了授权二维码，显示查看二维码按钮
                                mRlContentBottomFunction.removeAllViews();
                                View view_gen_code = View.inflate(this, R.layout.layout_zfb_gen_code, mRlContentBottomFunction);
                                mTvGenReviewCode = view_gen_code.findViewById(R.id.tv_gen_review_code);
                                mTvGenReviewCode.setOnClickListener(this);
                                mTvGenReviewCode.setText(getResources().getString(R.string.zfb_review_auth_code));
                            }*/
                                if( event.getTag().equals(Constants.TAG_QUERY_AGENT_STORE_INFO)){
                                    //是查授权信息返回
                                    showGenCodeBtnView();
                                }else {
                                    //生成授权二维码
                                    qrCodeUrl = agentStoreInfo.getQrCodeUrl();
                                    showQrCode();
                                }

                            }
                        }
                    }
                    break;
            }
        }else if (event.getTag().equals(Constants.TAG_CREATE_AGENT_STORE)){
            //去创建支付宝门店返回
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    DialogUtil.safeCloseDialog(mLoadDialog);
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(ZfbStoreAuthorActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    //创建门店失败，去获取失败原因描述信息展示
                    initData();
                    break;
                case Constants.ON_EVENT_TRUE://业务成功
                    //创建门店成功,去获取门店id信息展示
                    initData();
                    break;
            }
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_gen_review_code:
                //查看或生成授权二维码
                /*if (TextUtils.isEmpty(qrCodeUrl)){
                    //生成授权码，接口一定会返回一个qrCodeUrl，所以不存在生成授权码的情况。但以防万一出现了这种情况，就再调一次获取二维码的接口
                    initData();
                }else if (getResources().getString(R.string.zfb_review_auth_code).equals(tvGenReview)){
                    //查看授权二维码
                    showQrCode();
                }*/

                mRlContentBottomFunction.removeAllViews();
                View view_review_code = View.inflate(this, R.layout.layout_zfb_review_code, mRlContentBottomFunction);
                TextView tv_des_code_zfb = view_review_code.findViewById(R.id.tv_des_code_zfb);
                tv_des_code_zfb.setText(getResources().getString(R.string.zfb_tip_gen_code));
                //生成后显示授权二维码
                genQrCode();
                break;
            case R.id.tv_share_weixin_zfb:
                //分享二维码
                shareToWx();
                break;
            case R.id.tv_save_file_zfb:
                //保存二维码至相册
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    saveImgToGallery();
                }else {
                    ActivityCompat.requestPermissions(ZfbStoreAuthorActivity.this,new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},100);
                }
                break;
            case R.id.tv_create_zfb_store:
                //创建支付宝门店
                createZfbStore();
                break;
            case R.id.iv_fail_tip_zfb_store:
                //隐藏创建失败提示文案
                llFailTipZfbStore.setVisibility(View.GONE);
                break;
            default:
                break;

        }
    }

    //生成要查看的二维码
    private void genQrCode() {
        String merchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", merchantId);
            //生成授权二维码
            ServerClient.newInstance(MyApplication.getContext()).genZfbAgentAutoCode(MyApplication.getContext(), Constants.TAG_GEN_AGENT_STORE_INFO, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }


    //去创建支付宝门店
    private void createZfbStore() {
        String merchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            llFailTipZfbStore.setVisibility(View.GONE);
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", merchantId);
            //去创建支付宝代运营门店
            ServerClient.newInstance(MyApplication.getContext()).createAgentStore(MyApplication.getContext(), Constants.TAG_CREATE_AGENT_STORE, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    //显示生成授权二维码按钮view
    private void showGenCodeBtnView() {
        mRlContentBottomFunction.removeAllViews();
        View view_gen_code = View.inflate(this, R.layout.layout_zfb_gen_code, mRlContentBottomFunction);
        mTvGenReviewCode = view_gen_code.findViewById(R.id.tv_gen_review_code);
        mTvGenReviewCode.setOnClickListener(this);
        mTvGenReviewCode.setText(getResources().getString(R.string.zfb_review_auth_code));
    }

    //显示授权二维码view
    private void showQrCode() {
        mRlContentBottomFunction.removeAllViews();
        View view_review_code = View.inflate(this, R.layout.layout_zfb_review_code, mRlContentBottomFunction);
        TextView tv_des_code_zfb = view_review_code.findViewById(R.id.tv_des_code_zfb);
        tv_des_code_zfb.setText(getResources().getString(R.string.zfb_tip_scan_code));
        mIvTwoDimenCodeZfb = view_review_code.findViewById(R.id.iv_two_dimen_code_zfb);
        LinearLayout ll_share_code_zfb = findViewById(R.id.ll_share_code_zfb);
        ll_share_code_zfb.setVisibility(View.VISIBLE);
        mTvShareWeixinZfb = findViewById(R.id.tv_share_weixin_zfb);
        mTvSaveFileZfb = findViewById(R.id.tv_save_file_zfb);
        mTvShareWeixinZfb.setOnClickListener(this);
        mTvSaveFileZfb.setOnClickListener(this);
        try {
            Bitmap dCode = MaxCardManager.getInstance().create2DCode(qrCodeUrl,
                    DensityUtils.dip2px(this, 165),
                    DensityUtils.dip2px(this, 165));
            mIvTwoDimenCodeZfb.setImageBitmap(dCode);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }


    //分享图片到微信
    private void shareToWx() {
        if (!ClickUtil.isNotFastClick()){
            return;
        }
        UMImage image = new UMImage(this, ScreenUtils.createViewBitmapWithColor(findViewById(R.id.fl_two_dimen_code),getResources().getColor(R.color.bg_f4f6f8)));//bitmap文件
        new ShareAction(this)
                .withMedia(image)
                .setPlatform(SHARE_MEDIA.WEIXIN)
                .setCallback(mShareListener).share();
    }


    //保存图片至相册
    private void saveImgToGallery() {
        if (!ClickUtil.isNotFastClick()){
            return;
        }
        ScreenUtils.saveImageToGallery(this, ScreenUtils.createViewBitmapWithColor(findViewById(R.id.fl_two_dimen_code),getResources().getColor(R.color.bg_f4f6f8)));
        CommonNoticeDialog dialog = new CommonNoticeDialog(this,  getString(R.string.dialog_copy_picture), "");
        DialogHelper.resize(this, dialog);
        dialog.show();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DialogUtil.safeCloseDialog(shareDialog);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(shareDialog);
        UMShareAPI.get(this).release();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==100){
            if (grantResults.length>0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)){
                saveImgToGallery();
            }else {
                CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(this
                        , getString(R.string.permission_content_storage), getString(R.string.btn_know));
                commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
                    @Override
                    public void onClickOk() {
                        commonNoticeDialog.dismiss();
                    }
                });
                DialogHelper.resize(this, commonNoticeDialog);
                commonNoticeDialog.show();
            }
        }

    }

}
