/*
package com.hstypay.hstysales.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.CardDetailBean;
import com.hstypay.hstysales.bean.IndustryBean;
import com.hstypay.hstysales.bean.LocationBean;
import com.hstypay.hstysales.bean.MerChantModifyBean;
import com.hstypay.hstysales.bean.MerchantInfoBean;
import com.hstypay.hstysales.bean.UploadImageBean;
import com.hstypay.hstysales.bean.VipInfoBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LocationUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.AddressSelector;
import com.hstypay.hstysales.widget.BottomDialog;
import com.hstypay.hstysales.widget.NoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.ShadowLayout;
import com.qiezzi.choseviewlibrary.ChoseCityPicker;
import com.qiezzi.choseviewlibrary.bean.AddressBean;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class MerchantActivity extends BaseActivity implements View.OnClickListener, AddressSelector.OnAddressSelectedListener, AddressSelector.OnDialogCloseListener {
    private ImageView mIvBack, mIvTitleButton;
    private Button mBtnEnsure, mBtnUnsure;
    private TextView mTvTitle, mTvReason, mTvShopId, mTvIndustry, mTvShopName, mTvVipTitle;
    private LinearLayout mLlMerchantInfo,mLlRemark;
    private RelativeLayout mRlCardPhoto, mRlBankCard, mRlArea, mRlIndustry, mRlBindCode, mRlOpenVip;
    private TextView mTvArea,mTvKeeperNameTitle,mTvCardIdTitle;
    private LinearLayout mLlNotice, mLlShortName;
    private EditText mEtAddress, mEtShopName, mEtShortName, mEtKeeperName, mEtCardId,mEtRemark,mEtEmail,mEtServicePhone;
    private BottomDialog industryDialog;
    private ImageView mIvStatus, mIvAddress, mIvKeeperName, mIvCardId,mIvEmail,mIvServicePhone;
    private TextView mTvNotice, mTvLoadResult;
    private ChoseCityPicker mCityPicker;
    private static final int CHECKING = 0;
    private static final int CHECK_PASSED = 1;
    private static final int CHECK_FAILED = 2;
    private ImageView mIvShopName, mIvShortName,mIvRemark;
    //private MerchantInfoBean.DataEntity mData;
    private ImageView mIvIndustry, mIvArea;
    private SafeDialog mLoadDialog, mSubmitDialog;
    private String mMerchantId, mMerchantTel, mMerchantName;
    private ShadowLayout mShadowtButton;
    private RelativeLayout mRlSubmitButton;
    private String propValue;
    private CardDetailBean.DataEntity mCardData;
    private boolean isFirstOpen = false;
    private int memberCheckStatus = -1;// 0:不显示，1：没有开通，2：审核中(汇商),3：审核中(世明),4：审核失败，5:审核成功
    private static final int NOT_SHOW = 0;//不显示“开通会员卡”
    private static final int NOT_OPEN = 1;
    private static final int HS_CHECKING = 2;
    private static final int SM_CHECKING = 3;
    private static final int HS_CHECK_FAILED = 4;
    private static final int SM_CHECK_SUCCESS = 5;
    private static final int SM_CHECK_FAILED = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant);
        MyApplication.getInstance().addActivity(this);
        initView();
        initEvent();
        initData();
    }

    public void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mIvTitleButton = (ImageView) findViewById(R.id.iv_title_button);
        mIvTitleButton.setVisibility(View.VISIBLE);
        mRlSubmitButton = (RelativeLayout) findViewById(R.id.rl_submit_button);
        mShadowtButton = (ShadowLayout) findViewById(R.id.shadow_button);
        mBtnEnsure = (Button) findViewById(R.id.btn_common_enable);
        mBtnUnsure = (Button) findViewById(R.id.btn_common_unable);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mLlMerchantInfo = (LinearLayout) findViewById(R.id.ll_merchant_info);
        mRlBankCard = (RelativeLayout) findViewById(R.id.rl_bank_card);
        mRlCardPhoto = (RelativeLayout) findViewById(R.id.rv_card_photo);
        mRlBindCode = (RelativeLayout) findViewById(R.id.rl_bind_code);
        mRlOpenVip = (RelativeLayout) findViewById(R.id.rl_open_vip);
        mTvTitle.setText(UIUtils.getString(R.string.title_merchant_info));

        mLlNotice = (LinearLayout) findViewById(R.id.ll_merchant_notice);
        mTvReason = (TextView) findViewById(R.id.tv_merchant_reason);
        mIvStatus = (ImageView) findViewById(R.id.iv_check_status);
        mTvNotice = (TextView) findViewById(R.id.tv_merchant_notice);

        mTvKeeperNameTitle = (TextView) findViewById(R.id.tv_keeper_name_title);//姓名title
        mEtKeeperName = (EditText) findViewById(R.id.keeper_name);//姓名
        mIvKeeperName = (ImageView) findViewById(R.id.keeper_name_delete);
        mTvCardIdTitle = (TextView) findViewById(R.id.tv_card_id_title);//身份证号
        mEtCardId = (EditText) findViewById(R.id.card_id);//身份证号
        mIvCardId = (ImageView) findViewById(R.id.card_id_delete);

        mEtShopName = (EditText) findViewById(R.id.shop_name);//商户名称
        mIvShopName = (ImageView) findViewById(R.id.shop_name_delete);
        mEtShortName = (EditText) findViewById(R.id.short_name);//商户简称
        mIvShortName = (ImageView) findViewById(R.id.short_name_delete);
        mTvShopId = (TextView) findViewById(R.id.shop_id);//商户编号

        mTvIndustry = (TextView) findViewById(R.id.tv_shop_attach_content);//所属行业
        mRlIndustry = (RelativeLayout) findViewById(R.id.rl_attach_industry);
        mIvIndustry = (ImageView) findViewById(R.id.iv_shop_attach_arrow);
        mIvArea = (ImageView) findViewById(R.id.iv_shop_city_location);
        mRlArea = (RelativeLayout) findViewById(R.id.rl_area);
        mTvArea = (TextView) findViewById(R.id.tv_shop_city_content);

        mEtAddress = (EditText) findViewById(R.id.shop_city_detail);//详细地址
        mIvAddress = (ImageView) findViewById(R.id.iv_shop_city_delete);

        mLlShortName = (LinearLayout) findViewById(R.id.ll_short_name);//企业简称
        mTvShopName = (TextView) findViewById(R.id.tv_shop_name);
        mTvVipTitle = (TextView) findViewById(R.id.tv_open_vip);
        mTvLoadResult = (TextView) findViewById(R.id.tv_upload_result);

        mEtEmail = (EditText) findViewById(R.id.et_email);//邮箱
        mIvEmail = (ImageView) findViewById(R.id.iv_email_delete);
        mEtServicePhone = (EditText) findViewById(R.id.et_service_phone);//客服电话
        mIvServicePhone = (ImageView) findViewById(R.id.iv_service_phone_delete);
        mEtRemark = (EditText) findViewById(R.id.et_remark);//备注
        mIvRemark = (ImageView) findViewById(R.id.iv_remark_delete);
        mLlRemark = findViewById(R.id.ll_remark);

        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mSubmitDialog = getLoadDialog(this, UIUtils.getString(R.string.submit_loading), false);

    }

    public void initEvent() {
        mIvBack.setOnClickListener(this);
        mIvTitleButton.setOnClickListener(this);
        mBtnEnsure.setOnClickListener(this);
        mRlCardPhoto.setOnClickListener(this);
        mRlBankCard.setOnClickListener(this);
        mRlBindCode.setOnClickListener(this);
        mRlOpenVip.setOnClickListener(this);
        mRlArea.setOnClickListener(this);
        mRlIndustry.setOnClickListener(this);
        mIvArea.setOnClickListener(this);
        mIvKeeperName.setOnClickListener(this);
        mIvCardId.setOnClickListener(this);

        mIvAddress.setOnClickListener(this);
        mIvShopName.setOnClickListener(this);
        mIvShortName.setOnClickListener(this);
        mIvRemark.setOnClickListener(this);
        mIvEmail.setOnClickListener(this);
        mIvServicePhone.setOnClickListener(this);
        setEdittextListener(mEtKeeperName, mIvKeeperName);
        setEdittextListener(mEtCardId, mIvCardId);
        setEdittextListener(mEtAddress, mIvAddress);
        setEdittextListener(mEtShopName, mIvShopName);
        setEdittextListener(mEtShortName, mIvShortName);
        setEdittextListener(mEtRemark, mIvRemark);
        setEdittextListener(mEtEmail, mIvEmail);
        setEdittextListener(mEtServicePhone, mIvServicePhone);
    }

    public void initData() {
        mCardData = new CardDetailBean().new DataEntity();
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        mMerchantTel = getIntent().getStringExtra(Constants.INTENT_MERCHANT_TEL);
        mMerchantName = getIntent().getStringExtra(Constants.INTENT_MERCHANT_NAME);
        getData(mMerchantId);
    }

    private void getData(String merchantId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", merchantId);
            ServerClient.newInstance(MyApplication.getContext()).getMerchantInfo(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_INFO, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getMemberData(String merchantId) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", merchantId);
            ServerClient.newInstance(MyApplication.getContext())
                    .goEditMchMember(MyApplication.getContext(), Constants.TAG_GET_VIP_INFO, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    //注册Eventbus
    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MERCHANT_ADDRESS)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            AddressBean msg = (AddressBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.GET_ADDRESS_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }

                    break;
                case Constants.GET_ADDRESS_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        area(msg);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GET_INDUSTRY_TYPE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            IndustryBean msg = (IndustryBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.GET_INDUSTRY_TYPE_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_INDUSTRY_TYPE_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        showIndustryDiaog(msg);
                    }
                    break;
            }
            dismissLoading();
        }
        if (event.getTag().equals(Constants.TAG_GET_MERCHANT_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            MerchantInfoBean msg = (MerchantInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.GET_MERCHANT_INFO_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_MERCHANT_INFO_TRUE://请求成功
                    if (msg.getData() != null) {
                        mData = msg.getData();
                        setCardEntity(mCardData, mData);
                        mLlMerchantInfo.setVisibility(View.VISIBLE);
                        setView(mData);
                    }
                    break;
            }
            dismissLoading();
        }
        if (event.getTag().equals(Constants.TAG_SUBMIT_MERCHANT_INFO)) {
            DialogUtil.safeCloseDialog(mSubmitDialog);
            MerChantModifyBean msg = (MerChantModifyBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.SUBMIT_MERCHANT_INFO_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.SUBMIT_MERCHANT_INFO_TRUE://请求成功
                    if (msg.isStatus()) {
                        NoticeDialog noticeDialog = new NoticeDialog(MerchantActivity.this, null, null, R.layout.notice_dialog_submit);
                        noticeDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                            @Override
                            public void clickOk() {
                                setResult(RESULT_OK);
                                finish();
                            }
                        });
                        DialogHelper.resize(MerchantActivity.this, noticeDialog);
                        noticeDialog.show();
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_QUEST_LOCATION)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            LocationBean msg = (LocationBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.QUEST_LOCATION_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.QUEST_LOCATION_TRUE://请求成功
                    if (msg.getData() != null) {
                        mTvArea.setText(msg.getData().getProvince() + " " + msg.getData().getCity() + " " + msg.getData().getDistrict());
                        mData.setProvince(msg.getData().getProvinceCode());
                        mData.setCity(msg.getData().getCitycode());
                        mData.setCounty(msg.getData().getAdcode());
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CARD_DETAIL)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            CardDetailBean msg = (CardDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        Intent intent = new Intent();
                        intent.putExtra(Constants.INTENT_BANK_DETAIL, msg.getData());
                        if (msg.getData().getMerchantClass() == 1) {
                            intent.setClass(MerchantActivity.this, ChangeCompanyCardActivity.class);
                        } else if (msg.getData().getMerchantClass() == 2) {
                            intent.setClass(MerchantActivity.this, ChangeCardActivity.class);
                        }
                        startActivity(intent);
                        finish();
                    } else {
                        ToastUtil.showToastShort(getString(R.string.error_data));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_GET_VIP_INFO)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            VipInfoBean msg = (VipInfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(MerchantActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null) {
                        Intent intentOpenVip = new Intent();
                        intentOpenVip.putExtra(Constants.INTENT_MERCHANT_ID, mMerchantId);
                        memberCheckStatus = msg.getData().getExamineStatus();
                        switch (memberCheckStatus) {
                            case HS_CHECKING://跳转到汇商审核中页面
                                intentOpenVip.setClass(MerchantActivity.this, VipInfoActivity.class);
                                intentOpenVip.putExtra(Constants.INTENT_VIP_INFO, msg.getData());
                                intentOpenVip.putExtra(Constants.INTENT_SKIP_MEMBER_DETAIL, true);//重新提交，是否显示详情
                                startActivity(intentOpenVip);
                                break;
                            case HS_CHECK_FAILED://跳转到汇商审核失败页面
                                intentOpenVip.setClass(MerchantActivity.this, VipInfoActivity.class);
                                intentOpenVip.putExtra(Constants.INTENT_VIP_INFO, msg.getData());
                                intentOpenVip.putExtra(Constants.INTENT_SKIP_MEMBER_DETAIL, true);//重新提交，是否显示详情
                                intentOpenVip.putExtra(Constants.INTENT_NAME, Constants.INTENT_VIP_SUCCESS);
                                startActivity(intentOpenVip);
                                break;
                            case SM_CHECKING://跳转到微信审核中页面
                                intentOpenVip.setClass(MerchantActivity.this, VerifyStatusActivity.class);
                                intentOpenVip.putExtra(Constants.INTENT_OPEN_MEMBER, SM_CHECKING);
                                startActivity(intentOpenVip);
                                break;
                            case SM_CHECK_SUCCESS://跳转到会员卡二维码页面
                                intentOpenVip.setClass(MerchantActivity.this, ShareVipActivity.class);
                                intentOpenVip.putExtra(Constants.INTENT_MERCHANT_NAME, mMerchantName);
                                startActivity(intentOpenVip);
                                break;
                            case SM_CHECK_FAILED://跳转到微信审核失败页面
                                intentOpenVip.setClass(MerchantActivity.this, VerifyStatusActivity.class);
                                intentOpenVip.putExtra(Constants.INTENT_OPEN_MEMBER, SM_CHECK_FAILED);
                                startActivity(intentOpenVip);
                                break;
                        }
                    }
                    break;
            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_title_button:
                mtaProperties(MerchantActivity.this, "1038", "status", propValue);
                call(mMerchantTel);
                break;
            case R.id.rv_card_photo:
                //上传证件照
                Intent intent = new Intent(this, UploadImageActivity.class);
                if (mData != null) {
                    intent.putExtra(Constants.INTENT_NAME_MERCHANT_UPLOAD, mData);
                }
                startActivityForResult(intent, Constants.REQUEST_UPLOAD_IMAGE);
                break;
            case R.id.rl_bank_card:
                //更改结算银行卡
                Intent intentCard = new Intent();
                intentCard.putExtra(Constants.INTENT_BANK_DETAIL, mCardData);
                if (mData.getMerchantClass() == 1) {
                    intentCard.setClass(MerchantActivity.this, ChangeCompanyCardActivity.class);
                } else if (mData.getMerchantClass() == 2) {
                    intentCard.setClass(MerchantActivity.this, ChangeCardActivity.class);
                }
                startActivityForResult(intentCard, Constants.REQUEST_MERCHANT_CARD);
                break;
            case R.id.rl_bind_code:
                Intent intentBind = new Intent(MerchantActivity.this, BindingCodeActivity.class);
                intentBind.putExtra(Constants.INTENT_NAME, Constants.INTENT_BIND_CODE);
                intentBind.putExtra(Constants.INTENT_MERHANT_ID, mMerchantId);
                startActivity(intentBind);
                break;
            case R.id.rl_open_vip:
                if (memberCheckStatus == NOT_OPEN) {
                    Intent intentOpenVip = new Intent();
                    intentOpenVip.setClass(MerchantActivity.this, VipGuideActivity.class);
                    intentOpenVip.putExtra(Constants.INTENT_MERCHANT_ID, mMerchantId);
                    intentOpenVip.putExtra(Constants.INTENT_NAME, Constants.INTENT_VIP_SUCCESS);
                    startActivity(intentOpenVip);
                } else {
                    getMemberData(mMerchantId);
                }
                break;
            //所在地区
            case R.id.rl_area:
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    ServerClient.newInstance(MyApplication.getContext()).getAddress(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_ADDRESS, null);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                }
                break;
            case R.id.rl_attach_industry:
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    ServerClient.newInstance(MyApplication.getContext()).getIndustryType(MyApplication.getContext(), Constants.TAG_GET_INDUSTRY_TYPE, null);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                }
                break;
            case R.id.btn_common_enable:
                StatService.trackCustomKVEvent(MerchantActivity.this, "1039", null);
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    Map<String, Object> map = new HashMap<>();
                    if (StringUtils.isName(mEtKeeperName.getText().toString().trim())) {
                        map.put("principal", mEtKeeperName.getText().toString().trim());
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.error_name));
                        mEtKeeperName.setFocusable(true);
                        mEtKeeperName.setFocusableInTouchMode(true);
                        mEtKeeperName.requestFocus();
                        return;
                    }

                    if (StringUtils.isIdCard(mEtCardId.getText().toString().trim())) {
                        map.put("idCode", mEtCardId.getText().toString().trim());
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.error_card_id));
                        mEtCardId.setFocusable(true);
                        mEtCardId.setFocusableInTouchMode(true);
                        mEtCardId.requestFocus();
                        return;
                    }
                    if (!StringUtils.isEmptyOrNull(mEtEmail.getText().toString().trim())) {
                        map.put("email", mEtEmail.getText().toString().trim());
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.hint_email));
                        mEtEmail.setFocusable(true);
                        mEtEmail.setFocusableInTouchMode(true);
                        mEtEmail.requestFocus();
                        return;
                    }
                    if (!StringUtils.isEmptyOrNull(mEtServicePhone.getText().toString().trim())) {
                        map.put("servicePhone", mEtServicePhone.getText().toString().trim());
                    } else {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.hint_service_phone));
                        mEtServicePhone.setFocusable(true);
                        mEtServicePhone.setFocusableInTouchMode(true);
                        mEtServicePhone.requestFocus();
                        return;
                    }
                    map.put("merchantName", mEtShopName.getText().toString().trim());
                    map.put("merchantShortName", mEtShortName.getText().toString().trim());
                    map.put("remark", mEtRemark.getText().toString().trim());
                    */
/*if (TextUtils.isEmpty(mEtShopName.getText().toString().trim())) {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.error_merchant_name_empty));
                        mEtShopName.setFocusable(true);
                        mEtShopName.setFocusableInTouchMode(true);
                        mEtShopName.requestFocus();
                        return;
                    } else {
                        if (StringUtils.isMerchantName(mEtShopName.getText().toString().trim())) {
                            map.put("merchantName", mEtShopName.getText().toString().trim());
                        } else {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.error_merchant_short));
                            mEtShopName.setFocusable(true);
                            mEtShopName.setFocusableInTouchMode(true);
                            mEtShopName.requestFocus();
                            return;
                        }
                    }
                    if (mLlShortName.getVisibility() == View.VISIBLE) {
                        if (StringUtils.isEmptyOrNull(mEtShortName.getText().toString().trim())) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.error_merchant_short_empty));
                            mEtShortName.setFocusable(true);
                            mEtShortName.setFocusableInTouchMode(true);
                            mEtShortName.requestFocus();
                            return;
                        } else {
                            if (StringUtils.isShortName(mEtShortName.getText().toString().trim())) {
                                map.put("merchantShortName", mEtShortName.getText().toString().trim());
                            } else {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.error_merchant_short));
                                mEtShortName.setFocusable(true);
                                mEtShortName.setFocusableInTouchMode(true);
                                mEtShortName.requestFocus();
                                return;
                            }
                        }
                    }*//*

                    if (TextUtils.isEmpty(mEtAddress.getText().toString().trim())) {
                        ToastUtil.showToastShort(UIUtils.getString(R.string.error_merchant_address_empty));
                        mEtAddress.setFocusable(true);
                        mEtAddress.setFocusableInTouchMode(true);
                        mEtAddress.requestFocus();
                        return;
                    } else {
                        map.put("address", mEtAddress.getText().toString().trim());
                    }
                    map.put("merchantId", mData.getMerchantId());
                    */
/*map.put("principal", mData.getPrincipal());
                    map.put("idCode", mData.getIdCode());*//*

                    map.put("industryId", mData.getIndustryId());
                    map.put("province", mData.getProvince());
                    map.put("city", mData.getCity());
                    map.put("county", mData.getCounty());
                    map.put("idCardPhotos", mData.getIdCardPhotos());
                    map.put("companyPhoto", mData.getCompanyPhoto());
                    map.put("licenseCode", mData.getLicenseCode());
                    map.put("accountType", mData.getAccountType());
                    map.put("accountCode", mData.getAccountCode());
                    map.put("bankId", mData.getBankId());
                    map.put("bprovince", mData.getBprovince());
                    map.put("bcity", mData.getBcity());
                    map.put("bankBranchId", mData.getBankBranchId());
                    map.put("bankName", mData.getBankBranchIdCnt());
                    map.put("contactLine", mData.getContactLine());

                    if (!TextUtils.isEmpty(mData.getLicensePhoto())) {
                        map.put("licensePhoto", mData.getLicensePhoto());
                    }
                    if (!TextUtils.isEmpty(mData.getBkLicensePhoto())) {
                        map.put("bkLicensePhoto", mData.getBkLicensePhoto());
                    }
                    if (!TextUtils.isEmpty(mData.getBkCardPhoto())) {
                        map.put("bkCardPhoto", mData.getBkCardPhoto());
                    }
                    if (!TextUtils.isEmpty(mData.getHandIdcardPhoto())) {
                        map.put("handIdcardPhoto", mData.getHandIdcardPhoto());
                    }
                    DialogUtil.safeShowDialog(mSubmitDialog);
                    ServerClient.newInstance(MyApplication.getContext()).submitMerchantInfo(MyApplication.getContext(), Constants.TAG_SUBMIT_MERCHANT_INFO, map);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                }
                break;
            case R.id.keeper_name_delete:
                mEtKeeperName.setText("");
                break;
            case R.id.card_id_delete:
                mEtCardId.setText("");
                break;
            case R.id.iv_shop_city_delete:
                mEtAddress.setText("");
                break;
            case R.id.shop_name_delete:
                mEtShopName.setText("");
                break;
            case R.id.short_name_delete:
                mEtShortName.setText("");
                break;
            case R.id.iv_remark_delete:
                mEtRemark.setText("");
                break;
            case R.id.iv_email_delete:
                mEtEmail.setText("");
                break;
            case R.id.iv_service_phone_delete:
                mEtServicePhone.setText("");
                break;
            case R.id.iv_shop_city_location:
                location();
                break;
            default:
                break;
        }
    }

    private void location() {
        LocationUtil locationUtil = new LocationUtil();
        locationUtil.setOnLocationListener(new LocationUtil.OnLocationListener() {
            @Override
            public void location(double lat, double lng) {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    Map<String, Object> map = new HashMap();
                    map.put("longitude", lng);
                    map.put("dimensionality", lat);
                    ServerClient.newInstance(MyApplication.getContext()).questLocation(MyApplication.getContext(), Constants.TAG_QUEST_LOCATION, map);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                }
            }
        });
        locationUtil.getCNBylocation(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_UPLOAD_IMAGE && resultCode == RESULT_OK) {
            mTvLoadResult.setText("上传成功");
            Bundle extras = data.getExtras();
            UploadImageBean.DataEntity dataEntity = (UploadImageBean.DataEntity) extras.getSerializable(Constants.RESULT_UPLOAD_IMAGE_INTENT);
            if (!TextUtils.isEmpty(dataEntity.getStore())) {
                mData.setCompanyPhoto(dataEntity.getStore());
            }
            if (!TextUtils.isEmpty(dataEntity.getLicense())) {
                mData.setLicensePhoto(dataEntity.getLicense());
            }
            if (!TextUtils.isEmpty(dataEntity.getBkCardPhoto())) {
                mData.setBkCardPhoto(dataEntity.getBkCardPhoto());
            }
            if (!TextUtils.isEmpty(dataEntity.getHandIdcardPhoto())) {
                mData.setHandIdcardPhoto(dataEntity.getHandIdcardPhoto());
            }
            if (!TextUtils.isEmpty(dataEntity.getBkLicensePhoto())) {
                mData.setBkLicensePhoto(dataEntity.getBkLicensePhoto());
            }
            if (!TextUtils.isEmpty(dataEntity.getPicfilezm()) && !TextUtils.isEmpty(dataEntity.getPicfilefm())) {
                mData.setIdCardPhotos(dataEntity.getPicfilezm() + "," + dataEntity.getPicfilefm());
            } else if (!TextUtils.isEmpty(dataEntity.getPicfilezm())) {
                if (!TextUtils.isEmpty(mData.getIdCardPhotos())) {
                    String[] split = mData.getIdCardPhotos().split(",");
                    if (split != null && split.length > 0) {
                        if (split.length == 2) {
                            mData.setIdCardPhotos(dataEntity.getPicfilezm() + "," + split[1]);
                        }
                    }
                }
            } else if (!TextUtils.isEmpty(dataEntity.getPicfilefm())) {
                if (!TextUtils.isEmpty(mData.getIdCardPhotos())) {
                    String[] split = mData.getIdCardPhotos().split(",");
                    if (split != null && split.length > 0) {
                        if (split.length == 2) {
                            mData.setIdCardPhotos(split[0] + "," + dataEntity.getPicfilefm());
                        }
                    }
                }
            }
        }
        if (requestCode == Constants.REQUEST_MERCHANT_CARD && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            CardDetailBean.DataEntity dataEntity = (CardDetailBean.DataEntity) extras.getSerializable(Constants.RESULT_MERCHANT_CARD_INTENT);
            setDataEntity(dataEntity, mData);
            setCardEntity(mCardData, mData);
        }
    }

    private void showIndustryDiaog(IndustryBean industryBean) {
        if (industryDialog != null) {
            DialogHelper.resizeHeight(this, industryDialog, 0.6f);
            industryDialog.show();
        } else {
            industryDialog = new BottomDialog(this, industryBean);
            industryDialog.setOnAddressSelectedListener(this);
            industryDialog.setDialogDismisListener(this);
            industryDialog.setTextSize(16);//设置字体的大小
            industryDialog.setIndicatorBackgroundColor(R.color.theme_blue);//设置指示器的颜色
            industryDialog.setTextSelectedColor(R.color.theme_blue);//设置字体获得焦点的颜色
            industryDialog.setTextUnSelectedColor(R.color.tv_unchecked_color);//设置字体没有获得焦点的颜色
            DialogHelper.resizeHeight(this, industryDialog, 0.6f);
            industryDialog.show();
        }
    }

    private void area(AddressBean addressBean) {
        mCityPicker = new ChoseCityPicker(MerchantActivity.this, addressBean);
        mCityPicker.setOnGetAddress(new ChoseCityPicker.OnGetAddress() {
            @Override
            public void getAddress(String province, String city, String area) {
                //获取省市区地址
                mTvArea.setText(province + " " + city + " " + area);
            }
        });
        mCityPicker.setOnGetAddressCode(new ChoseCityPicker.OnGetAddressCode() {
            @Override
            public void getAddressCode(String province, String city, String area) {
                //获取省市区code
                mData.setProvince(province);
                mData.setCity(city);
                mData.setCounty(area);
            }
        });
        mCityPicker.show();
    }

    @Override
    public void onAddressSelected(IndustryBean.Industry1Entity industry1, IndustryBean.Industry1Entity.Industry2Entity industry2, IndustryBean.Industry1Entity.Industry2Entity.Industry3Entity industry3) {
        if (industryDialog != null) {
            industryDialog.dismiss();
        }
        if (industry3 != null) {
            mTvIndustry.setText(industry3.getIndustryName());
            mData.setIndustryId(industry3.getIndustryId());
        } else if (industry2 != null) {
            mTvIndustry.setText(industry2.getIndustryName());
            mData.setIndustryId(industry2.getIndustryId());
        } else if (industry1 != null) {
            mTvIndustry.setText(industry1.getIndustryName());
            mData.setIndustryId(industry1.getIndustryId());
        }
    }

    @Override
    public void dialogclose() {
        if (industryDialog != null) {
            industryDialog.dismiss();
        }
    }

    */
/**
     * 通过审核状态设置UI
     *//*

    public void setView(MerchantInfoBean.DataEntity dataEntity) {
        mEtKeeperName.setText(dataEntity.getPrincipal());
        if (dataEntity.getExamineStatus()==0 || dataEntity.getExamineStatus()==2){
            mEtCardId.setText(dataEntity.getIdCode());
        }else {
            mEtCardId.setText(StringUtils.hideCardID(dataEntity.getIdCode()));
        }
        mEtShopName.setText(dataEntity.getMerchantName());
        mTvShopId.setText(dataEntity.getMerchantId());
        mTvIndustry.setText(dataEntity.getIndustryName());
        mTvArea.setText(dataEntity.getProvinceName() + " " + dataEntity.getCityName() + " " + dataEntity.getCountyName());
        mEtAddress.setText(dataEntity.getAddress());
        if (dataEntity.getMerchantClass() == 2) {
            mLlShortName.setVisibility(View.GONE);
        } else if (dataEntity.getMerchantClass() == 1) {
            mLlShortName.setVisibility(View.VISIBLE);
            mTvCardIdTitle.setText(getText(R.string.company_card_id));
            mTvKeeperNameTitle.setText(getText(R.string.company_keeper_name));
        }
        mEtShortName.setText(dataEntity.getMerchantShortName());
        mEtEmail.setText(dataEntity.getEmail());
        mEtServicePhone.setText(dataEntity.getServicePhone());
        mEtRemark.setText(dataEntity.getRemark());
        if (dataEntity.getExamineStatus() != 2 && TextUtils.isEmpty(dataEntity.getRemark())){
            mLlRemark.setVisibility(View.GONE);
        } else {
            mLlRemark.setVisibility(View.VISIBLE);
        }
        if (dataEntity.getActivateStatus() == 2) {
            propValue = "已冻结";
            setFreezeView(dataEntity.getActivateRemark());
        } else {
            setTopView(dataEntity.getExamineStatus(), dataEntity.getExamineRemark());
        }
        mIvKeeperName.setVisibility(View.GONE);
        mIvCardId.setVisibility(View.GONE);
        mIvAddress.setVisibility(View.GONE);
        mIvShopName.setVisibility(View.GONE);
        mIvShortName.setVisibility(View.GONE);
        mIvRemark.setVisibility(View.GONE);
        mIvEmail.setVisibility(View.GONE);
        mIvServicePhone.setVisibility(View.GONE);
        if (dataEntity.getMchMemeberStatus() == 0) {
            mRlOpenVip.setVisibility(View.GONE);
        } else {
            mRlOpenVip.setVisibility(View.VISIBLE);
            memberCheckStatus = dataEntity.getMchMemeberStatus();
            if (dataEntity.getMchMemeberStatus() == 1) {
                mTvVipTitle.setText(getString(R.string.open_vip));
            } else {
                mTvVipTitle.setText(getString(R.string.get_vip));
            }
        }
    }

    private void setTopView(int status, String remark) {
        switch (status) {
            case CHECKING:
                propValue = "审核中";
                mIvStatus.setImageDrawable(UIUtils.getResource().getDrawable(R.mipmap.icon_checking));
                mTvNotice.setText(UIUtils.getString(R.string.tx_checking));
                mTvReason.setVisibility(View.GONE);
                mRlSubmitButton.setVisibility(View.GONE);
                mIvIndustry.setVisibility(View.GONE);
                mIvArea.setVisibility(View.GONE);
                mRlIndustry.setEnabled(false);
                */
/*mEtAddress.setEnabled(false);
                mEtKeeperName.setEnabled(false);
                mEtCardId.setEnabled(false);
                mEtShopName.setEnabled(false);
                mEtShortName.setEnabled(false);*//*

                mIvArea.setEnabled(false);
                mRlArea.setEnabled(false);
                setTextColor(R.color.tv_rb_color, false, mEtKeeperName, mEtCardId, mEtAddress, mEtShopName, mEtShortName,mEtRemark,mEtEmail,mEtServicePhone);
                mTvArea.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
                mTvIndustry.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
                break;
            case CHECK_PASSED:
                propValue = "审核成功";
                mIvStatus.setImageDrawable(UIUtils.getResource().getDrawable(R.mipmap.icon_check_successful));
                mTvNotice.setText(UIUtils.getString(R.string.tx_check_passed));
                mRlCardPhoto.setVisibility(View.GONE);
                mRlBankCard.setVisibility(View.GONE);
                mTvReason.setVisibility(View.GONE);
                mRlSubmitButton.setVisibility(View.GONE);
                mIvIndustry.setVisibility(View.GONE);
                mIvArea.setVisibility(View.GONE);
                mRlIndustry.setEnabled(false);
                */
/*mEtKeeperName.setEnabled(false);
                mEtCardId.setEnabled(false);
                mEtAddress.setEnabled(false);
                mEtShopName.setEnabled(false);
                mEtShortName.setEnabled(false);*//*

                mRlArea.setEnabled(false);
                mIvArea.setEnabled(false);
                setTextColor(R.color.tv_rb_color, false, mEtKeeperName, mEtCardId, mEtAddress, mEtShopName, mEtShortName,mEtRemark,mEtEmail,mEtServicePhone);
                mTvArea.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
                mTvIndustry.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
                break;
            case CHECK_FAILED:
                propValue = "审核失败";
                mIvStatus.setImageDrawable(UIUtils.getResource().getDrawable(R.mipmap.icon_check_failed));
                mTvReason.setVisibility(View.VISIBLE);
                mBtnEnsure.setVisibility(View.VISIBLE);
                mRlSubmitButton.setVisibility(View.VISIBLE);
                mTvReason.setText(remark);
                mTvNotice.setText(UIUtils.getString(R.string.txt_merchant_info_notice));
                //mTvResult.setText(UIUtils.getString(R.string.txt_merchant_info_result));
                mRlIndustry.setEnabled(true);
                */
/*mEtKeeperName.setEnabled(true);
                mEtCardId.setEnabled(true);
                mEtAddress.setEnabled(true);
                mEtShopName.setEnabled(true);
                mEtShortName.setEnabled(true);*//*

                mIvArea.setEnabled(true);
                mRlArea.setEnabled(true);
                setTextColor(R.color.tv_right_color, true, mEtKeeperName, mEtCardId, mEtAddress, mEtShopName, mEtShortName,mEtRemark,mEtEmail,mEtServicePhone);
                mTvArea.setTextColor(UIUtils.getColor(R.color.tv_right_color));
                mTvIndustry.setTextColor(UIUtils.getColor(R.color.tv_right_color));
                break;
        }
    }

    public void setFreezeView(String activateRemark) {
        mIvStatus.setImageDrawable(UIUtils.getResource().getDrawable(R.mipmap.icon_check_freeze));
        mRlCardPhoto.setVisibility(View.GONE);
        mTvReason.setVisibility(View.VISIBLE);
        mTvReason.setText(activateRemark);
        mRlSubmitButton.setVisibility(View.GONE);
        mIvIndustry.setVisibility(View.GONE);
        mIvArea.setVisibility(View.GONE);
        mTvNotice.setText(UIUtils.getString(R.string.freeze_merchant_info_notice));
        mRlIndustry.setEnabled(false);
        */
/*mEtKeeperName.setEnabled(false);
        mEtCardId.setEnabled(false);
        mEtAddress.setEnabled(false);
        mEtShopName.setEnabled(false);
        mEtShortName.setEnabled(false);*//*

        mRlArea.setEnabled(false);
        mIvArea.setEnabled(false);
        setTextColor(R.color.tv_rb_color, false, mEtKeeperName, mEtCardId, mEtAddress, mEtShopName, mEtShortName,mEtRemark,mEtEmail,mEtServicePhone);
        mTvArea.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
        mTvIndustry.setTextColor(UIUtils.getColor(R.color.tv_rb_color));
    }

    private void setEdittextListener(final EditText editText, final ImageView imageView) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    imageView.setVisibility(View.VISIBLE);
                } else {
                    imageView.setVisibility(View.INVISIBLE);
                }
                LogUtil.d("aaa=="+hasEmpty());
                if (hasEmpty()) {
                    mBtnUnsure.setVisibility(View.VISIBLE);
                    mShadowtButton.setVisibility(View.GONE);
                } else {
                    mBtnUnsure.setVisibility(View.GONE);
                    mShadowtButton.setVisibility(View.VISIBLE);
                }
            }
        });
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {//获得焦点
                    if (editText.getText().length() > 0) {
                        imageView.setVisibility(View.VISIBLE);
                    } else {
                        imageView.setVisibility(View.INVISIBLE);
                    }
                } else {//失去焦点
                    imageView.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void setTextColor(int resId, boolean enable, EditText... ets) {
        for (EditText et : ets) {
            et.setTextColor(UIUtils.getColor(resId));
            et.setEnabled(enable);
        }
    }

    private void call(String phone) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (ActivityNotFoundException a) {
            a.getMessage();
        }
    }

    private boolean hasEmpty() {

        if (TextUtils.isEmpty(mEtAddress.getText().toString().trim())
                || TextUtils.isEmpty(mEtKeeperName.getText().toString().trim())
                || TextUtils.isEmpty(mEtCardId.getText().toString().trim())
                || TextUtils.isEmpty(mEtShopName.getText().toString().trim())
                || TextUtils.isEmpty(mEtShortName.getText().toString().trim())
                || TextUtils.isEmpty(mEtEmail.getText().toString().trim())
                || TextUtils.isEmpty(mEtServicePhone.getText().toString().trim())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    public void mtaProperties(Activity activity, String number, String key, String value) {
        Properties prop = new Properties();
        prop.setProperty(key, value);
        StatService.trackCustomKVEvent(activity, number, prop);
    }

    private void setCardEntity(CardDetailBean.DataEntity cardEntity, MerchantInfoBean.DataEntity data) {
        cardEntity.setAccountType(data.getAccountType());
        cardEntity.setAccountCode(data.getAccountCode());
        cardEntity.setAccountName(data.getAccountName());
        cardEntity.setBankBranchId(data.getBankBranchId());
        cardEntity.setBankBranchIdCnt(data.getBankBranchIdCnt());
        cardEntity.setBankId(data.getBankId());
        cardEntity.setBankIdCnt(data.getBankIdCnt());
        cardEntity.setBkCardPhoto(data.getBkCardPhoto());
        cardEntity.setBkLicensePhoto(data.getBkLicensePhoto());
        cardEntity.setCity(data.getBcity());
        cardEntity.setCityCnt(data.getBcityCnt());
        cardEntity.setProvince(data.getBprovince());
        cardEntity.setProvinceCnt(data.getBprovinceCnt());
        cardEntity.setContactLine(data.getContactLine());
        cardEntity.setIdCard(data.getIdCard());
        cardEntity.setMerchantClass(data.getMerchantClass());
        cardEntity.setCardType(1);
        cardEntity.setMerchantExamineStatus(data.getExamineStatus());
    }

    private void setDataEntity(CardDetailBean.DataEntity cardEntity, MerchantInfoBean.DataEntity data) {
        data.setAccountType(cardEntity.getAccountType());
        data.setAccountCode(cardEntity.getAccountCode());
        data.setAccountName(cardEntity.getAccountName());
        data.setBankBranchId(cardEntity.getBankBranchId());
        data.setBankBranchIdCnt(cardEntity.getBankBranchIdCnt());
        data.setBankId(cardEntity.getBankId());
        data.setBankIdCnt(cardEntity.getBankIdCnt());
        data.setBkCardPhoto(cardEntity.getBkCardPhoto());
        data.setBkLicensePhoto(cardEntity.getBkLicensePhoto());
        data.setBcity(cardEntity.getCity());
        data.setBcityCnt(cardEntity.getCityCnt());
        data.setBprovince(cardEntity.getProvince());
        data.setBprovinceCnt(cardEntity.getProvinceCnt());
        data.setContactLine(cardEntity.getContactLine());
        data.setIdCard(cardEntity.getIdCard());
        data.setMerchantClass(cardEntity.getMerchantClass());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        String intentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        if (!TextUtils.isEmpty(intentName) && Constants.SUBMIT_CHECKING.equals(intentName)) {
            memberCheckStatus = 2;
            mTvVipTitle.setText(getString(R.string.get_vip));
        }
    }
}*/
