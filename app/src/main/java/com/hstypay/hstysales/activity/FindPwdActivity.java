package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.InfoBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.EditTextWatcher;
import com.hstypay.hstysales.widget.NoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.ShadowLayout;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.hstypay.hstysales.R.id.et_id;

/**
 * Created by admin on 2017/7/4.
 */

public class FindPwdActivity extends BaseActivity implements View.OnClickListener {

    private ImageView iv_back;
    private EditText mEtCode;
    private TextView mTvGetCode;
    private Button mBtnEnable,mBtnUnable;
    private ImageView mIvClean;
    private LinearLayout ly_title;
    private EditText mEtTelephone;
    private TimeCount time;
    private TextView mTvTitle;
    private TextView mTvVoiceButton;
    private NoticeDialog mNoticeDialog;
    private static final int GET_MESSAGE_CODE = 1;
    private static final int GET_VOICE_CODE = 2;
    private ShadowLayout mShadowButton;
    private SafeDialog mLoadDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pwd);
        StatusBarUtil.setTranslucentStatus(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
    }

    private void initListener() {
        iv_back.setOnClickListener(this);
        mIvClean.setOnClickListener(this);
        mTvGetCode.setOnClickListener(this);
        mBtnEnable.setOnClickListener(this);
        mTvVoiceButton.setOnClickListener(this);
    }

    private void initView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mTvVoiceButton = (TextView) findViewById(R.id.tv_phone_verify);
        ly_title = (LinearLayout) findViewById(R.id.ly_title);
        mIvClean = (ImageView) findViewById(R.id.iv_clean_input);
        mEtTelephone = (EditText) findViewById(et_id);//手机号
        mEtCode = (EditText) findViewById(R.id.et_code);
        mTvGetCode = (TextView) findViewById(R.id.tv_get_code);
        mShadowButton = (ShadowLayout) findViewById(R.id.shadow_button);
        mBtnEnable = (Button) findViewById(R.id.btn_common_enable);
        mBtnUnable = (Button) findViewById(R.id.btn_common_unable);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);

        EditTextWatcher editTextWatcher = new EditTextWatcher();

        editTextWatcher.setOnTextChanaged(new EditTextWatcher.OnTextChanged() {

            @Override
            public void onExecute(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void onAfterTextChanged(Editable s) {
                if (mEtTelephone.isFocused()) {
                    if (mEtTelephone.getText().toString().length() > 0) {
                        mIvClean.setVisibility(View.VISIBLE);
                    } else {
                        mIvClean.setVisibility(View.GONE);
                    }
                }

                if (!StringUtils.isEmptyOrNull(mEtTelephone.getText().toString())
                        && !StringUtils.isEmptyOrNull(mEtCode.getText().toString())) {
                    mShadowButton.setVisibility(View.VISIBLE);
                    mBtnUnable.setVisibility(View.GONE);
                } else {
                    mShadowButton.setVisibility(View.GONE);
                    mBtnUnable.setVisibility(View.VISIBLE);
                }

            }
        });
        mEtCode.addTextChangedListener(editTextWatcher);
        mEtTelephone.addTextChangedListener(editTextWatcher);
        mEtCode.setOnFocusChangeListener(listener);
        mEtTelephone.setOnFocusChangeListener(listener);
    }

    private void initData() {
        mTvTitle.setText(UIUtils.getString(R.string.title_tel_verify));
        //如果是商户进来
        ly_title.setVisibility(View.VISIBLE);
    }

    private final View.OnFocusChangeListener listener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case et_id:
                    if (hasFocus) {
                        if (mEtTelephone.getText().toString().length() > 0) {
                            mIvClean.setVisibility(View.VISIBLE);
                        } else {
                            mIvClean.setVisibility(View.GONE);
                        }
                    } else {
                        mIvClean.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_clean_input:
                mEtTelephone.setText("");
                mIvClean.setVisibility(View.GONE);
                break;
            case R.id.tv_get_code:
                //获取验证码
                getCode(GET_MESSAGE_CODE);
                break;
            //下一步
            case R.id.btn_common_enable:
                forgetPwd();
                break;
            case R.id.tv_phone_verify:
                getCode(GET_VOICE_CODE);
                break;
        }
    }

    private void showDialog() {
        if (mNoticeDialog == null) {
            mNoticeDialog = new NoticeDialog(FindPwdActivity.this, null, null, R.layout.notice_dialog_voice);
            mNoticeDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    mNoticeDialog.dismiss();
                }
            });
        }
        mNoticeDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mNoticeDialog != null) {
            mNoticeDialog.dismiss();
            mNoticeDialog = null;
        }
        DialogUtil.safeCloseDialog(mLoadDialog);
    }

    //Eventbus接收数据,验证手机号
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_SEND_PHONE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.SEND_PHONE_FALSE:
                    if (msg!=null && msg.getError()!=null) {
                        ToastUtil.showToastShort(msg.getError().getMessage());
                    }
                    break;
                case Constants.SEND_PHONE_TRUE://验证码返回成功
                    loadGetCode();
                    break;
            }
        }
        if(event.getTag().equals(Constants.TAG_GET_VOICE)){
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_VOICE_FALSE:
                    if (msg!=null && msg.getError()!=null) {
                        ToastUtil.showToastShort(msg.getError().getMessage());
                    }
                    break;
                case Constants.GET_VOICE_TRUE:
                    showDialog();
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_CHECK_CODE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InfoBean msg = (InfoBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.CHECK_CODE_FALSE:
                    if (msg!=null && msg.getError()!=null) {
                        ToastUtil.showToastShort(msg.getError().getMessage());
                    }
                    break;
                case Constants.CHECK_CODE_TRUE:
                    Intent intent = new Intent(FindPwdActivity.this, ResetPwdActivity.class);
                    intent.putExtra(Constants.INTENT_RESET_PWD_TELPHONE, mEtTelephone.getText().toString().trim());
                    startActivity(intent);
                    break;
            }
        }
    }

    private void forgetPwd() {
        if (!StringUtils.isEmptyOrNull(mEtTelephone.getText().toString())) {
            if (mEtTelephone.getText().toString().length() != 11) {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_tel_length));
                return;
            }
        }
        if (!StringUtils.isEmptyOrNull(mEtCode.getText().toString())) {
            if (mEtCode.getText().toString().length() != 6) {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_code_length));
                return;
            }
        }

        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).checkCode(MyApplication.getContext(), Constants.TAG_CHECK_CODE, mEtTelephone.getText().toString(), mEtCode.getText().toString().trim());
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }


    }

    private void getCode(int type) {
        if (!StringUtils.isEmptyOrNull(mEtTelephone.getText().toString())) {
            if (mEtTelephone.getText().toString().length() != 11) {
                ToastUtil.showToastShort(UIUtils.getString(R.string.error_tel_length));
                return;
            }
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_tel_empty));
            return;
        }

        if (type == GET_MESSAGE_CODE){
            getMessageCode();
        }else if(type == GET_VOICE_CODE){
            getVoiceCode();
        }

    }

    private void getMessageCode(){
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).getCode(MyApplication.getContext(), Constants.TAG_SEND_PHONE, mEtTelephone.getText().toString().trim());
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getVoiceCode(){
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).getVoiceCode(MyApplication.getContext(), Constants.TAG_GET_VOICE, mEtTelephone.getText().toString().trim());
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void loadGetCode() {
        Countdown();
    }

    /**
     * 验证码倒计时60秒
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void Countdown() {
        // 总共60 没间隔1秒
        time = new TimeCount(60000, 1000);
        time.start();

    }

    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {//计时完毕时触发
            mTvGetCode.setEnabled(true);
            mTvGetCode.setClickable(true);
            mTvGetCode.setText(R.string.reacquire_verify_code);
            mTvGetCode.setTextColor(getResources().getColor(R.color.rb_rank_days_text));
        }

        @Override
        public void onTick(long millisUntilFinished) {//计时过程显示
            mTvGetCode.setEnabled(false);
            mTvGetCode.setClickable(false);
            mTvGetCode.setTextColor(getResources().getColor(R.color.user_edit_color));
            mTvGetCode.setText(getString(R.string.verify_code_count_down)+millisUntilFinished / 1000 + "s");
            //mTvGetCode.setText(millisUntilFinished / 1000 + "s后" + getString(R.string.reacquire_verify_code));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }
}
