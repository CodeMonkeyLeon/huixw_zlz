package com.hstypay.hstysales.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hstypay.hstysales.BuildConfig;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.ConfigBean;
import com.hstypay.hstysales.bean.InvitationCodeBean;
import com.hstypay.hstysales.bean.LocationBean;
import com.hstypay.hstysales.bean.ProgressBean;
import com.hstypay.hstysales.bean.UpdateVersionBean;
import com.hstypay.hstysales.fragment.HomeFragment;
import com.hstypay.hstysales.fragment.MerchantFragment;
import com.hstypay.hstysales.fragment.RankFragment;
import com.hstypay.hstysales.fragment.UserFragment;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.service.DownLoadService;
import com.hstypay.hstysales.service.live.ForegroundLiveService;
import com.hstypay.hstysales.service.live.JobSchedulerManager;
import com.hstypay.hstysales.service.live.PlayerMusicService;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.ConfigUtil;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LocationUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.SpStayUtil;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.utils.VersionUtils;
import com.hstypay.hstysales.widget.CommonConfirmDialog;
import com.hstypay.hstysales.widget.CommonNoticeDialog;
import com.hstypay.hstysales.widget.NoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.SelectCommonDialog;
import com.hstypay.hstysales.widget.SelectDialog;
import com.igexin.sdk.PushManager;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {

    private RadioGroup mRgBottomNav1, mRgBottomNav2;
    //private NoScrollLazyViewPager mVpMain;
    private RadioButton mRbHome, mRbRank, mRbMerchant, mRbUser;
    private FragmentManager mManager;
    //private HashMap<Integer, HolderFragment> mFragments = new HashMap<>();
    private Boolean changeGroup = false;
    private LinearLayout mLlBottom, mLlRegister;
    private HomeFragment mHomeFragment;
    private RankFragment mRankFragment;
    private MerchantFragment mMerchantFragment;
    private UserFragment mUserFragment;
    private SafeDialog mLoadDialog;
    private Fragment[] mFragments;
    private int mIndex = 0;
    private SelectCommonDialog mSelectDialog;
    private Button mBtnRegister;
    private UpdateVersionBean.DataBean mUpdateVersion;
    private boolean isWifiType;
    private CommonConfirmDialog mCommonConfirmDialog;
    private Uri apkurl;
    private ImageView mIvUser;
    private String[] permissionArray = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA
            , Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.ACCESS_NETWORK_STATE
            , Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
    };
    private String[] permissionArrayDownload = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private int mVersionCode;
    private Intent mService;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 101;
    private boolean canDraw;
    private AppOpsManager.OnOpChangedListener onOpChangedListener;
    private SelectCommonDialog mDialogFloat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyApplication.getInstance().addActivity(this);
        initView();
        initListener();
        initData();
        PermissionUtils.checkPermissionArray(this, permissionArray, PermissionUtils.PERMISSION_REQUEST_CODE);
        //LogUtil.d("版本升级--1" + SpUtil.getBoolean(MyApplication.getContext(), Constants.UPDATE_VERSION_CANCEL));

        if (Build.VERSION.SDK_INT >= 23) {
            AppOpsManager opsManager = (AppOpsManager) MyApplication.getContext().getSystemService(Context.APP_OPS_SERVICE);
            canDraw = Settings.canDrawOverlays(MyApplication.getContext());
            onOpChangedListener = new AppOpsManager.OnOpChangedListener() {

                @Override
                public void onOpChanged(String op, String packageName) {
                    String myPackageName = MyApplication.getContext().getPackageName();
                    if (myPackageName.equals(packageName) &&
                            AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW.equals(op)) {
                        canDraw = !canDraw;
                    }
                }
            };
            opsManager.startWatchingMode(AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW,
                    null, onOpChangedListener);
            if (!canDraw) {
                getOverlayDialog(MainActivity.this);
            }
        }
    }

    private void initView() {
        mLlBottom = (LinearLayout) findViewById(R.id.ll_bottom);
        mRgBottomNav1 = (RadioGroup) findViewById(R.id.rg_bottom_nav1);
        mRgBottomNav2 = (RadioGroup) findViewById(R.id.rg_bottom_nav2);
        //mVpMain = (NoScrollLazyViewPager) findViewById(R.id.vp_main);
        mRbHome = (RadioButton) findViewById(R.id.rb_bottom_home);
        mRbRank = (RadioButton) findViewById(R.id.rb_bottom_rank);
        mRbMerchant = (RadioButton) findViewById(R.id.rb_bottom_merchant);
        mRbUser = (RadioButton) findViewById(R.id.rb_bottom_user);
        mBtnRegister = (Button) findViewById(R.id.btn_home_register);
        mLlRegister = findViewById(R.id.ll_register);
        mIvUser = (ImageView) findViewById(R.id.iv_user);
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
    }

    private void initListener() {
        mRgBottomNav1.setOnCheckedChangeListener(this);
        mRgBottomNav2.setOnCheckedChangeListener(this);
        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StatService.trackCustomKVEvent(MainActivity.this, "1026", null);
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    DialogUtil.safeShowDialog(mLoadDialog);
                    Map<String, Object> map = new HashMap<>();
                    map.put("pageSize", 15);
                    map.put("currentPage", 1);
                    ServerClient.newInstance(MyApplication.getContext()).getInvitationcode(MyApplication.getContext(), Constants.TAG_HOME_INVITATIONCODE, map);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                }
            }
        });
    }

    private void initData() {
        initFragment();
        /*mManager = getSupportFragmentManager();
        mNavAdapter = new NavAdapter(mManager, mFragments);
        mVpMain.setAdapter(mNavAdapter);*/

        String loginTelephone = SpUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_TELEPHONE);
        if (TextUtils.isEmpty(loginTelephone)) {
            NoticeDialog noticeDialog = new NoticeDialog(this, UIUtils.getString(R.string.dialog_notice_binding_tel)
                    , UIUtils.getString(R.string.btn_binding_tel), R.layout.notice_dialog_text);
            noticeDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    startActivity(new Intent(MainActivity.this, VerifyActivity.class));
                }
            });
            DialogHelper.resize(MainActivity.this, noticeDialog);
            noticeDialog.show();
        }

        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            collectInfo();
        } else {
            location();
        }

        if (ConfigUtil.updateVersionEnable()) {
            updateVersion();
        }

        getuiMessage();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //引导用户前往通知页面打开允许应用通知开关
            checkNotificationEnabled(false);
        }
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> configMap = null;
            if (!StringUtils.isEmptyOrNull(Constants.ORG_ID)) {
                configMap = new HashMap<>();
                configMap.put("orgId", Constants.ORG_ID);
                configMap.put("orgType", Constants.ORG_TYPE);
            }
            ServerClient.newInstance(MyApplication.getContext()).newConfig(MyApplication.getContext(), Constants.TAG_NEW_CONFIG, configMap);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getuiMessage() {
        //个推初始化和绑定设备
        String clientid = PushManager.getInstance().getClientid(MyApplication.getContext());
        LogUtil.i("Jeremy", "login=client1==" + clientid);
        if (clientid != null) {
            SpUtil.putString(MyApplication.getContext(), Constants.GETUI_CLIENT_ID, clientid);
            Map<String, Object> map = new HashMap<>();
            map.put("client", Constants.REQUEST_CLIENT_APP);
            map.put("pushDeviceId", clientid);
            LogUtil.i("Jeremy", "login=client==" + clientid);
            ServerClient.newInstance(MyApplication.getContext()).addPusher(MyApplication.getContext(), Constants.GETUI_ADD_PUSHER, map);
        }
        PushManager.getInstance().turnOnPush(MyApplication.getContext());
    }

    private void keepAlive() {
        //JobScheduler 大于5.0才可以使用
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            JobSchedulerManager mJobManager = JobSchedulerManager.getJobSchedulerInstance(this);
            mJobManager.startJobScheduler();
        }
        Intent intent = new Intent(this, ForegroundLiveService.class);
        startService(intent);
        Intent liveService = new Intent(this, PlayerMusicService.class);
        startService(liveService);
    }

    private void updateVersion() {
        //版本升级接口
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mVersionCode = VersionUtils.getVersionCode(MyApplication.getContext());
            Map<String, Object> map = new HashMap<>();
            map.put("client", Constants.REQUEST_CLIENT_APP);
            map.put("versionCode", mVersionCode);
            map.put("appChannel", ConfigUtil.getAppChannel());
            map.put("appType", "YWY");
            map.put("currentVersion", VersionUtils.getVersionName(MyApplication.getContext()));
            ServerClient.newInstance(MyApplication.getContext()).updateVersion2(MyApplication.getContext(), Constants.MAIN_UPDATE_VERSION_TAG, map);
        }
    }

    private void initFragment() {
        mHomeFragment = new HomeFragment();
        mRankFragment = new RankFragment();
        mMerchantFragment = new MerchantFragment();
        mUserFragment = new UserFragment();

        //添加到数组
        mFragments = new Fragment[]{mHomeFragment, mRankFragment, mMerchantFragment, mUserFragment};
        //开启事务
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        //添加首页
        ft.add(R.id.content, mHomeFragment).commit();
        //默认设置为第0个
        setIndexSelected(0);
    }

    private void initConfigView() {
        if (!MyApplication.getConfig().contains(Constants.CONFIG_INDEX) && !MyApplication.getConfig().contains(Constants.CONFIG_RANK)) {
            mRgBottomNav1.setVisibility(View.GONE);
        } else {
            mRgBottomNav1.setVisibility(View.VISIBLE);
            if (!MyApplication.getConfig().contains(Constants.CONFIG_INDEX))
                mRbHome.setVisibility(View.GONE);
            else if (!MyApplication.getConfig().contains(Constants.CONFIG_RANK))
                mRbRank.setVisibility(View.GONE);
        }
        if (!MyApplication.getConfig().contains(Constants.CONFIG_MERCHANT))
            mRbMerchant.setVisibility(View.GONE);
        if (!MyApplication.getConfig().contains(Constants.CONFIG_REGISTER)) {
            mBtnRegister.setVisibility(View.GONE);
            mLlRegister.setVisibility(View.GONE);
        }
        mLlBottom.setVisibility(View.VISIBLE);

        if (mHomeFragment != null)
            mHomeFragment.setConfig(MyApplication.getConfig());//刷新fragment，此时的fragment是已经创建完成了的
        if (mUserFragment != null)
            mUserFragment.initConfig();//刷新fragment，此时的fragment是已经创建完成了的
    }

    private void setIndexSelected(int index) {

        if (mIndex == index) {
            return;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();

        //隐藏
        ft.hide(mFragments[mIndex]);
        //判断是否添加
        if (!mFragments[index].isAdded()) {
            ft.add(R.id.content, mFragments[index], mFragments[index].getClass().getSimpleName()).show(mFragments[index]);
        } else {
            ft.show(mFragments[index]);
            if (index == 2) {
                mFragments[index].onResume();
            }
        }

        ft.commit();
        //再次赋值
        mIndex = index;

    }

    @SuppressLint("ResourceType")
    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        //底部导航按钮选中状态切换的时候就切换ViewPager显示的页面
        if (group != null && checkedId > -1 && changeGroup == false) {
            if (group == mRgBottomNav1) {
                changeGroup = true;
                mRgBottomNav2.clearCheck();
                changeGroup = false;
            } else if (group == mRgBottomNav2) {
                changeGroup = true;
                mRgBottomNav1.clearCheck();
                changeGroup = false;
            }
        }
        switch (checkedId) {
            case R.id.rb_bottom_home:
                setIndexSelected(0);
                break;
            case R.id.rb_bottom_rank:
                setIndexSelected(1);
                break;
            case R.id.rb_bottom_merchant:
                setIndexSelected(2);
                break;
            case R.id.rb_bottom_user:
                setIndexSelected(3);
                break;
        }
    }

    private void location() {
        LocationUtil locationUtil = new LocationUtil();
        locationUtil.setOnLocationListener(new LocationUtil.OnLocationListener() {
            @Override
            public void location(double lat, double lng) {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    Map<String, Object> map = new HashMap();
                    map.put("longitude", lng);
                    map.put("dimensionality", lat);
                    ServerClient.newInstance(MyApplication.getContext()).questLocation(MyApplication.getContext(), Constants.TAG_MAIN_QUEST_LOCATION, map);
                }
            }
        });
        locationUtil.getCNBylocation(MyApplication.getContext());
    }

    private void collectInfo() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            map.put("client", Constants.REQUEST_CLIENT_APP);
            map.put("versionCode", AppHelper.getVerCode(MyApplication.getContext()));
            map.put("phoneBrand", Build.BRAND);
            map.put("phoneModel", Build.MODEL);
            map.put("firmwareVersion", Build.VERSION.RELEASE);
            if (!TextUtils.isEmpty(AppHelper.getProvidersName(MyApplication.getContext()))) {
                map.put("telecomOperator", AppHelper.getProvidersName(MyApplication.getContext()));
            }
            map.put("network", NetworkUtils.getNetWorkStatus(MyApplication.getContext()));
            if ("WIFI".equals(NetworkUtils.getNetWorkStatus(MyApplication.getContext()))) {
                map.put("wifiName", NetworkUtils.getConnectWifiSsid(MyApplication.getContext()));
            }
            ServerClient.newInstance(MyApplication.getContext()).collectInfo(MyApplication.getContext(), Constants.TAG_COLLECT_INFO, map);
        }
    }

    private void collectInfo(String provinceCode, String cityCode, String countryCode, String address) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            Map<String, Object> map = new HashMap<>();
            map.put("client", Constants.REQUEST_CLIENT_APP);
            map.put("versionCode", AppHelper.getVerCode(MyApplication.getContext()));
            map.put("privince", provinceCode);
            map.put("city", cityCode);
            map.put("county", countryCode);
            map.put("address", address);
            map.put("phoneBrand", Build.BRAND);
            map.put("phoneModel", Build.MODEL);
            map.put("firmwareVersion", Build.VERSION.RELEASE);
            if (!TextUtils.isEmpty(AppHelper.getProvidersName(MyApplication.getContext()))) {
                map.put("telecomOperator", AppHelper.getProvidersName(MyApplication.getContext()));
            }
            map.put("network", NetworkUtils.getNetWorkStatus(MyApplication.getContext()));
            if ("WIFI".equals(NetworkUtils.getNetWorkStatus(MyApplication.getContext()))) {
                map.put("wifiName", NetworkUtils.getConnectWifiSsid(MyApplication.getContext()));
            }
            ServerClient.newInstance(MyApplication.getContext()).collectInfo(MyApplication.getContext(), Constants.TAG_COLLECT_INFO, map);
        }
    }

    /**
     * 监听返回键
     */
    @Override
    public void onBackPressed() {
        showExitDialog();
    }

    public void showExitDialog() {
        mSelectDialog = new SelectCommonDialog(MainActivity.this
                , UIUtils.getString(R.string.dialog_notice_exit)
                , R.layout.select_common_dialog);
        mSelectDialog.setOnClickOkListener(new SelectCommonDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                MyApplication.getInstance().finishAllActivity();
                SpUtil.removeKey(Constants.SP_NOTICE_CLOSE);
            }
        });
        DialogHelper.resize(MainActivity.this, mSelectDialog);
        mSelectDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mSelectDialog != null) {
            mSelectDialog.dismiss();
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNoticeEvent(NoticeEvent event) {
        //版本更新返回
        if (event.getTag().equals(Constants.MAIN_UPDATE_VERSION_TAG)) {
            UpdateVersionBean msg = (UpdateVersionBean) event.getMsg();
            dismissLoading();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    break;
                case Constants.ON_EVENT_FALSE:
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mUpdateVersion = msg.getData();
                    if (mUpdateVersion != null) {
                        updateVersions(mUpdateVersion);
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.MAIN_UPDATE_PROGRESS_TAG)) {
            switch (event.getCls()) {
                case Constants.UPDATE_DOWNLOAD_SUCCESS:
                    File file = (File) event.getMsg();
                    if (file != null && file.isFile()) {
                        Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), BuildConfig.APPLICATION_ID + ".fileProvider", file);
                        apkurl = contentUri;
                        /*if (isWifiType) {
                            showUpgradeInfoDialog(mUpdateVersion, new ComDialogListener(mUpdateVersion, true), true);
                        } else {*/
//                            installAPK(apkurl, MainActivity.this);
                        openAPKFile();
                        if (mCommonConfirmDialog != null) {
                            mCommonConfirmDialog.setView(true);
                        }
//                        }
                    }
                    break;
                case Constants.UPDATE_DOWNLOAD_FAILED:
//                    if (!isWifiType) {
                    if (mCommonConfirmDialog != null) {
                        mCommonConfirmDialog.setView(true);
//                        }
                        ToastUtil.showToastShort("下载失败，请重新下载！");
                    }
                    break;
                case Constants.UPDATE_DOWNLOADING:
//                    if (!isWifiType) {
                    ProgressBean progressBean = (ProgressBean) event.getMsg();
                    if (mCommonConfirmDialog != null) {
                        mCommonConfirmDialog.setProgress(progressBean);
                    }
//                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_MAIN_QUEST_LOCATION)) {
            LocationBean msg = (LocationBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    collectInfo();
                    break;
                case Constants.QUEST_LOCATION_FALSE:
                    collectInfo();
                    break;
                case Constants.QUEST_LOCATION_TRUE://请求成功
                    if (msg.getData() != null) {
                        LocationBean.DataEntity data = msg.getData();
                        collectInfo(data.getProvinceCode(), data.getCitycode(), data.getCitycode(), data.getTownship());
                    } else {
                        collectInfo();
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_HOME_INVITATIONCODE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            InvitationCodeBean msg = (InvitationCodeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_INVITATIONCODE_FALSE:
                    if (msg.getError() != null) {
                        if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                            if (msg.getError().getMessage() != null) {
                                loginDialog(MainActivity.this, msg.getError().getMessage());
                            }
                        } else {
                            if (msg.getError().getMessage() != null) {
                                ToastUtil.showToastShort(msg.getError().getMessage());
                            }
                        }
                    }
                    break;
                case Constants.GET_INVITATIONCODE_TRUE:
                    boolean hasInviteCode = false;
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        List<InvitationCodeBean.DataEntity.ItemData> data = msg.getData().getData();
                        for (int i = 0; i < data.size(); i++) {
                            if ("正常".equals(data.get(i).getStatusCnt()) && data.get(i).getMaxUseCount() > data.get(i).getUsedCount()) {
                                hasInviteCode = true;
                                break;
                            }
                        }
                    }
                    if (hasInviteCode) {
                        Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                        if ("SUPERADMIN".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_ROLE_CODE))) {
                            if ("".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                                intent.putExtra(Constants.REGISTER_INTENT, Constants.REGISTER_URL + "?" +AppHelper.getHtmlAndString() + "role=provinceService");
                            } else if ("SC".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                                intent.putExtra(Constants.REGISTER_INTENT, Constants.REGISTER_URL  + "?" +AppHelper.getHtmlAndString() + "role=cityService");
                            }
                        } else {
                            intent.putExtra(Constants.REGISTER_INTENT, Constants.REGISTER_URL+ AppHelper.getHtmlString());
                        }
                        startActivity(intent);
                    } else {
                        DialogUtil.noticeDialog(MainActivity.this, UIUtils.getString(R.string.error_has_useful_code));
                    }
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_NEW_CONFIG)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ConfigBean msg = (ConfigBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
//                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (MyApplication.getFreeLogin().equals(msg.getError().getCode())) {
                            if (msg.getError().getMessage() != null) {
                                loginDialog(MainActivity.this, msg.getError().getMessage());
                            }
                        } else if (Constants.ERROR_CODE_CONFIG.equals(msg.getError().getCode())) {
                            SpStayUtil.putString(MyApplication.getContext(), Constants.SP_CONFIG + MyApplication.getUserId(), Constants.CONFIG_ALL);
                            initConfigView();
                        } /*else {
                            if (msg.getError().getMessage() != null) {
                                ToastUtil.showToastShort(msg.getError().getMessage());
                            }
                        }*/
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        Gson gson = new Gson();
                        SpStayUtil.putString(MyApplication.getContext(), Constants.SP_CONFIG + MyApplication.getUserId(), gson.toJson(msg));
                    } else {
                        SpStayUtil.putString(MyApplication.getContext(), Constants.SP_CONFIG + MyApplication.getUserId(), Constants.CONFIG_ALL);
                    }
                    initConfigView();
                    break;
            }
        }
    }

    private void updateVersions(final UpdateVersionBean.DataBean data) {
        if (data != null) {
            if (VersionUtils.getVersionName(MyApplication.getContext()).equals(data.getVersionName())) {
                SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE, false);
                return;
            }
            if (data.getForceUpgrade() != null && (data.getForceUpgrade().intValue() == 0 || data.getForceUpgrade().intValue() == 1)) {
                update(data);
            }
        }
    }

    private void update(UpdateVersionBean.DataBean data) {
        //进行版本升级
        SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE, true);
        String versionName = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_VERSION_NAME);
        mService = new Intent(MainActivity.this, DownLoadService.class);
        mService.putExtra("appurl", data.getAppUrl());
        mService.putExtra("versionName", data.getVersionName());
        if (!versionName.equals(data.getVersionName())) {
            //如何服务器版本大于保存的版本，直接下载最新的版本
            //启动服务
            LogUtil.d("版本升级---aaaa");
            isWifiType = false;
            showUpgradeInfoDialog(data, new ComDialogListener(data), true);
//            }
        } else {
            LogUtil.d("版本升级---bbb");
            String string = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_APP_URL);
            if (!StringUtils.isEmptyOrNull(string)) {//已经下载完成直接安装
                LogUtil.d("版本升级---ccc");
                Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), BuildConfig.APPLICATION_ID + ".fileProvider", new File(string));
                apkurl = contentUri;
                showUpgradeInfoDialog(data, new ComDialogListener(data), true);
            }
        }
    }

    private void installDownloadApk(final UpdateVersionBean.DataBean data) {
        if (data != null) {
            if (data.getForceUpgrade() != null && (data.getForceUpgrade().intValue() == 0 || data.getForceUpgrade().intValue() == 1)) {
                //进行版本升级
                SpUtil.putBoolean(MyApplication.getContext(), Constants.SP_IS_UPDATE, true);
                String versionName = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_VERSION_NAME);
                mService = new Intent(MainActivity.this, DownLoadService.class);
                mService.putExtra("appurl", data.getAppUrl());
                mService.putExtra("versionName", data.getVersionName());
                mService.putExtra(Constants.INTENT_NAME, Constants.MAIN_UPDATE_PROGRESS_TAG);
                if (!versionName.equals(data.getVersionName())) {
                    //如何服务器版本大于保存的版本，直接下载最新的版本
                    //启动服务
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            MainActivity.this.startService(mService);
                        }
                    }).start();
                    if (mCommonConfirmDialog != null) {
                        mCommonConfirmDialog.setView(false);
                    }
                } else {
                    String string = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_APP_URL);
                    if (!StringUtils.isEmptyOrNull(string)) {//已经下载完成直接安装
                        Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), BuildConfig.APPLICATION_ID + ".fileProvider", new File(string));
                        apkurl = contentUri;
                        showUpgradeInfoDialog(data, new ComDialogListener(data), true);
                    } else {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                MainActivity.this.startService(mService);
                            }
                        }).start();
                        if (mCommonConfirmDialog != null) {
                            mCommonConfirmDialog.setView(false);
                        }
                    }
                }
            }
        }

    }

    public void showUpgradeInfoDialog(UpdateVersionBean.DataBean result, CommonConfirmDialog.ConfirmListener listener, boolean isWifi) {
        mCommonConfirmDialog = new CommonConfirmDialog(MainActivity.this, listener, result, isWifi);
        if (!mCommonConfirmDialog.isShowing()) {
            mCommonConfirmDialog.show();
        }
    }

    /**
     * 升级点击事件
     */
    class ComDialogListener implements CommonConfirmDialog.ConfirmListener {
        private UpdateVersionBean.DataBean result;

        public ComDialogListener(UpdateVersionBean.DataBean result) {
            this.result = result;
        }

        @Override
        public void ok() {
            boolean results = PermissionUtils.checkPermissionArray(MainActivity.this, permissionArrayDownload);
            if (results) {
                if (apkurl != null) {
                    openAPKFile();
                } else {
                    installDownloadApk(result);
                }
            } else {
                showNotice(MY_PERMISSIONS_REQUEST_CALL_PHONE, permissionArrayDownload, getString(R.string.permission_content_update));
            }
        }

        @Override
        public void cancel() {
            //SpUtil.putBoolean(MyApplication.getContext(), Constants.UPDATE_VERSION_CANCEL, false);
            if (mService != null) {
                stopService(mService);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (PermissionUtils.verifyPermissions(grantResults)) {
                    installDownloadApk(mUpdateVersion);
                } else {
                    showDialog(getString(R.string.permission_set_content_update));
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }


    public void showRedDot(boolean showDot) {
        mIvUser.setVisibility(showDot ? View.VISIBLE : View.GONE);
    }

    /**
     * 打开安装包
     */
    private void openAPKFile() {
        String filePath = SpStayUtil.getString(MyApplication.getContext(), Constants.UPDATE_APP_URL);
        String mimeDefault = "application/vnd.android.package-archive";
        File apkFile = null;
        if (!TextUtils.isEmpty(filePath) && !TextUtils.isEmpty(Uri.parse(filePath).getPath())) {
            apkFile = new File(filePath);
        }
        if (apkFile == null) {
            return;
        }
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //兼容7.0
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                //这里牵涉到7.0系统中URI读取的变更
                Uri contentUri = FileProvider.getUriForFile(MyApplication.getContext(), BuildConfig.APPLICATION_ID + ".fileProvider", new File(filePath));
                intent.setDataAndType(contentUri, mimeDefault);
                //兼容8.0
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        startInstallPermissionSettingActivity();
                        return;
                    }
                }
            } else {
                intent.setDataAndType(Uri.fromFile(apkFile), mimeDefault);
            }
            if (getPackageManager().queryIntentActivities(intent, 0).size() > 0) {
                //如果APK安装界面存在，携带请求码跳转。使用forResult是为了处理用户 取消 安装的事件。外面这层判断理论上来说可以不要，但是由于国内的定制，这个加上还是比较保险的
                startActivity(intent);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * 跳转到设置-允许安装未知来源-页面
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startInstallPermissionSettingActivity() {
        //后面跟上包名，可以直接跳转到对应APP的未知来源权限设置界面。使用startActivityForResult 是为了在关闭设置界面之后，获取用户的操作结果，然后根据结果做其他处理
        Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, Uri.parse("package:" + getPackageName()));
        startActivityForResult(intent, Constants.REQUEST_INSTALL_PACKAGES);
    }

    /**
     * 功用：未知来源权限弹窗
     * 说明：8.0系统中升级APK时，如果跳转到了 未知来源权限设置界面，并且用户没用允许该权限，会弹出此窗口
     */
    private void showUnKnowResourceDialog() {
        SelectDialog dialog = new SelectDialog(MainActivity.this, "未知来源权限设置界面",
                UIUtils.getString(R.string.btn_cancel_text), UIUtils.getString(R.string.btn_ensure_text)
                , new SelectDialog.HandleBtn() {
            @Override
            public void handleOkBtn() {
                //兼容8.0
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        startInstallPermissionSettingActivity();
                    }
                }
            }

            @Override
            public void handleCancleBtn() {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);//不调用super.onActivityResult()方法会导致Fragment中收不到onActivityResult的回调
        if (requestCode == Constants.REQUEST_INSTALL_PACKAGES) {
            if (resultCode == RESULT_OK) {
                openAPKFile();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    boolean hasInstallPermission = getPackageManager().canRequestPackageInstalls();
                    if (!hasInstallPermission) {
                        showUnKnowResourceDialog();
                    }
                }
            }
        } else if (requestCode == Constants.REQUEST_SYSTEM_OVERLAY_WINDOW) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (onOpChangedListener != null) {
                    AppOpsManager opsManager = (AppOpsManager) MyApplication.getContext().getSystemService(Context.APP_OPS_SERVICE);
                    opsManager.stopWatchingMode(onOpChangedListener);
                    onOpChangedListener = null;
                }
                // The draw overlay permission status can be retrieved from canDraw
                LogUtil.d("canDrawOverlay = {}" + canDraw);
                if (!canDraw) {
                    ToastUtil.showToastShort("允许应用显示悬浮窗才能显示商户审核通知浮窗");
                }
            }
        }
    }


    //引导用户前往通知页面打开允许应用通知开关
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void checkNotificationEnabled(boolean selectDialogCancel) {
        boolean isEnabled = isNotificationEnabled(this);
        LogUtil.i("MainActivity", "is notification enabled: " + isEnabled);
        if (!isEnabled) {
            getNoticeDialog(this, selectDialogCancel);
        } else {
            keepAlive();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public boolean isNotificationEnabled(Context context) {
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
        return notificationManagerCompat.areNotificationsEnabled();
    }


    private boolean isToSetNotice;

    public void getNoticeDialog(Activity context, boolean cancel) {
        SelectDialog mDialogFloat = new SelectDialog(context, "请允许应用显示通知,接收推送通知时需用到该权限", UIUtils.getString(R.string.btn_setting), cancel ? UIUtils.getString(R.string.btn_cancel_text) : null, new SelectDialog.HandleBtn() {
            @Override
            public void handleOkBtn() {
                Intent intent = new Intent();
                if (Build.VERSION.SDK_INT >= 26) {
                    // android 8.0引导设置通知开启
                    intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                    intent.putExtra("android.provider.extra.APP_PACKAGE", getPackageName());
                } else if (Build.VERSION.SDK_INT >= 21) {
                    // android 5.0-7.0引导设置通知开启
                    intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                    intent.putExtra("app_package", getPackageName());
                    intent.putExtra("app_uid", getApplicationInfo().uid);
                } else {
                    // 其他
                    intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                    intent.setData(Uri.fromParts("package", getPackageName(), null));
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                isToSetNotice = true;
            }

            @Override
            public void handleCancleBtn() {
                ToastUtil.showToastLong("取消后无法显示通知");
            }
        });
        DialogHelper.resize(context, mDialogFloat);
        mDialogFloat.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isToSetNotice) {
            isToSetNotice = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                checkNotificationEnabled(true);
            }
        }
    }

    public void getOverlayDialog(Activity context) {
        if (mDialogFloat == null) {
            mDialogFloat = new SelectCommonDialog(context, "请允许应用显示悬浮窗,商户审核通知需用到该权限", "取消", "去设置", R.layout.select_common_dialog);
            mDialogFloat.setOnClickOkListener(new SelectCommonDialog.OnClickOkListener() {
                @Override
                public void clickOk() {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, Constants.REQUEST_SYSTEM_OVERLAY_WINDOW);
                }
            });
            mDialogFloat.setOnClickCancelListener(new SelectCommonDialog.OnClickCancelListener() {
                @Override
                public void clickCancel() {
                    ToastUtil.showToastShort("取消后无法显示悬浮窗");
                }
            });
            DialogHelper.resize(context, mDialogFloat);
        }
        mDialogFloat.show();
    }
}
