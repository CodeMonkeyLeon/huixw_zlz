package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.Zxing.MaxCardManager;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.DeviceBean;
import com.hstypay.hstysales.bean.Info;
import com.hstypay.hstysales.bean.WxSignDetailBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.ImageUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CommonNoticeDialog;
import com.hstypay.hstysales.widget.CustomViewFullScreenDialog;
import com.hstypay.hstysales.widget.SafeDialog;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

//微信实名认证详情页面
public class WxRealNameAuthDetailActivity extends BaseActivity implements View.OnClickListener {

    private SafeDialog mLoadDialog;
    private TextView mTvTitle;
    private ImageView mIvBack;
    private ImageView mIvAuthTop;
    private Button mBtnSelfSignAuth;
    private TextView mTv_protocol_name_auth;
    private TextView mTv_merchant_name_auth;
    private TextView mTv_merchant_id_auth;
    private TextView mTv_sign_date_auth;
    private TextView mTv_sign_state_auth;
    private TextView TtvTitleMerchant;
    private Button mBtn_self_sign_auth;
    private Button mBtn_cancel_reply_auth;
    private Button mBtn_again_sign_auth;
    private String mMerchantId;
    private WxSignDetailBean.SignDetailData mSignDetailData;
    private String mIntentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wx_real_name_auth_detail);
        StatusBarUtil.setTranslucentStatusWhiteIcon(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initEvent() {
        mIvBack.setOnClickListener(this);
        mBtn_self_sign_auth.setOnClickListener(this);
        mBtn_cancel_reply_auth.setOnClickListener(this);
        mBtn_again_sign_auth.setOnClickListener(this);
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        View rl_title_wx_auth_detail = findViewById(R.id.rl_title_wx_auth_detail);
        rl_title_wx_auth_detail.setBackgroundColor(getResources().getColor(R.color.merchant_sure_commit));
        mTvTitle = findViewById(R.id.tv_title);
        mIvBack = findViewById(R.id.iv_back);
        mIvAuthTop = findViewById(R.id.iv_auth_top);

        mTv_protocol_name_auth = findViewById(R.id.tv_protocol_name_auth);
        mTv_merchant_name_auth = findViewById(R.id.tv_merchant_name_auth);
        mTv_merchant_id_auth = findViewById(R.id.tv_merchant_id_auth);
        mTv_sign_date_auth = findViewById(R.id.tv_sign_date_auth);
        mTv_sign_state_auth = findViewById(R.id.tv_sign_state_auth);
        mBtn_self_sign_auth = findViewById(R.id.btn_self_sign_auth);
        mBtn_cancel_reply_auth = findViewById(R.id.btn_cancel_reply_auth);
        mBtn_again_sign_auth = findViewById(R.id.btn_again_sign_auth);
        TtvTitleMerchant = findViewById(R.id.tv_title_merchant);

    }

    private void initData() {
        mIntentName = getIntent().getStringExtra(Constants.INTENT_NAME);
        mTvTitle.setText("INTENT_ALI_AUTH".equals(mIntentName) ? R.string.ali_real_name_auth : R.string.wx_real_name_auth);
        TtvTitleMerchant.setText("INTENT_ALI_AUTH".equals(mIntentName) ? R.string.tv_title_ali_merchant : R.string.tv_title_wx_merchant);
        mIvAuthTop.setImageResource("INTENT_ALI_AUTH".equals(mIntentName) ? R.mipmap.ic_ali_auth_top : R.mipmap.ic_wx_auth_top);
        mMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        mSignDetailData = (WxSignDetailBean.SignDetailData) getIntent().getSerializableExtra(Constants.INTENT_WX_SIGN_DATA);
        if (mSignDetailData ==null){
            getWxSignDetail(true);
        }else {
            setData();
        }

    }

    private void setData() {
        if (mSignDetailData!=null){
            mTv_protocol_name_auth.setText(mSignDetailData.getContractName());
            mTv_merchant_name_auth.setText(mSignDetailData.getMerchantIdCnt());
            mTv_merchant_id_auth.setText(mSignDetailData.getTradeCode());

            String status = mSignDetailData.getStatus();//签约状态, 0-初始,10-签约成功,20-签约失败, 30-待签约
            if ("10".equals(status) ||"20".equals(status)){
                mTv_sign_date_auth.setText(mSignDetailData.getSignTime());
            }else {
                mTv_sign_date_auth.setText(getResources().getString(R.string.wx_sign_not_auth));
            }

            mTv_sign_state_auth.setText(mSignDetailData.getStatusCnt());
            switch (status){
                case "0"://初始
                    mTv_sign_state_auth.setTextColor(getResources().getColor(R.color.merchant_sure_commit));
                    //当状态为初始/签约成功时，不展示认证按钮。
                    mBtn_self_sign_auth.setVisibility(View.GONE);
                    mBtn_cancel_reply_auth.setVisibility(View.GONE);
                    mBtn_again_sign_auth.setVisibility(View.GONE);
                    break;
                case "10"://成功
                    mTv_sign_state_auth.setTextColor(getResources().getColor(R.color.merchant_sure_commit));
                    //当状态为初始/签约成功时，不展示认证按钮。
                    mBtn_self_sign_auth.setVisibility(View.GONE);
                    mBtn_cancel_reply_auth.setVisibility(View.GONE);
                    mBtn_again_sign_auth.setVisibility(View.GONE);
                    break;
                case "20"://失败
                    mTv_sign_state_auth.setTextColor(getResources().getColor(R.color.color_fd414b));
                    //为签约失败，显示重新签约按钮。
                    mBtn_self_sign_auth.setVisibility(View.GONE);
                    mBtn_cancel_reply_auth.setVisibility(View.GONE);
                    mBtn_again_sign_auth.setVisibility(View.VISIBLE);
                    break;
                case "30"://待签约
                    mTv_sign_state_auth.setTextColor(getResources().getColor(R.color.color_ffa000));
                    //为待签约时，显示人工签约/撤销申请单按钮。
                    mBtn_self_sign_auth.setVisibility(View.VISIBLE);
                    mBtn_cancel_reply_auth.setVisibility(View.VISIBLE);
                    mBtn_again_sign_auth.setVisibility(View.GONE);
                    break;
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back://返回
                onBackPressed();
                break;
            case R.id.btn_self_sign_auth:
                //人工签约
                if ("INTENT_ALI_AUTH".equals(mIntentName)) {
                    if (mSignDetailData != null) {
                        Intent intent = new Intent(WxRealNameAuthDetailActivity.this, RealNameAuthCodeActivity.class);
                        intent.putExtra("INTENT_SIGN_DETAIL", mSignDetailData);
                        startActivity(intent);
                    } else {
                        ToastUtil.showToastLong(getString(R.string.error_data));
                    }
                } else {
                    showWxSignDialog();
                }
                break;
            case R.id.btn_cancel_reply_auth:
                //撤销申请单
                showRemindCancelReplyDialog();
                break;
            case R.id.btn_again_sign_auth:
                //重新签约
                againSignAuth();
                break;
            default:
                break;
        }
    }

    //重新签约
    private void againSignAuth() {
        if (mSignDetailData==null){return;}
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("id", mSignDetailData.getId());
            ServerClient.newInstance(MyApplication.getContext()).wxAuthReSign(MyApplication.getContext(), Constants.TAG_WX_SIGN_AGAIN+getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    //撤销申请单提示
    public void showRemindCancelReplyDialog(){
        CustomViewFullScreenDialog unBindDialog = new CustomViewFullScreenDialog(this);
        unBindDialog.setView(R.layout.dialog_device_unbind_tip);
        Button btn_not_unbind = unBindDialog.findViewById(R.id.btn_not_unbind);
        Button btn_to_unbind = unBindDialog.findViewById(R.id.btn_to_unbind);
        TextView tv_title_unbind_dialog = unBindDialog.findViewById(R.id.tv_title_unbind_dialog);
        TextView tv_content_unbind_dialog = unBindDialog.findViewById(R.id.tv_content_unbind_dialog);
        tv_title_unbind_dialog.setText(getResources().getString(R.string.title_cancel_reply_tip));
        tv_content_unbind_dialog.setText(getResources().getString(R.string.content_cancel_reply_tip));
        unBindDialog.show();
        btn_not_unbind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unBindDialog.dismiss();
            }
        });
        btn_to_unbind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unBindDialog.dismiss();
                cancelReply();
            }
        });
    }

    //撤销申请单
    private void cancelReply() {
        if (mSignDetailData==null){return;}
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("id", mSignDetailData.getId());
            ServerClient.newInstance(MyApplication.getContext()).cancelWxSignReply(MyApplication.getContext(), Constants.TAG_WX_SIGN_CANCEL+getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    //获取微信签约详情信息
    private void getWxSignDetail(boolean isShowLoad) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (isShowLoad){
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("merchantId", mMerchantId);
            if (mSignDetailData != null) {
                map.put("id", mSignDetailData.getId());
            } else {
                map.put("contractType", "INTENT_ALI_AUTH".equals(mIntentName) ? "14" : "5");
            }
            ServerClient.newInstance(MyApplication.getContext()).queryWxSignDetail(MyApplication.getContext(), Constants.TAG_WX_SIGN_DETAIL+getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    //Eventbus接收数据
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_WX_SIGN_DETAIL+getClass().getSimpleName())) {
            //微信签约详情
            DialogUtil.safeCloseDialog(mLoadDialog);
            WxSignDetailBean msg = (WxSignDetailBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(WxRealNameAuthDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                    showCommonNoticeDialog(WxRealNameAuthDetailActivity.this, "提示", msg.getError().getMessage(), new CommonNoticeDialog.OnClickOkListener() {
                                        @Override
                                        public void onClickOk() {
                                            finish();
                                        }
                                    });
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    mSignDetailData = msg.getData();
                    setData();
                    break;
            }
        }

        if (event.getTag().equals(Constants.TAG_WX_SIGN_CANCEL+getClass().getSimpleName())) {
            //撤销申请单
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(WxRealNameAuthDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(WxRealNameAuthDetailActivity.this, "提示", msg.getError().getMessage(), null);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    //撤销成功，状态设为初始
//                    mSignDetailData.setStatus("0");
//                    mSignDetailData.setStatusCnt("初始");
//                    mTv_sign_state_auth.setText(mSignDetailData.getStatusCnt());
//                    setBackData();
                    ToastUtil.showToastShort(getResources().getString(R.string.opt_success));
                    getWxSignDetail(false);
                    setResult(RESULT_OK);
                    break;
            }
        }
        if (event.getTag().equals(Constants.TAG_WX_SIGN_AGAIN+getClass().getSimpleName())) {
            //重新签约
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(WxRealNameAuthDetailActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    showCommonNoticeDialog(WxRealNameAuthDetailActivity.this, "提示", msg.getError().getMessage(), null);
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE://请求成功
                    //重新签约成功，状态设为待签约
//                    mSignDetailData.setStatus("30");
//                    mSignDetailData.setStatusCnt("待签约");
//                    mTv_sign_state_auth.setText(mSignDetailData.getStatusCnt());
//                    setBackData();
                    ToastUtil.showToastShort(getResources().getString(R.string.opt_success));
                    getWxSignDetail(false);
                    setResult(RESULT_OK);
                    break;
            }
        }
    }

    //返回新的数据
    private void setBackData() {
        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_WX_SIGN_DATA,mSignDetailData);
    }

    //人工签约
    private void showWxSignDialog() {
        CustomViewFullScreenDialog wxSignDialog = new CustomViewFullScreenDialog(this);
        wxSignDialog.setView(R.layout.dialog_wx_real_sign);
        ImageView iv_qrcode_wx_sign_dialog = wxSignDialog.findViewById(R.id.iv_qrcode_wx_sign_dialog);
        TextView tv_wx_sign_detail = wxSignDialog.findViewById(R.id.tv_wx_sign_detail);
        Button btn_know_dialog_wx = wxSignDialog.findViewById(R.id.btn_know_dialog_wx);
        wxSignDialog.show();

        btn_know_dialog_wx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wxSignDialog.dismiss();
            }
        });
        tv_wx_sign_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String jumpUrl = "https://pay.weixin.qq.com/static/help_guide/register_guide.shtml";
                /*Intent intent = new Intent(getActivity(), RegisterActivity.class);
                intent.putExtra(Constants.REGISTER_INTENT, jumpUrl);
                intent.putExtra(Constants.REGISTER_ALWAYS_ALLOW_CLOSE, true);
                intent.putExtra(Constants.REGISTER_ALLOW_CACHE, true);
                startActivity(intent);*/
                Intent intent= new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri content_url = Uri.parse(jumpUrl);
                intent.setData(content_url);
                startActivity(intent);
            }
        });

        try {
            String wechatRealQrcodeUrl = mSignDetailData.getWechatRealQrcodeUrl();
            Bitmap bitmap = ImageUtil.base64ToBitmap(wechatRealQrcodeUrl);
            if (bitmap != null) {
                iv_qrcode_wx_sign_dialog.setImageBitmap(bitmap);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}