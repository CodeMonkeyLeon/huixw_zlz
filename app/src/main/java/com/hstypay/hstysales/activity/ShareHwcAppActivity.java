package com.hstypay.hstysales.activity;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.Zxing.MaxCardManager;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.utils.ClickUtil;
import com.hstypay.hstysales.utils.DensityUtils;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.ScreenUtils;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.utils.UrlDecode;
import com.hstypay.hstysales.widget.SafeDialog;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.net.URL;
import java.net.URLEncoder;

//下载分享汇旺财app
public class ShareHwcAppActivity extends BaseActivity implements View.OnClickListener {

    private TextView mTvTitle;
    private ImageView mIvBack;
    private ImageView mIv_download_url_code;
    private LinearLayout mLl_share_wechat_hwc;
    private LinearLayout mLl_share_friends_hwc;
    private LinearLayout mLl_share_qq_hwc;
    private SafeDialog shareDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_hwc_app);
        StatusBarUtil.setTranslucentStatusWhiteIcon(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initEvent();
    }

    private void initEvent() {
        mIvBack.setOnClickListener(this);
        mLl_share_wechat_hwc.setOnClickListener(this);
        mLl_share_friends_hwc.setOnClickListener(this);
        mLl_share_qq_hwc.setOnClickListener(this);
    }


    private void initData() {
        shareDialog = getLoadDialog(this, UIUtils.getString(R.string.share_loading), true, 0.9f);
        mTvTitle.setText(getResources().getString(R.string.tv_title_down_hwc));
        try {
            String hwc = URLEncoder.encode("汇旺财", "UTF-8");
            String hwcAppUrl = "https://www.hstypay.com/uploads/soft/"+hwc+".apk";
            hwcAppUrl = "https://www.hstypay.com/download/app/page.html";
            Log.d("tagtag","hwcAppUrl:"+hwcAppUrl);
            Bitmap dCode = MaxCardManager.getInstance().create2DCode(hwcAppUrl,
                    DensityUtils.dip2px(this, 165),
                    DensityUtils.dip2px(this, 165));
            mIv_download_url_code.setImageBitmap(dCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        View rl_title_share_hwc = findViewById(R.id.rl_title_share_hwc);
        rl_title_share_hwc.setBackgroundColor(getResources().getColor(R.color.transparent));
        mTvTitle = findViewById(R.id.tv_title);
        mIvBack = findViewById(R.id.iv_back);

        mIv_download_url_code = findViewById(R.id.iv_download_url_code);
        mLl_share_wechat_hwc = findViewById(R.id.ll_share_wechat_hwc);
        mLl_share_friends_hwc = findViewById(R.id.ll_share_friends_hwc);
        mLl_share_qq_hwc = findViewById(R.id.ll_share_qq_hwc);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back://返回
                onBackPressed();
                break;
            case R.id.ll_share_wechat_hwc:
                shareToWx();
                break;
            case R.id.ll_share_friends_hwc:
                shareToFridens();
                break;
            case R.id.ll_share_qq_hwc:
                shareToQQ();
                break;
            default:
                break;
        }
    }

    //分享图片到微信
    private void shareToWx() {
        if (!ClickUtil.isNotFastClick()){
            return;
        }
        UMImage image = new UMImage(this, ScreenUtils.createViewBitmapWithColor(findViewById(R.id.iv_download_url_code),getResources().getColor(R.color.bg_f4f6f8)));//bitmap文件
//        image.setThumb(new UMImage(this, R.mipmap.share_icon));
        new ShareAction(this)
                .withMedia(image)
                .setPlatform(SHARE_MEDIA.WEIXIN)
                .setCallback(mShareListener).share();
    }

    //分享图片到朋友圈
    private void shareToFridens() {
        if (!ClickUtil.isNotFastClick()){
            return;
        }
        UMImage image = new UMImage(this, ScreenUtils.createViewBitmapWithColor(findViewById(R.id.iv_download_url_code),getResources().getColor(R.color.bg_f4f6f8)));//bitmap文件
//        image.setThumb(new UMImage(this, R.mipmap.share_icon));
        new ShareAction(this)
                .withMedia(image)
                .setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE)
                .setCallback(mShareListener).share();
    }

    //分享图片到QQ
    private void shareToQQ() {
        if (!ClickUtil.isNotFastClick()){
            return;
        }
        UMImage image = new UMImage(this, ScreenUtils.createViewBitmapWithColor(findViewById(R.id.iv_download_url_code),getResources().getColor(R.color.bg_f4f6f8)));//bitmap文件
//        image.setThumb(new UMImage(this, R.mipmap.share_icon));
        new ShareAction(this)
                .withMedia(image)
                .setPlatform(SHARE_MEDIA.QQ)
                .setCallback(mShareListener).share();
    }

    private UMShareListener mShareListener  = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
            DialogUtil.safeShowDialog(shareDialog);
        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(shareDialog);
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            DialogUtil.safeCloseDialog(shareDialog);
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            DialogUtil.safeCloseDialog(shareDialog);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        DialogUtil.safeCloseDialog(shareDialog);
    }
}