package com.hstypay.hstysales.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.hstypay.hstysales.R;
import com.hstypay.hstysales.adapter.DeviceManageAdapter;
import com.hstypay.hstysales.adapter.DeviceManageTypeAdapter;
import com.hstypay.hstysales.adapter.MerchantRecyclerAdapter;
import com.hstypay.hstysales.adapter.SalesmanAdapter;
import com.hstypay.hstysales.adapter.ServiceProviderAdapter;
import com.hstypay.hstysales.adapter.ShopRecyclerAdapter;
import com.hstypay.hstysales.adapter.StoreRecyclerAdapter;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.base.BaseActivity;
import com.hstypay.hstysales.bean.DeviceBean;
import com.hstypay.hstysales.bean.DeviceListBean;
import com.hstypay.hstysales.bean.DeviceType;
import com.hstypay.hstysales.bean.DeviceTypeBean;
import com.hstypay.hstysales.bean.Info;
import com.hstypay.hstysales.bean.MerchantListBean;
import com.hstypay.hstysales.bean.SalesmanBean;
import com.hstypay.hstysales.bean.ServiceProviderBean;
import com.hstypay.hstysales.bean.StoreListBean;
import com.hstypay.hstysales.network.NoticeEvent;
import com.hstypay.hstysales.network.ServerClient;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StatusBarUtil;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CustomLinearLayoutManager;
import com.hstypay.hstysales.widget.CustomViewFullScreenDialog;
import com.hstypay.hstysales.widget.EditTextDelete;
import com.hstypay.hstysales.widget.SafeDialog;
import com.scu.miomin.shswiperefresh.core.SHSwipeRefreshLayout;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.ykk.dropdownmenu.DropDownMenu;
import com.ykk.dropdownmenu.TabBean;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author dean.zeng
 * @Description 设备列表
 * @Date 2020-02-05 15:28
 **/
public class DeviceListActivity extends BaseActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_BINDDEVICE = 1001;//绑定设备
    private ImageView mIvBack, mIvStoreArrow, mIvDeviceTypeArrow;
    private LinearLayout mLlChoiceStore, mLlDeviceType;
    private EditTextDelete mEtInput;
    private RecyclerView mRecyclerView, mTypeRecyclerView;
    private TextView mButtonTitleRight, mTvTitle, mTvStoreName, mTvDeviceTypeName, mTvNull;
    private List<DeviceType> mTypeList;
    private List<DeviceBean> mDeviceList;
    private Animation rotate;
    private SafeDialog mLoadDialog;
    private SHSwipeRefreshLayout mSwipeRefreshLayout;
    private String categoryCode = "";
    private CustomLinearLayoutManager mLinearLayoutManager;
    private DeviceManageAdapter mAdapter;
    private boolean mTypeSwitchArrow, mStoreSwitchArrow, mMerchantSwithArrow;
    private DeviceManageTypeAdapter mDeviceTypeAdapter;
    private LinearLayout mLlChoiceMerchant;
    private ImageView mIvMerchantArrow;
    private TextView mTvMerchantName;
    private RelativeLayout mLlInput;

    //门店
    private RecyclerView mStoreRecyclerView;
    private SHSwipeRefreshLayout mSwipeRefreshLayoutStore;
    private LinearLayout mStorePopLayout;
    private EditTextDelete mEtStoreInput;
    private String mCurSelectStoreId;//当前选中的门店ID
    private String mCurSelectStoreName;//当前选中的门店名称
    private StoreRecyclerAdapter mShopRecyclerAdapter;
    private List<StoreListBean.DataEntity> mStoreList;//门店列表数据
    private StoreListBean.DataEntity mAllStoreBean;//"全部门店"
    private RelativeLayout mRlStoreInput;
    private TextView mTvShopNull;

    //商户
    private RecyclerView mRecyclerViewMerchant;
    private SHSwipeRefreshLayout mSwipeRefreshLayoutMerchant;
    private LinearLayout mMerchantPopLayout;
    private EditTextDelete mEtMerchantInput;
    private String mCurSelectMerchantId;//当前选中的商户ID
    private String mCurSelectMerchantName;//当前选中的商户名称
    private MerchantRecyclerAdapter mMerchantRecyclerAdapter;//商户列表的adapter
    private List<MerchantListBean.DataEntity.ItemData> mMerchantList;//商户列表数据
    private MerchantListBean.DataEntity.ItemData mAllMerchant;//"全部商户"
    private RelativeLayout mRlMerchantInput;
    private TextView mTvMerchantNull;
    private String mIntentMerchantId;
    private String mIntentMerchantName;
    private DropDownMenu mDropDownMenu;
    private String mCurSelectServiceProviderId;
    private String mCurSelectSalesmanId;
    private ArrayList<ServiceProviderBean.DataEntity.ServiceProvider> mSearchServiceProviderList;
    private ArrayList<SalesmanBean.DataEntity.Salesman> mSearchSalesmanList;
    private ArrayList<TabBean> headers;
    private int mSearchCurrentPage = 1;
    private int mSearchPageSize = 15;
    private boolean isLoadOver;
    private int mSelectLabelPosition;
    private EditText mEtServiceInput;
    private ImageView mIvServiceClean;
    private View mServiceDataEmpty;
    private ServiceProviderAdapter mServiceProviderAdapter;
    private EditText mEtSalesmanInput;
    private ImageView mIvSalesmanClean;
    private View mSalesmanDataEmpty;
    private SalesmanAdapter mSalesmanAdapter;
    private ServiceProviderBean.DataEntity.ServiceProvider mAllServiceProvider;
    private SalesmanBean.DataEntity.Salesman mAllSalesman;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);
        StatusBarUtil.setTranslucentStatusWhiteIcon(this);
        MyApplication.getInstance().addActivity(this);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        mLoadDialog = getLoadDialog(this, UIUtils.getString(R.string.public_loading), false);
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mTvTitle = findViewById(R.id.tv_title);
        mTvTitle.setText(R.string.string_title_device_list);
        mButtonTitleRight = findViewById(R.id.button_title);
        mButtonTitleRight.setVisibility(View.VISIBLE);
        mButtonTitleRight.setText(R.string.bind_device);

        mDropDownMenu = findViewById(R.id.dropDownMenu);
    }

    private void initListener() {
        mIvBack.setOnClickListener(this);
        mLlDeviceType.setOnClickListener(this);
        mButtonTitleRight.setOnClickListener(this);

    }

    private void initSwipeRefreshLayoutStore() {
        mSwipeRefreshLayoutStore.setRefreshEnable(false);
        mSwipeRefreshLayoutStore.setLoadmoreEnable(true);
        mSwipeRefreshLayoutStore.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadStoreMore = true;
                    loadStoreData(false);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayoutStore.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayoutStore.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutStore.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutStore.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayoutStore.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutStore.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutStore.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayoutStore.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });
    }

    private void initRecyclerView(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mLinearLayoutManager = new CustomLinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void initData() {
        mIntentMerchantId = getIntent().getStringExtra(Constants.INTENT_MERCHANT_ID);
        mIntentMerchantName = getIntent().getStringExtra(Constants.INTENT_MERCHANT_NAME);
        mMerchantList = new ArrayList<>();
        mStoreList = new ArrayList<>();
        mTypeList = new ArrayList<>();

        mDeviceList = new ArrayList<>();

        initDropDownMenu();
        //门店筛选
        mShopRecyclerAdapter = new StoreRecyclerAdapter(this);
        mStoreRecyclerView.setAdapter(mShopRecyclerAdapter);
        mCurSelectStoreId = "";
        mCurSelectStoreName = getResources().getString(R.string.tv_shop);
        mAllStoreBean = new StoreListBean().new DataEntity();//"全部商户"
        mAllStoreBean.setStoreName(mCurSelectStoreName);
        mAllStoreBean.setStoreId("");
        mTvStoreName.setText(mCurSelectStoreName);
        mShopRecyclerAdapter.setOnItemClickListener(new StoreRecyclerAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mCurSelectStoreId = mStoreList.get(position).getStoreId();
                mCurSelectStoreName = mStoreList.get(position).getStoreName().trim();
                if (mCurSelectStoreName.length() > 6) {
                    mCurSelectStoreName = mCurSelectStoreName.substring(0, 5) + "...";
                }
                mTvStoreName.setText(mCurSelectStoreName);
                mStorePopLayout.setVisibility(View.GONE);
                closeArrow(1);
                currentPageDevice = 1;
                getDeviceList();
            }
        });

        //商户筛选
        mMerchantRecyclerAdapter = new MerchantRecyclerAdapter(this);
        mRecyclerViewMerchant.setAdapter(mMerchantRecyclerAdapter);
        mCurSelectMerchantId = "";
        mCurSelectMerchantName = getResources().getString(R.string.all_merchant);
        mAllMerchant = new MerchantListBean.DataEntity.ItemData();//"全部商户"
        mAllMerchant.setMerchantName(mCurSelectMerchantName);
        mAllMerchant.setMerchantId("");
        mTvMerchantName.setText(mCurSelectMerchantName);
        mMerchantRecyclerAdapter.setOnItemClickListener(new MerchantRecyclerAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (!TextUtils.isEmpty(mCurSelectMerchantId) && !mCurSelectMerchantId.equals(mMerchantList.get(position).getMerchantId())) {
                    mEtStoreInput.setText("");
                    mCurSelectStoreId = "";
                    mTvStoreName.setText(R.string.tv_shop);
                }
                mCurSelectMerchantId = mMerchantList.get(position).getMerchantId();
                mCurSelectMerchantName = mMerchantList.get(position).getMerchantName().trim();
                if (mCurSelectMerchantName.length() > 6) {
                    mCurSelectMerchantName = mCurSelectMerchantName.substring(0, 5) + "...";
                }
                mTvMerchantName.setText(mCurSelectMerchantName);
                mMerchantPopLayout.setVisibility(View.GONE);
                closeArrow(0);
                currentPageDevice = 1;
                getDeviceList();
            }
        });
        if (!TextUtils.isEmpty(mIntentMerchantId)) {
            //是从商户进来的，就不能选商户了
            mLlChoiceMerchant.setVisibility(View.GONE);
            mCurSelectMerchantId = mIntentMerchantId;
            mCurSelectMerchantName = mIntentMerchantName;
            mEtInput.setHint(getResources().getString(R.string.hint_search_cloud_device_store));
        }


        //设备列表筛选
        mAdapter = new DeviceManageAdapter(DeviceListActivity.this, mDeviceList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new DeviceManageAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //设备详情
                String deviceId = mDeviceList.get(position).getId();
                String categoryCode = mDeviceList.get(position).getCategoryCode();
                String categoryName = mDeviceList.get(position).getCategoryName();
                String images = mDeviceList.get(position).getImages();
                Intent intent = new Intent(DeviceListActivity.this, DeviceDetailActivity.class);
                intent.putExtra(Constants.INTENT_NAME_DEVICE_ID, deviceId);
                intent.putExtra(Constants.INTENT_NAME_CATEGORY_CODE, categoryCode);
                intent.putExtra(Constants.INTENT_NAME_CATEGORY_TYPE, categoryName);
                intent.putExtra(Constants.INTENT_NAME_DEVICE_IMG, images);
                startActivity(intent);
            }

            @Override
            public void onUnbindClick(int position) {
                //解绑
                showRemindUnbindDialog(mDeviceList.get(position));
            }
        });


        mDeviceList.clear();
        currentPageDevice = 1;
        getDeviceList();
    }

    private void initDropDownMenu() {
        mCurSelectServiceProviderId = "";
        mCurSelectSalesmanId = "";
        mSearchServiceProviderList = new ArrayList<>();
        mSearchSalesmanList = new ArrayList<>();

        mAllServiceProvider = new ServiceProviderBean.DataEntity.ServiceProvider();//"全部商户"
        mAllServiceProvider.setServiceProviderName("全部市运营");
        mAllServiceProvider.setServiceProviderId("");

        mAllSalesman = new SalesmanBean.DataEntity.Salesman();//"全部商户"
        mAllSalesman.setEmpName("全部城市经理");
        mAllSalesman.setEmpId("");

        View contentView = getLayoutInflater().inflate(R.layout.layout_content_device_list, null);

        mLlInput = contentView.findViewById(R.id.ll_input);
        mEtInput = contentView.findViewById(R.id.et_input);
        mTvNull = (TextView) contentView.findViewById(R.id.tv_null);
        mEtInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mDeviceList.clear();
                        currentPageDevice = 1;
                        getDeviceList();
                    } else {
                        ToastUtil.showToastShort(getResources().getString(R.string.net_error));
                    }
                    return true;
                }
                return false;
            }
        });
        mEtInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                        mDeviceList.clear();
                        currentPageDevice = 1;
                        getDeviceList();
                    } else {
                        ToastUtil.showToastShort(getResources().getString(R.string.net_error));
                    }
                }
            }
        });
        initRecyclerView(contentView);
        initSwipeRefreshLayout(contentView);

        //设备类型筛选
        mTvDeviceTypeName = (TextView) contentView.findViewById(R.id.tv_device_type);
        mIvDeviceTypeArrow = (ImageView) contentView.findViewById(R.id.iv_device_type_arrow);
        mLlDeviceType = (LinearLayout) contentView.findViewById(R.id.ll_device_type);

        //门店筛选
        mTvStoreName = (TextView) contentView.findViewById(R.id.tv_store_name);
        mIvStoreArrow = (ImageView) contentView.findViewById(R.id.iv_store_arrow);
        mLlChoiceStore = (LinearLayout) contentView.findViewById(R.id.ll_choice_store);
        mStorePopLayout = (LinearLayout) contentView.findViewById(R.id.shop_pop_layout);
        mEtStoreInput = contentView.findViewById(R.id.et_store_input);
        mRlStoreInput = contentView.findViewById(R.id.rl_store_input);
        mTvShopNull = (TextView) contentView.findViewById(R.id.tv_shop_null);
        mSwipeRefreshLayoutStore = contentView.findViewById(R.id.swipeRefreshLayout_store);
        mStoreRecyclerView = (RecyclerView) contentView.findViewById(R.id.recyclerView_store);
        mStoreRecyclerView.setLayoutManager(new CustomLinearLayoutManager(this));
        initSwipeRefreshLayoutStore();
        mEtStoreInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //从门店列表里搜索门店
                    loadStoreData(true);
                    return true;
                }
                return false;
            }
        });
        mEtStoreInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear) {
                    if (isClear && mStorePopLayout.getVisibility() == View.VISIBLE) {//mEtStoreInput.setText("")会触发onEditChanged
                        loadStoreData(true);
                    }
                }
            }
        });
        mLlChoiceStore.setOnClickListener(this);


        //设备类型
        CustomLinearLayoutManager typeLinearLayoutManager = new CustomLinearLayoutManager(this);
        mTypeRecyclerView = (RecyclerView) contentView.findViewById(R.id.recyclerView_device_type);
        mTypeRecyclerView.setLayoutManager(typeLinearLayoutManager);


        //商户筛选
        mTvMerchantName = contentView.findViewById(R.id.tv_merchant_name);
        mIvMerchantArrow = contentView.findViewById(R.id.iv_merchant_arrow);
        mLlChoiceMerchant = contentView.findViewById(R.id.ll_choice_merchant);
        mMerchantPopLayout = contentView.findViewById(R.id.merchant_pop_layout);
        mRlMerchantInput = contentView.findViewById(R.id.rl_merchant_input);
        mTvMerchantNull = contentView.findViewById(R.id.tv_merchant_null);
        mSwipeRefreshLayoutMerchant = contentView.findViewById(R.id.swipeRefreshLayout_merchant);
        mRecyclerViewMerchant = contentView.findViewById(R.id.recyclerView_merchant);
        mEtMerchantInput = contentView.findViewById(R.id.et_merchant_input);
        mRecyclerViewMerchant.setLayoutManager(new CustomLinearLayoutManager(this));
        initSwipeRefreshLayoutMerchant();
        mEtMerchantInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //从商户列表里搜索商户
                    loadMerchantData(true);
                    return true;
                }
                return false;
            }
        });
        mEtMerchantInput.setOnEditChangedListener(new EditTextDelete.OnEditChangedListener() {
            @Override
            public void onEditChanged(boolean isClear) {
                if (isClear && mMerchantPopLayout.getVisibility() == View.VISIBLE) {//mEtMerchantInput.setText("")会触发onEditChanged
                    loadMerchantData(true);
                }
            }
        });
        mLlChoiceMerchant.setOnClickListener(this);

        headers = new ArrayList<>();
        TabBean tabBean2 = new TabBean();
        tabBean2.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean2.setLabel("cityService");
        tabBean2.setTabTitle("全部市运营");
        TabBean tabBean3 = new TabBean();
        tabBean3.setType(DropDownMenu.TYPE_CUSTOM);
        tabBean3.setLabel("cityManager");
        tabBean3.setTabTitle("全部城市经理");
        if (TextUtils.isEmpty(mIntentMerchantId))
            if ("SUPERADMIN".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_ROLE_CODE))) {
                if ("".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                    headers.add(tabBean2);
                    headers.add(tabBean3);
                } else if ("SC".equals(SpUtil.getString(MyApplication.getContext(), Constants.SP_SERVICE_PROVIDER))) {
                    headers.add(tabBean3);
                }
            }
        mDropDownMenu.setDropDownMenu(headers, initViewData(), contentView);
        mDropDownMenu.setOnTabClickListener(new DropDownMenu.OnTabClickListener() {
            @Override
            public void onTabClick(int position, boolean isOpen) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
                mSelectLabelPosition = position;
                switch (headers.get(position).getLabel()) {
                    case "cityService":
                        if (isOpen)
                            getServiceList(false);
                        break;
                    case "cityManager":
                        if (isOpen)
                            getSalesmanList(false);
                        break;
                }
            }
        });
    }

    /**
     * 设置类型和数据源：
     * DropDownMenu.KEY对应类型（DropDownMenu中的常量，参考上述核心源码）
     * DropDownMenu.VALUE对应数据源：key不是TYPE_CUSTOM则传递string[],key是TYPE_CUSTOM类型则传递对应view
     */
    private List<HashMap<String, Object>> initViewData() {
        List<HashMap<String, Object>> viewDatas = new ArrayList<>();
        HashMap<String, Object> map;
        for (int i = 0; i < headers.size(); i++) {
            map = new HashMap<String, Object>();
            map.put(DropDownMenu.KEY, headers.get(i).getType());
            map.put(DropDownMenu.VALUE, getCustomView(headers.get(i).getLabel()));
            viewDatas.add(map);
        }
        return viewDatas;
    }

    private View getCustomView(String label) {
        View view = getLayoutInflater().inflate(R.layout.layout_dropdown, null);
        switch (label) {
            case "cityService":
                mEtServiceInput = view.findViewById(R.id.et_input);
                mIvServiceClean = view.findViewById(R.id.iv_clean);
                mServiceDataEmpty = view.findViewById(R.id.common_data_empty);
                RecyclerView rvService = view.findViewById(R.id.recycler_view);
                LinearLayoutManager serviceLinearLayoutManager = new LinearLayoutManager(this);
                rvService.setLayoutManager(serviceLinearLayoutManager);
                mServiceProviderAdapter = new ServiceProviderAdapter(DeviceListActivity.this);
                mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                mServiceProviderAdapter.setOnItemClickListener(new ServiceProviderAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mCurSelectServiceProviderId != null && !mCurSelectServiceProviderId.equals(mSearchServiceProviderList.get(position).getServiceProviderId())) {
                            mCurSelectSalesmanId = "";
                            for (int i = 0; i < headers.size(); i++) {
                                if ("cityManager".equals(headers.get(i).getLabel()))
                                    mDropDownMenu.setTabText(i, "全部城市经理");
                            }
                            mCurSelectMerchantId = "";
                            mCurSelectMerchantName = getResources().getString(R.string.all_merchant);
                            mTvMerchantName.setText(mCurSelectMerchantName);
                            if (mMerchantSwithArrow)
                                closeArrow(0);
                            mCurSelectStoreId = "";
                            mCurSelectStoreName = getResources().getString(R.string.tv_shop);
                            mTvStoreName.setText(mCurSelectStoreName);
                            if (mStoreSwitchArrow)
                                closeArrow(1);
                            mCurSelectServiceProviderId = mSearchServiceProviderList.get(position).getServiceProviderId();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mDropDownMenu.setTabText(mSelectLabelPosition, mSearchServiceProviderList.get(position).getServiceProviderName());
                            currentPageDevice = 1;
                            getDeviceList();
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvService.setAdapter(mServiceProviderAdapter);
                rvService.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    //这个int用来记录最后一个可见的view
                    int lastVisibleItemPosition;
                    int refreshCurrentPage;//防止重复加载

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mServiceProviderAdapter.getItemCount()) {
                            refreshCurrentPage = mSearchCurrentPage;
                            if (!isLoadOver)
                                getServiceList(true);
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                        lastVisibleItemPosition = serviceLinearLayoutManager.findLastVisibleItemPosition();
                    }
                });
                mEtServiceInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            getServiceList(false);
                            return true;
                        }
                        return false;
                    }
                });
                mEtServiceInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            getServiceList(false);
                        }
                        mIvServiceClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                mIvServiceClean.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEtServiceInput.setText("");
                    }
                });
                break;
            case "cityManager":
                mEtSalesmanInput = view.findViewById(R.id.et_input);
                mIvSalesmanClean = view.findViewById(R.id.iv_clean);
                mSalesmanDataEmpty = view.findViewById(R.id.common_data_empty);
                RecyclerView rvSalesman = view.findViewById(R.id.recycler_view);
                LinearLayoutManager salesmanLinearLayoutManager = new LinearLayoutManager(this);
                rvSalesman.setLayoutManager(salesmanLinearLayoutManager);
                mSalesmanAdapter = new SalesmanAdapter(DeviceListActivity.this);
                mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                mSalesmanAdapter.setOnItemClickListener(new SalesmanAdapter.OnRecyclerViewItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        if (mCurSelectSalesmanId != null && !mCurSelectSalesmanId.equals(mSearchSalesmanList.get(position).getEmpId())) {
                            mCurSelectMerchantId = "";
                            mCurSelectMerchantName = getResources().getString(R.string.all_merchant);
                            mTvMerchantName.setText(mCurSelectMerchantName);
                            if (mMerchantSwithArrow)
                                closeArrow(0);
                            mCurSelectStoreId = "";
                            mCurSelectStoreName = getResources().getString(R.string.tv_shop);
                            mTvStoreName.setText(mCurSelectStoreName);
                            if (mStoreSwitchArrow)
                                closeArrow(1);
                            mCurSelectSalesmanId = mSearchSalesmanList.get(position).getEmpId();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mDropDownMenu.setTabText(mSelectLabelPosition, mSearchSalesmanList.get(position).getEmpName());
                            currentPageDevice = 1;
                            getDeviceList();
                        }
                        mDropDownMenu.closeMenu();
                    }
                });
                rvSalesman.setAdapter(mSalesmanAdapter);
                rvSalesman.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    //这个int用来记录最后一个可见的view
                    int lastVisibleItemPosition;
                    int refreshCurrentPage;//防止重复加载

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                        if (refreshCurrentPage != mSearchCurrentPage && newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItemPosition + 1 == mSalesmanAdapter.getItemCount()) {
                            refreshCurrentPage = mSearchCurrentPage;
                            if (!isLoadOver)
                                getSalesmanList(true);
                        }
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        //这个mLinearLayoutManager是LinearLayoutManager的一个实例。
                        lastVisibleItemPosition = salesmanLinearLayoutManager.findLastVisibleItemPosition();
                    }
                });
                mEtSalesmanInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            getSalesmanList(false);
                            return true;
                        }
                        return false;
                    }
                });
                mEtSalesmanInput.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 0) {
                            getSalesmanList(false);
                        }
                        mIvSalesmanClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                mIvSalesmanClean.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mEtSalesmanInput.setText("");
                    }
                });
                break;
        }
        return view;
    }


    //商户筛选的下拉刷新
    private void initSwipeRefreshLayoutMerchant() {
        mSwipeRefreshLayoutMerchant.setRefreshEnable(false);
        mSwipeRefreshLayoutMerchant.setLoadmoreEnable(true);
        mSwipeRefreshLayoutMerchant.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadMerchantMore = true;
                    loadMerchantData(false);
                } else {
                    ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
                    mSwipeRefreshLayoutMerchant.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayoutMerchant.finishLoadmore();
                        }
                    }, 500);
                }
            }

            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutMerchant.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutMerchant.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayoutMerchant.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutMerchant.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayoutMerchant.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayoutMerchant.setLoaderViewText("正在加载...");
                        break;
                }
            }
        });
    }


    //设备列表的下拉刷新
    private void initSwipeRefreshLayout(View view) {
        mSwipeRefreshLayout = (SHSwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SHSwipeRefreshLayout.SHSOnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isRefreshedDevice = true;
                    currentPageDevice = 1;
                    getDeviceList();
                } else {
                    ToastUtil.showToastShort(getResources().getString(R.string.net_error));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            }

            @Override
            public void onLoading() {
                if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
                    isLoadmoreDevice = true;
                    getDeviceList();
                } else {
                    ToastUtil.showToastShort(getResources().getString(R.string.net_error));
                    mSwipeRefreshLayout.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.finishLoadmore();
                        }
                    }, 500);
                }
            }

            /**
             * 监听下拉刷新过程中的状态改变
             * @param percent 当前下拉距离的百分比（0-1）
             * @param state 分三种状态{NOT_OVER_TRIGGER_POINT：还未到触发下拉刷新的距离；OVER_TRIGGER_POINT：已经到触发下拉刷新的距离；START：正在下拉刷新}
             */
            @Override
            public void onRefreshPulStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("下拉刷新");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        mSwipeRefreshLayout.setRefreshViewText("松开刷新");
                        break;
                    case SHSwipeRefreshLayout.START:
                        mSwipeRefreshLayout.setRefreshViewText("正在刷新");
                        break;
                }
            }

            @Override
            public void onLoadmorePullStateChange(float percent, int state) {
                switch (state) {
                    case SHSwipeRefreshLayout.NOT_OVER_TRIGGER_POINT:
                        //textView.setText("上拉加载");
                        mSwipeRefreshLayout.setLoaderViewText("上拉加载");
                        break;
                    case SHSwipeRefreshLayout.OVER_TRIGGER_POINT:
                        //textView.setText("松开加载");
                        mSwipeRefreshLayout.setLoaderViewText("松开加载");
                        break;
                    case SHSwipeRefreshLayout.START:
                        //textView.setText("正在加载");
                        mSwipeRefreshLayout.setLoaderViewText("正在加载");
                        break;
                }
            }
        });
    }


    @Override
    public boolean isNeedEventBus() {
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.btn_submit:
            case R.id.button_title:
                //新建，绑定设备
                Intent intent = new Intent(DeviceListActivity.this, CaptureActivity.class);
                intent.putExtra(Constants.INTENT_NAME, Constants.INTENT_BIND_DEVICE);
                intent.putExtra(Constants.INTENT_MERCHANT_ID, mIntentMerchantId);
                intent.putExtra(Constants.INTENT_MERCHANT_NAME, mIntentMerchantName);
                startActivityForResult(intent, REQUEST_CODE_BINDDEVICE);
                break;
            case R.id.ll_device_type:
                if (mMerchantSwithArrow) {
                    closeArrow(0);
                }
                if (mStoreSwitchArrow) {
                    closeArrow(1);
                }
                if (mTypeSwitchArrow) {
                    closeArrow(2);
                } else {
                    openArrow(2);
                }
                break;
            case R.id.ll_choice_store:
                if (mMerchantSwithArrow) {
                    closeArrow(0);
                }
                if (mTypeSwitchArrow) {
                    closeArrow(2);
                }
                if (mStoreSwitchArrow) {
                    closeArrow(1);
                } else {
                    openArrow(1);
                }
                break;
            case R.id.ll_choice_merchant:
                if (mStoreSwitchArrow) {
                    closeArrow(1);
                }
                if (mTypeSwitchArrow) {
                    closeArrow(2);
                }
                if (mMerchantSwithArrow) {
                    closeArrow(0);
                } else {
                    openArrow(0);
                }
                break;
            default:
                break;
        }
    }


    //condition 0商户，1门店，2类型
    public void closeArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 0:
                mMerchantSwithArrow = false;
                mIvMerchantArrow.startAnimation(rotate);
                mMerchantPopLayout.setVisibility(View.GONE);
                mEtMerchantInput.setText("");
                break;
            case 1:
                mStoreSwitchArrow = false;
                mIvStoreArrow.startAnimation(rotate);
                mStorePopLayout.setVisibility(View.GONE);
                mEtStoreInput.setText("");
                break;
            case 2:
                mTypeSwitchArrow = false;
                mIvDeviceTypeArrow.startAnimation(rotate);
                mTypeRecyclerView.setVisibility(View.GONE);
                break;
        }
    }

    //condition 0商户，1门店，2类型
    private void openArrow(int condition) {
        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        switch (condition) {
            case 1:
                mStoreSwitchArrow = true;
                mIvStoreArrow.startAnimation(rotate);
                loadStoreData(true);
                break;
            case 2:
                mTypeSwitchArrow = true;
                mIvDeviceTypeArrow.startAnimation(rotate);
                loadDeviceData();
                break;
            case 0:
                mMerchantSwithArrow = true;
                mIvMerchantArrow.startAnimation(rotate);
                loadMerchantData(true);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIvStoreArrow.clearAnimation();
    }

    //设备解绑
    private void unBindDevice(DeviceBean deviceBean) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            Map<String, Object> map = new HashMap<>();
            map.put("id", deviceBean.getId());
            ServerClient.newInstance(MyApplication.getContext()).unBindDevice(MyApplication.getContext(), Constants.TAG_UNBIND_DEVICE, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }


    //获取门店列表
    private static final int mStorePageSize = 15;
    private int mStoreCurrentPage = 1;
    private boolean isLoadStoreMore;//加载更多标识

    private void loadStoreData(boolean refresh) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!isLoadStoreMore) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            if (refresh)
                mStoreCurrentPage = 1;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mStorePageSize);
            map.put("currentPage", mStoreCurrentPage);
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mCurSelectSalesmanId))
                map.put("salesmanId", mCurSelectSalesmanId);
            if (!TextUtils.isEmpty(mCurSelectMerchantId))
                map.put("merchantId", mCurSelectMerchantId);
            if (!TextUtils.isEmpty(mEtStoreInput.getText().toString().trim())) {
                map.put("storeName", mEtStoreInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).choiceStore(MyApplication.getContext(), Constants.TAG_DEVICE_STORE, map);
        } else {
            ToastUtil.showToastShort(getResources().getString(R.string.net_error));
        }
    }

    private void getServiceList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                map.put("serviceProviderName", mEtServiceInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getServiceList(MyApplication.getContext(), "TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    private void getSalesmanList(boolean isLoadMore) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            DialogUtil.safeShowDialog(mLoadDialog);
            if (!isLoadMore) {
                mSearchCurrentPage = 1;
                isLoadMerchantMore = false;
                isLoadOver = false;
            }
            isLoadMerchantMore = isLoadMore;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mSearchPageSize);
            map.put("currentPage", mSearchCurrentPage);
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                map.put("empName", mEtSalesmanInput.getText().toString().trim());
            }
            ServerClient.newInstance(MyApplication.getContext()).getSalesmanList(MyApplication.getContext(), "TAG_SALESMAN_LIST_" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetStoreList(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_DEVICE_STORE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            setSwipeRefreshState(mSwipeRefreshLayoutStore, isLoadStoreMore, 500);
            StoreListBean msg = (StoreListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getResources().getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(DeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().size() > 0) {
                        if (mStoreCurrentPage == 1) {
                            mStoreList.clear();
                            if (TextUtils.isEmpty(mEtStoreInput.getText().toString().trim()))
                                mStoreList.add(mAllStoreBean);
                        }
                        mStoreList.addAll(msg.getData());
                        mShopRecyclerAdapter.setData(mStoreList, mCurSelectStoreId);
                        mStoreCurrentPage++;
                        mStorePopLayout.setVisibility(View.VISIBLE);
                        mRlStoreInput.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayoutStore.setVisibility(View.VISIBLE);
                        mTvShopNull.setVisibility(View.GONE);

                    } else {
                        if (isLoadStoreMore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mStoreList.clear();
                            mShopRecyclerAdapter.setData(mStoreList, mCurSelectStoreId);
                            mStorePopLayout.setVisibility(View.VISIBLE);
                            mRlStoreInput.setVisibility(View.GONE);
                            mSwipeRefreshLayoutStore.setVisibility(View.GONE);
                            mTvShopNull.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            isLoadStoreMore = false;

        } else if (event.getTag().equals("TAG_SERVICE_PROVIDER_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            ServiceProviderBean msg = (ServiceProviderBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(DeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mServiceDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchServiceProviderList.clear();
                            if (TextUtils.isEmpty(mEtServiceInput.getText().toString().trim())) {
                                mSearchServiceProviderList.add(mAllServiceProvider);
                            }
                        }

                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchServiceProviderList.addAll(msg.getData().getData());
                        mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchServiceProviderList.clear();
                            mServiceProviderAdapter.setData(mSearchServiceProviderList, mCurSelectServiceProviderId);
                            mServiceDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mServiceDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        } else if (event.getTag().equals("TAG_SALESMAN_LIST_" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            SalesmanBean msg = (SalesmanBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(DeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        mSalesmanDataEmpty.setVisibility(View.GONE);
                        if (mSearchCurrentPage == 1) {
                            mSearchSalesmanList.clear();
                            if (TextUtils.isEmpty(mEtSalesmanInput.getText().toString().trim())) {
                                mSearchSalesmanList.add(mAllSalesman);
                            }
                        }
                        if (msg.getData().getData().size() != mSearchPageSize)
                            isLoadOver = true;
                        mSearchSalesmanList.addAll(msg.getData().getData());
                        mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                        mSearchCurrentPage++;
                    } else {
                        isLoadOver = true;
                        if (!isLoadMerchantMore) {
                            mSearchSalesmanList.clear();
                            mSalesmanAdapter.setData(mSearchSalesmanList, mCurSelectSalesmanId);
                            mSalesmanDataEmpty.setVisibility(View.VISIBLE);
                        } else {
                            mSalesmanDataEmpty.setVisibility(View.GONE);
                        }
                    }
                    break;
            }
        }
    }

    //获取商户列表
    private static final int mMerchantPageSize = 15;
    private int mMerchantCurrentPage = 1;
    private boolean isLoadMerchantMore;//加载更多标识

    private void loadMerchantData(boolean isRefresh) {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!isLoadMerchantMore) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            if (isRefresh)
                mMerchantCurrentPage = 1;
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", mMerchantPageSize);
            map.put("currentPage", mMerchantCurrentPage);
            if (!TextUtils.isEmpty(mEtMerchantInput.getText().toString().trim())) {
                map.put("search", mEtMerchantInput.getText().toString().trim());
            }
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mCurSelectSalesmanId))
                map.put("salesmanId", mCurSelectSalesmanId);
            map.put("mchQueryStatus", "100");
            ServerClient.newInstance(MyApplication.getContext()).getMerchantList2(MyApplication.getContext(), Constants.TAG_GET_MERCHANT_LIST_MX_SEARCH + "" + this.getClass().getSimpleName(), map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGetMerchantList(NoticeEvent event) {
        if (event.getTag().equals(Constants.TAG_GET_MERCHANT_LIST_MX_SEARCH + "" + this.getClass().getSimpleName())) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            setSwipeRefreshState(mSwipeRefreshLayoutMerchant, isLoadMerchantMore, 500);
            MerchantListBean msg = (MerchantListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getString(R.string.error_network_request));
                    break;
                case Constants.GET_MERCHANT_LIST_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    loginDialog(DeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.GET_MERCHANT_LIST_TRUE:
                    if (msg != null && msg.getData() != null) {
                        if (msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                            if (mMerchantCurrentPage == 1) {
                                mMerchantList.clear();
                                if (TextUtils.isEmpty(mEtMerchantInput.getText().toString().trim()))
                                    mMerchantList.add(mAllMerchant);//添加"全部商户"
                            }

                            mMerchantList.addAll(msg.getData().getData());
                            mMerchantRecyclerAdapter.setData(mMerchantList, mCurSelectMerchantId);
                            mMerchantCurrentPage++;
                            mMerchantPopLayout.setVisibility(View.VISIBLE);
                            mRlMerchantInput.setVisibility(View.VISIBLE);
                            mSwipeRefreshLayoutMerchant.setVisibility(View.VISIBLE);
                            mTvMerchantNull.setVisibility(View.GONE);

                        } else {
                            if (isLoadMerchantMore) {
                                ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                            } else {
                                mMerchantList.clear();
                                mMerchantRecyclerAdapter.setData(mMerchantList, mCurSelectMerchantId);
                                mMerchantPopLayout.setVisibility(View.VISIBLE);
                                mRlMerchantInput.setVisibility(View.GONE);
                                mSwipeRefreshLayoutMerchant.setVisibility(View.GONE);
                                mTvMerchantNull.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        if (isLoadMerchantMore) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mMerchantList.clear();
                            mMerchantRecyclerAdapter.setData(mMerchantList, mCurSelectMerchantId);
                            mMerchantPopLayout.setVisibility(View.VISIBLE);
                            mRlMerchantInput.setVisibility(View.GONE);
                            mSwipeRefreshLayoutMerchant.setVisibility(View.GONE);
                            mTvMerchantNull.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
            isLoadMerchantMore = false;
        }
    }

    //获取设备类型列表
    private void loadDeviceData() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            mTypeList.clear();
            DialogUtil.safeShowDialog(mLoadDialog);
            ServerClient.newInstance(MyApplication.getContext()).deviceTypeList(MyApplication.getContext(), Constants.TAG_DEVICE_TYPE, null);
        } else {
            ToastUtil.showToastShort(getResources().getString(R.string.net_error));
        }
    }


    //获取设备列表
    private int currentPageDevice = 1;
    private int pageSizeDevice = 15;
    private boolean isRefreshedDevice;//刷新
    private boolean isLoadmoreDevice;//加载更多

    private void getDeviceList() {
        if (NetworkUtils.isNetWorkValid(MyApplication.getContext())) {
            if (!(isRefreshedDevice || isLoadmoreDevice)) {
                DialogUtil.safeShowDialog(mLoadDialog);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("pageSize", pageSizeDevice);
            map.put("currentPage", currentPageDevice);
            if (!TextUtils.isEmpty(mEtInput.getText().toString().trim()))
                map.put("q", mEtInput.getText().toString().trim());
            if (!TextUtils.isEmpty(categoryCode))
                map.put("categoryCode", categoryCode);
            if (!TextUtils.isEmpty(mCurSelectServiceProviderId))
                map.put("serviceChannelId", mCurSelectServiceProviderId);
            if (!TextUtils.isEmpty(mCurSelectSalesmanId))
                map.put("salesmanId", mCurSelectSalesmanId);
            if (!TextUtils.isEmpty(mCurSelectMerchantId))
                map.put("merchantId", mCurSelectMerchantId);
            if (!TextUtils.isEmpty(mCurSelectStoreId)) {
                map.put("storeMerchantId", mCurSelectStoreId);
            }
            ServerClient.newInstance(MyApplication.getContext()).getDeviceList(MyApplication.getContext(), Constants.TAG_DEVICE_LIST, map);
        } else {
            ToastUtil.showToastShort(UIUtils.getString(R.string.error_network));
        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataEvent(NoticeEvent event) {
        //解绑设备
        if (event.getTag().equals(Constants.TAG_UNBIND_DEVICE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            Info msg = (Info) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getResources().getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(DeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    //解绑成功，刷新设备列表
                    mDeviceList.clear();
                    currentPageDevice = 1;
                    getDeviceList();
                    break;
            }
        }
        //设备类型
        if (event.getTag().equals(Constants.TAG_DEVICE_TYPE)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            DeviceTypeBean msg = (DeviceTypeBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getResources().getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(DeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());
                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null) {
                        DeviceType dataEntity = new DeviceType();
                        dataEntity.setTypeName(getString(R.string.tv_all_device_type));
                        dataEntity.setTypeCode("");
                        mTypeList.add(dataEntity);
                        mTypeList.addAll(msg.getData());
                        mDeviceTypeAdapter = new DeviceManageTypeAdapter(this, mTypeList, categoryCode);
                        mDeviceTypeAdapter.setOnItemClickListener(new DeviceManageTypeAdapter.OnRecyclerViewItemClickListener() {
                            @Override
                            public void onItemClick(int position) {
                                categoryCode = mTypeList.get(position).getTypeCode();
                                mTvDeviceTypeName.setText(mTypeList.get(position).getTypeName());
                                closeArrow(2);
                                mDeviceList.clear();
                                currentPageDevice = 1;
                                getDeviceList();
                            }
                        });
                        mTypeRecyclerView.setVisibility(View.VISIBLE);
                        mTypeRecyclerView.setAdapter(mDeviceTypeAdapter);
                    } else {
                        showCommonNoticeDialog(this, getString(R.string.tv_moment_null));
                    }
                    break;
            }
        }
        //设备列表
        if (event.getTag().equals(Constants.TAG_DEVICE_LIST)) {
            DialogUtil.safeCloseDialog(mLoadDialog);
            setSwipeRefreshState(isRefreshedDevice, isLoadmoreDevice, 500);
            DeviceListBean msg = (DeviceListBean) event.getMsg();
            switch (event.getCls()) {
                case Constants.MSG_NET_ERROR:
                    ToastUtil.showToastShort(getResources().getString(R.string.net_error));
                    break;
                case Constants.ON_EVENT_FALSE:
                    if (msg.getError() != null) {
                        if (msg.getError().getCode() != null) {
                            if (msg.getError().getCode().equals(MyApplication.getFreeLogin())) {
                                if (msg.getError().getMessage() != null) {
                                    getLoginDialog(DeviceListActivity.this, msg.getError().getMessage());
                                }
                            } else {
                                if (msg.getError().getMessage() != null) {
                                    ToastUtil.showToastShort(msg.getError().getMessage());

                                }
                            }
                        }
                    }
                    break;
                case Constants.ON_EVENT_TRUE:
                    if (msg.getData() != null && msg.getData().getData() != null && msg.getData().getData().size() > 0) {
                        if (currentPageDevice == 1) {
                            mDeviceList.clear();
                        }
                        mDeviceList.addAll(msg.getData().getData());
                        mAdapter.notifyDataSetChanged();
                        currentPageDevice++;
                    } else {
                        if (isLoadmoreDevice) {
                            ToastUtil.showToastShort(UIUtils.getString(R.string.no_more_data));
                        } else {
                            mDeviceList.clear();//使后面会展示空视图
                            mAdapter.notifyDataSetChanged();
                        }

                    }
                    if (mDeviceList != null && mDeviceList.size() > 0) {
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mLlInput.setVisibility(View.VISIBLE);
                        mTvNull.setVisibility(View.GONE);
                    } else {
                        mSwipeRefreshLayout.setVisibility(View.GONE);
                        mTvNull.setVisibility(View.VISIBLE);
                        if (TextUtils.isEmpty(mEtInput.getText().toString())) {
                            mLlInput.setVisibility(View.GONE);
                        } else {
                            mLlInput.setVisibility(View.VISIBLE);
                        }
                    }

                    break;
            }
            isLoadmoreDevice = false;
            isRefreshedDevice = false;
        }

    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mEtInput.setText("");
    }

    //解除绑定提示
    public void showRemindUnbindDialog(DeviceBean deviceBean) {
        CustomViewFullScreenDialog unBindDialog = new CustomViewFullScreenDialog(this);
        unBindDialog.setView(R.layout.dialog_device_unbind_tip);
        Button btn_not_unbind = unBindDialog.findViewById(R.id.btn_not_unbind);
        Button btn_to_unbind = unBindDialog.findViewById(R.id.btn_to_unbind);
        unBindDialog.show();
        btn_not_unbind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unBindDialog.dismiss();
            }
        });
        btn_to_unbind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unBindDialog.dismiss();
                unBindDevice(deviceBean);
            }
        });
    }

    private void toMiniProgram() {
        if (AppHelper.isWeixinAvilible(MyApplication.getContext())) {
            String appId = "wx6e411e48e4dacb00"; // 填应用AppId
            IWXAPI api = WXAPIFactory.createWXAPI(DeviceListActivity.this, appId);
            WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
            req.userName = "gh_1d2ba02ad88f"; // 填小程序原始id
            req.path = "pages/device-mall/home/home";  //拉起小程序页面的可带参路径，不填默认拉起小程序首页，对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"。
            req.miniprogramType = WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE;// 可选打开 开发版，体验版和正式版
            api.sendReq(req);
        } else {
            ToastUtil.showToastShort("请先安装微信！");
        }
    }

    //关闭下拉加载的进度条
    private void setSwipeRefreshState(SHSwipeRefreshLayout shSwipeRefreshLayout, boolean isLoadMore, int delay) {
        if (isLoadMore) {
            shSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    shSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    /**
     * 关闭下拉刷新和上拉加载的进度条
     */
    private void setSwipeRefreshState(boolean isPullRefresh, boolean isLoadmore, int delay) {
        if (isPullRefresh) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishRefresh();
                }
            }, delay);
        }
        if (isLoadmore) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.finishLoadmore();
                }
            }, delay);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_BINDDEVICE) {
            if (resultCode == RESULT_OK) {
                //绑定设备成功
                currentPageDevice = 1;
                getDeviceList();
            }
        }
    }
}
