package com.hstypay.hstysales.base;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.AboutUsActivity;
import com.hstypay.hstysales.activity.LoginActivity;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.commonlib.http.ApiManager;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DialogHelper;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.PermissionUtils;
import com.hstypay.hstysales.utils.ShapeSelectorUtils;
import com.hstypay.hstysales.utils.SpStayUtil;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.CommonNoticeDialog;
import com.hstypay.hstysales.widget.NoticeDialog;
import com.hstypay.hstysales.widget.SafeDialog;
import com.hstypay.hstysales.widget.SelectCommonDialog;
import com.hstypay.hstysales.widget.SelectDialog;
import com.tencent.stat.MtaSDkException;
import com.tencent.stat.StatConfig;
import com.tencent.stat.StatService;

import org.greenrobot.eventbus.EventBus;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/6/27 11:40
 * @描述: ${TODO}
 */

public class BaseActivity extends FragmentActivity {

    private SafeDialog dialogNew;
    private CommonNoticeDialog mReoginDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.d("onCreate--" + this.getLocalClassName());
        MyApplication.getInstance().addActivity(this);

        SpUtil.init(MyApplication.getContext());
        SpStayUtil.init(MyApplication.getContext());
        if (isNeedEventBus()) {
            EventBus.getDefault().register(this);
        }
        initStatConfig();
        initWindows();
    }

    /**
     * 获取应用详情页面intent
     *
     * @return
     */
    public static Intent getAppDetailSettingIntent(Context context) {
        Intent localIntent = new Intent();
        localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= 9) {
            localIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            localIntent.setData(Uri.fromParts("package", context.getPackageName(), null));
        } else if (Build.VERSION.SDK_INT <= 8) {
            localIntent.setAction(Intent.ACTION_VIEW);
            localIntent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            localIntent.putExtra("com.android.settings.ApplicationPkgName", context.getPackageName());
        }
        return localIntent;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        StatService.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        StatService.onPause(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isNeedEventBus()) {
            EventBus.getDefault().unregister(this);
        }
        MyApplication.getInstance().removeActivity(this);
    }

    private void initWindows() {
        Window window = getWindow();
        //这里是自己定义状态栏的颜色
        int color = getResources().getColor(R.color.transparent);

        ViewGroup contentView = ((ViewGroup) findViewById(android.R.id.content));
        View childAt = contentView.getChildAt(0);
        if (childAt != null) {
            // 设置内容布局充满屏幕
            childAt.setFitsSystemWindows(true);
        }
        // 5.0及以上
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
            // 4.4到5.0
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // 透明状态栏
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            View view = new View(this);
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getStatusBarHeight(this)));
            view.setBackgroundColor(color);
            contentView.addView(view);
        }
    }

    /**
     * 获取状态栏高度
     *
     * @param context context
     * @return 状态栏高度
     */
    private static int getStatusBarHeight(Context context) {
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        return context.getResources().getDimensionPixelSize(resourceId);
    }

    //销毁加载
    public void dismissLoading() {
        if (null != dialogNew) {
            dialogNew.dismiss();
            dialogNew = null;
        }
    }

    //加载样式
    public void showLoading(boolean cancelble, String str) {
        if (!cancelble)
            return;
        loadDialog(BaseActivity.this, str);
    }

    //
    public void loadDialog(Activity context, String str) {
        // 加载样式
        if (null == dialogNew) {
            dialogNew = new SafeDialog(context, R.style.my_dialog);
            dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogNew.setCancelable(false);
            dialogNew.setCanceledOnTouchOutside(false);
            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
            TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
            tv.setText(str);
            // dialog透明
            WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
            lp.alpha = 0.6f;
            dialogNew.getWindow().setAttributes(lp);
            dialogNew.setContentView(v);
            DialogHelper.resizeNew(context, dialogNew);
        }
        dialogNew.show();
    }

    public SafeDialog getLoadDialog(Activity context, String str,boolean cancelable){
        SafeDialog dialogNew = new SafeDialog(context, R.style.my_dialog);
        dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogNew.setCancelable(cancelable);
        dialogNew.setCanceledOnTouchOutside(cancelable);
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
        TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
        tv.setText(str);
        // dialog透明
        WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
        lp.alpha = 0.6f;
        dialogNew.getWindow().setAttributes(lp);
        dialogNew.setContentView(v);
        DialogHelper.resizeNew(context, dialogNew);
        return dialogNew;
    }

    public SafeDialog getLoadDialog(Activity context, String str,boolean cancelable,float alpha){
        SafeDialog dialogNew = new SafeDialog(context, R.style.my_dialog);
        dialogNew.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogNew.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogNew.setCancelable(cancelable);
        dialogNew.setCanceledOnTouchOutside(cancelable);
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.load_progress_info, null);// 得到加载view dialog_common
        TextView tv = (TextView) v.findViewById(R.id.tv_dialog);
        tv.setText(str);
        // dialog透明
        WindowManager.LayoutParams lp = dialogNew.getWindow().getAttributes();
        lp.alpha = alpha;
        dialogNew.getWindow().setAttributes(lp);
        dialogNew.setContentView(v);
        DialogHelper.resizeNew(context, dialogNew);
        return dialogNew;
    }

    public void loginDialog(Activity context, String title) {
        NoticeDialog noticeDialog = new NoticeDialog(context, title, UIUtils.getString(R.string.btn_ensure_text),R.layout.notice_dialog_text);
        noticeDialog.setOnClickOkListener(new NoticeDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                MyApplication.iscache = false;
                SpUtil.removeKey(Constants.SKEY);
                SpUtil.removeAll();
                startActivity(new Intent(MyApplication.getContext(), LoginActivity.class));
                MyApplication.getInstance().finishAllActivity();
            }
        });
        DialogHelper.resize(context, noticeDialog);
        noticeDialog.show();
        noticeDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    public boolean isNeedEventBus() {
        return false;
    }

    public void initStatConfig(){
//        String appkey = "AR28XP9VN3YD";//测试MTA APP KEY
        String appkey = "A38NFTRHM14N";//正式APP KEY
        // 初始化并启动MTA
        try {
            initMTAConfig(false);//正式
//            initMTAConfig(true);
            // 第三个参数必须为：com.tencent.stat.common.StatConstants.VERSION
            StatService.startStatService(this, appkey,
                    com.tencent.stat.common.StatConstants.VERSION);
            LogUtil.i("zhouwei", "MTA初始化成功");
        } catch (MtaSDkException e) {
            // MTA初始化失败
            LogUtil.i("zhouwei", "MTA初始化失败" + e);
        }
    }

    /**
     * 根据不同的模式，建议设置的开关状态，可根据实际情况调整，仅供参考。
     *
     * @param isDebugMode 根据调试或发布条件，配置对应的MTA配置
     */
    private void initMTAConfig(boolean isDebugMode) {
        // logger.d("isDebugMode:" + isDebugMode);
        if (isDebugMode) { // 调试时建议设置的开关状态
            // 查看MTA日志及上报数据内容
            StatConfig.setDebugEnable(true);
            // 禁用MTA对app未处理异常的捕获，方便开发者调试时，及时获知详细错误信息。
            StatConfig.setAutoExceptionCaught(false);
            // StatConfig.setEnableSmartReporting(false);
            // Thread.setDefaultUncaughtExceptionHandler(new
            // UncaughtExceptionHandler() {
            //
            // @Override
            // public void uncaughtException(Thread thread, Throwable ex) {
            // logger.error("setDefaultUncaughtExceptionHandler");
            // }
            // });
            // 调试时，使用实时发送
            // StatConfig.setStatSendStrategy(StatReportStrategy.BATCH);
            // // 是否按顺序上报
            // StatConfig.setReportEventsByOrder(false);
            // // 缓存在内存的buffer日志数量,达到这个数量时会被写入db
            // StatConfig.setNumEventsCachedInMemory(30);
            // // 缓存在内存的buffer定期写入的周期
            // StatConfig.setFlushDBSpaceMS(10 * 1000);
            // // 如果用户退出后台，记得调用以下接口，将buffer写入db
            // StatService.flushDataToDB(getApplicationContext());

            // StatConfig.setEnableSmartReporting(false);
            // StatConfig.setSendPeriodMinutes(1);
            // StatConfig.setStatSendStrategy(StatReportStrategy.PERIOD);
        } else { // 发布时，建议设置的开关状态，请确保以下开关是否设置合理
            // 禁止MTA打印日志
            StatConfig.setDebugEnable(false);
            // 根据情况，决定是否开启MTA对app未处理异常的捕获
            StatConfig.setAutoExceptionCaught(true);
            // StatConfig.setInstallChannel("InstallChannel");
            // 选择默认的上报策略
            //StatConfig.setStatSendStrategy(StatReportStrategy.APP_LAUNCH);
        }
    }

    public void showCommonNoticeDialog(Activity activity, String title) {
        CommonNoticeDialog dialog = new CommonNoticeDialog(activity, title, "");
        DialogHelper.resize(activity, dialog);
        dialog.show();
    }

    public void showCommonNoticeDialog(Activity activity, String title, String content, CommonNoticeDialog.OnClickOkListener onClickOkListener) {
        CommonNoticeDialog dialog = new CommonNoticeDialog(activity, title, content, "");
        if (onClickOkListener != null) {
            dialog.setOnClickOkListener(onClickOkListener);
        }
        DialogHelper.resize(activity, dialog);
        dialog.show();
    }


    public void getLoginDialog(Context context, String title) {
        if (mReoginDialog == null) {
            mReoginDialog = new CommonNoticeDialog(context, title, null);
            mReoginDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
                @Override
                public void onClickOk() {
                    MyApplication.iscache = false;
                    SpUtil.removeKey(Constants.SKEY);
                    SpUtil.removeAll();
                    ApiManager.get().setMService(null);
                    startActivity(new Intent(MyApplication.getContext(), LoginActivity.class));
                    MyApplication.getInstance().finishAllActivity();
                }
            });
        }
        DialogHelper.resize(context, mReoginDialog);
        mReoginDialog.show();
    }


    public void setButtonEnable(Button button, boolean enable) {
        if (enable) {
            Drawable rectangleDefault = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), MyApplication.themeColor(), 25);
            String colorClickedString = "#" + Integer.toHexString(ContextCompat.getColor(MyApplication.getContext(), R.color.theme_color_clicked));
            Drawable rectanglePressed = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), colorClickedString, 25);
            rectanglePressed.setAlpha(204);
            StateListDrawable stateListDrawable = ShapeSelectorUtils.makeSelector(rectangleDefault, rectanglePressed, rectanglePressed);
            button.setBackground(stateListDrawable);
            button.setEnabled(true);
        } else {
            Drawable rectangleUnable = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), MyApplication.themeColor(), 25);
            rectangleUnable.setAlpha(102);
            button.setBackground(rectangleUnable);
            button.setEnabled(false);
        }
    }
    public void setButtonWithColor(Button button, int color) {
        String colorString = "#" + Integer.toHexString(ContextCompat.getColor(MyApplication.getContext(), color));
        Drawable rectangleDefault = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), colorString, 25);
        Drawable rectanglePressed = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), colorString, 25);
        rectanglePressed.setAlpha(204);
        StateListDrawable stateListDrawable = ShapeSelectorUtils.makeSelector(rectangleDefault, rectanglePressed, rectanglePressed);
        button.setBackground(stateListDrawable);
        button.setEnabled(true);
    }

    public void setButtonWhite(Button button) {
        String colorString = "#" + Integer.toHexString(ContextCompat.getColor(MyApplication.getContext(), R.color.white));
        Drawable rectangleDefault = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), colorString, 25);
        Drawable rectanglePressed = ShapeSelectorUtils.createRectangleWithRadius(MyApplication.getContext(), colorString, 25);
        rectanglePressed.setAlpha(204);
        StateListDrawable stateListDrawable = ShapeSelectorUtils.makeSelector(rectangleDefault, rectanglePressed, rectanglePressed);
        button.setBackground(stateListDrawable);
        button.setEnabled(true);
    }

    protected void showNotice(final int requestCode, final String[] permissionArray, String content) {
        if (isFinishing()) {
            return;
        }
        CommonNoticeDialog commonNoticeDialog = new CommonNoticeDialog(BaseActivity.this
                , content, getString(R.string.btn_know));
        commonNoticeDialog.setOnClickOkListener(new CommonNoticeDialog.OnClickOkListener() {
            @Override
            public void onClickOk() {
                PermissionUtils.checkPermissionArray(BaseActivity.this, permissionArray, requestCode);
            }
        });
        DialogHelper.resize(this, commonNoticeDialog);
        commonNoticeDialog.show();
    }

    protected void showDialog(String content) {
        if (isFinishing()) {
            return;
        }
        SelectCommonDialog dialog = new SelectCommonDialog(this, content
                , getString(R.string.btn_cancel_text), getString(R.string.btn_setting), R.layout.select_common_dialog);
        dialog.setOnClickOkListener(new SelectCommonDialog.OnClickOkListener() {
            @Override
            public void clickOk() {
                Intent intent = getAppDetailSettingIntent(BaseActivity.this);
                startActivity(intent);
            }
        });
        DialogHelper.resize(this, dialog);
        dialog.show();
    }
}
