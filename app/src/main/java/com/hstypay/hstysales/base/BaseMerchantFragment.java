package com.hstypay.hstysales.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.utils.DialogUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.NetworkUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;

import org.greenrobot.eventbus.EventBus;


public abstract class BaseMerchantFragment extends BaseFragment {
	public String mServiceProviderId;
	public String mSalesmanId;

	public void setFilter(String serviceProviderId, String salesmanId) {
		mServiceProviderId = serviceProviderId;
		mSalesmanId = salesmanId;
	}

	public abstract void refreshData();
}
