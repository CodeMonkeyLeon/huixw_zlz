package com.hstypay.hstysales.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.hstysales.utils.LogUtil;

import org.greenrobot.eventbus.EventBus;


public abstract class BaseFragment extends Fragment {

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		LogUtil.d("aa");

		if(isNeedEventBus()){
			EventBus.getDefault().register(this);
		}
		return loadSuccessView(inflater,container);
	}



	/**
	 * 前进到下一个Fragment之前被调用
	 *
	 * @return true - 允许跳转到下一个页面
	 */
	public boolean preGoForward() {
		return true;
	}

	/**
	 * 使用成功数据刷新视图
	 *
	 * @param data 数据
	 */
	//protected abstract void refreshSuccessView(T data);

	/**
	 * 子类实现这个方法加载成功视图
	 *
	 * @return 视图
	 */
	protected abstract View loadSuccessView(LayoutInflater inflater,ViewGroup container);

	/**
	 * 子类实现这个方法加载数据
	 * 成功失败都必须调用listener的相对应方法
	 *
	 * @param listener 监听
	 */
	//protected abstract void loadData(LoadListener<T> listener);

	/**
	 * 处理异常
	 *
	 * 异常
	 */
	//protected abstract void handleError(Exception e);



	public boolean isNeedEventBus() {
		return false;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		LogUtil.d("des");
		if(isNeedEventBus()){
			EventBus.getDefault().unregister(this);
		}
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		boolean change = isVisibleToUser != getUserVisibleHint();
		super.setUserVisibleHint(isVisibleToUser);
		if (change && isResumed() && isVisible()) {
			if (getUserVisibleHint()) {
				onFragmentVisible();
			} else {
				onFragmentInVisible();
			}
		}

	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (hidden) {
			onFragmentInVisible();
		} else {
			onFragmentVisible();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (getUserVisibleHint() && !isHidden()) {
			onFragmentVisible();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (getUserVisibleHint() && !isHidden()) {
			onFragmentInVisible();
		}
	}

	/**
	 * 对用户不可见时调用
	 */
	protected void onFragmentInVisible() {
		LogUtil.d(this.getClass().getSimpleName(), "onFragmentInVisible");
	}
	/**
	 * 对用户可见时调用
	 */
	protected void onFragmentVisible() {
		LogUtil.d(this.getClass().getSimpleName(), "onFragmentVisible");
	}
}
