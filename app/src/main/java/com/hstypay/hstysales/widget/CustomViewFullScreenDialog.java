package com.hstypay.hstysales.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.hstypay.hstysales.R;

/**
 * @author zeyu.kuang
 * @time 2020/8/26
 * @desc 可自定义布局的全屏弹窗dialog
 */
public class CustomViewFullScreenDialog extends Dialog {


    public CustomViewFullScreenDialog(@NonNull Context context) {
        this(context, R.style.custom_full_dialog);
    }

    public CustomViewFullScreenDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }


    public CustomViewFullScreenDialog setView(int layoutResID){
        setContentView(layoutResID);//可使用dialog.findViewById找控件
//        View mView = LayoutInflater.from(getContext()).inflate(layoutResID, null);// 不适合在根布局上设置margin
//        setContentView(mView);
        return this;
    }

    public CustomViewFullScreenDialog setView(View view){
        setContentView(view);
        return this;
    }

    public View getContentView(){
        return getWindow().getDecorView().findViewById(android.R.id.content);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().getDecorView().setPadding(0,0,0,0);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(params);
    }

    /**
     * 设置点击内容view的外部区域时dialog消失
     * 作用类似setCanceledOnTouchOutside(true)
     */
    public void setClickOutDismiss(){
        View contentView = getContentView();
        if (contentView instanceof ViewGroup){
            final View rootView = ((ViewGroup) contentView).getChildAt(0);
            rootView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (rootView instanceof ViewGroup){
                            ViewGroup viewGroup = (ViewGroup) rootView;
                            int childCount = viewGroup.getChildCount();
                            int getX = (int) event.getX();
                            int getY = (int) event.getY();
                            boolean isClickOut = false;
                            for (int i=0;i<childCount;i++){
                                View childView = viewGroup.getChildAt(i);
                                int[] loc = new int[2];
                                childView.getLocationInWindow(loc);
                                Rect rect = new Rect(loc[0],loc[1],loc[0]+childView.getWidth(),loc[1]+childView.getHeight());
                                if (!rect.contains(getX,getY)){
                                    isClickOut = true;
                                }else {
                                    isClickOut = false;
                                }
                            }
                            if (isClickOut){
                                dismiss();
                            }
                        }
                    }
                    return true;
                }
            });
        }

    }

}
