package com.hstypay.hstysales.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.Widget
 * @创建者: Jeremy
 * @创建时间: 2017/7/22 9:48
 * @描述: ${TODO}
 */

public class CustomLinearLayoutManager extends LinearLayoutManager {
    private boolean isScrollEnabled = true;

    public CustomLinearLayoutManager(Context context) {
        super(context);
    }

    public void setScrollEnabled(boolean flag) {
        this.isScrollEnabled = flag;
    }

    @Override
    public boolean canScrollVertically() {
        return isScrollEnabled && super.canScrollVertically();
    }
}
