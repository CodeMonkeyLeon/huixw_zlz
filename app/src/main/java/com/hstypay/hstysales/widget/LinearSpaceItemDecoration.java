package com.hstypay.hstysales.widget;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * @author zeyu.kuang
 * @time 2020/10/26
 * @desc 设置LinearLayoutManager时recycleView的item间隔
 */
public class LinearSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private int space;//item 间隔
    private boolean needTopSpace;//第一个item与顶部控件之间是否需要间隔
    public LinearSpaceItemDecoration(int space,boolean needTopSpace){
        this.space = space;
        this.needTopSpace = needTopSpace;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        outRect.bottom = space;
        if (parent.getChildAdapterPosition(view) == 0 && needTopSpace){
            //给第一个子项设置上方向的间隔
            outRect.top = space;
        }
    }
}
