package com.hstypay.hstysales.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.hstypay.hstysales.R;


/**
 * Created by admin on 2017/7/12.
 */

public class CheckPictureDialog extends Dialog {

    Context context;
    private TextView ok;

    public CheckPictureDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_check_picture);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        this.setCanceledOnTouchOutside(false);
        this.context = context;
        ok = (TextView) findViewById(R.id.ok);

        setLister();
    }

    private void setLister() {
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

}
