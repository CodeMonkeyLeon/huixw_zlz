package com.hstypay.hstysales.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.hstypay.hstysales.R;

import static android.content.Context.WINDOW_SERVICE;

/**
 * <style name="My_Dialog_Theme" parent="Theme.AppCompat.Dialog">
 * <item name="android:windowBackground">@android:color/transparent</item>
 * <item name="windowNoTitle">true</item>
 * </style>
 */
public class OpenOrangeDialog extends Dialog {
    //TAG
    protected String TAG = OpenOrangeDialog.class.getCanonicalName();

    //子View的集合
    private SparseArray<View> mViews;

    //
    private final WindowManager.LayoutParams layoutParams;
    private final WindowManager wm;

    //用于获取Builder 参数
    final Context context; //上下文
    final boolean widthMatch; //弹窗宽度是否MATCH_PARENT
    final int gravity; //居中方式
    final boolean cancelable; //Dialog 是否支持自动关闭

    //选项类型
    private int currentType;
    private String currentTypeDes;
    public static final int IGNORE = -1;
    public static final int ITEM_NO_OPEN = 0;
    public static final int ITEM_ORANGE = 1;
    public static final int ITEM_ORANGE_SMOOTH = 2;

    //接口
    private HandlerListener handlerListener = null;

    //处理取消/确定事件的接口
    public interface HandlerListener {
        default void afterCancel() { } //可复写

        void handleOk(int type, String typeStr, String typeDes); //必须实现
    }

    //设置事件监听器
    public void setHandlerListener(HandlerListener listener) {
        if (listener != null) {
            handlerListener = listener;
        }
    }

    /**
     * 提供给外部访问View的方法
     *
     * @param viewId
     * @param <T>
     * @return
     */
    public <T extends View> T getView(int viewId) {
        View view = mViews.get(viewId);
        if (view == null) {
            view = this.findViewById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * 设置控件的点击事件
     *
     * @param viewId
     * @param listener
     */
    public void setClick(final int viewId, View.OnClickListener listener) {
        View view = getView(viewId);
        if (view == null || listener == null) return;
        view.setOnClickListener(listener);
    }

    /**
     * 设置TextView文本
     *
     * @param viewId
     * @param text
     */
    public void setText(final int viewId, String text) {
        if (text == null) return;
        TextView textView = getView(viewId);
        textView.setText(text);
    }

    /**
     * 设置TextView文本
     *
     * @param viewId
     * @param stringId
     */
    public void setText(final int viewId, int stringId) {
        if (context == null) return;
        String string = context.getResources().getString(stringId);
        setText(viewId, string);
    }

    /**
     * 设置背景颜色
     *
     * @param viewId
     * @param color
     */
    public void setBackgroundColor(final int viewId, int color) {
        if (context == null) return;
        View view = getView(viewId);
        view.setBackgroundColor(context.getResources().getColor(color));
    }

    /**
     * 设置背景颜色
     *
     * @param viewId
     * @param colorStr
     */
    public void setBackgroundColor(final int viewId, String colorStr) {
        if (colorStr == null || TextUtils.isEmpty(colorStr)) return;
        View view = getView(viewId);
        view.setBackgroundColor(Color.parseColor(colorStr));
    }

    /**
     * 设置TextView文本颜色
     *
     * @param viewId
     * @param color
     */
    public void setTextColor(final int viewId, int color) {
        if (context == null) return;
        TextView textView = getView(viewId);
        textView.setTextColor(context.getResources().getColor(color));
    }

    /**
     * 设置TextView文本颜色
     *
     * @param viewId
     * @param colorStr
     */
    public void setTextColor(final int viewId, String colorStr) {
        if (colorStr == null || TextUtils.isEmpty(colorStr)) return;
        TextView textView = getView(viewId);
        textView.setTextColor(Color.parseColor(colorStr));
    }

    /**
     * 设置背景样式
     * 注意：如果shape是有点击样式的，则单纯设置setBackgroundShape会无法显示state_pressed的样式，
     * state_pressed样式需要该控件setClickable(true)或有点击事件监听器才会起作用
     *
     * @param viewId
     * @param shape
     * @return
     */
    public void setBackgroundShape(final int viewId, int shape) {
        if (context == null) return;
        View view = getView(viewId);
        Drawable drawable = context.getResources().getDrawable(shape);
        if (drawable != null) {
            view.setBackground(drawable);
        }
    }

    //显示Dialog
    public void showDialog() {
        if (this != null && !isShowing()) {
            Log.i(TAG, "show");
            show();
        }
    }

    //隐藏Dialog
    public void hideDialog() {
        if (this != null && isShowing()) {
            Log.i(TAG, "hide");
            hide();
        }
    }

    //销毁Dialog
    public void dismissDialog() {
        if (this != null) {
            Log.i(TAG, "dismiss");
            dismiss();
            mViews = null;
        }
    }

    private OpenOrangeDialog(Builder builder) {
        //初始化
        super(builder.context, R.style.My_Dialog_Theme);
        //获取参数
        this.context = builder.context;
        this.widthMatch = builder.widthMatch;
        this.gravity = builder.gravity;
        this.cancelable = builder.cancelable;
        //设置布局
        setContentView(R.layout.dialog_open_orange);
        mViews = new SparseArray<>();
        //获取父容器
        Window window = getWindow();
        layoutParams = window.getAttributes();
        wm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        layoutParams.width = widthMatch ? WindowManager.LayoutParams.MATCH_PARENT : WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = gravity;
        window.setAttributes(layoutParams);
        setCancelable(cancelable);
        //初始化事件
        initEvent(window.getDecorView());
    }

    //初始化事件
    private void initEvent(View decorView) {
        //默认选择不开通
        currentType = ITEM_NO_OPEN;
        currentTypeDes = getTypeDes(currentType);
        setSelectView(R.id.tv_not_open);
        Log.i(TAG, "initEvent");
        setOnCancelListener(dialogInterface -> {
            dismissDialog();
        });
        setOnKeyListener((OnKeyListener) (dialog, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                if (cancelable) {
                    dismissDialog();
                    return true;
                }
            }
            return false;
        });
        setClick(R.id.tv_cancel, view -> {
            dismissDialog();
            if (handlerListener != null) handlerListener.afterCancel();
        });
        setClick(R.id.tv_sure, view -> {
            dismissDialog();
            if (handlerListener != null)
                handlerListener.handleOk(currentType, String.valueOf(currentType), currentTypeDes);
        });
        setClick(R.id.tv_not_open, view -> {
            currentType = ITEM_NO_OPEN;
            currentTypeDes = getTypeDes(currentType);
            setSelectView(R.id.tv_not_open, R.id.tv_orange, R.id.tv_orange_smooth);
        });
        setClick(R.id.tv_orange, view -> {
            currentType = ITEM_ORANGE;
            currentTypeDes = getTypeDes(currentType);
            setSelectView(R.id.tv_orange, R.id.tv_not_open, R.id.tv_orange_smooth);
        });
        setClick(R.id.tv_orange_smooth, view -> {
            currentType = ITEM_ORANGE_SMOOTH;
            currentTypeDes = getTypeDes(currentType);
            setSelectView(R.id.tv_orange_smooth, R.id.tv_not_open, R.id.tv_orange);
        });
    }

    //首个为选中
    public void setSelectView(int... viewIds) {
        for (int i = 0; i < viewIds.length; i++) {
            if (i == 0) setBackgroundColor(viewIds[i], "#f2f2f2");
            else setBackgroundColor(viewIds[i], "#ffffff");
        }
    }

    //建造者模式
    public static class Builder {
        Context context;
        boolean widthMatch;
        int gravity;
        boolean cancelable;

        /**
         * 默认配置
         *
         * @param context
         */
        public Builder(Context context) {
            this.context = context;
            this.widthMatch = true;
            this.gravity = Gravity.BOTTOM;
            this.cancelable = true;
        }

        /**
         * @param widthMatch 弹窗宽度是否 MATCH_PARENT
         * @return
         */
        public Builder widthMatch(boolean widthMatch) {
            this.widthMatch = widthMatch;
            return this;
        }

        /**
         * @param gravity 居中方式
         * @return
         */
        public Builder gravity(int gravity) {
            this.gravity = gravity;
            return this;
        }

        /**
         * 是否支持自动取消
         * 建议使用这种方式而不是直接对Dialog对象设置setCancelable，
         *
         * @param cancelable 是否支持自动取消
         * @return
         */
        public Builder cancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        /**
         * @return
         */
        public OpenOrangeDialog build() {
            return new OpenOrangeDialog(this);
        }
    }

    //创建Builder对象
    public static Builder newBuilder(Context context) {
        return new Builder(context);
    }

    //获取橙意宝类型文案
    private String getTypeDes(int type) {
        if (type == OpenOrangeDialog.ITEM_NO_OPEN) {
            return context.getResources().getString(R.string.no_open);
        } else if (type == OpenOrangeDialog.ITEM_ORANGE) {
            return context.getResources().getString(R.string.orange_service);
        } else if (type == OpenOrangeDialog.ITEM_ORANGE_SMOOTH) {
            return context.getResources().getString(R.string.orange_smooth_service);
        } else {
            return "";
        }
    }

}