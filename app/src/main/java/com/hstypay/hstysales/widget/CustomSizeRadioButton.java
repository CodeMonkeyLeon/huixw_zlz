package com.hstypay.hstysales.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.utils.DensityUtils;

/**
 * @author zeyu.kuang
 * @time 2020/9/14
 * @desc 可设置RadioButton的Button图标宽高
 */
@SuppressLint("AppCompatCustomView")
public class CustomSizeRadioButton extends RadioButton {

    private float mImg_width;
    private float mImg_height;

    public CustomSizeRadioButton(Context context) {
        super(context);
    }

    public CustomSizeRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.CustomSizeRadioButton);
        mImg_width = t.getDimension(R.styleable.CustomSizeRadioButton_rb_width, 0);
        mImg_height = t.getDimension(R.styleable.CustomSizeRadioButton_rb_height, 0);
        t.recycle();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //让RadioButton的图标可调大小 属性：
        Drawable drawableLeft = this.getCompoundDrawables()[0];//获得文字左侧图片
        Drawable drawableTop = this.getCompoundDrawables()[1];//获得文字顶部图片
        Drawable drawableRight = this.getCompoundDrawables()[2];//获得文字右侧图片
        Drawable drawableBottom = this.getCompoundDrawables()[3];//获得文字底部图片
        if (drawableLeft != null && mImg_height!=0 && mImg_width!=0) {
            drawableLeft.setBounds(0, 0, (int) mImg_width, (int) mImg_height);
            this.setCompoundDrawables(drawableLeft, null, null, null);
        }
        if (drawableRight != null&& mImg_height!=0 && mImg_width!=0) {
            drawableRight.setBounds(0, 0, (int) mImg_width, (int) mImg_height);
            this.setCompoundDrawables(null, null, drawableRight, null);
        }
        if (drawableTop != null&& mImg_height!=0 && mImg_width!=0) {
            drawableTop.setBounds(0, 0, (int) mImg_width, (int) mImg_height);
            this.setCompoundDrawables(null, drawableTop, null, null);
        }
        if (drawableBottom != null&& mImg_height!=0 && mImg_width!=0) {
            drawableBottom.setBounds(0, 0, (int) mImg_width, (int) mImg_height);
            this.setCompoundDrawables(null, null, null, drawableBottom);
        }
    }

}
