package com.hstypay.hstysales.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.utils.DensityUtils;

/**
 * @author zeyu.kuang
 * @time 2020/8/23
 * @desc 镂空圆形
 */
public class CircleHollowOutView extends View {

    private final int defColor = 0xFFFFFFFF;
    private Paint mPaint;
    private int mOutColor = defColor;//镂空外的颜色
    private int mCha;

    public CircleHollowOutView(Context context) {
        this(context, null);
    }

    public CircleHollowOutView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public CircleHollowOutView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CicleHollowOutViewAttrs);
        if (typedArray != null) {
            mOutColor = typedArray.getColor(R.styleable.CicleHollowOutViewAttrs_outColor, defColor);
            typedArray.recycle();
        }
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(mOutColor);
        mCha = DensityUtils.dip2px(getContext(), 15);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        RectF rectF = new RectF(0, 0, measuredWidth, measuredHeight);
        canvas.saveLayer(0, 0, measuredWidth, measuredHeight, null, Canvas.ALL_SAVE_FLAG);
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));
        canvas.drawCircle(measuredWidth / 2f, -mCha, (float) (measuredHeight + mCha), mPaint);//下层
        canvas.drawRect(rectF, mPaint);//上层
        mPaint.setXfermode(null);
        canvas.restore();
    }
}
