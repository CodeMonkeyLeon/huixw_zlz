package com.hstypay.hstysales.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.utils.DensityUtils;

/**
 * @项目名: LightPos
 * @包名: com.liaowangta.lightpos.view
 * @创建者: Ashin
 * @创建时间: 2016/8/29 11:29
 * @描述: ${TODO}
 */
@SuppressLint("AppCompatCustomView")
public class EditTextDelete extends EditText {
    private final static String TAG = "EditTextWithDel";
    private Drawable imgDisable;
    private Drawable imgEnable;
    private Context mContext;
    private int padding;
    private final static int SPACE = 5;
    private Drawable left, top, right, bottom;
    private OnEditChangedListener mOnEditChangedListener;
    private OnEditClearListener mOnEditClearListener;
    private boolean enable = true;

    public EditTextDelete(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public EditTextDelete(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init();
    }

    public EditTextDelete(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    private void init() {
        Drawable[] drawable = getCompoundDrawables();
        left = drawable[0];
        top = drawable[1];
        right = drawable[2];
        bottom = drawable[3];
        imgDisable = mContext.getResources().getDrawable(R.mipmap.ic_edit_clear_grey);
        imgEnable = mContext.getResources().getDrawable(R.mipmap.ic_edit_clear_grey);
        padding = DensityUtils.dip2px(mContext, SPACE);
        addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mOnEditChangedListener != null) {
                    mOnEditChangedListener.onEditChanged(s.length() == 0 ? true : false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                //setDrawable();
                if (enable) {
                    if (hasFocus()) {//获得焦点
                        if (getText().length() > 0) {
                            setDrawable();
                        } else {
                            setImageGone();
                        }
                    } else {//失去焦点
                        setImageGone();
                    }
                } else {
                    setImageGone();
                }
            }
        });
        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (enable) {
                    if (hasFocus) {//获得焦点
                        if (getText().length() > 0) {
                            setDrawable();
                        } else {
                            setImageGone();
                        }
                    } else {//失去焦点
                        setImageGone();
                    }
                } else {
                    setImageGone();
                }
            }
        });

    }

    public void setImageGone() {
        setCompoundDrawablesWithIntrinsicBounds(left, top, null, bottom);
    }

    // 设置删除图片
    private void setDrawable() {
        if (length() >= 1) {
            setCompoundDrawablesWithIntrinsicBounds(left, top, imgDisable, bottom);
        } else {
            setCompoundDrawablesWithIntrinsicBounds(left, top, null, bottom);
            if (mOnEditClearListener != null) {
                mOnEditClearListener.onEditClear();
            }
        }
        setCompoundDrawablePadding(padding);//设置图片与内容间隔
    }

    // 处理删除事件
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (enable) {
            if (imgEnable != null && event.getAction() == MotionEvent.ACTION_UP) {
                int eventX = (int) event.getX();
                int eventY = (int) event.getY();
                Log.e(TAG, "eventX = " + eventX + "; eventY = " + eventY);
                Rect rect = new Rect();
                getLocalVisibleRect(rect);
                rect.left = rect.right - 50 - getPaddingRight();
                rect.right = rect.left + 70;
                Log.e(TAG, "left=" + rect.left + ",right=" + rect.right + ",top=" + rect.top + ",bottom=" + rect.bottom);
                if (rect.contains(eventX, eventY)) setText("");
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    public interface OnEditChangedListener {
        void onEditChanged(boolean isClear);
    }

    public void setOnEditChangedListener(OnEditChangedListener onEditChangedListener) {
        this.mOnEditChangedListener = onEditChangedListener;
    }

    ;

    public interface OnEditClearListener {
        void onEditClear();
    }

    public void setOnEditClearListener(OnEditClearListener onEditClearListener) {
        this.mOnEditClearListener = onEditClearListener;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
        setEnabled(enable);
        //setImageGone();
    }

    public void setClearImage(int res){
        imgDisable = mContext.getResources().getDrawable(res);
        imgEnable = mContext.getResources().getDrawable(res);
    }
}

