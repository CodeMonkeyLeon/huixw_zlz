/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.hstysales.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.adapter.ChoiceAdapter;
import com.hstypay.hstysales.bean.ChoiceBean;
import com.hstypay.hstysales.utils.StringUtils;

import java.util.List;

public class ChoiceDialog extends Dialog {
    private Context context;
    private ViewGroup mRootView;
    private HandleBtn handleBtn;
    private ListView list_view;
    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public ChoiceDialog(Context context, List<ChoiceBean> title, HandleBtn handleBtn) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_choice, null);
        //this.setCanceledOnTouchOutside(true);
       // this.setCancelable(false);
        setContentView(mRootView);
        this.context = context;
        this.handleBtn = handleBtn;
        initView(title);
        setLinster();
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {

        if (MotionEvent.ACTION_OUTSIDE == event.getAction()) {
            dismiss();
            return true;
        }
        return super.onTouchEvent(event);
    }

    /**
     * 设置监听
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLinster() {


    }

    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView(final List<ChoiceBean> list) {

        list_view = findViewById(R.id.list_view);
        ChoiceAdapter adapter=new ChoiceAdapter(context,list,null);
        list_view.setAdapter(adapter);

        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                ChoiceBean bean = list.get(i);

                handleBtn.handleOkBtn(bean);
            }
        });
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn(ChoiceBean s);
    }
}
