package com.hstypay.hstysales.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import com.hstypay.hstysales.R;

/**
 * @author zeyu.kuang
 * @time 2020/9/7
 * @desc
 */
public class CustomViewCenterDialog extends Dialog {

    public CustomViewCenterDialog(@NonNull Context context) {
        this(context, R.style.bottom_dialog);
    }

    public CustomViewCenterDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    public CustomViewCenterDialog setView(int layoutResID) {
        setContentView(layoutResID);//可使用dialog.findViewById找控件
        return this;
    }

    public CustomViewCenterDialog setView(View view) {
        setContentView(view);
        return this;
    }

    public View getContentView() {
        return getWindow().getDecorView().findViewById(android.R.id.content);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);
    }
}