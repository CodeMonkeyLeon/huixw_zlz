package com.hstypay.hstysales.widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.ToastUtil;


/**
 * 弹出提示框
 * <功能详细描述>
 * 
 * @author  he_hui
 * @version  [版本号, 2013-3-11]
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class ExchangeDialog extends Dialog implements View.OnClickListener {

    private ViewGroup mRootView;
    private Activity mContext;
    private TextView mTvDevelop,mTvTest,mTvUnion,mTvOld,mTvNew,mTvGray,mTvFormal,mTvLocal,mTvHost,mTvService;
    private ImageView mIvClose;
    private EditTextDelete mEtId,mEtType,mEtAppCode,mEtH5Url;
    private Button mBtnEnsure;
    private EditTextDelete mEt_providerid;
    private TextView mTvMoc;
    private EditText etHost;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public ExchangeDialog(Activity context)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.exchange_dialog_url, null);
        this.setCancelable(false);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);

        this.mContext = context;
        initView();
        initListener();
    }

    private void initView(){
        mIvClose = (ImageView) mRootView.findViewById(R.id.iv_close);
        mTvDevelop = (TextView) mRootView.findViewById(R.id.tv_develop);
        mTvTest = (TextView) mRootView.findViewById(R.id.tv_test);
        mTvUnion = (TextView) mRootView.findViewById(R.id.tv_union);
        mTvOld = (TextView) mRootView.findViewById(R.id.tv_old);
        mTvNew = (TextView) mRootView.findViewById(R.id.tv_new);
        mTvGray = (TextView) mRootView.findViewById(R.id.tv_gray);
        mTvFormal = (TextView) mRootView.findViewById(R.id.tv_formal);
        mTvLocal = (TextView) mRootView.findViewById(R.id.tv_local);
        mTvHost = (TextView) mRootView.findViewById(R.id.tv_host);
        mEt_providerid = findViewById(R.id.et_providerid);
        mEtId = (EditTextDelete) mRootView.findViewById(R.id.et_id);
        mEtType = (EditTextDelete) mRootView.findViewById(R.id.et_type);
        mEtAppCode = (EditTextDelete) mRootView.findViewById(R.id.et_app_code);
        mEtH5Url = (EditTextDelete) mRootView.findViewById(R.id.et_h5_url);
        mBtnEnsure = (Button) mRootView.findViewById(R.id.btn_ensure);
        mTvMoc = (TextView) mRootView.findViewById(R.id.tv_moc);
        mTvService = (TextView) mRootView.findViewById(R.id.tv_service);
        etHost = mRootView.findViewById(R.id.et_host);

        etHost.setText(Constants.BASE_URL);
        mEtId.setText(Constants.ORG_ID);
        mEtType.setText(Constants.ORG_TYPE);
        mEtAppCode.setText("2");

    }

    private void initListener(){
        mIvClose.setOnClickListener(this);
        mTvDevelop.setOnClickListener(this);
        mTvTest.setOnClickListener(this);
        mTvUnion.setOnClickListener(this);
        mTvOld.setOnClickListener(this);
        mTvNew.setOnClickListener(this);
        mTvGray.setOnClickListener(this);
        mTvFormal.setOnClickListener(this);
        mTvLocal.setOnClickListener(this);
        mBtnEnsure.setOnClickListener(this);
        mTvMoc.setOnClickListener(this);
        mTvService.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_close:
                dismiss();
                break;
            case R.id.tv_develop:
                Constants.setBaseUrl("https://salesman.hstydev.com",mEt_providerid.getText().toString().trim(),mEtId.getText().toString().trim()
                        ,mEtType.getText().toString().trim(),mEtAppCode.getText().toString().trim(),mEtH5Url.getText().toString().trim());
                ToastUtil.showToastShort(Constants.BASE_URL);
                etHost.setText(Constants.BASE_URL);
                break;
            case R.id.tv_test:
                Constants.setBaseUrl("https://salesman.hstytest.com",mEt_providerid.getText().toString().trim(),mEtId.getText().toString().trim()
                        ,mEtType.getText().toString().trim(),mEtAppCode.getText().toString().trim(),mEtH5Url.getText().toString().trim());
                ToastUtil.showToastShort(Constants.BASE_URL);
                etHost.setText(Constants.BASE_URL);
                break;
            case R.id.tv_union:
                Constants.setBaseUrl("https://usalesman.hstytest.com",mEt_providerid.getText().toString().trim(),mEtId.getText().toString().trim()
                        ,mEtType.getText().toString().trim(),mEtAppCode.getText().toString().trim(),mEtH5Url.getText().toString().trim());
                ToastUtil.showToastShort(Constants.BASE_URL);
                etHost.setText(Constants.BASE_URL);
                break;
            case R.id.tv_service:
                Constants.setBaseUrl("https://isalesman.hstytest.com",mEt_providerid.getText().toString().trim(),mEtId.getText().toString().trim()
                        ,mEtType.getText().toString().trim(),mEtAppCode.getText().toString().trim(),mEtH5Url.getText().toString().trim());
                ToastUtil.showToastShort(Constants.BASE_URL);
                etHost.setText(Constants.BASE_URL);
                break;
            case R.id.tv_old:
                Constants.setBaseUrl("https://salesman-old.hstytest.com",mEt_providerid.getText().toString().trim(),mEtId.getText().toString().trim()
                        ,mEtType.getText().toString().trim(),mEtAppCode.getText().toString().trim(),mEtH5Url.getText().toString().trim());
                ToastUtil.showToastShort(Constants.BASE_URL);
                etHost.setText(Constants.BASE_URL);
                break;
            case R.id.tv_new:
                Constants.setBaseUrl("https://salesman-new.hstytest.com",mEt_providerid.getText().toString().trim(),mEtId.getText().toString().trim()
                        ,mEtType.getText().toString().trim(),mEtAppCode.getText().toString().trim(),mEtH5Url.getText().toString().trim());
                ToastUtil.showToastShort(Constants.BASE_URL);
                etHost.setText(Constants.BASE_URL);
                break;
            case R.id.tv_gray:
                Constants.setBaseUrl("https://gsalesman.hstypay.com",mEt_providerid.getText().toString().trim(),mEtId.getText().toString().trim()
                        ,mEtType.getText().toString().trim(),mEtAppCode.getText().toString().trim(),mEtH5Url.getText().toString().trim());
                ToastUtil.showToastShort(Constants.BASE_URL);
                etHost.setText(Constants.BASE_URL);
                break;
            case R.id.tv_formal:
                Constants.setBaseUrl("https://salesman.hstypay.com",mEt_providerid.getText().toString().trim(),mEtId.getText().toString().trim()
                        ,mEtType.getText().toString().trim(),mEtAppCode.getText().toString().trim(),mEtH5Url.getText().toString().trim());
                etHost.setText(Constants.BASE_URL);
                ToastUtil.showToastShort(Constants.BASE_URL);
                break;
            case R.id.btn_ensure:
                Constants.setBaseUrl(etHost.getText().toString().trim(),mEt_providerid.getText().toString().trim(),mEtId.getText().toString().trim()
                        ,mEtType.getText().toString().trim(),mEtAppCode.getText().toString().trim(),mEtH5Url.getText().toString().trim());
                dismiss();
                break;
            case R.id.tv_moc:
                Constants.setBaseUrl("https://msalesman.hstytest.com",mEt_providerid.getText().toString().trim(),mEtId.getText().toString().trim()
                        ,mEtType.getText().toString().trim(),mEtAppCode.getText().toString().trim(),mEtH5Url.getText().toString().trim());
                ToastUtil.showToastShort(Constants.BASE_URL);
                etHost.setText(Constants.BASE_URL);
                break;
        }
    }
}
