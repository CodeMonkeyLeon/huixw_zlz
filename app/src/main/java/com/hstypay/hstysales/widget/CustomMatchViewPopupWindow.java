package com.hstypay.hstysales.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.IdRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.utils.UIUtils;


/**
 * @author zeyu.kuang
 * @time 2020/12/15
 * @desc 可自定义宽高为MATCH_PARENT布局的弹窗Popup
 */
public class CustomMatchViewPopupWindow extends PopupWindow {

    private View mRootView;

    public CustomMatchViewPopupWindow(Context context,int layoutResID){

        mRootView = LayoutInflater.from(context).inflate(layoutResID, null);
        setContentView(mRootView);

        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        //this.setFocusable(true);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(UIUtils.getColor(R.color.popup_bg));
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);

        this.setOutsideTouchable(true);// 触摸popupwindow外部，popupwindow消失。这个要求你的popupwindow要有背景图片才可以成功 这样点击PopupWindow外部就会自行消失

    }

    /*public View setView(int layoutResID){

        mRootView = LayoutInflater.from(context).inflate(layoutResID, null);
        setContentView(mRootView);

        setAnimationStyle(R.style.Animation_Bottom_Dialog);
        final Window window = mContext.get().getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.alpha = 0.7f;
        window.setAttributes(lp);
        setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {
                if (mOnPopDismissListener!=null){
                    mOnPopDismissListener.onDismiss();
                }
                WindowManager.LayoutParams lp = window.getAttributes();
                lp.alpha = 1f;
                window.setAttributes(lp);
            }
        });

        return mRootView;
    }*/


    /**
     * 设置点击outView弹窗消失
     * @param outViewId outView的资源ID
     */
    public void setClickDismissOutView(@IdRes int outViewId){
        if (mRootView!=null){
            mRootView.findViewById(outViewId).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }

    }

    public View findViewById(@IdRes int id){
        if (mRootView!=null){
            return mRootView.findViewById(id);
        }
        return null;
    }

    public void show(View anchor){
        showAsDropDown(anchor);
    }

    public void show(View anchor, int xoff, int yoff) {
        showAsDropDown(anchor, xoff, yoff);
    }

}
