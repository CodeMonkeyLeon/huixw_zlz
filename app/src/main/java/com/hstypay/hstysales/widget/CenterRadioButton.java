package com.hstypay.hstysales.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.hstypay.hstysales.R;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.widget
 * @创建者: Jeremy
 * @创建时间: 2017/11/21 15:30
 * @描述: ${TODO}
 */

public class CenterRadioButton extends RadioButton {

    public CenterRadioButton(Context context) {
        super(context);
    }

    public CenterRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CenterRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onDraw(Canvas canvas) {

        Drawable[] drawables = getCompoundDrawables();
        if (drawables != null) {
            Drawable drawable = drawables[1];
            float textWidth = getPaint().measureText(getText().toString());
            int drawablePadding = getCompoundDrawablePadding();
            float bodyWidth = textWidth + drawablePadding;
            //居中显示图片
            int width = (int) getResources().getDimension(R.dimen.bottom_icon_size);
            int height = width * drawable.getIntrinsicHeight() / drawable.getIntrinsicWidth();
            int imageY = (this.getHeight() - height - (int) bodyWidth) / 2;

            drawable.setBounds(0, 0, width, height);
            canvas.translate(0, imageY);
        }
        super.onDraw(canvas);
    }

}
