package com.hstypay.hstysales.widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.hstypay.hstysales.R;

/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class LogoNoticeDialog extends Dialog {

    private ViewGroup mRootView;
    private Activity mContext;
    private Button mBtnKnow;
    private ImageView mIvClose;
    private OnClickOkListener mOnClickOkListener;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public LogoNoticeDialog(Activity context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.notice_dialog_logo, null);
        this.setCanceledOnTouchOutside(true);
        this.setCancelable(false);
        setContentView(mRootView);

        this.mContext = context;
        initView();
    }

    private void initView() {
        mBtnKnow = (Button) mRootView.findViewById(R.id.btn_know);
        mIvClose = (ImageView) mRootView.findViewById(R.id.iv_close);

        mBtnKnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnClickOkListener != null) {
                    mOnClickOkListener.clickOk();
                }
            }
        });
        mIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface OnClickOkListener{
        void clickOk();
    }

    public void setOnClickOkListener(OnClickOkListener onClickOkListener){
        this.mOnClickOkListener = onClickOkListener;
    }
}
