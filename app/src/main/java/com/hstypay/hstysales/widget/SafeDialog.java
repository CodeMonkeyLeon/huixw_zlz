package com.hstypay.hstysales.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.Widget
 * @创建者: Jeremy
 * @创建时间: 2017/11/7 11:38
 * @描述: ${TODO}
 */

public class SafeDialog extends Dialog {
    private Activity mParentActivity;

    public SafeDialog(@NonNull Context context) {
        super(context);
        mParentActivity = (Activity) context;
    }

    public SafeDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        mParentActivity = (Activity) context;
    }

    protected SafeDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    public void dismiss()
    {
        if (mParentActivity != null && !mParentActivity.isFinishing())
        {
            super.dismiss();    //调用超类对应方法
        }
    }
}
