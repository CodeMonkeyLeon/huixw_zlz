/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.hstysales.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.utils.StringUtils;


/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author
 * @version
 * @see  [相关类/方法]
 * @since  [产品/模块版本]
 */
public class CommonNoticeDialog extends Dialog
{

    private Context context;
    private TextView title,content;
    private Button btnOk;
    private ViewGroup mRootView;
    private OnClickOkListener mOnClickOkListener;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public CommonNoticeDialog(Context context, String titleStr, String btnOkStr)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.common_notice_dialog, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);
        this.context = context;
        initView(titleStr,"", btnOkStr);
    }

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */
    public CommonNoticeDialog(Context context, String titleStr, String contentStr, String btnOkStr)
    {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup)getLayoutInflater().inflate(R.layout.common_notice_dialog, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);
        this.context = context;
        initView(titleStr,contentStr, btnOkStr);
    }


    /**
     * 初始化
     * <功能详细描述>
     * @see [类、类#方法、类#成员]
     */
    private void initView(String titleStr, String contentStr, String btnOkStr) {
        title = (TextView)findViewById(R.id.title);
        content = (TextView)findViewById(R.id.content);
        btnOk = (Button)findViewById(R.id.btnOk);

        if (!StringUtils.isEmptyOrNull(titleStr)) {
            title.setText(titleStr);
        }else {
            title.setVisibility(View.GONE);
        }
        if (!StringUtils.isEmptyOrNull(btnOkStr)) {
            btnOk.setText(btnOkStr);
        }
        if (!StringUtils.isEmptyOrNull(contentStr)) {
            content.setText(contentStr);
        }else {
            content.setVisibility(View.GONE);
        }

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOnClickOkListener!=null){
                    mOnClickOkListener.onClickOk();
                }
            }
        });
    }

    public interface OnClickOkListener{
        void onClickOk();
    }

    public void setOnClickOkListener(OnClickOkListener onClickOkListener) {
        mOnClickOkListener = onClickOkListener;
    }
}
