package com.hstypay.hstysales.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.activity
 * @创建者: Jeremy
 * @创建时间: 2017/6/29 14:53
 * @描述: ${TODO}
 */

public class NoScrollLazyViewPager extends LazyViewPager {
	private boolean noScroll = true; //true 代表不能滑动 //false 代表能滑动

	public NoScrollLazyViewPager(Context context) {
		this(context, null);
	}

	public NoScrollLazyViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setNoScroll(boolean noScroll) {
		this.noScroll = noScroll;
	}

	@Override
	public void scrollTo(int x, int y) {
		super.scrollTo(x, y);
	}

	@Override
	public boolean onTouchEvent(MotionEvent arg0) {
        /* return false;//super.onTouchEvent(arg0); */
		if (noScroll)
			return false;
		else
			return super.onTouchEvent(arg0);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0) {
		if (noScroll)
			return false;
		else
			return super.onInterceptTouchEvent(arg0);
	}

	@Override
	public void setCurrentItem(int item, boolean smoothScroll) {
		super.setCurrentItem(item, smoothScroll);
	}

	@Override
	public void setCurrentItem(int item) {
		super.setCurrentItem(item, false);//表示切换的时候，不需要切换时间。
	}
}
