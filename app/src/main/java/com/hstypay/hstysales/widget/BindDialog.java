/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.hstysales.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.utils.UIUtils;


/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class BindDialog extends Dialog {
    private Context context;
    private TextView mTvStoreName,mTvCodeId;
    private Button btnOk;
    private Button btnCancel;
    private ViewGroup mRootView;
    private HandleBtn handleBtn;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */

    public BindDialog(Context context, String contentStoreName,String contentCodeId, HandleBtn handleBtn) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_bind_code, null);
        this.setCanceledOnTouchOutside(false);
        setContentView(mRootView);

        this.context = context;
        this.handleBtn = handleBtn;

        initView(contentStoreName, contentCodeId);
        setLinster();

    }

    /**
     * 设置监听
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLinster() {

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                handleBtn.handleCancelBtn();
            }

        });
        btnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                handleBtn.handleOkBtn();
            }
        });
    }

    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView(String contentStoreName,String contentCodeId) {
        mTvStoreName = (TextView) findViewById(R.id.content_store_name);
        mTvCodeId = (TextView) findViewById(R.id.content_code_id);
        btnOk = (Button) findViewById(R.id.btnOk);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        mTvStoreName.setText(UIUtils.getString(R.string.dialog_store_name_before)+contentStoreName);
        mTvCodeId.setText(UIUtils.getString(R.string.dialog_code_id_before)+contentCodeId);
    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn();
        void handleCancelBtn();
    }
}
