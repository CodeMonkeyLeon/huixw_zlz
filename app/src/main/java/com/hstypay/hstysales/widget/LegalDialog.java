/*
 * 文 件 名:  DialogInfo.java
 * 版    权:   Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  he_hui
 * 修改时间:  2013-3-11
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.hstypay.hstysales.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.ToastUtil;
import com.hstypay.hstysales.utils.UIUtils;


/**
 * 弹出提示框
 * <功能详细描述>
 *
 * @author he_hui
 * @version [版本号, 2013-3-11]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class LegalDialog extends Dialog {
    private Context context;
    private TextView tv_content;
    private TextView btnOk;
    private TextView btnCancel;
    private ViewGroup mRootView;
    private HandleBtn handleBtn;
    private View line_img;
    private TextView tv_title;
    private RelativeLayout rl_msg;
    private EditText et_msg_num;
    private TextView tv_num;
    private CountDownTimer mTimer;
    private OnClickSend mOnClickSend;
    private String type;

    /**
     * title
     * content 提示内容
     * <默认构造函数>
     */

    public LegalDialog(Context context, String title, String contentStr, String btnOkStr, String btnCancle, String type, HandleBtn handleBtn) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = getWindow();
        window.setBackgroundDrawable(new ColorDrawable(0));
        mRootView = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_legal, null);
        this.setCanceledOnTouchOutside(false);
        this.setCancelable(false);
        setContentView(mRootView);
        this.context = context;
        this.type = type;
        this.handleBtn = handleBtn;
        initView(title, contentStr, btnOkStr, btnCancle,type);
        setLinster(type);
    }

    /**
     * 设置监听
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void setLinster(final String type) {

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                handleBtn.handleCancleBtn();
            }

        });
        btnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(!StringUtils.isEmptyOrNull(type) && type.equals("legal") || type.equals("card")) {

                    if(StringUtils.isEmptyOrNull(et_msg_num.getText().toString())){
                        ToastUtil.showToastShort("请输入短信验证码");
                        return;
                    }
                    dismiss();
                    handleBtn.handleOkBtn(et_msg_num.getText().toString());
                }else {

                    handleBtn.handleOkBtn(et_msg_num.getText().toString());
                }
            }
        });
        tv_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnClickSend.onClickSend();
            }
        });
    }

    public void setTimerStart() {
        if(type!=null && type.equals("legal")){
            mTimer.start();
        }else if(type!=null && type.equals("card")){
            mTimer.start();
        }
    }

    /**
     * 初始化
     * <功能详细描述>
     *
     * @see [类、类#方法、类#成员]
     */
    private void initView(String title, String contentStr, String btnOkStr, String btnCancelStr,String type) {
        tv_title = findViewById(R.id.tv_title);
        tv_content = (TextView) findViewById(R.id.tv_content);
        rl_msg = findViewById(R.id.rl_msg);
        et_msg_num = findViewById(R.id.et_msg_num);
        tv_num = findViewById(R.id.tv_num);
        btnCancel = (TextView) findViewById(R.id.btnCancel);
        btnOk = (TextView) findViewById(R.id.btnOk);

        mTimer = new CountDownTimer(100000, 1000) {
            public void onTick(long millisUntilFinished) {
                tv_num.setText( millisUntilFinished / 1000 + "s");
                tv_num.setEnabled(false);
            }

            public void onFinish() {
                tv_num.setEnabled(true);
                tv_num.setText("发送");
            }
        };


        if (!StringUtils.isEmptyOrNull(title)) {
            tv_title.setText(title);
        }

        if (!StringUtils.isEmptyOrNull(contentStr)) {
            tv_content.setText(contentStr);
        }

        if (!StringUtils.isEmptyOrNull(btnOkStr)) {

            btnOk.setText(btnOkStr);
        }

        if (!StringUtils.isEmptyOrNull(btnCancelStr)){

            btnCancel.setText(btnCancelStr);
        }

        if(type!=null && type.equals("legal_success")){

            btnCancel.setText("跳过");
            btnOk.setText("需要");
            rl_msg.setVisibility(View.GONE);
            // timer.start();

        }else if(type!=null && type.equals("change_card")){
            rl_msg.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
            //timer.start();

        }else if(type!=null && type.equals("card_success")){
            rl_msg.setVisibility(View.GONE);
            tv_title.setText("变更提示");
            btnCancel.setText("取消");
            btnOk.setText("前往");

        }else if(type!=null && type.equals("merchant")){

            rl_msg.setVisibility(View.GONE);
        }

    }

    /**
     * 定义一个接口  去处理Dialog点击确定时需要做的操作
     *
     * @author Administrator
     */
    public interface HandleBtn {
        void handleOkBtn(String s);

        void handleCancleBtn();
    }

    public interface OnClickSend {
        void onClickSend();
    }

    public void setOnClickSend(OnClickSend onClickSend) {
        mOnClickSend = onClickSend;
    }
}
