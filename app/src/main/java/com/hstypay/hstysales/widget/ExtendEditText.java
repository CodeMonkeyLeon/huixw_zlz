package com.hstypay.hstysales.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;

public class ExtendEditText extends AppCompatEditText {

    public ExtendEditText(Context context) {
        super(context);
    }

    public ExtendEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExtendEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * 监听长按后选项菜单
     *
     * @param id static final int ID_SELECT_ALL = android.R.id.selectAll;
     *           <p>
     *           static final int ID_UNDO = android.R.id.undo;
     *           <p>
     *           static final int ID_REDO = android.R.id.redo;
     *           <p>
     *           static final int ID_CUT = android.R.id.cut;
     *           <p>
     *           static final int ID_COPY = android.R.id.copy;
     *           <p>
     *           static final int ID_PASTE = android.R.id.paste;
     *           <p>
     *           static final int ID_SHARE = android.R.id.shareText;
     *           <p>
     *           static final int ID_PASTE_AS_PLAIN_TEXT = android.R.id.pasteAsPlainText;
     *           <p>
     *           static final int ID_REPLACE = android.R.id.replaceText;
     *           <p>
     *           static final int ID_ASSIST = android.R.id.textAssist;
     *           <p>
     *           static final int ID_AUTOFILL = android.R.id.autofill;
     * @return
     */

    @Override

    public boolean onTextContextMenuItem(int id) {
        boolean handled = false;
        if (mOnTextMenuItemClickListener != null) {
            handled = mOnTextMenuItemClickListener.onTextMenuItemClick(id);
        }
        return handled || super.onTextContextMenuItem(id);
    }

    private OnTextMenuItemClickListener mOnTextMenuItemClickListener;

    /**
     * 设置文字菜单选项点击监听，用于监听粘贴、全选、复制等操作，可以重写这些操作逻辑。
     */

    public void setOnTextMenuItemClickListener(OnTextMenuItemClickListener onTextMenuItemClickListener) {
        mOnTextMenuItemClickListener = onTextMenuItemClickListener;
    }

    /**
     * 文字菜单选项点击监听
     */

    public interface OnTextMenuItemClickListener {
        boolean onTextMenuItemClick(int id);
    }

    /**
     * 输入法
     *
     * @param outAttrs
     * @return
     */

    @Override

    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        return new MyInputConnection(super.onCreateInputConnection(outAttrs), false);
    }

    private int count;

    class MyInputConnection extends InputConnectionWrapper implements InputConnection {
        public MyInputConnection(InputConnection target, boolean mutable) {
            super(target, mutable);
        }

        /**
         * 对输入的内容进行拦截
         *
         * @param text
         * @param newCursorPosition
         * @return
         */

        @Override

        public boolean commitText(CharSequence text, int newCursorPosition) {
            if (mOnCommitTextListener != null) {
                if (!mOnCommitTextListener.onCommitText(text, newCursorPosition)) {
                    return false;
                }
            }
            return super.commitText(text, newCursorPosition);
        }

        private void tips(String append) {
            count = 0;
            String text = getText().toString();
            for (int i = 0; i < text.length(); i++) {
                String a = String.valueOf(text.charAt(i));
                if (a.matches("[\u4e00-\u9fa5]+")) {
                    count = count + 2;
                } else {
                    count = count + 1;
                }
            }
            for (int i = 0; i < append.length(); i++) {
                String a = String.valueOf(append.charAt(i));
                if (a.matches("[\u4e00-\u9fa5]+")) {
                    count = count + 2;
                } else {
                    count = count + 1;
                }
            }
        }

        @Override
        public boolean sendKeyEvent(KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_DEL) {
                if (mOnCommitTextListener != null) {
                    if (mOnCommitTextListener.onDeleteClick()) { // 软键盘删除按钮点击事件拦截。
                        return true;
                    }
                }
            }
            return super.sendKeyEvent(event);
        }

        @Override
        public boolean deleteSurroundingText(int beforeLength, int afterLength) {
            if (beforeLength == 1 && afterLength == 0) {
                return sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL))
                        && sendKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL));

            }
            return super.deleteSurroundingText(beforeLength, afterLength);
        }
    }

    private OnCommitTextListener mOnCommitTextListener;
    public void setOnCommitTextListener(OnCommitTextListener onCommitTextListener) {
        mOnCommitTextListener = onCommitTextListener;
    }

    public interface OnCommitTextListener {
        /**
         * 输入文字监听回调。可以对输入文字进行拦截，处理后再显示(比如去掉特殊字符)。
         *
         * @param text
         * @param newCursorPosition
         * @return
         */
        boolean onCommitText(CharSequence text, int newCursorPosition);

        /**
         * 当软键盘删除按钮点击时的回调方法。
         *
         * @return true，拦截；false，默认
         */
        boolean onDeleteClick();

    }

}