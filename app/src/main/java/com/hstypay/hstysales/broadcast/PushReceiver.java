package com.hstypay.hstysales.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.ReceiveMerchantBean;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.widget.ToastDialog;


/**
 * Created by admin on 2017/8/21.
 */

public class PushReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {
        String title = intent.getStringExtra("title");
        String content = intent.getStringExtra("content");
        final String data = intent.getStringExtra("data");
        final String activityName = intent.getStringExtra("activityName");
        ToastDialog dialog = new ToastDialog(context, content, title);
        dialog.setOnClickToastListener(new ToastDialog.OnClickToastListener() {
            @Override
            public void clickToast() {
                Intent intentNew = new Intent(activityName);
                ReceiveMerchantBean merchantBean = new Gson().fromJson(data, ReceiveMerchantBean.class);
                intentNew.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intentNew.putExtra(Constants.INTENT_EXAMINE_BILLNO, merchantBean.getBillNo());
                intentNew.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_MSG_SKIP);
                context.startActivity(intentNew);
            }
        });
        dialog.showUpdateSuccessDialog(R.layout.view_push_toast);
    }

}
