package com.hstypay.hstysales.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.hstypay.hstysales.activity.AppOpenDetailActivity;
import com.hstypay.hstysales.activity.AppOpenManagerActivity;
import com.hstypay.hstysales.activity.MerchantListActivity;
import com.hstypay.hstysales.bean.ReceiveMerchantBean;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.LogUtil;

/**
 * Created by admin on 2017/8/21.
 * 商户进件资料状态变更广播接收
 */

public class MerchantAppOpenReceiver extends BroadcastReceiver {

    public static final String INTENT_EXTRA_KEY_DATA = "data";

    @Override
    public void onReceive(final Context context, Intent intent) {
        String data = intent.getStringExtra(INTENT_EXTRA_KEY_DATA);
        Gson gson = new Gson();
        ReceiveMerchantBean merchantBean = gson.fromJson(data, ReceiveMerchantBean.class);
        Intent intentNew = new Intent(context, AppOpenManagerActivity.class);
        intentNew.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        intentNew.putExtra(Constants.INTENT_EXAMINE_APPCODE, examineItemBean.getAppCode());
        intentNew.putExtra(Constants.INTENT_NAME, Constants.INTENT_NAME_MSG_SKIP);
        intentNew.putExtra(Constants.INTENT_EXAMINE_BILLNO, merchantBean.getBillNo());
        context.startActivity(intentNew);
    }
}
