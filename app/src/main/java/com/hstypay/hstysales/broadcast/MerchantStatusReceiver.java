package com.hstypay.hstysales.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.hstypay.hstysales.activity.MerchantListActivity;
import com.hstypay.hstysales.bean.ReceiveMerchantBean;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.LogUtil;

/**
 * Created by admin on 2017/8/21.
 * 商户进件资料状态变更广播接收
 */

public class MerchantStatusReceiver extends BroadcastReceiver {

    public static final String INTENT_EXTRA_KEY_DATA = "data";

    @Override
    public void onReceive(final Context context, Intent intent) {
        LogUtil.d("MerchantStatusReceiver",">>>>>>>>>>>>跳转商户进件列表");
        String data = intent.getStringExtra(INTENT_EXTRA_KEY_DATA);
        Gson gson = new Gson();
        ReceiveMerchantBean merchantBean = gson.fromJson(data, ReceiveMerchantBean.class);
        int mchQueryStatus = merchantBean.getMchQueryStatus();
        LogUtil.d("MerchantStatusReceiver",">>>>>>>>>>>>mchQueryStatus:"+mchQueryStatus);
        Intent intentNew = new Intent(context, MerchantListActivity.class);
        intentNew.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intentNew.putExtra(Constants.MERCHANT_CHECK_TYPE, getPagerType(mchQueryStatus));
        context.startActivity(intentNew);
    }

    public static int getPagerType(int mchQueryStatus){
        int pagerType = Constants.MERCHANT_NOT_CONFIRM;
        switch (mchQueryStatus){
            case 2:
                pagerType = Constants.MERCHANT_CHECK_FAILED;
                break;
            case 0:
                pagerType = Constants.MERCHANT_CHECKING;
                break;
            case 1:
                pagerType = Constants.MERCHANT_CHECK_SUCCESS;
                break;
            case 101:
                pagerType = Constants.MERCHANT_FREEZE;
                break;
            case 100:
                pagerType = Constants.MERCHANT_TRADABLE;
                break;
            case 3:
                pagerType = Constants.MERCHANT_CHANGEING;
                break;
            case 10:
                pagerType = Constants.MERCHANT_CHANGE_PASS;
                break;
            case 4:
                pagerType = Constants.MERCHANT_CHANGE_FAIL;
                break;
            case 5:
                pagerType = Constants.MERCHANT_INVALID;
                break;
            case 6://待确认
                pagerType = Constants.MERCHANT_NOT_CONFIRM;
                break;
            default:
                pagerType = Constants.MERCHANT_NO_INFO;
                break;
        }
        return pagerType;
    }

}
