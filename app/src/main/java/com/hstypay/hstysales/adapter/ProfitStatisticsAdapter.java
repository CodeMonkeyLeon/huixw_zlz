package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.ProfitsummaryBean;
import com.hstypay.hstysales.bean.ProfitListBean;
import com.hstypay.hstysales.viewholder.ProfitListViewHolder;
import com.hstypay.hstysales.viewholder.ProfitStatisticsAmountViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/10/27
 * @desc 分润统计的adapter
 */
public class ProfitStatisticsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int ITEM_TYPE_STATISTICS = 0;//统计部分的viewType
    private static final int ITEM_TYPE_LIST = 1;//列表部分的viewType
    private final int mStaticProfitItemCount = 1;

    private Context mContext;
    private ProfitsummaryBean.ProfitSummaryData mProfitSummaryData;
    private List<ProfitListBean.SummaryData> mSummaryDataList = new ArrayList<>();

    public ProfitStatisticsAdapter(Context context){
        mContext = context;
    }

    public void setProfitSummaryData(ProfitsummaryBean.ProfitSummaryData profitSummaryData){
        mProfitSummaryData = profitSummaryData;
        notifyDataSetChanged();
    }

    public void setProfitListData(List<ProfitListBean.SummaryData> summaryDataList){
        mSummaryDataList.clear();
        notifyDataSetChanged();
        if (summaryDataList!=null && summaryDataList.size()>0){
            mSummaryDataList.addAll(summaryDataList);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        if (viewType == ITEM_TYPE_STATISTICS){
            viewHolder = new ProfitStatisticsAmountViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_profit_statistics_amount,parent,false));
        }else /*if (viewType == ITEM_TYPE_LIST)*/{
            viewHolder = new ProfitListViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_profit_list,parent,false));
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == ITEM_TYPE_STATISTICS){
            ((ProfitStatisticsAmountViewHolder)holder).setData(mProfitSummaryData);
        }else if (getItemViewType(position) == ITEM_TYPE_LIST){
            ((ProfitListViewHolder)holder).setData(mSummaryDataList.get(position-mStaticProfitItemCount),ProfitListViewHolder.VIEW_TYPE_STATISTIC);
        }
    }

    @Override
    public int getItemCount() {
        return mStaticProfitItemCount+mSummaryDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0){
            return ITEM_TYPE_STATISTICS;
        }else {
            return ITEM_TYPE_LIST;
        }
    }
}
