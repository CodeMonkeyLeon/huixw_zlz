package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.ChoiceBean;

import java.util.List;

/**
 * Created by admin on 2017/12/7.
 */

public class ChoiceAdapter extends BaseAdapter {


    Context mContext;
    List<ChoiceBean> mList;
    String id;

    public ChoiceAdapter(Context choiceStoreActivity, List<ChoiceBean> list, String store_id) {

        this.mContext=choiceStoreActivity;
        this.mList=list;
        this.id=store_id;
    }

    @Override
    public int getCount() {

        if(mList!=null && mList.size()>0){

            return mList.size();
        }else {

            return 0;
        }

    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ChoiceStoreHolder holder;
        if(convertView==null){

            convertView= View.inflate(mContext, R.layout.choice_list_item,null);
            holder=new ChoiceStoreHolder();
            holder.tv_choice_name= (TextView) convertView.findViewById(R.id.tv_choice_name);
            holder.iv_choice_store= (ImageView) convertView.findViewById(R.id.iv_choice_store);
            holder.v_choice_line= convertView.findViewById(R.id.v_choice_line);

            convertView.setTag(holder);
            convertView.setBackgroundResource(R.color.white);
        }else {
            holder= (ChoiceStoreHolder) convertView.getTag();
            convertView.setBackgroundResource(R.color.white);

        }

        if(mList!=null && mList.size()>0){

            ChoiceBean bean = mList.get(position);
            if(bean!=null && bean.getName()!=null){
                holder.tv_choice_name.setText(bean.getName());
            }

            if(mList.size()-1==position){
                holder.v_choice_line.setVisibility(View.GONE);
            }else {
                holder.v_choice_line.setVisibility(View.VISIBLE);
            }
        }

        return convertView;
    }


    class ChoiceStoreHolder{
        TextView tv_choice_name;
        ImageView iv_choice_store;
        View v_choice_line;
    }
}
