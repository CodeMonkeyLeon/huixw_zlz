package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.BranchListBean;
import com.hstypay.hstysales.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class BranchRecyclerAdapter extends RecyclerView.Adapter<BranchRecyclerAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<BranchListBean.DataEntity> mList;
    private long mBankId;
    private boolean isFirstEnter = true;
    private int layoutPosition;

    public BranchRecyclerAdapter(Context context, List<BranchListBean.DataEntity> list, long bankId) {
        this.mContext = context;
        this.mList = list;
        this.mBankId = bankId;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public BranchRecyclerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_bank, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final BranchRecyclerAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        holder.mTvName.setText(mList.get(position).getBankBranchName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                    layoutPosition = holder.getLayoutPosition();
                    isFirstEnter = false;
                    notifyDataSetChanged();
                }
            }
        });
        if (isFirstEnter) {
            if (mBankId!=-1L) {
                if (mBankId==mList.get(position).getBankBranchId()) {
                    holder.mIvNameChoiced.setVisibility(View.VISIBLE);
                    holder.mTvName.setTextColor(UIUtils.getColor(R.color.theme_blue));
                } else {
                    holder.mIvNameChoiced.setVisibility(View.GONE);
                    holder.mTvName.setTextColor(UIUtils.getColor(R.color.tv_right_color));
                }
            }
        } else {
            if (position == layoutPosition) {
                holder.mIvNameChoiced.setVisibility(View.VISIBLE);
                holder.mTvName.setTextColor(UIUtils.getColor(R.color.theme_blue));
            } else {
                holder.mIvNameChoiced.setVisibility(View.GONE);
                holder.mTvName.setTextColor(UIUtils.getColor(R.color.tv_right_color));
            }
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvName;
        private ImageView mIvNameChoiced;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvName = (TextView) itemView.findViewById(R.id.tv_name_choice);
            mIvNameChoiced = (ImageView) itemView.findViewById(R.id.iv_name_choice);
        }
    }

}