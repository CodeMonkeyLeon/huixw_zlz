package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.StoreListBean;

import java.util.List;

/**
 * Created by admin on 2017/7/8.
 */

public class BindingCodeAdapter extends RecyclerView.Adapter<BindingCodeAdapter.HomeViewHolder>{

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    Context mContext;
    List<StoreListBean.DataEntity> mDataBean;

    public BindingCodeAdapter(Context context, List<StoreListBean.DataEntity> data) {
        this.mContext=context;
        this.mDataBean=data;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public BindingCodeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.binding_code_item, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final BindingCodeAdapter.HomeViewHolder holder, final int position) {

        StoreListBean.DataEntity dataBean = mDataBean.get(position);
        if(dataBean!=null){
            holder.mTvShopName.setText(dataBean.getStoreName());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if(mDataBean!=null){
            return mDataBean.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvShopName;
        private ImageView mIvLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvShopName = (TextView) itemView.findViewById(R.id.tv_store_code_item);
            mIvLine = (ImageView) itemView.findViewById(R.id.line_01);
        }
    }
}
