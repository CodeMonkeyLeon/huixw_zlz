package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.DeviceOrderListBean;
import com.hstypay.hstysales.viewholder.DeviceMallOrderViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/8/24
 * @desc 硬件商城订单adapter
 */
public class DeviceMallOrderRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;

    private List<DeviceOrderListBean.DataEntity.DataBean> mOrdersBeans = new ArrayList<>();

    public void setOrderList(List<DeviceOrderListBean.DataEntity.DataBean> orderList){
        mOrdersBeans.clear();
        notifyDataSetChanged();
        if (orderList!=null && orderList.size()>0){
            mOrdersBeans.addAll(orderList);
            notifyDataSetChanged();
        }
    }

    public DeviceMallOrderRecyclerAdapter(Context context){
        mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DeviceMallOrderViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_device_mall_order,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        DeviceMallOrderViewHolder deviceMallOrderViewHolder = (DeviceMallOrderViewHolder) holder;
        deviceMallOrderViewHolder.setData(mOrdersBeans.get(position));
        deviceMallOrderViewHolder.setOnDeviceMallItemBtnClickListener(mOnDeviceMallItemBtnClickListener);
    }

    @Override
    public int getItemCount() {
        return mOrdersBeans.size();
    }

    private DeviceMallOrderViewHolder.OnDeviceMallItemBtnClickListener mOnDeviceMallItemBtnClickListener;
    public void setOnDeviceMallItemBtnClickListener(DeviceMallOrderViewHolder.OnDeviceMallItemBtnClickListener onDeviceMallItemBtnClickListener) {
        mOnDeviceMallItemBtnClickListener = onDeviceMallItemBtnClickListener;
    }

}
