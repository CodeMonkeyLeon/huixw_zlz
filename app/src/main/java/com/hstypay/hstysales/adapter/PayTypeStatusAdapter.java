package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.PayTypeStatusBean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class PayTypeStatusAdapter extends RecyclerView.Adapter<PayTypeStatusAdapter.HomeViewHolder> {

    private Context mContext;
    private List<PayTypeStatusBean.PayTypeEntity> mListData;

    public PayTypeStatusAdapter(Context context, List<PayTypeStatusBean.PayTypeEntity> listData) {
        this.mContext = context;
        this.mListData = listData;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public PayTypeStatusAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_pay_type, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(PayTypeStatusAdapter.HomeViewHolder holder, final int position) {
        if (mListData != null && mListData.size() > 0) {
            holder.mTvPayType.setText(mListData.get(position).getApiName());
            holder.mIvPayType.setImageResource(mListData.get(position).getTradeStatus() ? R.mipmap.icon_trade_enable : R.mipmap.icon_trade_unable);
            holder.mViewLine.setVisibility(position == mListData.size()-1 ? View.INVISIBLE : View.VISIBLE);
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mListData != null)
            return mListData.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvPayType;
        private ImageView mIvPayType;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvPayType = itemView.findViewById(R.id.tv_pay_type);
            mIvPayType = itemView.findViewById(R.id.iv_pay_type);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }
}