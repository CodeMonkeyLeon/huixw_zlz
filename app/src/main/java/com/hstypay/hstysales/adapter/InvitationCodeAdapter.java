package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.InvitationCodeBean;
import com.hstypay.hstysales.utils.DateUtil;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.hstypay.hstysales.widget.ShadowLayout;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class InvitationCodeAdapter extends RecyclerView.Adapter<InvitationCodeAdapter.HomeViewHolder> {

    private Context mContext;
    private List<InvitationCodeBean.DataEntity.ItemData> mListData;
    private OnShareClickListener mOnShareClickListener;
    private int layoutPosition;
    private OnDetailGetListener mOnDetailGetListener;

    public interface OnDetailGetListener {
        void showDetail(int position);
    }

    public void setOnDetailGetListener(OnDetailGetListener onDetailGetListener) {
        mOnDetailGetListener = onDetailGetListener;
    }

    public interface OnShareClickListener {
        void skipToshare(String invitationCode);
    }

    public void setOnShareClickListener(OnShareClickListener onShareClickListener) {
        mOnShareClickListener = onShareClickListener;
    }

    public InvitationCodeAdapter(Context context, List<InvitationCodeBean.DataEntity.ItemData> listData) {
        this.mContext = context;
        this.mListData = listData;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public InvitationCodeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_invitation_code, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final InvitationCodeAdapter.HomeViewHolder holder, final int position) {
        if (mListData != null && mListData.size() > 0) {
            holder.mTvCode.setText(mListData.get(position).getInvitationCode());
            holder.mTvCodeRate.setText("费率：" + mListData.get(position).getCostRateCnt());
            holder.mTvCodeUseTotal.setText("总次数：" + mListData.get(position).getMaxUseCount());
            holder.mTvCodeStatus.setText(mListData.get(position).getStatusCnt());
            if ("已过期".equals(mListData.get(position).getStatusCnt())) {
                holder.mTvCodeStatus.setVisibility(View.VISIBLE);
                holder.mIvArrow.setVisibility(View.INVISIBLE);
                setViewEnable(false, holder);
            } else if ("已禁用".equals(mListData.get(position).getStatusCnt())) {
                holder.mTvCodeStatus.setVisibility(View.VISIBLE);
                holder.mIvArrow.setVisibility(View.VISIBLE);
                setViewEnable(true, holder);
                holder.mShadowLayout.setVisibility(View.GONE);
                holder.mBtnCommonUnable.setVisibility(View.VISIBLE);
            } else if ("正常".equals(mListData.get(position).getStatusCnt())) {
                holder.mTvCodeStatus.setVisibility(View.INVISIBLE);
                holder.mIvArrow.setVisibility(View.VISIBLE);
                setViewEnable(true, holder);
                holder.mShadowLayout.setVisibility(View.VISIBLE);
                holder.mBtnCommonUnable.setVisibility(View.GONE);
            }
            holder.mTvCodeValidity.setText(DateUtil.formartDateToYYMMDD(mListData.get(position).getValidStartAt())
                    + "-" + DateUtil.formartDateToYYMMDD(mListData.get(position).getValidEndAt()));
            holder.mTvCodeUsedCount.setText(mListData.get(position).getUsedCount() + "次");
            if (!TextUtils.isEmpty(mListData.get(position).getMerchantCount())) {
                holder.mTvCodeMerchantCount.setText(mListData.get(position).getMerchantCount() + "家");
            }else{
                holder.mTvCodeMerchantCount.setText("");
            }
            if (!mListData.get(position).isOpen()) {
                holder.mIvArrow.setImageResource(R.mipmap.icon_arrow_down);
                holder.mLlCodeDetail.setVisibility(View.GONE);   // 显示布局
            } else {
                holder.mIvArrow.setImageResource(R.mipmap.icon_arrow_up);
                holder.mLlCodeDetail.setVisibility(View.VISIBLE);   // 显示布局
            }
            holder.mIvArrow.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    layoutPosition = holder.getLayoutPosition();
                    LogUtil.v("showDetail==2="+layoutPosition+"--"+position+"--"+mListData.get(position).isOpen());
                    mOnDetailGetListener.showDetail(layoutPosition);
                    /*if (mListData.get(position).isOpen()) {
                        mListData.get(position).setOpen(false);
                        holder.mIvArrow.setImageResource(R.mipmap.icon_arrow_down);
                        holder.mLlCodeDetail.setVisibility(View.GONE);   // 显示布局
                    } else {
                        StatService.trackCustomKVEvent(mContext, "1045", null);
                        mListData.get(position).setOpen(true);
                        holder.mIvArrow.setImageResource(R.mipmap.icon_arrow_up);
                        holder.mLlCodeDetail.setVisibility(View.VISIBLE);   // 显示布局
                        mOnDetailGetListener.showDetail(layoutPosition);
                    }*/
                }
            });
            holder.mBtnCommonEnable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnShareClickListener != null) {
                        mOnShareClickListener.skipToshare(mListData.get(position).getInvitationCode());
                    }
                }
            });
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mListData != null)
            return mListData.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {
        private ImageView mIvArrow;
        private TextView mTvCodeRate, mTvCodeUseTotal, mTvCodeValidity, mTvCodeUsedCount, mTvCodeMerchantCount, mTvCode, mTvCodeStatus;
        private LinearLayout mLlCodeDetail;
        private Button mBtnCommonEnable,mBtnCommonUnable;
        private ShadowLayout mShadowLayout;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mIvArrow = (ImageView) itemView.findViewById(R.id.iv_arrow);
            mTvCodeRate = (TextView) itemView.findViewById(R.id.tv_code_rate);
            mTvCodeUseTotal = (TextView) itemView.findViewById(R.id.tv_code_use_total);
            mTvCode = (TextView) itemView.findViewById(R.id.tv_invitation_code);
            mTvCodeStatus = (TextView) itemView.findViewById(R.id.tv_code_status);
            mTvCodeValidity = (TextView) itemView.findViewById(R.id.tv_invitation_code_validity);
            mTvCodeUsedCount = (TextView) itemView.findViewById(R.id.tv_code_used_count);
            mTvCodeMerchantCount = (TextView) itemView.findViewById(R.id.tv_code_merchant_count);
            mLlCodeDetail = (LinearLayout) itemView.findViewById(R.id.ll_code_detail);
            mBtnCommonEnable = (Button) itemView.findViewById(R.id.btn_common_enable);
            mBtnCommonUnable = (Button) itemView.findViewById(R.id.btn_common_unable);
            mShadowLayout = (ShadowLayout) itemView.findViewById(R.id.shadow_button);
        }
    }

    private void setViewEnable(boolean enable, InvitationCodeAdapter.HomeViewHolder holder) {
        if (enable) {
            holder.mIvArrow.setVisibility(View.VISIBLE);
            holder.mTvCodeRate.setTextColor(UIUtils.getColor(R.color.home_text));
            holder.mTvCodeUseTotal.setTextColor(UIUtils.getColor(R.color.home_text));
            holder.mTvCodeStatus.setTextColor(UIUtils.getColor(R.color.home_text));
        } else {
            holder.mIvArrow.setVisibility(View.INVISIBLE);
            holder.mTvCodeRate.setTextColor(UIUtils.getColor(R.color.tv_useless));
            holder.mTvCodeUseTotal.setTextColor(UIUtils.getColor(R.color.tv_useless));
            holder.mTvCodeStatus.setTextColor(UIUtils.getColor(R.color.tv_useless));
        }
    }
}