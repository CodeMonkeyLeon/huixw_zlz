package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.DeviceType;
import com.hstypay.hstysales.utils.UIUtils;

import java.util.List;


/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class DeviceManageTypeAdapter extends RecyclerView.Adapter<DeviceManageTypeAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<DeviceType> mList;
    private String mId;

    public DeviceManageTypeAdapter(Context context, List<DeviceType> list, String id) {
        this.mContext = context;
        this.mList = list;
        this.mId = id;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public DeviceManageTypeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_device_type, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final DeviceManageTypeAdapter.HomeViewHolder holder, final int position) {
        if (mList == null)
            return;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                    notifyDataSetChanged();
                }
            }
        });
        holder.mViewLine.setVisibility(position == mList.size()-1 ? View.INVISIBLE : View.VISIBLE);
        holder.mTvCashierName.setText(mList.get(position).getTypeName());
        if (mList.get(position).getTypeCode().equals(mId)) {
            holder.mIvCashierChoiced.setVisibility(View.VISIBLE);
            holder.mTvCashierName.setTextColor(UIUtils.getColor(R.color.theme_color));
        } else {
            holder.mIvCashierChoiced.setVisibility(View.GONE);
            holder.mTvCashierName.setTextColor(UIUtils.getColor(R.color.black));
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mList != null)
            return mList.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvCashierName;
        private ImageView mIvCashierChoiced;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvCashierName = (TextView) itemView.findViewById(R.id.tv_cashier_choice);
            mIvCashierChoiced = (ImageView) itemView.findViewById(R.id.iv_cashier_choice);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

}