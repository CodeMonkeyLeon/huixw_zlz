package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.RankBean;
import com.hstypay.hstysales.utils.DateUtil;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class RankAdapter extends RecyclerView.Adapter<RankAdapter.HomeViewHolder> {

    private Context mContext;
    private List<RankBean.DataEntity.ItemData> mListData;
    private OnCallClickListener mOnCallClickListener;

    public interface OnCallClickListener {
        void onCallClick(int position);
    }

    public void setOnCallClickListener(OnCallClickListener listener) {
        this.mOnCallClickListener = listener;
    }

    public RankAdapter(Context context, List<RankBean.DataEntity.ItemData> listData) {
        this.mContext = context;
        this.mListData = listData;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public RankAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_rank, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(RankAdapter.HomeViewHolder holder, final int position) {
        if (mListData!=null &&mListData.size()>0) {
            holder.mTvMerchantName.setText(mListData.get(position).getPrincipal() + "-" + mListData.get(position).getMerchantName());
            holder.mTvTotalMoney.setText(DateUtil.formatMoney(mListData.get(position).getTotalFee()) + "元");
            holder.mTvTotalCount.setText(mListData.get(position).getBillCount() + "笔");
            holder.mIvCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnCallClickListener!=null && !TextUtils.isEmpty(mListData.get(position).getTelphone())) {
                        //call(mListData.get(position).getTelphone(), mContext);
                        mOnCallClickListener.onCallClick(position);
                    }
                }
            });
            if (position == 0) {
                holder.mIvRank.setBackgroundResource(R.mipmap.rank_first);
                holder.mTvRank.setVisibility(View.GONE);
            } else if (position == 1) {
                holder.mIvRank.setBackgroundResource(R.mipmap.rank_second);
                holder.mTvRank.setVisibility(View.GONE);
            } else if (position == 2) {
                holder.mIvRank.setBackgroundResource(R.mipmap.rank_third);
                holder.mTvRank.setVisibility(View.GONE);
            } else {
                holder.mIvRank.setBackgroundResource(R.drawable.shape_rank_circle);
                holder.mTvRank.setVisibility(View.VISIBLE);
                holder.mTvRank.setText((position+1) + "");
            }
            if (position == mListData.size()-1){
                holder.mViewLine.setVisibility(View.INVISIBLE);
            }else{
                holder.mViewLine.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mListData!=null)
            return mListData.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvMerchantName,mTvTotalMoney,mTvTotalCount,mTvRank;
        private final ImageView mIvCall,mIvRank;
        private RelativeLayout mRlRankLeft;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvMerchantName = (TextView) itemView.findViewById(R.id.tv_merchant_name);
            mIvCall = (ImageView) itemView.findViewById(R.id.iv_call);
            mTvTotalCount = (TextView) itemView.findViewById(R.id.tv_trade_total_count);
            mTvTotalMoney = (TextView) itemView.findViewById(R.id.tv_trade_total_money);
            mIvRank = (ImageView) itemView.findViewById(R.id.iv_rank);
            mTvRank = (TextView) itemView.findViewById(R.id.tv_rank);
            mRlRankLeft = (RelativeLayout) itemView.findViewById(R.id.rl_rank_left);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

}