package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.ProfitListBean;
import com.hstypay.hstysales.viewholder.ProfitListViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/10/26
 * @desc 日分润明细和月分润明细的adapter
 */
public class ProfitListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<ProfitListBean.SummaryData> mProfitDataList = new ArrayList<>();

    public enum ProfitAdapterType{
        //日分润明细
        DAY_PROFIT,
        //月分润明细
        MONTH_PROFIT,
    }
    private ProfitAdapterType mProfitAdapterType;

    public ProfitListAdapter(Context context,ProfitAdapterType type){
        mContext = context;
        mProfitAdapterType = type;
    }


    public void setProfitListData(List<ProfitListBean.SummaryData> summaryDataList) {
        mProfitDataList.clear();
        notifyDataSetChanged();
        if (summaryDataList!=null && summaryDataList.size()>0){
            mProfitDataList.addAll(summaryDataList);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ProfitListViewHolder viewHolder = new ProfitListViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_profit_list,parent,false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        ProfitListViewHolder profitListViewHolder = (ProfitListViewHolder) holder;
        if (mProfitAdapterType == ProfitAdapterType.DAY_PROFIT){
            profitListViewHolder.setData(mProfitDataList.get(position),ProfitListViewHolder.VIEW_TYPE_DAY);
        }else if (mProfitAdapterType == ProfitAdapterType.MONTH_PROFIT){
            profitListViewHolder.setData(mProfitDataList.get(position),ProfitListViewHolder.VIEW_TYPE_MONTH);
            profitListViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnProfitItemClickListener!=null){
                        mOnProfitItemClickListener.onItemClick(mProfitDataList.get(holder.getAdapterPosition()));
                    }
                    /*try {
                        Intent intent = new Intent(mContext, ProfitDayListDetailActivity.class);
                        ProfitListBean.SummaryData summaryData = mProfitDataList.get(holder.getAdapterPosition());
                        String payTradeTime = summaryData.getPayTradeTime();//2020-10-02 00:00:00
                        Date tradeDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(payTradeTime);
                        intent.putExtra(Constants.INTENT_NAME_DATE, tradeDate);
                        mContext.startActivity(intent);
                    }catch (Exception e){
                        e.printStackTrace();
                    }*/
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mProfitDataList.size();
    }

    public interface OnProfitItemClickListener{
        void onItemClick(ProfitListBean.SummaryData summaryData);
    }
    private OnProfitItemClickListener mOnProfitItemClickListener;

    public void setOnProfitItemClickListener(OnProfitItemClickListener onProfitItemClickListener) {
        mOnProfitItemClickListener = onProfitItemClickListener;
    }
}