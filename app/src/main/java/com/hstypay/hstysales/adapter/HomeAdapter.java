package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.AppOpenManagerActivity;
import com.hstypay.hstysales.activity.MerchantListActivity;
import com.hstypay.hstysales.activity.ProfitDayListDetailActivity;
import com.hstypay.hstysales.activity.ProfitMonthListActivity;
import com.hstypay.hstysales.activity.ProfitStatisticsActivity;
import com.hstypay.hstysales.app.MyApplication;
import com.hstypay.hstysales.bean.HomeDataBean;
import com.hstypay.hstysales.bean.HomeNoticeBean;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.DateUtil;
import com.hstypay.hstysales.utils.UIUtils;
import com.tencent.stat.StatService;

import java.util.Calendar;
import java.util.Date;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeViewHolder> {

    private Context mContext;
    private boolean isEyeClose = true;
    private boolean mMarquee = true;
    private HomeNoticeBean.DataEntity mNoticeData;
    private HomeDataBean mHomeDataBean;
//    private String mConfig;

    public HomeAdapter(Context context, HomeDataBean homeDataBean, boolean marquee, HomeNoticeBean.DataEntity noticeData) {
        this.mContext = context;
        this.mHomeDataBean = homeDataBean;
        this.mMarquee = marquee;
//        this.mConfig = MyApplication.getConfig();
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public HomeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_home, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final HomeAdapter.HomeViewHolder holder, final int position) {
        if (!MyApplication.getConfig().contains(Constants.CONFIG_SHARE_DETAIL) && !MyApplication.getConfig().contains(Constants.CONFIG_SHARE_STATISTICS)) {
            holder.ll_profit_btn.setVisibility(View.GONE);
        } else {
            holder.ll_profit_btn.setVisibility(View.VISIBLE);
            if (!MyApplication.getConfig().contains(Constants.CONFIG_SHARE_DETAIL)) {
                holder.ll_profit_detail_list.setVisibility(View.GONE);
                holder.view_line.setVisibility(View.GONE);
            } else if (!MyApplication.getConfig().contains(Constants.CONFIG_SHARE_STATISTICS)) {
                holder.ll_profit_statistics.setVisibility(View.GONE);
                holder.view_line.setVisibility(View.GONE);
            } else {
                holder.ll_profit_detail_list.setVisibility(View.VISIBLE);
                holder.ll_profit_statistics.setVisibility(View.VISIBLE);
                holder.view_line.setVisibility(View.VISIBLE);
            }
        }
        if (mHomeDataBean != null && mHomeDataBean.getData() != null) {
//            holder.mTvIncomeYesterday.setText(DateUtil.formatMoneyTwo(mHomeDataBean.getData().getMyEarningsYesterday()) + "(元)");
//            holder.mTvTradeTotal.setText(DateUtil.formatDoubleMoney(mHomeDataBean.getData().getPayNetAmount()));
//            holder.mTvTradeYesterdayMoney.setText(DateUtil.formatDoubleMoney(mHomeDataBean.getData().getPayNetAmountYesterday()));

            holder.mTvIncomeTodayHome.setText(DateUtil.formatMoneyCn(mHomeDataBean.getData().getMyEarningsToday()));
            holder.mTvIncomeYesterdayHome.setText(DateUtil.formatMoneyCn(mHomeDataBean.getData().getMyEarningsYesterday()));
            holder.mTvIncomeTotalHome.setText(DateUtil.formatMoneyCn(mHomeDataBean.getData().getMyEarnings()));
            if (mHomeDataBean.getData().getSvcOpenApplyCount() > 0) {
                holder.mRlNotice.setVisibility(View.VISIBLE);
                holder.mTvNoticeCount.setText("您有" + mHomeDataBean.getData().getSvcOpenApplyCount() + "条商户应用开通申请待处理，请尽快处理");
            } else {
                holder.mRlNotice.setVisibility(View.GONE);
            }
            holder.mRlNotice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, AppOpenManagerActivity.class));
                }
            });

            HomeDataBean.DataBean data = mHomeDataBean.getData();
            HomeDataBean.DataBean.MchTotalBean mchTotal = data.getMchTotal();
            if (mchTotal != null) {

                holder.tv_tradable.setText(mchTotal.getCanTradeMchTotal() + "");//可交易
                holder.mTvNumNotConfirmHome.setText(mchTotal.getNoConfirmMchTotal() + "");//待确认

                holder.tv_check_pass.setText(mchTotal.getExaminePassMchTotal() + "");//审核通过
                holder.tv_checking.setText(mchTotal.getExamingMchTotal() + "");//审核中
                holder.tv_check_fail.setText(mchTotal.getExamineFailMchTotal() + "");//审核失败
                holder.tv_changing.setText(mchTotal.getChangeExamingMchTotal() + "");//变更审核中
                holder.tv_change_pass.setText(mchTotal.getChangeExaminePassMchTotal() + "");//变更审核通过
                holder.tv_change_fail.setText(mchTotal.getChangeExamineFailMchTotal() + "");//变更审核失败

                holder.tv_initial.setText(mchTotal.getInitMchTotal() + "");//初始商户
                holder.tv_freeze.setText(mchTotal.getFreezeMchTotal() + "");//冻结商户
                holder.tv_invalid.setText(mchTotal.getInvalidMchTotal() + "");//作废商户
            }
        }

    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        return 1;
    }

    private void setPwdVisible(boolean isEyeClose, ImageView ivEye, TextView textView) {
        if (mHomeDataBean != null && mHomeDataBean.getData() != null) {
            if (isEyeClose) {
                ivEye.setImageResource(R.mipmap.login_password_close);
                textView.setText("********");
            } else {
                ivEye.setImageResource(R.mipmap.login_password_open);
                textView.setText(UIUtils.getString(R.string.tv_mark) + DateUtil.formatMoneyTwo(mHomeDataBean.getData().getMyEarnings()));
            }
        }
    }

    private boolean setEye(boolean isEyeClose) {
        return isEyeClose ? false : true;
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private LinearLayout  ll_tradable, ll_check_pass, ll_checking, ll_check_fail, ll_changing,
                ll_change_pass, ll_change_fail, ll_initial, ll_freeze, ll_invalid,mLlNotConfirmHome,
                ll_profit_detail_list,ll_profit_statistics,ll_profit_btn;
        private TextView tv_tradable, tv_check_pass, tv_checking, tv_check_fail,
                tv_changing, tv_change_pass, tv_change_fail, tv_initial, tv_freeze, tv_invalid,mTvNumNotConfirmHome;
        private View view_line;
        private RelativeLayout mRlNotice;
        private TextView mTvNoticeCount;

        private TextView mTvIncomeTodayHome;//今日收益
        private TextView mTvIncomeYesterdayHome;//昨日收益
        private TextView mTvIncomeTotalHome;//累计收益

        public HomeViewHolder(View itemView) {
            super(itemView);

            mTvIncomeTodayHome = itemView.findViewById(R.id.tv_income_today_home);
            mTvIncomeYesterdayHome = itemView.findViewById(R.id.tv_income_yesterday_home);
            mTvIncomeTotalHome = itemView.findViewById(R.id.tv_income_total_home);

            mRlNotice = itemView.findViewById(R.id.rl_notice);
            mTvNoticeCount = itemView.findViewById(R.id.tv_notice_count);

            ll_tradable = itemView.findViewById(R.id.ll_tradable);//可交易商户
            tv_tradable = itemView.findViewById(R.id.tv_tradable);

            mLlNotConfirmHome = itemView.findViewById(R.id.ll_not_confirm_home);//待确认
            mTvNumNotConfirmHome = itemView.findViewById(R.id.tv_num_not_confirm_home);

            ll_check_pass = itemView.findViewById(R.id.ll_check_pass);//审核通过
            tv_check_pass = itemView.findViewById(R.id.tv_check_pass);

            ll_checking = itemView.findViewById(R.id.ll_checking);//审核中
            tv_checking = itemView.findViewById(R.id.tv_checking);

            ll_check_fail = itemView.findViewById(R.id.ll_check_fail);//审核失败
            tv_check_fail = itemView.findViewById(R.id.tv_check_fail);

            ll_changing = itemView.findViewById(R.id.ll_changing);//变更审核中
            tv_changing = itemView.findViewById(R.id.tv_changing);

            ll_change_pass = itemView.findViewById(R.id.ll_change_pass);//变更审核通过
            tv_change_pass = itemView.findViewById(R.id.tv_change_pass);


            ll_change_fail = itemView.findViewById(R.id.ll_change_fail);//变更审核失败
            tv_change_fail = itemView.findViewById(R.id.tv_change_fail);

            ll_initial = itemView.findViewById(R.id.ll_initial);//初始商户
            tv_initial = itemView.findViewById(R.id.tv_initial);

            ll_freeze = itemView.findViewById(R.id.ll_freeze);//冻结商户
            tv_freeze = itemView.findViewById(R.id.tv_freeze);

            ll_invalid = itemView.findViewById(R.id.ll_invalid);//作废商户
            tv_invalid = itemView.findViewById(R.id.tv_invalid);

            ll_profit_btn = itemView.findViewById(R.id.ll_profit_btn);
            ll_profit_detail_list = itemView.findViewById(R.id.ll_profit_detail_list);
            ll_profit_statistics = itemView.findViewById(R.id.ll_profit_statistics);
            view_line = itemView.findViewById(R.id.view_line);

            ll_tradable.setOnClickListener(this);
            mLlNotConfirmHome.setOnClickListener(this);
            ll_check_pass.setOnClickListener(this);
            ll_checking.setOnClickListener(this);
            ll_check_fail.setOnClickListener(this);
            ll_changing.setOnClickListener(this);
            ll_change_pass.setOnClickListener(this);
            ll_change_fail.setOnClickListener(this);
            ll_invalid.setOnClickListener(this);
            ll_initial.setOnClickListener(this);
            ll_freeze.setOnClickListener(this);
            mTvIncomeTodayHome.setOnClickListener(this);
            itemView.findViewById(R.id.tv_income_today_title).setOnClickListener(this);//今日收益title
            itemView.findViewById(R.id.ll_income_yesterday_home).setOnClickListener(this);//昨日收益ll
            ll_profit_detail_list.setOnClickListener(this);//分润明细
            ll_profit_statistics.setOnClickListener(this);//分润统计
        }

        @Override
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()) {
                case R.id.tv_income_today_home:
                case R.id.tv_income_today_title:
                    //今日收益
                    intent = new Intent(mContext, ProfitDayListDetailActivity.class);
                    intent.putExtra(Constants.INTENT_NAME_DATE, new Date());
                    intent.putExtra(Constants.INTENT_NAME_DATE_CN, mContext.getResources().getString(R.string.name_today));
                    mContext.startActivity(intent);
                    break;
                case R.id.ll_income_yesterday_home:
                    //昨日收益
                    intent = new Intent(mContext, ProfitDayListDetailActivity.class);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date());
                    calendar.add(calendar.DATE, -1);
                    intent.putExtra(Constants.INTENT_NAME_DATE, calendar.getTime());
                    intent.putExtra(Constants.INTENT_NAME_DATE_CN, mContext.getResources().getString(R.string.name_yesterday));
                    mContext.startActivity(intent);
                    break;
                case R.id.ll_profit_detail_list://分润明细
                    mContext.startActivity(new Intent(mContext, ProfitMonthListActivity.class));
                    break;
                case R.id.ll_profit_statistics://分润统计
                    mContext.startActivity(new Intent(mContext, ProfitStatisticsActivity.class));
                    break;
                case R.id.ll_tradable:
                    //可交易
                    intent = new Intent(mContext, MerchantListActivity.class);
                    intent.putExtra(Constants.MERCHANT_CHECK_TYPE, Constants.MERCHANT_TRADABLE);
                    mContext.startActivity(intent);
                    break;
                case R.id.ll_not_confirm_home:
                    //待确认
                    intent = new Intent(mContext, MerchantListActivity.class);
                    intent.putExtra(Constants.MERCHANT_CHECK_TYPE, Constants.MERCHANT_NOT_CONFIRM);
                    mContext.startActivity(intent);
                    break;
                case R.id.ll_check_pass://审核通过商户
                    StatService.trackCustomKVEvent(mContext, "1002", null);
                    intent = new Intent(mContext, MerchantListActivity.class);
                    intent.putExtra(Constants.MERCHANT_CHECK_TYPE, Constants.MERCHANT_CHECK_SUCCESS);
                    mContext.startActivity(intent);
                    break;
                case R.id.ll_checking://审核中商户
                    StatService.trackCustomKVEvent(mContext, "1005", null);
                    intent = new Intent(mContext, MerchantListActivity.class);
                    intent.putExtra(Constants.MERCHANT_CHECK_TYPE, Constants.MERCHANT_CHECKING);
                    mContext.startActivity(intent);
                    break;
                case R.id.ll_check_fail://审核失败商户
                    StatService.trackCustomKVEvent(mContext, "1008", null);
                    intent = new Intent(mContext, MerchantListActivity.class);
                    intent.putExtra(Constants.MERCHANT_CHECK_TYPE, Constants.MERCHANT_CHECK_FAILED);
                    mContext.startActivity(intent);
                    break;

                case R.id.ll_changing:
                    //变更审核中
                    intent = new Intent(mContext, MerchantListActivity.class);
                    intent.putExtra(Constants.MERCHANT_CHECK_TYPE, Constants.MERCHANT_CHANGEING);
                    mContext.startActivity(intent);
                    break;
                case R.id.ll_change_pass:
                    //变更审核通过
                    intent = new Intent(mContext, MerchantListActivity.class);
                    intent.putExtra(Constants.MERCHANT_CHECK_TYPE, Constants.MERCHANT_CHANGE_PASS);
                    mContext.startActivity(intent);
                    break;
                case R.id.ll_change_fail:

                    intent = new Intent(mContext, MerchantListActivity.class);
                    intent.putExtra(Constants.MERCHANT_CHECK_TYPE, Constants.MERCHANT_CHANGE_FAIL);
                    mContext.startActivity(intent);
                    //变更失败
                    break;
                case R.id.ll_initial://待補件商戶
                    StatService.trackCustomKVEvent(mContext, "1011", null);
                    intent = new Intent(mContext, MerchantListActivity.class);
                    intent.putExtra(Constants.MERCHANT_CHECK_TYPE, Constants.MERCHANT_NO_INFO);
                    mContext.startActivity(intent);
                    break;
                case R.id.ll_freeze://冻结商户
                    StatService.trackCustomKVEvent(mContext, "1013", null);
                    intent = new Intent(mContext, MerchantListActivity.class);
                    intent.putExtra(Constants.MERCHANT_CHECK_TYPE, Constants.MERCHANT_FREEZE);
                    mContext.startActivity(intent);
                    break;

                case R.id.ll_invalid:
                    //作废商户
                    intent = new Intent(mContext, MerchantListActivity.class);
                    intent.putExtra(Constants.MERCHANT_CHECK_TYPE, Constants.MERCHANT_INVALID);
                    mContext.startActivity(intent);
                    break;
            }
        }
    }

    public void setConfig(String config){
        notifyDataSetChanged();
    }

}