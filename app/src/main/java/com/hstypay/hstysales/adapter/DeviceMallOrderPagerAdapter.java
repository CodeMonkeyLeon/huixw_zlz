package com.hstypay.hstysales.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.hstypay.hstysales.bean.DeviceMallFragmentTitleBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/8/23
 * @desc 硬件商城PagerAdapter
 */
public class DeviceMallOrderPagerAdapter extends FragmentStatePagerAdapter {

    private List<DeviceMallFragmentTitleBean> mFragmentList = new ArrayList<>();

    public DeviceMallOrderPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position).getFragment();
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentList.get(position).getTitle();
    }

    public void setPagers(List<DeviceMallFragmentTitleBean> fragmentList) {
        mFragmentList.clear();
        notifyDataSetChanged();
        if (fragmentList!=null && fragmentList.size()>0){
            mFragmentList.addAll(fragmentList);
            notifyDataSetChanged();
        }
    }

}
