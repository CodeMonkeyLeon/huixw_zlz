package com.hstypay.hstysales.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.RankBean;
import com.hstypay.hstysales.utils.DateUtil;
import com.tencent.stat.StatService;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class TradeAdapter extends RecyclerView.Adapter<TradeAdapter.HomeViewHolder> {

    private Context mContext;
    private List<RankBean.DataEntity.ItemData> mListData;

    public TradeAdapter(Context context, List<RankBean.DataEntity.ItemData> listData) {
        this.mContext = context;
        this.mListData = listData;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public TradeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_trade, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(TradeAdapter.HomeViewHolder holder, final int position) {
        if (mListData!=null &&mListData.size()>0){
            holder.mTvMerchantName.setText(mListData.get(position).getPrincipal()+"-"+mListData.get(position).getMerchantName());
            holder.mTvTotalMoney.setText(DateUtil.formatMoney(mListData.get(position).getTotalFee())+"元");
            holder.mTvTotalCount.setText(mListData.get(position).getBillCount()+"笔");
            holder.mIvCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StatService.trackCustomKVEvent(mContext, "1016", null);
                    if (!TextUtils.isEmpty(mListData.get(position).getTelphone())){
                        call(mListData.get(position).getTelphone(), mContext);
                    }
                }
            });
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mListData!=null)
            return mListData.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvMerchantName,mTvTotalMoney,mTvTotalCount;
        private final ImageView mIvCall;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvMerchantName = (TextView) itemView.findViewById(R.id.tv_merchant_name);
            mIvCall = (ImageView) itemView.findViewById(R.id.iv_call);
            mTvTotalMoney = (TextView) itemView.findViewById(R.id.tv_trade_total_money);
            mTvTotalCount = (TextView) itemView.findViewById(R.id.tv_trade_total_count);
        }
    }

    private void call(String phone, Context context) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (ActivityNotFoundException a) {
            a.getMessage();
        }
    }

}