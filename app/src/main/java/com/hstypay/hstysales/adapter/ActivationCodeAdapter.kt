package com.hstypay.hstysales.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.hstypay.hstysales.R
import com.hstypay.hstysales.bean.ActivationCode

/**
 * @Author dean.zeng
 * @Description  微收银激活码列表
 * @Date 2020-07-17 14:45
 **/
class ActivationCodeAdapter(data: List<ActivationCode>) : BaseQuickAdapter<ActivationCode, BaseViewHolder>(R.layout.item_activation_code, data) {


    override fun convert(helper: BaseViewHolder, item: ActivationCode) {
        helper.setText(R.id.tvStoreMerchantName, item.storeMerchantName)
                .setText(R.id.tvActivationCode, "激活码："+item.activationCode)
                .setText(R.id.tvSn, "设备号："+item.sn)
                .setImageResource(R.id.imgActivationStatus, if (item.activationStatus != 0)
                    R.mipmap.img_activity_ongoing else
                    R.mipmap.img_activity_close)
    }
}