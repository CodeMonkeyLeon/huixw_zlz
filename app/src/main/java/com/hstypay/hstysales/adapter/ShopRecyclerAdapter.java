package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.StoreListBean;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.UIUtils;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */
public class ShopRecyclerAdapter extends RecyclerView.Adapter<ShopRecyclerAdapter.HomeViewHolder> {

    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private Context mContext;
    private List<StoreListBean.DataEntity> mDataBean;
    private String mStoreId;
    private int layoutPosition;
    private boolean isFirstEnter = true;

    public ShopRecyclerAdapter(Context context, List<StoreListBean.DataEntity> data, String storeId) {
        this.mContext = context;
        this.mDataBean = data;
        this.mStoreId = storeId;

        LogUtil.d("data====" + mStoreId);
    }

    public void setStoreId(String storeId) {
        this.mStoreId = storeId;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public ShopRecyclerAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_shop, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final ShopRecyclerAdapter.HomeViewHolder holder, final int position) {
        if (mDataBean == null)
            return;
        StoreListBean.DataEntity dataBean = mDataBean.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                    layoutPosition = holder.getLayoutPosition();
                    isFirstEnter = false;
                    notifyDataSetChanged();
                }
            }
        });
        holder.mViewLine.setVisibility(position == mDataBean.size() - 1 ? View.INVISIBLE : View.VISIBLE);
        if (dataBean != null) {
            holder.mTvShopName.setText(dataBean.getStoreName());
            if (dataBean.getStoreId().equals(mStoreId)) {
                holder.mIvShopChoiced.setVisibility(View.VISIBLE);
                holder.mTvShopName.setTextColor(UIUtils.getColor(R.color.theme_color));
            } else {
                holder.mIvShopChoiced.setVisibility(View.INVISIBLE);
                holder.mTvShopName.setTextColor(UIUtils.getColor(R.color.black));
            }
        }
    }


    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mDataBean != null) {
            return mDataBean.size();
        }
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvShopName;
        private ImageView mIvShopChoiced;
        private View mViewLine;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvShopName = (TextView) itemView.findViewById(R.id.tv_shop_choice);
            mIvShopChoiced = (ImageView) itemView.findViewById(R.id.iv_shop_choice);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

}