package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.ProfitListBean;
import com.hstypay.hstysales.bean.ProfitsummaryBean;
import com.hstypay.hstysales.bean.RankListBean;
import com.hstypay.hstysales.bean.RankSumaryBean;
import com.hstypay.hstysales.viewholder.RankMerchantListViewHolder;
import com.hstypay.hstysales.viewholder.RankSummaryViewHolder;

import java.util.ArrayList;
import java.util.List;

public class RankFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;

    private RankSumaryBean.RankSumaryData mRankSummaryData;
    private List<RankListBean.SummaryData> mRankListDatas = new ArrayList<>();

    private static final int VIEW_TYPE_SUMMARY = 0;//头部
    private static final int VIEW_TYPE_LIST = 1;//列表

    public RankSummaryViewHolder.OnRankHeaderContentClickListener mOnRankHeaderContentClickListener;

    public void setOnRankHeaderContentClickListener(RankSummaryViewHolder.OnRankHeaderContentClickListener onRankHeaderContentClickListener) {
        mOnRankHeaderContentClickListener = onRankHeaderContentClickListener;
    }

    public RankFragmentAdapter(Context context) {
        mContext = context;
    }

    public void setRankSummaryBean(RankSumaryBean.RankSumaryData rankSummaryData) {
        mRankSummaryData = rankSummaryData;
        notifyDataSetChanged();
    }

    public void setRankListDatas(List<RankListBean.SummaryData> rankListDatas) {
        mRankListDatas.clear();
        notifyDataSetChanged();
        if (rankListDatas != null && rankListDatas.size() > 0) {
            mRankListDatas.addAll(rankListDatas);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        if (viewType == VIEW_TYPE_SUMMARY) {
            viewHolder = new RankSummaryViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_fragment_rank_header, viewGroup, false), mContext);
        } else {
            viewHolder = new RankMerchantListViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_fragment_rank_list, viewGroup, false));
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_SUMMARY) {
            ((RankSummaryViewHolder) viewHolder).setData(mRankSummaryData);
            ((RankSummaryViewHolder) viewHolder).setOnRankHeaderContentClickListener(mOnRankHeaderContentClickListener);
        } else {
            ((RankMerchantListViewHolder) viewHolder).setData(mRankListDatas.get(getListItemPosition(viewHolder.getAdapterPosition())));
            ((RankMerchantListViewHolder) viewHolder).mLlCallTelephoneRankItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnRankHeaderContentClickListener != null) {
                        mOnRankHeaderContentClickListener.onClickTelPhone(mRankListDatas.get(getListItemPosition(viewHolder.getAdapterPosition())).getTelphone());
                    }
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        if (mRankSummaryData == null) {
            return mRankListDatas.size() + 0;
        } else {
            return mRankListDatas.size() + 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mRankSummaryData == null) {
            return VIEW_TYPE_LIST;
        } else {
            if (position == 0) {
                return VIEW_TYPE_SUMMARY;
            } else {
                return VIEW_TYPE_LIST;
            }
        }
    }

    private int getListItemPosition(int position) {
        if (mRankSummaryData == null) {
            return position;
        } else {
            return position - 1;
        }
    }
}
