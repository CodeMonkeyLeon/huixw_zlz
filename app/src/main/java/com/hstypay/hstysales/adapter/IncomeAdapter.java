package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.IncomeDetailBean;
import com.hstypay.hstysales.utils.DateUtil;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class IncomeAdapter extends RecyclerView.Adapter<IncomeAdapter.HomeViewHolder> {

    private Context mContext;
    private List<IncomeDetailBean.DataEntity.ItemData> mListData;

    public IncomeAdapter(Context context, List<IncomeDetailBean.DataEntity.ItemData> listData) {
        this.mContext = context;
        this.mListData = listData;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public IncomeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_income, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(IncomeAdapter.HomeViewHolder holder, final int position) {
        if (mListData!=null && mListData.size()>0){
            holder.mTvIncomeDate.setText(DateUtil.formartDateToYYMMDD(mListData.get(position).getBillTime()));
            holder.mTvIncomeMoney.setText("+"+DateUtil.formatMoneyTwo(mListData.get(position).getFeeToday())+"元");
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mListData!=null)
            return mListData.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvIncomeDate,mTvIncomeMoney;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mTvIncomeDate = (TextView) itemView.findViewById(R.id.tv_item_income_date);
            mTvIncomeMoney = (TextView) itemView.findViewById(R.id.tv_item_income_money);
        }
    }

}