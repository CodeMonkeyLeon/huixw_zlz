package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.MerchantListBean;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/10/28
 * @desc
 */
public class MerchantRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<MerchantListBean.DataEntity.ItemData> mItemDataList = new ArrayList<>();
    private OnRecyclerViewItemClickListener mOnItemClickListener;
    private String mId;

    public MerchantRecyclerAdapter(Context context){
        mContext = context;
    }

    public void setData(List<MerchantListBean.DataEntity.ItemData> itemDatas, String id){
        mId = id;
        mItemDataList.clear();
        notifyDataSetChanged();
        if (itemDatas!=null && itemDatas.size()>0){
            mItemDataList.addAll(itemDatas);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_merchant_select,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        MyViewHolder myViewHolder = (MyViewHolder) holder;
        MerchantListBean.DataEntity.ItemData itemData = mItemDataList.get(position);
        myViewHolder.mTvName.setText(itemData.getMerchantName());
        myViewHolder.mViewLine.setVisibility(position == mItemDataList.size()-1 ? View.INVISIBLE : View.VISIBLE);
        if (mItemDataList.get(position).getMerchantId().equals(mId)) {
            myViewHolder.mIvChoiced.setVisibility(View.VISIBLE);
            myViewHolder.mTvName.setTextColor(UIUtils.getColor(R.color.theme_color));
        } else {
            myViewHolder.mIvChoiced.setVisibility(View.GONE);
            myViewHolder.mTvName.setTextColor(UIUtils.getColor(R.color.home_text));
        }
        /*if (itemData.isSelcet()) {
            myViewHolder.mIvShopChoiced.setVisibility(View.VISIBLE);
            myViewHolder.mTvShopName.setTextColor(UIUtils.getColor(R.color.theme_color));
        } else {
            myViewHolder.mIvShopChoiced.setVisibility(View.INVISIBLE);
            myViewHolder.mTvShopName.setTextColor(UIUtils.getColor(R.color.black));
        }*/

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView mTvName;
        private ImageView mIvChoiced;
        private View mViewLine;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvName = (TextView) itemView.findViewById(R.id.tv_shop_choice);
            mIvChoiced = (ImageView) itemView.findViewById(R.id.iv_shop_choice);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
}
