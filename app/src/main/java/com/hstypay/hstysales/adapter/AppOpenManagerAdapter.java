package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.ExamineListBean;
import com.hstypay.hstysales.viewholder.AppOpenManagerViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/12/10
 * @desc 商户应用开通管理页的adapter
 */
public class AppOpenManagerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private OnItemClickListener mOnItemClickListener;
    List<ExamineListBean.ExamineBean> mExamineBeans = new ArrayList<>();

    public AppOpenManagerAdapter(Context context){
        mContext = context;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public void setData(List<ExamineListBean.ExamineBean> examineBeans) {
        mExamineBeans.clear();
        notifyDataSetChanged();
        if (examineBeans!=null && examineBeans.size()>0){
            mExamineBeans.addAll(examineBeans);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AppOpenManagerViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_app_open_manager,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener!=null){
                    mOnItemClickListener.onItemClick(mExamineBeans.get(holder.getAdapterPosition()));
                }
            }
        });
        AppOpenManagerViewHolder appOpenManagerViewHolder = (AppOpenManagerViewHolder) holder;
        appOpenManagerViewHolder.setData(mExamineBeans.get(position));
    }

    @Override
    public int getItemCount() {
        return mExamineBeans.size();
    }


    public interface OnItemClickListener{
        void onItemClick(ExamineListBean.ExamineBean examineItemBean);
    }
}
