package com.hstypay.hstysales.adapter;

import java.io.Serializable;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.adapter
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/05/16 19:04
 * @描述: ${TODO}
 */
public class DataEntity implements Serializable {
    private String realName;
    private String userId;
    private String empId;
    private int type;
    public DataEntity(){}

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRealName() {
        return realName;
    }

    public String getUserId() {
        return userId;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
