package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.base.BaseMerchantFragment;

import java.util.List;

/**
 * 类名：
 * 类描述：ViewPager的适配器
 * 创建人：fly
 * 创建日期： 2017/2/2.
 * 版本：V1.0
 */
public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private List<BaseMerchantFragment> mFragmentList;     //  对应fragment的集合
    private List<String> mPageTitleList;      //  对应TabLayout对应的标题，与fragment相对应

    public SimpleFragmentPagerAdapter(Context context,
                                      FragmentManager fm,
                                      List<BaseMerchantFragment> fragmentList,
                                      List<String> pageTitleList) {
        super(fm);
        this.mContext = context;
        this.mFragmentList = fragmentList;
        this.mPageTitleList = pageTitleList;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitleList.get(position);
    }

    /**根据角标获取标题item的布局文件*/
    public View getTabItemView(int position,boolean noInfo) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.tab_layout_item, null);  // 标题布局
        TextView textView = (TextView) view.findViewById(R.id.textview);
        ImageView ivRedDot = (ImageView) view.findViewById(R.id.iv_red);
        textView.setText(mPageTitleList.get(position));  // 设置标题内容
        if (position == 2 && noInfo == true){
            ivRedDot.setVisibility(View.VISIBLE);
        }else {
            ivRedDot.setVisibility(View.GONE);
        }
        return view;
    }
}
