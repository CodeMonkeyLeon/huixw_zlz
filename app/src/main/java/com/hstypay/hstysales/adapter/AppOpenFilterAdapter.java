package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.AppOpenFilterBean;
import com.hstypay.hstysales.bean.MerchantListBean;
import com.hstypay.hstysales.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/12/15
 * @desc 商户应用开通管理类型、状态筛选
 */
public class AppOpenFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<AppOpenFilterBean> mFilterBeans = new ArrayList<>();
    private String mId;

    public AppOpenFilterAdapter(Context context){
        mContext = context;
    }

    public void setData(List<AppOpenFilterBean> filterBeans, String id){
        mId = id;
        mFilterBeans.clear();
        notifyDataSetChanged();
        if (filterBeans!=null && filterBeans.size()>0){
            mFilterBeans.addAll(filterBeans);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FilterViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_app_filter,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        if (mFilterBeans == null || mFilterBeans .size() == 0)
            return;
        FilterViewHolder filterViewHolder = (FilterViewHolder) holder;
        filterViewHolder.mTv_title_filter.setText(mFilterBeans.get(position).getName());
        if (mFilterBeans.get(position).getId().equals(mId)) {
            filterViewHolder.mIv_choice_filter.setVisibility(View.VISIBLE);
            filterViewHolder.mTv_title_filter.setTextColor(UIUtils.getColor(R.color.theme_color));
        } else {
            filterViewHolder.mIv_choice_filter.setVisibility(View.GONE);
            filterViewHolder.mTv_title_filter.setTextColor(UIUtils.getColor(R.color.home_text));
        }
        filterViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnFilterItemClickListener!=null){
                    mOnFilterItemClickListener.onItemClick(holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFilterBeans.size();
    }


    class FilterViewHolder extends RecyclerView.ViewHolder{

        private  TextView mTv_title_filter;
        private  ImageView mIv_choice_filter;

        public FilterViewHolder(@NonNull View itemView) {
            super(itemView);
            mTv_title_filter = itemView.findViewById(R.id.tv_title_filter);
            mIv_choice_filter = itemView.findViewById(R.id.iv_choice_filter);
        }
    }


    public interface OnFilterItemClickListener{
        void onItemClick(int position);
    }

    private OnFilterItemClickListener mOnFilterItemClickListener;

    public void setOnFilterItemClickListener(OnFilterItemClickListener onFilterItemClickListener) {
        mOnFilterItemClickListener = onFilterItemClickListener;
    }
}
