package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.ProfitListBean;
import com.hstypay.hstysales.bean.WxSignDetailBean;
import com.hstypay.hstysales.viewholder.WxRealMerchantViewHolder;

import java.util.ArrayList;
import java.util.List;

//微信实名认证商户adapter
public class WxRealMerchantAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<WxSignDetailBean.SignDetailData> mSignDetailDataList = new ArrayList<>();
    private Context mContext;
    private OnWxRealMerchantFunctionClickListener mOnWxRealMerchantFunctionClickListener;

    public void setOnWxRealMerchantFunctionClickListener(OnWxRealMerchantFunctionClickListener onWxRealMerchantFunctionClickListener) {
        mOnWxRealMerchantFunctionClickListener = onWxRealMerchantFunctionClickListener;
    }

    public WxRealMerchantAdapter(Context context){
        mContext = context;
    }
    public void setSignListDatas(List<WxSignDetailBean.SignDetailData>  signDetailDataList){
        mSignDetailDataList.clear();
        notifyDataSetChanged();
        if (signDetailDataList!=null && signDetailDataList.size()>0){
            mSignDetailDataList.addAll(signDetailDataList);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_wx_real_merchant, viewGroup, false);
        return new WxRealMerchantViewHolder(mContext,view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        WxRealMerchantViewHolder wxRealMerchantViewHolder = (WxRealMerchantViewHolder) viewHolder;
        wxRealMerchantViewHolder.setData(mSignDetailDataList.get(position));
        wxRealMerchantViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnWxRealMerchantFunctionClickListener!=null){
                    mOnWxRealMerchantFunctionClickListener.itemClick(viewHolder.getAdapterPosition());
                }
            }
        });
        wxRealMerchantViewHolder.mLlCancelSignWx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //撤销申请单
                if (mOnWxRealMerchantFunctionClickListener!=null){
                    mOnWxRealMerchantFunctionClickListener.cancelSignWx(viewHolder.getAdapterPosition());
                }
            }
        });
        wxRealMerchantViewHolder.mLlSignHelpWx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //人工处理
                if (mOnWxRealMerchantFunctionClickListener!=null){
                    mOnWxRealMerchantFunctionClickListener.signHelpWx(viewHolder.getAdapterPosition());
                }
            }
        });
        wxRealMerchantViewHolder.mLlOptFailSignWx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //签约失败重新签约
                if (mOnWxRealMerchantFunctionClickListener!=null){
                    mOnWxRealMerchantFunctionClickListener.reSignWx(viewHolder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSignDetailDataList.size();
    }

    public interface OnWxRealMerchantFunctionClickListener{
        void itemClick(int position);
        void cancelSignWx(int position);//撤销申请单
        void signHelpWx(int position); //人工处理
        void reSignWx(int position);//签约失败重新签约
    }


}
