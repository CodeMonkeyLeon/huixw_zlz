package com.hstypay.hstysales.adapter;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.MerchantInfoActivity;
import com.hstypay.hstysales.bean.MerchantListBean;
import com.hstypay.hstysales.utils.Constants;

import java.util.List;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/11/21 20:31
 * @描述: ${TODO}
 */

public class MerchantSearchAdapter extends RecyclerView.Adapter<MerchantSearchAdapter.ViewHolder> {

    private Activity mContext;
    private int type;
    private List<MerchantListBean.DataEntity.ItemData> mListData;

    public MerchantSearchAdapter(Activity mContext, List<MerchantListBean.DataEntity.ItemData> listData) {
        this.mContext = mContext;
        this.mListData = listData;
    }

    @Override
    public MerchantSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_merchant_check, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MerchantSearchAdapter.ViewHolder holder, final int position) {
        if (mListData != null && mListData.size() > 0) {
            if (mListData.get(position).getActivateStatus()==2){
                type = 4;
            }else {
                if (mListData.get(position).getExamineStatus()==0){
                    type = 1;
                }else if (mListData.get(position).getExamineStatus()==1){
                    if (mListData.get(position).getTradeStatus()==0){//不可交易
                        type=1;
                    }else if (mListData.get(position).getTradeStatus()==10){//可交易
                        type=2;
                    }
                }else if (mListData.get(position).getExamineStatus()==2){
                    type = 0;
                }
            }
            switch (type) {
                case 0://审核失败
                    holder.mRlTop.setVisibility(View.VISIBLE);
                    holder.mRlBottom.setVisibility(View.GONE);
                    holder.mTvReasonTitle.setText(mContext.getString(R.string.check_failed_reason));
                    holder.mTvReason.setText(mListData.get(position).getExamineRemark());
                    setView(holder, position);
                    break;
                case 1://审核中
                    holder.mRlTop.setVisibility(View.VISIBLE);
                    holder.mRlBottom.setVisibility(View.GONE);
                    holder.mLlMerchantReason.setVisibility(View.GONE);
                    holder.mTvReason.setText(mListData.get(position).getExamineRemark());
                    setView(holder, position);
                    break;
                case 2://审核通过
                    holder.mRlTop.setVisibility(View.VISIBLE);
                    holder.mRlBottom.setVisibility(View.GONE);
                    holder.mLlMerchantReason.setVisibility(View.GONE);
                    holder.mTvReason.setText(mListData.get(position).getExamineRemark());
                    setView(holder, position);
                    break;
                case 3://未进件
                    holder.mRlTop.setVisibility(View.GONE);
                    holder.mRlBottom.setVisibility(View.VISIBLE);
                    holder.mTvInfoDate.setText(mListData.get(position).getRegisteAt());
                    holder.mTvInfoTel.setText(mListData.get(position).getTelphone());
                    break;
                case 4://已冻结
                    holder.mRlTop.setVisibility(View.VISIBLE);
                    holder.mRlBottom.setVisibility(View.GONE);
                    holder.mTvReasonTitle.setText(mContext.getString(R.string.freeze_reason));
                    holder.mTvReason.setText(mListData.get(position).getActivateRemark());
                    setView(holder, position);
                    break;
            }
            holder.mIvCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(mListData.get(position).getTelphone())) {
                        call(mListData.get(position).getTelphone(), mContext);
                    }
                }
            });
            if (position == mListData.size() - 1) {
                holder.mViewLine.setVisibility(View.INVISIBLE);
            } else {
                holder.mViewLine.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setView(ViewHolder holder, final int position) {
        holder.mTvMerchantName.setText(mListData.get(position).getPrincipal() + "-" + mListData.get(position).getMerchantName());
        holder.mTvMerchantAddress.setText("地址：" + mListData.get(position).getProvinceCnt()+ mListData.get(position).getCityCnt()
                + mListData.get(position).getCountyCnt() + mListData.get(position).getAddress());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mListData.get(position).getMerchantId())) {
                    Intent intent = new Intent(mContext, MerchantInfoActivity.class);
                    intent.putExtra(Constants.INTENT_MERCHANT_ID, mListData.get(position).getMerchantId());
                    intent.putExtra(Constants.INTENT_MERCHANT_TEL, mListData.get(position).getTelphone());
                    intent.putExtra(Constants.INTENT_MERCHANT_NAME, mListData.get(position).getMerchantName());
                    mContext.startActivityForResult(intent, Constants.REQUEST_LIST_TO_MERCHANT);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mListData != null)
            return mListData.size();
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvReason, mTvReasonTitle, mTvMerchantName, mTvMerchantAddress, mTvInfoDate, mTvInfoTel;
        private LinearLayout mLlMerchantReason;
        private RelativeLayout mRlTop, mRlBottom;
        private ImageView mIvCall;
        private View mViewLine;

        public ViewHolder(View itemView) {
            super(itemView);
            mRlTop = (RelativeLayout) itemView.findViewById(R.id.rl_check_type_top);
            mLlMerchantReason = (LinearLayout) itemView.findViewById(R.id.ll_merchant_reason);
            mTvReason = (TextView) itemView.findViewById(R.id.tv_reason);
            mTvReasonTitle = (TextView) itemView.findViewById(R.id.tv_reason_title);

            mTvMerchantName = (TextView) itemView.findViewById(R.id.tv_merchant_name);
            mTvMerchantAddress = (TextView) itemView.findViewById(R.id.tv_merchant_address);

            mRlBottom = (RelativeLayout) itemView.findViewById(R.id.rl_check_type_bottom);
            mTvInfoDate = (TextView) itemView.findViewById(R.id.tv_info_date);
            mTvInfoTel = (TextView) itemView.findViewById(R.id.tv_info_tel);
            mIvCall = (ImageView) itemView.findViewById(R.id.iv_call);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

    private void call(String phone, Context context) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (ActivityNotFoundException a) {
            a.getMessage();
        }
    }
}