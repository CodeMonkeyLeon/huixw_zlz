package com.hstypay.hstysales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.NoticeBean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/7/1 15:03
 * @描述: ${TODO}
 */

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.HomeViewHolder> {

    private Context mContext;
    private List<NoticeBean.DataEntity.ItemData> mListData;
    private OnRecyclerViewItemClickListener mOnItemClickListener;

    public NoticeAdapter(Context context, List<NoticeBean.DataEntity.ItemData> listData) {
        this.mContext = context;
        this.mListData = listData;
    }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * @param parent
     * @param viewType
     * @return
     * @des 决定条目的视图
     */
    @Override
    public NoticeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_notice, null);
        return new HomeViewHolder(itemView);
    }

    /**
     * @param holder
     * @param position
     * @desc 条目数据和视图的绑定
     */
    @Override
    public void onBindViewHolder(final NoticeAdapter.HomeViewHolder holder, final int position) {
        if (mListData != null && mListData.size() > 0) {
            holder.mTvNoticeTitle.setText(mListData.get(position).getTitle());
            holder.mTvNoticeDate.setText(mListData.get(position).getCreateTime());
            if (mListData.get(position).getIsRead() > 0) {//大于0 已读
                holder.mIvNotice.setImageResource(R.mipmap.notice_icon);
            } else {
                holder.mIvNotice.setImageResource(R.mipmap.notice_icon_red);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(position);
                    }
                    holder.mIvNotice.setImageResource(R.mipmap.notice_icon);
                }
            });
        }
    }

    /**
     * @return
     * @des 获取条目数
     */
    @Override
    public int getItemCount() {
        if (mListData != null)
            return mListData.size();
        return 0;
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private TextView mTvNoticeTitle, mTvNoticeDate;
        private final ImageView mIvNotice;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mIvNotice = (ImageView) itemView.findViewById(R.id.iv_notice);
            mTvNoticeTitle = (TextView) itemView.findViewById(R.id.tv_notice_title);
            mTvNoticeDate = (TextView) itemView.findViewById(R.id.tv_notice_date);
        }
    }

}