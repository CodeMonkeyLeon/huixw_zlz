package com.hstypay.hstysales.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.activity.MerchantInfoActivity;
import com.hstypay.hstysales.activity.RegisterActivity;
import com.hstypay.hstysales.bean.MerchantListBean;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.hstypay.hstysales.utils.UIUtils;
import com.tencent.stat.StatService;

import java.util.List;
import java.util.Properties;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.adapter
 * @创建者: Jeremy
 * @创建时间: 2017/11/21 20:31
 * @描述: ${TODO}
 */

public class MerchantAdapter extends RecyclerView.Adapter<MerchantAdapter.ViewHolder> {

    private Activity mContext;
    private Fragment mFragment;
    private String mFromIntent;
    private int type;//页面标识
    private List<MerchantListBean.DataEntity.ItemData> mListData;

    public MerchantAdapter(Activity mContext, Fragment fragment, String fromIntent, int type, List<MerchantListBean.DataEntity.ItemData> listData) {
        this.mContext = mContext;
        this.mFragment = fragment;
        this.mFromIntent = fromIntent;
        this.type = type;
        this.mListData = listData;
        LogUtil.d("type==="+type);
    }

    @Override
    public MerchantAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_merchant_check, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MerchantAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        LogUtil.d("type==1="+type);
        if (mListData != null && mListData.size() > 0) {
            switch (type) {
                case Constants.MERCHANT_CHECK_FAILED://审核失败
                    holder.mRlTop.setVisibility(View.VISIBLE);
                    holder.mRlBottom.setVisibility(View.GONE);

                    if (mListData.get(position).getExamineRemark() != null) {
                        holder.mTvReasonTitle.setText(mContext.getString(R.string.check_failed_reason));
                        holder.mTvReason.setText(mListData.get(position).getExamineRemark());
                        holder.mLlMerchantReason.setVisibility(View.VISIBLE);
                    } else {
                        holder.mLlMerchantReason.setVisibility(View.GONE);
                    }
                    setView(holder, position);
                    break;
                case Constants.MERCHANT_CHECKING://审核中
                    holder.mRlTop.setVisibility(View.VISIBLE);
                    holder.mRlBottom.setVisibility(View.GONE);
                    holder.mLlMerchantReason.setVisibility(View.GONE);
                    /*if (mListData.get(position).getExamineRemark() != null) {
                        holder.mTvReasonTitle.setText(mContext.getString(R.string.check_failed_reason));
                        holder.mTvReason.setText(mListData.get(position).getExamineRemark());
                        holder.mLlMerchantReason.setVisibility(View.VISIBLE);
                    }else {
                        holder.mLlMerchantReason.setVisibility(View.GONE);
                    }*/
                    setView(holder, position);
                    break;
                case Constants.MERCHANT_CHECK_SUCCESS://审核通过
                    holder.mRlTop.setVisibility(View.VISIBLE);
                    holder.mRlBottom.setVisibility(View.GONE);
                    holder.mLlMerchantReason.setVisibility(View.GONE);
                    // holder.mTvReason.setText(mListData.get(position).getExamineRemark());
                    setView(holder, position);
                    break;
                case Constants.MERCHANT_NO_INFO://未进件
                    holder.mRlTop.setVisibility(View.GONE);
                    holder.mRlBottom.setVisibility(View.VISIBLE);
                    holder.mTvInfoDate.setText(mListData.get(position).getRegisteAt());
                    holder.mTvInfoTel.setText(mListData.get(position).getTelphone());
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, RegisterActivity.class);
                            intent.putExtra(Constants.REGISTER_INTENT, Constants.INFO_URL + mListData.get(position).getUserId()+"&invitationCode="+mListData.get(position).getInvitationCode());
                            LogUtil.d("url---===="+Constants.INFO_URL + mListData.get(position).getUserId()+"&invitationCode="+mListData.get(position).getInvitationCode());
                            mContext.startActivity(intent);
                        }
                    });
                    break;
                case Constants.MERCHANT_FREEZE://冻结商户
                    holder.mRlTop.setVisibility(View.VISIBLE);
                    holder.mRlBottom.setVisibility(View.GONE);
                    if (StringUtils.isEmptyOrNull(mListData.get(position).getActivateRemark())) {
                        holder.mLlMerchantReason.setVisibility(View.GONE);
                    }
                    holder.mTvReasonTitle.setText(mContext.getString(R.string.freeze_reason));
                    holder.mTvReason.setText(mListData.get(position).getActivateRemark());
                    setView(holder, position);
                    break;
                case Constants.MERCHANT_TRADABLE://可交易
                    holder.mRlBottom.setVisibility(View.GONE);
                    holder.mLlMerchantReason.setVisibility(View.GONE);
                    setView(holder, position);
                    break;
                case Constants.MERCHANT_NOT_CONFIRM://待确认
                    holder.mRlTop.setVisibility(View.VISIBLE);
                    holder.mRlBottom.setVisibility(View.GONE);
                    holder.mLlMerchantReason.setVisibility(View.GONE);
                    setView(holder, position);
                    break;
                case Constants.MERCHANT_CHANGEING://变更审核中
                    holder.mRlBottom.setVisibility(View.GONE);
                    holder.mLlMerchantReason.setVisibility(View.GONE);
                    setView(holder, position);
                    break;
                case Constants.MERCHANT_CHANGE_PASS://变更审核通过
                    holder.mRlTop.setVisibility(View.VISIBLE);
                    holder.mRlBottom.setVisibility(View.GONE);
                    holder.mLlMerchantReason.setVisibility(View.GONE);
                    setView(holder, position);
                    break;
                case Constants.MERCHANT_CHANGE_FAIL://变更失败
                    holder.mRlTop.setVisibility(View.VISIBLE);
                    holder.mRlBottom.setVisibility(View.GONE);
                    if (mListData.get(position).getExamineRemark() != null) {
                        holder.mTvReasonTitle.setText(mContext.getString(R.string.check_failed_reason));
                        holder.mTvReason.setText(mListData.get(position).getExamineRemark());
                        holder.mLlMerchantReason.setVisibility(View.VISIBLE);
                    } else {
                        holder.mLlMerchantReason.setVisibility(View.GONE);
                    }
                    setView(holder, position);
                    break;

                case Constants.MERCHANT_INVALID://作废商户
                    holder.mRlTop.setVisibility(View.VISIBLE);
                    holder.mRlBottom.setVisibility(View.GONE);
                    if (mListData.get(position).getExamineRemark() != null) {
                        holder.mTvReasonTitle.setText(mContext.getString(R.string.check_failed_reason));
                        holder.mTvReason.setText(mListData.get(position).getExamineRemark());
                        holder.mLlMerchantReason.setVisibility(View.VISIBLE);
                    } else {
                        holder.mLlMerchantReason.setVisibility(View.GONE);
                    }
                    setView(holder, position);
                    break;
            }
            holder.mIvCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StatService.trackCustomKVEvent(mContext, "1037", null);
                    if (!TextUtils.isEmpty(mListData.get(position).getTelphone())) {
                        call(mListData.get(position).getTelphone(), mContext);
                    }
                }
            });
            if (position == mListData.size() - 1) {
                holder.mViewLine.setVisibility(View.INVISIBLE);
            } else {
                holder.mViewLine.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setView(ViewHolder holder, final int position) {
        holder.mTvMerchantName.setText(mListData.get(position).getPrincipal() + "-" + mListData.get(position).getMerchantName());
        StringBuilder builder = new StringBuilder();
        builder.append("地址：");
        if (!TextUtils.isEmpty(mListData.get(position).getProvinceCnt()))
            builder.append(mListData.get(position).getProvinceCnt());
        if (!TextUtils.isEmpty(mListData.get(position).getCityCnt()))
            builder.append(mListData.get(position).getCityCnt());
        if (!TextUtils.isEmpty(mListData.get(position).getCountyCnt()))
            builder.append(mListData.get(position).getCountyCnt());
        if (!TextUtils.isEmpty(mListData.get(position).getAddress()))
            builder.append(mListData.get(position).getAddress());
        holder.mTvMerchantAddress.setText(builder.toString());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mListData.get(position).getMerchantId())) {
                    // Intent intent = new Intent(mContext, MerchantActivity.class);
                    Intent intent = new Intent(mContext, MerchantInfoActivity.class);
                    intent.putExtra(Constants.INTENT_MERCHANT_ID, mListData.get(position).getMerchantId());
                    intent.putExtra(Constants.INTENT_MERCHANT_NAME, mListData.get(position).getMerchantName());
                    intent.putExtra(Constants.INTENT_MERCHANT_TEL, mListData.get(position).getTelphone());
                    intent.putExtra("type", String.valueOf(type));
                    if (mFragment == null) {
                        mContext.startActivityForResult(intent, Constants.REQUEST_LIST_TO_MERCHANT);
                    } else {
                        mFragment.startActivityForResult(intent, Constants.REQUEST_LIST_TO_MERCHANT);
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mListData != null)
            return mListData.size();
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvReason, mTvReasonTitle, mTvMerchantName, mTvMerchantAddress, mTvInfoDate, mTvInfoTel;
        private LinearLayout mLlMerchantReason;
        private RelativeLayout mRlTop, mRlBottom;
        private ImageView mIvCall;
        private View mViewLine;

        public ViewHolder(View itemView) {
            super(itemView);
            mRlTop = (RelativeLayout) itemView.findViewById(R.id.rl_check_type_top);
            mLlMerchantReason = (LinearLayout) itemView.findViewById(R.id.ll_merchant_reason);
            mTvReason = (TextView) itemView.findViewById(R.id.tv_reason);
            mTvReasonTitle = (TextView) itemView.findViewById(R.id.tv_reason_title);

            mTvMerchantName = (TextView) itemView.findViewById(R.id.tv_merchant_name);
            mTvMerchantAddress = (TextView) itemView.findViewById(R.id.tv_merchant_address);

            mRlBottom = (RelativeLayout) itemView.findViewById(R.id.rl_check_type_bottom);
            mTvInfoDate = (TextView) itemView.findViewById(R.id.tv_info_date);
            mTvInfoTel = (TextView) itemView.findViewById(R.id.tv_info_tel);
            mIvCall = (ImageView) itemView.findViewById(R.id.iv_call);
            mViewLine = itemView.findViewById(R.id.view_line);
        }
    }

    private void call(String phone, Context context) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (ActivityNotFoundException a) {
            a.getMessage();
        }
    }
}