package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/4 21:00
 * @描述: ${TODO}
 */

public class RankBean {
    /**
     * data : null
     * logId : 0QqVhThv
     * error : {"args":[{}],"code":"auth.username.pass.fail","message":"账号或密码不正确"}
     * status : false
     */
    private ErrorEntity error;

    public void setError(ErrorEntity error) {
        this.error = error;
    }
    public ErrorEntity getError() {
        return error;
    }
    public class ErrorEntity {
        /**
         * args : [{}]
         * code : auth.username.pass.fail
         * message : 账号或密码不正确
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"data":[{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":55557.28,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试013","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":5555728,"rankNum":0,"billCount":123},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":12217.49,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"彭国卿商户","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":1221749,"rankNum":0,"billCount":38},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":10178.08,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试006","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":1017808,"rankNum":0,"billCount":20},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":2595.93,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试010","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":259593,"rankNum":0,"billCount":67},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":1403.52,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试001","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":140352,"rankNum":0,"billCount":20},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":1361.63,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试004","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":136163,"rankNum":0,"billCount":64},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":422.41,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"重庆火锅","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":42241,"rankNum":0,"billCount":72},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":346.16,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试011","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":34616,"rankNum":0,"billCount":10},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":100,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"南岸区美岑工艺饰品店测试","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":10000,"rankNum":0,"billCount":1},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":12.13,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试023","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":1213,"rankNum":0,"billCount":12},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":2.16,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试015","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":216,"rankNum":0,"billCount":9},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":0.14,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试007","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":14,"rankNum":0,"billCount":5},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":0.03,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试020","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":3,"rankNum":0,"billCount":1}],"totalPages":0,"pageSize":15,"totalRows":0,"currentPage":1}
     * logId : o2GNKZKu
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * data : [{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":55557.28,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试013","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":5555728,"rankNum":0,"billCount":123},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":12217.49,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"彭国卿商户","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":1221749,"rankNum":0,"billCount":38},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":10178.08,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试006","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":1017808,"rankNum":0,"billCount":20},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":2595.93,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试010","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":259593,"rankNum":0,"billCount":67},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":1403.52,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试001","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":140352,"rankNum":0,"billCount":20},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":1361.63,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试004","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":136163,"rankNum":0,"billCount":64},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":422.41,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"重庆火锅","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":42241,"rankNum":0,"billCount":72},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":346.16,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试011","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":34616,"rankNum":0,"billCount":10},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":100,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"南岸区美岑工艺饰品店测试","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":10000,"rankNum":0,"billCount":1},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":12.13,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试023","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":1213,"rankNum":0,"billCount":12},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":2.16,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试015","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":216,"rankNum":0,"billCount":9},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":0.14,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试007","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":14,"rankNum":0,"billCount":5},{"successFeeYuan":0,"benefitTotalFeeYuan":0,"commissionFeeYuan":0,"benefitRateStr":"0.0\u2030","totalFeeYuan":0.03,"commissionFeeSix":0,"billRateStr":"0.0\u2030","merchantName":"汇商通盈测试020","payRemitFeeYuan":0,"payNetFeeYuan":0,"refundFeeYuan":0,"totalFee":3,"rankNum":0,"billCount":1}]
         * totalPages : 0
         * pageSize : 15
         * totalRows : 0
         * currentPage : 1
         */
        private List<ItemData> data;
        private int totalPages;
        private int pageSize;
        private int totalRows;
        private int currentPage;

        public void setData(List<ItemData> data) {
            this.data = data;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public List<ItemData> getData() {
            return data;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public int getPageSize() {
            return pageSize;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public class ItemData {
            /**
             * successFeeYuan : 0.0
             * benefitTotalFeeYuan : 0
             * commissionFeeYuan : 0
             * benefitRateStr : 0.0‰
             * totalFeeYuan : 55557.28
             * commissionFeeSix : 0
             * billRateStr : 0.0‰
             * merchantName : 汇商通盈测试013
             * payRemitFeeYuan : 0
             * payNetFeeYuan : 0.0
             * refundFeeYuan : 0.0
             * totalFee : 5555728
             * rankNum : 0
             * billCount : 123
             */
            private double successFeeYuan;
            private int benefitTotalFeeYuan;
            private int commissionFeeYuan;
            private String benefitRateStr;
            private double totalFeeYuan;
            private int commissionFeeSix;
            private String billRateStr;
            private String merchantName;
            private int payRemitFeeYuan;
            private double payNetFeeYuan;
            private double refundFeeYuan;
            private long totalFee;
            private int rankNum;
            private long billCount;
            private String principal;
            private String telphone;

            public String getTelphone() {
                return telphone;
            }

            public void setTelphone(String telphone) {
                this.telphone = telphone;
            }

            public String getPrincipal() {
                return principal;
            }

            public void setPrincipal(String principal) {
                this.principal = principal;
            }

            public void setSuccessFeeYuan(double successFeeYuan) {
                this.successFeeYuan = successFeeYuan;
            }

            public void setBenefitTotalFeeYuan(int benefitTotalFeeYuan) {
                this.benefitTotalFeeYuan = benefitTotalFeeYuan;
            }

            public void setCommissionFeeYuan(int commissionFeeYuan) {
                this.commissionFeeYuan = commissionFeeYuan;
            }

            public void setBenefitRateStr(String benefitRateStr) {
                this.benefitRateStr = benefitRateStr;
            }

            public void setTotalFeeYuan(double totalFeeYuan) {
                this.totalFeeYuan = totalFeeYuan;
            }

            public void setCommissionFeeSix(int commissionFeeSix) {
                this.commissionFeeSix = commissionFeeSix;
            }

            public void setBillRateStr(String billRateStr) {
                this.billRateStr = billRateStr;
            }

            public void setMerchantName(String merchantName) {
                this.merchantName = merchantName;
            }

            public void setPayRemitFeeYuan(int payRemitFeeYuan) {
                this.payRemitFeeYuan = payRemitFeeYuan;
            }

            public void setPayNetFeeYuan(double payNetFeeYuan) {
                this.payNetFeeYuan = payNetFeeYuan;
            }

            public void setRefundFeeYuan(double refundFeeYuan) {
                this.refundFeeYuan = refundFeeYuan;
            }

            public void setTotalFee(long totalFee) {
                this.totalFee = totalFee;
            }

            public void setRankNum(int rankNum) {
                this.rankNum = rankNum;
            }

            public void setBillCount(long billCount) {
                this.billCount = billCount;
            }

            public double getSuccessFeeYuan() {
                return successFeeYuan;
            }

            public int getBenefitTotalFeeYuan() {
                return benefitTotalFeeYuan;
            }

            public int getCommissionFeeYuan() {
                return commissionFeeYuan;
            }

            public String getBenefitRateStr() {
                return benefitRateStr;
            }

            public double getTotalFeeYuan() {
                return totalFeeYuan;
            }

            public int getCommissionFeeSix() {
                return commissionFeeSix;
            }

            public String getBillRateStr() {
                return billRateStr;
            }

            public String getMerchantName() {
                return merchantName;
            }

            public int getPayRemitFeeYuan() {
                return payRemitFeeYuan;
            }

            public double getPayNetFeeYuan() {
                return payNetFeeYuan;
            }

            public double getRefundFeeYuan() {
                return refundFeeYuan;
            }

            public long getTotalFee() {
                return totalFee;
            }

            public int getRankNum() {
                return rankNum;
            }

            public long getBillCount() {
                return billCount;
            }
        }
    }
}
