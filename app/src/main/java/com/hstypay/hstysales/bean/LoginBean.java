package com.hstypay.hstysales.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/7/25.
 */

public class LoginBean implements Serializable {

    /**
     * data : null
     * logId : 0QqVhThv
     * error : {"args":[{}],"code":"auth.username.pass.fail","message":"账号或密码不正确"}
     * status : false
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    /**
     * data : {"ops":{"userOps":[{}]},"signKey":"8ce420aaed5fece68c852a3bba0c1ba2","funcs":[{"funcName":"业务员APP相关","ordered":0,"funcUrl":"","permissions":[{"permissionCode":"APPLOGIN","funcId":122,"permissionName":"登录"}],"appId":2,"menuFlag":false,"funcCode":"SALESMAN-APP-OPERATION","parentFunc":0,"remark":"业务员APP登录","funcId":122}],"user":{"realName":"小坤坤","updatePwdFlag":false,"roles":[{"createTime":"2017-12-02 10:05:35","enable":true,"roleId":113,"appId":2,"roleCode":"SALESMAN","roleName":"业务员","serviceProvider":"731100000001","createUser":1,"createUserName":"superadmin","remark":"服务商平台业务员","updateTime":"2017-12-02 10:05:35","deleteEnable":false}],"serviceProvider":{"serviceProviderId":"731100000001","serviceProviderName":"我是新合伙人"},"emp":{"empId":17,"serviceProviderId":"731100000001","empType":1,"salesGroupName":"","mobile":"18664579147","createUserName":"超级管理员","remark":"","updateTime":"2017-12-02 11:30:43","userId":85,"enabled":true,"empSex":0,"createTime":"2017-12-02 11:30:43","empName":"小坤坤","enabledCnt":"启用","createUser":1},"userType":2,"userName":"ykk123","userId":85,"orgId":"731100000001"}}
     * logId : VmQXcppX
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * ops : {"userOps":[{}]}
         * signKey : 8ce420aaed5fece68c852a3bba0c1ba2
         * funcs : [{"funcName":"业务员APP相关","ordered":0,"funcUrl":"","permissions":[{"permissionCode":"APPLOGIN","funcId":122,"permissionName":"登录"}],"appId":2,"menuFlag":false,"funcCode":"SALESMAN-APP-OPERATION","parentFunc":0,"remark":"业务员APP登录","funcId":122}]
         * user : {"realName":"小坤坤","updatePwdFlag":false,"roles":[{"createTime":"2017-12-02 10:05:35","enable":true,"roleId":113,"appId":2,"roleCode":"SALESMAN","roleName":"业务员","serviceProvider":"731100000001","createUser":1,"createUserName":"superadmin","remark":"服务商平台业务员","updateTime":"2017-12-02 10:05:35","deleteEnable":false}],"serviceProvider":{"serviceProviderId":"731100000001","serviceProviderName":"我是新合伙人"},"emp":{"empId":17,"serviceProviderId":"731100000001","empType":1,"salesGroupName":"","mobile":"18664579147","createUserName":"超级管理员","remark":"","updateTime":"2017-12-02 11:30:43","userId":85,"enabled":true,"empSex":0,"createTime":"2017-12-02 11:30:43","empName":"小坤坤","enabledCnt":"启用","createUser":1},"userType":2,"userName":"ykk123","userId":85,"orgId":"731100000001"}
         */
        private OpsEntity ops;
        private String signKey;
        private List<FuncsEntity> funcs;
        private UserEntity user;

        public void setOps(OpsEntity ops) {
            this.ops = ops;
        }

        public void setSignKey(String signKey) {
            this.signKey = signKey;
        }

        public void setFuncs(List<FuncsEntity> funcs) {
            this.funcs = funcs;
        }

        public void setUser(UserEntity user) {
            this.user = user;
        }

        public OpsEntity getOps() {
            return ops;
        }

        public String getSignKey() {
            return signKey;
        }

        public List<FuncsEntity> getFuncs() {
            return funcs;
        }

        public UserEntity getUser() {
            return user;
        }

        public class OpsEntity {
            /**
             * userOps : [{}]
             */
            private List<UserOpsEntity> userOps;

            public void setUserOps(List<UserOpsEntity> userOps) {
                this.userOps = userOps;
            }

            public List<UserOpsEntity> getUserOps() {
                return userOps;
            }

            public class UserOpsEntity {

            }
        }

        public class FuncsEntity {
            /**
             * funcName : 业务员APP相关
             * ordered : 0
             * funcUrl :
             * permissions : [{"permissionCode":"APPLOGIN","funcId":122,"permissionName":"登录"}]
             * appId : 2
             * menuFlag : false
             * funcCode : SALESMAN-APP-OPERATION
             * parentFunc : 0
             * remark : 业务员APP登录
             * funcId : 122
             */
            private String funcName;
            private int ordered;
            private String funcUrl;
            private List<PermissionsEntity> permissions;
            private int appId;
            private boolean menuFlag;
            private String funcCode;
            private int parentFunc;
            private String remark;
            private int funcId;

            public void setFuncName(String funcName) {
                this.funcName = funcName;
            }

            public void setOrdered(int ordered) {
                this.ordered = ordered;
            }

            public void setFuncUrl(String funcUrl) {
                this.funcUrl = funcUrl;
            }

            public void setPermissions(List<PermissionsEntity> permissions) {
                this.permissions = permissions;
            }

            public void setAppId(int appId) {
                this.appId = appId;
            }

            public void setMenuFlag(boolean menuFlag) {
                this.menuFlag = menuFlag;
            }

            public void setFuncCode(String funcCode) {
                this.funcCode = funcCode;
            }

            public void setParentFunc(int parentFunc) {
                this.parentFunc = parentFunc;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public void setFuncId(int funcId) {
                this.funcId = funcId;
            }

            public String getFuncName() {
                return funcName;
            }

            public int getOrdered() {
                return ordered;
            }

            public String getFuncUrl() {
                return funcUrl;
            }

            public List<PermissionsEntity> getPermissions() {
                return permissions;
            }

            public int getAppId() {
                return appId;
            }

            public boolean isMenuFlag() {
                return menuFlag;
            }

            public String getFuncCode() {
                return funcCode;
            }

            public int getParentFunc() {
                return parentFunc;
            }

            public String getRemark() {
                return remark;
            }

            public int getFuncId() {
                return funcId;
            }

            public class PermissionsEntity {
                /**
                 * permissionCode : APPLOGIN
                 * funcId : 122
                 * permissionName : 登录
                 */
                private String permissionCode;
                private int funcId;
                private String permissionName;

                public void setPermissionCode(String permissionCode) {
                    this.permissionCode = permissionCode;
                }

                public void setFuncId(int funcId) {
                    this.funcId = funcId;
                }

                public void setPermissionName(String permissionName) {
                    this.permissionName = permissionName;
                }

                public String getPermissionCode() {
                    return permissionCode;
                }

                public int getFuncId() {
                    return funcId;
                }

                public String getPermissionName() {
                    return permissionName;
                }
            }
        }

        public class UserEntity {
            /**
             * realName : 小坤坤
             * updatePwdFlag : false
             * roles : [{"createTime":"2017-12-02 10:05:35","enable":true,"roleId":113,"appId":2,"roleCode":"SALESMAN","roleName":"业务员","serviceProvider":"731100000001","createUser":1,"createUserName":"superadmin","remark":"服务商平台业务员","updateTime":"2017-12-02 10:05:35","deleteEnable":false}]
             * serviceProvider : {"serviceProviderId":"731100000001","serviceProviderName":"我是新合伙人"}
             * emp : {"empId":17,"serviceProviderId":"731100000001","empType":1,"salesGroupName":"","mobile":"18664579147","createUserName":"超级管理员","remark":"","updateTime":"2017-12-02 11:30:43","userId":85,"enabled":true,"empSex":0,"createTime":"2017-12-02 11:30:43","empName":"小坤坤","enabledCnt":"启用","createUser":1}
             * userType : 2
             * userName : ykk123
             * userId : 85
             * orgId : 731100000001
             */
            private String realName;
            private boolean updatePwdFlag;
            private List<RolesEntity> roles;
            private ServiceProviderEntity serviceProvider;
            private ServiceChannelEntity serviceChannel;
            private EmpEntity emp;
            private int userType;
            private String userName;
            private int userId;
            private String orgId;
            private String loginPhone;

            public ServiceChannelEntity getServiceChannel() {
                return serviceChannel;
            }

            public void setServiceChannel(ServiceChannelEntity serviceChannel) {
                this.serviceChannel = serviceChannel;
            }

            public String getLoginPhone() {
                return loginPhone;
            }

            public void setLoginPhone(String loginPhone) {
                this.loginPhone = loginPhone;
            }

            public void setRealName(String realName) {
                this.realName = realName;
            }

            public void setUpdatePwdFlag(boolean updatePwdFlag) {
                this.updatePwdFlag = updatePwdFlag;
            }

            public void setRoles(List<RolesEntity> roles) {
                this.roles = roles;
            }

            public void setServiceProvider(ServiceProviderEntity serviceProvider) {
                this.serviceProvider = serviceProvider;
            }

            public void setEmp(EmpEntity emp) {
                this.emp = emp;
            }

            public void setUserType(int userType) {
                this.userType = userType;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public void setOrgId(String orgId) {
                this.orgId = orgId;
            }

            public String getRealName() {
                return realName;
            }

            public boolean isUpdatePwdFlag() {
                return updatePwdFlag;
            }

            public List<RolesEntity> getRoles() {
                return roles;
            }

            public ServiceProviderEntity getServiceProvider() {
                return serviceProvider;
            }

            public EmpEntity getEmp() {
                return emp;
            }

            public int getUserType() {
                return userType;
            }

            public String getUserName() {
                return userName;
            }

            public int getUserId() {
                return userId;
            }

            public String getOrgId() {
                return orgId;
            }

            public class RolesEntity {
                /**
                 * createTime : 2017-12-02 10:05:35
                 * enable : true
                 * roleId : 113
                 * appId : 2
                 * roleCode : SALESMAN
                 * roleName : 业务员
                 * serviceProvider : 731100000001
                 * createUser : 1
                 * createUserName : superadmin
                 * remark : 服务商平台业务员
                 * updateTime : 2017-12-02 10:05:35
                 * deleteEnable : false
                 */
                private String createTime;
                private boolean enable;
                private int roleId;
                private int appId;
                private String roleCode;
                private String roleName;
                private String serviceProvider;
                private int createUser;
                private String createUserName;
                private String remark;
                private String updateTime;
                private boolean deleteEnable;

                public void setCreateTime(String createTime) {
                    this.createTime = createTime;
                }

                public void setEnable(boolean enable) {
                    this.enable = enable;
                }

                public void setRoleId(int roleId) {
                    this.roleId = roleId;
                }

                public void setAppId(int appId) {
                    this.appId = appId;
                }

                public void setRoleCode(String roleCode) {
                    this.roleCode = roleCode;
                }

                public void setRoleName(String roleName) {
                    this.roleName = roleName;
                }

                public void setServiceProvider(String serviceProvider) {
                    this.serviceProvider = serviceProvider;
                }

                public void setCreateUser(int createUser) {
                    this.createUser = createUser;
                }

                public void setCreateUserName(String createUserName) {
                    this.createUserName = createUserName;
                }

                public void setRemark(String remark) {
                    this.remark = remark;
                }

                public void setUpdateTime(String updateTime) {
                    this.updateTime = updateTime;
                }

                public void setDeleteEnable(boolean deleteEnable) {
                    this.deleteEnable = deleteEnable;
                }

                public String getCreateTime() {
                    return createTime;
                }

                public boolean isEnable() {
                    return enable;
                }

                public int getRoleId() {
                    return roleId;
                }

                public int getAppId() {
                    return appId;
                }

                public String getRoleCode() {
                    return roleCode;
                }

                public String getRoleName() {
                    return roleName;
                }

                public String getServiceProvider() {
                    return serviceProvider;
                }

                public int getCreateUser() {
                    return createUser;
                }

                public String getCreateUserName() {
                    return createUserName;
                }

                public String getRemark() {
                    return remark;
                }

                public String getUpdateTime() {
                    return updateTime;
                }

                public boolean isDeleteEnable() {
                    return deleteEnable;
                }
            }

            /**
             * 省运营
             */
            public class ServiceProviderEntity {
                /**
                 * serviceProviderId : 731100000001
                 * serviceProviderName : 我是新合伙人
                 */
                private String serviceProviderId;
                private String serviceProviderName;

                public void setServiceProviderId(String serviceProviderId) {
                    this.serviceProviderId = serviceProviderId;
                }

                public void setServiceProviderName(String serviceProviderName) {
                    this.serviceProviderName = serviceProviderName;
                }

                public String getServiceProviderId() {
                    return serviceProviderId;
                }

                public String getServiceProviderName() {
                    return serviceProviderName;
                }
            }

            /**
             * 市运营
             */
            public class ServiceChannelEntity {
                /**
                 * serviceProviderId : 731100000001
                 * serviceProviderName : 我是新合伙人
                 */
                private String serviceProviderId;
                private String serviceProviderName;

                public void setServiceProviderId(String serviceProviderId) {
                    this.serviceProviderId = serviceProviderId;
                }

                public void setServiceProviderName(String serviceProviderName) {
                    this.serviceProviderName = serviceProviderName;
                }

                public String getServiceProviderId() {
                    return serviceProviderId;
                }

                public String getServiceProviderName() {
                    return serviceProviderName;
                }
            }

            public class EmpEntity {
                /**
                 * empId : 17
                 * serviceProviderId : 731100000001
                 * empType : 1
                 * salesGroupName :
                 * mobile : 18664579147
                 * createUserName : 超级管理员
                 * remark :
                 * updateTime : 2017-12-02 11:30:43
                 * userId : 85
                 * enabled : true
                 * empSex : 0
                 * createTime : 2017-12-02 11:30:43
                 * empName : 小坤坤
                 * enabledCnt : 启用
                 * createUser : 1
                 */
                private int empId;
                private String serviceProviderId;
                private int empType;
                private String salesGroupName;
                private String mobile;
                private String createUserName;
                private String remark;
                private String updateTime;
                private int userId;
                private boolean enabled;
                private int empSex;
                private String createTime;
                private String empName;
                private String enabledCnt;
                private int createUser;

                public void setEmpId(int empId) {
                    this.empId = empId;
                }

                public void setServiceProviderId(String serviceProviderId) {
                    this.serviceProviderId = serviceProviderId;
                }

                public void setEmpType(int empType) {
                    this.empType = empType;
                }

                public void setSalesGroupName(String salesGroupName) {
                    this.salesGroupName = salesGroupName;
                }

                public void setMobile(String mobile) {
                    this.mobile = mobile;
                }

                public void setCreateUserName(String createUserName) {
                    this.createUserName = createUserName;
                }

                public void setRemark(String remark) {
                    this.remark = remark;
                }

                public void setUpdateTime(String updateTime) {
                    this.updateTime = updateTime;
                }

                public void setUserId(int userId) {
                    this.userId = userId;
                }

                public void setEnabled(boolean enabled) {
                    this.enabled = enabled;
                }

                public void setEmpSex(int empSex) {
                    this.empSex = empSex;
                }

                public void setCreateTime(String createTime) {
                    this.createTime = createTime;
                }

                public void setEmpName(String empName) {
                    this.empName = empName;
                }

                public void setEnabledCnt(String enabledCnt) {
                    this.enabledCnt = enabledCnt;
                }

                public void setCreateUser(int createUser) {
                    this.createUser = createUser;
                }

                public int getEmpId() {
                    return empId;
                }

                public String getServiceProviderId() {
                    return serviceProviderId;
                }

                public int getEmpType() {
                    return empType;
                }

                public String getSalesGroupName() {
                    return salesGroupName;
                }

                public String getMobile() {
                    return mobile;
                }

                public String getCreateUserName() {
                    return createUserName;
                }

                public String getRemark() {
                    return remark;
                }

                public String getUpdateTime() {
                    return updateTime;
                }

                public int getUserId() {
                    return userId;
                }

                public boolean isEnabled() {
                    return enabled;
                }

                public int getEmpSex() {
                    return empSex;
                }

                public String getCreateTime() {
                    return createTime;
                }

                public String getEmpName() {
                    return empName;
                }

                public String getEnabledCnt() {
                    return enabledCnt;
                }

                public int getCreateUser() {
                    return createUser;
                }
            }
        }
    }
}
