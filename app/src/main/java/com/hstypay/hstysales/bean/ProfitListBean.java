package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/10/29
 * @desc 有分页处理的分润列表bean，包括分润统计，日分润明细
 */
public class ProfitListBean extends BaseBean<ProfitListBean.Data>{

    public class Data{
        private int pageSize;
        private int currentPage;
        private int totalPages;
        private int totalRows;
        private List<SummaryData> data;

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public List<SummaryData> getData() {
            return data;
        }

        public void setData(List<SummaryData> data) {
            this.data = data;
        }
    }

    public class SummaryData{
        private Double salesmanTotalFee;//分润金额（分）
        private Double salesmanTotalFeeYuan;//分润金额（元）,要处理为空的情况
        private double payNetFee;//交易净额（分）
        private double payNetFeeYuan;//交易净额（元）
        private String merchantName;//商户名称
        private String payTradeTime;//日期  2020-10-02 00:00:00

        public String getMerchantName() {
            return merchantName;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public String getPayTradeTime() {
            return payTradeTime;
        }

        public void setPayTradeTime(String payTradeTime) {
            this.payTradeTime = payTradeTime;
        }

        public Double getSalesmanTotalFee() {
            return salesmanTotalFee;
        }

        public void setSalesmanTotalFee(Double salesmanTotalFee) {
            this.salesmanTotalFee = salesmanTotalFee;
        }

        public Double getSalesmanTotalFeeYuan() {
            return salesmanTotalFeeYuan;
        }

        public void setSalesmanTotalFeeYuan(Double salesmanTotalFeeYuan) {
            this.salesmanTotalFeeYuan = salesmanTotalFeeYuan;
        }

        public double getPayNetFee() {
            return payNetFee;
        }

        public void setPayNetFee(double payNetFee) {
            this.payNetFee = payNetFee;
        }

        public double getPayNetFeeYuan() {
            return payNetFeeYuan;
        }

        public void setPayNetFeeYuan(double payNetFeeYuan) {
            this.payNetFeeYuan = payNetFeeYuan;
        }

    }
}
