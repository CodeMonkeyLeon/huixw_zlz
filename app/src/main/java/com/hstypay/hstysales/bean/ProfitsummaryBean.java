package com.hstypay.hstysales.bean;

/**
 * @author zeyu.kuang
 * @time 2020/10/29
 * @desc 分润统计-概览副本
 */
public class ProfitsummaryBean extends BaseBean<ProfitsummaryBean.ProfitSummaryData> {

    public class ProfitSummaryData {
        private int refundCount;//退款笔数
        private double refundFee;//退款金额（分）
        private double refundFeeYuan;//退款金额（元）
        private int successCount;//收款笔数
        private double payFee;//收款金额（分）
        private double payFeeYuan;//收款金额（元）
        private Double salesmanTotalFee;//分润金额（分）,需要处理为空的情况
        private Double salesmanTotalFeeYuan;//分润金额（元）
        private double payNetFee;//交易净额（分）
        private double payNetFeeYuan;//交易净额（元）

        public int getRefundCount() {
            return refundCount;
        }

        public void setRefundCount(int refundCount) {
            this.refundCount = refundCount;
        }

        public double getRefundFee() {
            return refundFee;
        }

        public void setRefundFee(double refundFee) {
            this.refundFee = refundFee;
        }

        public double getRefundFeeYuan() {
            return refundFeeYuan;
        }

        public void setRefundFeeYuan(double refundFeeYuan) {
            this.refundFeeYuan = refundFeeYuan;
        }

        public int getSuccessCount() {
            return successCount;
        }

        public void setSuccessCount(int successCount) {
            this.successCount = successCount;
        }

        public double getPayFee() {
            return payFee;
        }

        public void setPayFee(double payFee) {
            this.payFee = payFee;
        }

        public double getPayFeeYuan() {
            return payFeeYuan;
        }

        public void setPayFeeYuan(double payFeeYuan) {
            this.payFeeYuan = payFeeYuan;
        }

        public Double getSalesmanTotalFee() {
            return salesmanTotalFee;
        }

        public void setSalesmanTotalFee(Double salesmanTotalFee) {
            this.salesmanTotalFee = salesmanTotalFee;
        }

        public Double getSalesmanTotalFeeYuan() {
            return salesmanTotalFeeYuan;
        }

        public void setSalesmanTotalFeeYuan(Double salesmanTotalFeeYuan) {
            this.salesmanTotalFeeYuan = salesmanTotalFeeYuan;
        }

        public double getPayNetFee() {
            return payNetFee;
        }

        public void setPayNetFee(double payNetFee) {
            this.payNetFee = payNetFee;
        }

        public double getPayNetFeeYuan() {
            return payNetFeeYuan;
        }

        public void setPayNetFeeYuan(double payNetFeeYuan) {
            this.payNetFeeYuan = payNetFeeYuan;
        }
    }
}
