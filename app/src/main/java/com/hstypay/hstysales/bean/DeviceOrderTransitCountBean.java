package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/8/26
 * @desc 订单管理 -> 在途订单数量
 */
public class DeviceOrderTransitCountBean {

    private ErrorEntity error;
    public void setError(ErrorEntity error) {
        this.error = error;
    }
    public ErrorEntity getError() {
        return error;
    }
    public static class ErrorEntity {
        /**
         * args : [{}]
         * code : auth.username.pass.fail
         * message : 账号或密码不正确
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * logId : cebfcb2b159c4e728a1fce0e7c601363
     * status : true
     * data : [{"tradeStatus":1,"count":2},{"tradeStatus":1,"count":2}]
     */
    private String logId;
    private boolean status;
    private List<DataBean> data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * tradeStatus : 1
         * count : 2
         */

        private int tradeStatus;
        private int count;

        public int getTradeStatus() {
            return tradeStatus;
        }

        public void setTradeStatus(int tradeStatus) {
            this.tradeStatus = tradeStatus;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }
}
