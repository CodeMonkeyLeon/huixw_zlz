package com.hstypay.hstysales.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2018/4/26 10:52
 * @描述: ${TODO}
 */

public class VipInfoBean {
    /**
     * data : {"picfilefm":"/pic/2017/08/04/af81e9fe-284f-4412-a9ff-383cfc7dd66c.jpg","license":"/pic/2017/08/04/6b6f2292-2bae-4502-a53f-7cfdd97adf93.jpg","picfilezm":"/pic/2017/08/04/ddee6216-b515-4d37-9069-642b44b9ca3b.jpg","store":"/pic/2017/08/04/0ae995e5-d0e4-41cc-a6b7-f8e080d3e0b5.jpg"}
     * logId : ti41OKRM
     * status : true
     */
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"brandName":"力争上游","costRate":9123,"merchantId":"358200000010","brandLogo":"/pic/mch/2018/04/26/9bae3757-33ed-4693-a736-2e38b6db5108.jpg","examineStatus":2}
     * logId : TvygfTSY
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity implements Serializable {
        /**
         * brandName : 力争上游
         * costRate : 9123
         * merchantId : 358200000010
         * brandLogo : /pic/mch/2018/04/26/9bae3757-33ed-4693-a736-2e38b6db5108.jpg
         * examineStatus : 2
         */
        private String brandName;
        private String costRate;
        private String merchantId;
        private String brandLogo;
        private String examineRemark;
        private int examineStatus;

        public String getExamineRemark() {
            return examineRemark;
        }

        public void setExamineRemark(String examineRemark) {
            this.examineRemark = examineRemark;
        }

        public void setBrandName(String brandName) {
            this.brandName = brandName;
        }

        public void setCostRate(String costRate) {
            this.costRate = costRate;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public void setBrandLogo(String brandLogo) {
            this.brandLogo = brandLogo;
        }

        public void setExamineStatus(int examineStatus) {
            this.examineStatus = examineStatus;
        }

        public String getBrandName() {
            return brandName;
        }

        public String getCostRate() {
            return costRate;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public String getBrandLogo() {
            return brandLogo;
        }

        public int getExamineStatus() {
            return examineStatus;
        }
    }
}
