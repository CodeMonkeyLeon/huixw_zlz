package com.hstypay.hstysales.bean;

import java.io.Serializable;

/**
 * 更新橙意宝的response
 */
public class OrangeStateBean implements Serializable {
    //{"logId":"2ZLxVm6o","status":false,"error":{"code":"app.commons.para.null.exception","message":"请求参数为空","args":[]},"data":null}
    //{"logId":"tsRFf11o","status":false,"error":{"code":"","message":"不存在翼支付签约记录","args":null},"data":null}
    //{"logId":"e0aNd9bg","status":true}
    private String logId;
    private Boolean status;
    private Error error;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public static class Error implements Serializable {
        private String code;
        private String message;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}
