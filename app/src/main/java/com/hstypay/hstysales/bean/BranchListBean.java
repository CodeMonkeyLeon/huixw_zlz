package com.hstypay.hstysales.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/16 20:42
 * @描述: ${TODO}
 */

public class BranchListBean {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : [{"bankBranchId":1086,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 19:00:04","bankBranchName":"招商银行股份有限公司北京富力又一城支行","contactLine":"308100005922","createUser":0,"createUserName":"system","updateTime":"2017-09-08 19:00:04"},{"bankBranchId":2124,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 18:59:41","bankBranchName":"招商银行股份有限公司北京三环新城支行","contactLine":"308100005754","createUser":0,"createUserName":"system","updateTime":"2017-09-08 18:59:41"},{"bankBranchId":4606,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 19:00:16","bankBranchName":"招商银行股份有限公司北京甘家口支行","contactLine":"308100005051","createUser":0,"createUserName":"system","updateTime":"2017-09-08 19:00:16"},{"bankBranchId":4724,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 18:58:37","bankBranchName":"招商银行股份有限公司北京西直门支行","contactLine":"308100005631","createUser":0,"createUserName":"system","updateTime":"2017-09-08 18:58:37"},{"bankBranchId":6923,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 18:57:39","bankBranchName":"招商银行股份有限公司北京清华东路支行","contactLine":"308100005787","createUser":0,"createUserName":"system","updateTime":"2017-09-08 18:57:39"},{"bankBranchId":7845,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 18:57:33","bankBranchName":"招商银行股份有限公司北京北苑路支行","contactLine":"308100005369","createUser":0,"createUserName":"system","updateTime":"2017-09-08 18:57:33"},{"bankBranchId":8917,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 18:58:25","bankBranchName":"招商银行股份有限公司北京朝外大街支行","contactLine":"308100005328","createUser":0,"createUserName":"system","updateTime":"2017-09-08 18:58:25"},{"bankBranchId":10725,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 18:59:28","bankBranchName":"招商银行股份有限公司北京金融街中心支行","contactLine":"308100005537","createUser":0,"createUserName":"system","updateTime":"2017-09-08 18:59:28"},{"bankBranchId":13730,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 18:59:29","bankBranchName":"招商银行股份有限公司北京广渠路支行","contactLine":"308100005826","createUser":0,"createUserName":"system","updateTime":"2017-09-08 18:59:29"},{"bankBranchId":13893,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 18:59:07","bankBranchName":"招商银行股份有限公司北京京广桥支行","contactLine":"308100005588","createUser":0,"createUserName":"system","updateTime":"2017-09-08 18:59:07"},{"bankBranchId":14398,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 18:59:13","bankBranchName":"招商银行股份有限公司北京亦庄支行","contactLine":"308100005699","createUser":0,"createUserName":"system","updateTime":"2017-09-08 18:59:13"},{"bankBranchId":15058,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 18:57:56","bankBranchName":"招商银行股份有限公司北京德胜门支行","contactLine":"308100005289","createUser":0,"createUserName":"system","updateTime":"2017-09-08 18:57:56"},{"bankBranchId":16167,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 18:58:08","bankBranchName":"招商银行股份有限公司北京亦庄文化园支行","contactLine":"308100005891","createUser":0,"createUserName":"system","updateTime":"2017-09-08 18:58:08"},{"bankBranchId":17401,"bankId":5,"province":"010000","city":"010100","createTime":"2017-09-08 18:57:59","bankBranchName":"招商银行股份有限公司北京凤凰商街支行","contactLine":"308100005795","createUser":0,"createUserName":"system","updateTime":"2017-09-08 18:57:59"}]
     * logId : IWQKMVVN
     * status : true
     */
    private List<DataEntity> data;
    private String logId;
    private boolean status;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity implements Serializable{
        /**
         * bankBranchId : 1086
         * bankId : 5
         * province : 010000
         * city : 010100
         * createTime : 2017-09-08 19:00:04
         * bankBranchName : 招商银行股份有限公司北京富力又一城支行
         * contactLine : 308100005922
         * createUser : 0
         * createUserName : system
         * updateTime : 2017-09-08 19:00:04
         */
        private long bankBranchId;
        private long bankId;
        private String province;
        private String city;
        private String createTime;
        private String bankBranchName;
        private String contactLine;
        private String createUser;
        private String createUserName;
        private String updateTime;

        public void setBankBranchId(long bankBranchId) {
            this.bankBranchId = bankBranchId;
        }

        public void setBankId(long bankId) {
            this.bankId = bankId;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public void setBankBranchName(String bankBranchName) {
            this.bankBranchName = bankBranchName;
        }

        public void setContactLine(String contactLine) {
            this.contactLine = contactLine;
        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public long getBankBranchId() {
            return bankBranchId;
        }

        public long getBankId() {
            return bankId;
        }

        public String getProvince() {
            return province;
        }

        public String getCity() {
            return city;
        }

        public String getCreateTime() {
            return createTime;
        }

        public String getBankBranchName() {
            return bankBranchName;
        }

        public String getContactLine() {
            return contactLine;
        }

        public String getCreateUser() {
            return createUser;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public String getUpdateTime() {
            return updateTime;
        }
    }
}
