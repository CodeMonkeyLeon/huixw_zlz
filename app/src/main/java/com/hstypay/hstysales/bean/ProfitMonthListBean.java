package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/10/29
 * @desc 无分页处理的分润列表bean，包括月分润明细
 */
public class ProfitMonthListBean extends BaseBean<List<ProfitListBean.SummaryData>>{

}
