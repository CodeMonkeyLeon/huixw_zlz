package com.hstypay.hstysales.bean;

import com.umeng.socialize.media.Base;

import java.util.List;

public class SignMerchantListBean extends BaseBean<SignMerchantListBean.SignMerchantListData> {
    public class SignMerchantListData{
        private int pageSize;
        private int currentPage;
        private int totalPages;
        private int totalRows;
        private List<WxSignDetailBean.SignDetailData> data;

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public List<WxSignDetailBean.SignDetailData> getData() {
            return data;
        }

        public void setData(List<WxSignDetailBean.SignDetailData> data) {
            this.data = data;
        }
    }
}
