package com.hstypay.hstysales.bean;

import java.io.Serializable;

public class WxSignDetailBean extends BaseBean<WxSignDetailBean.SignDetailData> implements Serializable{
    public class SignDetailData implements Serializable {
        private String id;
        private String merchantId;
        private String merchantIdCnt;
        private String contractType;
        private String contractName;
        private String status;//签约状态, 0-初始,10-签约成功,20-签约失败, 30-待签约
        private String signTime;
        private String statusCnt;
        private String tradeCode;
        private String wechatRealQrcodeUrl;
        private String createTime;
        private String updateTime;
        private String pageSize;
        private String currentPage;
        private String serviceProviderId;
        private String serviceChannelId;
        private String thirdChannelId;
        private String principal;
        private String telephone;
        private String accountCode;
        private String failDesc;
        private String regType;
        private String regTypeCnt;
        private String tryNum;
        private String payCenterId;
        private boolean payTypeEnable;
        private String merchantName;

        public String getId() {
            return id == null ? "" : id;


        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMerchantId() {
            return merchantId == null ? "" : merchantId;


        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getMerchantIdCnt() {
            return merchantIdCnt == null ? "" : merchantIdCnt;


        }

        public void setMerchantIdCnt(String merchantIdCnt) {
            this.merchantIdCnt = merchantIdCnt;
        }

        public String getContractType() {
            return contractType == null ? "" : contractType;


        }

        public void setContractType(String contractType) {
            this.contractType = contractType;
        }

        public String getContractName() {
            return contractName == null ? "" : contractName;


        }

        public void setContractName(String contractName) {
            this.contractName = contractName;
        }

        public String getStatus() {
            return status == null ? "" : status;


        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSignTime() {
            return signTime == null ? "" : signTime;


        }

        public void setSignTime(String signTime) {
            this.signTime = signTime;
        }

        public String getStatusCnt() {
            return statusCnt == null ? "" : statusCnt;


        }

        public void setStatusCnt(String statusCnt) {
            this.statusCnt = statusCnt;
        }

        public String getTradeCode() {
            return tradeCode == null ? "" : tradeCode;


        }

        public void setTradeCode(String tradeCode) {
            this.tradeCode = tradeCode;
        }

        public String getWechatRealQrcodeUrl() {
            return wechatRealQrcodeUrl == null ? "" : wechatRealQrcodeUrl;


        }

        public void setWechatRealQrcodeUrl(String wechatRealQrcodeUrl) {
            this.wechatRealQrcodeUrl = wechatRealQrcodeUrl;
        }

        public String getCreateTime() {
            return createTime == null ? "" : createTime;


        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUpdateTime() {
            return updateTime == null ? "" : updateTime;


        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public String getPageSize() {
            return pageSize == null ? "" : pageSize;


        }

        public void setPageSize(String pageSize) {
            this.pageSize = pageSize;
        }

        public String getCurrentPage() {
            return currentPage == null ? "" : currentPage;


        }

        public void setCurrentPage(String currentPage) {
            this.currentPage = currentPage;
        }

        public String getServiceProviderId() {
            return serviceProviderId == null ? "" : serviceProviderId;


        }

        public void setServiceProviderId(String serviceProviderId) {
            this.serviceProviderId = serviceProviderId;
        }

        public String getServiceChannelId() {
            return serviceChannelId == null ? "" : serviceChannelId;


        }

        public void setServiceChannelId(String serviceChannelId) {
            this.serviceChannelId = serviceChannelId;
        }

        public String getThirdChannelId() {
            return thirdChannelId == null ? "" : thirdChannelId;


        }

        public void setThirdChannelId(String thirdChannelId) {
            this.thirdChannelId = thirdChannelId;
        }

        public String getPrincipal() {
            return principal == null ? "" : principal;


        }

        public void setPrincipal(String principal) {
            this.principal = principal;
        }

        public String getTelephone() {
            return telephone == null ? "" : telephone;


        }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        public String getAccountCode() {
            return accountCode == null ? "" : accountCode;


        }

        public void setAccountCode(String accountCode) {
            this.accountCode = accountCode;
        }

        public String getFailDesc() {
            return failDesc == null ? "" : failDesc;


        }

        public void setFailDesc(String failDesc) {
            this.failDesc = failDesc;
        }

        public String getRegType() {
            return regType == null ? "" : regType;


        }

        public void setRegType(String regType) {
            this.regType = regType;
        }

        public String getRegTypeCnt() {
            return regTypeCnt == null ? "" : regTypeCnt;


        }

        public void setRegTypeCnt(String regTypeCnt) {
            this.regTypeCnt = regTypeCnt;
        }

        public String getTryNum() {
            return tryNum == null ? "" : tryNum;


        }

        public void setTryNum(String tryNum) {
            this.tryNum = tryNum;
        }

        public String getPayCenterId() {
            return payCenterId == null ? "" : payCenterId;


        }

        public void setPayCenterId(String payCenterId) {
            this.payCenterId = payCenterId;
        }

        public boolean isPayTypeEnable() {
            return payTypeEnable;


        }

        public void setPayTypeEnable(boolean payTypeEnable) {
            this.payTypeEnable = payTypeEnable;
        }

        public String getMerchantName() {
            return merchantName == null ? "" : merchantName;


        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }
    }
}
