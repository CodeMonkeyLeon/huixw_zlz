package com.hstypay.hstysales.bean;

import android.print.PageRange;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/7/31 14:59
 * @描述: ${TODO}
 */

public class MerchantInfoBean implements Serializable {
    /**
     * data : {"industryName":"综合商城(弃用)","address":"广东省深圳市","city":"100500","idCode":"431321198801010203","county":"100504","orgPhoto":"/pic/mch/2017/07/29/17196fd1-596e-4399-9e93-00adae6409c1.jpg","licenseCode":"45684165214","examineStatus":1,"merchantName":"支付接口联调测试数据_商户","principal":"刘德华","companyPhoto":"/pic/mch/2017/07/29/523c422b-2b98-4a2a-9265-c7a6e3f3baf7.gif","industryId":110,"idCardPhotos":"/pic/mch/2017/07/29/1a837fc2-930b-4818-be42-2e5febd5826c.jpg,/pic/mch/2017/07/29/858b9298-5052-4e45-81f9-1f47188995df.gif","province":"100000","cityName":"苏州","licensePhoto":"/pic/mch/2017/07/29/5a9f4166-685a-42f8-b7c9-9edee469150e.png","merchantId":"683200000001","telphone":"18666076073","examineRemark":"商户门头照片与营业执照名称出入太大;商户业务模式不清晰;","provinceName":"江苏","email":"fei.wang@swiftpass.cn","countyName":"虎丘区"}
     * logId : azaKm5yD
     * status : true
     */
    private ErrorBean error;
    /**
     * logId : szKzGCoc
     * status : true
     * data : {"merchantId":"1020001897","merchantName":"谢国通测试商户小店","examineStatus":3,"examineStatusCnt":"修改待审核","mchQueryStatus":3,"merchantClass":1,"outMchType":2,"outMchTypeCnt":"个体工商户","contractStatus":0,"remark":"分手大师","mchDetail":{"merchantId":"1020001897","merchantShortName":"谢国通测试商户小店","industryId":1001,"industryName":"食品","province":"120000","provinceName":"天津市","city":"120100","cityName":"市辖区","county":"120101","countyName":"和平区","address":"分手大师","servicePhone":"075523110501","telphone":"18825202209","email":"841479557@qq.com","licenseCode":"13123232234324","licensePhoto":"/pic/mch/2020/07/05/4e338b4a-05a7-4ed1-bc33-3ed4b837a1fc.jpg","licenseExpiredBegin":"2020-07-07 00:00:00","licenseExpiredFlag":true,"handIdcardPhoto":"","bkLicensePhoto":"","bussinessPlacePhoto":"","bussinessPlacePhoto2":"","supplementPhotos":"","idCardPhotos":"/pic/mch/2020/07/05/da989ccd-9b69-4988-8103-b77c8dbaba26.jpg,/pic/mch/2020/07/05/93881a7e-4807-4e7c-b3b3-e3e04b3b7269.jpg","orgPhoto":"","companyPhoto":"/pic/mch/2020/07/05/f8826df1-713f-4c83-9cba-e41c7bf74c49.jpg","bkCardPhoto":"/pic/mch/2020/07/05/92f86e36-22b5-4450-a421-ba70b911e383.jpg","thirdAuthPhoto":"","principal":"谢家盛","idCodeType":1,"idCode":"360781199005184718","idCodeValidFlag":false,"idCodeValidBegin":"2020-07-05 00:00:00","idCodeValidEnd":"2030-07-05 00:00:00","contactPersonName":"谢家盛","idCard":"360781199005184718","idCardType":1,"idCardValidFlag":true,"idCardValidBegin":"2020-07-05 00:00:00"},"bankAccount":{"accountType":2,"accountName":"谢家盛","idCardType":1,"idCard":"360781199005184718","accountCode":"6222980017823677","accountExpiredDate":"2030-07-05 00:00:00","accountExpiredBegin":"2020-07-05 00:00:00","accountExpiredFlag":false,"bankId":1,"bankIdCnt":"中国工商银行","contactLine":"102584002539","province":"440000","provinceCnt":"广东省","city":"440300","cityCnt":"深圳市","bankBranchId":169186,"bankName":"中国工商银行深圳车公庙支行","address":"fffffff"},"mchUnincorporatedSettlement":0,"mchUnincorporatedSettlementCnt":"不支持"}
     */

    private String logId;
    private boolean status;
    private DataBean data;

    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBean implements Serializable{
        /**
         * merchantId : 1020001897
         * merchantName : 谢国通测试商户小店
         * examineStatus : 3
         * examineStatusCnt : 修改待审核
         * mchQueryStatus : 3
         * merchantClass : 1
         * outMchType : 2
         * outMchTypeCnt : 个体工商户
         * contractStatus : 0
         * remark : 分手大师
         * mchDetail : {"merchantId":"1020001897","merchantShortName":"谢国通测试商户小店","industryId":1001,"industryName":"食品","province":"120000","provinceName":"天津市","city":"120100","cityName":"市辖区","county":"120101","countyName":"和平区","address":"分手大师","servicePhone":"075523110501","telphone":"18825202209","email":"841479557@qq.com","licenseCode":"13123232234324","licensePhoto":"/pic/mch/2020/07/05/4e338b4a-05a7-4ed1-bc33-3ed4b837a1fc.jpg","licenseExpiredBegin":"2020-07-07 00:00:00","licenseExpiredFlag":true,"handIdcardPhoto":"","bkLicensePhoto":"","bussinessPlacePhoto":"","bussinessPlacePhoto2":"","supplementPhotos":"","idCardPhotos":"/pic/mch/2020/07/05/da989ccd-9b69-4988-8103-b77c8dbaba26.jpg,/pic/mch/2020/07/05/93881a7e-4807-4e7c-b3b3-e3e04b3b7269.jpg","orgPhoto":"","companyPhoto":"/pic/mch/2020/07/05/f8826df1-713f-4c83-9cba-e41c7bf74c49.jpg","bkCardPhoto":"/pic/mch/2020/07/05/92f86e36-22b5-4450-a421-ba70b911e383.jpg","thirdAuthPhoto":"","principal":"谢家盛","idCodeType":1,"idCode":"360781199005184718","idCodeValidFlag":false,"idCodeValidBegin":"2020-07-05 00:00:00","idCodeValidEnd":"2030-07-05 00:00:00","contactPersonName":"谢家盛","idCard":"360781199005184718","idCardType":1,"idCardValidFlag":true,"idCardValidBegin":"2020-07-05 00:00:00"}
         * bankAccount : {"accountType":2,"accountName":"谢家盛","idCardType":1,"idCard":"360781199005184718","accountCode":"6222980017823677","accountExpiredDate":"2030-07-05 00:00:00","accountExpiredBegin":"2020-07-05 00:00:00","accountExpiredFlag":false,"bankId":1,"bankIdCnt":"中国工商银行","contactLine":"102584002539","province":"440000","provinceCnt":"广东省","city":"440300","cityCnt":"深圳市","bankBranchId":169186,"bankName":"中国工商银行深圳车公庙支行","address":"fffffff"}
         * mchUnincorporatedSettlement : 0
         * mchUnincorporatedSettlementCnt : 不支持
         */

        private String merchantId;
        private String merchantName;
        private int examineStatus;
        private String examineStatusCnt;
        private int mchQueryStatus;//审核状态：-1:初始商户,0:审核中,1:审核通过,2:审核不通过,3:变更审核中,4:修改审核不通过,5:作废商户,6 待确认商户,10:变更审核通过,100:可交易,101:冻结商户
        private int merchantClass;
        private int outMchType;
        private String outMchTypeCnt;
        private int contractStatus;
        private String remark;
        private MchDetailBean detail;
        private BankAccountBean account;
        private int mchUnincorporatedSettlement;
        private String mchUnincorporatedSettlementCnt;
        private String examineRemark;
        private String dataBeforeJson;
        private String dataAfterJson;
        private int tradeStatus;//0：不可交易  10：可交易  20：部分可交易
        private String updateVersion;
        private Integer wxRealNameStatus; // 微信实名认证状态,null-未进行微信实名认证, 0-初始, 10-签约成功,20-签约失败,30-待签约
        private String wxRealNameStatusCnt;//微信实名认证状态中文
        private Integer aliRealNameStatus; // 支付宝实名认证状态,null-未进行微信实名认证, 0-初始, 10-签约成功,20-签约失败,30-待签约
        private String aliRealNameStatusCnt;//支付宝名认证状态中文
        //新增字段三方商户号 thirdMchId
        private String thirdMchId;

        public String getThirdMchId() {
            return thirdMchId;
        }

        public void setThirdMchId(String thirdMchId) {
            this.thirdMchId = thirdMchId;
        }

        public Integer getAliRealNameStatus() {
            return aliRealNameStatus;
        }

        public void setAliRealNameStatus(Integer aliRealNameStatus) {
            this.aliRealNameStatus = aliRealNameStatus;
        }

        public String getAliRealNameStatusCnt() {
            return aliRealNameStatusCnt;
        }

        public void setAliRealNameStatusCnt(String aliRealNameStatusCnt) {
            this.aliRealNameStatusCnt = aliRealNameStatusCnt;
        }

        public String getWxRealNameStatusCnt() {
            return wxRealNameStatusCnt;
        }

        public void setWxRealNameStatusCnt(String wxRealNameStatusCnt) {
            this.wxRealNameStatusCnt = wxRealNameStatusCnt;
        }

        public Integer getWxRealNameStatus() {
            return wxRealNameStatus;
        }

        public void setWxRealNameStatus(Integer wxRealNameStatus) {
            this.wxRealNameStatus = wxRealNameStatus;
        }

        public int getTradeStatus() {
            return tradeStatus;
        }

        public void setTradeStatus(int tradeStatus) {
            this.tradeStatus = tradeStatus;
        }

        public String getDataBeforeJson() {
            return dataBeforeJson;
        }

        public void setDataBeforeJson(String dataBeforeJson) {
            this.dataBeforeJson = dataBeforeJson;
        }

        public String getDataAfterJson() {
            return dataAfterJson;
        }

        public void setDataAfterJson(String dataAfterJson) {
            this.dataAfterJson = dataAfterJson;
        }

        public String getExamineRemark() {
            return examineRemark;
        }

        public void setExamineRemark(String examineRemark) {
            this.examineRemark = examineRemark;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getMerchantName() {
            return merchantName;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public int getExamineStatus() {
            return examineStatus;
        }

        public void setExamineStatus(int examineStatus) {
            this.examineStatus = examineStatus;
        }

        public String getExamineStatusCnt() {
            return examineStatusCnt;
        }

        public void setExamineStatusCnt(String examineStatusCnt) {
            this.examineStatusCnt = examineStatusCnt;
        }

        public int getMchQueryStatus() {
            return mchQueryStatus;
        }

        public void setMchQueryStatus(int mchQueryStatus) {
            this.mchQueryStatus = mchQueryStatus;
        }

        public int getMerchantClass() {
            return merchantClass;
        }

        public void setMerchantClass(int merchantClass) {
            this.merchantClass = merchantClass;
        }

        public int getOutMchType() {
            return outMchType;
        }

        public void setOutMchType(int outMchType) {
            this.outMchType = outMchType;
        }

        public String getOutMchTypeCnt() {
            return outMchTypeCnt;
        }

        public void setOutMchTypeCnt(String outMchTypeCnt) {
            this.outMchTypeCnt = outMchTypeCnt;
        }

        public int getContractStatus() {
            return contractStatus;
        }

        public void setContractStatus(int contractStatus) {
            this.contractStatus = contractStatus;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public MchDetailBean getMchDetail() {
            return detail;
        }

        public void setMchDetail(MchDetailBean mchDetail) {
            this.detail = mchDetail;
        }

        public BankAccountBean getBankAccount() {
            return account;
        }

        public void setBankAccount(BankAccountBean bankAccount) {
            this.account = bankAccount;
        }

        public int getMchUnincorporatedSettlement() {
            return mchUnincorporatedSettlement;
        }

        public void setMchUnincorporatedSettlement(int mchUnincorporatedSettlement) {
            this.mchUnincorporatedSettlement = mchUnincorporatedSettlement;
        }

        public String getMchUnincorporatedSettlementCnt() {
            return mchUnincorporatedSettlementCnt;
        }

        public void setMchUnincorporatedSettlementCnt(String mchUnincorporatedSettlementCnt) {
            this.mchUnincorporatedSettlementCnt = mchUnincorporatedSettlementCnt;
        }

        public String getUpdateVersion() {
            return updateVersion;
        }

        public void setUpdateVersion(String updateVersion) {
            this.updateVersion = updateVersion;
        }

        public static class MchDetailBean implements Serializable{
            /**
             * merchantId : 1020001897
             * merchantShortName : 谢国通测试商户小店
             * industryId : 1001
             * industryName : 食品
             * province : 120000
             * provinceName : 天津市
             * city : 120100
             * cityName : 市辖区
             * county : 120101
             * countyName : 和平区
             * address : 分手大师
             * servicePhone : 075523110501
             * telphone : 18825202209
             * email : 841479557@qq.com
             * licenseCode : 13123232234324
             * licensePhoto : /pic/mch/2020/07/05/4e338b4a-05a7-4ed1-bc33-3ed4b837a1fc.jpg
             * licenseExpiredBegin : 2020-07-07 00:00:00
             * licenseExpiredFlag : true
             * handIdcardPhoto :
             * bkLicensePhoto :
             * bussinessPlacePhoto :
             * bussinessPlacePhoto2 :
             * supplementPhotos :
             * idCardPhotos : /pic/mch/2020/07/05/da989ccd-9b69-4988-8103-b77c8dbaba26.jpg,/pic/mch/2020/07/05/93881a7e-4807-4e7c-b3b3-e3e04b3b7269.jpg
             * orgPhoto :
             * companyPhoto : /pic/mch/2020/07/05/f8826df1-713f-4c83-9cba-e41c7bf74c49.jpg
             * bkCardPhoto : /pic/mch/2020/07/05/92f86e36-22b5-4450-a421-ba70b911e383.jpg
             * thirdAuthPhoto :
             * principal : 谢家盛
             * idCodeType : 1
             * idCode : 360781199005184718
             * idCodeValidFlag : false
             * idCodeValidBegin : 2020-07-05 00:00:00
             * idCodeValidEnd : 2030-07-05 00:00:00
             * contactPersonName : 谢家盛
             * idCard : 360781199005184718
             * idCardType : 1
             * idCardValidFlag : true
             * idCardValidBegin : 2020-07-05 00:00:00
             */

            private String merchantId;
            private String merchantShortName;
            private int industryId;
            private String industryName;
            private String province;
            private String provinceCnt;
            private String city;
            private String cityCnt;
            private String county;
            private String countyCnt;
            private String address;
            private String servicePhone;
            private String telphone;
            private String email;
            private String licenseCode;
            private String licensePhoto;
            private String licenseExpiredBegin;
            private boolean licenseExpiredFlag;
            private String handIdcardPhoto;
            private String bkLicensePhoto;
            private String bussinessPlacePhoto;
            private String bussinessPlacePhoto2;
            private String supplementPhotos;
            private String idCardPhotos;
            private String orgPhoto;
            private String companyPhoto;
            private String bkCardPhoto;
            private String thirdAuthPhoto;
            private String principal;
            private int idCodeType;
            private String idCode;
            private boolean idCodeValidFlag;
            private String idCodeValidBegin;
            private String idCodeValidEnd;
            private String contactPersonName;
            private String idCard;
            private int idCardType;
            private boolean idCardValidFlag;
            private String idCardValidBegin;
            private String licenseExpiredDate;
            private String idCardValidEnd;
            private String accountChangePhoto;
            private String contactHandCertPhoto;
            private String contactCertPhoto;
            private String otherPhoto;
            private String bkCardPhotoBack;
            /**单位证明函*/
            private String companyProve;
            /**商户协议照*/
            private String protocolPhoto;
            /**收银台照*/
            private String cashRegisterPhoto;
            /** 法人类型不是身份证时的证件照正反面*/
            private String certificatePhoto;
            /** 联系人手机号*/
            private String contactTelephone;
            /** 主营业务*/
            private String businScope;
            /** 注册资本*/
            private String capital;

            public String getBusinScope() {
                return businScope;
            }

            public void setBusinScope(String businScope) {
                this.businScope = businScope;
            }

            public String getCapital() {
                return capital;
            }

            public void setCapital(String capital) {
                this.capital = capital;
            }

            public String getContactTelephone() {
                return contactTelephone;
            }

            public void setContactTelephone(String contactTelephone) {
                this.contactTelephone = contactTelephone;
            }

            public String getCompanyProve() {
                return companyProve;
            }

            public void setCompanyProve(String companyProve) {
                this.companyProve = companyProve;
            }

            public String getProtocolPhoto() {
                return protocolPhoto;
            }

            public void setProtocolPhoto(String protocolPhoto) {
                this.protocolPhoto = protocolPhoto;
            }

            public String getCashRegisterPhoto() {
                return cashRegisterPhoto;
            }

            public void setCashRegisterPhoto(String cashRegisterPhoto) {
                this.cashRegisterPhoto = cashRegisterPhoto;
            }

            public String getCertificatePhoto() {
                return certificatePhoto;
            }

            public void setCertificatePhoto(String certificatePhoto) {
                this.certificatePhoto = certificatePhoto;
            }

            public String getBkCardPhotoBack() {
                return bkCardPhotoBack;
            }

            public void setBkCardPhotoBack(String bkCardPhotoBack) {
                this.bkCardPhotoBack = bkCardPhotoBack;
            }

            public String getOtherPhoto() {
                return otherPhoto;
            }

            public void setOtherPhoto(String otherPhoto) {
                this.otherPhoto = otherPhoto;
            }

            public String getContactCertPhoto() {
                return contactCertPhoto;
            }

            public void setContactCertPhoto(String contactCertPhoto) {
                this.contactCertPhoto = contactCertPhoto;
            }

            public String getContactHandCertPhoto() {
                return contactHandCertPhoto;
            }

            public void setContactHandCertPhoto(String contactHandCertPhoto) {
                this.contactHandCertPhoto = contactHandCertPhoto;
            }

            public String getAccountChangePhoto() {
                return accountChangePhoto;
            }

            public void setAccountChangePhoto(String accountChangePhoto) {
                this.accountChangePhoto = accountChangePhoto;
            }

            public String getIdCardValidEnd() {
                return idCardValidEnd;
            }

            public void setIdCardValidEnd(String idCardValidEnd) {
                this.idCardValidEnd = idCardValidEnd;
            }

            public String getLicenseExpiredDate() {
                return licenseExpiredDate;
            }

            public void setLicenseExpiredDate(String licenseExpiredDate) {
                this.licenseExpiredDate = licenseExpiredDate;
            }

            public String getMerchantId() {
                return merchantId;
            }

            public void setMerchantId(String merchantId) {
                this.merchantId = merchantId;
            }

            public String getMerchantShortName() {
                return merchantShortName;
            }

            public void setMerchantShortName(String merchantShortName) {
                this.merchantShortName = merchantShortName;
            }

            public int getIndustryId() {
                return industryId;
            }

            public void setIndustryId(int industryId) {
                this.industryId = industryId;
            }

            public String getIndustryName() {
                return industryName;
            }

            public void setIndustryName(String industryName) {
                this.industryName = industryName;
            }

            public String getProvince() {
                return province;
            }

            public void setProvince(String province) {
                this.province = province;
            }

            public String getProvinceName() {
                return provinceCnt;
            }

            public void setProvinceName(String provinceName) {
                this.provinceCnt = provinceName;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCityName() {
                return cityCnt;
            }

            public void setCityName(String cityName) {
                this.cityCnt = cityName;
            }

            public String getCounty() {
                return county;
            }

            public void setCounty(String county) {
                this.county = county;
            }

            public String getCountyName() {
                return countyCnt;
            }

            public void setCountyName(String countyName) {
                this.countyCnt = countyName;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getServicePhone() {
                return servicePhone;
            }

            public void setServicePhone(String servicePhone) {
                this.servicePhone = servicePhone;
            }

            public String getTelphone() {
                return telphone;
            }

            public void setTelphone(String telphone) {
                this.telphone = telphone;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getLicenseCode() {
                return licenseCode;
            }

            public void setLicenseCode(String licenseCode) {
                this.licenseCode = licenseCode;
            }

            public String getLicensePhoto() {
                return licensePhoto;
            }

            public void setLicensePhoto(String licensePhoto) {
                this.licensePhoto = licensePhoto;
            }

            public String getLicenseExpiredBegin() {
                return licenseExpiredBegin;
            }

            public void setLicenseExpiredBegin(String licenseExpiredBegin) {
                this.licenseExpiredBegin = licenseExpiredBegin;
            }

            public boolean isLicenseExpiredFlag() {
                return licenseExpiredFlag;
            }

            public void setLicenseExpiredFlag(boolean licenseExpiredFlag) {
                this.licenseExpiredFlag = licenseExpiredFlag;
            }

            public String getHandIdcardPhoto() {
                return handIdcardPhoto;
            }

            public void setHandIdcardPhoto(String handIdcardPhoto) {
                this.handIdcardPhoto = handIdcardPhoto;
            }

            public String getBkLicensePhoto() {
                return bkLicensePhoto;
            }

            public void setBkLicensePhoto(String bkLicensePhoto) {
                this.bkLicensePhoto = bkLicensePhoto;
            }

            public String getBussinessPlacePhoto() {
                return bussinessPlacePhoto;
            }

            public void setBussinessPlacePhoto(String bussinessPlacePhoto) {
                this.bussinessPlacePhoto = bussinessPlacePhoto;
            }

            public String getBussinessPlacePhoto2() {
                return bussinessPlacePhoto2;
            }

            public void setBussinessPlacePhoto2(String bussinessPlacePhoto2) {
                this.bussinessPlacePhoto2 = bussinessPlacePhoto2;
            }

            public String getSupplementPhotos() {
                return supplementPhotos;
            }

            public void setSupplementPhotos(String supplementPhotos) {
                this.supplementPhotos = supplementPhotos;
            }

            public String getIdCardPhotos() {
                return idCardPhotos;
            }

            public void setIdCardPhotos(String idCardPhotos) {
                this.idCardPhotos = idCardPhotos;
            }

            public String getOrgPhoto() {
                return orgPhoto;
            }

            public void setOrgPhoto(String orgPhoto) {
                this.orgPhoto = orgPhoto;
            }

            public String getCompanyPhoto() {
                return companyPhoto;
            }

            public void setCompanyPhoto(String companyPhoto) {
                this.companyPhoto = companyPhoto;
            }

            public String getBkCardPhoto() {
                return bkCardPhoto;
            }

            public void setBkCardPhoto(String bkCardPhoto) {
                this.bkCardPhoto = bkCardPhoto;
            }

            public String getThirdAuthPhoto() {
                return thirdAuthPhoto;
            }

            public void setThirdAuthPhoto(String thirdAuthPhoto) {
                this.thirdAuthPhoto = thirdAuthPhoto;
            }

            public String getPrincipal() {
                return principal;
            }

            public void setPrincipal(String principal) {
                this.principal = principal;
            }

            public int getIdCodeType() {
                return idCodeType;
            }

            public void setIdCodeType(int idCodeType) {
                this.idCodeType = idCodeType;
            }

            public String getIdCode() {
                return idCode;
            }

            public void setIdCode(String idCode) {
                this.idCode = idCode;
            }

            public boolean isIdCodeValidFlag() {
                return idCodeValidFlag;
            }

            public void setIdCodeValidFlag(boolean idCodeValidFlag) {
                this.idCodeValidFlag = idCodeValidFlag;
            }

            public String getIdCodeValidBegin() {
                return idCodeValidBegin;
            }

            public void setIdCodeValidBegin(String idCodeValidBegin) {
                this.idCodeValidBegin = idCodeValidBegin;
            }

            public String getIdCodeValidEnd() {
                return idCodeValidEnd;
            }

            public void setIdCodeValidEnd(String idCodeValidEnd) {
                this.idCodeValidEnd = idCodeValidEnd;
            }

            public String getContactPersonName() {
                return contactPersonName;
            }

            public void setContactPersonName(String contactPersonName) {
                this.contactPersonName = contactPersonName;
            }

            public String getIdCard() {
                return idCard;
            }

            public void setIdCard(String idCard) {
                this.idCard = idCard;
            }

            public int getIdCardType() {
                return idCardType;
            }

            public void setIdCardType(int idCardType) {
                this.idCardType = idCardType;
            }

            public boolean isIdCardValidFlag() {
                return idCardValidFlag;
            }

            public void setIdCardValidFlag(boolean idCardValidFlag) {
                this.idCardValidFlag = idCardValidFlag;
            }

            public String getIdCardValidBegin() {
                return idCardValidBegin;
            }

            public void setIdCardValidBegin(String idCardValidBegin) {
                this.idCardValidBegin = idCardValidBegin;
            }
        }

        public static class BankAccountBean implements Serializable {
            /**
             * accountType : 2
             * accountName : 谢家盛
             * idCardType : 1
             * idCard : 360781199005184718
             * accountCode : 6222980017823677
             * accountExpiredDate : 2030-07-05 00:00:00
             * accountExpiredBegin : 2020-07-05 00:00:00
             * accountExpiredFlag : false
             * bankId : 1
             * bankIdCnt : 中国工商银行
             * contactLine : 102584002539
             * province : 440000
             * provinceCnt : 广东省
             * city : 440300
             * cityCnt : 深圳市
             * bankBranchId : 169186
             * bankName : 中国工商银行深圳车公庙支行
             * address : fffffff
             */

            private int accountType;
            private String accountName;
            private int idCardType;
            private String idCard;
            private String accountCode;
            private String accountExpiredDate;
            private String accountExpiredBegin;
            private boolean accountExpiredFlag;
            private int bankId;
            private String bankIdCnt;
            private String contactLine;
            private String province;
            private String provinceCnt;
            private String city;
            private String cityCnt;
            private int bankBranchId;
            private String bankName;
            private String address;
            private String tel;
            //新增 productMark字段    可能为空
            private String productMark; //橙意宝标记（0：不开通，1：橙意宝，2：橙意宝（畅收版））

            public String getProductMark() {
                return productMark;
            }

            public void setProductMark(String productMark) {
                this.productMark = productMark;
            }

            public String getTel() {
                return tel;
            }

            public void setTel(String tel) {
                this.tel = tel;
            }

            public int getAccountType() {
                return accountType;
            }

            public void setAccountType(int accountType) {
                this.accountType = accountType;
            }

            public String getAccountName() {
                return accountName;
            }

            public void setAccountName(String accountName) {
                this.accountName = accountName;
            }

            public int getIdCardType() {
                return idCardType;
            }

            public void setIdCardType(int idCardType) {
                this.idCardType = idCardType;
            }

            public String getIdCard() {
                return idCard;
            }

            public void setIdCard(String idCard) {
                this.idCard = idCard;
            }

            public String getAccountCode() {
                return accountCode;
            }

            public void setAccountCode(String accountCode) {
                this.accountCode = accountCode;
            }

            public String getAccountExpiredDate() {
                return accountExpiredDate;
            }

            public void setAccountExpiredDate(String accountExpiredDate) {
                this.accountExpiredDate = accountExpiredDate;
            }

            public String getAccountExpiredBegin() {
                return accountExpiredBegin;
            }

            public void setAccountExpiredBegin(String accountExpiredBegin) {
                this.accountExpiredBegin = accountExpiredBegin;
            }

            public boolean isAccountExpiredFlag() {
                return accountExpiredFlag;
            }

            public void setAccountExpiredFlag(boolean accountExpiredFlag) {
                this.accountExpiredFlag = accountExpiredFlag;
            }

            public int getBankId() {
                return bankId;
            }

            public void setBankId(int bankId) {
                this.bankId = bankId;
            }

            public String getBankIdCnt() {
                return bankIdCnt;
            }

            public void setBankIdCnt(String bankIdCnt) {
                this.bankIdCnt = bankIdCnt;
            }

            public String getContactLine() {
                return contactLine;
            }

            public void setContactLine(String contactLine) {
                this.contactLine = contactLine;
            }

            public String getProvince() {
                return province;
            }

            public void setProvince(String province) {
                this.province = province;
            }

            public String getProvinceCnt() {
                return provinceCnt;
            }

            public void setProvinceCnt(String provinceCnt) {
                this.provinceCnt = provinceCnt;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCityCnt() {
                return cityCnt;
            }

            public void setCityCnt(String cityCnt) {
                this.cityCnt = cityCnt;
            }

            public int getBankBranchId() {
                return bankBranchId;
            }

            public void setBankBranchId(int bankBranchId) {
                this.bankBranchId = bankBranchId;
            }

            public String getBankName() {
                return bankName;
            }

            public void setBankName(String bankName) {
                this.bankName = bankName;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }
        }
    }

   /* private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity implements Serializable {
        *//**
         * industryName : 综合商城(弃用)
         * address : 广东省深圳市
         * city : 100500
         * idCode : 431321198801010203
         * county : 100504
         * orgPhoto : /pic/mch/2017/07/29/17196fd1-596e-4399-9e93-00adae6409c1.jpg
         * licenseCode : 45684165214
         * examineStatus : 1
         * merchantName : 支付接口联调测试数据_商户
         * principal : 刘德华
         * companyPhoto : /pic/mch/2017/07/29/523c422b-2b98-4a2a-9265-c7a6e3f3baf7.gif
         * industryId : 110
         * idCardPhotos : /pic/mch/2017/07/29/1a837fc2-930b-4818-be42-2e5febd5826c.jpg,/pic/mch/2017/07/29/858b9298-5052-4e45-81f9-1f47188995df.gif
         * province : 100000
         * cityName : 苏州
         * licensePhoto : /pic/mch/2017/07/29/5a9f4166-685a-42f8-b7c9-9edee469150e.png
         * merchantId : 683200000001
         * telphone : 18666076073
         * examineRemark : 商户门头照片与营业执照名称出入太大;商户业务模式不清晰;
         * provinceName : 江苏
         * email : fei.wang@swiftpass.cn
         * countyName : 虎丘区
         *//*
        private String industryName;
        private String address;
        private String city;
        private String idCode;
        private String county;
        private String orgPhoto;
        private String licenseCode;
        private int examineStatus;
        private String merchantName;

        private String merchantShortName;
        private String principal;
        private String companyPhoto;
        private long industryId;
        private String idCardPhotos;
        private String province;
        private String cityName;
        private String licensePhoto;
        private String merchantId;
        private String telphone;
        private String examineRemark;
        private String provinceName;
        private String email;
        private String countyName;
        private String servicePhone;

        private String bkLicensePhoto;//开户许可证
        private String bkCardPhoto;//银行卡正面照
        private String handIdcardPhoto;//手持身份证照

        private String examineStatusCnt;
        private int activateStatus;
        private String activateRemark;
        private int merchantClass;//1 企业商户 2 个人商户
        private int accountType;//1 对公 2 对私

        private String accountCode;
        private long bankId;
        private long bankBranchId;
        private String bprovince;
        private String bcity;
        private String contactLine;

        private String accountName;
        private String idCard;
        private String bankIdCnt;
        private String bprovinceCnt;
        private String bcityCnt;
        private String bankBranchIdCnt;
        private String remark;

        private int mchMemeberStatus;//0:不显示，1：没有开通，2：审核中(汇商),3：审核中(世明),4：审核失败，5:审核成功

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public int getMchMemeberStatus() {
            return mchMemeberStatus;
        }

        public void setMchMemeberStatus(int mchMemeberStatus) {
            this.mchMemeberStatus = mchMemeberStatus;
        }

        public String getAccountCode() {
            return accountCode;
        }

        public void setAccountCode(String accountCode) {
            this.accountCode = accountCode;
        }

        public long getBankId() {
            return bankId;
        }

        public void setBankId(long bankId) {
            this.bankId = bankId;
        }

        public long getBankBranchId() {
            return bankBranchId;
        }

        public void setBankBranchId(long bankBranchId) {
            this.bankBranchId = bankBranchId;
        }

        public String getBprovince() {
            return bprovince;
        }

        public void setBprovince(String bprovince) {
            this.bprovince = bprovince;
        }

        public String getBcity() {
            return bcity;
        }

        public void setBcity(String bcity) {
            this.bcity = bcity;
        }

        public String getContactLine() {
            return contactLine;
        }

        public void setContactLine(String contactLine) {
            this.contactLine = contactLine;
        }

        public String getAccountName() {
            return accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }

        public String getIdCard() {
            return idCard;
        }

        public void setIdCard(String idCard) {
            this.idCard = idCard;
        }

        public String getBankIdCnt() {
            return bankIdCnt;
        }

        public void setBankIdCnt(String bankIdCnt) {
            this.bankIdCnt = bankIdCnt;
        }

        public String getBprovinceCnt() {
            return bprovinceCnt;
        }

        public void setBprovinceCnt(String bprovinceCnt) {
            this.bprovinceCnt = bprovinceCnt;
        }

        public String getBcityCnt() {
            return bcityCnt;
        }

        public void setBcityCnt(String bcityCnt) {
            this.bcityCnt = bcityCnt;
        }

        public String getBankBranchIdCnt() {
            return bankBranchIdCnt;
        }

        public void setBankBranchIdCnt(String bankBranchIdCnt) {
            this.bankBranchIdCnt = bankBranchIdCnt;
        }

        public int getMerchantClass() {
            return merchantClass;
        }

        public void setMerchantClass(int merchantClass) {
            this.merchantClass = merchantClass;
        }

        public int getAccountType() {
            return accountType;
        }

        public void setAccountType(int accountType) {
            this.accountType = accountType;
        }

        public String getExamineStatusCnt() {
            return examineStatusCnt;
        }

        public void setExamineStatusCnt(String examineStatusCnt) {
            this.examineStatusCnt = examineStatusCnt;
        }

        public int getActivateStatus() {
            return activateStatus;
        }

        public void setActivateStatus(int activateStatus) {
            this.activateStatus = activateStatus;
        }

        public String getActivateRemark() {
            return activateRemark;
        }

        public void setActivateRemark(String activateRemark) {
            this.activateRemark = activateRemark;
        }

        public String getBkLicensePhoto() {
            return bkLicensePhoto;
        }

        public void setBkLicensePhoto(String bkLicensePhoto) {
            this.bkLicensePhoto = bkLicensePhoto;
        }

        public String getBkCardPhoto() {
            return bkCardPhoto;
        }

        public void setBkCardPhoto(String bkCardPhoto) {
            this.bkCardPhoto = bkCardPhoto;
        }

        public String getHandIdcardPhoto() {
            return handIdcardPhoto;
        }

        public void setHandIdcardPhoto(String handIdcardPhoto) {
            this.handIdcardPhoto = handIdcardPhoto;
        }

        public String getMerchantShortName() {
            return merchantShortName;
        }

        public void setMerchantShortName(String merchantShortName) {
            this.merchantShortName = merchantShortName;
        }

        public String getServicePhone() {
            return servicePhone;
        }

        public void setServicePhone(String servicePhone) {
            this.servicePhone = servicePhone;
        }

        public void setIndustryName(String industryName) {
            this.industryName = industryName;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public void setIdCode(String idCode) {
            this.idCode = idCode;
        }

        public void setCounty(String county) {
            this.county = county;
        }

        public void setOrgPhoto(String orgPhoto) {
            this.orgPhoto = orgPhoto;
        }

        public void setLicenseCode(String licenseCode) {
            this.licenseCode = licenseCode;
        }

        public void setExamineStatus(int examineStatus) {
            this.examineStatus = examineStatus;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public void setPrincipal(String principal) {
            this.principal = principal;
        }

        public void setCompanyPhoto(String companyPhoto) {
            this.companyPhoto = companyPhoto;
        }

        public void setIndustryId(long industryId) {
            this.industryId = industryId;
        }

        public void setIdCardPhotos(String idCardPhotos) {
            this.idCardPhotos = idCardPhotos;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public void setLicensePhoto(String licensePhoto) {
            this.licensePhoto = licensePhoto;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public void setTelphone(String telphone) {
            this.telphone = telphone;
        }

        public void setExamineRemark(String examineRemark) {
            this.examineRemark = examineRemark;
        }

        public void setProvinceName(String provinceName) {
            this.provinceName = provinceName;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setCountyName(String countyName) {
            this.countyName = countyName;
        }

        public String getIndustryName() {
            return industryName;
        }

        public String getAddress() {
            return address;
        }

        public String getCity() {
            return city;
        }

        public String getIdCode() {
            return idCode;
        }

        public String getCounty() {
            return county;
        }

        public String getOrgPhoto() {
            return orgPhoto;
        }

        public String getLicenseCode() {
            return licenseCode;
        }

        public int getExamineStatus() {
            return examineStatus;
        }

        public String getMerchantName() {
            return merchantName;
        }

        public String getPrincipal() {
            return principal;
        }

        public String getCompanyPhoto() {
            return companyPhoto;
        }

        public long getIndustryId() {
            return industryId;
        }

        public String getIdCardPhotos() {
            return idCardPhotos;
        }

        public String getProvince() {
            return province;
        }

        public String getCityName() {
            return cityName;
        }

        public String getLicensePhoto() {
            return licensePhoto;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public String getTelphone() {
            return telphone;
        }

        public String getExamineRemark() {
            return examineRemark;
        }

        public String getProvinceName() {
            return provinceName;
        }

        public String getEmail() {
            return email;
        }

        public String getCountyName() {
            return countyName;
        }
    }*/








}
