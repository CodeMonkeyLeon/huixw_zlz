package com.hstypay.hstysales.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/4 17:17
 * @描述: ${TODO}
 */

public class MerchantListBean implements Serializable {
    /**
     * data : null
     * logId : 0QqVhThv
     * error : {"args":[{}],"code":"auth.username.pass.fail","message":"账号或密码不正确"}
     * status : false
     */
    private ErrorEntity error;

    public void setError(ErrorEntity error) {
        this.error = error;
    }
    public ErrorEntity getError() {
        return error;
    }
    public class ErrorEntity {
        /**
         * args : [{}]
         * code : auth.username.pass.fail
         * message : 账号或密码不正确
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"data":[{"serviceProviderId":"319100000001","city":"150300","county":"150303","createUserName":"system","examineStatus":2,"merchantName":"张丹商户","riskId":0,"principal":"张丹","province":"150000","merchantId":"942200000001","examineUserName":"超级管理员","dayTradeLimit":10000,"examineStatusCnt":"审核不通过","tradeStatusCnt":"不可交易","singleTradeLimit":200,"examineUser":1,"activateStatus":1,"merchantType":11,"countyCnt":"海南区","examineTime":"2017-12-04 14:14:38","salesmanId":17,"contractStatus":0,"address":"高新园","physicsFlag":1,"merchantClassCnt":"企业商户","provinceCnt":"内蒙古自治区","activateTime":"2017-12-04 14:21:50","updateTime":"2017-12-04 14:14:38","merchantDealType":1,"feeType":"CNY","cityCnt":"乌海市","merchantClass":1,"createTime":"2017-10-11 19:52:44","refundAuditFlag":false,"tradeStatus":0,"telphone":"18565877792","createUser":0,"examineRemark":"商户现场照片与商户经营范围不符;商户门头照片与营业执照名称出入太大;","dataSource":1,"storeGroup":0}],"totalPages":0,"pageSize":15,"totalRows":0,"currentPage":1}
     * logId : qC0nhhTx
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity {
        /**
         * data : [{"serviceProviderId":"319100000001","city":"150300","county":"150303","createUserName":"system","examineStatus":2,"merchantName":"张丹商户","riskId":0,"principal":"张丹","province":"150000","merchantId":"942200000001","examineUserName":"超级管理员","dayTradeLimit":10000,"examineStatusCnt":"审核不通过","tradeStatusCnt":"不可交易","singleTradeLimit":200,"examineUser":1,"activateStatus":1,"merchantType":11,"countyCnt":"海南区","examineTime":"2017-12-04 14:14:38","salesmanId":17,"contractStatus":0,"address":"高新园","physicsFlag":1,"merchantClassCnt":"企业商户","provinceCnt":"内蒙古自治区","activateTime":"2017-12-04 14:21:50","updateTime":"2017-12-04 14:14:38","merchantDealType":1,"feeType":"CNY","cityCnt":"乌海市","merchantClass":1,"createTime":"2017-10-11 19:52:44","refundAuditFlag":false,"tradeStatus":0,"telphone":"18565877792","createUser":0,"examineRemark":"商户现场照片与商户经营范围不符;商户门头照片与营业执照名称出入太大;","dataSource":1,"storeGroup":0}]
         * totalPages : 0
         * pageSize : 15
         * totalRows : 0
         * currentPage : 1
         */
        private List<ItemData> data;
        private int totalPages;
        private int pageSize;
        private int totalRows;
        private int currentPage;

        public void setData(List<ItemData> data) {
            this.data = data;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public List<ItemData> getData() {
            return data;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public int getPageSize() {
            return pageSize;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public static class ItemData implements Serializable{
            /**
             * serviceProviderId : 319100000001
             * city : 150300
             * county : 150303
             * createUserName : system
             * examineStatus : 2
             * merchantName : 张丹商户
             * riskId : 0
             * principal : 张丹
             * province : 150000
             * merchantId : 942200000001
             * examineUserName : 超级管理员
             * dayTradeLimit : 10000
             * examineStatusCnt : 审核不通过
             * tradeStatusCnt : 不可交易
             * singleTradeLimit : 200
             * examineUser : 1
             * activateStatus : 1
             * merchantType : 11
             * countyCnt : 海南区
             * examineTime : 2017-12-04 14:14:38
             * salesmanId : 17
             * contractStatus : 0
             * address : 高新园
             * physicsFlag : 1
             * merchantClassCnt : 企业商户
             * provinceCnt : 内蒙古自治区
             * activateTime : 2017-12-04 14:21:50
             * updateTime : 2017-12-04 14:14:38
             * merchantDealType : 1
             * feeType : CNY
             * cityCnt : 乌海市
             * merchantClass : 1
             * createTime : 2017-10-11 19:52:44
             * refundAuditFlag : false
             * tradeStatus : 0
             * telphone : 18565877792
             * createUser : 0
             * examineRemark : 商户现场照片与商户经营范围不符;商户门头照片与营业执照名称出入太大;
             * dataSource : 1
             * storeGroup : 0
             */

            private int costRate;
            private int costRateType;
            private String validStartAt;
            private String validEndAt;
            private String registeAt;
            private String userId;
            private boolean enabled;
            private int maxUseCount;
            private String invitationCode;
            private int usedCount;

            private int activateUser;
            private String activateRemark;
            private String activateUserName;
            private String serviceProviderId;
            private String city;
            private String county;
            private String createUserName;
            private int examineStatus;
            private String merchantName;
            private int riskId;
            private String principal;
            private String province;
            private String merchantId;
            private String examineUserName;
            private int dayTradeLimit;
            private String examineStatusCnt;
            private String tradeStatusCnt;
            private int singleTradeLimit;
            private int examineUser;
            private int activateStatus;
            private int merchantType;
            private String countyCnt;
            private String examineTime;
            private int salesmanId;
            private int contractStatus;
            private String address;
            private int physicsFlag;
            private String merchantClassCnt;
            private String provinceCnt;
            private String activateTime;
            private String updateTime;
            private int merchantDealType;
            private String feeType;
            private String cityCnt;
            private int merchantClass;
            private String createTime;
            private boolean refundAuditFlag;
            private int tradeStatus;
            private String telphone;
            private int createUser;
            private String examineRemark;
            private int dataSource;
            private int storeGroup;
            private Number mchQueryStatus;//查询状态：-1:初始商户,0:审核中,1:审核通过,2:审核不通过,3:变更审核中,4:修改审核不通过,5:作废商户,10:变更审核通过,100:可交易,101:冻结商户

            public Number getMchQueryStatus() {
                return mchQueryStatus;
            }

            public void setMchQueryStatus(Number mchQueryStatus) {
                this.mchQueryStatus = mchQueryStatus;
            }

            public String getActivateRemark() {
                return activateRemark;
            }

            public void setActivateRemark(String activateRemark) {
                this.activateRemark = activateRemark;
            }

            public int getCostRate() {
                return costRate;
            }

            public void setCostRate(int costRate) {
                this.costRate = costRate;
            }

            public int getCostRateType() {
                return costRateType;
            }

            public void setCostRateType(int costRateType) {
                this.costRateType = costRateType;
            }

            public String getValidStartAt() {
                return validStartAt;
            }

            public void setValidStartAt(String validStartAt) {
                this.validStartAt = validStartAt;
            }

            public String getValidEndAt() {
                return validEndAt;
            }

            public void setValidEndAt(String validEndAt) {
                this.validEndAt = validEndAt;
            }

            public String getRegisteAt() {
                return registeAt;
            }

            public void setRegisteAt(String registeAt) {
                this.registeAt = registeAt;
            }

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public int getMaxUseCount() {
                return maxUseCount;
            }

            public void setMaxUseCount(int maxUseCount) {
                this.maxUseCount = maxUseCount;
            }

            public String getInvitationCode() {
                return invitationCode;
            }

            public void setInvitationCode(String invitationCode) {
                this.invitationCode = invitationCode;
            }

            public int getUsedCount() {
                return usedCount;
            }

            public void setUsedCount(int usedCount) {
                this.usedCount = usedCount;
            }

            public int getActivateUser() {
                return activateUser;
            }

            public void setActivateUser(int activateUser) {
                this.activateUser = activateUser;
            }

            public String getActivateUserName() {
                return activateUserName;
            }

            public void setActivateUserName(String activateUserName) {
                this.activateUserName = activateUserName;
            }

            public void setServiceProviderId(String serviceProviderId) {
                this.serviceProviderId = serviceProviderId;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public void setCounty(String county) {
                this.county = county;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public void setExamineStatus(int examineStatus) {
                this.examineStatus = examineStatus;
            }

            public void setMerchantName(String merchantName) {
                this.merchantName = merchantName;
            }

            public void setRiskId(int riskId) {
                this.riskId = riskId;
            }

            public void setPrincipal(String principal) {
                this.principal = principal;
            }

            public void setProvince(String province) {
                this.province = province;
            }

            public void setMerchantId(String merchantId) {
                this.merchantId = merchantId;
            }

            public void setExamineUserName(String examineUserName) {
                this.examineUserName = examineUserName;
            }

            public void setDayTradeLimit(int dayTradeLimit) {
                this.dayTradeLimit = dayTradeLimit;
            }

            public void setExamineStatusCnt(String examineStatusCnt) {
                this.examineStatusCnt = examineStatusCnt;
            }

            public void setTradeStatusCnt(String tradeStatusCnt) {
                this.tradeStatusCnt = tradeStatusCnt;
            }

            public void setSingleTradeLimit(int singleTradeLimit) {
                this.singleTradeLimit = singleTradeLimit;
            }

            public void setExamineUser(int examineUser) {
                this.examineUser = examineUser;
            }

            public void setActivateStatus(int activateStatus) {
                this.activateStatus = activateStatus;
            }

            public void setMerchantType(int merchantType) {
                this.merchantType = merchantType;
            }

            public void setCountyCnt(String countyCnt) {
                this.countyCnt = countyCnt;
            }

            public void setExamineTime(String examineTime) {
                this.examineTime = examineTime;
            }

            public void setSalesmanId(int salesmanId) {
                this.salesmanId = salesmanId;
            }

            public void setContractStatus(int contractStatus) {
                this.contractStatus = contractStatus;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public void setPhysicsFlag(int physicsFlag) {
                this.physicsFlag = physicsFlag;
            }

            public void setMerchantClassCnt(String merchantClassCnt) {
                this.merchantClassCnt = merchantClassCnt;
            }

            public void setProvinceCnt(String provinceCnt) {
                this.provinceCnt = provinceCnt;
            }

            public void setActivateTime(String activateTime) {
                this.activateTime = activateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public void setMerchantDealType(int merchantDealType) {
                this.merchantDealType = merchantDealType;
            }

            public void setFeeType(String feeType) {
                this.feeType = feeType;
            }

            public void setCityCnt(String cityCnt) {
                this.cityCnt = cityCnt;
            }

            public void setMerchantClass(int merchantClass) {
                this.merchantClass = merchantClass;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public void setRefundAuditFlag(boolean refundAuditFlag) {
                this.refundAuditFlag = refundAuditFlag;
            }

            public void setTradeStatus(int tradeStatus) {
                this.tradeStatus = tradeStatus;
            }

            public void setTelphone(String telphone) {
                this.telphone = telphone;
            }

            public void setCreateUser(int createUser) {
                this.createUser = createUser;
            }

            public void setExamineRemark(String examineRemark) {
                this.examineRemark = examineRemark;
            }

            public void setDataSource(int dataSource) {
                this.dataSource = dataSource;
            }

            public void setStoreGroup(int storeGroup) {
                this.storeGroup = storeGroup;
            }

            public String getServiceProviderId() {
                return serviceProviderId;
            }

            public String getCity() {
                return city;
            }

            public String getCounty() {
                return county;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public int getExamineStatus() {
                return examineStatus;
            }

            public String getMerchantName() {
                return merchantName;
            }

            public int getRiskId() {
                return riskId;
            }

            public String getPrincipal() {
                return principal;
            }

            public String getProvince() {
                return province;
            }

            public String getMerchantId() {
                return merchantId;
            }

            public String getExamineUserName() {
                return examineUserName;
            }

            public int getDayTradeLimit() {
                return dayTradeLimit;
            }

            public String getExamineStatusCnt() {
                return examineStatusCnt;
            }

            public String getTradeStatusCnt() {
                return tradeStatusCnt;
            }

            public int getSingleTradeLimit() {
                return singleTradeLimit;
            }

            public int getExamineUser() {
                return examineUser;
            }

            public int getActivateStatus() {
                return activateStatus;
            }

            public int getMerchantType() {
                return merchantType;
            }

            public String getCountyCnt() {
                return countyCnt;
            }

            public String getExamineTime() {
                return examineTime;
            }

            public int getSalesmanId() {
                return salesmanId;
            }

            public int getContractStatus() {
                return contractStatus;
            }

            public String getAddress() {
                return address;
            }

            public int getPhysicsFlag() {
                return physicsFlag;
            }

            public String getMerchantClassCnt() {
                return merchantClassCnt;
            }

            public String getProvinceCnt() {
                return provinceCnt;
            }

            public String getActivateTime() {
                return activateTime;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public int getMerchantDealType() {
                return merchantDealType;
            }

            public String getFeeType() {
                return feeType;
            }

            public String getCityCnt() {
                return cityCnt;
            }

            public int getMerchantClass() {
                return merchantClass;
            }

            public String getCreateTime() {
                return createTime;
            }

            public boolean isRefundAuditFlag() {
                return refundAuditFlag;
            }

            public int getTradeStatus() {
                return tradeStatus;
            }

            public String getTelphone() {
                return telphone;
            }

            public int getCreateUser() {
                return createUser;
            }

            public String getExamineRemark() {
                return examineRemark;
            }

            public int getDataSource() {
                return dataSource;
            }

            public int getStoreGroup() {
                return storeGroup;
            }
        }
    }
}
