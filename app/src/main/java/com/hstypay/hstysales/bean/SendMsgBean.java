package com.hstypay.hstysales.bean;

import java.util.List;

public class SendMsgBean {


    /**
     * logId : IebYXjQK
     * status : false
     * error : {"code":"common.verify.code.sended","message":"短信验证码已经发送，请稍后再试","args":[]}
     * data : null
     */
    private String logId;
    private boolean status;
    private ErrorBean error;
    private Object data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : common.verify.code.sended
         * message : 短信验证码已经发送，请稍后再试
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
}
