package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/8/3 22:56
 * @描述: ${TODO}
 */

public class ServiceProviderBean {
    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * data : null
     * logId : YJWbPg3d
     * error : {"args":[],"code":"app.commons.store.qrcode.is.bind.exception","message":"门店二维码已绑定"}
     * status : false
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataEntity {
        private List<ServiceProvider> data;
        private int totalPages;
        private int pageSize;
        private int totalRows;
        private int currentPage;

        public List<ServiceProvider> getData() {
            return data;
        }

        public void setData(List<ServiceProvider> data) {
            this.data = data;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public static class ServiceProvider {
            /**
             * code : client.user.pwd.incorrect
             * message : 用户名或者密码输入不正确
             * args : []
             */

            private String serviceProviderId;
            private String serviceProviderName;

            public String getServiceProviderId() {
                return serviceProviderId;
            }

            public void setServiceProviderId(String serviceProviderId) {
                this.serviceProviderId = serviceProviderId;
            }

            public String getServiceProviderName() {
                return serviceProviderName;
            }

            public void setServiceProviderName(String serviceProviderName) {
                this.serviceProviderName = serviceProviderName;
            }
        }
    }
}
