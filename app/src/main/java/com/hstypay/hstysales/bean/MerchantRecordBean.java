package com.hstypay.hstysales.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/7/31 14:59
 * @描述: ${TODO}
 */

/*public class MerchantRecordBean implements Serializable {
    private String merchantId;
    private String merchantShortName;
    private int industryId;
    private String industryName;
    private String province;
    private String provinceName;
    private String city;
    private String cityName;
    private String county;
    private String countyName;
    private String address;
    private String servicePhone;
    private String telphone;
    private String email;
    private String licenseCode;
    private String licensePhoto;
    private String licenseExpiredBegin;
    private boolean licenseExpiredFlag;
    private String handIdcardPhoto;
    private String bkLicensePhoto;
    private String bussinessPlacePhoto;
    private String bussinessPlacePhoto2;
    private String supplementPhotos;
    private String idCardPhotos;
    private String orgPhoto;
    private String companyPhoto;
    private String bkCardPhoto;
    private String thirdAuthPhoto;
    private String principal;
    private int idCodeType;
    private String idCode;
    private boolean idCodeValidFlag;
    private String idCodeValidBegin;
    private String idCodeValidEnd;
    private String contactPersonName;
    private String idCard;
    private int idCardType;
    private boolean idCardValidFlag;
    private String idCardValidBegin;
    private String licenseExpiredDate;
    private String idCardValidEnd;
    private String accountChangePhoto;
    private String contactHandCertPhoto;
    private String contactCertPhoto;
    private String detail.otherPhoto;
    private String bkCardPhotoBack;
    private int accountType;
    private String accountName;
    private int idCardType;
    private String idCard;
    private String accountCode;
    private String accountExpiredDate;
    private String accountExpiredBegin;
    private boolean accountExpiredFlag;
    private int bankId;
    private String bankIdCnt;
    private String contactLine;
    private String province;
    private String provinceCnt;
    private String city;
    private String cityCnt;
    private int bankBranchId;
    private String bankName;
    private String address;

    public String getBkCardPhotoBack() {
        return bkCardPhotoBack;
    }

    public void setBkCardPhotoBack(String bkCardPhotoBack) {
        this.bkCardPhotoBack = bkCardPhotoBack;
    }

    public String getOtherPhoto() {
        return otherPhoto;
    }

    public void setOtherPhoto(String otherPhoto) {
        this.otherPhoto = otherPhoto;
    }

    public String getContactCertPhoto() {
        return contactCertPhoto;
    }

    public void setContactCertPhoto(String contactCertPhoto) {
        this.contactCertPhoto = contactCertPhoto;
    }

    public String getContactHandCertPhoto() {
        return contactHandCertPhoto;
    }

    public void setContactHandCertPhoto(String contactHandCertPhoto) {
        this.contactHandCertPhoto = contactHandCertPhoto;
    }

    public String getAccountChangePhoto() {
        return accountChangePhoto;
    }

    public void setAccountChangePhoto(String accountChangePhoto) {
        this.accountChangePhoto = accountChangePhoto;
    }

    public String getIdCardValidEnd() {
        return idCardValidEnd;
    }

    public void setIdCardValidEnd(String idCardValidEnd) {
        this.idCardValidEnd = idCardValidEnd;
    }

    public String getLicenseExpiredDate() {
        return licenseExpiredDate;
    }

    public void setLicenseExpiredDate(String licenseExpiredDate) {
        this.licenseExpiredDate = licenseExpiredDate;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantShortName() {
        return merchantShortName;
    }

    public void setMerchantShortName(String merchantShortName) {
        this.merchantShortName = merchantShortName;
    }

    public int getIndustryId() {
        return industryId;
    }

    public void setIndustryId(int industryId) {
        this.industryId = industryId;
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getServicePhone() {
        return servicePhone;
    }

    public void setServicePhone(String servicePhone) {
        this.servicePhone = servicePhone;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLicenseCode() {
        return licenseCode;
    }

    public void setLicenseCode(String licenseCode) {
        this.licenseCode = licenseCode;
    }

    public String getLicensePhoto() {
        return licensePhoto;
    }

    public void setLicensePhoto(String licensePhoto) {
        this.licensePhoto = licensePhoto;
    }

    public String getLicenseExpiredBegin() {
        return licenseExpiredBegin;
    }

    public void setLicenseExpiredBegin(String licenseExpiredBegin) {
        this.licenseExpiredBegin = licenseExpiredBegin;
    }

    public boolean isLicenseExpiredFlag() {
        return licenseExpiredFlag;
    }

    public void setLicenseExpiredFlag(boolean licenseExpiredFlag) {
        this.licenseExpiredFlag = licenseExpiredFlag;
    }

    public String getHandIdcardPhoto() {
        return handIdcardPhoto;
    }

    public void setHandIdcardPhoto(String handIdcardPhoto) {
        this.handIdcardPhoto = handIdcardPhoto;
    }

    public String getBkLicensePhoto() {
        return bkLicensePhoto;
    }

    public void setBkLicensePhoto(String bkLicensePhoto) {
        this.bkLicensePhoto = bkLicensePhoto;
    }

    public String getBussinessPlacePhoto() {
        return bussinessPlacePhoto;
    }

    public void setBussinessPlacePhoto(String bussinessPlacePhoto) {
        this.bussinessPlacePhoto = bussinessPlacePhoto;
    }

    public String getBussinessPlacePhoto2() {
        return bussinessPlacePhoto2;
    }

    public void setBussinessPlacePhoto2(String bussinessPlacePhoto2) {
        this.bussinessPlacePhoto2 = bussinessPlacePhoto2;
    }

    public String getSupplementPhotos() {
        return supplementPhotos;
    }

    public void setSupplementPhotos(String supplementPhotos) {
        this.supplementPhotos = supplementPhotos;
    }

    public String getIdCardPhotos() {
        return idCardPhotos;
    }

    public void setIdCardPhotos(String idCardPhotos) {
        this.idCardPhotos = idCardPhotos;
    }

    public String getOrgPhoto() {
        return orgPhoto;
    }

    public void setOrgPhoto(String orgPhoto) {
        this.orgPhoto = orgPhoto;
    }

    public String getCompanyPhoto() {
        return companyPhoto;
    }

    public void setCompanyPhoto(String companyPhoto) {
        this.companyPhoto = companyPhoto;
    }

    public String getBkCardPhoto() {
        return bkCardPhoto;
    }

    public void setBkCardPhoto(String bkCardPhoto) {
        this.bkCardPhoto = bkCardPhoto;
    }

    public String getThirdAuthPhoto() {
        return thirdAuthPhoto;
    }

    public void setThirdAuthPhoto(String thirdAuthPhoto) {
        this.thirdAuthPhoto = thirdAuthPhoto;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public int getIdCodeType() {
        return idCodeType;
    }

    public void setIdCodeType(int idCodeType) {
        this.idCodeType = idCodeType;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public boolean isIdCodeValidFlag() {
        return idCodeValidFlag;
    }

    public void setIdCodeValidFlag(boolean idCodeValidFlag) {
        this.idCodeValidFlag = idCodeValidFlag;
    }

    public String getIdCodeValidBegin() {
        return idCodeValidBegin;
    }

    public void setIdCodeValidBegin(String idCodeValidBegin) {
        this.idCodeValidBegin = idCodeValidBegin;
    }

    public String getIdCodeValidEnd() {
        return idCodeValidEnd;
    }

    public void setIdCodeValidEnd(String idCodeValidEnd) {
        this.idCodeValidEnd = idCodeValidEnd;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public int getIdCardType() {
        return idCardType;
    }

    public void setIdCardType(int idCardType) {
        this.idCardType = idCardType;
    }

    public boolean isIdCardValidFlag() {
        return idCardValidFlag;
    }

    public void setIdCardValidFlag(boolean idCardValidFlag) {
        this.idCardValidFlag = idCardValidFlag;
    }

    public String getIdCardValidBegin() {
        return idCardValidBegin;
    }

    public void setIdCardValidBegin(String idCardValidBegin) {
        this.idCardValidBegin = idCardValidBegin;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public int getIdCardType() {
        return idCardType;
    }

    public void setIdCardType(int idCardType) {
        this.idCardType = idCardType;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getAccountExpiredDate() {
        return accountExpiredDate;
    }

    public void setAccountExpiredDate(String accountExpiredDate) {
        this.accountExpiredDate = accountExpiredDate;
    }

    public String getAccountExpiredBegin() {
        return accountExpiredBegin;
    }

    public void setAccountExpiredBegin(String accountExpiredBegin) {
        this.accountExpiredBegin = accountExpiredBegin;
    }

    public boolean isAccountExpiredFlag() {
        return accountExpiredFlag;
    }

    public void setAccountExpiredFlag(boolean accountExpiredFlag) {
        this.accountExpiredFlag = accountExpiredFlag;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public String getBankIdCnt() {
        return bankIdCnt;
    }

    public void setBankIdCnt(String bankIdCnt) {
        this.bankIdCnt = bankIdCnt;
    }

    public String getContactLine() {
        return contactLine;
    }

    public void setContactLine(String contactLine) {
        this.contactLine = contactLine;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getProvinceCnt() {
        return provinceCnt;
    }

    public void setProvinceCnt(String provinceCnt) {
        this.provinceCnt = provinceCnt;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityCnt() {
        return cityCnt;
    }

    public void setCityCnt(String cityCnt) {
        this.cityCnt = cityCnt;
    }

    public int getBankBranchId() {
        return bankBranchId;
    }

    public void setBankBranchId(int bankBranchId) {
        this.bankBranchId = bankBranchId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}*/
