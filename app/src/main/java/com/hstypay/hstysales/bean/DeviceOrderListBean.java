package com.hstypay.hstysales.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/8/25
 * @desc api.vuxd.cn/repository/editor
 *      汇小旺 -> 订单管理 -> 商户订单列表
 */
public class DeviceOrderListBean implements Serializable {
    /**
     * data : null
     * logId : 0QqVhThv
     * error : {"args":[{}],"code":"auth.username.pass.fail","message":"账号或密码不正确"}
     * status : false
     */
    private ErrorEntity error;

    public void setError(ErrorEntity error) {
        this.error = error;
    }
    public ErrorEntity getError() {
        return error;
    }
    public static class ErrorEntity implements Serializable{
        /**
         * args : [{}]
         * code : auth.username.pass.fail
         * message : 账号或密码不正确
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"data":[{"serviceProviderId":"319100000001","city":"150300","county":"150303","createUserName":"system","examineStatus":2,"merchantName":"张丹商户","riskId":0,"principal":"张丹","province":"150000","merchantId":"942200000001","examineUserName":"超级管理员","dayTradeLimit":10000,"examineStatusCnt":"审核不通过","tradeStatusCnt":"不可交易","singleTradeLimit":200,"examineUser":1,"activateStatus":1,"merchantType":11,"countyCnt":"海南区","examineTime":"2017-12-04 14:14:38","salesmanId":17,"contractStatus":0,"address":"高新园","physicsFlag":1,"merchantClassCnt":"企业商户","provinceCnt":"内蒙古自治区","activateTime":"2017-12-04 14:21:50","updateTime":"2017-12-04 14:14:38","merchantDealType":1,"feeType":"CNY","cityCnt":"乌海市","merchantClass":1,"createTime":"2017-10-11 19:52:44","refundAuditFlag":false,"tradeStatus":0,"telphone":"18565877792","createUser":0,"examineRemark":"商户现场照片与商户经营范围不符;商户门头照片与营业执照名称出入太大;","dataSource":1,"storeGroup":0}],"totalPages":0,"pageSize":15,"totalRows":0,"currentPage":1}
     * logId : qC0nhhTx
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public static class DataEntity implements Serializable{

        /**
         * pageSize : 10
         * currentPage : 1
         * totalPages : 1
         * totalRows : 1
         * data : [{"orderNo":"","tradeStatus":1,"tradeStatusDesc":"待发货","tradeTime":"1","merchantId":"","merchantName":"","sendSalesmanName":"","goodsDTOList":[{"unitType":"","quantity":1}]}]
         */

        private int pageSize;
        private int currentPage;
        private int totalPages;
        private int totalRows;
        private ArrayList<DataBean> data;

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public ArrayList<DataBean> getData() {
            return data;
        }

        public void setData(ArrayList<DataBean> data) {
            this.data = data;
        }

        public static class DataBean implements Serializable{
            /**
             * orderNo :
             * tradeStatus : 1
             * tradeStatusDesc : 待发货
             * tradeTime : 1
             * merchantId :
             * merchantName :
             * sendSalesmanName :
             * goodsDTOList : [{"unitType":"","quantity":1}]
             */

            private String orderNo;
            private int tradeStatus;
            private String tradeStatusDesc;
            private String tradeTime;
            private String merchantId;
            private String merchantName;
            private String sendSalesmanName;
            private List<GoodsDTOListBean> goodsDTOList;

            public String getOrderNo() {
                return orderNo;
            }

            public void setOrderNo(String orderNo) {
                this.orderNo = orderNo;
            }

            public int getTradeStatus() {
                return tradeStatus;
            }

            public void setTradeStatus(int tradeStatus) {
                this.tradeStatus = tradeStatus;
            }

            public String getTradeStatusDesc() {
                return tradeStatusDesc;
            }

            public void setTradeStatusDesc(String tradeStatusDesc) {
                this.tradeStatusDesc = tradeStatusDesc;
            }

            public String getTradeTime() {
                return tradeTime;
            }

            public void setTradeTime(String tradeTime) {
                this.tradeTime = tradeTime;
            }

            public String getMerchantId() {
                return merchantId;
            }

            public void setMerchantId(String merchantId) {
                this.merchantId = merchantId;
            }

            public String getMerchantName() {
                return merchantName;
            }

            public void setMerchantName(String merchantName) {
                this.merchantName = merchantName;
            }

            public String getSendSalesmanName() {
                return sendSalesmanName;
            }

            public void setSendSalesmanName(String sendSalesmanName) {
                this.sendSalesmanName = sendSalesmanName;
            }

            public List<GoodsDTOListBean> getGoodsDTOList() {
                return goodsDTOList;
            }

            public void setGoodsDTOList(List<GoodsDTOListBean> goodsDTOList) {
                this.goodsDTOList = goodsDTOList;
            }

            public static class GoodsDTOListBean {
                /**
                 * unitType :
                 * quantity : 1
                 */

                private String deviceModel;
                private String quantity;

                public String getDeviceModel() {
                    return deviceModel;
                }

                public void setDeviceModel(String deviceModel) {
                    this.deviceModel = deviceModel;
                }

                public String getQuantity() {
                    return quantity;
                }

                public void setQuantity(String quantity) {
                    this.quantity = quantity;
                }
            }
        }
    }

}
