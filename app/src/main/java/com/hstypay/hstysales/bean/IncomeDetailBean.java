package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/4 14:01
 * @描述: ${TODO}
 */

public class IncomeDetailBean {
    /**
     * data : null
     * logId : 0QqVhThv
     * error : {"args":[{}],"code":"auth.username.pass.fail","message":"账号或密码不正确"}
     * status : false
     */
    private ErrorEntity error;

    public void setError(ErrorEntity error) {
        this.error = error;
    }
    public ErrorEntity getError() {
        return error;
    }
    public class ErrorEntity {
        /**
         * args : [{}]
         * code : auth.username.pass.fail
         * message : 账号或密码不正确
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"data":[{"billTime":"2017-12-02 00:00:00","salesmanId":17,"createTime":"2017-12-04 11:26:18","mchCount":40,"updateTime":"2017-12-04 13:58:04","id":1,"mchNew":5,"taskId":"1","enabled":true},{"billTime":"2017-12-03 00:00:00","salesmanId":17,"createTime":"2017-12-04 11:26:18","mchCount":40,"updateTime":"2017-12-04 13:58:14","id":2,"mchNew":5,"taskId":"1","enabled":true}],"totalPages":0,"pageSize":15,"totalRows":0,"currentPage":1}
     * logId : 4a2KAd1Y
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * data : [{"billTime":"2017-12-02 00:00:00","salesmanId":17,"createTime":"2017-12-04 11:26:18","mchCount":40,"updateTime":"2017-12-04 13:58:04","id":1,"mchNew":5,"taskId":"1","enabled":true},{"billTime":"2017-12-03 00:00:00","salesmanId":17,"createTime":"2017-12-04 11:26:18","mchCount":40,"updateTime":"2017-12-04 13:58:14","id":2,"mchNew":5,"taskId":"1","enabled":true}]
         * totalPages : 0
         * pageSize : 15
         * totalRows : 0
         * currentPage : 1
         */
        private List<ItemData> data;
        private int totalPages;
        private int pageSize;
        private int totalRows;
        private int currentPage;

        public void setData(List<ItemData> data) {
            this.data = data;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public List<ItemData> getData() {
            return data;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public int getPageSize() {
            return pageSize;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public class ItemData {
            /**
             * billTime : 2017-12-02 00:00:00
             * salesmanId : 17
             * createTime : 2017-12-04 11:26:18
             * mchCount : 40
             * updateTime : 2017-12-04 13:58:04
             * id : 1
             * mchNew : 5
             * taskId : 1
             * enabled : true
             */
            private String billTime;
            private int salesmanId;
            private String createTime;
            private int mchCount;
            private String updateTime;
            private int id;
            private int mchNew;
            private String taskId;
            private boolean enabled;
            private double feeToday;
            private double feeCount;

            public double getFeeToday() {
                return feeToday;
            }

            public void setFeeToday(double feeToday) {
                this.feeToday = feeToday;
            }

            public double getFeeCount() {
                return feeCount;
            }

            public void setFeeCount(double feeCount) {
                this.feeCount = feeCount;
            }

            public void setBillTime(String billTime) {
                this.billTime = billTime;
            }

            public void setSalesmanId(int salesmanId) {
                this.salesmanId = salesmanId;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public void setMchCount(int mchCount) {
                this.mchCount = mchCount;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public void setId(int id) {
                this.id = id;
            }

            public void setMchNew(int mchNew) {
                this.mchNew = mchNew;
            }

            public void setTaskId(String taskId) {
                this.taskId = taskId;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public String getBillTime() {
                return billTime;
            }

            public int getSalesmanId() {
                return salesmanId;
            }

            public String getCreateTime() {
                return createTime;
            }

            public int getMchCount() {
                return mchCount;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public int getId() {
                return id;
            }

            public int getMchNew() {
                return mchNew;
            }

            public String getTaskId() {
                return taskId;
            }

            public boolean isEnabled() {
                return enabled;
            }
        }
    }
}
