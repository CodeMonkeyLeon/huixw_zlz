package com.hstypay.hstysales.bean

/**
 * @Author dean.zeng
 * @Description 人脸支付
 * @Date 2020-06-30 10:25
 */
data class ApiRes<T>(val error: ErrorBean?,
                     val logId: String,
                     val status: Boolean,
                     val data: T)

data class ErrorBean(var code: String, var message: String, var args: List<*>)

data class ListData<T>(
    val currentPage: Int,
    val `data`: List<T>,
    val pageSize: Int,
    val totalPages: Int,
    val totalRows: Int
)