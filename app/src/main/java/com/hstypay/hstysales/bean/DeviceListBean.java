package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2019/3/13 16:36
 * @描述: ${TODO}
 */
public class DeviceListBean {
    private ErrorBean error;

    /**
     * logId : e522af5c0d8d4fc58fae5139a6079114
     * status : true
     * data : {"pageSize":15,"currentPage":1,"totalPages":0,"totalRows":0,"data":[{"createUser":1,"createUserName":"超级管理员","createTime":"2019-05-20 16:51:56","updateTime":"2019-05-21 17:41:03","pageSize":0,"currentPage":0,"id":161,"merchantId":"735200000001","merchantIdCnt":"彭国卿测试商户(威富通支付专用)","storeMerchantId":"462300000001","storeMerchantIdCnt":"彭国卿测试商户","sn":"10002066","status":1,"statusCnt":"已激活","model":"汇播报V1.0","lableServiceChannelId":"0","merchantName":"彭国卿测试商户","salesmanId":253,"serviceProviderId":"319100000001","serviceChannelId":"720100000001","setUp":"{\"payTypeBroadcast\":1}","firmName":"智网","purposeName":"语音播报","deviceId":4}]}
     */
    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private String logId;
    private boolean status;
    private DataEntity data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public class DataEntity {
        private List<DeviceBean> data;

        public List<DeviceBean> getData() {
            return data;
        }

        public void setData(List<DeviceBean> data) {
            this.data = data;
        }
    }
}
