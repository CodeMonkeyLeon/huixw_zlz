package com.hstypay.hstysales.bean;

import java.io.Serializable;

/**
 * @author zeyu.kuang
 * @time 2020/12/17
 * @desc 应用审核详情bean
 */
public class ExamineDetailBean extends BaseBean<ExamineDetailBean.DataBean>{
    public class DataBean implements Serializable {
        private static final long serialVersionUID = 1L;
        private String appId;//应用id
        private String appIdCnt;//应用名称
        private String orderStatus;//审核状态  1待支付  20待审核  30已完成  40已取消  50审核失败
        private String adminId;//商户编号
        private String adminIdCnt;//商户名称
        private String createTime;//申请时间
        private String examineUser;//审核人id
        private String examineUserCnt;//审核人名称
        private String examineTime;//审核时间
        private String examineRemark;//原因
        private String tel;//联系电话
        private String address;//地址
        private String appCode;//应用类型，花呗分期免息ALIPAY-ISTALLMENT  押金收款 PREAUTH-PAY
        private String orderStatusCnt;//审核状态: 中文
        private String billNo;//订购单号
        private String examineUserNo;//审核编号(城市经理编号、省市运营编号或平台)

        public String getExamineUserNo() {

            return examineUserNo == null ? "" : examineUserNo;

        }

        public void setExamineUserNo(String examineUserNo) {
            this.examineUserNo = examineUserNo;
        }

        public String getBillNo() {

            return billNo == null ? "" : billNo;

        }

        public void setBillNo(String billNo) {
            this.billNo = billNo;
        }

        public String getOrderStatusCnt() {

            return orderStatusCnt == null ? "" : orderStatusCnt;

        }

        public void setOrderStatusCnt(String orderStatusCnt) {
            this.orderStatusCnt = orderStatusCnt;
        }

        public String getAppCode() {

            return appCode == null ? "" : appCode;

        }

        public void setAppCode(String appCode) {
            this.appCode = appCode;
        }

        public String getAppId() {

            return appId == null ? "" : appId;

        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public String getAppIdCnt() {

            return appIdCnt == null ? "" : appIdCnt;

        }

        public void setAppIdCnt(String appIdCnt) {
            this.appIdCnt = appIdCnt;
        }

        public String getOrderStatus() {

            return orderStatus == null ? "" : orderStatus;

        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getAdminId() {

            return adminId == null ? "" : adminId;

        }

        public void setAdminId(String adminId) {
            this.adminId = adminId;
        }

        public String getAdminIdCnt() {

            return adminIdCnt == null ? "" : adminIdCnt;

        }

        public void setAdminIdCnt(String adminIdCnt) {
            this.adminIdCnt = adminIdCnt;
        }

        public String getCreateTime() {

            return createTime == null ? "" : createTime;

        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getExamineUser() {

            return examineUser == null ? "" : examineUser;

        }

        public void setExamineUser(String examineUser) {
            this.examineUser = examineUser;
        }

        public String getExamineUserCnt() {

            return examineUserCnt == null ? "" : examineUserCnt;

        }

        public void setExamineUserCnt(String examineUserCnt) {
            this.examineUserCnt = examineUserCnt;
        }

        public String getExamineTime() {

            return examineTime == null ? "" : examineTime;

        }

        public void setExamineTime(String examineTime) {
            this.examineTime = examineTime;
        }

        public String getExamineRemark() {

            return examineRemark == null ? "" : examineRemark;

        }

        public void setExamineRemark(String examineRemark) {
            this.examineRemark = examineRemark;
        }

        public String getTel() {

            return tel == null ? "" : tel;

        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getAddress() {

            return address == null ? "" : address;

        }

        public void setAddress(String address) {
            this.address = address;
        }
    }
}
