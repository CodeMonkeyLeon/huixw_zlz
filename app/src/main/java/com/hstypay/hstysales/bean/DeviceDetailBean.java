package com.hstypay.hstysales.bean;

public class DeviceDetailBean extends BaseBean<DeviceDetailBean.DeviceDetailData>{
    public class DeviceDetailData{
        private String id;
        private String merchantId;
        private String merchantIdCnt;
        private String storeMerchantId;
        private String storeMerchantIdCnt;
        private String sn;
        private String createUser;
        private String createUserName;
        private String createTime;
        private String updateTime;
        private long pageSize;
        private long currentPage;
        private int status;
        private String statusCnt;
        private String model;
        private String lableServiceProviderId;
        private String serviceProviderId;
        private String serviceChannelId;
        private String deviceId;
        private String bindingTime;
        private String configType;
        private String enabled;
        private String deviceModel;
        private String categoryName;

        private TradeResult tradeResult;

        public String getId() {
            return id == null ? "" : id;


        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMerchantId() {
            return merchantId == null ? "" : merchantId;


        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getMerchantIdCnt() {
            return merchantIdCnt == null ? "" : merchantIdCnt;


        }

        public void setMerchantIdCnt(String merchantIdCnt) {
            this.merchantIdCnt = merchantIdCnt;
        }

        public String getStoreMerchantId() {
            return storeMerchantId == null ? "" : storeMerchantId;


        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public String getStoreMerchantIdCnt() {
            return storeMerchantIdCnt == null ? "" : storeMerchantIdCnt;


        }

        public void setStoreMerchantIdCnt(String storeMerchantIdCnt) {
            this.storeMerchantIdCnt = storeMerchantIdCnt;
        }

        public String getSn() {
            return sn == null ? "" : sn;


        }

        public void setSn(String sn) {
            this.sn = sn;
        }

        public String getCreateUser() {
            return createUser == null ? "" : createUser;


        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public String getCreateUserName() {
            return createUserName == null ? "" : createUserName;


        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public String getCreateTime() {
            return createTime == null ? "" : createTime;


        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUpdateTime() {
            return updateTime == null ? "" : updateTime;


        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public long getPageSize() {
            return pageSize;


        }

        public void setPageSize(long pageSize) {
            this.pageSize = pageSize;
        }

        public long getCurrentPage() {
            return currentPage;


        }

        public void setCurrentPage(long currentPage) {
            this.currentPage = currentPage;
        }

        public int getStatus() {
            return status;


        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getStatusCnt() {
            return statusCnt == null ? "" : statusCnt;


        }

        public void setStatusCnt(String statusCnt) {
            this.statusCnt = statusCnt;
        }

        public String getModel() {
            return model == null ? "" : model;


        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getLableServiceProviderId() {
            return lableServiceProviderId == null ? "" : lableServiceProviderId;


        }

        public void setLableServiceProviderId(String lableServiceProviderId) {
            this.lableServiceProviderId = lableServiceProviderId;
        }

        public String getServiceProviderId() {
            return serviceProviderId == null ? "" : serviceProviderId;


        }

        public void setServiceProviderId(String serviceProviderId) {
            this.serviceProviderId = serviceProviderId;
        }

        public String getServiceChannelId() {
            return serviceChannelId == null ? "" : serviceChannelId;


        }

        public void setServiceChannelId(String serviceChannelId) {
            this.serviceChannelId = serviceChannelId;
        }

        public String getDeviceId() {
            return deviceId == null ? "" : deviceId;


        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getBindingTime() {
            return bindingTime == null ? "" : bindingTime;


        }

        public void setBindingTime(String bindingTime) {
            this.bindingTime = bindingTime;
        }

        public String getConfigType() {
            return configType == null ? "" : configType;


        }

        public void setConfigType(String configType) {
            this.configType = configType;
        }

        public String getEnabled() {
            return enabled == null ? "" : enabled;


        }

        public void setEnabled(String enabled) {
            this.enabled = enabled;
        }

        public String getDeviceModel() {
            return deviceModel == null ? "" : deviceModel;


        }

        public void setDeviceModel(String deviceModel) {
            this.deviceModel = deviceModel;
        }

        public String getCategoryName() {
            return categoryName == null ? "" : categoryName;


        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public TradeResult getTradeResult() {
            return tradeResult;


        }

        public void setTradeResult(TradeResult tradeResult) {
            this.tradeResult = tradeResult;
        }
    }

    //设备交易统计
    public class TradeResult{
        private double totalIncome;
        private double payAmount;
        private double payNetAmount;
        private double mchDiscountsTotalAmount;
        private long mchDiscountsTotalCount;
        private long payTotalCount;
        private double refundTotalAmount;
        private long refundTotalCount;
        private double saleTotalAmount;

        public double getTotalIncome() {
            return totalIncome;


        }

        public void setTotalIncome(double totalIncome) {
            this.totalIncome = totalIncome;
        }

        public double getPayAmount() {
            return payAmount;


        }

        public void setPayAmount(double payAmount) {
            this.payAmount = payAmount;
        }

        public double getPayNetAmount() {
            return payNetAmount;


        }

        public void setPayNetAmount(double payNetAmount) {
            this.payNetAmount = payNetAmount;
        }

        public double getMchDiscountsTotalAmount() {
            return mchDiscountsTotalAmount;


        }

        public void setMchDiscountsTotalAmount(double mchDiscountsTotalAmount) {
            this.mchDiscountsTotalAmount = mchDiscountsTotalAmount;
        }

        public long getMchDiscountsTotalCount() {
            return mchDiscountsTotalCount;


        }

        public void setMchDiscountsTotalCount(long mchDiscountsTotalCount) {
            this.mchDiscountsTotalCount = mchDiscountsTotalCount;
        }

        public long getPayTotalCount() {
            return payTotalCount;


        }

        public void setPayTotalCount(long payTotalCount) {
            this.payTotalCount = payTotalCount;
        }

        public double getRefundTotalAmount() {
            return refundTotalAmount;


        }

        public void setRefundTotalAmount(double refundTotalAmount) {
            this.refundTotalAmount = refundTotalAmount;
        }

        public long getRefundTotalCount() {
            return refundTotalCount;


        }

        public void setRefundTotalCount(long refundTotalCount) {
            this.refundTotalCount = refundTotalCount;
        }

        public double getSaleTotalAmount() {
            return saleTotalAmount;


        }

        public void setSaleTotalAmount(double saleTotalAmount) {
            this.saleTotalAmount = saleTotalAmount;
        }
    }
}
