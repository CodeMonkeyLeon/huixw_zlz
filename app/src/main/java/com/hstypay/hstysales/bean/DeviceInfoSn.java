package com.hstypay.hstysales.bean;

public class DeviceInfoSn extends BaseBean<DeviceInfoSn.DeviceInfoData>{
    public class DeviceInfoData{
        private String mallDeviceId;
        private String createUser;
        private String createUserName;
        private String createTime;
        private String updateTime;
        private String pageSize;
        private String currentPage;
        private String id;
        private String merchantId;
        private String merchantIdCnt;
        private String storeMerchantId;
        private String storeMerchantIdCnt;
        private String sn;
        private String status;
        private String statusCnt;
        private String signKey;
        private String model;
        private String firm;
        private String firmName;
        private String deviceId;
        private String enabled;
        private String deviceModel;
        private String categoryName;
        private String categoryCode;
        private String bindSet;

        public String getMallDeviceId() {
            return mallDeviceId == null ? "" : mallDeviceId;


        }

        public void setMallDeviceId(String mallDeviceId) {
            this.mallDeviceId = mallDeviceId;
        }

        public String getCreateUser() {
            return createUser == null ? "" : createUser;


        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public String getCreateUserName() {
            return createUserName == null ? "" : createUserName;


        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public String getCreateTime() {
            return createTime == null ? "" : createTime;


        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUpdateTime() {
            return updateTime == null ? "" : updateTime;


        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public String getPageSize() {
            return pageSize == null ? "" : pageSize;


        }

        public void setPageSize(String pageSize) {
            this.pageSize = pageSize;
        }

        public String getCurrentPage() {
            return currentPage == null ? "" : currentPage;


        }

        public void setCurrentPage(String currentPage) {
            this.currentPage = currentPage;
        }

        public String getId() {
            return id == null ? "" : id;


        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMerchantId() {
            return merchantId == null ? "" : merchantId;


        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getMerchantIdCnt() {
            return merchantIdCnt == null ? "" : merchantIdCnt;


        }

        public void setMerchantIdCnt(String merchantIdCnt) {
            this.merchantIdCnt = merchantIdCnt;
        }

        public String getStoreMerchantId() {
            return storeMerchantId == null ? "" : storeMerchantId;


        }

        public void setStoreMerchantId(String storeMerchantId) {
            this.storeMerchantId = storeMerchantId;
        }

        public String getStoreMerchantIdCnt() {
            return storeMerchantIdCnt == null ? "" : storeMerchantIdCnt;


        }

        public void setStoreMerchantIdCnt(String storeMerchantIdCnt) {
            this.storeMerchantIdCnt = storeMerchantIdCnt;
        }

        public String getSn() {
            return sn == null ? "" : sn;


        }

        public void setSn(String sn) {
            this.sn = sn;
        }

        public String getStatus() {
            return status == null ? "" : status;


        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatusCnt() {
            return statusCnt == null ? "" : statusCnt;


        }

        public void setStatusCnt(String statusCnt) {
            this.statusCnt = statusCnt;
        }

        public String getSignKey() {
            return signKey == null ? "" : signKey;


        }

        public void setSignKey(String signKey) {
            this.signKey = signKey;
        }

        public String getModel() {
            return model == null ? "" : model;


        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getFirm() {
            return firm == null ? "" : firm;


        }

        public void setFirm(String firm) {
            this.firm = firm;
        }

        public String getFirmName() {
            return firmName == null ? "" : firmName;


        }

        public void setFirmName(String firmName) {
            this.firmName = firmName;
        }

        public String getDeviceId() {
            return deviceId == null ? "" : deviceId;


        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getEnabled() {
            return enabled == null ? "" : enabled;


        }

        public void setEnabled(String enabled) {
            this.enabled = enabled;
        }

        public String getDeviceModel() {
            return deviceModel == null ? "" : deviceModel;


        }

        public void setDeviceModel(String deviceModel) {
            this.deviceModel = deviceModel;
        }

        public String getCategoryName() {
            return categoryName == null ? "" : categoryName;


        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getCategoryCode() {
            return categoryCode == null ? "" : categoryCode;


        }

        public void setCategoryCode(String categoryCode) {
            this.categoryCode = categoryCode;
        }

        public String getBindSet() {
            return bindSet == null ? "" : bindSet;


        }

        public void setBindSet(String bindSet) {
            this.bindSet = bindSet;
        }
    }
}
