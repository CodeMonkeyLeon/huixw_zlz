package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/7 19:59
 * @描述: ${TODO}
 */

public class CodeUrlBean {
    /**
     * data : null
     * logId : 0QqVhThv
     * error : {"args":[{}],"code":"auth.username.pass.fail","message":"账号或密码不正确"}
     * status : false
     */
    private ErrorEntity error;

    public void setError(ErrorEntity error) {
        this.error = error;
    }
    public ErrorEntity getError() {
        return error;
    }
    public class ErrorEntity {
        /**
         * args : [{}]
         * code : auth.username.pass.fail
         * message : 账号或密码不正确
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"url":"http://hpay.dev.hstypay.com/app/registe/qrcode/11cc8a48ac6b405f12d13ac26c81044501b92d5421e18afda18c340fc273e5f89aae49272741566f7f68c801f57bf23ee47572baa600d842","timeout":1800}
     * logId : V6Cf0wjq
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity  {
        /**
         * url : http://hpay.dev.hstypay.com/app/registe/qrcode/11cc8a48ac6b405f12d13ac26c81044501b92d5421e18afda18c340fc273e5f89aae49272741566f7f68c801f57bf23ee47572baa600d842
         * timeout : 1800
         */
        private String url;
        private long timeout;

        /**
         * 领取会员卡链接接口
         */
        private boolean retcode;
        private String mcard_receive_url;
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void setMemerUrl(String mcard_receive_url) {
            this.mcard_receive_url = mcard_receive_url;
        }

        public String getMemerUrl() {
            return mcard_receive_url;
        }

        public void setRetCode(boolean retcode) {
            this.retcode = retcode;
        }

        public boolean getRetCode() {
            return retcode;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public void setTimeout(long timeout) {
            this.timeout = timeout;
        }

        public String getUrl() {
            return url;
        }

        public long getTimeout() {
            return timeout;
        }
    }
}
