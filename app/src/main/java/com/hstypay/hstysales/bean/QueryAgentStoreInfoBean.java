package com.hstypay.hstysales.bean;

//代运营门店信息
public class QueryAgentStoreInfoBean extends BaseBean<QueryAgentStoreInfoBean.AgentStoreInfo>{
    public static class AgentStoreInfo{
        private String merchantId;//商户号
        private String tradeCode;//交易识别码
        private String agentType;//代运营授权类型 默认：支付宝门店代运营
        private String authorizeStatus;//授权状态:1-初始，2-成功，3-待确认,4-失败
        private String authorizeResultDesc;//授权状态描述
        private String shopStatus;//支付宝门店创建状态:1-初始，2-受理中，3-创建成功，4-创建失败
        private String shopResultDesc;//支付宝门店创建结果描述
        private String qrCodeUrl;//代运营授权二维码
        private String payOrgId;//支付机构
        private String payCenterId;//支付通道
        private String shopId;//支付宝门店编号

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getTradeCode() {
            return tradeCode;
        }

        public void setTradeCode(String tradeCode) {
            this.tradeCode = tradeCode;
        }

        public String getAgentType() {
            return agentType;
        }

        public void setAgentType(String agentType) {
            this.agentType = agentType;
        }

        public String getAuthorizeStatus() {
            return authorizeStatus;
        }

        public void setAuthorizeStatus(String authorizeStatus) {
            this.authorizeStatus = authorizeStatus;
        }

        public String getAuthorizeResultDesc() {
            return authorizeResultDesc;
        }

        public void setAuthorizeResultDesc(String authorizeResultDesc) {
            this.authorizeResultDesc = authorizeResultDesc;
        }

        public String getShopStatus() {
            return shopStatus;
        }

        public void setShopStatus(String shopStatus) {
            this.shopStatus = shopStatus;
        }

        public String getShopResultDesc() {
            return shopResultDesc;
        }

        public void setShopResultDesc(String shopResultDesc) {
            this.shopResultDesc = shopResultDesc;
        }

        public String getQrCodeUrl() {
            return qrCodeUrl==null?"":qrCodeUrl;
        }

        public void setQrCodeUrl(String qrCodeUrl) {
            this.qrCodeUrl = qrCodeUrl;
        }

        public String getPayOrgId() {
            return payOrgId;
        }

        public void setPayOrgId(String payOrgId) {
            this.payOrgId = payOrgId;
        }

        public String getPayCenterId() {
            return payCenterId;
        }

        public void setPayCenterId(String payCenterId) {
            this.payCenterId = payCenterId;
        }

        public String getShopId() {
            return shopId;
        }

        public void setShopId(String shopId) {
            this.shopId = shopId;
        }
    }
}
