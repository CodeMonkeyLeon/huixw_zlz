package com.hstypay.hstysales.bean;

import android.support.v4.app.Fragment;

import com.hstypay.hstysales.fragment.devicemall.BaseDeviceMallFragment;

/**
 * @author zeyu.kuang
 * @time 2020/8/23
 * @desc
 */
public class DeviceMallFragmentTitleBean {
    private String title;
    private BaseDeviceMallFragment fragment;

    public DeviceMallFragmentTitleBean(String title, BaseDeviceMallFragment fragment) {
        this.title = title;
        this.fragment = fragment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BaseDeviceMallFragment getFragment() {
        return fragment;
    }

    public void setFragment(BaseDeviceMallFragment fragment) {
        this.fragment = fragment;
    }
}
