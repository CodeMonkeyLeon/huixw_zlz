package com.hstypay.hstysales.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/12/17
 * @desc
 */
public class ExamineListBean extends BaseBean<ExamineListBean.DataBean>{

    public class DataBean{
        private int pageSize;
        private int currentPage;
        private int totalPages;
        private int totalRows;
        private List<ExamineBean> data;

        public int getPageSize() {

            return pageSize;

        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getCurrentPage() {

            return currentPage;

        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotalPages() {

            return totalPages;

        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public int getTotalRows() {

            return totalRows;

        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public List<ExamineBean> getData() {

            if (data == null) {
                return new ArrayList<>();
            }
            return data;

        }

        public void setData(List<ExamineBean> data) {
            this.data = data;
        }
    }
    public class ExamineBean{
        private String billNo;//订购单号
        private String appId;//应用id
        private String appIdCnt;//应用名称
        private String adminId;//商户id
        private String adminIdCnt;//商户名称
        private String createTime;//创建时间
        private String orderStatus;//审核状态  1待支付  20待审核  30已完成  40已取消  50审核失败
        private String orderStatusCnt;//审核状态中文
        private String appCode;//应用类型，花呗分期免息ALIPAY-ISTALLMENT  押金收款 PREAUTH-PAY

        public String getAppCode() {

            return appCode == null ? "" : appCode;

        }

        public void setAppCode(String appCode) {
            this.appCode = appCode;
        }

        public String getBillNo() {

            return billNo == null ? "" : billNo;

        }

        public void setBillNo(String billNo) {
            this.billNo = billNo;
        }

        public String getAppId() {

            return appId == null ? "" : appId;

        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public String getAppIdCnt() {

            return appIdCnt == null ? "" : appIdCnt;

        }

        public void setAppIdCnt(String appIdCnt) {
            this.appIdCnt = appIdCnt;
        }

        public String getAdminId() {

            return adminId == null ? "" : adminId;

        }

        public void setAdminId(String adminId) {
            this.adminId = adminId;
        }

        public String getAdminIdCnt() {

            return adminIdCnt == null ? "" : adminIdCnt;

        }

        public void setAdminIdCnt(String adminIdCnt) {
            this.adminIdCnt = adminIdCnt;
        }

        public String getCreateTime() {

            return createTime == null ? "" : createTime;

        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getOrderStatus() {

            return orderStatus == null ? "" : orderStatus;

        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getOrderStatusCnt() {

            return orderStatusCnt == null ? "" : orderStatusCnt;

        }

        public void setOrderStatusCnt(String orderStatusCnt) {
            this.orderStatusCnt = orderStatusCnt;
        }
    }
}
