package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/6 15:27
 * @描述: ${TODO}
 */

public class CodeDetailBean {
    /**
     * data : null
     * logId : 0QqVhThv
     * error : {"args":[{}],"code":"auth.username.pass.fail","message":"账号或密码不正确"}
     * status : false
     */
    private ErrorEntity error;

    public void setError(ErrorEntity error) {
        this.error = error;
    }
    public ErrorEntity getError() {
        return error;
    }
    public class ErrorEntity {
        /**
         * args : [{}]
         * code : auth.username.pass.fail
         * message : 账号或密码不正确
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"salesmanId":17,"serviceProviderId":"731100000001","costRate":2600,"costRateType":0,"merchantCount":0,"validStartAt":"2017-12-02 00:00:00","createUserName":"超级管理员","updateTime":"2017-12-02 11:45:37","validEndAt":"2017-12-30 23:59:59","enabled":true,"validDateCnt":"2017.12.02-2017.12.30","createTime":"2017-12-02 11:45:37","createUser":1,"maxUseCount":2,"costRateCnt":"0.26%","invitationCode":"663351","usedCount":0}
     * logId : Uc1kaw1n
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * salesmanId : 17
         * serviceProviderId : 731100000001
         * costRate : 2600
         * costRateType : 0
         * merchantCount : 0
         * validStartAt : 2017-12-02 00:00:00
         * createUserName : 超级管理员
         * updateTime : 2017-12-02 11:45:37
         * validEndAt : 2017-12-30 23:59:59
         * enabled : true
         * validDateCnt : 2017.12.02-2017.12.30
         * createTime : 2017-12-02 11:45:37
         * createUser : 1
         * maxUseCount : 2
         * costRateCnt : 0.26%
         * invitationCode : 663351
         * usedCount : 0
         */
        private int salesmanId;
        private String serviceProviderId;
        private int costRate;
        private int costRateType;
        private int merchantCount;
        private String validStartAt;
        private String createUserName;
        private String updateTime;
        private String validEndAt;
        private boolean enabled;
        private String validDateCnt;
        private String createTime;
        private int createUser;
        private int maxUseCount;
        private String costRateCnt;
        private String invitationCode;
        private int usedCount;

        public String toString(){
            return usedCount+"--"+merchantCount+"---"+validStartAt+"-"+validEndAt;
        }

        public void setSalesmanId(int salesmanId) {
            this.salesmanId = salesmanId;
        }

        public void setServiceProviderId(String serviceProviderId) {
            this.serviceProviderId = serviceProviderId;
        }

        public void setCostRate(int costRate) {
            this.costRate = costRate;
        }

        public void setCostRateType(int costRateType) {
            this.costRateType = costRateType;
        }

        public void setMerchantCount(int merchantCount) {
            this.merchantCount = merchantCount;
        }

        public void setValidStartAt(String validStartAt) {
            this.validStartAt = validStartAt;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public void setValidEndAt(String validEndAt) {
            this.validEndAt = validEndAt;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public void setValidDateCnt(String validDateCnt) {
            this.validDateCnt = validDateCnt;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public void setCreateUser(int createUser) {
            this.createUser = createUser;
        }

        public void setMaxUseCount(int maxUseCount) {
            this.maxUseCount = maxUseCount;
        }

        public void setCostRateCnt(String costRateCnt) {
            this.costRateCnt = costRateCnt;
        }

        public void setInvitationCode(String invitationCode) {
            this.invitationCode = invitationCode;
        }

        public void setUsedCount(int usedCount) {
            this.usedCount = usedCount;
        }

        public int getSalesmanId() {
            return salesmanId;
        }

        public String getServiceProviderId() {
            return serviceProviderId;
        }

        public int getCostRate() {
            return costRate;
        }

        public int getCostRateType() {
            return costRateType;
        }

        public int getMerchantCount() {
            return merchantCount;
        }

        public String getValidStartAt() {
            return validStartAt;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public String getValidEndAt() {
            return validEndAt;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public String getValidDateCnt() {
            return validDateCnt;
        }

        public String getCreateTime() {
            return createTime;
        }

        public int getCreateUser() {
            return createUser;
        }

        public int getMaxUseCount() {
            return maxUseCount;
        }

        public String getCostRateCnt() {
            return costRateCnt;
        }

        public String getInvitationCode() {
            return invitationCode;
        }

        public int getUsedCount() {
            return usedCount;
        }
    }
}
