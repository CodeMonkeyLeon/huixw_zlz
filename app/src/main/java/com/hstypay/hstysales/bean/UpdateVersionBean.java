package com.hstypay.hstysales.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/8/11.
 */

public class UpdateVersionBean implements Serializable {


    /**
     * logId : A3ApTbnD
     * status : true
     * error : {"code":"client.user.pwd.incorrect","message":"用户名或者密码输入不正确","args":[]}
     * data : {"version":"1.0","appName":"HstyPay","client":"HPAY_AND","versionMsg":"更新了语音播报设置功能，优化了一些用户体验","appUrl":"/xxx/xxx/hstypay","forceUpgrade":"0"}
     */

    private String logId;
    private boolean status;
    private ErrorBean error;
    private DataBean data;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public static class DataBean implements Serializable{

        /**
         * version : 1.0
         * appName : HstyPay
         * client : HPAY_AND
         * versionMsg : 更新了语音播报设置功能，优化了一些用户体验
         * appUrl : /xxx/xxx/hstypay
         * forceUpgrade : 0
         */

        private String versionName;
        private int versionCode;
        private String appName;
        private String client;
        private String versionMsg;
        private String appUrl;
        private Number forceUpgrade;
        private String versionTitle;
        private boolean enable;

        private int createUser;
        private String createUserName;
        private String createTime;
        private String updateTime;
        private int pageSize;
        private int currentPage;
        private int versionId;
        private String appChannel;
        private String appType;
        private String appAndroidUrl;
        private String appIosUrl;

        public int getCreateUser() {
            return createUser;
        }

        public void setCreateUser(int createUser) {
            this.createUser = createUser;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getVersionId() {
            return versionId;
        }

        public void setVersionId(int versionId) {
            this.versionId = versionId;
        }

        public String getAppChannel() {
            return appChannel;
        }

        public void setAppChannel(String appChannel) {
            this.appChannel = appChannel;
        }

        public String getAppType() {
            return appType;
        }

        public void setAppType(String appType) {
            this.appType = appType;
        }

        public String getAppAndroidUrl() {
            return appAndroidUrl;
        }

        public void setAppAndroidUrl(String appAndroidUrl) {
            this.appAndroidUrl = appAndroidUrl;
        }

        public String getAppIosUrl() {
            return appIosUrl;
        }

        public void setAppIosUrl(String appIosUrl) {
            this.appIosUrl = appIosUrl;
        }

        public boolean isEnable() {
            return enable;
        }

        public void setEnable(boolean enable) {
            this.enable = enable;
        }

        public String getVersionTitle() {
            return versionTitle;
        }

        public void setVersionTitle(String versionTitle) {
            this.versionTitle = versionTitle;
        }

        public String getVersionName() {
            return versionName;
        }

        public void setVersionName(String versionName) {
            this.versionName = versionName;
        }

        public int getVersionCode() {
            return versionCode;
        }

        public void setVersionCode(int versionCode) {
            this.versionCode = versionCode;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

        public String getClient() {
            return client;
        }

        public void setClient(String client) {
            this.client = client;
        }

        public String getVersionMsg() {
            return versionMsg;
        }

        public void setVersionMsg(String versionMsg) {
            this.versionMsg = versionMsg;
        }

        public String getAppUrl() {
            return appUrl;
        }

        public void setAppUrl(String appUrl) {
            this.appUrl = appUrl;
        }

        public Number getForceUpgrade() {
            return forceUpgrade;
        }

        public void setForceUpgrade(Number forceUpgrade) {
            this.forceUpgrade = forceUpgrade;
        }
    }
}
