package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2018/3/9 9:58
 * @描述: ${TODO}
 */

public class HomeNoticeBean {
    /**
     * data : null
     * logId : 0QqVhThv
     * error : {"args":[{}],"code":"auth.username.pass.fail","message":"账号或密码不正确"}
     * status : false
     */
    private ErrorEntity error;

    public void setError(ErrorEntity error) {
        this.error = error;
    }
    public ErrorEntity getError() {
        return error;
    }
    public class ErrorEntity {
        /**
         * args : [{}]
         * code : auth.username.pass.fail
         * message : 账号或密码不正确
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    /**
     * data : {"platformCnt":"汇小旺","createUserName":"超级管理员","updateTime":"2018-03-08 17:18:50","title":"测试公告666666","readCount":0,"statusCnt":"已发布","userId":0,"content":"<p style=\"text-align: left;\"><span style=\"font-family: Georgia, STSong, 华文宋体, STFangsong, 华文仿宋, FangSong, NSimSun, SimSun, FangSongGB2312, serif; background-color: rgb(195, 153, 102);\">朝辞白帝彩云间，千里江陵一日还。两岸猿声啼不住，轻舟已过万重山。<\/span><\/p>","platform":3,"createTime":"2018-03-08 17:18:50","createUser":1,"createUserType":1,"id":59,"status":1}
     * logId : eKiZGLZj
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * platformCnt : 汇小旺
         * createUserName : 超级管理员
         * updateTime : 2018-03-08 17:18:50
         * title : 测试公告666666
         * readCount : 0
         * statusCnt : 已发布
         * userId : 0
         * content : <p style="text-align: left;"><span style="font-family: Georgia, STSong, 华文宋体, STFangsong, 华文仿宋, FangSong, NSimSun, SimSun, FangSongGB2312, serif; background-color: rgb(195, 153, 102);">朝辞白帝彩云间，千里江陵一日还。两岸猿声啼不住，轻舟已过万重山。</span></p>
         * platform : 3
         * createTime : 2018-03-08 17:18:50
         * createUser : 1
         * createUserType : 1
         * id : 59
         * status : 1
         */
        private String platformCnt;
        private String createUserName;
        private String updateTime;
        private String title;
        private int readCount;
        private String statusCnt;
        private int userId;
        private String content;
        private int platform;
        private String createTime;
        private int createUser;
        private int createUserType;
        private int id;
        private int status;

        public void setPlatformCnt(String platformCnt) {
            this.platformCnt = platformCnt;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setReadCount(int readCount) {
            this.readCount = readCount;
        }

        public void setStatusCnt(String statusCnt) {
            this.statusCnt = statusCnt;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public void setPlatform(int platform) {
            this.platform = platform;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public void setCreateUser(int createUser) {
            this.createUser = createUser;
        }

        public void setCreateUserType(int createUserType) {
            this.createUserType = createUserType;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getPlatformCnt() {
            return platformCnt;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public String getTitle() {
            return title;
        }

        public int getReadCount() {
            return readCount;
        }

        public String getStatusCnt() {
            return statusCnt;
        }

        public int getUserId() {
            return userId;
        }

        public String getContent() {
            return content;
        }

        public int getPlatform() {
            return platform;
        }

        public String getCreateTime() {
            return createTime;
        }

        public int getCreateUser() {
            return createUser;
        }

        public int getCreateUserType() {
            return createUserType;
        }

        public int getId() {
            return id;
        }

        public int getStatus() {
            return status;
        }
    }
}
