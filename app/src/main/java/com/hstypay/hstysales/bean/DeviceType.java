package com.hstypay.hstysales.bean;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @e-mail: yaokunkun@hstypay.com
 * @创建者: Jeremy
 * @创建时间: 2020/10/09 10:58
 * @描述: ${TODO}
 */
public class DeviceType {
    private String typeId;
    private String typeName;
    private String typeCode;
    private String firmId;

    public String getTypeId() {
        return typeId == null ? "" : typeId;


    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName == null ? "" : typeName;


    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeCode() {
        return typeCode == null ? "" : typeCode;


    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getFirmId() {
        return firmId == null ? "" : firmId;


    }

    public void setFirmId(String firmId) {
        this.firmId = firmId;
    }
}
