package com.hstypay.hstysales.bean;

/**
 * @author zeyu.kuang
 * @time 2020/12/15
 * @desc 应用开通筛选bean
 */
public class AppOpenFilterBean {
    private String id;
    private String name;

    public AppOpenFilterBean(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id == null ? "" : id;

    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {

        return name == null ? "" : name;

    }

    public void setName(String name) {
        this.name = name;
    }

}
