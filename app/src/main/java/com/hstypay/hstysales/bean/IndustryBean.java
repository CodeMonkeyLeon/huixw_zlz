package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @项目名: Test
 * @包名: com.hstypay.test.address.bean
 * @创建者: Jeremy
 * @创建时间: 2017/7/15 17:02
 * @描述: ${TODO}
 */

public class IndustryBean {
    /**
     * data : [{"industryName":"企业","industryId":10000001,"list":[{"industryName":"生活/咨询服务","industryId":10000042,"list":[{"industryName":"企业-生活/咨询服务-人才中介机构/招聘/猎头","industryId":221},{"industryName":"企业-生活/咨询服务-职业社交/婚介/交友","industryId":222}]}]}]
     * logId : z9BAy7M1
     * status : true
     */
    private List<Industry1Entity> data;
    private String logId;
    private boolean status;

    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    public void setData(List<Industry1Entity> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<Industry1Entity> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class Industry1Entity {
        /**
         * industryName : 企业
         * industryId : 10000001
         * list : [{"industryName":"生活/咨询服务","industryId":10000042,"list":[{"industryName":"企业-生活/咨询服务-人才中介机构/招聘/猎头","industryId":221},{"industryName":"企业-生活/咨询服务-职业社交/婚介/交友","industryId":222}]}]
         */
        private String industryName;
        private int industryId;
        private List<Industry2Entity> list;

        public void setIndustryName(String industryName) {
            this.industryName = industryName;
        }

        public void setIndustryId(int industryId) {
            this.industryId = industryId;
        }

        public void setList(List<Industry2Entity> list) {
            this.list = list;
        }

        public String getIndustryName() {
            return industryName;
        }

        public int getIndustryId() {
            return industryId;
        }

        public List<Industry2Entity> getList() {
            return list;
        }

        public class Industry2Entity {
            /**
             * industryName : 生活/咨询服务
             * industryId : 10000042
             * list : [{"industryName":"企业-生活/咨询服务-人才中介机构/招聘/猎头","industryId":221},{"industryName":"企业-生活/咨询服务-职业社交/婚介/交友","industryId":222}]
             */
            private String industryName;
            private int industryId;
            private List<Industry3Entity> list;

            public void setIndustryName(String industryName) {
                this.industryName = industryName;
            }

            public void setIndustryId(int industryId) {
                this.industryId = industryId;
            }

            public void setList(List<Industry3Entity> list) {
                this.list = list;
            }

            public String getIndustryName() {
                return industryName;
            }

            public int getIndustryId() {
                return industryId;
            }

            public List<Industry3Entity> getList() {
                return list;
            }

            public class Industry3Entity {
                /**
                 * industryName : 企业-生活/咨询服务-人才中介机构/招聘/猎头
                 * industryId : 221
                 */
                private String industryName;
                private int industryId;

                public void setIndustryName(String industryName) {
                    this.industryName = industryName;
                }

                public void setIndustryId(int industryId) {
                    this.industryId = industryId;
                }

                public String getIndustryName() {
                    return industryName;
                }

                public int getIndustryId() {
                    return industryId;
                }
            }
        }
    }
}
