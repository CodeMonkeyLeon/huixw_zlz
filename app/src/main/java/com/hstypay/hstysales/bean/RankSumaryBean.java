package com.hstypay.hstysales.bean;

public class RankSumaryBean extends BaseBean<RankSumaryBean.RankSumaryData>{

    public static class RankSumaryData{

        private String date;
        private double actualMoney;
        private int successCount;
        private int bigTradeCount;
        private int activityMerchantCount;
        private int totalMerchantCount;
        private double unitPrice;
        private int noActivityMerchantCount;

        public String getDate() {
            return date == null ? "" : date;

        }

        public void setDate(String date) {
            this.date = date;
        }

        public double getActualMoney() {
            return actualMoney;
        }

        public void setActualMoney(double actualMoney) {
            this.actualMoney = actualMoney;
        }

        public int getSuccessCount() {
            return successCount;
        }

        public void setSuccessCount(int successCount) {
            this.successCount = successCount;
        }

        public int getBigTradeCount() {
            return bigTradeCount;
        }

        public void setBigTradeCount(int bigTradeCount) {
            this.bigTradeCount = bigTradeCount;
        }

        public int getActivityMerchantCount() {
            return activityMerchantCount;
        }

        public void setActivityMerchantCount(int activityMerchantCount) {
            this.activityMerchantCount = activityMerchantCount;
        }

        public int getTotalMerchantCount() {
            return totalMerchantCount;
        }

        public void setTotalMerchantCount(int totalMerchantCount) {
            this.totalMerchantCount = totalMerchantCount;
        }

        public double getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(double unitPrice) {
            this.unitPrice = unitPrice;
        }

        public int getNoActivityMerchantCount() {
            return noActivityMerchantCount;
        }

        public void setNoActivityMerchantCount(int noActivityMerchantCount) {
            this.noActivityMerchantCount = noActivityMerchantCount;
        }
    }
}
