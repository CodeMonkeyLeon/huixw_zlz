package com.hstypay.hstysales.bean

import java.io.Serializable

/**
 * @Author dean.zeng
 * @Description TODO
 * @Date 2020-07-16 18:15
 **/
/**
 * 门店
 */
data class StoreList(
        val storeId: String,
        val storeName: String
)

//微收银激活码管理
data class ActivationCode(
        val storeMerchantName: String,
        val storeMerchantId: String,
        val storeMerchantShortName: String,
        val activationCode: String,
        val activationStatus: Int,
        val sn: String,
        var userName: String,
        val activationTime: String,
        var userId: String,
        val merchantId: String,
        val merchantName: String,
        val merchantShortName: String,
        val id: Long
) : Serializable


/**
 * 收银员
 */
data class CashierItem(
        val name: String,
        val phone: String,
        val role: Int,
        val userId: String
) : Serializable

/**
 * 退款密码
 */
data class RefundPwd(val storeId: String,
                     /**
                      * 退款密码校验类型：1 角色登录密码 2退款专用密码
                      */
                     val refundCheckType: Int)


/**
 * 易生打印bean
 */
data class EasyPrintBean(val type: Int, val content: String)