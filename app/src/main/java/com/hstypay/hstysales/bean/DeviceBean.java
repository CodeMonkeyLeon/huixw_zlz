package com.hstypay.hstysales.bean;

import java.io.Serializable;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.enterprise.bean
 * @创建者: Jeremy
 * @创建时间: 2019/3/13 16:36
 * @描述: ${TODO}
 */
public class DeviceBean implements Serializable {

    //界面需要的辅助字段
    private boolean isTitleView;//是否是标题
    private boolean isBluthDevice;//是否是蓝牙打印机
    private String bluthPrintName;//蓝牙打印机名称
    private boolean isSelect;//是否被选中

    private String id;
    private String categoryName;
    private String categoryCode;
    private String merchantId;
    private String storeMerchantId;
    private String sn;
    private String images;
    private String deviceId;
    private String deviceModel;
    private String model;
    private String categoryId;
    private String storeMerchantIdCnt;
    private String bindSet;
    private String userId;
    private String userIdCnt;
    private String firmCode;
    private String firmName;
    private int status;//0：待激活 1：已激活 2：已解绑 3：待绑定

    public boolean isTitleView() {
        return isTitleView;


    }

    public void setTitleView(boolean titleView) {
        isTitleView = titleView;
    }

    public boolean isBluthDevice() {
        return isBluthDevice;


    }

    public void setBluthDevice(boolean bluthDevice) {
        isBluthDevice = bluthDevice;
    }

    public String getBluthPrintName() {
        return bluthPrintName == null ? "" : bluthPrintName;


    }

    public void setBluthPrintName(String bluthPrintName) {
        this.bluthPrintName = bluthPrintName;
    }

    public boolean isSelect() {
        return isSelect;


    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getId() {
        return id == null ? "" : id;


    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName == null ? "" : categoryName;


    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryCode() {
        return categoryCode == null ? "" : categoryCode;


    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getMerchantId() {
        return merchantId == null ? "" : merchantId;


    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getStoreMerchantId() {
        return storeMerchantId == null ? "" : storeMerchantId;


    }

    public void setStoreMerchantId(String storeMerchantId) {
        this.storeMerchantId = storeMerchantId;
    }

    public String getSn() {
        return sn == null ? "" : sn;


    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getImages() {
        return images == null ? "" : images;


    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getDeviceId() {
        return deviceId == null ? "" : deviceId;


    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceModel() {
        return deviceModel == null ? "" : deviceModel;


    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getModel() {
        return model == null ? "" : model;


    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCategoryId() {
        return categoryId == null ? "" : categoryId;


    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getStoreMerchantIdCnt() {
        return storeMerchantIdCnt == null ? "" : storeMerchantIdCnt;


    }

    public void setStoreMerchantIdCnt(String storeMerchantIdCnt) {
        this.storeMerchantIdCnt = storeMerchantIdCnt;
    }

    public String getBindSet() {
        return bindSet == null ? "" : bindSet;


    }

    public void setBindSet(String bindSet) {
        this.bindSet = bindSet;
    }

    public String getUserId() {
        return userId == null ? "" : userId;


    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserIdCnt() {
        return userIdCnt == null ? "" : userIdCnt;


    }

    public void setUserIdCnt(String userIdCnt) {
        this.userIdCnt = userIdCnt;
    }

    public String getFirmCode() {
        return firmCode == null ? "" : firmCode;


    }

    public void setFirmCode(String firmCode) {
        this.firmCode = firmCode;
    }

    public String getFirmName() {
        return firmName == null ? "" : firmName;


    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public int getStatus() {
        return status;


    }

    public void setStatus(int status) {
        this.status = status;
    }
}
