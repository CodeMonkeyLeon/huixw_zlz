package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/6 12:28
 * @描述: ${TODO}
 */

public class NoticeBean {
    /**
     * data : null
     * logId : 0QqVhThv
     * error : {"args":[{}],"code":"auth.username.pass.fail","message":"账号或密码不正确"}
     * status : false
     */
    private ErrorEntity error;

    public void setError(ErrorEntity error) {
        this.error = error;
    }
    public ErrorEntity getError() {
        return error;
    }
    public class ErrorEntity {
        /**
         * args : [{}]
         * code : auth.username.pass.fail
         * message : 账号或密码不正确
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"data":[{"isRead":0,"createUserName":"超级管理员","updateTime":"2017-12-06 10:43:34","title":"测试001","readCount":2,"statusCnt":"已发布","userId":0,"content":"<p>测试001<\/p>","platform":123,"platforms":"合伙人平台/商户平台/业务员APP","createTime":"2017-12-06 10:43:34","createUser":1,"id":15,"status":1},{"isRead":0,"createUserName":"超级管理员","updateTime":"2017-12-04 14:51:23","title":"662","readCount":1,"statusCnt":"已发布","userId":0,"content":"<p>662<\/p>","platform":13,"platforms":"合伙人平台/业务员APP","createTime":"2017-12-04 14:30:04","createUser":1,"id":13,"status":1},{"isRead":0,"createUserName":"超级管理员","updateTime":"2017-12-04 14:10:06","title":"11","readCount":0,"statusCnt":"已发布","userId":0,"content":"<p>11<\/p>","platform":13,"platforms":"合伙人平台/业务员APP","createTime":"2017-12-04 14:10:06","createUser":1,"id":12,"status":1},{"isRead":0,"createUserName":"超级管理员","updateTime":"2017-12-04 14:02:12","title":"2222","readCount":1,"statusCnt":"已发布","userId":0,"content":"<p>2222<\/p>","platform":123,"platforms":"合伙人平台/商户平台/业务员APP","createTime":"2017-12-04 14:02:12","createUser":1,"id":11,"status":1}],"totalPages":0,"pageSize":15,"totalRows":0,"currentPage":1}
     * logId : vqh43PlY
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * data : [{"isRead":0,"createUserName":"超级管理员","updateTime":"2017-12-06 10:43:34","title":"测试001","readCount":2,"statusCnt":"已发布","userId":0,"content":"<p>测试001<\/p>","platform":123,"platforms":"合伙人平台/商户平台/业务员APP","createTime":"2017-12-06 10:43:34","createUser":1,"id":15,"status":1},{"isRead":0,"createUserName":"超级管理员","updateTime":"2017-12-04 14:51:23","title":"662","readCount":1,"statusCnt":"已发布","userId":0,"content":"<p>662<\/p>","platform":13,"platforms":"合伙人平台/业务员APP","createTime":"2017-12-04 14:30:04","createUser":1,"id":13,"status":1},{"isRead":0,"createUserName":"超级管理员","updateTime":"2017-12-04 14:10:06","title":"11","readCount":0,"statusCnt":"已发布","userId":0,"content":"<p>11<\/p>","platform":13,"platforms":"合伙人平台/业务员APP","createTime":"2017-12-04 14:10:06","createUser":1,"id":12,"status":1},{"isRead":0,"createUserName":"超级管理员","updateTime":"2017-12-04 14:02:12","title":"2222","readCount":1,"statusCnt":"已发布","userId":0,"content":"<p>2222<\/p>","platform":123,"platforms":"合伙人平台/商户平台/业务员APP","createTime":"2017-12-04 14:02:12","createUser":1,"id":11,"status":1}]
         * totalPages : 0
         * pageSize : 15
         * totalRows : 0
         * currentPage : 1
         */
        private List<ItemData> data;
        private int totalPages;
        private int pageSize;
        private int totalRows;
        private int currentPage;

        public void setData(List<ItemData> data) {
            this.data = data;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public List<ItemData> getData() {
            return data;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public int getPageSize() {
            return pageSize;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public class ItemData {
            /**
             * isRead : 0
             * createUserName : 超级管理员
             * updateTime : 2017-12-06 10:43:34
             * title : 测试001
             * readCount : 2
             * statusCnt : 已发布
             * userId : 0
             * content : <p>测试001</p>
             * platform : 123
             * platforms : 合伙人平台/商户平台/业务员APP
             * createTime : 2017-12-06 10:43:34
             * createUser : 1
             * id : 15
             * status : 1
             */
            private int isRead;
            private String createUserName;
            private String updateTime;
            private String title;
            private int readCount;
            private String statusCnt;
            private int userId;
            private String content;
            private int platform;
            private String platforms;
            private String createTime;
            private int createUser;
            private int id;
            private int status;

            public void setIsRead(int isRead) {
                this.isRead = isRead;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public void setReadCount(int readCount) {
                this.readCount = readCount;
            }

            public void setStatusCnt(String statusCnt) {
                this.statusCnt = statusCnt;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public void setPlatform(int platform) {
                this.platform = platform;
            }

            public void setPlatforms(String platforms) {
                this.platforms = platforms;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public void setCreateUser(int createUser) {
                this.createUser = createUser;
            }

            public void setId(int id) {
                this.id = id;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public int getIsRead() {
                return isRead;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public String getTitle() {
                return title;
            }

            public int getReadCount() {
                return readCount;
            }

            public String getStatusCnt() {
                return statusCnt;
            }

            public int getUserId() {
                return userId;
            }

            public String getContent() {
                return content;
            }

            public int getPlatform() {
                return platform;
            }

            public String getPlatforms() {
                return platforms;
            }

            public String getCreateTime() {
                return createTime;
            }

            public int getCreateUser() {
                return createUser;
            }

            public int getId() {
                return id;
            }

            public int getStatus() {
                return status;
            }
        }
    }
}
