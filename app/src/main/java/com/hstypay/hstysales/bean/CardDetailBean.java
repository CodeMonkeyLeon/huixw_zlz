package com.hstypay.hstysales.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/16 11:05
 * @描述: ${TODO}
 */

public class CardDetailBean implements Serializable {
    private ErrorBean error;

    public ErrorBean getError() {
        return error;
    }

    public void setError(ErrorBean error) {
        this.error = error;
    }

    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"accountCode":"62224242424242424242","bkCardPhoto":"/pic/mch/2017/10/26/9e5c6777-747e-40ed-9003-a6bb7180bd31.jpg","accountName":"韦小宝","city":"130300","idCard":"11204416541220213X","accountType":2,"provinceCnt":"河北省","bankIdCnt":"中国工商银行","examineStatus":1,"cityCnt":"秦皇岛市","merchantClass":1,"bankBranchId":8106,"bankId":1,"bankBranchIdCnt":"中国工商银行昌黎支行","province":"130000","examineStatusCnt":"审核通过","examineRemark":"商户业务模式不清晰;商户经营范围不符合法律法规规定;营业执照不清晰;"}
     * logId : Fpxtya4e
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity implements Serializable {
        /**
         * accountCode : 62224242424242424242
         * bkCardPhoto : /pic/mch/2017/10/26/9e5c6777-747e-40ed-9003-a6bb7180bd31.jpg
         * accountName : 韦小宝
         * city : 130300
         * idCard : 11204416541220213X
         * accountType : 2
         * provinceCnt : 河北省
         * bankIdCnt : 中国工商银行
         * examineStatus : 1
         * cityCnt : 秦皇岛市
         * merchantClass : 1
         * bankBranchId : 8106
         * bankId : 1
         * bankBranchIdCnt : 中国工商银行昌黎支行
         * province : 130000
         * examineStatusCnt : 审核通过
         * examineRemark : 商户业务模式不清晰;商户经营范围不符合法律法规规定;营业执照不清晰;
         */
        private String accountCode;
        private String bkCardPhoto;
        private String bkLicensePhoto;
        private String accountName;
        private String city;
        private String idCard;
        private int accountType;
        private String provinceCnt;
        private String bankIdCnt;
        private int examineStatus;
        private String cityCnt;
        private int merchantClass;
        private long bankBranchId;
        private long bankId;
        private String bankBranchIdCnt;
        private String province;
        private String examineStatusCnt;
        private String examineRemark;
        private String contactLine;
        private int cardType;
        private int merchantExamineStatus;
        private int activateStatus;
        private String telphone;
        private String merchantId;

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getTelphone() {
            return telphone;
        }

        public void setTelphone(String telphone) {
            this.telphone = telphone;
        }

        public int getActivateStatus() {
            return activateStatus;
        }

        public void setActivateStatus(int activateStatus) {
            this.activateStatus = activateStatus;
        }

        public String getIdCode() {
            return idCode;
        }

        public void setIdCode(String idCode) {
            this.idCode = idCode;
        }

        private String idCode;

        public int getMerchantExamineStatus(){
            return merchantExamineStatus;
        }

        public void setMerchantExamineStatus(int merchantExamineStatus){
            this.merchantExamineStatus = merchantExamineStatus;
        }

        public int getCardType(){
            return cardType;
        }

        public void setCardType(int cardType){
            this.cardType = cardType;
        }

        public String getContactLine() {
            return contactLine;
        }

        public void setContactLine(String contactLine) {
            this.contactLine = contactLine;
        }

        public String getBkLicensePhoto() {
            return bkLicensePhoto;
        }

        public void setBkLicensePhoto(String bkLicensePhoto) {
            this.bkLicensePhoto = bkLicensePhoto;
        }

        public void setAccountCode(String accountCode) {
            this.accountCode = accountCode;
        }

        public void setBkCardPhoto(String bkCardPhoto) {
            this.bkCardPhoto = bkCardPhoto;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public void setIdCard(String idCard) {
            this.idCard = idCard;
        }

        public void setAccountType(int accountType) {
            this.accountType = accountType;
        }

        public void setProvinceCnt(String provinceCnt) {
            this.provinceCnt = provinceCnt;
        }

        public void setBankIdCnt(String bankIdCnt) {
            this.bankIdCnt = bankIdCnt;
        }

        public void setExamineStatus(int examineStatus) {
            this.examineStatus = examineStatus;
        }

        public void setCityCnt(String cityCnt) {
            this.cityCnt = cityCnt;
        }

        public void setMerchantClass(int merchantClass) {
            this.merchantClass = merchantClass;
        }

        public void setBankBranchId(long bankBranchId) {
            this.bankBranchId = bankBranchId;
        }

        public void setBankId(long bankId) {
            this.bankId = bankId;
        }

        public void setBankBranchIdCnt(String bankBranchIdCnt) {
            this.bankBranchIdCnt = bankBranchIdCnt;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public void setExamineStatusCnt(String examineStatusCnt) {
            this.examineStatusCnt = examineStatusCnt;
        }

        public void setExamineRemark(String examineRemark) {
            this.examineRemark = examineRemark;
        }

        public String getAccountCode() {
            return accountCode;
        }

        public String getBkCardPhoto() {
            return bkCardPhoto;
        }

        public String getAccountName() {
            return accountName;
        }

        public String getCity() {
            return city;
        }

        public String getIdCard() {
            return idCard;
        }

        public int getAccountType() {
            return accountType;
        }

        public String getProvinceCnt() {
            return provinceCnt;
        }

        public String getBankIdCnt() {
            return bankIdCnt;
        }

        public int getExamineStatus() {
            return examineStatus;
        }

        public String getCityCnt() {
            return cityCnt;
        }

        public int getMerchantClass() {
            return merchantClass;
        }

        public long getBankBranchId() {
            return bankBranchId;
        }

        public long getBankId() {
            return bankId;
        }

        public String getBankBranchIdCnt() {
            return bankBranchIdCnt;
        }

        public String getProvince() {
            return province;
        }

        public String getExamineStatusCnt() {
            return examineStatusCnt;
        }

        public String getExamineRemark() {
            return examineRemark;
        }
    }
}
