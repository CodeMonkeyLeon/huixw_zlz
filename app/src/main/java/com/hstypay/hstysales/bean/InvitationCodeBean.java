package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/6 14:18
 * @描述: ${TODO}
 */

public class InvitationCodeBean {
    /**
     * data : null
     * logId : 0QqVhThv
     * error : {"args":[{}],"code":"auth.username.pass.fail","message":"账号或密码不正确"}
     * status : false
     */
    private ErrorEntity error;

    public void setError(ErrorEntity error) {
        this.error = error;
    }
    public ErrorEntity getError() {
        return error;
    }
    public class ErrorEntity {
        /**
         * args : [{}]
         * code : auth.username.pass.fail
         * message : 账号或密码不正确
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }
    /**
     * data : {"data":[{"salesmanId":17,"serviceProviderId":"731100000001","costRate":2600,"costRateType":0,"validStartAt":"2017-12-02 00:00:00","createUserName":"超级管理员","updateTime":"2017-12-02 11:45:37","validEndAt":"2017-12-30 23:59:59","statusCnt":"正常","enabled":true,"createTime":"2017-12-02 11:45:37","createUser":1,"maxUseCount":2,"costRateCnt":"0.26%","invitationCode":"663351","usedCount":0},{"salesmanId":17,"serviceProviderId":"889100000001","costRate":6800,"costRateType":0,"validStartAt":"2017-10-26 00:00:00","createUserName":"超级管理员","updateTime":"2017-12-04 14:21:26","validEndAt":"2017-12-30 23:59:59","statusCnt":"正常","enabled":true,"createTime":"2017-10-26 10:09:12","createUser":1,"maxUseCount":10000,"costRateCnt":"0.68%","invitationCode":"677416","usedCount":8}],"totalPages":0,"pageSize":15,"totalRows":0,"currentPage":1}
     * logId : bdb4wwC9
     * status : true
     */
    private DataEntity data;
    private String logId;
    private boolean status;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataEntity getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity {
        /**
         * data : [{"salesmanId":17,"serviceProviderId":"731100000001","costRate":2600,"costRateType":0,"validStartAt":"2017-12-02 00:00:00","createUserName":"超级管理员","updateTime":"2017-12-02 11:45:37","validEndAt":"2017-12-30 23:59:59","statusCnt":"正常","enabled":true,"createTime":"2017-12-02 11:45:37","createUser":1,"maxUseCount":2,"costRateCnt":"0.26%","invitationCode":"663351","usedCount":0},{"salesmanId":17,"serviceProviderId":"889100000001","costRate":6800,"costRateType":0,"validStartAt":"2017-10-26 00:00:00","createUserName":"超级管理员","updateTime":"2017-12-04 14:21:26","validEndAt":"2017-12-30 23:59:59","statusCnt":"正常","enabled":true,"createTime":"2017-10-26 10:09:12","createUser":1,"maxUseCount":10000,"costRateCnt":"0.68%","invitationCode":"677416","usedCount":8}]
         * totalPages : 0
         * pageSize : 15
         * totalRows : 0
         * currentPage : 1
         */
        private List<ItemData> data;
        private int totalPages;
        private int pageSize;
        private int totalRows;
        private int currentPage;

        public void setData(List<ItemData> data) {
            this.data = data;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public List<ItemData> getData() {
            return data;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public int getPageSize() {
            return pageSize;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public class ItemData {
            /**
             * salesmanId : 17
             * serviceProviderId : 731100000001
             * costRate : 2600
             * costRateType : 0
             * validStartAt : 2017-12-02 00:00:00
             * createUserName : 超级管理员
             * updateTime : 2017-12-02 11:45:37
             * validEndAt : 2017-12-30 23:59:59
             * statusCnt : 正常
             * enabled : true
             * createTime : 2017-12-02 11:45:37
             * createUser : 1
             * maxUseCount : 2
             * costRateCnt : 0.26%
             * invitationCode : 663351
             * usedCount : 0
             */
            private int salesmanId;
            private String serviceProviderId;
            private int costRate;
            private int costRateType;
            private String validStartAt;
            private String createUserName;
            private String updateTime;
            private String validEndAt;
            private String statusCnt;
            private boolean enabled;
            private String createTime;
            private int createUser;
            private int maxUseCount;
            private String costRateCnt;
            private String invitationCode;
            private int usedCount;
            private boolean isOpen;
            private String merchantCount;

            public String getMerchantCount() {
                return merchantCount;
            }

            public void setMerchantCount(String merchantCount) {
                this.merchantCount = merchantCount;
            }

            public boolean isOpen() {
                return isOpen;
            }

            public void setOpen(boolean open) {
                isOpen = open;
            }

            public void setSalesmanId(int salesmanId) {
                this.salesmanId = salesmanId;
            }

            public void setServiceProviderId(String serviceProviderId) {
                this.serviceProviderId = serviceProviderId;
            }

            public void setCostRate(int costRate) {
                this.costRate = costRate;
            }

            public void setCostRateType(int costRateType) {
                this.costRateType = costRateType;
            }

            public void setValidStartAt(String validStartAt) {
                this.validStartAt = validStartAt;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public void setValidEndAt(String validEndAt) {
                this.validEndAt = validEndAt;
            }

            public void setStatusCnt(String statusCnt) {
                this.statusCnt = statusCnt;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public void setCreateUser(int createUser) {
                this.createUser = createUser;
            }

            public void setMaxUseCount(int maxUseCount) {
                this.maxUseCount = maxUseCount;
            }

            public void setCostRateCnt(String costRateCnt) {
                this.costRateCnt = costRateCnt;
            }

            public void setInvitationCode(String invitationCode) {
                this.invitationCode = invitationCode;
            }

            public void setUsedCount(int usedCount) {
                this.usedCount = usedCount;
            }

            public int getSalesmanId() {
                return salesmanId;
            }

            public String getServiceProviderId() {
                return serviceProviderId;
            }

            public int getCostRate() {
                return costRate;
            }

            public int getCostRateType() {
                return costRateType;
            }

            public String getValidStartAt() {
                return validStartAt;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public String getValidEndAt() {
                return validEndAt;
            }

            public String getStatusCnt() {
                return statusCnt;
            }

            public boolean isEnabled() {
                return enabled;
            }

            public String getCreateTime() {
                return createTime;
            }

            public int getCreateUser() {
                return createUser;
            }

            public int getMaxUseCount() {
                return maxUseCount;
            }

            public String getCostRateCnt() {
                return costRateCnt;
            }

            public String getInvitationCode() {
                return invitationCode;
            }

            public int getUsedCount() {
                return usedCount;
            }
        }
    }
}
