package com.hstypay.hstysales.bean;

import java.util.List;

//排名列表数据
public class RankListBean extends BaseBean<RankListBean.Data>{
    public static  class Data{
        private int pageSize;
        private int currentPage;
        private int totalPages;
        private int totalRows;
        private List<RankListBean.SummaryData> data;

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public int getTotalRows() {
            return totalRows;
        }

        public void setTotalRows(int totalRows) {
            this.totalRows = totalRows;
        }

        public List<SummaryData> getData() {
            return data;
        }

        public void setData(List<SummaryData> data) {
            this.data = data;
        }
    }

    public static class SummaryData{
        private String telphone;
        private String principal;
        private String merchantName;
        private String merchantRegisterTime;
        private double totalFee;
        private int billCount;

        public String getTelphone() {
            return telphone == null ? "" : telphone;


        }

        public void setTelphone(String telphone) {
            this.telphone = telphone;
        }

        public String getPrincipal() {
            return principal == null ? "" : principal;


        }

        public void setPrincipal(String principal) {
            this.principal = principal;
        }

        public String getMerchantName() {
            return merchantName == null ? "" : merchantName;


        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public String getMerchantRegisterTime() {
            return merchantRegisterTime == null ? "" : merchantRegisterTime;


        }

        public void setMerchantRegisterTime(String merchantRegisterTime) {
            this.merchantRegisterTime = merchantRegisterTime;
        }

        public double getTotalFee() {
            return totalFee;


        }

        public void setTotalFee(double totalFee) {
            this.totalFee = totalFee;
        }

        public int getBillCount() {
            return billCount;


        }

        public void setBillCount(int billCount) {
            this.billCount = billCount;
        }
    }

}
