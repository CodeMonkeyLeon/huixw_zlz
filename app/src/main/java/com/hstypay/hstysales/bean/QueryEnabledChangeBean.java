package com.hstypay.hstysales.bean;

/**
 * @author zeyu.kuang
 * @time 2020/11/6
 * @desc
 */
public class QueryEnabledChangeBean extends BaseBean<QueryEnabledChangeBean.Data>{

    public class Data{
        boolean editMchBasic;//商户基础信息+法人信息；true-允许修改；false-不允许修改
        boolean editMchAccount;//商户账户信息（结算信息）；true-允许修改；false-不允许修改
        boolean editMchCredentials;//商户证件信息；true-允许修改；false-不允许修改

        public boolean isEditMchBasic() {
            return editMchBasic;
        }

        public void setEditMchBasic(boolean editMchBasic) {
            this.editMchBasic = editMchBasic;
        }

        public boolean isEditMchAccount() {
            return editMchAccount;
        }

        public void setEditMchAccount(boolean editMchAccount) {
            this.editMchAccount = editMchAccount;
        }

        public boolean isEditMchCredentials() {
            return editMchCredentials;
        }

        public void setEditMchCredentials(boolean editMchCredentials) {
            this.editMchCredentials = editMchCredentials;
        }
    }
}
