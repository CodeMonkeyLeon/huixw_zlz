package com.hstypay.hstysales.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 2017/7/29.
 */

public class StoreListBean implements Serializable{
    /**
     * data : [{"storeName":"测试店","storeId":"661200010002"},{"storeName":"测试店","storeId":"661200010302"},{"storeName":"测试店","storeId":"661209510302"},{"storeName":"测试","storeId":"661300000001"},{"storeName":"测试店","storeId":"661300010002"},{"storeName":"门店2","storeId":"535300000001"},{"storeName":"店2","storeId":"535300010002"},{"storeName":"店2","storeId":"535302010002"},{"storeName":"店2","storeId":"535302310002"},{"storeName":"店2","storeId":"5353083510002"},{"storeName":"门店1","storeId":"371300000002"},{"storeName":"店1","storeId":"371300001003"},{"storeName":"店1","storeId":"371302001003"},{"storeName":"店1","storeId":"371302031003"},{"storeName":"店1","storeId":"371302037003"}]
     * logId : ML7qKgVI
     * status : true
     */
    public StoreListBean(){}
    private ErrorBean error;
    public ErrorBean getError() {
        return error;
    }
    public void setError(ErrorBean error) {
        this.error = error;
    }
    public static class ErrorBean {
        /**
         * code : client.user.pwd.incorrect
         * message : 用户名或者密码输入不正确
         * args : []
         */

        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }

    private List<DataEntity> data;
    private String logId;
    private boolean status;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public String getLogId() {
        return logId;
    }

    public boolean isStatus() {
        return status;
    }

    public class DataEntity implements Serializable {
        /**
         * storeName : 测试店
         * storeId : 661200010002
         */
        private String storeName;
        private String storeId;
        private int examineStatus;

        public int getExamineStatus() {
            return examineStatus;
        }

        public void setExamineStatus(int examineStatus) {
            this.examineStatus = examineStatus;
        }

        public DataEntity(){}

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public void setStoreId(String storeId) {
            this.storeId = storeId;
        }

        public String getStoreName() {
            return storeName;
        }

        public String getStoreId() {
            return storeId;
        }
    }
}
