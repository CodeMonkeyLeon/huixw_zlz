package com.hstypay.hstysales.bean;

/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/10/27 17:02
 * @描述: ${TODO}
 */

public class ProgressBean {
    private long downSize;
    private long totalSize;

    public long getDownSize() {
        return downSize;
    }

    public void setDownSize(long downSize) {
        this.downSize = downSize;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }
}
