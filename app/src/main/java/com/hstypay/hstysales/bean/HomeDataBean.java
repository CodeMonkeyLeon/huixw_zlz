package com.hstypay.hstysales.bean;

import java.util.List;

/**
 * @项目名: HstySales
 * @包名: com.hstypay.hstysales.bean
 * @创建者: Jeremy
 * @创建时间: 2017/12/4 10:46
 * @描述: ${TODO}
 */

public class HomeDataBean {
    /**
     * data : null
     * logId : 0QqVhThv
     * error : {"args":[{}],"code":"auth.username.pass.fail","message":"账号或密码不正确"}
     * status : false
     */
    private ErrorEntity error;
    /**
     * logId : bWvjGu2Q
     * status : true
     * data : {"myEarnings":0,"myEarningsYesterday":0,"payNetAmount":0,"payNetAmountYesterday":0,"mchTotal":{"canTradeMchTotal":0,"examingMchTotal":0,"examinePassMchTotal":0,"examineFailMchTotal":0,"changeExamingMchTotal":4,"changeExaminePassMchTotal":0,"changeExamineFailMchTotal":0,"freezeMchTotal":0,"initMchTotal":0,"invalidMchTotal":0}}
     */


    public void setError(ErrorEntity error) {
        this.error = error;
    }
    public ErrorEntity getError() {
        return error;
    }





    public class ErrorEntity {
        /**
         * args : [{}]
         * code : auth.username.pass.fail
         * message : 账号或密码不正确
         */
        private String code;
        private String message;
        private List<?> args;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<?> getArgs() {
            return args;
        }

        public void setArgs(List<?> args) {
            this.args = args;
        }
    }







    private String logId;
    private boolean status;
    private DataBean data;
    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * myEarnings : 0
         * myEarningsYesterday : 0
         * payNetAmount : 0
         * payNetAmountYesterday : 0
         * mchTotal : {"canTradeMchTotal":0,"examingMchTotal":0,"examinePassMchTotal":0,"examineFailMchTotal":0,"changeExamingMchTotal":4,"changeExaminePassMchTotal":0,"changeExamineFailMchTotal":0,"freezeMchTotal":0,"initMchTotal":0,"invalidMchTotal":0}
         */

        private double myEarnings;//累计收益
        private double myEarningsYesterday;//昨日收益
        private double payNetAmount;
        private double payNetAmountYesterday;
        private double myEarningsToday;//今日收益
        private long svcOpenApplyCount;//商户应用开通申请条数
        private MchTotalBean mchTotal;

        public long getSvcOpenApplyCount() {
            return svcOpenApplyCount;
        }

        public void setSvcOpenApplyCount(long svcOpenApplyCount) {
            this.svcOpenApplyCount = svcOpenApplyCount;
        }

        public double getMyEarningsToday() {
            return myEarningsToday;
        }

        public void setMyEarningsToday(double myEarningsToday) {
            this.myEarningsToday = myEarningsToday;
        }

        public double getMyEarnings() {
            return myEarnings;
        }

        public void setMyEarnings(double myEarnings) {
            this.myEarnings = myEarnings;
        }

        public double getMyEarningsYesterday() {
            return myEarningsYesterday;
        }

        public void setMyEarningsYesterday(double myEarningsYesterday) {
            this.myEarningsYesterday = myEarningsYesterday;
        }
        public void setPayNetAmount(double payNetAmount) {
            this.payNetAmount = payNetAmount;
        }
        public double getPayNetAmount() {
            return payNetAmount;
        }

        public void setPayNetAmount(int payNetAmount) {
            this.payNetAmount = payNetAmount;
        }

        public double getPayNetAmountYesterday() {
            return payNetAmountYesterday;
        }

        public void setPayNetAmountYesterday(double payNetAmountYesterday) {
            this.payNetAmountYesterday = payNetAmountYesterday;
        }

        public MchTotalBean getMchTotal() {
            return mchTotal;
        }

        public void setMchTotal(MchTotalBean mchTotal) {
            this.mchTotal = mchTotal;
        }

        public static class MchTotalBean {
            /**
             * canTradeMchTotal : 0
             * examingMchTotal : 0
             * examinePassMchTotal : 0
             * examineFailMchTotal : 0
             * changeExamingMchTotal : 4
             * changeExaminePassMchTotal : 0
             * changeExamineFailMchTotal : 0
             * freezeMchTotal : 0
             * initMchTotal : 0
             * invalidMchTotal : 0
             */

            private int canTradeMchTotal;
            private int examingMchTotal;
            private int examinePassMchTotal;
            private int examineFailMchTotal;
            private int changeExamingMchTotal;
            private int changeExaminePassMchTotal;
            private int changeExamineFailMchTotal;
            private int freezeMchTotal;
            private int initMchTotal;
            private int invalidMchTotal;
            private int noConfirmMchTotal;

            public int getCanTradeMchTotal() {
                return canTradeMchTotal;
            }

            public void setCanTradeMchTotal(int canTradeMchTotal) {
                this.canTradeMchTotal = canTradeMchTotal;
            }

            public int getExamingMchTotal() {
                return examingMchTotal;
            }

            public void setExamingMchTotal(int examingMchTotal) {
                this.examingMchTotal = examingMchTotal;
            }

            public int getExaminePassMchTotal() {
                return examinePassMchTotal;
            }

            public void setExaminePassMchTotal(int examinePassMchTotal) {
                this.examinePassMchTotal = examinePassMchTotal;
            }

            public int getExamineFailMchTotal() {
                return examineFailMchTotal;
            }

            public void setExamineFailMchTotal(int examineFailMchTotal) {
                this.examineFailMchTotal = examineFailMchTotal;
            }

            public int getChangeExamingMchTotal() {
                return changeExamingMchTotal;
            }

            public void setChangeExamingMchTotal(int changeExamingMchTotal) {
                this.changeExamingMchTotal = changeExamingMchTotal;
            }

            public int getChangeExaminePassMchTotal() {
                return changeExaminePassMchTotal;
            }

            public void setChangeExaminePassMchTotal(int changeExaminePassMchTotal) {
                this.changeExaminePassMchTotal = changeExaminePassMchTotal;
            }

            public int getChangeExamineFailMchTotal() {
                return changeExamineFailMchTotal;
            }

            public void setChangeExamineFailMchTotal(int changeExamineFailMchTotal) {
                this.changeExamineFailMchTotal = changeExamineFailMchTotal;
            }

            public int getFreezeMchTotal() {
                return freezeMchTotal;
            }

            public void setFreezeMchTotal(int freezeMchTotal) {
                this.freezeMchTotal = freezeMchTotal;
            }

            public int getInitMchTotal() {
                return initMchTotal;
            }

            public void setInitMchTotal(int initMchTotal) {
                this.initMchTotal = initMchTotal;
            }

            public int getInvalidMchTotal() {
                return invalidMchTotal;
            }

            public void setInvalidMchTotal(int invalidMchTotal) {
                this.invalidMchTotal = invalidMchTotal;
            }

            public int getNoConfirmMchTotal() {
                return noConfirmMchTotal;
            }

            public void setNoConfirmMchTotal(int noConfirmMchTotal) {
                this.noConfirmMchTotal = noConfirmMchTotal;
            }
        }
    }

}
