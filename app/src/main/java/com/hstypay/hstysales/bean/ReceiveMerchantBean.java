package com.hstypay.hstysales.bean;

/**
 * @author zeyu.kuang
 * @time 2020/9/8
 * @desc
 */
public class ReceiveMerchantBean {
    private int mchQueryStatus;
    private String merchantId;
    private String billNo;

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public int getMchQueryStatus() {
        return mchQueryStatus;
    }

    public void setMchQueryStatus(int mchQueryStatus) {
        this.mchQueryStatus = mchQueryStatus;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}
