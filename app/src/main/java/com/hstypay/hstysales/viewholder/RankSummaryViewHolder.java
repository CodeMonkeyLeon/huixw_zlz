package com.hstypay.hstysales.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.RankSumaryBean;
import com.hstypay.hstysales.utils.ArrowAnimationUtil;
import com.hstypay.hstysales.utils.DateUtil;
import com.hstypay.hstysales.utils.StringUtils;

public class RankSummaryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final LinearLayout mLlDateRankItem;//头部日期选择
    private final TextView mTvDateRankItem;//头部日期展示
    private final ImageView mIvArrowDateItem;//头部日期箭头
    private final TextView mTvAmountShRankItem;//tv实收金额
    private final TextView mTvCountTradeRankItem;//tv交易笔数
    private final TextView mTvKdjDbsRankItem;//tv单客价，大额笔数
    private final ImageView mIvQuestionDkjRankItem;//单客价问号
    private final LinearLayout mLlExpandMerchantItem;//商户数展开
    private final ImageView mIvArrowExpandMerchantItem;//商户数展开箭头
    private final LinearLayout mLlExpandAfterMerchantItem;//展开后的商户数
    private final TextView mTvTotalMerchantNumItem;//tv商户总数
    private final LinearLayout mLlQuestionHyshItem;//活跃商户问号
    private final TextView mTvNumHyshRankItem;//活跃商户数量
    private final LinearLayout mLlQuestionNotHyshItem;//非活跃商户问号
    private final TextView mTvNumNotHyshItem;//非活跃商户数量

    private final LinearLayout mLlClickJyjeRankItem;//交易净值点击
    private final TextView mTvClickJyjeRankItem;//交易净值文字
    private final ImageView mIvClickJyjeRankItem;//交易净值排序图标

    private final LinearLayout mLlClickJybsRankItem;//交易笔数点击
    private final TextView mTvClickJybsRankItem;//交易笔数文字
    private final ImageView mIvClickJybsRankItem;//交易笔数排序图标

    private final LinearLayout mLlClickCjsjRankItem;//创建时间点击
    private final TextView mTvClickCjsjRankItem;//创建时间文字
    private final ImageView mIvClickCjsjRankItem;//创建时间排序图标

    private Context mContext;

    private boolean isHeaderDateOpen;//头部日期是否展开
    private boolean isHeaderMerchantOpen;//商户统计数据是否展开

    private boolean isJyjeSelected = true;//交易净额是否选中
    private ArrowType jyjeArrowType = ArrowType.ARROWUP;//交易净额的箭头被选中向上

    private boolean isJybsSelected = false;//交易笔数是否选中
    private ArrowType jybsArrowType = ArrowType.ARROWNOTSELECT;//交易笔数的箭头没被选中

    private boolean isCjsjSelected = false;//创建时间是否选中
    private ArrowType cjsjArrowType = ArrowType.ARROWNOTSELECT;//创建时间的箭头没被选中

    public enum  ArrowType{
        //选中向上箭头
        ARROWUP,
        //选中向下箭头
        ARROWDOWN,
        //没有箭头被选中
        ARROWNOTSELECT
    }

    public RankSummaryViewHolder(@NonNull View itemView, Context context) {
        super(itemView);
        mContext = context;
        mLlDateRankItem = itemView.findViewById(R.id.ll_date_rank_item);//头部日期选择
        mTvDateRankItem = itemView.findViewById(R.id.tv_date_rank_item);//头部日期展示
        mIvArrowDateItem = itemView.findViewById(R.id.iv_arrow_date_item);//头部日期箭头
        mTvAmountShRankItem = itemView.findViewById(R.id.tv_amount_sh_rank_item);//tv实收金额
        mTvCountTradeRankItem = itemView.findViewById(R.id.tv_count_trade_rank_item);//tv交易笔数
        mTvKdjDbsRankItem = itemView.findViewById(R.id.tv_kdj_dbs_rank_item);//tv单客价，大额笔数
        mIvQuestionDkjRankItem = itemView.findViewById(R.id.iv_question_dkj_rank_item);//单客价问号
        mLlExpandMerchantItem = itemView.findViewById(R.id.ll_expand_merchant_item);//商户数展开
        mIvArrowExpandMerchantItem = itemView.findViewById(R.id.iv_arrow_expand_merchant_item);//商户数展开箭头
        mLlExpandAfterMerchantItem = itemView.findViewById(R.id.ll_expand_after_merchant_item);//展开后的商户数
        mTvTotalMerchantNumItem = itemView.findViewById(R.id.tv_total_merchant_num_item);//tv商户总数
        mLlQuestionHyshItem = itemView.findViewById(R.id.ll_question_hysh_item);//活跃商户问号
        mTvNumHyshRankItem = itemView.findViewById(R.id.tv_num_hysh_rank_item);//活跃商户数量
        mLlQuestionNotHyshItem = itemView.findViewById(R.id.ll_question_not_hysh_item);//非活跃商户问号
        mTvNumNotHyshItem = itemView.findViewById(R.id.tv_num_not_hysh_item);//非活跃商户数量
        mLlClickJyjeRankItem = itemView.findViewById(R.id.ll_click_jyje_rank_item);//交易净值点击
        mTvClickJyjeRankItem = itemView.findViewById(R.id.tv_click_jyje_rank_item);//交易净值文字
        mIvClickJyjeRankItem = itemView.findViewById(R.id.iv_click_jyje_rank_item);//交易净值排序图标
        mLlClickJybsRankItem = itemView.findViewById(R.id.ll_click_jybs_rank_item);//交易笔数点击
        mTvClickJybsRankItem = itemView.findViewById(R.id.tv_click_jybs_rank_item);//交易笔数文字
        mIvClickJybsRankItem = itemView.findViewById(R.id.iv_click_jybs_rank_item);//交易笔数排序图标
        mLlClickCjsjRankItem = itemView.findViewById(R.id.ll_click_cjsj_rank_item);//创建时间点击
        mTvClickCjsjRankItem = itemView.findViewById(R.id.tv_click_cjsj_rank_item);//创建时间文字
        mIvClickCjsjRankItem = itemView.findViewById(R.id.iv_click_cjsj_rank_item);//创建时间排序图标

        mLlDateRankItem.setOnClickListener(this);
        mIvQuestionDkjRankItem.setOnClickListener(this);
        mLlExpandMerchantItem.setOnClickListener(this);
        mLlQuestionHyshItem.setOnClickListener(this);
        mLlQuestionNotHyshItem.setOnClickListener(this);
        mLlClickJyjeRankItem.setOnClickListener(this);
        mLlClickJybsRankItem.setOnClickListener(this);
        mLlClickCjsjRankItem.setOnClickListener(this);
    }

    public void setData(RankSumaryBean.RankSumaryData rankSummaryData) {
        if (rankSummaryData!=null){
            mTvDateRankItem.setText(rankSummaryData.getDate());
            mTvAmountShRankItem.setText(DateUtil.formatMoneyUtil((rankSummaryData.getActualMoney()) / 100d));
            mTvCountTradeRankItem.setText(""+rankSummaryData.getSuccessCount());
            String unitPrice = DateUtil.formatMoneyUtil((rankSummaryData.getUnitPrice()) / 100d);
            int bigTradeCount = rankSummaryData.getBigTradeCount();
            mTvKdjDbsRankItem.setText("客单价¥"+unitPrice+"，大额笔数"+bigTradeCount+"笔");
            mTvTotalMerchantNumItem.setText(""+rankSummaryData.getTotalMerchantCount());
            mTvNumHyshRankItem.setText(""+rankSummaryData.getActivityMerchantCount());
            mTvNumNotHyshItem.setText(""+rankSummaryData.getNoActivityMerchantCount());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_date_rank_item:
                //头部日期选择
                if (isHeaderDateOpen){
                    //是打开则关闭
                    ArrowAnimationUtil.closeArrow(mIvArrowDateItem,mContext);
                }else {
                    //是关闭则打开
                    ArrowAnimationUtil.openArrow(mIvArrowDateItem,mContext);
                }
                if (mOnRankHeaderContentClickListener!=null){
                    mOnRankHeaderContentClickListener.onClickHeaderSelectDate(isHeaderDateOpen, new CallBackRankHeaderClick() {
                        @Override
                        public void backSelectDate(String date) {
                            if (!StringUtils.isEmptyOrNull(date)){
                                mTvDateRankItem.setText(date);
                            }
                            ArrowAnimationUtil.closeArrow(mIvArrowDateItem,mContext);
                            isHeaderDateOpen = false;
                        }
                    });
                }
                isHeaderDateOpen = !isHeaderDateOpen;
                break;
            case R.id.iv_question_dkj_rank_item:
                //单客价问号
                if (mOnRankHeaderContentClickListener!=null){
                    mOnRankHeaderContentClickListener.onClickDkjQuestion();
                }
                break;
            case R.id.ll_expand_merchant_item:
                //展开商户数
                if (isHeaderMerchantOpen){
                    //是展开的就关闭
                    ArrowAnimationUtil.closeArrow(mIvArrowExpandMerchantItem,mContext);
                    mLlExpandAfterMerchantItem.setVisibility(View.GONE);
                }else {
                    //是关闭的就展开
                    ArrowAnimationUtil.openArrow(mIvArrowExpandMerchantItem,mContext);
                    mLlExpandAfterMerchantItem.setVisibility(View.VISIBLE);
                }
                if (mOnRankHeaderContentClickListener!=null){
                    mOnRankHeaderContentClickListener.onClickExpandMerchant();
                }
                isHeaderMerchantOpen = !isHeaderMerchantOpen;
                break;
            case R.id.ll_question_hysh_item:
                //活跃商户问号
                if (mOnRankHeaderContentClickListener!=null){
                    mOnRankHeaderContentClickListener.onClickHyshQuestion();
                }
                break;
            case R.id.ll_question_not_hysh_item:
                //非活跃商户问号
                if (mOnRankHeaderContentClickListener!=null){
                    mOnRankHeaderContentClickListener.onClickNotHyshQuestion();
                }
                break;
            case R.id.ll_click_jyje_rank_item:
                //交易净值点击
                if (isJyjeSelected){
                    //当前交易净值是被选中的，则判断箭头是向上还是向下
                    if (jyjeArrowType == ArrowType.ARROWUP){
                        //箭头是向上的，则改变成向下的
                        mIvClickJyjeRankItem.setImageResource(R.mipmap.ic_up_down_down);
                        jyjeArrowType = ArrowType.ARROWDOWN;
                        isJyjeSelected = true;
                    }else {
                        //箭头是向下的，则改变成向上的
                        mIvClickJyjeRankItem.setImageResource(R.mipmap.ic_up_down_up);
                        jyjeArrowType = ArrowType.ARROWUP;
                        isJyjeSelected = true;
                    }
                }else {
                    //当前交易净值是未被选中，则选中并默认箭头向上
                    mTvClickJyjeRankItem.setTextColor(mContext.getResources().getColor(R.color.home_blue_text));
                    mIvClickJyjeRankItem.setImageResource(R.mipmap.ic_up_down_up);
                    jyjeArrowType = ArrowType.ARROWUP;
                    isJyjeSelected = true;
                }

                //使另外两个(交易笔数和创建时间)的箭头变成未选中
                mTvClickJybsRankItem.setTextColor(mContext.getResources().getColor(R.color.color_ff999999));
                mIvClickJybsRankItem.setImageResource(R.mipmap.ic_up_down_default);
                isJybsSelected = false;
                jybsArrowType  = ArrowType.ARROWNOTSELECT;

                mTvClickCjsjRankItem.setTextColor(mContext.getResources().getColor(R.color.color_ff999999));
                mIvClickCjsjRankItem.setImageResource(R.mipmap.ic_up_down_default);
                isCjsjSelected = false;
                cjsjArrowType = ArrowType.ARROWNOTSELECT;

                //回调
                if (mOnRankHeaderContentClickListener!=null){
                    mOnRankHeaderContentClickListener.onClickJyjeRank(jyjeArrowType);
                }
                break;
            case R.id.ll_click_jybs_rank_item:
                //交易笔数点击
                if (isJybsSelected){
                    //当前交易净值是被选中的，则判断箭头是向上还是向下
                    if (jybsArrowType == ArrowType.ARROWUP){
                        //箭头是向上的，则改变成向下的
                        mIvClickJybsRankItem.setImageResource(R.mipmap.ic_up_down_down);
                        jybsArrowType = ArrowType.ARROWDOWN;
                        isJybsSelected = true;
                    }else {
                        //箭头是向下的，则改变成向上的
                        mIvClickJybsRankItem.setImageResource(R.mipmap.ic_up_down_up);
                        jybsArrowType = ArrowType.ARROWUP;
                        isJybsSelected = true;
                    }
                }else {
                    //当前交易净值是未被选中，则选中并默认箭头向上
                    mTvClickJybsRankItem.setTextColor(mContext.getResources().getColor(R.color.home_blue_text));
                    mIvClickJybsRankItem.setImageResource(R.mipmap.ic_up_down_up);
                    jybsArrowType = ArrowType.ARROWUP;
                    isJybsSelected = true;
                }

                //使另外两个（交易净额和创建时间）的文字及箭头变成未选中
                mTvClickJyjeRankItem.setTextColor(mContext.getResources().getColor(R.color.color_ff999999));
                mIvClickJyjeRankItem.setImageResource(R.mipmap.ic_up_down_default);
                isJyjeSelected = false;
                jyjeArrowType  = ArrowType.ARROWNOTSELECT;

                mTvClickCjsjRankItem.setTextColor(mContext.getResources().getColor(R.color.color_ff999999));
                mIvClickCjsjRankItem.setImageResource(R.mipmap.ic_up_down_default);
                isCjsjSelected = false;
                cjsjArrowType = ArrowType.ARROWNOTSELECT;

                //回调
                if (mOnRankHeaderContentClickListener!=null){
                    mOnRankHeaderContentClickListener.onClickJybsRank(jybsArrowType);
                }
                break;
            case R.id.ll_click_cjsj_rank_item:
                //创建时间点击
                if (isCjsjSelected){
                    //当前交易净值是被选中的，则判断箭头是向上还是向下
                    if (cjsjArrowType == ArrowType.ARROWUP){
                        //箭头是向上的，则改变成向下的
                        mIvClickCjsjRankItem.setImageResource(R.mipmap.ic_up_down_down);
                        cjsjArrowType = ArrowType.ARROWDOWN;
                        isCjsjSelected = true;
                    }else {
                        //箭头是向下的，则改变成向上的
                        mIvClickCjsjRankItem.setImageResource(R.mipmap.ic_up_down_up);
                        cjsjArrowType = ArrowType.ARROWUP;
                        isCjsjSelected = true;
                    }
                }else {
                    //当前交易净值是未被选中，则选中并默认箭头向上
                    mTvClickCjsjRankItem.setTextColor(mContext.getResources().getColor(R.color.home_blue_text));
                    mIvClickCjsjRankItem.setImageResource(R.mipmap.ic_up_down_up);
                    cjsjArrowType = ArrowType.ARROWUP;
                    isCjsjSelected = true;
                }

                //使另外两个（交易净额和交易笔数）的文字及箭头变成未选中
                mTvClickJyjeRankItem.setTextColor(mContext.getResources().getColor(R.color.color_ff999999));
                mIvClickJyjeRankItem.setImageResource(R.mipmap.ic_up_down_default);
                isJyjeSelected = false;
                jyjeArrowType  = ArrowType.ARROWNOTSELECT;

                mTvClickJybsRankItem.setTextColor(mContext.getResources().getColor(R.color.color_ff999999));
                mIvClickJybsRankItem.setImageResource(R.mipmap.ic_up_down_default);
                isJybsSelected = false;
                jybsArrowType = ArrowType.ARROWNOTSELECT;

                //回调
                if (mOnRankHeaderContentClickListener!=null){
                    mOnRankHeaderContentClickListener.onClickCjsjRank(cjsjArrowType);
                }
                break;
            default:
                break;
        }
    }

    public interface OnRankHeaderContentClickListener{
        void onClickHeaderSelectDate(boolean isHeaderDateExpand, CallBackRankHeaderClick callBackRankHeaderClick);//头部日期选择
        void onClickDkjQuestion(); //单客价问号
        void onClickExpandMerchant();//展开商户数
        void onClickHyshQuestion();//活跃商户问号
        void onClickNotHyshQuestion();//非活跃商户问号
        void onClickJyjeRank(ArrowType arrowType);//交易净值点击,arrowType箭头的方向
        void onClickJybsRank(ArrowType arrowType);//交易笔数点击
        void onClickCjsjRank(ArrowType arrowType);//创建时间点击
        void onClickTelPhone(String telPhone);
    }

    public OnRankHeaderContentClickListener mOnRankHeaderContentClickListener;

    public void setOnRankHeaderContentClickListener(OnRankHeaderContentClickListener onRankHeaderContentClickListener) {
        mOnRankHeaderContentClickListener = onRankHeaderContentClickListener;
    }

    public interface CallBackRankHeaderClick{
        void backSelectDate(String date);
    }
}
