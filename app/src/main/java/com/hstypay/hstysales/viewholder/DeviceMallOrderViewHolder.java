package com.hstypay.hstysales.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.DeviceOrderListBean;
import com.hstypay.hstysales.utils.Constants;

import java.util.List;

/**
 * @author zeyu.kuang
 * @time 2020/8/24
 * @desc 硬件商城的viewHolder
 */
public class DeviceMallOrderViewHolder extends RecyclerView.ViewHolder {

    public final RelativeLayout mRlOrderMessItemMall;//订单信息
    public final TextView mTvCopyOrderNumMall;//复制
    public final TextView mTvOrderNumMall;//订单号
    public final RelativeLayout mRlOrderStateItemMall;//当前状态
    public final TextView mTvStateDesMall;//状态描述
    public final TextView mTvStateBtnMall;//状态按钮(派单)
    public final RelativeLayout mRlOrderTimeItemAll;//rl下单时间
    public final TextView mTvShowCommitTimeOrderMall;//下单时间显示
    public final RelativeLayout mRlOrderPersonItemMall;//rl派送员
    public final TextView mTvNamePersonOrderMall;//派送员名称
    public final RelativeLayout mRlOrderMerchantItemMall;//rl商户
    public final TextView mTvMerchantNameMall;//商户名称
    public final RelativeLayout mRlOrderProductItemMall;//rl商品
    public final TextView mTvProductNameMall;//商品名称
    public final TextView mTvSureArriveMall;//确认送达

    public DeviceMallOrderViewHolder(@NonNull View itemView) {
        super(itemView);
        mRlOrderMessItemMall = itemView.findViewById(R.id.rl_order_mess_item_mall);
        mTvCopyOrderNumMall = itemView.findViewById(R.id.tv_copy_order_num_mall);
        mTvOrderNumMall = itemView.findViewById(R.id.tv_order_num_mall);
        mRlOrderStateItemMall = itemView.findViewById(R.id.rl_order_state_item_mall);
        mTvStateDesMall = itemView.findViewById(R.id.tv_state_des_mall);
        mTvStateBtnMall = itemView.findViewById(R.id.tv_state_btn_mall);
        mRlOrderTimeItemAll = itemView.findViewById(R.id.rl_order_time_item_all);
        mTvShowCommitTimeOrderMall = itemView.findViewById(R.id.tv_show_commit_time_order_mall);
        mRlOrderPersonItemMall = itemView.findViewById(R.id.rl_order_person_item_mall);
        mTvNamePersonOrderMall = itemView.findViewById(R.id.tv_name_person_order_mall);
        mRlOrderMerchantItemMall = itemView.findViewById(R.id.rl_order_merchant_item_mall);
        mTvMerchantNameMall = itemView.findViewById(R.id.tv_merchant_name_mall);
        mRlOrderProductItemMall = itemView.findViewById(R.id.rl_order_product_item_mall);
        mTvProductNameMall = itemView.findViewById(R.id.tv_product_name_mall);
        mTvSureArriveMall = itemView.findViewById(R.id.tv_sure_arrive_mall);
    }

    public void setData(final DeviceOrderListBean.DataEntity.DataBean ordersBean) {
        if (ordersBean==null)return;
        String orderNo = ordersBean.getOrderNo();
        if (!TextUtils.isEmpty(orderNo)){
            mTvOrderNumMall.setText(orderNo);
            mTvCopyOrderNumMall.setVisibility(View.VISIBLE);
            mTvCopyOrderNumMall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnDeviceMallItemBtnClickListener!=null){
                        mOnDeviceMallItemBtnClickListener.onCopyOrderNum(ordersBean);
                    }
                }
            });
        }else {
            mTvOrderNumMall.setText("");
            mTvCopyOrderNumMall.setVisibility(View.GONE);
        }

        String tradeStatusDesc = ordersBean.getTradeStatusDesc();
        if (!TextUtils.isEmpty(tradeStatusDesc)){
            mTvStateDesMall.setText(tradeStatusDesc);
        }else {
            mTvStateDesMall.setText("");
        }

        String tradeTime = ordersBean.getTradeTime();
        if (!TextUtils.isEmpty(tradeTime)){
            /*SimpleDateFormat dateFormat = new SimpleDateFormat("MM月dd HH:mm");
            String tradeTimes = dateFormat.format(new Date(tradeTime));*/
            mTvShowCommitTimeOrderMall.setText(tradeTime);
        }else {
            mTvShowCommitTimeOrderMall.setText("");
        }

        String sendSalesmanName = ordersBean.getSendSalesmanName();
        if (!TextUtils.isEmpty(sendSalesmanName)){
            mTvNamePersonOrderMall.setText(sendSalesmanName);
        }else {
            mTvNamePersonOrderMall.setText("");
        }

        String merchantName = ordersBean.getMerchantName();
        if (!TextUtils.isEmpty(merchantName)){
            mTvMerchantNameMall.setText(merchantName);
        }else {
            mTvMerchantNameMall.setText("");
        }

        List<DeviceOrderListBean.DataEntity.DataBean.GoodsDTOListBean> goodsDTOList = ordersBean.getGoodsDTOList();
        if (goodsDTOList!=null && goodsDTOList.size()>0){
            DeviceOrderListBean.DataEntity.DataBean.GoodsDTOListBean goodsDTOListBean = goodsDTOList.get(0);
            if (!TextUtils.isEmpty(goodsDTOListBean.getDeviceModel()) && !TextUtils.isEmpty(goodsDTOListBean.getQuantity())){
                mTvProductNameMall.setText(goodsDTOListBean.getDeviceModel()+"  x"+goodsDTOListBean.getQuantity());
            }else {
                mTvProductNameMall.setText("");
            }
        }else {
            mTvProductNameMall.setText("");
        }

        int tradeStatus = ordersBean.getTradeStatus();
        switch (tradeStatus){
            case Constants.DEVICE_MALL_WAIT_POST:
                //待发货
                mTvStateBtnMall.setVisibility(View.VISIBLE);
                mRlOrderPersonItemMall.setVisibility(View.GONE);
                mTvSureArriveMall.setVisibility(View.GONE);
                mTvStateBtnMall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnDeviceMallItemBtnClickListener!=null){
                            mOnDeviceMallItemBtnClickListener.onPostGoods(ordersBean);
                        }
                    }
                });
                break;
            case Constants.DEVICE_MALL_WAIT_SEND:
                //待收货
                mTvStateBtnMall.setVisibility(View.GONE);
                mRlOrderPersonItemMall.setVisibility(View.VISIBLE);
                mTvSureArriveMall.setVisibility(View.VISIBLE);
                mTvSureArriveMall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnDeviceMallItemBtnClickListener!=null){
                            mOnDeviceMallItemBtnClickListener.onSureArrived(ordersBean);
                        }
                    }
                });
                break;
            case Constants.DEVICE_MALL_WAIT_FINISH:
                //已完成
            case Constants.DEVICE_MALL_WAIT_ClOSE:
                //已关闭
                mTvStateBtnMall.setVisibility(View.GONE);
                mRlOrderPersonItemMall.setVisibility(View.VISIBLE);
                mTvSureArriveMall.setVisibility(View.GONE);
                break;
            default:
                mTvStateBtnMall.setVisibility(View.GONE);
                mRlOrderPersonItemMall.setVisibility(View.GONE);
                mTvSureArriveMall.setVisibility(View.GONE);
                break;
        }
    }

    public interface OnDeviceMallItemBtnClickListener{

        /**
         * 复制订单号
         * @param ordersBean
         */
        void onCopyOrderNum(DeviceOrderListBean.DataEntity.DataBean ordersBean);

        /**
         * 派单
         * @param ordersBean
         */
        void onPostGoods(DeviceOrderListBean.DataEntity.DataBean ordersBean);

        /**
         * 确认送达
         * @param ordersBean
         */
        void onSureArrived(DeviceOrderListBean.DataEntity.DataBean ordersBean);
    }
    private OnDeviceMallItemBtnClickListener mOnDeviceMallItemBtnClickListener;

    public void setOnDeviceMallItemBtnClickListener(OnDeviceMallItemBtnClickListener onDeviceMallItemBtnClickListener) {
        mOnDeviceMallItemBtnClickListener = onDeviceMallItemBtnClickListener;
    }
}
