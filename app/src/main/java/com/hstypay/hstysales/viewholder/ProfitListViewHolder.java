package com.hstypay.hstysales.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.ProfitListBean;
import com.hstypay.hstysales.utils.DateUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zeyu.kuang
 * @time 2020/10/26
 * @desc 日分润明细和月分润明细、分润统计列表部分的ViewHolder
 */
public class ProfitListViewHolder extends RecyclerView.ViewHolder {

    public static final int VIEW_TYPE_DAY = 1;//日分润明细
    public static final int VIEW_TYPE_MONTH = 2;//月分润明细
    public static final int VIEW_TYPE_STATISTIC = 3;//分润统计
    private  ImageView mIvItemProfitIcon;//图标
    private  TextView mTvItemMchDateName;//商户名称或日期
    private  TextView mTvItemAccountJyje;//交易净额
    private  TextView mTvItemAccountFrje;//分润金额
    private final LinearLayout mLlItemAccountFrje;

    public ProfitListViewHolder(@NonNull View itemView) {
        super(itemView);
        mIvItemProfitIcon = itemView.findViewById(R.id.iv_item_profit_icon);
        mTvItemMchDateName = itemView.findViewById(R.id.tv_item_mch_date_name);
        mTvItemAccountJyje = itemView.findViewById(R.id.tv_item_account_jyje);
        mTvItemAccountFrje = itemView.findViewById(R.id.tv_item_account_frje);
        mLlItemAccountFrje = itemView.findViewById(R.id.ll_item_account_frje);
    }

    /**
     * 设置item数据
     * @param summaryData  分润item数据
     * @param viewType 1是日分润明细，2是月分润明细，3是分润统计
     */
    public void setData(ProfitListBean.SummaryData summaryData, int viewType) {
        if (summaryData==null)return;
        if (viewType==VIEW_TYPE_DAY){
            mIvItemProfitIcon.setImageResource(R.mipmap.ic_store_black_profit);
            mTvItemMchDateName.setText(summaryData.getMerchantName());
        }else if (viewType == VIEW_TYPE_MONTH){
            mIvItemProfitIcon.setImageResource(R.mipmap.ic_time_black_profit);
            mTvItemMchDateName.setText(""+getFormatTime(summaryData.getPayTradeTime()));//2020-10-02 00:00:00
        }else {
            mIvItemProfitIcon.setImageResource(R.mipmap.ic_store_black_profit);
            mTvItemMchDateName.setText(summaryData.getMerchantName());
        }
        mTvItemAccountJyje.setText(DateUtil.formatMoneyCn(summaryData.getPayNetFee() / 100d));
        if (summaryData.getSalesmanTotalFee()==null){
            mLlItemAccountFrje.setVisibility(View.INVISIBLE);
        }else {
            mLlItemAccountFrje.setVisibility(View.VISIBLE);
            mTvItemAccountFrje.setText(DateUtil.formatMoneyCn(summaryData.getSalesmanTotalFee() / 100d));
        }
    }

    private String getFormatTime(String orgTime){
        String targTime = "";
        SimpleDateFormat orgSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = orgSdf.parse(orgTime);
            SimpleDateFormat targSdf = new SimpleDateFormat("yyyy年MM月dd日");
            targTime = targSdf.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
            targTime = orgTime;
        }
        return targTime;
    }
}
