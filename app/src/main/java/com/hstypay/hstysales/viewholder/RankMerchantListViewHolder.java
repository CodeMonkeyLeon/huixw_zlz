package com.hstypay.hstysales.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.RankListBean;
import com.hstypay.hstysales.utils.DateUtil;

public class RankMerchantListViewHolder extends RecyclerView.ViewHolder {

    private final TextView mTvMerchantNameStoreItem;//名称和店名
    private final TextView mTvCreateTimeRankItem;//创建时间
    public final LinearLayout mLlCallTelephoneRankItem;//电话联系
    private final TextView mTvTradeAmountRankItem;//交易时间
    private final TextView mTvTradeCountRankItem;//交易笔数

    public RankMerchantListViewHolder(@NonNull View itemView) {
        super(itemView);
        mTvMerchantNameStoreItem = itemView.findViewById(R.id.tv_merchant_name_store_item);
        mTvCreateTimeRankItem = itemView.findViewById(R.id.tv_create_time_rank_item);
        mLlCallTelephoneRankItem = itemView.findViewById(R.id.ll_call_telephone_rank_item);
        mTvTradeAmountRankItem = itemView.findViewById(R.id.tv_trade_amount_rank_item);
        mTvTradeCountRankItem = itemView.findViewById(R.id.tv_trade_count_rank_item);
    }

    public void setData(RankListBean.SummaryData summaryData) {
        if (summaryData!=null){
            mTvMerchantNameStoreItem.setText(summaryData.getPrincipal()+"-"+summaryData.getMerchantName());
            mTvCreateTimeRankItem.setText(summaryData.getMerchantRegisterTime());
            mTvTradeAmountRankItem.setText(DateUtil.formatMoneyUtil((summaryData.getTotalFee()) / 100d));
            mTvTradeCountRankItem.setText(""+summaryData.getBillCount());
        }

    }
}
