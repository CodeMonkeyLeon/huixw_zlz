package com.hstypay.hstysales.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.WxSignDetailBean;

public class WxRealMerchantViewHolder extends RecyclerView.ViewHolder {

    private final TextView mTvNameWxMerchantItem;//商户名称
    private final TextView mTvStateSignWxItem;//签约状态
    private final TextView mTvTelphoneMerchantItem;//手机号
    private final LinearLayout mLlWaitFailStateSignWx;//待签约或签约失败时显示，初始或签约成功隐藏
    private final LinearLayout mLlOptWaitSignWx;//待签约时显示
    public final LinearLayout mLlCancelSignWx;//撤销申请按钮
    public final LinearLayout mLlSignHelpWx;//人工签约按钮
    public final LinearLayout mLlOptFailSignWx;//签约失败时显示，重新签约按钮

    private Context mContext;
    public WxRealMerchantViewHolder(Context context, @NonNull View itemView) {
        super(itemView);
        mContext = context;
        mTvNameWxMerchantItem = itemView.findViewById(R.id.tv_name_wx_merchant_item);
        mTvStateSignWxItem = itemView.findViewById(R.id.tv_state_sign_wx_item);
        mTvTelphoneMerchantItem = itemView.findViewById(R.id.tv_telphone_merchant_item);
        mLlWaitFailStateSignWx = itemView.findViewById(R.id.ll_wait_fail_state_sign_wx);
        mLlOptWaitSignWx = itemView.findViewById(R.id.ll_opt_wait_sign_wx);
        mLlCancelSignWx = itemView.findViewById(R.id.ll_cancel_sign_wx);
        mLlSignHelpWx = itemView.findViewById(R.id.ll_sign_help_wx);
        mLlOptFailSignWx = itemView.findViewById(R.id.ll_opt_fail_sign_wx);
    }

    public void setData(WxSignDetailBean.SignDetailData signDetailData) {
        if (signDetailData!= null){
            String principal = signDetailData.getPrincipal();
            String merchantName = signDetailData.getMerchantName();
            if (!TextUtils.isEmpty(principal) && !TextUtils.isEmpty(merchantName)){
                mTvNameWxMerchantItem.setText(principal+"-"+ merchantName);
            }else {
                if (!TextUtils.isEmpty(merchantName)){
                    mTvNameWxMerchantItem.setText(merchantName);
                }else if (!TextUtils.isEmpty(principal)){
                    mTvNameWxMerchantItem.setText(principal);
                }
            }

            mTvStateSignWxItem.setText(signDetailData.getStatusCnt());
            String status = signDetailData.getStatus();
            switch (status){
                case "0"://初始
                    mTvStateSignWxItem.setTextColor(mContext.getResources().getColor(R.color.merchant_sure_commit));
                    mLlWaitFailStateSignWx.setVisibility(View.GONE);
                    mLlOptWaitSignWx.setVisibility(View.GONE);
                    mLlOptFailSignWx.setVisibility(View.GONE);
                    break;
                case "10"://成功
                    mTvStateSignWxItem.setTextColor(mContext.getResources().getColor(R.color.merchant_sure_commit));
                    mLlWaitFailStateSignWx.setVisibility(View.GONE);
                    mLlOptWaitSignWx.setVisibility(View.GONE);
                    mLlOptFailSignWx.setVisibility(View.GONE);

                    break;
                case "20"://失败
                    mTvStateSignWxItem.setTextColor(mContext.getResources().getColor(R.color.color_fd414b));
                    mLlWaitFailStateSignWx.setVisibility(View.VISIBLE);
                    mLlOptWaitSignWx.setVisibility(View.GONE);
                    mLlOptFailSignWx.setVisibility(View.VISIBLE);
                    break;
                case "30"://待签约
                    mTvStateSignWxItem.setTextColor(mContext.getResources().getColor(R.color.color_ffa000));
                    mLlWaitFailStateSignWx.setVisibility(View.VISIBLE);
                    mLlOptWaitSignWx.setVisibility(View.VISIBLE);
                    mLlOptFailSignWx.setVisibility(View.GONE);
                    break;
            }
            mTvTelphoneMerchantItem.setText(signDetailData.getMerchantId());
        }
    }
}
