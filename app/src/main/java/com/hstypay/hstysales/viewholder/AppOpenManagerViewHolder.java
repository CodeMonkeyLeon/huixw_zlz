package com.hstypay.hstysales.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.ExamineListBean;

/**
 * @author zeyu.kuang
 * @time 2020/12/10
 * @desc 商户应用开通管理页的viewHolder
 */
public class AppOpenManagerViewHolder extends RecyclerView.ViewHolder {

    public static final String TYPE_HBFQ_CODE = "ALIPAY-ISTALLMENT";//应用类型：花呗分期免息
    public static final String TYPE_PLEDGE_CODE = "PREAUTH-PAY";//应用类型：押金收款
    public static final String STATUS_NEED_EXAMINE = "20";//待审核
    public static final String STATUS_OK = "30";//已完成(审核通过)
    public static final String STATUS_FAIL = "50";//审核失败
    private  ImageView mIvIconAppOpenList;//应用类型图标
    private  TextView mTvNameAppOpenList;//应用名称
    private  TextView mTvMerchantNameAppList;//商户名称
    private  TextView mTvApplyTimeAppList;//申请开通时间
    private  ImageView mIvIconStatusAppList;//状态图标

    public AppOpenManagerViewHolder(@NonNull View itemView) {
        super(itemView);
        mIvIconAppOpenList = itemView.findViewById(R.id.iv_icon_app_open_list);
        mTvNameAppOpenList = itemView.findViewById(R.id.tv_name_app_open_list);
        mTvMerchantNameAppList = itemView.findViewById(R.id.tv_merchant_name_app_list);
        mTvApplyTimeAppList = itemView.findViewById(R.id.tv_apply_time_app_list);
        mIvIconStatusAppList = itemView.findViewById(R.id.iv_icon_status_app_list);
    }

    public void setData(ExamineListBean.ExamineBean examineBean) {
        if (examineBean==null)return;
        String appCode = examineBean.getAppCode();
        if (TYPE_HBFQ_CODE.equals(appCode)){
            mIvIconAppOpenList.setImageResource(R.mipmap.icon_app_huabeibig);
        }else if (TYPE_PLEDGE_CODE.equals(appCode)){
            mIvIconAppOpenList.setImageResource(R.mipmap.icon_app_yajin);
        }else {
            mIvIconAppOpenList.setVisibility(View.GONE);
        }
        mTvNameAppOpenList.setText(examineBean.getAppIdCnt());
        mTvMerchantNameAppList.setText(examineBean.getAdminIdCnt());
        mTvApplyTimeAppList.setText(examineBean.getCreateTime());
        String orderStatus = examineBean.getOrderStatus();
        if (STATUS_NEED_EXAMINE.equals(orderStatus)){
            mIvIconStatusAppList.setImageResource(R.mipmap.icon_app_daishenhe);
        }else if (STATUS_OK.equals(orderStatus)){
            mIvIconStatusAppList.setImageResource(R.mipmap.icon_app_yitongguo);
        }else if (STATUS_FAIL.equals(orderStatus)){
            mIvIconStatusAppList.setImageResource(R.mipmap.icon_app_weitongguo);
        }else {
            mIvIconStatusAppList.setVisibility(View.GONE);
        }
    }
}
