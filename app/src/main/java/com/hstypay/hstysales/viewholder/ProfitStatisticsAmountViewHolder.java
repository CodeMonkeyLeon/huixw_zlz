package com.hstypay.hstysales.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hstypay.hstysales.R;
import com.hstypay.hstysales.bean.ProfitsummaryBean;
import com.hstypay.hstysales.utils.DateUtil;

/**
 * @author zeyu.kuang
 * @time 2020/10/27
 * @desc 分润统计的金额+笔数部分的viewholder
 */
public class ProfitStatisticsAmountViewHolder extends RecyclerView.ViewHolder {

    private final TextView mTvIncomeMoneyProfitStatistics;//实收金额
    private final TextView mTvIncomeCountProfitStatistics;//实收笔数
    private final TextView mTvRefundMoneyProfitStatistics;//退款金额
    private final TextView mTvRefundCountProfitStatistics;//退款笔数
    private final TextView mTvTradeMoneyProfitStatistics;//交易金额
    private final TextView mTvSalesMoneyProfitStatistics;//分润金额
    private final LinearLayout mLlSalesMoneyProfitStatistics;

    public ProfitStatisticsAmountViewHolder(@NonNull View itemView) {
        super(itemView);
        mTvIncomeMoneyProfitStatistics = itemView.findViewById(R.id.tv_income_money_profit_statistics);
        mTvIncomeCountProfitStatistics = itemView.findViewById(R.id.tv_income_count_profit_statistics);
        mTvRefundMoneyProfitStatistics = itemView.findViewById(R.id.tv_refund_money_profit_statistics);
        mTvRefundCountProfitStatistics = itemView.findViewById(R.id.tv_refund_count_profit_statistics);
        mTvTradeMoneyProfitStatistics = itemView.findViewById(R.id.tv_trade_money_profit_statistics);
        mTvSalesMoneyProfitStatistics = itemView.findViewById(R.id.tv_sales_money_profit_statistics);
        mLlSalesMoneyProfitStatistics = itemView.findViewById(R.id.ll_sales_money_profit_statistics);
    }

    public void setData(ProfitsummaryBean.ProfitSummaryData profitSummaryData) {
        if (profitSummaryData==null)return;
        mTvIncomeMoneyProfitStatistics.setText(DateUtil.formatMoneyCn(profitSummaryData.getPayFee() / 100d));
        mTvIncomeCountProfitStatistics.setText(""+profitSummaryData.getSuccessCount());
        mTvRefundMoneyProfitStatistics.setText(DateUtil.formatMoneyCn(profitSummaryData.getRefundFee() / 100d));
        mTvRefundCountProfitStatistics.setText(""+profitSummaryData.getRefundCount());
        mTvTradeMoneyProfitStatistics.setText(DateUtil.formatMoneyCn(profitSummaryData.getPayNetFee() / 100d));
        if (profitSummaryData.getSalesmanTotalFee()==null){
            mLlSalesMoneyProfitStatistics.setVisibility(View.INVISIBLE);
        }else {
            mLlSalesMoneyProfitStatistics.setVisibility(View.VISIBLE);
            mTvSalesMoneyProfitStatistics.setText(DateUtil.formatMoneyCn(profitSummaryData.getSalesmanTotalFee() / 100d));

        }

    }
}
