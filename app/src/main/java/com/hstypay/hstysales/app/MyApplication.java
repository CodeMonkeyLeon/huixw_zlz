package com.hstypay.hstysales.app;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;

import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.hstypay.hstysales.BuildConfig;
import com.hstypay.hstysales.R;
import com.hstypay.hstysales.utils.AppHelper;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.SpStayUtil;
import com.hstypay.hstysales.utils.SpUtil;
import com.igexin.sdk.PushManager;
import com.tencent.stat.StatCrashCallback;
import com.tencent.stat.StatCrashReporter;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareConfig;
import com.v5kf.client.lib.Logger;
import com.v5kf.client.lib.V5ClientAgent;
import com.v5kf.client.lib.V5ClientConfig;
import com.v5kf.client.lib.callback.V5InitCallback;
import com.zhy.http.okhttp.OkHttpUtils;

import android.app.ActivityManager.RunningAppProcessInfo;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import okhttp3.OkHttpClient;

import static com.tencent.stat.StatTrackLog.log;


/**
 * @项目名: HstyPay
 * @包名: com.hstypay.hstysales.app
 * @创建者: Jeremy
 * @创建时间: 2017/6/27 11:28
 * @描述: ${TODO}
 */
public class MyApplication extends Application {

    private static Context mContext;
    private static MyApplication instance = null;
    private List<Activity> activities = new LinkedList<>();
    private static OkHttpClient okHttpClient;

    public static int type = 100;
    public static boolean isupdate = true;
    public static boolean iscache = false;
    public static boolean isRefresh = false;

    {
        PlatformConfig.setWeixin("wxc07495e2b6c59812", "c43cd4d780a6b4f2d828993b10f30ecc");
        PlatformConfig.setQQZone("1106492275", "NjuhyUsYpW2j35TZ");
        PlatformConfig.setSinaWeibo("", "", "");
    }

    public MyApplication() {

    }

    public static MyApplication getInstance() {
        if (instance == null) {
            instance = new MyApplication();
        }
        return instance;
    }

    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = new MyApplication();
        mContext = getApplicationContext();
        try {
            if (iscache)
                return;
            AppHelper.getAppCacheDir();
            iscache = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        PushManager.getInstance().initialize(this);
        OkHttpClient okHttpClient = initClient();
        OkHttpUtils.initClient(okHttpClient);

        initV5Client();
        initStatCrash(mContext);
        initShare();
    }

    /**
     * 初始化V5KF
     */
    private void initV5Client() {
        if (isMainProcess()) { // 判断为主进程，在主进程中初始化，多进程同时初始化可能导致不可预料的后果
            Logger.w("MyApplication", "onCreate isMainProcess V5ClientAgent.init");
            V5ClientConfig.FILE_PROVIDER = BuildConfig.APPLICATION_ID + ".fileprovider"; // 设置fileprovider的authorities
            V5ClientAgent.init(this, "156230", "2624608012e41", new V5InitCallback() {

                @Override
                public void onSuccess(String response) {
                    // TODO Auto-generated method stub
                    Logger.i("MyApplication", "V5ClientAgent.init(): " + response);
                }

                @Override
                public void onFailure(String response) {
                    // TODO Auto-generated method stub
                    Logger.e("MyApplication", "V5ClientAgent.init(): " + response);
                }
            });
        }
    }

    public boolean isMainProcess() {
        ActivityManager am = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE));
        List<RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        String mainProcessName = getPackageName();
        int myPid = android.os.Process.myPid();
        for (RunningAppProcessInfo info : processInfos) {
            if (info.pid == myPid && mainProcessName.equals(info.processName)) {
                return true;
            }
        }
        return false;
    }

    private void initShare() {
        Config.DEBUG = true;
        UMShareAPI.get(mContext);
        UMShareConfig config = new UMShareConfig();
        config.isNeedAuthOnGetUserInfo(false);
        config.isOpenShareEditActivity(true);
        config.setSinaAuthType(UMShareConfig.AUTH_TYPE_SSO);
        config.setFacebookAuthType(UMShareConfig.AUTH_TYPE_SSO);
        config.setShareToLinkedInFriendScope(UMShareConfig.LINKED_IN_FRIEND_SCOPE_ANYONE);
    }

    public void initStatCrash(Context context) {
        StatCrashReporter crashReporter = StatCrashReporter.getStatCrashReporter(context);
        crashReporter.setEnableInstantReporting(true);
        crashReporter.setJavaCrashHandlerStatus(true);
        crashReporter.addCrashCallback(
                new StatCrashCallback() {
                    @Override
                    public void onJniNativeCrash(String nativeCrashStacks) { // native crash happened
                        // do something
                    }

                    @Override
                    public void onJavaCrash(Thread thread, Throwable ex) {// java crash happened
                        // do something
                        log("MTA StatCrashCallback onJavaCrash:\n" + ex);
                    }
                });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    public OkHttpClient initClient() {
        if (okHttpClient == null) {
            ClearableCookieJar cookieJar =
                    new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(getApplicationContext()));
            okHttpClient = new OkHttpClient.Builder()
                    .cookieJar(cookieJar)
                    //其他配置
                    .build();
        }
        return okHttpClient;
    }

    public void addActivity(Activity paramActivity) {
        this.activities.add(paramActivity);
    }

    public void finishAllActivity() {
        try {
            Iterator<Activity> localIterator = this.activities.iterator();
            while (localIterator.hasNext()) {
                localIterator.next().finish();
            }
            activities.clear();

        } catch (Exception localException) {
            localException.printStackTrace();
        }
        //android.os.Process.killProcess(android.os.Process.myPid());
    }

    public void removeActivity(Activity paramActivity) {

        this.activities.remove(paramActivity);
    }

    /**
     * 蓝牙打印设置状态
     * <功能详细描述>
     * param context
     *
     * @return
     * @see [类、类#方法、类#成员]
     */


    public static Boolean getBluePrintSetting() {
        return SpUtil.getBoolean(getContext(),"BluePrintSetting", true);
    }

    //签名的key
    public static String getSignKey() {
        return SpUtil.getString(mContext, Constants.SP_SIGN_KEY, "");
    }

    //cookie中的skey
    public static String getSkey() {

        return SpUtil.getString(MyApplication.getContext(), Constants.SKEY, "");
    }

    //cookie
    public static String getCookies() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_LOGIN_COOKIE, "");
    }

    //用户名
    public static String getUsername() {
        return SpUtil.getString(MyApplication.getContext(), Constants.APP_USERNAME, "");
    }

    //个推clientID
    public static String getGetuiClienId() {
        return SpUtil.getString(MyApplication.getContext(), Constants.GETUI_CLIENT_ID, "");
    }

    //common.not.login
    //密码登录过期
    public static String getFreeLogin() {
        return SpUtil.getString(MyApplication.getContext(), Constants.AUTO_PWD_FREE_LOGIN_ERROR, "common.not.login");
    }

    //取消版本升级
    public static Boolean getUpdateCancel() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.UPDATE_VERSION_CANCEL, true);
    }

    //存在未进件
    public static Boolean hasNoInfo() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_HAS_NO_INFO, false);
    }

    //首页消息是否关闭
    public static Boolean noticeClose() {
        return SpUtil.getBoolean(MyApplication.getContext(), Constants.SP_NOTICE_CLOSE, false);
    }

    //用户名
    public static String getRealName() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_REALNAME, "");
    }

    //用户ID
    public static String getUserId() {
        return SpUtil.getString(mContext, Constants.USER_ID, "");
    }


    //imei
    public static String getImei() {
        return SpUtil.getString(MyApplication.getContext(), Constants.SP_IMEI, "");
    }

    //imei
    public static void setImei(String S) {
        SpUtil.putString(MyApplication.getContext(), Constants.SP_IMEI, S);
    }

    public static String getConfig() {
        return SpStayUtil.getString(MyApplication.getContext(), Constants.SP_CONFIG + MyApplication.getUserId(), Constants.CONFIG_ALL);
    }

    /**
     * 获取主题色
     *
     * @return
     */
    public static String themeColor() {
        String colorString = "#" + Integer.toHexString(ContextCompat.getColor(getContext(), R.color.theme_color));
        return colorString;
    }
}

