package com.hstypay.hstysales.network;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.hstypay.hstysales.bean.BankListBean;
import com.hstypay.hstysales.bean.BaseBean;
import com.hstypay.hstysales.bean.BindCodeBean;
import com.hstypay.hstysales.bean.BranchListBean;
import com.hstypay.hstysales.bean.CodeDetailBean;
import com.hstypay.hstysales.bean.CodeUrlBean;
import com.hstypay.hstysales.bean.ConfigBean;
import com.hstypay.hstysales.bean.ConfirmMerchantBean;
import com.hstypay.hstysales.bean.DeviceDetailBean;
import com.hstypay.hstysales.bean.DeviceInfoSn;
import com.hstypay.hstysales.bean.DeviceListBean;
import com.hstypay.hstysales.bean.DeviceOrderDispatchBean;
import com.hstypay.hstysales.bean.DeviceOrderListBean;
import com.hstypay.hstysales.bean.DeviceOrderTransitCountBean;
import com.hstypay.hstysales.bean.DeviceTypeBean;
import com.hstypay.hstysales.bean.ExamineDetailBean;
import com.hstypay.hstysales.bean.ExamineListBean;
import com.hstypay.hstysales.bean.HomeDataBean;
import com.hstypay.hstysales.bean.HomeNoticeBean;
import com.hstypay.hstysales.bean.IncomeDetailBean;
import com.hstypay.hstysales.bean.IndustryBean;
import com.hstypay.hstysales.bean.Info;
import com.hstypay.hstysales.bean.InfoBean;
import com.hstypay.hstysales.bean.InvitationCodeBean;
import com.hstypay.hstysales.bean.LocationBean;
import com.hstypay.hstysales.bean.LoginBean;
import com.hstypay.hstysales.bean.MerChantModifyBean;
import com.hstypay.hstysales.bean.MerchantInfoBean;
import com.hstypay.hstysales.bean.MerchantListBean;
import com.hstypay.hstysales.bean.NoticeBean;
import com.hstypay.hstysales.bean.OrangeStateBean;
import com.hstypay.hstysales.bean.PayTypeStatusBean;
import com.hstypay.hstysales.bean.ProfitMonthListBean;
import com.hstypay.hstysales.bean.ProfitsummaryBean;
import com.hstypay.hstysales.bean.ProfitListBean;
import com.hstypay.hstysales.bean.QrcodeBean;
import com.hstypay.hstysales.bean.QueryAgentStoreInfoBean;
import com.hstypay.hstysales.bean.QueryEnabledChangeBean;
import com.hstypay.hstysales.bean.RankBean;
import com.hstypay.hstysales.bean.RankListBean;
import com.hstypay.hstysales.bean.RankSumaryBean;
import com.hstypay.hstysales.bean.SalesmanBean;
import com.hstypay.hstysales.bean.SendMsgBean;
import com.hstypay.hstysales.bean.ServiceProviderBean;
import com.hstypay.hstysales.bean.SettleChangeCountBean;
import com.hstypay.hstysales.bean.SignMerchantListBean;
import com.hstypay.hstysales.bean.StoreListBean;
import com.hstypay.hstysales.bean.UpdateVersionBean;
import com.hstypay.hstysales.bean.VipInfoBean;
import com.hstypay.hstysales.bean.WxSignDetailBean;
import com.hstypay.hstysales.network.OkHttpUtil.OnResponse;
import com.hstypay.hstysales.utils.ConfigUtil;
import com.hstypay.hstysales.utils.Constants;
import com.hstypay.hstysales.utils.LogUtil;
import com.hstypay.hstysales.utils.SignUtil;
import com.hstypay.hstysales.utils.SpUtil;
import com.hstypay.hstysales.utils.StringUtils;
import com.qiezzi.choseviewlibrary.bean.AddressBean;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2017/7/22.
 */
public class ServerClient {
    private static ServerClient mServerClient;
    private static OkHttpUtil httpUtils = null;
    private static Gson gson = null;
    private Context context;
    //时间戳
    public static final long times = System.currentTimeMillis();
    String time = times + "";

    private ServerClient(Context context) {
        this.context = context;
    }

    public static ServerClient newInstance(Context context) {
        if (mServerClient == null) {
            mServerClient = new ServerClient(context);
        }
        if (httpUtils == null) {
            httpUtils = OkHttpUtil.newInstance(context);
        }
        if (gson == null) {
            gson = new Gson();
        }
        return mServerClient;
    }

    //登录接口
    public void login(final Context context, final String tag, final String username, final String pwd) {
        final String url = Constants.BASE_URL + "/app/login";
        Map<String, Object> map = new HashMap<>();
        map.put("userName", username);
        map.put("password", pwd);
        if (ConfigUtil.verifyServiceId()) {
            map.put("serviceProviderId", Constants.SERVICE_PROVIDER_ID);
        }
        if (ConfigUtil.getConfig()) {
            map.put("orgId", Constants.ORG_ID);
            map.put("orgType", Constants.ORG_TYPE);
            map.put("appCode", Constants.APP_CODE + Constants.ORG_ID);
        }

        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postCall(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "登录接口==" + response+"\n"+url);
                LoginBean info = HstyCallBack(response, LoginBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.MSG_LOGIN_FALSE, info));
                    } else {
                        if (info != null && info.getData() != null) {
                            int userId = info.getData().getUser().getUserId();
                            SpUtil.putString(context, Constants.USER_ID, userId + "");//用户id
                            String realName = info.getData().getUser().getRealName();
                            SpUtil.putString(context, Constants.SP_REALNAME, realName);
                            if (!TextUtils.isEmpty(info.getData().getSignKey())) {
                                SpUtil.putString(context, Constants.SP_SIGN_KEY, info.getData().getSignKey());//签名的key
                            }
                            if (info.getData().getUser() != null) {
                                LoginBean.DataEntity.UserEntity user = info.getData().getUser();
                                if (!TextUtils.isEmpty(user.getLoginPhone())) {
                                    SpUtil.putString(context, Constants.SP_LOGIN_TELEPHONE, user.getLoginPhone());
                                } else {
                                    SpUtil.putString(context, Constants.SP_LOGIN_TELEPHONE, "");
                                }
                                if (!TextUtils.isEmpty(user.getRealName())) {
                                    SpUtil.putString(context, Constants.SP_REAL_NAME, user.getRealName());
                                } else {
                                    SpUtil.putString(context, Constants.SP_REAL_NAME, "");
                                }
                                if (!TextUtils.isEmpty(user.getUserName())) {
                                    SpUtil.putString(context, Constants.SP_USER_NAME, user.getUserName());
                                } else {
                                    SpUtil.putString(context, Constants.SP_USER_NAME, "");
                                }
                                if (user.getServiceProvider() != null && !TextUtils.isEmpty(user.getServiceProvider().getServiceProviderName())) {
                                    SpUtil.putString(context, Constants.SP_SERVICE_PROVIDER_NAME, user.getServiceProvider().getServiceProviderName());
                                } else {
                                    SpUtil.putString(context, Constants.SP_SERVICE_PROVIDER_NAME, "");
                                }
                                if (user.getServiceChannel() != null && !TextUtils.isEmpty(user.getServiceChannel().getServiceProviderName())) {
                                    SpUtil.putString(context, Constants.SP_SERVICE_CHANNEL_NAME, user.getServiceChannel().getServiceProviderName());
                                } else {
                                    SpUtil.putString(context, Constants.SP_SERVICE_CHANNEL_NAME, "");
                                }
                                if (user.getRoles() != null) {
                                    SpUtil.putString(context, Constants.SP_ROLE_CODE, user.getRoles().get(0).getRoleCode());
                                    SpUtil.putString(context, Constants.SP_SERVICE_PROVIDER, user.getRoles().get(0).getServiceProvider());
                                } else {
                                    SpUtil.putString(context, Constants.SP_ROLE_CODE, "");
                                    SpUtil.putString(context, Constants.SP_SERVICE_PROVIDER, "");
                                }
                            }
                        }
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.MSG_LOGIN_TRUE, info));
                    }
                }
            }
        });
    }

    //免密登录
    public void autoPwd(final Context context, final String tag) {
        Map<String, Object> map = new HashMap<>();
        String url = Constants.BASE_URL + "/app/login/auto";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.autoPostCall(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LoginBean info = HstyCallBack(response, LoginBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.AUTO_PWD_FALSE, info));
                    } else {
                        if (info != null && info.getData() != null) {
                            int userId = info.getData().getUser().getUserId();
                            SpUtil.putString(context, Constants.USER_ID, userId + "");//用户id
                            String realName = info.getData().getUser().getRealName();
                            SpUtil.putString(context, Constants.SP_REALNAME, realName);
                            if (!TextUtils.isEmpty(info.getData().getSignKey())) {
                                SpUtil.putString(context, Constants.SP_SIGN_KEY, info.getData().getSignKey());//签名的key
                            }
                            if (info.getData().getUser() != null) {
                                LoginBean.DataEntity.UserEntity user = info.getData().getUser();
                                if (!TextUtils.isEmpty(user.getLoginPhone())) {
                                    SpUtil.putString(context, Constants.SP_LOGIN_TELEPHONE, user.getLoginPhone());
                                } else {
                                    SpUtil.putString(context, Constants.SP_LOGIN_TELEPHONE, "");
                                }
                                if (!TextUtils.isEmpty(user.getRealName())) {
                                    SpUtil.putString(context, Constants.SP_REAL_NAME, user.getRealName());
                                } else {
                                    SpUtil.putString(context, Constants.SP_REAL_NAME, "");
                                }
                                if (!TextUtils.isEmpty(user.getUserName())) {
                                    SpUtil.putString(context, Constants.SP_USER_NAME, user.getUserName());
                                } else {
                                    SpUtil.putString(context, Constants.SP_USER_NAME, "");
                                }
                                if (user.getServiceProvider() != null && !TextUtils.isEmpty(user.getServiceProvider().getServiceProviderName())) {
                                    SpUtil.putString(context, Constants.SP_SERVICE_PROVIDER_NAME, user.getServiceProvider().getServiceProviderName());
                                } else {
                                    SpUtil.putString(context, Constants.SP_SERVICE_PROVIDER_NAME, "");
                                }
                                if (user.getServiceChannel() != null && !TextUtils.isEmpty(user.getServiceChannel().getServiceProviderName())) {
                                    SpUtil.putString(context, Constants.SP_SERVICE_CHANNEL_NAME, user.getServiceChannel().getServiceProviderName());
                                } else {
                                    SpUtil.putString(context, Constants.SP_SERVICE_CHANNEL_NAME, "");
                                }
                                if (user.getRoles() != null) {
                                    SpUtil.putString(context, Constants.SP_ROLE_CODE, user.getRoles().get(0).getRoleCode());
                                    SpUtil.putString(context, Constants.SP_SERVICE_PROVIDER, user.getRoles().get(0).getServiceProvider());
                                } else {
                                    SpUtil.putString(context, Constants.SP_ROLE_CODE, "");
                                    SpUtil.putString(context, Constants.SP_SERVICE_PROVIDER, "");
                                }
                            }
                        }
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.AUTO_PWD_TRUE, info));
                    }

                }
            }
        });
    }

    //获取短信验证码
    public void getCode(Context context, final String tag, String telphone) {
        String url = Constants.BASE_URL + "/app/password/sms/send";
        Map<String, Object> map = new HashMap<>();
        map.put("phone", telphone);
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "短信验证码==" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.SEND_PHONE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.SEND_PHONE_TRUE, info));
                }
            }
        });
    }

    //语音验证码
    public void getVoiceCode(Context context, final String tag, String telphone) {
        String url = Constants.BASE_URL + "/app/password/voice/sms/send";
        Map<String, Object> map = new HashMap<>();
        map.put("phone", telphone);
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "语音验证码==" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_VOICE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_VOICE_TRUE, info));
                }
            }
        });
    }

    //验证短信验证码
    public void checkCode(Context context, final String tag, String telphone, String code) {
        Map<String, Object> map = new HashMap<>();
        String url = Constants.BASE_URL + "/app/password/sms/check";
        map.put("phone", telphone);
        map.put("code", code);
        if (ConfigUtil.getConfig()) {
            map.put("orgId", Constants.ORG_ID);
            map.put("orgType", Constants.ORG_TYPE);
            map.put("appCode", Constants.APP_CODE + Constants.ORG_ID);
        }
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "验证验证码==" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CHECK_CODE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CHECK_CODE_TRUE, info));
                }
            }
        });

    }

    //重置密码
    public void resetPwd(Context context, final String tag, String pwd, String telephone) {
        //password/reset
        Map<String, Object> map = new HashMap<>();
        String url = Constants.BASE_URL + "/app/password/reset";
        map.put("newPassword", pwd);
        map.put("telphone", telephone);
        String data = SignUtil.getInstance().loginAppSign(map, time, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {

                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.RESET_PWD_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.RESET_PWD_TRUE, info));
                }

            }
        });
    }

    //退出登录
    public void logout(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/logout";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "退出登录==" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.MSG_LOGOUT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.MSG_LOGOUT_TRUE, info));
                }
            }
        });
    }

    // 绑定/更换手机号---短信验证码
    public void getBindingMsg(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/phone/verifycode/send";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "绑定短信验证==" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_BINDING_MSG_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_BINDING_MSG_TRUE, info));
                }
            }
        });
    }

    // 绑定/更换手机号---语音验证码
    public void getBindingVoice(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/phone/voice/verifycode/send";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "绑定语音验证==" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_BINDING_VOICE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_BINDING_VOICE_TRUE, info));
                }
            }
        });
    }

    // 绑定手机号
    public void bindingTel(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/login/phone/bind";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "绑定手机号==" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_BINDING_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_BINDING_TRUE, info));
                }
            }
        });
    }

    // 更换手机号
    public void changeBindingTel(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/set/modify/phone";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "更换手机号==" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    // 首页
    public void homeData(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/index/v2";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "首页数据==" + response);
                HomeDataBean info = HstyCallBack(response, HomeDataBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.HOME_DATA_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.HOME_DATA_TRUE, info));
                }
            }
        });
    }

    // 我的收入详情
    public void incomeDetail(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/myEarnings/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "我的收入详情==" + response);
                IncomeDetailBean info = HstyCallBack(response, IncomeDetailBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.INCOME_DETAIL_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.INCOME_DETAIL_TRUE, info));
                }
            }
        });
    }

    // 排名
    public void getRank(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/rank/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "排名==" + response);
                RankBean info = HstyCallBack(response, RankBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_RANK_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_RANK_TRUE, info));
                }
            }
        });
    }

    // 商户列表
    public void getMerchantList(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/v2/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户列表==" + response);
                MerchantListBean info = HstyCallBack(response, MerchantListBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_MERCHANT_LIST_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_MERCHANT_LIST_TRUE, info));
                }
            }
        });
    }

    // 城市经理分润的商户列表
    public void getMerchantList2(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/v2/simple/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户列表2==" + response);
                MerchantListBean info = HstyCallBack(response, MerchantListBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_MERCHANT_LIST_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_MERCHANT_LIST_TRUE, info));
                }
            }
        });
    }

    //硬件商城 -> 订单管理 -> 商户订单列表
    public void getDeviceOrderList(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/mall/device/order/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "硬件商城订单列表==" + response);
                DeviceOrderListBean info = HstyCallBack(response, DeviceOrderListBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_DEVICE_ORDER_LIST_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_DEVICE_ORDER_LIST_TRUE, info));
                }
            }
        });

    }

    //硬件商城 -> 订单管理 -> 派单
    public void deviceDispatchOrder(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/mall/device/order/dispatch";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "硬件商城 派单==" + response);
                DeviceOrderDispatchBean info = HstyCallBack(response, DeviceOrderDispatchBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_DEVICE_ORDER_DISPATCH_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_DEVICE_ORDER_DISPATCH_TRUE, info));
                }
            }
        });
    }

    //硬件商城 -> 订单管理 -> 确认订单到达(确认收货)
    public void deviceSureOrderArrive(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/mall/device/order/receive/ensure";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "硬件商城 确认订单到达==" + response);
                DeviceOrderDispatchBean info = HstyCallBack(response, DeviceOrderDispatchBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_DEVICE_ORDER_ARRIVE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_DEVICE_ORDER_ARRIVE_TRUE, info));
                }
            }
        });
    }

    //硬件商城 -> 订单管理 -> 获取在途订单数量
    public void getTransitCount(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/mall/device/order/in/transit/count";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "硬件商城获取在途订单数量==" + response);
                DeviceOrderTransitCountBean info = HstyCallBack(response, DeviceOrderTransitCountBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_DEVICE_ORDER_TRANSITCOUNT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_DEVICE_ORDER_TRANSITCOUNT_TRUE, info));
                }
            }
        });
    }

    //查询应用审核列表
    public void getAppExamineList(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/svc/examineList";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询应用审核列表==" + response);
                ExamineListBean info = HstyCallBack(response, ExamineListBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_APP_EXAMINE_LIST_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_APP_EXAMINE_LIST_TRUE, info));
                }
            }
        });
    }

    //查询应用审核详情
    public void getExamineDetail(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/svc/examineDetail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询应用审核详情==" + response);
                ExamineDetailBean info = HstyCallBack(response, ExamineDetailBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_APP_EXAMINE_DETAIL_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_APP_EXAMINE_DETAIL_TRUE, info));
                }
            }
        });
    }

    //应用审核
    public void examineApp(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/svc/examine";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "应用审核==" + response);
                BaseBean info = HstyCallBack(response, BaseBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.EXAMINE_APP_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.EXAMINE_APP_TRUE, info));
                }
            }
        });
    }

    // 未进件商户
    public void getMerchantNoInfo(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/user/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "未进件商户==" + response);
                MerchantListBean info = HstyCallBack(response, MerchantListBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.MERCHANT_NO_INFO_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.MERCHANT_NO_INFO_TRUE, info));
                }
            }
        });
    }

    //获取商户信息
    public void getMerchantInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/v2/detail/info/new";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "获取商户信息==" + response);
                MerchantInfoBean info = HstyCallBack(response, MerchantInfoBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_MERCHANT_INFO_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_MERCHANT_INFO_TRUE, info));
                }
            }
        });
    }

    //查询商户剩余修改结算信息次数
    public void getSettleChangeCount(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/v2/account/changeCount";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询商户剩余修改结算信息次数==" + response);
                SettleChangeCountBean info = HstyCallBack(response, SettleChangeCountBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_SETTLE_CHANGE_COUNT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_SETTLE_CHANGE_COUNT_TRUE, info));
                }
            }
        });
    }

    //确认商户信息
    public void confirmMerchant(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/v2/confirm";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "确认商户信息==" + response);
                ConfirmMerchantBean info = HstyCallBack(response, ConfirmMerchantBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_MERCHANT_INFO_CONFIRM_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_MERCHANT_INFO_CONFIRM_TRUE, info));
                }
            }
        });
    }

    //获取行业类别
    public void getIndustryType(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/find/industry";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "获取行业类别==" + response);
                IndustryBean info = HstyCallBack(response, IndustryBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_INDUSTRY_TYPE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_INDUSTRY_TYPE_TRUE, info));
                }
            }
        });
    }

    //获取地址
    public void getAddress(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/find/all/area/grading";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "获取地址==" + response);
                AddressBean info = HstyCallBack(response, AddressBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_ADDRESS_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_ADDRESS_TRUE, info));
                }
            }
        });
    }

    //提交修改资料
    public void submitMerchantInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/v2/edit/mch/info/new";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "提交修改资料==" + response);
                MerChantModifyBean info = HstyCallBack(response, MerChantModifyBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.SUBMIT_MERCHANT_INFO_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.SUBMIT_MERCHANT_INFO_TRUE, info));
                }
            }
        });
    }


    //验证商户资料是否允许修改
    public void queryEnabledChange(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/v2/enabledChange";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.d("Jeremy", "验证商户资料是否允许修改==" + response);
                QueryEnabledChangeBean info = HstyCallBack(response, QueryEnabledChangeBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUERY_ENABLED_CHANGE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUERY_ENABLED_CHANGE_TRUE, info));
                }
            }
        });
    }

    //定位查询
    public void questLocation(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/query/location/info";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "定位查询==" + response);
                LocationBean info = HstyCallBack(response, LocationBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUEST_LOCATION_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.QUEST_LOCATION_TRUE, info));
                }
            }
        });
    }

    //收集用户信息
    public void collectInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/collect/user/info";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收集用户信息=" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //通知
    public void getMessage(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/notice/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "通知==" + response);
                NoticeBean info = HstyCallBack(response, NoticeBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_MESSAGE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_MESSAGE_TRUE, info));
                }
            }
        });
    }

    //我的邀请码
    public void getInvitationcode(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/invitationcode/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "我的邀请码==" + response);
                InvitationCodeBean info = HstyCallBack(response, InvitationCodeBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_INVITATIONCODE_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.GET_INVITATIONCODE_TRUE, info));
                }
            }
        });
    }

    //邀请码详情
    public void getCodeDetail(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/invitationcode/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "邀请码详情==" + response);
                CodeDetailBean info = HstyCallBack(response, CodeDetailBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CODE_DETAIL_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CODE_DETAIL_TRUE, info));
                }
            }
        });
    }

    //邀请码链接
    public void getCodeUrl(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/invitationcode/url";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postString(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "邀请码链接==" + response);
                CodeUrlBean info = HstyCallBack(response, CodeUrlBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CODE_URL_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.CODE_URL_TRUE, info));
                }
            }
        });
    }

    //修改密码接口
    public void changePwd(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/set/modify/pwd";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "修改密码接口=" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.LOGIN_CHANGE_PWD_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.LOGIN_CHANGE_PWD_TRUE, info));
                    }
                }
            }
        });
    }

    //验证身份
    public void authentication(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/set/authentication";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "验证身份=" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.AUTHENTICATION_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.AUTHENTICATION_TRUE, info));
                    }
                }
            }
        });
    }

    //版本升级接口
    public void updateVersion(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/set/find/app/version";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "版本升级==" + response);
                UpdateVersionBean info = HstyCallBack(response, UpdateVersionBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //版本升级接口2.0
    public void updateVersion2(Context context, final String tag, Map<String, Object> map) {
        LogUtil.i("Jeremy", "版本升级2request==" + gson.toJson(map));
        String url = Constants.BASE_URL + "/app/set/find/app/channel/version";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "版本升级2==" + response);
                UpdateVersionBean info = HstyCallBack(response, UpdateVersionBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //首页消息
    public void homeNotice(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/notice/find/nearby/notice";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "首页消息==" + response);
                HomeNoticeBean info = HstyCallBack(response, HomeNoticeBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //是否有未读消息
    public void unReadNotice(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/notice/unRead";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "是否有未读消息==" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }


    //门店列表接口
    public void choiceStore(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/store/findStores";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "选择门店接口=" + response);
                StoreListBean info = HstyCallBack(response, StoreListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //查询二维码
    public void checkQrcode(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/store/query/qrcode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("zhouwei", "查询二维码==" + response);
                QrcodeBean info = HstyCallBack(response, QrcodeBean.class);
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }

    //绑定接口
    public void bindCode(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/store/storeBindQrcode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("zhouwei", "绑定二维码接口=" + response);
                BindCodeBean info = HstyCallBack(response, BindCodeBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //银行列表
    public void bankList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/find/banks";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "银行列表=" + response);
                BankListBean info = HstyCallBack(response, BankListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //支行列表
    public void bankBranchList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/find/bank/branchs";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "支行列表=" + response);
                BranchListBean info = HstyCallBack(response, BranchListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //开通会员卡
    public void addVipInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/addMchMember";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "开通会员卡=" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //会员卡详情
    public void goEditMchMember(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/goEditMchMember";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员卡详情=" + response);
                VipInfoBean info = HstyCallBack(response, VipInfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //编辑会员卡详情
    public void editMchMember(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/editMchMember";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "编辑会员卡详情=" + response);
                InfoBean info = HstyCallBack(response, InfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //会员卡领卡链接
    public void queryMchMemberQRCode(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/queryMchMemberQRCode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "会员卡领卡链接=" + response);
                CodeUrlBean info = HstyCallBack(response, CodeUrlBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    //发送短信验证接口
    public void send(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/v2/sms/send/new";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "发送短信验证=" + response);
                SendMsgBean info = HstyCallBack(response, SendMsgBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    //修改法人信息
    public void legalInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/v2/edit/detail/info/new";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "修改法人信息=" + response);
                SendMsgBean info = HstyCallBack(response, SendMsgBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //修改结算卡信息
    public void bankaccountInfo(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/v2/edit/account/info/new";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "修改结算卡信息=" + response);
                SendMsgBean info = HstyCallBack(response, SendMsgBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 更改橙意宝状态
     * 产品标记（0：不开通，1：橙意宝，2：橙意宝（畅收版））
     * @param context
     * @param tag
     * @param map
     */
    public void updateOrangeState(final Context context, final String tag, Map<String, Object> map, final String typeStr) {
        String url = Constants.BASE_URL + "/app/merchant/v2/edit/productMark";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("updateOrangeState", "橙意宝状态" + response);
                OrangeStateBean info = HstyCallBack(response, OrangeStateBean.class);
                if (info != null) {
                    if (info.getError() != null) { //请求失败
                        String msg = info.getError().getMessage();
                        msg = StringUtils.isEmptyOrNull(msg) ? "申请失败" : msg;
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, msg));
                    } else { //请求成功
                        //info.getStatus()
                        EventBus.getDefault().postSticky(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, typeStr));
                    }
                }
            }
        });
    }



    //支付类型
    public void payTypeList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/v2/query/mch/pay";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "支付类型=" + response);
                PayTypeStatusBean info = HstyCallBack(response, PayTypeStatusBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    //推送用户绑定
    public void addPusher(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/voice/add/pushuser";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "推送用户绑定==" + response);
            }
        });
    }

    //获取分润统计 概览 数据
    public void getSummaryStatistic(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/salesman/benefit/summary";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "获取分润统计概览数据=" + response);
                ProfitsummaryBean info = HstyCallBack(response, ProfitsummaryBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //获取分润统计 列表 数据
    public void getListStatisticData(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/salesman/benefit/summary/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "获取分润统计列表数据=" + response);
                ProfitListBean info = HstyCallBack(response, ProfitListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    //获取 月分润明细 列表数据，分润明细-按日期（年月）
    public void getMonthProfitDetailList(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/salesman/benefit/date/detail/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "获取 月分润明细 列表数据=" + response);
                ProfitMonthListBean info = HstyCallBack(response, ProfitMonthListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });

    }

    //获取 日分润明细 列表数据，分润明细-按商户
    public void getDayProfitDetailList(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/salesman/benefit/merchant/detail/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "获取 日分润明细 列表数据=" + response);
                ProfitListBean info = HstyCallBack(response, ProfitListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });

    }

    /**
     * 微收银激活码收银员绑定
     *
     * @param context
     * @param tag
     * @param map
     */
    public void activationCodeUserBind(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/activationcode/bindCashier";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "收银员绑定=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });

    }


    //查询支付宝门店代运营门店信息，包括获取代运营门店id
    public void queryZfbAgentStoreInfo(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/agentop/query/auth";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "查询支付宝门店代运营门店信息=" + response);
                QueryAgentStoreInfoBean info = HstyCallBack(response, QueryAgentStoreInfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });

    }

    //生成授权二维码
    public void genZfbAgentAutoCode(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/agentop/query/qrcode";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "支付宝门店代运营生成授权二维码=" + response);
                QueryAgentStoreInfoBean info = HstyCallBack(response, QueryAgentStoreInfoBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });

    }


    //去创建支付宝代运营门店
    public void createAgentStore(Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/agentop/create/store";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "去创建支付宝代运营门店=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });

    }


    /**
     * 刷卡数据同步埋点
     *
     * @param context
     * @param tag
     * @param map
     */
    public void newConfig(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/org/newConfig";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, (response, cancalTag) -> {
            LogUtil.i("Jeremy", "功能权限=" + response);
            ConfigBean info = HstyCallBack(response, ConfigBean.class);
            if (info != null) {
                if (!info.isStatus()) {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                } else {
                    EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                }
            }
        });
    }


    /**
     * 设备类型列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void deviceTypeList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/device/category/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "设备类型列表=" + response);
                DeviceTypeBean info = HstyCallBack(response, DeviceTypeBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 已绑定设备
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getDeviceList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/device/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "已绑定设备=" + response);
                DeviceListBean info = HstyCallBack(response, DeviceListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 设备解绑
     *
     * @param context
     * @param tag
     * @param map
     */
    public void unBindDevice(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/device/unbind";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "设备解绑=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 设备详情
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getDeviceDeatil(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/device/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "设备详情=" + response);
                DeviceDetailBean info = HstyCallBack(response, DeviceDetailBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }
    /**
     * 绑定设备
     *
     * @param context
     * @param tag
     * @param map
     */
    public void bindDevice(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/device/bind";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "绑定设备=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 根据sn查询设备信息
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryDeviceInfoBySn(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/device/query/sn";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "根据sn查询设备信息=" + response);
                DeviceInfoSn info = HstyCallBack(response, DeviceInfoSn.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 获取商户微信签约详情信息
     *
     * @param context
     * @param tag
     * @param map
     */
    public void queryWxSignDetail(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/contract/detail";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "获取商户微信签约详情信息=" + response);
                WxSignDetailBean info = HstyCallBack(response, WxSignDetailBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 撤销微信签约申请单
     *
     * @param context
     * @param tag
     * @param map
     */
    public void cancelWxSignReply(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/contract/revoke/apply";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "撤销微信签约申请单=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 微信实名认证的重新签约
     *
     * @param context
     * @param tag
     * @param map
     */
    public void wxAuthReSign(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/contract/resign";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "微信实名认证的重新签约=" + response);
                Info info = HstyCallBack(response, Info.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 签约管理--列表（目前仅微信实名认证）
     *
     * @param context
     * @param tag
     * @param map
     */
    public void wxSignMerchantList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/contract/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "签约管理--列表=" + response);
                SignMerchantListBean info = HstyCallBack(response, SignMerchantListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 排名页的交易统计
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getRankSumaryData(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/trade/summary";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "排名页的交易统计=" + response);
                RankSumaryBean info = HstyCallBack(response, RankSumaryBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }


    /**
     * 商户排名列表数据
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getRankListData(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/merchant/rank/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "商户排名列表数据=" + response);
                RankListBean info = HstyCallBack(response, RankListBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 市运营列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getServiceList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/service/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "市运营列表=" + response);
                ServiceProviderBean info = HstyCallBack(response, ServiceProviderBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }
    /**
     * 城市经理列表
     *
     * @param context
     * @param tag
     * @param map
     */
    public void getSalesmanList(final Context context, final String tag, Map<String, Object> map) {
        String url = Constants.BASE_URL + "/app/salesman/list";
        String data = SignUtil.getInstance().createAppSign(map, time);
        OkHttpUtil.postData(context, url, tag, data, time, new OnResponse() {
            @Override
            public void onStringResponse(String response, int cancalTag) {
                LogUtil.i("Jeremy", "城市经理列表=" + response);
                SalesmanBean info = HstyCallBack(response, SalesmanBean.class);
                if (info != null) {
                    if (!info.isStatus()) {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_FALSE, info));
                    } else {
                        EventBus.getDefault().post(new NoticeEvent(tag, Constants.ON_EVENT_TRUE, info));
                    }
                }
            }
        });
    }

    /**
     * 泛型返回，在xutils的 httputils 中 访问网络成功后，onSuccess方法中调用
     *
     * @param arg0
     * @param
     * @param <T>
     * @return
     */
    public <T> T HstyCallBack(String arg0, Class clazz) {
        return (T) gson.fromJson(arg0, clazz);
    }
}
