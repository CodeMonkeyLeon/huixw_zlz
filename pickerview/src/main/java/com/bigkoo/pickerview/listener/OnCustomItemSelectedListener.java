package com.bigkoo.pickerview.listener;

/**
 * @author zeyu.kuang
 * @time 2020/10/28
 * @desc 给外界调用监听wheelView 选中了item的事件。外界应避免调用OnItemSelectedListener，否则造成pickview已设置的OnItemSelectedListener失效造成功能异常
 */
public interface OnCustomItemSelectedListener {
    /**
     * 选中了某个条目，子线中执行
     * @param index
     */
    void onItemSelected(int index);
}
